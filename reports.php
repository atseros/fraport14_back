<?php

include('config.php');

$con = mysqli_connect($config['DB_servername'],$config['DB_username'],$config['DB_password'],$config['DB_dbname']);

// Check connection
if (mysqli_connect_errno())
{
	$site_name = "Connection";
	$error_message = "Failed to connect to MySQL: " .mysqli_connect_error();
	
	send_error_message($site_name, $error_message);
	exit();
}

mysqli_set_charset($con,"utf8");

date_default_timezone_set('Europe/Athens') ;

// GET AIRPORT SITES
$airports = getAirportSites($con);

if(!is_array($airports) OR empty($airports))
{
	$site_name = "No Airport Found";
	$error_message = "No airports found at  getAirportSites";
	
	send_error_message($site_name, $error_message);
	exit();
}

$now = date("Y-m-d H:i:s");

mysqli_query($con,"DELETE FROM sys_flight_tracker_log WHERE FltLogDateTimeProduced < '$now' - INTERVAL 3 DAY");

foreach($airports as $air)
{
	$SiteID = $air["SiteID"];
	$SiteName = $air["SiteUrlTitle"];
	
	$now = date("Y-m-d H:i:s");
		
	//Check FLightTrackerLog FOR NOT OK
	$query = "
					SELECT 
						SiteUrlTitle
					FROM sys_flight_tracker_log
					LEFT JOIN sys_site ON (SiteID = FltLogSiteID)	
					WHERE 
					`FltLogSiteID`= '$SiteID' 
					AND
					FltLogDateTimeProduced BETWEEN '$now' - INTERVAL 30 MINUTE AND '$now'
					AND
					FltLogResult = 'OK'
					
				 ";

	$result = mysqli_query($con, $query);
	
	$SiteUrlTitleResult =mysqli_fetch_assoc($result);
	
	$SiteUrlTitle = $SiteUrlTitleResult['SiteUrlTitle'];
	
	if($SiteUrlTitle == '')
	{
		echo 'NOTOK';
	
		$subject = 'XML Report for ' . $SiteName . '';
		$html = 'Failure at inserting xml, within the last 30 minutes, for ' . $SiteName . '';
		
		require_once(realpath(__DIR__."/libphp/phpMailer")."/class.phpmailer.php");
		require_once(realpath(__DIR__."/libphp/phpMailer")."/class.smtp.php");
		
		$mail = new PHPMailer(true);
				
		
		if($config["mailsettings"] == 0)
		{
			$mail->isSMTP();
			$mail->Host = 'smtp.live.com';  
			$mail->SMTPAuth = true;                              
			$mail->Username = 'webform@outlook.com.gr';                           
			$mail->Password = '9yf4b1s7';                         
			$mail->SMTPSecure = 'tls'; 
		}
		else
        {
            $mail->isSMTP();
            $mail->Host = 'mail.qwe.gr';
            $mail->SMTPAuth = true;
            $mail->Username = 'fraport-contact@b2btech.gr';
            $mail->Password = 'ouL9HQJP';
        }


		   
		$mail->CharSet = 'UTF-8';
		
		$mail->setFrom('airports@fraport-greece.com', 'airports@fraport-greece.com');
		$mail->addAddress('fraport.alerts@b2btech.gr');
		
		$mail->Subject = $subject;
		$mail->MsgHTML($html);
		
		$mail->send();	
	}
	else
	{
		echo 'OK';
	}	
	
}

/***************************************************************************************
* getAirportSites
****************************************************************************************/
function getAirportSites($conn)
{	
		// TODO query
		$airports = array();
		
		$result = mysqli_query($conn,"SELECT * FROM sys_site");
		
		while( $row = $result->fetch_assoc())
		{
			array_push($airports, $row);
		}
		
		return $airports;
}

/***************************************************************************************
* send email notification for error
****************************************************************************************/
function send_error_message($site_name, $error_message)
{	
	echo "site_name = ".$site_name."<br> error=<br> ".$error_message."<br>";
}