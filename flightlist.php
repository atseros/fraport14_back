<?php

include('config.php');


$con = mysqli_connect($config['DB_servername'],$config['DB_username'],$config['DB_password'],$config['DB_dbname'],$config["DB_port"]);

// Check connection
if (mysqli_connect_errno())
{
	$site_name = "Connection";
	$error_message = "Failed to connect to MySQL: " .mysqli_connect_error();
	
	send_error_message($site_name, $error_message);
	exit();
}

mysqli_set_charset($con,"utf8");


// GET AIRPORT SITES
$airports = getAirportSites($con);

if(!is_array($airports) OR empty($airports))
{
	$site_name = "No Airport Found";
	$error_message = "No airports found at  getAirportSites";
	
	send_error_message($site_name, $error_message);
	exit();
}


foreach($airports as $air)
{
	// $xmlfile = '/var/www/html/fraport14cms.b2btech.gr/xmls/flighttracker_SKG.xml'; 
	// $xsdfile = __DIR__.'/demo_flightlist.xsd'; 
	$xmlfile = $FLIGHT_TRACKER_XML_FILES_ABS_PATH.$air["SiteXmlFilename"];
	$SiteID = $air["SiteID"];
	$SiteName = $air["SiteUrlTitle"];
	
	$FltLngType_Eng = 1;
	$FltLngType_Gr = 2;

	
	$xml_flights_array = parse_xml_flights($xmlfile);
	
	$FltVersion = get_NextVersion($con,$SiteID);


	//GET DATA AND INSERT

	if (is_array($xml_flights_array))
	{
		//GET flights count
		$flights_count = sizeof($xml_flights_array);

		foreach ($xml_flights_array as $fltdata)
		{	
			//FLIGHT DATA
			
			$FltID = get_NextID('sys_flight_tracker',$con);
			
			if(isset($fltdata['Type'])) { $FltType = $fltdata['Type']; } else { $FltType = "" ;}
			
			// TODO 
			//
			// mysql_real_escape_string(
			//
			
			
			if(isset($fltdata['FlightNumber'])) { $FltFlightNumber = mysqli_real_escape_string($con, $fltdata['FlightNumber']); } else { $FltFlightNumber = "" ;}
			if(isset($fltdata['FlightNumber2'])) { $FltFlightNumber2 = mysqli_real_escape_string($con, $fltdata['FlightNumber2']); } else { $FltFlightNumber2 = "" ;}
			if(isset($fltdata['FlightNumber3'])) { $FltFlightNumber3 = mysqli_real_escape_string($con, $fltdata['FlightNumber3']); } else { $FltFlightNumber3 = "" ;}
			if(isset($fltdata['FlightNumber4'])) { $FltFlightNumber4 = mysqli_real_escape_string($con, $fltdata['FlightNumber4']); } else { $FltFlightNumber4 = "" ;}
			if(isset($fltdata['FlightNumber5'])) { $FltFlightNumber5 = mysqli_real_escape_string($con, $fltdata['FlightNumber5']); } else { $FltFlightNumber5 = "" ;}
			
			if(isset($fltdata['Airline_code'])) { $FltAirlineCode = mysqli_real_escape_string($con, $fltdata['Airline_code']); } else { $FltAirlineCode = "" ;}
			if(isset($fltdata['Airline_Carrier'])) { $FltAirlineCarrier = mysqli_real_escape_string($con, $fltdata['Airline_Carrier']); } else { $FltAirlineCarrier = "" ;}
			if(isset($fltdata['Airline_IATA'])) { $FltAirlineIATA = mysqli_real_escape_string($con, $fltdata['Airline_IATA']); } else { $FltAirlineIATA = "" ;}
			if(isset($fltdata['Airline_ICAO'])) { $FltAirlineICAO = mysqli_real_escape_string($con, $fltdata['Airline_ICAO']); } else { $FltAirlineICAO = "" ;}
			
			if(isset($fltdata['ScheduledTime'])) { $FltScheduledTime = mysqli_real_escape_string($con, $fltdata['ScheduledTime']); } else { $FltScheduledTime = "" ;}
			if(isset($fltdata['EstimatedTime'])) { $FltEstimatedTime = mysqli_real_escape_string($con, $fltdata['EstimatedTime']); } else { $FltEstimatedTime = "" ;}
			if(isset($fltdata['ActualTime'])) { $FltActualTime = mysqli_real_escape_string($con, $fltdata['ActualTime']); } else { $FltActualTime = "" ;}
			
			if(isset($fltdata['Airport_name'])) { $FltAirportName = mysqli_real_escape_string($con, $fltdata['Airport_name']); } else { $FltAirportName = "" ;}
			if(isset($fltdata['Airport_shortname'])) { $FltAirportShortName = mysqli_real_escape_string($con, $fltdata['Airport_shortname']); } else { $FltAirportShortName = "" ;}
			if(isset($fltdata['Airport_iata'])) { $FltAirportIATA = mysqli_real_escape_string($con, $fltdata['Airport_iata']); } else { $FltAirportIATA = "" ;}
			if(isset($fltdata['Airport_icao'])) { $FltAirportICAO = mysqli_real_escape_string($con, $fltdata['Airport_icao']); } else { $FltAirportICAO = "" ;}
			
			if(isset($fltdata['Via1_name'])) { $FltVia1Name = mysqli_real_escape_string($con, $fltdata['Via1_name']); } else { $FltVia1Name = "" ;}
			if(isset($fltdata['Via1_shortname'])) { $FltVia1ShortName = mysqli_real_escape_string($con, $fltdata['Via1_shortname']); } else { $FltVia1ShortName = "" ;}
			if(isset($fltdata['Via1_iata'])) { $FltVia1IATA = mysqli_real_escape_string($con, $fltdata['Via1_iata']); } else { $FltVia1IATA = "" ;}
			if(isset($fltdata['Via1_icao'])) { $FltVia1ICAO = mysqli_real_escape_string($con, $fltdata['Via1_icao']); } else { $FltVia1ICAO = "" ;}
			
			if(isset($fltdata['Via2_name'])) { $FltVia2Name = mysqli_real_escape_string($con, $fltdata['Via2_name']); } else { $FltVia2Name = "" ;}
			if(isset($fltdata['Via2_shortname'])) { $FltVia2ShortName = mysqli_real_escape_string($con, $fltdata['Via2_shortname']); } else { $FltVia2ShortName = "" ;}
			if(isset($fltdata['Via2_iata'])) { $FltVia2IATA = mysqli_real_escape_string($con, $fltdata['Via2_iata']); } else { $FltVia2IATA = "" ;}
			if(isset($fltdata['Via2_icao'])) { $FltVia2ICAO = mysqli_real_escape_string($con, $fltdata['Via2_icao']); } else { $FltVia2ICAO = "" ;}
			
			if(isset($fltdata['Via3_name'])) { $FltVia3Name = mysqli_real_escape_string($con, $fltdata['Via3_name']); } else { $FltVia3Name = "" ;}
			if(isset($fltdata['Via3_shortname'])) { $FltVia3ShortName = mysqli_real_escape_string($con, $fltdata['Via3_shortname']); } else { $FltVia3ShortName = "" ;}
			if(isset($fltdata['Via3_iata'])) { $FltVia3IATA = mysqli_real_escape_string($con, $fltdata['Via3_iata']); } else { $FltVia3IATA = "" ;}
			if(isset($fltdata['Via3_icao'])) { $FltVia3ICAO = mysqli_real_escape_string($con, $fltdata['Via3_icao']); } else { $FltVia3ICAO = "" ;}
			
			if(isset($fltdata['AircraftType'])) { $FltAircraftType = mysqli_real_escape_string($con, $fltdata['AircraftType']); } else { $FltAircraftType = "" ;}
			
			if(isset($fltdata['KindOfFlight'])) { $FltKindOfFlight = mysqli_real_escape_string($con, $fltdata['KindOfFlight']); } else { $FltKindOfFlight = "" ;}
			
			if(isset($fltdata['Terminal'])) { $FltTerminal = mysqli_real_escape_string($con, $fltdata['Terminal']); } else { $FltTerminal = "" ;}
			
			if(isset($fltdata['Gate'])) { $FltGate = mysqli_real_escape_string($con, $fltdata['Gate']); } else { $FltGate = "" ;}
			
			if(isset($fltdata['CheckIn'])) { $FltCheckIn = mysqli_real_escape_string($con, $fltdata['CheckIn']); } else { $FltCheckIn = "" ;}
			
			if(isset($fltdata['RemarkFids_code'])) { $FltRemarkFids = mysqli_real_escape_string($con, $fltdata['RemarkFids_code']); } else { $FltRemarkFids = "" ;}
			
						 
			
			// Insert Data
			mysqli_query($con,"INSERT INTO sys_flight_tracker 
											(FltID
											,FltSiteID
											,FltType
											,FltFlightNumber
											,FltFlightNumber2
											,FltFlightNumber3
											,FltFlightNumber4
											,FltFlightNumber5
											,FltAirlineCode
											,FltAirlineCarrier
											,FltAirlineIATA
											,FltAirlineICAO
											,FltScheduledTime
											,FltEstimatedTime
											,FltActualTime
											,FltAirportName
											,FltAirportShortName
											,FltAirportIATA
											,FltAirportICAO
											,FltVia1Name
											,FltVia1ShortName
											,FltVia1IATA
											,FltVia1ICAO
											,FltVia2Name
											,FltVia2ShortName
											,FltVia2IATA
											,FltVia2ICAO
											,FltVia3Name
											,FltVia3ShortName
											,FltVia3IATA
											,FltVia3ICAO
											,FltAircraftType
											,FltKindOfFlight
											,FltTerminal
											,FltGate
											,FltCheckIn
											,FltRemarkFids
											,FltVersion
											) 
											VALUES 
											('$FltID'
											,'$SiteID'
											,'$FltType'
											,'$FltFlightNumber'
											,'$FltFlightNumber2'
											,'$FltFlightNumber3'
											,'$FltFlightNumber4'
											,'$FltFlightNumber5'
											,'$FltAirlineCode'
											,'$FltAirlineCarrier'
											,'$FltAirlineIATA'
											,'$FltAirlineICAO'
											,'$FltScheduledTime'
											,'$FltEstimatedTime'
											,'$FltActualTime'
											,'$FltAirportName'
											,'$FltAirportShortName'
											,'$FltAirportIATA'
											,'$FltAirportICAO'
											,'$FltVia1Name'
											,'$FltVia1ShortName'
											,'$FltVia1IATA'
											,'$FltVia1ICAO'
											,'$FltVia2Name'
											,'$FltVia2ShortName'
											,'$FltVia2IATA'
											,'$FltVia2ICAO'
											,'$FltVia3Name'
											,'$FltVia3ShortName'
											,'$FltVia3IATA'
											,'$FltVia3ICAO'
											,'$FltAircraftType'
											,'$FltKindOfFlight'
											,'$FltTerminal'
											,'$FltGate'
											,'$FltCheckIn'
											,'$FltRemarkFids'
											,'$FltVersion'
											)"
								) or send_error_message($SiteName, $FltFlightNumber);
				
				//TRANSLATIONS
				
				//EN	
				$FltLngID = get_NextID('sys_flight_trackerlng',$con);	

				if(isset($fltdata['Airport_Translations_en'])) { $AirportNameEN = $fltdata['Airport_Translations_en']; } else { $AirportNameEN = "" ;}
				if(isset($fltdata['Via1_Translations_en'])) { $Via1EN = $fltdata['Via1_Translations_en']; } else { $Via1EN = "" ;}
				if(isset($fltdata['Via2_Translations_en'])) { $Via2EN = $fltdata['Via2_Translations_en']; } else { $Via2EN = "" ;}
				if(isset($fltdata['Via3_Translations_en'])) { $Via3EN = $fltdata['Via3_Translations_en']; } else { $Via3EN = "" ;}
				if(isset($fltdata['RemarkFids_Translations_en'])) { $RemarksEN = $fltdata['RemarkFids_Translations_en']; } else { $RemarksEN = "" ;}
				
				// Insert ΕΝ Data
				mysqli_query($con,"INSERT INTO sys_flight_trackerlng 
											(FltLngID
											,FltLngFltID
											,FltLngType
											,FltLngAirport
											,FltLngVia1
											,FltLngVia2
											,FltLngVia3
											,FltLngRemarks
											) 
											VALUES 
											('$FltLngID'
											,'$FltID'
											,'$FltLngType_Eng'
											,'$AirportNameEN'
											,'$Via1EN'
											,'$Via2EN'
											,'$Via3EN'
											,'$RemarksEN'
											)"
								);
				
				//GR	
				$FltLngID = get_NextID('sys_flight_trackerlng',$con);	

				if(isset($fltdata['Airport_Translations_el'])) { $AirportNameEL = $fltdata['Airport_Translations_el']; } else { $AirportNameEL = "" ;}
				if(isset($fltdata['Via1_Translations_el'])) { $Via1EL = $fltdata['Via1_Translations_el']; } else { $Via1EL = "" ;}
				if(isset($fltdata['Via2_Translations_el'])) { $Via2EL = $fltdata['Via2_Translations_el']; } else { $Via2EL = "" ;}
				if(isset($fltdata['Via3_Translations_el'])) { $Via3EL = $fltdata['Via3_Translations_el']; } else { $Via3EL = "" ;}
				if(isset($fltdata['RemarkFids_Translations_el'])) { $RemarksEL = $fltdata['RemarkFids_Translations_el']; } else { $RemarksEL = "" ;}
				
				// Insert ΕΝ Data
				mysqli_query($con,"INSERT INTO sys_flight_trackerlng 
											(FltLngID
											,FltLngFltID
											,FltLngType
											,FltLngAirport
											,FltLngVia1
											,FltLngVia2
											,FltLngVia3
											,FltLngRemarks
											) 
											VALUES 
											('$FltLngID'
											,'$FltID'
											,'$FltLngType_Gr'
											,'$AirportNameEL'
											,'$Via1EL'
											,'$Via2EL'
											,'$Via3EL'
											,'$RemarksEL'
											)"
								);
				
		}
		
		//Check consistency
		$query_count = "
			SELECT 
				Count(FltID) as NumOfFlt
			FROM sys_flight_tracker 
			WHERE 
			`FltSiteID`= '$SiteID' 
			AND 
			FltVersion = '$FltVersion'
		";
		$flights_inserted = mysqli_query($con, $query_count);

		$flights_inserted_nr =mysqli_fetch_assoc($flights_inserted);
		
		$Flt_Inserted = $flights_inserted_nr['NumOfFlt'];
		
		if ($flights_count == $Flt_Inserted)
		{
			$message = "FltVersion= ".$FltVersion."\n";
			$message .= "SiteID= ".$SiteID."\n";
			$message .= "XML flights_count= ".$flights_count."\n";
			$message .= "Flt_Inserted= ".$Flt_Inserted."\n";
			send_error_message($SiteName, $message);
			
			log_message($con, $SiteID, $xml_flights_array['timestamp'], "OK");	
			
			$FltVersionForDelete = $FltVersion - 30;
			
			// DELETE PREVIOUS VERSIONS FROM FLIGHT TRACKER
			mysqli_query($con,"DELETE FROM sys_flight_tracker WHERE FltSiteID = $SiteID AND FltVersion <= $FltVersionForDelete");	
			
			// DELETE PREVIOUS TRANSLATIONS FROM FLIGHT TRACKER
			mysqli_query($con,"DELETE FROM sys_flight_trackerlng WHERE FltLngFltID NOT IN (SELECT FltID FROM sys_flight_tracker)");
		}
		else
		{
			// delete last version records for this site
			$message = "FltVersion= ".$FltVersion."\n";
			$message .= "SiteID= ".$SiteID."\n";
			$message .= "XML flights_count= ".$flights_count."\n";
			$message .= "Flt_Inserted= ".$Flt_Inserted."\n";
			
			log_message($con, $SiteID, $xml_flights_array['timestamp'], $message."NOT OK - Not all records of xml inserted in database!");			
			send_error_message($SiteName, $message."Not all records of xml inserted in database!");
			mysqli_query($con,"DELETE FROM sys_flight_tracker WHERE FltSiteID = $SiteID AND FltVersion = $FltVersion");	
		}		
		
	}
	
}



/***************************************************************************************
* getAirportSites
****************************************************************************************/
function getAirportSites($conn)
{	
		// TODO query
		$airports = array();
		
		$result = mysqli_query($conn,"SELECT * FROM sys_site");
		
		while( $row = $result->fetch_assoc())
		{
			array_push($airports, $row);
		}

		
		// $airports[0]["SiteUrlTitle"] = "Thessaloniki Airport";
		// $airports[0]["SiteID"] = 1;
		// $airports[0]["SiteAirportCode"] = "SKG";
		// $airports[0]["SiteDomainName"] = "xip.fraport14_front.gr";
		// $airports[0]["SiteXmlFilename"] = "flighttracker_SKG.xml";


		// $airports[1]["SiteUrlTitle"] = "Kekryra Airport";
		// $airports[1]["SiteID"] = 2;
		// $airports[1]["SiteAirportCode"] = "CFU";
		// $airports[1]["SiteDomainName"] = "xip.fraport14_kerkyra.gr";
		// $airports[1]["SiteXmlFilename"] = "flighttracker_CFU.xml";
		
		return $airports;
}

/***************************************************************************************
* parse_xml_flights
****************************************************************************************/
function log_message($conn, $SiteID, $xml_timestamp, $message)
{	
	$FltLogID = get_NextID('sys_flight_tracker_log',$conn);
	$FltLogDateTimeProduced = $xml_timestamp;
	$FltLogDateTimeProcessed = date("Y-m-d H:i:s");
		
	$query = "
		INSERT INTO sys_flight_tracker_log 
		(FltLogID
		,FltLogResult
		,FltLogDateTimeProduced
		,FltLogDateTimeProcessed
		,FltLogSiteID
		) 
		VALUES 
		('$FltLogID'
		,'$message'
		,'$FltLogDateTimeProduced'
		,'$FltLogDateTimeProcessed'
		,'$SiteID'
		)
	";
	
	// Insert Success Log
	mysqli_query($conn,	$query);
}

/***************************************************************************************
* send email notification for error
****************************************************************************************/
function send_error_message($site_name, $error_message)
{	
	echo "site_name = ".$site_name."<br> error=<br> ".$error_message."<br>";
}

/***************************************************************************************
* get_NextID
****************************************************************************************/
function get_NextID($tableName,$con)
{	
	
	$newID = 0;
	
	$result = mysqli_query($con,"SELECT KxnInc FROM sys_kxn WHERE KxnTableName='$tableName' LIMIT 1");

	$newID=mysqli_fetch_assoc($result);
	
	$newID = $newID['KxnInc'];
	
	// update next id for next time 
	$nextID = $newID + 1;
	
	mysqli_query($con,"UPDATE sys_kxn SET KxnInc='$nextID' WHERE KxnTableName='$tableName'");
	
	// return 
	return $newID;
}


/***************************************************************************************
* get_NextVersion
*
* GET MAX VERSION
****************************************************************************************/
function get_NextVersion($con,$SiteID)
{	
	
	$result = mysqli_query($con,"SELECT MAX(FltVersion) FROM sys_flight_tracker WHERE `FltSiteID`= $SiteID");

	$MaxVersion=mysqli_fetch_assoc($result);
	
	$MaxVersion = $MaxVersion['MAX(FltVersion)'];
	
	$MaxVersion = $MaxVersion + 1;
	// return 
	return $MaxVersion;
}

/***************************************************************************************
* parse_xml_flights
****************************************************************************************/
function parse_xml_flights($xmlfile)
{
	$flights_array = array();

	$xml = new DOMDocument(); 
	$xml->load($xmlfile); 

	$x = $xml->documentElement;

	if($x->hasAttributes())
	foreach ($x->attributes as $attr) 
	{
		if($attr->nodeName == "timestamp")
			$flights_array["timestamp"] = $attr->nodeValue;
	}
	
		$flight_count=1;
	foreach ($x->childNodes AS $flight) 
	{
		if($flight->nodeName == "Flight")
		{			
			$flights_array[$flight_count] = array();
			
			
			if($flight->childNodes)
			{
				foreach ($flight->childNodes AS $flight_elem) 
				{
					if($flight_elem->nodeName == "Type")
						$flights_array[$flight_count]["Type"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber")
						$flights_array[$flight_count]["FlightNumber"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber2")
						$flights_array[$flight_count]["FlightNumber2"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber3")
						$flights_array[$flight_count]["FlightNumber3"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber4")
						$flights_array[$flight_count]["FlightNumber4"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber5")
						$flights_array[$flight_count]["FlightNumber5"] = $flight_elem->nodeValue;

					// AIRLINE
					else if($flight_elem->nodeName == "Airline")
					{
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "code")
								$flights_array[$flight_count]["Airline_code"] = $attr->nodeValue;
						}
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Carrier" )
								$flights_array[$flight_count]["Airline_Carrier"] = $child->nodeValue;
							else if($child->nodeName == "IATA" )
								$flights_array[$flight_count]["Airline_IATA"] = $child->nodeValue;
							else if($child->nodeName == "ICAO" )
								$flights_array[$flight_count]["Airline_ICAO"] = $child->nodeValue;
						}
					}
					else if($flight_elem->nodeName == "ScheduledTime")
					{					
						date_default_timezone_set('UTC');
						$timestamp = strtotime($flight_elem->nodeValue);					
						date_default_timezone_set('Europe/Athens') ;
						
						$flights_array[$flight_count]["ScheduledTime"] = date( 'Y-m-d H:i', $timestamp);
					}
					else if($flight_elem->nodeName == "EstimatedTime")
					{
						date_default_timezone_set('UTC');
						$timestamp = strtotime($flight_elem->nodeValue);					
						date_default_timezone_set('Europe/Athens') ;
						
						$flights_array[$flight_count]["EstimatedTime"] = date( 'Y-m-d H:i', $timestamp);
					}
					else if($flight_elem->nodeName == "ActualTime")
					{
						date_default_timezone_set('UTC');
						$timestamp = strtotime($flight_elem->nodeValue);					
						date_default_timezone_set('Europe/Athens') ;
						
						$flights_array[$flight_count]["ActualTime"] = date( 'Y-m-d H:i', $timestamp);
					}
					
					// AIRPORT
					else if($flight_elem->nodeName == "Airport")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "iata")
								$flights_array[$flight_count]["Airport_iata"] = $attr->nodeValue;
							else if($attr->nodeName == "icao")
								$flights_array[$flight_count]["Airport_icao"] = $attr->nodeValue;
							else if($attr->nodeName == "name")
								$flights_array[$flight_count]["Airport_name"] = $attr->nodeValue;
							else if($attr->nodeName == "shortName")
								$flights_array[$flight_count]["Airport_shortname"] = $attr->nodeValue;
						}
						
						// $translation_count = 1;
						// $flights_array[$flight_count]["Airport_Translations"][$translation_count] = array();
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$tmp_name = $child->nodeValue;
								//$flights_array[$flight_count]["Airport_Translations"][$translation_count]["name"] = $child->nodeValue;
								
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
									{
										$tmp_lang = $attr->nodeValue;
										$flights_array[$flight_count]["Airport_Translations_".$tmp_lang] = $tmp_name;
										//$flights_array[$flight_count]["Airport_Translations"][$translation_count]["lang"] = $attr->nodeValue;
									}	
										
								}
								
								//$translation_count++;
							}
						}
					}
					// Via1
					else if($flight_elem->nodeName == "Via1")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "iata")
								$flights_array[$flight_count]["Via1_iata"] = $attr->nodeValue;
							else if($attr->nodeName == "icao")
								$flights_array[$flight_count]["Via1_icao"] = $attr->nodeValue;
							else if($attr->nodeName == "name")
								$flights_array[$flight_count]["Via1_name"] = $attr->nodeValue;
							else if($attr->nodeName == "shortName")
								$flights_array[$flight_count]["Via1_shortname"] = $attr->nodeValue;
						}
						
						//$translation_count = 1;
						//$flights_array[$flight_count]["Via1_Translations"][$translation_count] = array();
						
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$tmp_name = $child->nodeValue;
								//$flights_array[$flight_count]["Via1_Translations"][$translation_count]["name"] = $child->nodeValue;
									
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
									{
										$flights_array[$flight_count]["Via1_Translations_".$tmp_lang] = $tmp_name;										
										//$flights_array[$flight_count]["Via1_Translations"][$translation_count]["lang"] = $attr->nodeValue;
									}	
								}
								
								//$translation_count++;
							}
						}
					}
					// Via2
					else if($flight_elem->nodeName == "Via2")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "iata")
								$flights_array[$flight_count]["Via2_iata"] = $attr->nodeValue;
							else if($attr->nodeName == "icao")
								$flights_array[$flight_count]["Via2_icao"] = $attr->nodeValue;
							else if($attr->nodeName == "name")
								$flights_array[$flight_count]["Via2_name"] = $attr->nodeValue;
							else if($attr->nodeName == "shortName")
								$flights_array[$flight_count]["Via2_shortname"] = $attr->nodeValue;
						}
						
						//$translation_count = 1;
						//$flights_array[$flight_count]["Via2_Translations"][$translation_count] = array();
						
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$tmp_name = $child->nodeValue;
								//$flights_array[$flight_count]["Via2_Translations"][$translation_count]["name"] = $child->nodeValue;
									
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
									{
										$flights_array[$flight_count]["Via2_Translations_".$tmp_lang] = $tmp_name;	
										//$flights_array[$flight_count]["Via2_Translations"][$translation_count]["lang"] = $attr->nodeValue;
									}
										
								}
								
								//$translation_count++;
							}
						}
					}
					
					// Via3
					else if($flight_elem->nodeName == "Via3")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "iata")
								$flights_array[$flight_count]["Via3_iata"] = $attr->nodeValue;
							else if($attr->nodeName == "icao")
								$flights_array[$flight_count]["Via3_icao"] = $attr->nodeValue;
							else if($attr->nodeName == "name")
								$flights_array[$flight_count]["Via3_name"] = $attr->nodeValue;
							else if($attr->nodeName == "shortName")
								$flights_array[$flight_count]["Via3_shortname"] = $attr->nodeValue;
						}
						
						//$translation_count = 1;
						//$flights_array[$flight_count]["Via3_Translations"][$translation_count] = array();
						
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$tmp_name = $child->nodeValue;
								//$flights_array[$flight_count]["Via3_Translations"][$translation_count]["name"] = $child->nodeValue;
								
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
									{
										$flights_array[$flight_count]["Via3_Translations_".$tmp_lang] = $tmp_name;	
										//$flights_array[$flight_count]["Via3_Translations"][$translation_count]["lang"] = $attr->nodeValue;
									}
										
								}
								
								//$translation_count++;
							}
						}
					}
					else if($flight_elem->nodeName == "AircraftType")
						$flights_array[$flight_count]["AircraftType"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "KindOfFlight")
						$flights_array[$flight_count]["KindOfFlight"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "Terminal")
						$flights_array[$flight_count]["Terminal"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "Gate")
						$flights_array[$flight_count]["Gate"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "CheckIn")
						$flights_array[$flight_count]["CheckIn"] = $flight_elem->nodeValue;
					
					
					// RemarkFids
					else if($flight_elem->nodeName == "RemarkFids")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "code")
								$flights_array[$flight_count]["RemarkFids_code"] = $attr->nodeValue;
						}
						
						//$translation_count = 1;
						//$flights_array[$flight_count]["RemarkFids_Translations"][$translation_count] = array();
						
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$tmp_name = $child->nodeValue;	
								//$flights_array[$flight_count]["RemarkFids_Translations"][$translation_count]["name"] = $child->nodeValue;
									
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
									{
										$tmp_lang = $attr->nodeValue;
										$flights_array[$flight_count]["RemarkFids_Translations_".$tmp_lang] = $tmp_name;
										//$flights_array[$flight_count]["RemarkFids_Translations"][$translation_count]["lang"] = $attr->nodeValue;
									}	
								}
								
								//$translation_count++;
							}
						}
					}				
				}
			}		
			
			$flight_count++;
		}
	  
	}
	return $flights_array;
}




















