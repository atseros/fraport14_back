<?php
// set error reporting
error_reporting(E_ALL | E_STRICT);


$config["AppName"]			= "FraportAirports";

$config["DB_servername"]	= "10.10.192.4";
$config["DB_username"]	= "fraport14user";
$config["DB_password"]	= "3rmVe0oW2zm626a";
$config["DB_dbname"]		= "fraport14";
$config["DB_port"]			= "3307";

$config["seo_urls"] 		= 1;
$config["default_app"]		= "Home";
$config["home_app"]		= "Home";
//$config["solr_url"]			= 'http://192.168.60.107:8989/solr/';

// FILE MANAGEMENT
$ROOT_FOLDER_FOR_FILES = realpath(__DIR__).'/uploads/';  
$ROOT_URL_FOR_FILES = '/uploads/';

$ROOT_FOLDER_FOR_FILES_OF_LINE  = "c:/wamp/www/of_line_data/";  


$FLIGHT_TRACKER_XML_FILES_ABS_PATH = "/var/xmldata/";

// LANGUAGES
$config["default_lang"]		= "en";

$config["supported_lang"]["en"]["id"] 		= 1;
$config["supported_lang"]["en"]["code"] 	= "en";
$config["supported_lang"]["en"]["descr"] = "english";

$config["supported_lang"]["el"]["id"] 		= 2;
$config["supported_lang"]["el"]["code"] 	= "el";
$config["supported_lang"]["el"]["descr"] = "greek";

$config["supported_lang"]["de"]["id"] 		= 3;
$config["supported_lang"]["de"]["code"] 	= "de";
$config["supported_lang"]["de"]["descr"] = "deutch";

$config["supported_lang"]["ru"]["id"] 		= 4;
$config["supported_lang"]["ru"]["code"] 	= "ru";
$config["supported_lang"]["ru"]["descr"] = "russian";

$config["mailsettings"] = 1;

?>