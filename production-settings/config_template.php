<?php
// set error reporting
error_reporting(E_ALL | E_STRICT);

include('lastBuild.php');

$config["AppName"]			= "FraportAirports";

// DATABASE

$config["DB_servername"]	= "192.168.60.108";
$config["DB_username"]		= "root";
$config["DB_password"]		= "QWer!@34";
$config["DB_dbname"]		= "fraport14";
$config["DB_port"]			= "3306";


// $config["DB_servername"]	= "localhost";
// $config["DB_username"]		= "root";
// $config["DB_password"]		= "";
// $config["DB_dbname"]			= "fraport14";

//Languages
$config["default_lang"]		= "el";

$config["supported_lang"]["en"]["id"] 		= 1;
$config["supported_lang"]["en"]["code"] 	= "en";
$config["supported_lang"]["en"]["descr"] = "english";

$config["supported_lang"]["el"]["id"] 		= 2;
$config["supported_lang"]["el"]["code"] 	= "el";
$config["supported_lang"]["el"]["descr"] = "greek";

$config["supported_lang"]["de"]["id"] 		= 3;
$config["supported_lang"]["de"]["code"] 	= "de";
$config["supported_lang"]["de"]["descr"] = "deutch";

$config["supported_lang"]["ru"]["id"] 		= 4;
$config["supported_lang"]["ru"]["code"] 	= "ru";
$config["supported_lang"]["ru"]["descr"] = "russian";


$config["is_linux"] = 1;	

$config["solr_url"]			= 'http://192.168.60.8:8989/solr/';

// FILE MANAGEMENT

$ROOT_FOLDER_FOR_FILES = realpath(__DIR__).'\\uploads\\';  
$ROOT_URL_FOR_FILES = '/uploads/';

$ROOT_FOLDER_FOR_FILES_OF_LINE  = "c:/wamp/www/of_line_data/";  

$config["captcha_proxy"] = '';	

$config["mailsettings"] = 0;

$config["last_build"] = LAST_BUILD;

?>