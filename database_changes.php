<?php
exit(0);
?>



CREATE TABLE sys_kxn
(
	 KxnID					int(11)
	,KxnInc					int(11)
	,KxnTableName		varchar(64)
	,KxnTableDescr		varchar(512)
	
	,PRIMARY KEY ( KxnID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE sys_usr 
(
	 UsrID 				int(11) NOT NULL
	,UsrLogin 			varchar(512)
	,UsrPassword	varchar(512)
	,UsrStatus	 	int(11)
	,UsrRole 			int(11)
	,UsrFirstName 	varchar(512)
	,UsrLastName 	varchar(512)
	
	,PRIMARY KEY ( UsrID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8


INSERT INTO sys_usr 
(
	 UsrID
	,UsrLogin
	,UsrPassword
	,UsrStatus
	,UsrRole 
	,UsrFirstName 
	,UsrLastName 
) 
VALUES
(
	 1
	,'admin'
	,MD5('QWer!@34')
	,1
	,1
	,'admin'
	,'user '	
)

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (1, 2, 'sys_usr', 'Users')



CREATE TABLE sys_site
(
	 SiteID						int(11) NOT NULL
	,SiteDomainName		varchar(512)
	
	,PRIMARY KEY ( SiteID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (2, 1, 'sys_site', 'Sites')


CREATE TABLE sys_node 
(
	 NodeID					int(11) NOT NULL
	,NodeSiteID				int(11)
	,NodeNodeID	 		int(11)
	,NodeLevel 				int(11)
	,NodeCode				int(11) NOT NULL DEFAULT '0'
	,NodeParentCode		varchar(1024)
	,NodeFullCode			varchar(1024)
	,NodeTitle					varchar(1024)
	,NodeUrlAlias 			varchar(128)
	,NodePublished 			int(11) NOT NULL DEFAULT '0'
	,NodeIsHomePage	int(11)  NOT NULL DEFAULT '0'
	,NodeTemplateFile		varchar(1024)
	,NodeCreatedAt		date NOT NULL
	
	,PRIMARY KEY ( NodeID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (3, 1, 'sys_node', 'Nodes')



CREATE TABLE sys_nodelng 
(
	 NodeLngID					int(11) NOT NULL
	,NodeLngNodeID	 		int(11)
	,NodeLngType	 			int(11)
	,NodeLngTitle				varchar(1024)
	,NodeLngStory				text
	,NodeLngIntro				text
	
	,PRIMARY KEY ( NodeLngID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (4, 1, 'sys_nodelng', 'Node Translation')

/*
ΕΙΣΑΓΩΓΗ ΔΟΚΙΜΑΣΤΙΚΩΝ ΕΓΓΡΑΦΩΝ ΣΤΟΝ Nodes
*/

INSERT INTO sys_site
(
	 SiteID
	,SiteDomainName
)
VALUES
(
	1
	,'xip.fraport14_front.gr'
);

INSERT INTO sys_node 
(
	 NodeID
	,NodeSiteID
	,NodeNodeID
	,NodeLevel
	,NodeCode
	,NodeParentCode
	,NodeFullCode
	,NodeTitle
	,NodeUrlAlias
	,NodePublished
	,NodeIsHomePage
	,NodeTemplateFile
	,NodeCreatedAt
)
VALUES
(
	 1
	,1
	,0
	,0
	,'001'
	,''
	,'001'
	,'Home Page'
	,'home_page'
	,1
	,1
	,'tpl_home_page'
	,'2016-12-06'
);

INSERT INTO sys_nodelng 
(
	 NodeLngID
	,NodeLngNodeID
	,NodeLngType
	,NodeLngTitle
	,NodeLngIntro
	,NodeLngStory	
)
VALUES
(
	 1
	,1
	,1
	,'Home Page'
	,'test intro'
	,'test story'
	
);

INSERT INTO sys_nodelng 
(
	 NodeLngID
	,NodeLngNodeID
	,NodeLngType
	,NodeLngTitle
	,NodeLngIntro
	,NodeLngStory	
)
VALUES
(
	 2
	,1
	,2
	,'Αρχική Σελίδα'
	,'δοκιμή εισαγωγής'
	,'δοκιμή κειμένου άρθρου'	
);

UPDATE sys_kxn SET KxnInc = 2 WHERE  KxnTableName = 'sys_site';

UPDATE sys_kxn SET KxnInc = 2 WHERE  KxnTableName = 'sys_node';

UPDATE sys_kxn SET  KxnInc = 3 WHERE  KxnTableName = 'sys_nodelng';




CREATE TABLE dat_airports 
(
	 AipID					int(11) NOT NULL
	,Aip_Code				varchar(1024)
	,Aip_ICAO				varchar(1024)
	,PRIMARY KEY ( AipID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE dat_airports_lang 
(
	 AipLngID					int(11) NOT NULL
	,AipLngAipID				int(11) 
	,AipLngLngType			int(11) 
	
	,AipLng_Region			varchar(1024)
	,AipLng_Regionorg		varchar(1024)
	,AipLng_NameShort	varchar(1024)
	,AipLng_Name			varchar(1024)
	,AipLng_Land				varchar(1024)
	,PRIMARY KEY ( AipLngID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8

/*
example

[region] => af													Region
[id] => ABV														Code
[nameshort] => Abuja										NameShort
[uwi] => 1
[icao] => DNAA													ICAO
[name] => Abuja-Nnamdi Azikiwe International	Name
[land] => Nigeria												Land
[regionorg] => Nord-Afrika								Regionorg
*/

CREATE TABLE dat_airlines
(
	 ArlID					int(11) NOT NULL
	,Arl_Code				varchar(1024)
	,PRIMARY KEY ( ArlID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE dat_airlines_lang
(
	 ArlLngID				int(11) NOT NULL
	,ArlLngArlID			int(11) 
	,ArlLngLngType		int(11) 
	
	,ArlLng_Name		varchar(1024)
	,PRIMARY KEY ( ArlLngID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8

/*
example

[id] => SM					Code
[t] => 2
[s] => 886-888
[name] => Air Cairo		Name
[h] => D
*/

CREATE TABLE dat_flights
(
	 FliID									int(11) NOT NULL
	,Fli_timestamp						varchar(1024)
	,Fli_Type								char(1)
	,Fli_FlightNumber					varchar(1024)
	,Fli_FlightNumber2				varchar(1024)
	,Fli_FlightNumber3				varchar(1024)
	,Fli_FlightNumber4				varchar(1024)
	,Fli_FlightNumber5				varchar(1024)
	,Fli_ScheduledTime				varchar(1024)
	,Fli_EstimatedTime					varchar(1024)
	,Fli_ActualTime						varchar(1024)
	,Fli_Airport_iata						varchar(512)
	,Fli_Airport_icao					varchar(512)
	
	,Fli_Via1_iata						varchar(512)
	,Fli_Via1_icao						varchar(512)
	
	,Fli_Via2_iata						varchar(512)
	,Fli_Via2_icao						varchar(512)
	
	,Fli_AircraftType					varchar(512)
	,Fli_KindOfFlight					char(1)
	,Fli_Terminal							varchar(512)
	,Fli_Gate								varchar(512)
	,Fli_CheckIn							varchar(512)
	,Fli_RemarkFids_code				varchar(512)
	

	,Fli_Duration							int(11) 
	,PRIMARY KEY ( FliID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE dat_flights_lang
(
	 FliLngID								int(11) NOT NULL
	,FliLngFliID							int(11)
	,FliLngLngType						int(11)
	
	,FliLng_Airport_Name				varchar(1024)
	,FliLng_Via1_Name				varchar(1024)
	,FliLng_Via2_Name				varchar(1024)
	,FliLng_RemarkFids_Name		varchar(1024)	
	,PRIMARY KEY ( FliLngID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8

/*
examples
<Flights timestamp="24.10.2016 13:15" xmlns="http://www.isogmbh.de/flights_xml">
	<Flight>
		<Type>A</Type>
		<FlightNumber>A37732</FlightNumber>
		<FlightNumber2>DE7732</FlightNumber2>
		<FlightNumber3>LH7732</FlightNumber3>
		<FlightNumber4>SX7732</FlightNumber4>
		<FlightNumber5>AB7732</FlightNumber5>
		<ScheduledTime>07:30</ScheduledTime>
		<EstimatedTime>07:30</EstimatedTime>
		<ActualTime>07:35</ActualTime>
		<Airport iata="MUC" icao="EDDM">
			<Translation language="de">München</Translation>
			<Translation language="en">Munich</Translation>
		</Airport>
		<Via1 iata="NUE" icao="EDDN">
			<Translation language="de">Nürnberg</Translation>
			<Translation language="en">Nuremberg</Translation>
		</Via1>
		<Via2 iata="FRA" icao="EDDF">
			<Translation language="de">Frankfurt</Translation>
			<Translation language="en">Frankfurt</Translation>
		</Via2>
		<AircraftType>A320</AircraftType>
		<KindOfFlight>S</KindOfFlight>
		<Terminal>1</Terminal>
		<Gate>G01</Gate>
		<CheckIn>123-124</CheckIn>
		<RemarkFids code="LAND">
			<Translation language="de">Gelandet</Translation>
			<Translation language="en">Landed</Translation>
		</RemarkFids>
	</Flight>
	
	
	[fnr] => LH 084										FlightNumber
	[halle] => AB
	[status] => Gate open							RemarkFids_Name
	[apname] => Dusseldorf						Airport_Name
	[flstatus] => 1
	[lu] => 2016-12-07T16:46:00+0100		
	[lang] => en
	[cs] => Array
	(
		[0] => CA 6031									FlightNumber2
		[1] => JJ 6016									FlightNumber3
		[2] => MS 9005								FlightNumber4
		[3] => NH 6065								FlightNumber5
		[4] => TP 7752
	)
	[gate] => A40											Gate
	[id] => d20161207lh084
	[stops] => 0												Count(Via)
	[schalter] => 260-461								CheckIn
	[al] => LH												
	[iata] => DUS											Airport_iata
	[duration] => 50
	[s] => 
	[terminal] => 1											Terminal
	[reg] => DAIZG
	[sched] => 2016-12-07T16:50:00+0100	ScheduledTime
	[ac] => A320											AircraftType
	[alname] => Lufthansa
	[typ] => P

*/


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (5, 1, 'dat_airports', 'Airports Data');
INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (6, 1, 'dat_airports_lang', 'Airports Data Translation');
INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (7, 1, 'dat_airlines', 'Airlines Data');
INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (8, 1, 'dat_airlines_lang', 'Airlines Data Translation');
INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (9, 1, 'dat_flights', 'Flights Data ');
INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (10, 1, 'dat_flights_lang', 'Flights Data Translation');

////////////// 13/12/2016 \\\\\\\\\\\\\\\\

CREATE TABLE sys_tpl 
(
	 TplID					int(11) NOT NULL
	,TplName		varchar(1024)
	
	,PRIMARY KEY ( TplID )
) ENGINE=InnoDB DEFAULT CHARSET=utf8


INSERT INTO sys_tpl ( TplID, TplName) VALUES (1, 'tpl_home_page')

////////////// 14/12/2016 XIPOLAIS \\\\\\\\\\\\\\\\
ALTER TABLE dat_airlines ADD ArlSiteID INT( 11 );
ALTER TABLE dat_airports ADD AipSiteID INT( 11 );

UPDATE dat_airlines SET ArlSiteID = 1;
UPDATE dat_airports SET AipSiteID = 1;

////////////// 20/12/2016 TSEROS \\\\\\\\\\\\\\\\
ALTER TABLE sys_node ADD NodeMenu INT( 11 );

////////////// 23/12/2016 TSEROS \\\\\\\\\\\\\\\\
CREATE TABLE `sys_shop` (
 `ShopID` int(11) NOT NULL,
 `ShopTitle` varchar(1024) NOT NULL,
 `ShopSubTitle` varchar(1024) NOT NULL,
 `ShopText` text NOT NULL,
 `ShopPayment` varchar(1024) NOT NULL,
 `ShopLat` varchar(1024) NOT NULL,
 `ShopLong` varchar(1024) NOT NULL,
 `ShopImgFilename` varchar(1024) NOT NULL,
 PRIMARY KEY (`ShopID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (11, 1, 'sys_shop', 'Shops');

ALTER TABLE sys_shop ADD ShopPriority INT( 11 );
ALTER TABLE sys_shop ADD ShopSiteID INT( 11 );
ALTER TABLE sys_shop ADD ShopStatus INT( 11 );
ALTER TABLE sys_shop DROP COLUMN ShopLong;
ALTER TABLE sys_shop DROP COLUMN ShopLat;


////////////// 23/12/2016 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `shop_location` (
 `LocationID` int(11) NOT NULL,
 `LocationOpeningTimes` varchar(1024) NOT NULL,
 `LocationImgFilename` varchar(1024) NOT NULL,
 `LocationLocation` varchar(1024) NOT NULL,
 `LocationPhone` varchar(1024) NOT NULL,
 `LocationEmail` varchar(1024) NOT NULL,
 PRIMARY KEY (`LocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (12, 1, 'shop_location', 'Shop Location');

ALTER TABLE shop_location ADD LocationShopID INT( 11 );
ALTER TABLE shop_location ADD LocationLatitude INT( 11 );
ALTER TABLE shop_location ADD LocationLongitude INT( 11 );


////////////// 23/12/2016 XIPOLIAS \\\\\\\\\\\\\\\\
ALTER TABLE sys_node ADD NodeFullUrlAlias varchar(1024);

////////////// 06/01/2017 TSEROS \\\\\\\\\\\\\\\\
ALTER TABLE sys_shop DROP COLUMN ShopSubTitle;
ALTER TABLE sys_shop DROP COLUMN ShopText;
ALTER TABLE sys_shop ADD ShopDisabilityAccess INT( 11 );

////////////// 06/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_shoplng` (
 `ShopLngID` int(11) NOT NULL,
 `ShopLngShopID` int(11) NOT NULL,
 `ShopLngType` int(11) NOT NULL,
 `ShopLngTitle` varchar(1024) NOT NULL,
 `ShopLngSubtitle` varchar(1024) NOT NULL,
 `ShopLngText` text NOT NULL,
 PRIMARY KEY (`ShopLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (13, 1, 'sys_shoplng', 'Shop Translation')

////////////// 06/01/2017 TSEROS \\\\\\\\\\\\\\\\
ALTER TABLE shop_location DROP COLUMN LocationOpeningTimes;
ALTER TABLE shop_location DROP COLUMN LocationLocation;
ALTER TABLE shop_location ADD LocationLevel INT( 11 );
ALTER TABLE shop_location DROP COLUMN LocationLatitude;
ALTER TABLE shop_location DROP COLUMN LocationLongitude;
ALTER TABLE shop_location ADD LocationLatitude varchar(1024);
ALTER TABLE shop_location ADD LocationLongitude varchar(1024);
ALTER TABLE shop_location ADD LocationPriority INT( 11 );

////////////// 06/01/2017 TSEROS \\\\\\\\\\\\\\\\
CREATE TABLE `shop_locationlng` (
 `LocationLngID` int(11) NOT NULL,
 `LocationLngLocationID` int(11) NOT NULL,
 `LocationLngType` int(11) NOT NULL,
 `LocationLngLocation` varchar(1024) NOT NULL,
 `LocationLngOpeningTimes` varchar(1024) NOT NULL,
 PRIMARY KEY (`LocationLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (14, 1, 'shop_locationlng', 'Shop Location Translation')

////////////// 06/01/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_node ADD NodeMetaDescription varchar(1024);
ALTER TABLE sys_node ADD NodeMetaKeywords varchar(1024);

////////////// 06/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_media` (
 `MediaID` int(11) NOT NULL,
 `MediaTitle` varchar(1024) NOT NULL,
 `MediaFilename` varchar(1024) NOT NULL,
 `MediaType` int(11) NOT NULL,
  PRIMARY KEY (`MediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (15, 1, 'sys_media', 'Media')

ALTER TABLE sys_media ADD MediaSiteID INT( 11 );

CREATE TABLE `sys_medialng` (
 `MediaLngID` int(11) NOT NULL,
 `MediaLngTitle` varchar(1024) NOT NULL,
 `MediaLngType` int(11) NOT NULL,
 `MediaLngMediaID` int(11) NOT NULL,
 PRIMARY KEY (`MediaLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (16, 1, 'sys_medialng', 'Media Translation')

////////////// 06/01/2016 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_node_media` (
 `NodeMediaID` int(11) NOT NULL,
 `NodeMediaNodeID` int(11) NOT NULL,
 `NodeMediaMediaID` int(11) NOT NULL,
 PRIMARY KEY (`NodeMediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (17, 1, 'sys_node_media', 'Node Media')

ALTER TABLE sys_node_media ADD NodeMediaPriority INT( 11 );

////////////// 10/01/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_node ADD NodeMetaTitle varchar(1024);

////////////// 11/01/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_node DROP COLUMN NodeMetaDescription;
ALTER TABLE sys_node DROP COLUMN NodeMetaKeywords;
ALTER TABLE sys_node DROP COLUMN NodeMetaTitle;
ALTER TABLE sys_nodelng ADD NodeLngMetaTitle varchar(1024);
ALTER TABLE sys_nodelng ADD NodeLngMetaKeywords varchar(1024);
ALTER TABLE sys_nodelng ADD NodeLngMetaDescription varchar(1024);

////////////// 11/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_alert` (
 `AlertID` int(11) NOT NULL,
 `AlertStatus` int(11) NOT NULL,
 `AlertType` int(11) NOT NULL,
 `AlertPriority` int(11) NOT NULL,
 `AlertTitle` varchar(1024) NOT NULL,
 `AlertStartDateTime` datetime NOT NULL,
 `AlertEndDateTime` datetime NOT NULL,
 PRIMARY KEY (`AlertID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (18, 1, 'sys_alert', 'Alerts')

ALTER TABLE sys_alert ADD AlertSiteID INT( 11 );
ALTER TABLE sys_alert DROP AlertEndDateTime ;
ALTER TABLE sys_alert DROP AlertStartDateTime;
ALTER TABLE sys_alert ADD AlertStartDate date;
ALTER TABLE sys_alert ADD AlertStartTime time;
ALTER TABLE sys_alert ADD AlertEndDate date;
ALTER TABLE sys_alert ADD AlertEndTime time;

////////////// 12/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_slider` (
 `SliderID` int(11) NOT NULL,
 `SliderSiteID` int(11) NOT NULL,
 `SliderTitle` varchar(1024) NOT NULL,
 `SliderStatus` int(11) NOT NULL,
 PRIMARY KEY (`SliderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (19, 1, 'sys_slider', 'Slider')

CREATE TABLE `sys_slider_images` (
 `SliderImagesID` int(11) NOT NULL,
 `SliderImagesSliderID` int(11) NOT NULL,
 `SliderImagesTitle` varchar(1024) NOT NULL,
 `SliderImagesFIlename` varchar(11) NOT NULL,
 `SliderImagesPriority` int(11) NOT NULL,
 `SliderImagesNodeID` int(11) NOT NULL,
 PRIMARY KEY (`SliderImagesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (20, 1, 'sys_slider_images', 'Slider Images')

ALTER TABLE sys_slider_images ADD SliderImagesStatus INT( 11 );

CREATE TABLE `sys_slider_imageslng` (
 `SliderImagesLngID` int(11) NOT NULL,
 `SliderImagesLngSliderImagesID` int(11) NOT NULL,
 `SliderImagesLngTitle` varchar(1024) NOT NULL,
 `SliderImagesLngSubtitle` varchar(1024) NOT NULL,
 `SliderImagesLngLinkTitle` varchar(1024) NOT NULL,
 PRIMARY KEY (`SliderImagesLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (21, 1, 'sys_slider_imageslng', 'Slider Images Translation')

ALTER TABLE sys_slider_imageslng ADD SliderImagesLngType INT( 11 );

CREATE TABLE `sys_node_slider` (
 `NodeSliderID` int(11) NOT NULL,
 `NodeSliderNodeID` int(11) NOT NULL,
 `NodeSliderSliderID` int(11) NOT NULL,
 PRIMARY KEY (`NodeSliderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (22, 1, 'sys_node_slider', 'Node Slider')

////////////// 13/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_alertlng` (
 `AlertLngID` int(11) NOT NULL,
 `AlertLngAlertID` int(11) NOT NULL,
 `AlertLngTitle` varchar(1024) NOT NULL,
 `AlertLngContent` text NOT NULL,
 `AlertLngType` int(11) NOT NULL,
 PRIMARY KEY (`AlertLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (23, 1, 'sys_alertlng', 'Alert Translation')

CREATE TABLE `sys_disaster` (
 `DisasterID` int(11) NOT NULL,
 `DisasterStatus` int(11) NOT NULL,
 `DisasterTitle` varchar(1024) NOT NULL,
 `DisasterSiteID` int(11) NOT NULL,
 PRIMARY KEY (`DisasterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (24, 1, 'sys_disaster', 'Disaster Mode')

CREATE TABLE `sys_disasterlng` (
 `DisasterLngID` int(11) NOT NULL,
 `DisasterLngDisasterID` int(11) NOT NULL,
 `DisasterLngTitle` varchar(1024) NOT NULL,
 `DisasterLngContent` text NOT NULL,
 `DisasterLngType` int(11) NOT NULL,
 PRIMARY KEY (`DisasterLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (25, 1, 'sys_disasterlng', 'Disaster Mode Translation')

////////////// 16/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_central_airlines` (
 `C_AirlinesID` int(11) NOT NULL,
 `C_AirlinesName` varchar(1024) NOT NULL,
 `C_AirlinesWebsite` varchar(1024) NOT NULL,
 `C_AirlinesEmail` varchar(1024) NOT NULL,
 PRIMARY KEY (`C_AirlinesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (26, 1, 'sys_central_airlines', 'Central Airlines')

ALTER TABLE sys_central_airlines ADD C_AirlinesFileName varchar(1024);

CREATE TABLE `sys_central_airlineslng` (
 `C_AirlinesLngID` int(11) NOT NULL,
 `C_AirlinesLngC_AirlinesID` int(11) NOT NULL,
 `C_AirlinesLngName` varchar(1024) NOT NULL,
 `C_AirlinesLngAddress` varchar(1024) NOT NULL,
 `C_AirlinesLngPhones` varchar(1024) NOT NULL,
 PRIMARY KEY (`C_AirlinesLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (27, 1, 'sys_central_airlineslng', 'Central Airlines Translation')

ALTER TABLE sys_central_airlineslng ADD C_AirlinesLngType int(11);

CREATE TABLE `sys_site_airline` (
 `SiteAirlineID` int(11) NOT NULL,
 `SiteAirlineAirlineID` int(11) NOT NULL,
 `SiteAirlineSiteID` int(11) NOT NULL,
 PRIMARY KEY (`SiteAirlineID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (28, 1, 'sys_site_airline', 'Site Airline')

CREATE TABLE `sys_site_airlinelng` (
 `SiteAirlineLngID` int(11) NOT NULL,
 `SiteAirlineLngSiteAirlineID` int(11) NOT NULL,
 `SiteAirlineLngCheckInCounters` varchar(2048) NOT NULL,
 `SiteAirlineLngAirportPhones` varchar(2048) NOT NULL,
 `SiteAirlineLngType` int(11) NOT NULL,
 PRIMARY KEY (`SiteAirlineLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (29, 1, 'sys_site_airlinelng', 'Site Airline Translation')

////////////// 17/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_related_links` (
 `RelatedLinkID` int(11) NOT NULL,
 `RelatedLinkTitle` varchar(1024) NOT NULL,
 `RelatedLinkFIlename` varchar(1024) NOT NULL,
 `RelatedLinkNodeID` int(11) NOT NULL,
 `RelatedLinkSiteID` int(11) NOT NULL,
 PRIMARY KEY (`RelatedLinkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (30, 1, 'sys_related_links', 'Related Links')

ALTER TABLE sys_related_links ADD RelatedLinkType int(11);

CREATE TABLE `sys_related_linkslng` (
 `RelatedLinkLngID` int(11) NOT NULL,
 `RelatedLinkLngTitle` varchar(1024) NOT NULL,
 `RelatedLinkLngType` int(11) NOT NULL,
 `RelatedLinkLngRelatedLinkID` int(11) NOT NULL,
 PRIMARY KEY (`RelatedLinkLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (31, 1, 'sys_related_linkslng', 'Related Links Translation')

CREATE TABLE `sys_node_related_link` (
 `NodeRelatedLinkID` int(11) NOT NULL,
 `NodeRelatedLinkNodeID` int(11) NOT NULL,
 `NodeRelatedLinkRelatedLinkID` int(11) NOT NULL,
 PRIMARY KEY (`NodeRelatedLinkID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (32, 1, 'sys_node_related_link', 'Node Related Links')

ALTER TABLE sys_node_related_link ADD NodeRelatedLinkPriority INT( 11 );

////////////// 18/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_central_media` (
 `CentralMediaID` int(11) NOT NULL,
 `CentralMediaTitle` varchar(1024) NOT NULL,
 `CentralMediaFilename` varchar(1024) NOT NULL,
 `CentralMediaType` int(11) NOT NULL,
 PRIMARY KEY (`CentralMediaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (33, 1, 'sys_central_media', 'Central Media')

CREATE TABLE `sys_central_medialng` (
 `CentralMediaLngID` int(11) NOT NULL,
 `CentralMediaLngTitle` varchar(1024) NOT NULL,
 `CentralMediaLngType` int(11) NOT NULL,
 `CentralMediaLngCentralMediaID` int(11) NOT NULL,
 PRIMARY KEY (`CentralMediaLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (34, 1, 'sys_central_medialng', 'Central Media Translation')

////////////// 20/01/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_shop ADD ShopType INT( 11 );

////////////// 23/01/2016 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_central_airlines ADD C_AirlinesCode varchar(1024);

////////////// 23/01/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_node ADD NodeIsFlight INT( 11 );
ALTER TABLE sys_node ADD NodeIsFlightDetailed INT( 11 );
ALTER TABLE sys_node ADD NodeIsAirline INT( 11 );
ALTER TABLE sys_node ADD NodeIsAirlineDetailed INT( 11 );

////////////// 25/01/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_node ADD NodeIsShop INT( 11 );
ALTER TABLE sys_node ADD NodeIsShopDetailed INT( 11 );
ALTER TABLE sys_node ADD NodeIsDarkSite INT( 11 );

////////////// 27/01/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_news_events` (
 `NewsEventsID` int(11) NOT NULL,
 `NewsEventsStatus` int(11) NOT NULL,
 `NewsEventsType` int(11) NOT NULL,
 `NewsEventsPriority` int(11) NOT NULL,
 `NewsEventsTitle` varchar(1024) NOT NULL,
 `NewsEventsSiteID` int(11) NOT NULL,
 `NewsEventsStartDate` date NOT NULL,
 `NewsEventsStartTime` time NOT NULL,
 `NewsEventsEndDate` date NOT NULL,
 `NewsEventsEndTime` time NOT NULL,
 `NewsEventsFilename` varchar(1024) NOT NULL,
 PRIMARY KEY (`NewsEventsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (35, 1, 'sys_news_events', 'News Events')

CREATE TABLE `sys_news_eventslng` (
 `NewsEventsLngID` int(11) NOT NULL,
 `NewsEventsLngNewsEventsID` int(11) NOT NULL,
 `NewsEventsLngType` int(11) NOT NULL,
 `NewsEventsLngTitle` varchar(1024) NOT NULL,
 `NewsEventsLngContent` text NOT NULL,
 PRIMARY KEY (`NewsEventsLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (36, 1, 'sys_news_eventslng', 'News Events Translation')

ALTER TABLE sys_medialng ADD MediaLngDescription varchar(1024);
ALTER TABLE sys_media ADD MediaFilesize varchar(1024);
ALTER TABLE sys_media ADD MediaExtension varchar(1024);

////////////// 30/01/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_central_airlines ADD C_AirlinesE_Check_In varchar(1024);
ALTER TABLE sys_central_airlines ADD C_AirlinesE_Booking varchar(1024);
ALTER TABLE sys_site_airlinelng ADD SiteAirlineLngReservations varchar(1024);
ALTER TABLE sys_site_airlinelng ADD SiteAirlineLngLostAndFound varchar(1024);

////////////// 31/01/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_node ADD NodeDataType INT( 11 );
ALTER TABLE sys_node ADD NodeViewType INT( 11 );

////////////// 01/02/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_site ADD SiteUrlTitle varchar(1024);

////////////// 02/02/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_central_airlineslng ADD C_AirlinesLngDictionaryDescription varchar(1024);
ALTER TABLE sys_shoplng ADD ShopLngDictionaryDescription varchar(1024);

////////////// 04/02/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_categories` (
 `CtgID` int(11) NOT NULL,
 `CtgTitle` varchar(1024) NOT NULL,
 `CtgSiteID` int(11) NOT NULL,
 PRIMARY KEY (`CtgID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (37, 1, 'sys_categories', 'Categories')

////////////// 05/02/2017 TSEROS \\\\\\\\\\\\\\\\
CREATE TABLE `sys_categories_records` (
 `CtgRecID` int(11) NOT NULL,
 `CtgRecCtgID` int(11) NOT NULL,
 `CtgRecPriority` int(11) NOT NULL,
 `CtgRecTitle` varchar(1024) NOT NULL,
 `CtgRecStartDate` date NOT NULL,
 `CtgRecStartTime` time NOT NULL,
 `CtgRecEndDate` date NOT NULL,
 `CtgRecEndTime` time NOT NULL,
 `CtgRecTimer` int(11) NOT NULL,
 `CtgRecFilename` varchar(1024) NOT NULL,
 PRIMARY KEY (`CtgRecID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (38, 1, 'sys_categories_records', 'Categories Records')

CREATE TABLE `sys_categories_recordslng` (
 `CtgRecLngID` int(11) NOT NULL,
 `CtgRecLngCtgRecID` int(11) NOT NULL,
 `CtgRecLngType` int(11) NOT NULL,
 `CtgRecLngTitle` varchar(1024) NOT NULL,
 `CtgRecLngSubtitle` varchar(1024) NOT NULL,
 `CtgRecLngText` text NOT NULL,
 `CtgRecLngDictionaryDescription` varchar(1024) DEFAULT NULL,
 PRIMARY KEY (`CtgRecLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (39, 1, 'sys_categories_recordslng', 'Categories Records Translation')

////////////// 06/02/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_node ADD NodeIsCategory INT( 11 );
ALTER TABLE sys_node ADD NodeIsCategoryDetailed INT( 11 );
ALTER TABLE sys_node ADD NodeIsRestaurant INT( 11 );
ALTER TABLE sys_node ADD NodeIsDutyFree INT( 11 );

////////////// 10/02/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_categories_records ADD CtgRecStatus INT( 11 );

CREATE TABLE `sys_footer` (
 `FtrID` int(11) NOT NULL,
 `FtrSiteID` int(11) NOT NULL,
 `FtrTitle` varchar(1024) NOT NULL,
 `FtrType` int(11) NOT NULL,
 `FtrPriority` int(11) NOT NULL,
 PRIMARY KEY (`FtrID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (40, 1, 'sys_footer', 'Footer')

ALTER TABLE sys_footer ADD FtrStatus INT( 11 );

CREATE TABLE `sys_footerlng` (
 `FtrLngID` int(11) NOT NULL,
 `FtrLngTitle` varchar(1024) NOT NULL,
 `FtrLngType` int(11) NOT NULL,
 `FtrLngFtrID` int(11) NOT NULL,
 PRIMARY KEY (`FtrLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (41, 1, 'sys_footerlng', 'Footer Translation')

CREATE TABLE `sys_footer_data` (
 `FtrDataID` int(11) NOT NULL,
 `FtrDataFtrID` int(11) NOT NULL,
 `FtrDataTitle` varchar(1024) NOT NULL,
 `FtrDataNodeID` int(11) NOT NULL,
 `FtrDataExternalLink` varchar(1024) NOT NULL,
 `FtrDataIcon` varchar(1024) NOT NULL,
 PRIMARY KEY (`FtrDataID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (42, 1, 'sys_footer_data', 'Footer Data')

ALTER TABLE sys_footer_data ADD FtrDataStatus INT( 11 );
ALTER TABLE sys_footer_data ADD FtrDataPriority INT( 11 );

CREATE TABLE `sys_footer_datalng` (
 `FtrDataLngID` int(11) NOT NULL,
 `FtrDataLngTitle` varchar(1024) NOT NULL,
 `FtrDataLngType` int(11) NOT NULL,
 `FtrDataLngFtrDataID` int(11) NOT NULL,
 PRIMARY KEY (`FtrDataLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (43, 1, 'sys_footer_datalng', 'Footer Data Translation')

////////////// 13/02/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_image_gallery` (
 `ImgID` int(11) NOT NULL,
 `ImgSiteID` int(11) NOT NULL,
 `ImgTitle` varchar(1024) NOT NULL,
 `ImgPriority` int(11) NOT NULL,
 `ImgStatus` int(11) NOT NULL,
 `ImgFilename` varchar(1024) NOT NULL,
 PRIMARY KEY (`ImgID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (44, 1, 'sys_image_gallery', 'Image Gallery')

CREATE TABLE `sys_image_gallerylng` (
 `ImgLngID` int(11) NOT NULL,
 `ImgLngImgID` int(11) NOT NULL,
 `ImgLngTitle` varchar(1024) NOT NULL,
 `ImgLngType` int(11) NOT NULL,
 PRIMARY KEY (`ImgLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (45, 1, 'sys_image_gallerylng', 'Image Gallery Translation')

CREATE TABLE `sys_image_gallery_data` (
 `ImgDataID` int(11) NOT NULL,
 `ImgDataImgID` int(11) NOT NULL,
 `ImgDataTitle` varchar(1024) NOT NULL,
 `ImgDataPriority` int(11) NOT NULL,
 `ImgDataStatus` int(11) NOT NULL,
 `ImgDataFilename` varchar(1024) NOT NULL,
 PRIMARY KEY (`ImgDataID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (46, 1, 'sys_image_gallery_data', 'Image Gallery Data')

CREATE TABLE `sys_image_gallery_datalng` (
 `ImgDataLngID` int(11) NOT NULL,
 `ImgDataLngImgDataID` int(11) NOT NULL,
 `ImgDataLngTitle` varchar(1024) NOT NULL,
 `ImgDataLngType` int(11) NOT NULL,
 PRIMARY KEY (`ImgDataLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (47, 1, 'sys_image_gallery_datalng', 'Image Gallery Data Translation')

////////////// 14/02/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_video_gallery` (
 `VidID` int(11) NOT NULL,
 `VidSiteID` int(11) NOT NULL,
 `VidTitle` varchar(1024) NOT NULL,
 `VidPriority` int(11) NOT NULL,
 `VidStatus` int(11) NOT NULL,
 `VidFilename` varchar(1024) NOT NULL,
 PRIMARY KEY (`VidID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (48, 1, 'sys_video_gallery', 'Video Gallery')

CREATE TABLE `sys_video_gallerylng` (
 `VidLngID` int(11) NOT NULL,
 `VidLngVidID` int(11) NOT NULL,
 `VidLngTitle` varchar(1024) NOT NULL,
 `VidLngType` int(11) NOT NULL,
 PRIMARY KEY (`VidLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (49, 1, 'sys_video_gallerylng', 'Video Gallery Translation')

CREATE TABLE `sys_video_gallery_data` (
 `VidDataID` int(11) NOT NULL,
 `VidDataVidID` int(11) NOT NULL,
 `VidDataTitle` varchar(1024) NOT NULL,
 `VidDataPriority` int(11) NOT NULL,
 `VidDataStatus` int(11) NOT NULL,
 `VidDataFilename` varchar(1024) NOT NULL,
 PRIMARY KEY (`VidDataID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (50, 1, 'sys_video_gallery_data', 'Video Gallery Data')

CREATE TABLE `sys_video_gallery_datalng` (
 `VidDataLngID` int(11) NOT NULL,
 `VidDataLngVidDataID` int(11) NOT NULL,
 `VidDataLngTitle` varchar(1024) NOT NULL,
 `VidDataLngType` int(11) NOT NULL,
 PRIMARY KEY (`VidDataLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (51, 1, 'sys_video_gallery_datalng', 'Video Gallery Data Translation')

CREATE TABLE `sys_publications_gallery` (
 `PubID` int(11) NOT NULL,
 `PubSiteID` int(11) NOT NULL,
 `PubTitle` varchar(1024) NOT NULL,
 `PubPriority` int(11) NOT NULL,
 `PubStatus` int(11) NOT NULL,
 `PubFilename` varchar(1024) NOT NULL,
 PRIMARY KEY (`PubID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (52, 1, 'sys_publications_gallery', 'Publications Gallery')

////////////// 15/02/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_publications_gallerylng` (
 `PubLngID` int(11) NOT NULL,
 `PubLngPubID` int(11) NOT NULL,
 `PubLngTitle` varchar(1024) NOT NULL,
 `PubLngType` int(11) NOT NULL,
 PRIMARY KEY (`PubLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (53, 1, 'sys_publications_gallerylng', 'Publications Gallery Translation')

CREATE TABLE `sys_publications_gallery_data` (
 `PubDataID` int(11) NOT NULL,
 `PubDataPubID` int(11) NOT NULL,
 `PubDataTitle` varchar(1024) NOT NULL,
 `PubDataPriority` int(11) NOT NULL,
 `PubDataStatus` int(11) NOT NULL,
 `PubDataFilename` varchar(1024) NOT NULL,
 `PubDataFilesize` varchar(1024) NOT NULL,
 `PubDataExtension` varchar(1024) NOT NULL,
 PRIMARY KEY (`PubDataID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (54, 1, 'sys_publications_gallery_data', 'Publications Gallery Data')

CREATE TABLE `sys_publications_gallery_datalng` (
 `PubDataLngID` int(11) NOT NULL,
 `PubDataLngPubDataID` int(11) NOT NULL,
 `PubDataLngTitle` varchar(1024) NOT NULL,
 `PubDataLngDescription` varchar(1024) NOT NULL,
 `PubDataLngType` int(11) NOT NULL,
 PRIMARY KEY (`PubDataLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (55, 1, 'sys_publications_gallery_datalng', 'Publications Gallery Data Translation')

ALTER TABLE sys_image_gallery_datalng ADD ImgDataLngSubtitle varchar(1024);
ALTER TABLE sys_image_gallery_datalng ADD ImgDataLngDescription varchar(1024);
ALTER TABLE sys_image_gallerylng ADD ImgLngSubtitle varchar(1024);
ALTER TABLE sys_image_gallerylng ADD ImgLngDescription varchar(1024);
ALTER TABLE sys_video_gallerylng ADD VidLngSubtitle varchar(1024);
ALTER TABLE sys_video_gallerylng ADD VidLngDescription varchar(1024);
ALTER TABLE sys_video_gallery_datalng ADD VidDataLngSubtitle varchar(1024);
ALTER TABLE sys_video_gallery_datalng ADD VidDataLngDescription varchar(1024);
ALTER TABLE sys_publications_gallerylng ADD PubLngSubtitle varchar(1024);
ALTER TABLE sys_publications_gallerylng ADD PubLngDescription varchar(1024);
ALTER TABLE sys_publications_gallery_datalng ADD PubDataLngSubtitle varchar(1024);

////////////// 15/02/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_node ADD NodeIsImageGallery INT( 11 );
ALTER TABLE sys_node ADD NodeIsImageGalleryDetailed INT( 11 );
ALTER TABLE sys_node ADD NodeIsVideoGallery INT( 11 );
ALTER TABLE sys_node ADD NodeIsVideoGalleryDetailed INT( 11 );
ALTER TABLE sys_node ADD NodeIsPublications INT( 11 );
ALTER TABLE sys_node ADD NodeIsPublicationsDetailed INT( 11 );


////////////// 20/02/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_dictionary` (
 `DicID` int(11) NOT NULL,
 `DicName` varchar(1024) NOT NULL,
 PRIMARY KEY (`DicID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (56, 1, 'sys_dictionary', 'Dictionary')

CREATE TABLE `sys_dictionarylng` (
 `DicLngID` int(11) NOT NULL,
 `DicLngDicID` int(11) NOT NULL,
 `DicLngName` varchar(1024) NOT NULL,
 `DicLngType` int(11) NOT NULL,
 PRIMARY KEY (`DicLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (57, 1, 'sys_dictionarylng', 'Dictionary Translation')

////////////// 21/02/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_shop ADD ShopImgFilenameLogo varchar(1024);
ALTER TABLE sys_shop ADD ShopPhone varchar(1024);

////////////// 21/02/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_central_airlines ADD C_AirlinesFileNameLogo varchar(1024);



////////////// 02/03/2017 ZAGALIKI \\\\\\\\\\\\\\\\

ALTER TABLE `sys_site_airlinelng` ADD `SiteAirlineLngSiteAirlineLastUpdate` DATETIME NOT NULL AFTER `SiteAirlineLngLostAndFound`;

////////////// 02/03/2017 TSEROS \\\\\\\\\\\\\\\\

ALTER TABLE sys_site ADD SiteAirportCode varchar(1024);

CREATE TABLE `sys_seasonal_flight_plan_version` (
 `SfpvID` int(11) NOT NULL,
 `SfpvDateAdded` date NOT NULL,
 `SfpvTitle` varchar(1024) NOT NULL,
 `SfpvStatus` int(11) NOT NULL,
 `SfpvSiteID` int(11) NOT NULL,
 PRIMARY KEY (`SfpvID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (58, 1, 'sys_seasonal_flight_plan_version', 'Seasonal Flight Plan Version')

CREATE TABLE `sys_seasonal_flight_plan` (
 `SfpID` int(11) NOT NULL,
 `SfpArrivalDeparture` varchar(1024) NOT NULL,
 `SfpFromDate` varchar(1024) NOT NULL,
 `SfpToDate` varchar(1024) NOT NULL,
 `SfpDays` varchar(1024) NOT NULL,
 `SfpFlightNumber` varchar(1024) NOT NULL,
 `SfpTime` varchar(1024) NOT NULL,
 `SfpDestination` varchar(1024) NOT NULL,
 `SfpSfpvID` int(11) NOT NULL,
 PRIMARY KEY (`SfpID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (59, 1, 'sys_seasonal_flight_plan', 'Seasonal Flight Plan')

ALTER TABLE sys_seasonal_flight_plan ADD SfpDestinationID int(11) AFTER SfpDestination;


////////////// 06/03/2017 ZAGALIKI \\\\\\\\\\\\\\\\

ALTER TABLE `shop_locationlng` ADD `LocationLngLastUpdate` DATETIME NOT NULL DEFAULT '2017-03-06 11:30:01' AFTER `LocationLngOpeningTimes`;

ALTER TABLE `sys_categories_recordslng` ADD `CtgRecLngLastUpdate` DATETIME NOT NULL DEFAULT '2017-03-06 11:30:01' AFTER `CtgRecLngDictionaryDescription`;


////////////// 07/03/2017 ZAGALIKI \\\\\\\\\\\\\\\\


ALTER TABLE `sys_site_airlinelng` DROP `SiteAirlineLngSiteAirlineLastUpdate`;

ALTER TABLE `sys_site_airlinelng` ADD `SiteAirlineLngSiteAirlineLastUpdate` DATETIME NOT NULL DEFAULT '2017-03-07 11:30:01' AFTER `SiteAirlineLngLostAndFound`;


ALTER TABLE `sys_node` ADD `NodeIsSearchPage` INT NOT NULL DEFAULT '0' AFTER `NodeIsPublicationsDetailed`;

 
 
 ////////////// 08/03/2017 ZAGALIKI \\\\\\\\\\\\\\\\
 
 
 ALTER TABLE `sys_airlines` ADD PRIMARY KEY(`AirlineID`);
 
 ALTER TABLE `sys_airlines` ADD UNIQUE(`AirlineID`);
 
 
 ALTER TABLE `sys_seasonal_flight_plan` ADD `SfpAirlineID` INT NOT NULL DEFAULT '0' AFTER `SfpSfpvID`;
 
 
 
 
 ////////////// 09/03/2017 ZAGALIKI \\\\\\\\\\\\\\\\

ALTER TABLE `sys_node` ADD `NodeIsDomesticDestinations` INT NOT NULL DEFAULT '0' AFTER `NodeIsSearchPage`, ADD `NodeIsInternationalDestinations` INT NOT NULL DEFAULT '0' AFTER `NodeIsDomesticDestinations`;
 
 
 
 ALTER TABLE `sys_slider_images` ADD `SliderImagesExternalLink` VARCHAR(1024) NOT NULL DEFAULT '' AFTER `SliderImagesNodeID`;
 
 
 
 
 ALTER TABLE `sys_central_airlines` ADD `C_AirlinesEscapeDefault` VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `C_AirlinesFileNameLogo`;
 
 ALTER TABLE `sys_shop` ADD `ShopEscapeDefault` VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `ShopPhone`;
 
 ALTER TABLE `sys_categories_records` ADD `CtgRecEscapeDefault` VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `CtgRecFilename`;
 
 
 
////////////// 13/03/2017 ZAGALIKI \\\\\\\\\\\\\\\\


ALTER TABLE `sys_shoplng` ADD `ShopLngImgFilenameLogo` VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `ShopLngDictionaryDescription`;


////////////// 23/03/2017 TSEROS \\\\\\\\\\\\\\\\

CREATE TABLE `sys_flight_tracker` (
 `FltID` int(11) NOT NULL,
 `FltSiteID` int(11) NOT NULL,
 `FltType` varchar(10) NOT NULL,
 `FltFlightNumber` varchar(10) NOT NULL,
 `FltFlightNumber2` varchar(10) NOT NULL,
 `FltFlightNumber3` varchar(10) NOT NULL,
 `FltFlightNumber4` varchar(10) NOT NULL,
 `FltFlightNumber5` varchar(10) NOT NULL,
 `FltAirlineCode` varchar(10) NOT NULL,
 `FltAirlineCarrier` varchar(10) NOT NULL,
 `FltAirlineIATA` varchar(10) NOT NULL,
 `FltAirlineICAO` varchar(10) NOT NULL,
 `FltScheduledTime` datetime NOT NULL,
 `FltEstimatedTime` datetime NOT NULL,
 `FltActualTime` datetime NOT NULL,
 `FltAirportName` varchar(1024) NOT NULL,
 `FltAirportShortName` varchar(1024) NOT NULL,
 `FltAirportIATA` varchar(10) NOT NULL,
 `FltAirportICAO` varchar(10) NOT NULL,
 `FltVia1Name` varchar(1024) NOT NULL,
 `FltVia1ShortName` varchar(1024) NOT NULL,
 `FltVia1IATA` varchar(10) NOT NULL,
 `FltVia1ICAO` varchar(10) NOT NULL,
 `FltVia2Name` varchar(1024) NOT NULL,
 `FltVia2ShortName` varchar(1024) NOT NULL,
 `FltVia2IATA` varchar(10) NOT NULL,
 `FltVia2ICAO` varchar(10) NOT NULL,
 `FltVia3Name` varchar(1024) NOT NULL,
 `FltVia3ShortName` varchar(1024) NOT NULL,
 `FltVia3IATA` varchar(10) NOT NULL,
 `FltVia3ICAO` varchar(10) NOT NULL,
 `FltAircraftType` varchar(10) NOT NULL,
 `FltKindOfFlight` varchar(10) NOT NULL,
 `FltTerminal` varchar(255) NOT NULL,
 `FltGate` varchar(255) NOT NULL,
 `FltCheckIn` varchar(1024) NOT NULL,
 `FltRemarkFids` varchar(1024) NOT NULL,
 PRIMARY KEY (`FltID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (60, 1, 'sys_flight_tracker', 'Flight Tracker')

CREATE TABLE `sys_flight_trackerlng` (
 `FltLngID` int(11) NOT NULL,
 `FltLngFltID` int(11) NOT NULL,
 `FltLngType` int(11) NOT NULL,
 `FltLngAirport` varchar(1024) NOT NULL,
 `FltLngVia1` varchar(1024) NOT NULL,
 `FltLngVia2` varchar(1024) NOT NULL,
 `FltLngVia3` varchar(1024) NOT NULL,
 `FltLngRemarks` varchar(1024) NOT NULL,
 PRIMARY KEY (`FltLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
 
INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (61, 1, 'sys_flight_trackerlng', 'Flight Tracker Translations')
 
 ALTER TABLE sys_flight_tracker ADD FltVersion INT( 11 );
 
 ////////////// 24/03/2017 TSEROS \\\\\\\\\\\\\\\\
 
 ALTER TABLE sys_central_airlines ADD C_AirlinesIATA varchar(1024);
 ALTER TABLE sys_central_airlines ADD C_AirlinesICAO varchar(1024);
 ALTER TABLE sys_central_airlines ADD C_AirlinesFlightsLogo varchar(1024);
 
 ////////////// 27/03/2017 TSEROS \\\\\\\\\\\\\\\\
 
 ALTER TABLE sys_flight_tracker ADD FltVersionTimestamp datetime;
 ALTER TABLE sys_flight_tracker ADD FltVersionTimestampProcessed datetime;
 
 
 CREATE TABLE `sys_flight_tracker_log` (
 `FltLogID` int(11) NOT NULL,
 `FltLogResult` varchar(10) NOT NULL,
 `FltLogDateTimeProduced` datetime NOT NULL,
 `FltLogDateTimeProcessed` datetime NOT NULL,
 `FltLogSiteID` int(11) NOT NULL,
 `FltLogVersion` int(11) NOT NULL,
 PRIMARY KEY (`FltLogID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (62, 1, 'sys_flight_tracker_log', 'Flight Tracker Log')

 ////////////// 01/04/2017 TSEROS \\\\\\\\\\\\\\\\
 
  ALTER TABLE sys_node ADD NodeIsWeatherPage INT( 11 );
	
	
////////////// 01/04/2017 XIPOLIAS \\\\\\\\\\\\\\\\
 
  ALTER TABLE sys_site ADD SiteXmlFilename varchar(1024);
	
ALTER TABLE sys_node ADD Node_SourceCopyID int(11);
 

 
////////////// 03/04/2017 ZAGALIKI \\\\\\\\\\\\\\\\
 
ALTER TABLE `sys_node` ADD `NodeIsDestinations` INT NOT NULL AFTER `NodeIsInternationalDestinations`;




////////////// 04/04/2017 ZAGALIKI \\\\\\\\\\\\\\\\



ALTER TABLE `sys_node` ADD `NodeCopyNodeID` INT NOT NULL AFTER `NodeIsWeatherPage`;


ALTER TABLE `sys_categories` ADD `CtgCopyCtgID` INT NOT NULL AFTER `CtgSiteID`;


ALTER TABLE `sys_categories_records` ADD `CtgRecCopyCtgRecID` INT NOT NULL AFTER `CtgRecStatus`;



ALTER TABLE `sys_media` ADD `MediaCopyMediaID` INT NOT NULL AFTER `MediaExtension`;



ALTER TABLE `sys_footer_datalng` ADD `FtrDataLngExternalLink` VARCHAR(1024) NOT NULL DEFAULT '' AFTER `FtrDataLngFtrDataID`;



////////////// 05/04/2017 ZAGALIKI \\\\\\\\\\\\\\\\


ALTER TABLE `sys_slider_imageslng` ADD `SliderImagesLngExternalLink` VARCHAR(1024) NOT NULL DEFAULT '' AFTER `SliderImagesLngType`;




ALTER TABLE `sys_central_airlines` ADD `C_AirlinesOnlineContact` VARCHAR(1024) NOT NULL AFTER `C_AirlinesFlightsLogo`;



ALTER TABLE `sys_site_airlinelng` ADD `SiteAirlineLngHandler` VARCHAR(1024) NOT NULL AFTER `SiteAirlineLngSiteAirlineLastUpdate`, ADD `SiteAirlineLngHandlerAirportPhone` VARCHAR(1024) NOT NULL AFTER `SiteAirlineLngHandler`, ADD `SiteAirlineLngEmail` VARCHAR(1024) NOT NULL AFTER `SiteAirlineLngHandlerAirportPhone`;




-- --------------------------------------------------------

-- 21/04/2017 [ZAGALIKI] ---------------------------------------

-- --------------------------------------------------------



DROP TABLE  IF EXISTS `sys_categories_records_slider`;

CREATE TABLE IF NOT EXISTS `sys_categories_records_slider` (
  `CtgRecSliderID` int(11) NOT NULL,
  `CtgRecSliderCtgRecID` int(11) NOT NULL,
  `CtgRecSliderSliderID` int(11) NOT NULL,
  PRIMARY KEY (`CtgRecSliderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `fraport14`.`sys_kxn` (`KxnID`, `KxnInc`, `KxnTableName`, `KxnTableDescr`) VALUES ('63', '1', 'sys_categories_records_slider', 'Categories Records Slider');


-- --------------------------------------------------------

-- 26/04/2017 [ZAGALIKI] ---------------------------------------

-- --------------------------------------------------------



ALTER TABLE `sys_seasonal_flight_plan` DROP COLUMN `SfpFromDate`;

ALTER TABLE `sys_seasonal_flight_plan` DROP COLUMN `SfpToDate`;

ALTER TABLE `sys_seasonal_flight_plan` DROP COLUMN `SfpTime`;


ALTER TABLE `sys_seasonal_flight_plan` ADD `SfpFromDate` date AFTER `SfpArrivalDeparture`;

ALTER TABLE `sys_seasonal_flight_plan` ADD `SfpToDate` date AFTER `SfpFromDate`;

ALTER TABLE `sys_seasonal_flight_plan` ADD `SfpTime` datetime AFTER `SfpToDate`;



-- --------------------------------------------------------

-- 05/05/2017 [ZAGALIKI] ---------------------------------------

-- --------------------------------------------------------

UPDATE sys_slider_imageslng SET SliderImagesLngExternalLink = REPLACE(SliderImagesLngExternalLink, '?', '/');

UPDATE sys_slider_imageslng SET SliderImagesLngExternalLink = REPLACE(SliderImagesLngExternalLink, '&', '/');

UPDATE sys_slider_imageslng SET SliderImagesLngExternalLink = REPLACE(SliderImagesLngExternalLink, '=', '-');

UPDATE sys_slider_images SET SliderImagesExternalLink = REPLACE(SliderImagesExternalLink, '?', '/');

UPDATE sys_slider_images SET SliderImagesExternalLink = REPLACE(SliderImagesExternalLink, '&', '/');

UPDATE sys_slider_images SET SliderImagesExternalLink = REPLACE(SliderImagesExternalLink, '=', '-');







-- --------------------------------------------------------

-- 20/12/2017 [ZAGALIKI] ---------------------------------------

-- --------------------------------------------------------

ALTER TABLE `fraport14`.`sys_slider_imageslng`
ADD COLUMN `SliderImagesLngExternalLinkTarget` INT NOT NULL DEFAULT 0 AFTER `SliderImagesLngExternalLink`;




-- --------------------------------------------------------

-- 11/04/2018 [ZAGALLO] -----------------------------------

-- --------------------------------------------------------


CREATE TABLE `fraport14`.`cf_subjects` (
    `SubID` INT NOT NULL,
    `SubSiteID` INT NULL,
    `SubTitle` VARCHAR(1024) NULL,
    `SubPriority` INT NULL,
    `SubStatus` INT NULL,
    PRIMARY KEY (`SubID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (64, 1, 'cf_subjects', 'Contact Form - Subjects')


CREATE TABLE `fraport14`.`cf_subjects_lang` (
    `SubLngID` INT NOT NULL,
    `SubLngSubID` INT NULL,
    `SubLngType` INT NULL,
    `SubLngTitle` VARCHAR(1024) NULL,
    PRIMARY KEY (`SubLngID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (65, 1, 'cf_subjects_lang', 'Contact Form - Subjects Translations')


CREATE TABLE `fraport14`.`cf_sub_subject` (
    `SubSubID` INT NOT NULL,
    `SubSubSubID` INT NULL,
    `SubSubTitle` VARCHAR(1024) NULL,
    `SubSubPriority` INT NULL,
    `SubSubStatus` INT NULL,
    PRIMARY KEY (`SubSubID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (66, 1, 'cf_sub_subject', 'Contact Form - Sub Subjects')


CREATE TABLE `fraport14`.`cf_sub_subject_lang` (
    `SubSubLngID` INT NOT NULL,
    `SubSubLngSubSubID` INT NULL,
    `SubSubLngTitle` VARCHAR(1024) NULL,
    `SubSubLngType` INT NULL,
    PRIMARY KEY (`SubSubLngID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (67, 1, 'cf_sub_subject_lang', 'Contact Form - Sub Subjects Translations')


CREATE TABLE `fraport14`.`cf_answers_lang` (
    `AnsLngID` INT NOT NULL,
    `AnsLngSubSubID` INT NULL,
    `AnsLngType` INT NULL,
    `AnsLngTitle` VARCHAR(1024) NULL,
    `AnsLngDescr` TEXT NULL,
    PRIMARY KEY (`AnsLngID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (68, 1, 'cf_answers_lang', 'Contact Form - Subjects Answers Translations')




-- --------------------------------------------------------

-- 16/04/2018 [ZAGALLO] -----------------------------------

-- --------------------------------------------------------

ALTER TABLE `fraport14`.`sys_dictionary`
CHANGE COLUMN `DicName` `DicName` VARCHAR(1024) NULL;


ALTER TABLE `fraport14`.`sys_dictionarylng`
CHANGE COLUMN `DicLngDicID` `DicLngDicID` INT(11) NULL ,
CHANGE COLUMN `DicLngName` `DicLngName` VARCHAR(1024) NULL ,
CHANGE COLUMN `DicLngType` `DicLngType` INT(11) NULL ;


-- --------------------------------------------------------

-- 18/04/2018 [ZAGALLO] -----------------------------------

-- --------------------------------------------------------

ALTER TABLE `cf_answers_lang` ADD COLUMN `AnsLngShowCnt` INT NULL AFTER `AnsLngDescr`;

ALTER TABLE `cf_subjects_lang` ADD COLUMN `SubLngShowOther` INT NULL AFTER `SubLngTitle`;



-- --------------------------------------------------------

-- 02/05/2018 [ZAGALLO] -----------------------------------

-- --------------------------------------------------------

ALTER TABLE `cf_subjects` ADD COLUMN `SubParentID` INT NULL AFTER `SubStatus`;


ALTER TABLE `cf_sub_subject` ADD COLUMN `SubSubParentID` INT NULL AFTER `SubSubStatus`;

-- --------------------------------------------------------

-- 08/12/2020 [TSEROS] -----------------------------------

-- --------------------------------------------------------

CREATE TABLE `sys_footer_timetable` (
`FooterTimetableID` int(11) NOT NULL,
`FooterTimetableStatus` int(11) NOT NULL,
`FooterTimetableTitle` varchar(1024) NOT NULL,
`FooterTimetableSiteID` int(11) NOT NULL,
PRIMARY KEY (`FooterTimetableID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (69, 1, 'sys_footer_timetable', 'Footer Timetable');

CREATE TABLE `sys_footer_timetablelng` (
`FooterTimetableLngID` int(11) NOT NULL,
`FooterTimetableLngIDFooterTimetableID` int(11) NOT NULL,
`FooterTimetableLngTitle` varchar(1024) NOT NULL,
`FooterTimetableLngContent` text NOT NULL,
`FooterTimetableLngType` int(11) NOT NULL,
PRIMARY KEY (`FooterTimetableLngID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sys_kxn ( KxnID, KxnInc, KxnTableName, KxnTableDescr) VALUES (70, 1, 'sys_footer_timetablelng', 'Footer Timetable Translation');

