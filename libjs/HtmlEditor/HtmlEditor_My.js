

Ext.define('Ext.form.field.HtmlEditor_My', {
    extend:'Ext.form.field.HtmlEditor',   
    alias: 'widget.htmleditor_my',
   

    fontFamilies : [
        'Arial',
        'Courier New',
        'Tahoma',
        'Times New Roman',
        'Verdana'
    ],
    defaultFont: 'tahoma',
   

    // private
    initComponent : function(){
        var me = this;

        me.callParent(arguments);

    },

    
});