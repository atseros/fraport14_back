<?php

require_once 'libjs/HtmlEditor/thumbnailer/ThumbLib.inc.php';

/*********************************************************************************************
* CLASS HandleHtmlEditorImageUpload
*
* DESCRIPTION: 
*	Class that handles requests from HandleHtmlEditorImageUpload page 
*
*********************************************************************************************/
class HandleHtmlEditorImageUpload
{
	private $appFrw;
	private $imagesPath;
	private $imagesThumbsPath;
	private $imagesTumbsUrl;
	private $imagesUrl;
	
	// change these parameters to fit your server config
	private $allowedFormats;
	private $maxSize;
	private $createThumbnails;

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::CONSTRUCTOR
	****************************************************************************************/
	function HandleHtmlEditorImageUpload($appFrw, $params)
	{
		$this->appFrw = $appFrw;
		
		$this->imagesPath        = $params['imagesPath'];
		$this->imagesThumbsPath  = $params['imagesThumbsPath'];
		$this->imagesTumbsUrl    = $params['imagesTumbsUrl'];
		$this->imagesUrl         = $params['imagesUrl'];
		$this->allowedFormats    = $params['allowedFormats'];
		$this->maxSize           = $params['maxSize'];
		$this->createThumbnails  = $params['createThumbnails'];
	}

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::handle
	****************************************************************************************/
	function handle()
	{
		if(isset($_REQUEST['action']))
		{
			switch($_REQUEST['action'])
			{
				case 'upload':		
					print_r( json_encode( $this->uploadHtmlEditorImage(	$this->allowedFormats,
																		$this->maxSize,
																		$this->imagesPath,
																		$this->imagesUrl,
																		$this->createThumbnails,
																		$this->imagesTumbsUrl,
																		$this->imagesThumbsPath
																		) 
										) 
					);
				break;
		
				case 'crop':
		
					$imageSrc      = isset($_REQUEST["image"])?($_REQUEST["image"]):'';
					$width         = isset($_REQUEST["width"])?intval($_REQUEST["width"]):0;
					$height        = isset($_REQUEST["height"])?intval($_REQUEST["height"]):0;
					$offsetLeft    = isset($_REQUEST["offsetLeft"])?intval($_REQUEST["offsetLeft"]):0;
					$offsetTop     = isset($_REQUEST["offsetTop"])?intval($_REQUEST["offsetTop"]):0;
					$zoom          = isset($_REQUEST["zoom"])?intval($_REQUEST["zoom"]):1;
					print_r( json_encode( $this->cropImage(	$this->imagesPath,
															$this->imagesThumbsPath,
															$this->imagesUrl,
															$this->imagesTumbsUrl,
															$imageSrc,
															$width,
															$height,
															$offsetLeft,
															$offsetTop, 
															$zoom, 
															$this->allowedFormats
														) 
										) 
					);
				break;
		
				case 'rotate':		
					$imageSrc      = isset($_REQUEST["image"])?($_REQUEST["image"]):'';
					print_r( json_encode( $this->rotateImage(	$this->imagesPath,
																$this->imagesThumbsPath,
																$this->imagesUrl,
																$imageSrc
															) 
										) 
					);
				break;
				
				case 'resize':
					$width         = isset($_REQUEST["width"])?intval($_REQUEST["width"]):0;
					$height        = isset($_REQUEST["height"])?intval($_REQUEST["height"]):0;		
					$imageSrc      = isset($_REQUEST["image"])?($_REQUEST["image"]):'';
					print_r( json_encode( $this->resizeImage(	$this->imagesPath,
																$this->imagesThumbsPath,
																$this->imagesUrl,
																$imageSrc,
																$width,
																$height
															) 
										) 
					);
				break;
				
				case 'imagesList':
					$limit         = isset($_REQUEST["limit"])?intval($_REQUEST["limit"]):10;
					$start         = isset($_REQUEST["start"])?intval($_REQUEST["start"]):0;
					$query         = isset($_REQUEST["query"])?$_REQUEST["query"]:0;
					print_r(json_encode( $this->getImages(	$this->imagesPath, 
															$this->imagesUrl,
															$this->imagesTumbsUrl,
															$this->imagesThumbsPath, 
															$this->allowedFormats, 
															$start, 
															$limit, 
															$query
														) 
										)
					);
				break;
				
				case 'delete':
					$image         = isset($_REQUEST["image"]) ? stripslashes($_REQUEST["image"]):"";
					print_r( json_encode( $this->deleteImage(	$this->imagesPath,
																$this->imagesThumbsPath, 
																$image
															) 
										)
					);
				break;
			}
		}
		
	}

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::checkAllowedFormats
	****************************************************************************************/
	function checkAllowedFormats($imageName, $allowedFormats)
	{
		// quitamos caracteres extra�os
		$imageName = preg_replace('/[^(\x20-\x7F)]*/','', $imageName);
		
		// extensi�n del archivo
		$ext       =  strtolower( substr($imageName, strpos($imageName,'.')+1, strlen($imageName)-1) );

		if(!in_array($ext,explode(',', $allowedFormats))) return false;
		else return true;
	}

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::uploadHtmlEditorImage
	****************************************************************************************/
	function uploadHtmlEditorImage($allowedFormats,$maxSize,$imagesPath,$imagesUrl,$createThumbnails=false,$imagesTumbsUrl,$imagesThumbsPath)
	{	
		global $_FILES;
		$result = array();
		
		if(!$this->checkAllowedFormats($_FILES['photo-path']['name'], $allowedFormats))
		{
			$result= array(
				'success'	=> false,
				'message'	=> 'Error',
				'data'		=> '',
				'total'		=> '0',
				'errors'	=> 'The file you attempted to upload is not allowed.'
			);
			return $result;
		}

		$nombreArchivo = preg_replace('/[^(\x20-\x7F)]*/','', $_FILES['photo-path']['name']);
		$ext           =  strtolower( substr($nombreArchivo, strpos($nombreArchivo,'.')+1, strlen($nombreArchivo)-1) );
		
		while (file_exists($imagesPath.$nombreArchivo)) {
			$prefijo       = substr(md5(uniqid(rand())),0,6);
			$nombreArchivo = $prefijo.'_'.$nombreArchivo;
		}

		if(filesize($_FILES['photo-path']['tmp_name']) > $maxSize)
		{
			$result= array(
				'success'	=> false,
				'message'	=> 'Error',
				'data'		=> '',
				'total'		=> '0',
				'errors'	=> 'The file you attempted to upload is too large.'
			);
			return $result;
		}
		
		// Check if we can upload to the specified path, if not DIE and inform the user.
		if(!is_writable($imagesPath))
		{
			$result= array(
				'success'	=> false,
				'message'	=> 'Error',
				'data'		=> '',
				'total'		=> '0',
				'errors'	=> 'You cannot upload to the specified directory: '.$imagesPath.', please CHMOD it to 777 or check permissions'
			);
			return $result;
		}

		
		if(move_uploaded_file($_FILES['photo-path']['tmp_name'],$imagesPath.$nombreArchivo))
		{		
			if($createThumbnails)
			{			
				$thumb = PhpThumbFactory::create($imagesPath.$nombreArchivo);
				$thumb->adaptiveResize(64, 64);
				$thumb->save($imagesThumbsPath.$nombreArchivo);
			}
			
			$result= array(
				'success'	=> true,
				'message'	=> 'Image Uploaded Successfully',
				'data'		=> array('src'=>$imagesUrl.$nombreArchivo),
				'total'		=> '1',
				'errors'	=> ''
			);
		}
		else
		{
			$result= array(
				'success'	=> false,
				'message'	=> 'Error',
				'data'		=> '',
				'total'		=> '0',
				'errors'	=> 'Error Uploading Image'
			);
		}
		return $result;
	}

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::deleteImage
	****************************************************************************************/
	function deleteImage($imagesPath,$imagesThumbsPath, $image = null)
	{
		if(file_exists($imagesPath.$image))
		{
			// remove image
			unlink($imagesPath.$image);
			 
			// remove thumbnail if exists
			if(file_exists($imagesThumbsPath.$image))unlink($imagesThumbsPath.$image);
			
			return array(
				'success'	=> true,
				'message'	=> 'Success',
				'data'		=> '',
				'total'		=> 1,
				'errors'	=> ''
			);
		}
		else
		{
			return array(
				'success'	=> false,
				'message'	=> 'Error',
				'data'		=> '',
				'total'		=> 0,
				'errors'	=> 'Delete Operation Failed'
			);
		}
	}

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::cropImage
	****************************************************************************************/
	function cropImage($imagesPath,$imagesThumbsPath,$imagesUrl,$imagesThumbsUrl,$imageSrc,$width,$height,$offsetLeft,$offsetTop, $zoom, $allowedFormats)
	{
		$imageName = preg_replace('/[^(\x20-\x7F)]*/','', basename($imageSrc));
			
		$zoom      = $zoom/100;
		$left      = $offsetLeft > 0 ? round($offsetLeft/$zoom) : 0;
		$width     = round($width/$zoom);		
		$top       = $offsetTop > 0 ? round($offsetTop/$zoom) : 0;
		$height    = round($height/$zoom);
		
		try
		{
			// make the cropped image
			$thumb = PhpThumbFactory::create($imagesPath.$imageName);
			$thumb->crop($left, $top, $width, $height);
			$thumb->save($imagesPath.$imageName);
			
			// make the thumbnail for the cropped image
			$thumb->adaptiveResize(64, 64);
			$thumb->save($imagesThumbsPath.$imageName);
			
			return array(
				'success'	=> true,
				'message'	=> 'Success',
				'data'		=>  array('src'=>$imagesUrl.$imageName),
				'total'		=> 1,
				'errors'	=> ''
			);
		}
		catch (Exception $e)
		{
			return array(
				'success'	=> false,
				'message'	=> 'Error',
				'data'		=>  'Error with rotate operation.'.$e,
				'total'		=> 1,
				'errors'	=> ''
			);
		}
	}

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::rotateImage
	****************************************************************************************/
	function rotateImage($imagesPath,$imagesThumbsPath,$imagesUrl,$imageSrc)
	{
		$imageName = preg_replace('/[^(\x20-\x7F)]*/','', basename($imageSrc));
		
		try
		{
			// make the cropped image
			$thumb = PhpThumbFactory::create($imagesPath.$imageName);
			$thumb->rotateImageNDegrees(-90);
			$thumb->save($imagesPath.$imageName);
			
			// make the thumbnail for the cropped image
			$thumb->adaptiveResize(64, 64);
			$thumb->save($imagesThumbsPath.$imageName);
			
			return array(
				'success'	=> true,
				'message'	=> 'Success',
				'data'		=>  array('src'=>$imagesUrl.$imageName),
				'total'		=> 1,
				'errors'	=> ''
			);
		}
		catch (Exception $e)
		{
			return array(
				'success'	=> false,
				'message'	=> 'Error',
				'data'		=>  'Error with rotate operation.'.$e,
				'total'		=> 1,
				'errors'	=> ''
			);
		}
	}

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::resizeImage
	****************************************************************************************/
	function resizeImage($imagesPath,$imagesThumbsPath,$imagesUrl,$imageSrc, $width, $height)
	{
		$imageName = preg_replace('/[^(\x20-\x7F)]*/','', basename($imageSrc));
		
		try
		{
			$thumb = PhpThumbFactory::create($imagesPath.$imageName, array('resizeUp' => true));
			$thumb->resize($width, $height);
			$thumb->save($imagesPath.$imageName);
			
			// make the thumbnail for the cropped image
			$thumb->adaptiveResize(64, 64);
			$thumb->save($imagesThumbsPath.$imageName);
			
			return array(
				'success'	=> true,
				'message'	=> 'Success',
				'data'		=>  array('src'=>$imagesUrl.$imageName),
				'total'		=> 1,
				'errors'	=> ''
			);
		}
		catch (Exception $e)
		{
			return array(
				'success'	=> false,
				'message'	=> 'Error',
				'data'		=>  'Error with rotate operation.'.$e,
				'total'		=> 1,
				'errors'	=> ''
			);
		}
	}

	/****************************************************************************************
	* HandleHtmlEditorImageUpload::getImages
	****************************************************************************************/
	function getImages($imagesPath, $imagesUrl,$imagesTumbsUrl,$imagesThumbsPath, $allowedFormats, $start = 0, $limit = 10,$query ="")
	{
		// array to hold return value
		$results = array();
		
		$handler = opendir($imagesPath);

		// open directory and walk through the filenames
		while ($file = readdir($handler)) {
		
			// extensi�n del archivo
			$ext =  strtolower( substr($file, strpos($file,'.')+1, strlen($file)-1) );

			if(in_array($ext,explode(',', $allowedFormats)))
			{		
				$resume = strlen ( $file ) > 18 ?  substr($file,0, 12).'...' : $file;
				if ($file != "." && $file != "..") {
					
					if( $query == "" || ($query != "" && stripos($file,$query)!== false) ){
						
						// XIPOLIAS CHANGE
						//$thumbSrc = file_exists($imagesThumbsPath.$file) ? $imagesTumbsUrl.$file : $imagesUrl.$file;
						$thumbSrc = $imagesUrl.$file;
						$thumbSrc = strpos($thumbSrc, "https://") ? $thumbSrc : $thumbSrc.'?'.rand(1, 10000);
						$results[] = array('fullname'=>$file,'name'=>$resume,'src'=>$imagesUrl.$file,'thumbSrc'=>$thumbSrc);
					}
				}
			}
		}
		
		return array(
			'success'	=> true,
			'message'	=> 'Success',
			'data'		=> $output = array_slice($results, $start, $limit),
			'total'		=> count($results),
			'errors'	=> ''
		);
	}
	
}


?>