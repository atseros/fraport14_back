<?php
// include config file
include_once("config.php");
// include framework file
include_once("./framework/class.Framework.php");

$appFrw = new Framework($config);
$appFrw->init();

$appPage = "";

////////////////////////////////////////////////////////////////////////
// Check if it is a logout
////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['logout']))
{
	$appFrw->resetSession();	
	$app_url = $appFrw->appMainUrl;	
	header("Location: ".$app_url);
	$appFrw->close();
	exit();
}

////////////////////////////////////////////////////////////////////////
// Check if user is authenticated - find page to send
////////////////////////////////////////////////////////////////////////
if( !$appFrw->isAuthenticated)
	$appPage = "Login";
else if(isset($_REQUEST['appPage']) )
	$appPage = $_REQUEST['appPage'];
else
	$appPage = $appFrw->getDefaultPage();

	
$page_file = "./apps/$appPage/page.$appPage.php";

if( !is_file("./apps/$appPage/page.$appPage.php") )
	exit("Error:: Page Not Found :: ".$page_file);

$appFrw->RedirectToPage = "";

include($page_file);


////////////////////////////////////////////////////////////////////////
// check if redirect is needed
////////////////////////////////////////////////////////////////////////
if( $appFrw->RedirectToPage != "" )
{
	$redirect_url = "/appMain.php?appPage=".$appFrw->RedirectToPage;
	if( is_array($appFrw->RedirectToParams) )
	{
		foreach ($appFrw->RedirectToParams as $key => $value)
		{
			$redirect_url .= "&".$key."=".$value;
		}
	}

	header("Location: ".$redirect_url);
}

$appFrw->close();
