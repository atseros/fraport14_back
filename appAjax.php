<?php

// include config file
include_once("config.php");
// include framework file
include_once("./framework/class.Framework.php");

$appFrw = new Framework($config);
$appFrw->init();

$appPage = "";
$appMethod = "";

////////////////////////////////////////////////////////////////////////
// Check if it is a logout
////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST['appMethod']) AND $_REQUEST['appMethod'] == 'logout')
{
	$appFrw->resetSession();	
	$result = array('success' => true);
	header('Content-type: application/json; charset=utf-8');
	exit(json_encode($result));
}

////////////////////////////////////////////////////////////////////////
// if not logged in 
////////////////////////////////////////////////////////////////////////
if( !$appFrw->isAuthenticated)
{
	// Check for Mobile case - uses a different login
	if(isset($_REQUEST['appMethod']) AND $_REQUEST['appMethod'] == 'login_verify')
	{
		$result = array('id' => 0);
		header('Content-type: application/json; charset=utf-8');
		exit(json_encode($result));
		
	}
		
	$result = array('error' => 'Έχεις αποσυνδεθεί. Πρέπει να ξανακάνεις login!');
	header('Content-type: application/json; charset=utf-8');
	exit(json_encode($result));
}

////////////////////////////////////////////////////////////////////////////
// HANDLE Ajax Request
////////////////////////////////////////////////////////////////////////////
if(isset($_REQUEST) AND isset($_REQUEST["appPage"]) AND isset($_REQUEST["appMethod"]) )
{
	$appPage = $_REQUEST["appPage"];
	$appMethod = $_REQUEST["appMethod"];
	
	$controller_file = "./apps/$appPage/class.$appPage.php";
	
	if(is_file($controller_file))
	{
		require_once($controller_file);

		$controller = new $appPage($appFrw);
		$params = array();
		$response = call_user_func_array(array($controller, $appMethod), $params);

		echo $response;
	}
	else
	{
		throw new Exception("Error:: ".$controller_file);
	}
	
}


