<?php

/*********************************************************************************************
* CLASS FileManager
*
* DESCRIPTION: 
*	
*
*********************************************************************************************/
class FileManager
{		
	
	
	/****************************************************************************************
	* FileManager::getTblFolderPath
	*
	* $tblName			- Entity Table Name - to find root folder
	* $recID			- Entity Record ID - to find root folder
	****************************************************************************************/
	public static function getTblFolderPath($tblName, $recID )
	{
		$rootFolder = $GLOBALS["ROOT_FOLDER_FOR_FILES"];
		$tblFolder = "";
		
		if($tblName == '')
			return '';
		
		// check folder's entity 
		if($tblName == "sys_nodelng")
			$tblFolder = $rootFolder.'sys_nodelng';
		else if($tblName == "sys_shop")
			$tblFolder = $rootFolder.'sys_shop';
		else if($tblName == "sys_shoplng")
			$tblFolder = $rootFolder.'sys_shoplng';
		else if($tblName == "shop_location")
			$tblFolder = $rootFolder.'shop_location';
		else if($tblName == "sys_media")
			$tblFolder = $rootFolder.'sys_media';
		else if($tblName == "sys_slider_images")
			$tblFolder = $rootFolder.'sys_slider_images';
		else if($tblName == "sys_alertlng")
			$tblFolder = $rootFolder.'sys_alertlng';
		else if($tblName == "sys_disasterlng")
			$tblFolder = $rootFolder.'sys_disasterlng';
		else if($tblName == "sys_central_airlines")
			$tblFolder = $rootFolder.'sys_central_airlines';
		else if($tblName == "sys_related_links")
			$tblFolder = $rootFolder.'sys_related_links';
		else if($tblName == "sys_central_media")
			$tblFolder = $rootFolder.'sys_central_media';
		else if($tblName == "sys_categories_records")
			$tblFolder = $rootFolder.'sys_categories_records';
		else if($tblName == "sys_categories_recordslng")
			$tblFolder = $rootFolder.'sys_categories_recordslng';
		else if($tblName == "sys_image_gallery")
			$tblFolder = $rootFolder.'sys_image_gallery';
		else if($tblName == "sys_image_gallery_data")
			$tblFolder = $rootFolder.'sys_image_gallery_data';
		else if($tblName == "sys_video_gallery")
			$tblFolder = $rootFolder.'sys_video_gallery';
		else if($tblName == "sys_publications_gallery")
			$tblFolder = $rootFolder.'sys_publications_gallery';
		else if($tblName == "sys_publications_gallery_data")
			$tblFolder = $rootFolder.'sys_publications_gallery_data';
        else if($tblName == "cf_answers_lang")
            $tblFolder = $rootFolder.'cf_answers_lang';
        else if($tblName == "sys_footer_timetablelng")
            $tblFolder = $rootFolder.'sys_footer_timetablelng';
		else
			return '';
		
		// check root entity folder exists 
		if( $tblFolder <> "" AND !is_dir( $tblFolder ) )
			mkdir ( $tblFolder );
		
		//
		$n = (int) ($recID / 1000);
		if($recID > 0)
			$tblFolder = $tblFolder.'/'.$n;
		//
		if( $tblFolder <> "" AND !is_dir( $tblFolder ) )
			mkdir ( $tblFolder );
	
		// check record id
		if($recID > 0)
			$tblFolder = $tblFolder.'/'.$recID;

		// check record folder exists 
		if( $tblFolder <> "" AND !is_dir( $tblFolder ) )
			mkdir ( $tblFolder );
		
		
		$thumb_folder = $tblFolder."/thumbs";
		if( $thumb_folder <> "" AND !is_dir( $thumb_folder ) )
			mkdir ( $thumb_folder );
		
		return $tblFolder;
	}
	
	/****************************************************************************************
	* FileManager::getTblFolderUrl
	*
	* $tblName			- Entity Table Name - to find root folder
	* $recID			- Entity Record ID - to find root folder
	****************************************************************************************/
	public static function getTblFolderUrl($tblName, $recID )
	{
		$rootUrl = $GLOBALS["ROOT_URL_FOR_FILES"];
		
		// check folder's entity 
		if($tblName == "sys_nodelng")
			$tblUrl = $rootUrl.'sys_nodelng';
		else if($tblName == "sys_shop")
			$tblUrl = $rootUrl.'sys_shop';
		else if($tblName == "sys_shoplng")
			$tblUrl = $rootUrl.'sys_shoplng';
		else if($tblName == "shop_location")
			$tblUrl = $rootUrl.'shop_location';
		else if($tblName == "sys_media")
			$tblUrl = $rootUrl.'sys_media';
		else if($tblName == "sys_slider_images")
			$tblUrl = $rootUrl.'sys_slider_images';
		else if($tblName == "sys_alertlng")
			$tblUrl = $rootUrl.'sys_alertlng';
		else if($tblName == "sys_disasterlng")
			$tblUrl = $rootUrl.'sys_disasterlng';
		else if($tblName == "sys_central_airlines")
			$tblUrl = $rootUrl.'sys_central_airlines';
		else if($tblName == "sys_related_links")
			$tblUrl = $rootUrl.'sys_related_links';
		else if($tblName == "sys_central_media")
			$tblUrl = $rootUrl.'sys_central_media';
		else if($tblName == "sys_categories_records")
			$tblUrl = $rootUrl.'sys_categories_records';
		else if($tblName == "sys_categories_recordslng")
			$tblUrl = $rootUrl.'sys_categories_recordslng';
		else if($tblName == "sys_image_gallery")
			$tblUrl = $rootUrl.'sys_image_gallery';
		else if($tblName == "sys_image_gallery_data")
			$tblUrl = $rootUrl.'sys_image_gallery_data';
		else if($tblName == "sys_video_gallery")
			$tblUrl = $rootUrl.'sys_video_gallery';
		else if($tblName == "sys_publications_gallery")
			$tblUrl = $rootUrl.'sys_publications_gallery';
		else if($tblName == "sys_publications_gallery_data")
			$tblUrl = $rootUrl.'sys_publications_gallery_data';
        else if($tblName == "cf_answers_lang")
            $tblUrl = $rootUrl.'cf_answers_lang';
        else if($tblName == "sys_footer_timetablelng")
            $tblUrl = $rootUrl.'sys_footer_timetablelng';
		else
			return '';
		
		$n = (int) ($recID / 1000);
		if($recID > 0)
			$tblUrl = $tblUrl.'/'.$n;
		
		$tblUrl = $tblUrl.'/'.$recID;
		
		return $tblUrl;
	}

	
	/****************************************************************************************
	* FileManager::get_of_line_TblFolderPath
	*
	* $tblName			- Entity Table Name - to find root folder
	* $recID			- Entity Record ID - to find root folder
	****************************************************************************************/
	public static function get_of_line_TblFolderPath($tblName, $recID )
	{
		$rootFolder = $GLOBALS["ROOT_FOLDER_FOR_FILES_OF_LINE"];
		$tblFolder = "";
		
		if($tblName == '')
			return '';
		
		// check folder's entity 
		if($tblName == "sys_cvt")
			$tblFolder = $rootFolder.'sys_cvt';
		else
			return '';
		
		// check root entity folder exists 
		if( $tblFolder <> "" AND !is_dir( $tblFolder ) )
			mkdir ( $tblFolder );
		
		//
		$n = (int) ($recID / 1000);
		if($recID > 0)
			$tblFolder = $tblFolder.'/'.$n;
		//
		if( $tblFolder <> "" AND !is_dir( $tblFolder ) )
			mkdir ( $tblFolder );
	
		// check record id
		if($recID > 0)
			$tblFolder = $tblFolder.'/'.$recID;

		// check record folder exists 
		if( $tblFolder <> "" AND !is_dir( $tblFolder ) )
			mkdir ( $tblFolder );
		
		return $tblFolder;
	}

	/****************************************************************************************
	* FileManager::get_of_line_TblFolderUrl
	*
	* $tblName			- Entity Table Name - to find root folder
	* $recID			- Entity Record ID - to find root folder
	****************************************************************************************/
	public static function get_of_line_TblFolderUrl($tblName, $recID )
	{		
		$urlFolder = '';

		if($tblName == "sys_cvt")
			$urlFolder = "/appAjax.php?appPage=Cvts&appMethod=handleGetCvtFile&id=".$recID;
		else
			return '';
	}
	
	/****************************************************************************************
	* FileManager::getMimeTypeFromExtension
	****************************************************************************************/
	public static function getMimeTypeFromExtension($ext) 
	{
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (array_key_exists($ext, $mime_types)) 
		{
            return $mime_types[$ext];
        }        
        else 
		{
            return 'application/octet-stream';
        }
    }
	
	/****************************************************************************************
	* FileManager::resize_crop_image //resize and crop image by center
	****************************************************************************************/
	
	public static function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80){
		$imgsize = getimagesize($source_file);
		$width = $imgsize[0];
		$height = $imgsize[1];
		$mime = $imgsize['mime'];
	 
		switch($mime){
			case 'image/gif':
				$image_create = "imagecreatefromgif";
				$image = "imagegif";
				break;
	 
			case 'image/png':
				$image_create = "imagecreatefrompng";
				$image = "imagepng";
				$quality = 7;
				break;
	 
			case 'image/jpeg':
				$image_create = "imagecreatefromjpeg";
				$image = "imagejpeg";
				$quality = 80;
				break;
	 
			default:
				return false;
				break;
		}
		 
		$dst_img = imagecreatetruecolor($max_width, $max_height);
		$src_img = $image_create($source_file);
		 
		$width_new = $height * $max_width / $max_height;
		$height_new = $width * $max_height / $max_width;
		//if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
		if($width_new > $width){
			//cut point by height
			$h_point = (($height - $height_new) / 2);
			//copy image
			imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
		}else{
			//cut point by width
			$w_point = (($width - $width_new) / 2);
			imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
		}
		 
		$image($dst_img, $dst_dir, $quality);
	 
		if($dst_img)imagedestroy($dst_img);
		if($src_img)imagedestroy($src_img);
	}
}

