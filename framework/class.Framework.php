<?php
require_once('./framework/class.FileManager.php');
require_once('./db/class.DB_sys_usr.php');


/*********************************************************************************************
* CLASS Framework
*
* DESCRIPTION: 
*	The Framework Class 
*
*********************************************************************************************/
class Framework
{
	public	$AppName;
	
	// DB config
	private $DB_servername;
	private $DB_username;
	private $DB_password;
	private $DB_dbname;
	private $DB_port;

	// Database link
	public $DB_Link;
	
	// User params
	public $isAuthenticated;
	public $UsrID;
	
	//
	public $seo_urls;
	
	// urls
	public $rootUrl;
	public $appMainUrl;
	public $appAjaxUrl;
	public $appPrintUrl;
	public $frontMainUrl;
	public $frontAjaxUrl;
	
	// Error Handling
	public 	$ErrorMsg;
	
	// default_app
	public $default_app;
	public $home_app;
	
	// Redirect
	public $RedirectToPage;
	public $RedirectToParams;
	
	// supported languages
	public $default_lang;
	public $supported_lang;
	
	
    /****************************************************************************************
	* Framework::CONSTRUCTOR
	****************************************************************************************/
	function Framework($config)
	{
		$this->AppName			= $config["AppName"];
		
		$this->DB_servername	= $config["DB_servername"];
		$this->DB_username		= $config["DB_username"];
		$this->DB_password		= $config["DB_password"];
		$this->DB_dbname		= $config["DB_dbname"];
		$this->DB_port			= $config["DB_port"];
		
		$this->seo_urls			= $config["seo_urls"];

		$this->default_app		= $config["default_app"];
		$this->home_app			= $config["home_app"];
		
		$this->default_lang			= $config["default_lang"];
		$this->supported_lang		= $config["supported_lang"];
	}
	
	/****************************************************************************************
	* Framework::init
	****************************************************************************************/
	function init()
	{		
		// start session management
		session_start();
				
		// check if there are seesion vars saved
		$this->checkAuthentication();
		
		// Create connection
		$this->DB_Link = new mysqli($this->DB_servername, $this->DB_username, $this->DB_password, $this->DB_dbname, $this->DB_port);

		// Check connection
		if ($this->DB_Link->connect_error) 
		{
			exit($this->AppName.":: Failed to connect to database; <br><pre>".print_r( $this->DB_Link->connect_error."</pre>", true) );
		} 

		
		//$this->DB_Link->set_charset("latin1_swedish_ci");
		$this->DB_Link->set_charset("utf8");
		
		// it can be index.php, appMain.php, appAjax.php, appPrint.php
		$REQUEST_SCHEME = ( isset($_SERVER['REQUEST_SCHEME']) AND $_SERVER['REQUEST_SCHEME'] != "") ? $_SERVER['REQUEST_SCHEME'] : "http";

		$SERVER_PORT = ($_SERVER['SERVER_PORT'] == "80") ? "" : ":".$_SERVER['SERVER_PORT'];
		
		$page_url = $REQUEST_SCHEME."://".$_SERVER['SERVER_NAME'].$SERVER_PORT.$_SERVER['SCRIPT_NAME'];

		$page_url = str_replace("/index.php", "", $page_url);
		$page_url = str_replace("/admin.php", "", $page_url);
		$page_url = str_replace("/appMain.php", "", $page_url);
		$page_url = str_replace("/appAjax.php", "", $page_url);
		$page_url = str_replace("/appPrint.php", "", $page_url);
		$page_url = str_replace("/frontMain.php", "", $page_url);
		$page_url = str_replace("/frontAjax.php", "", $page_url);
		
		$this->rootUrl = $page_url;
		
		$this->appMainUrl = $this->rootUrl."/appMain.php";
		$this->appAjaxUrl = $this->rootUrl."/appAjax.php";
		$this->appPrintUrl = $this->rootUrl."/appPrint.php";
		
		$this->frontMainUrl = $this->rootUrl."/frontMain.php";
		$this->frontAjaxUrl = $this->rootUrl."/frontAjax.php";
	}

	/****************************************************************************************
	* Framework::checkAuthentication
	****************************************************************************************/
	function checkAuthentication()
	{
		if( isset( $_SESSION[$this->AppName]) && isset( $_SESSION[$this->AppName]['Authenticated'] ) &&  $_SESSION[$this->AppName]['Authenticated'] == 1 )
			$this->isAuthenticated = 1;
		else
			$this->isAuthenticated = 0;
			
		if($this->isAuthenticated AND isset($_SESSION[$this->AppName]['UsrID']))
		{
			$this->UsrID			= $_SESSION[$this->AppName]['UsrID'];
		}
	}

	/****************************************************************************************
	* Framework::authenticate
	****************************************************************************************/
	function authenticate($username, $password)
	{
		// check for user
		if( $this->DB_Link )
			$UsrID = DB_sys_usr::get_LoginUsrID($this, $username, $password);
		else 
			$UsrID = 0;
	
		
		if( $UsrID > 0)
		{
			$this->setSessionVar('UsrID', $UsrID);
			$this->setSessionVar('Authenticated', 1);
			return 1;
		}
					
		return 0;
	}
	
	/****************************************************************************************
	* Framework::get_LangID_By_LangType
	****************************************************************************************/
	function get_LangID_By_LangType($LangType)
	{
		if( isset($this->supported_lang[$LangType]) AND isset($this->supported_lang[$LangType]["id"]) )
			return    $this->supported_lang[$LangType]["id"];
		else 
			return 0;
	}
	
	/****************************************************************************************
	* Framework::get_LangType_By_LangID
	****************************************************************************************/
	function get_LangType_By_LangID($LangID)
	{
		foreach($this->supported_lang as $lang)
		{
			if($lang["id"] == $LangID)
				return $lang["code"];
		}
		return "";
	}
	
	/****************************************************************************************
	* Framework::setSessionVar
	****************************************************************************************/
	function setSessionVar($name, $value)
	{
		$_SESSION[$this->AppName][$name] = $value;
	}

	/****************************************************************************************
	* Framework::getSessionVar
	****************************************************************************************/
	function getSessionVar($name)
	{
		if(isset($_SESSION[$this->AppName][$name]))
			return $_SESSION[$this->AppName][$name];
		else 
			return 0;
	}

	/****************************************************************************************
	* Framework::resetSession
	****************************************************************************************/
	function resetSession()
	{
		$_SESSION[$this->AppName] = null;
		session_unset();
		session_destroy();
		session_start();
	}

	/****************************************************************************************
	* Framework::getDefaultPage
	****************************************************************************************/
	function getDefaultPage()
	{	
		if($this->UsrID > 0)
		{			
			$UsrDetails = DB_sys_usr::get_UsrDetails($this, $this->UsrID);

			//if($UsrDetails["UsrRole"] == 0)
			return $this->home_app;			
		}		
		else
			return 'Login';
	}
	

	/****************************************************************************************
	* Framework::build_page_url
	****************************************************************************************/
	function build_page_url($lang, $page_url)
	{
		
		
		if($this->seo_urls == 0)
		{
			$link = $this->rootUrl."/index.php";
			
			if($lang)
				$link = $link."?lang=".$lang;
		
			if($page_url)
				$link = $link."&page=".$page_url;
		}
		else
		{
			$link = $this->rootUrl."";
			
			if($lang)
				$link = $link."/".$lang;
		
			if($page_url)
				$link = $link."/".$page_url;
			else
				$link = $link."/";
		}
		

		return $link;
	}
		
	/****************************************************************************************
	* Framework::close
	****************************************************************************************/
	function close()
	{
		if($this->DB_Link) 
			$this->DB_Link->close();
	}
}

