<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_seasonal_flight_plan
*
* DESCRIPTION: 
*	Class for table sys_seasonal_flight_plan. 
*
*
* table fields:
*
 `SfpArrivalDeparture` varchar(1024) NOT NULL,
 `SfpFromDate` varchar(1024) NOT NULL,
 `SfpToDate` varchar(1024) NOT NULL,
 `SfpDays` varchar(1024) NOT NULL,
 `SfpFlightNumber` varchar(1024) NOT NULL,
 `SfpTime` varchar(1024) NOT NULL,
 `SfpDestination` varchar(1024) NOT NULL,
 `SfpSfpvID` int(11) NOT NULL
*
*********************************************************************************************/
class DB_sys_seasonal_flight_plan
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan::sys_seasonal_flight_plan_insert_csv
	* --------------------------------------------------------------------------
	*/
	public static function sys_seasonal_flight_plan_insert_csv($appFrw, $params)
	{
		$results = array();

		$SfpSfpvID = $params["SfpvID"];	
		
		$filename=$params["FlightCsv"]["FlightCsv"]["tmp_name"];
		
		$file = fopen($filename, "r");

        $RowCount=0;

        while ((fgetcsv($file)) !== FALSE)
        {
            $RowCount++;
        }

        fclose($file);


        $AirportIATA_str = "";
        $SfpFlightNumber_str = "";

        // AirportID & AirlineID IN PHP Array
//        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
//        {
//            // $AirportIATA_str
//            if($AirportIATA_str !="") $AirportIATA_str .= ",";
//
//            $AirportIATA = $getData[6];
//
//            if(($AirportIATA < 0) || ($AirportIATA == null))
//            {
//                $AirportIATA = 0;
//            }
//
//            $AirportIATA_str .= $AirportIATA;
//
//            // $SfpFlightNumber_str
//            if($SfpFlightNumber_str !="") $SfpFlightNumber_str .= ",";
//
//            $SfpFlightNumber = $getData[4];
//
//            $SfpFlightNumber_str .= $SfpFlightNumber;
//
//        }

//        fclose($file);

        $filename=$params["FlightCsv"]["FlightCsv"]["tmp_name"];

        $file = fopen($filename, "r");

        $insert_str = "";

        $cnt = 0;

//        $SfpID = DB_sys_kxn::get_NextID_mass($appFrw, 'sys_seasonal_flight_plan', $RowCount );

        while (($getData = fgetcsv($file, 0, ",")) !== FALSE)
         {
            $AirportIATA = $getData[6];

            if(($AirportIATA < 0) || ($AirportIATA == null))
            {
                $AirportIATA = 0;
            }

            $SfpDestinationID = DB_sys_seasonal_flight_plan::getAirportID($appFrw, $AirportIATA);

            $SfpFlightNumber = $getData[4];

            $SfpAirlineID = DB_sys_seasonal_flight_plan::getAirlineID($appFrw, $SfpFlightNumber);

            $SfpFromDate = strtotime($getData[1]);
            $SfpFromDate_New = date('Y-m-d',$SfpFromDate);

            $SfpToDate = strtotime($getData[2]);
            $SfpToDate_New = date('Y-m-d',$SfpToDate);

            $Date_now = date("Y-m-d");
            $SfpTime = $Date_now.$getData[5];
            $SfpTime2 = strtotime($SfpTime);
            $SfpTime_New = date('Y-m-d H:i',$SfpTime2);

            if($insert_str !="") $insert_str .= ",";

             $SfpID = DB_sys_kxn::get_NextID($appFrw, 'sys_seasonal_flight_plan');

             $insert_str.="(
                             '".$SfpID."'
                            ,'".$getData[0]."'
                            ,'".$SfpFromDate_New."'
                            ,'".$SfpToDate_New."'
                            ,'".$getData[3]."'
                            ,'".$getData[4]."'
                            ,'".$SfpTime_New."'
                            ,'".$getData[6]."'
                            ,'".$SfpDestinationID."'
                            ,'".$SfpSfpvID."'
                            ,'".$SfpAirlineID."'
                        )";

             $cnt ++;
//             $SfpID ++;

             if($cnt > 100)
             {
                 DB_sys_seasonal_flight_plan::massInsert($appFrw, $insert_str);

                 $insert_str = "";
                 $cnt=0;
             }

         }

         if($cnt > 0){
             DB_sys_seasonal_flight_plan::massInsert($appFrw, $insert_str);

             $insert_str = "";
             $cnt=0;
         }
		
		 fclose($file);	

		 $query = "DELETE FROM sys_seasonal_flight_plan
						
						WHERE 
						
						SfpArrivalDeparture != 'A'
						AND 
						SfpArrivalDeparture != 'D' 
						";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
	
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();
		
		$results["success"] = true;
		$results["data"] = $SfpSfpvID;
		return $results;
		
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan::massInsert
	* --------------------------------------------------------------------------
	*/
	public static function massInsert($appFrw, $insert_str)
	{
        //sleep(1);

	    $query = "INSERT into sys_seasonal_flight_plan
            (
                SfpID
                ,SfpArrivalDeparture
                ,SfpFromDate
                ,SfpToDate
                ,SfpDays
                ,SfpFlightNumber
                ,SfpTime
                ,SfpDestination
                ,SfpDestinationID
                ,SfpSfpvID
                ,SfpAirlineID

            )
            VALUES
            ".$insert_str;

			$stmt = $appFrw->DB_Link->prepare($query);

			if(!$stmt)
				exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);

			//$stmt->bind_param("iii", $UsrID, $UsrRole, $UsrStatus);

			if(!$stmt->execute())
				exit("insert_Record: error at insert : ".$stmt->error);

			$stmt->close();
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan::getAirportID
	* --------------------------------------------------------------------------
	*/
	public static function getAirportID($appFrw, $AirportIATA)
	{
		$query = "SELECT AirportID FROM sys_airports WHERE AirportIATA = ? ";

		$stmt = $appFrw->DB_Link->prepare($query);

		if(!$stmt)
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);

		$stmt->bind_param("s", $AirportIATA);

		if(!$stmt->execute())
			exit("check_RecordExists: error at select : ".$stmt->error);

		$result = $stmt->get_result();
		$stmt->close();

		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);

		$row = $result->fetch_assoc();
		$result->close();

		return $row["AirportID"];
	}

	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan::getAirlineID
	* --------------------------------------------------------------------------
	*/
	public static function getAirlineID($appFrw, $SfpFlightNumber)
	{
		$substr3 = substr($SfpFlightNumber, 0, 3);
		$substr2 = substr($SfpFlightNumber, 0, 2);
		
		$query = "
			SELECT
				C_AirlinesID
			FROM sys_central_airlines
			WHERE C_AirlinesICAO LIKE '%" . $substr3 . "%'
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("getAirlineID: error at prepare statement: ".$appFrw->DB_Link->error);
		
		if(!$stmt->execute()) 
			exit("getAirlineID: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("getAirlineID: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		if(empty($row["C_AirlinesID"]))
		{
			$query = "
				SELECT
					C_AirlinesID
				FROM sys_central_airlines
				WHERE C_AirlinesIATA LIKE '%" . $substr2 . "%'
			";
			
			$stmt = $appFrw->DB_Link->prepare($query);
			
			if(!$stmt) 
				exit("getAirlineID: error at prepare statement: ".$appFrw->DB_Link->error);
			
			if(!$stmt->execute()) 
				exit("getAirlineID: error at select : ".$stmt->error);
					
			$result = $stmt->get_result();
			$stmt->close();			
									
			if(!$result)
				exit("getAirlineID: error at select : ".$stmt->error);
										
			$row = $result->fetch_assoc();		
			$result->close();
		}
		
		return $row["C_AirlinesID"]; 
	}

	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan::sys_seasonal_flight_plan_get_List
	* --------------------------------------------------------------------------
	*/
	public static function sys_seasonal_flight_plan_get_List($appFrw, $params)
	{
		$results = array();

		$SfpvID	= isset($params["SfpvID"]) ? (int)$params["SfpvID"] : 0;
		
				
		$query = "	SELECT
						
							SfpArrivalDeparture
							,SfpFromDate
							,SfpToDate
							,SfpDays
							,SfpFlightNumber
							,SfpTime
							,SfpDestination
							,SfpAirlineID
						
							FROM sys_seasonal_flight_plan
							
							WHERE
							(
								SfpSfpvID = ?
							)

									
		";
		

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $SfpvID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$SfpFromDate = strtotime($row['SfpFromDate']);
			
			$row['SfpFromDate'] = '';
			$row['SfpFromDate'] = date('d/m/Y',$SfpFromDate);
			
			$SfpToDate = strtotime($row['SfpToDate']);
			
			$row['SfpToDate'] = '';
			$row['SfpToDate'] = date('d/m/Y',$SfpToDate);
			
			$SfpTime = strtotime($row['SfpTime']);
			
			$row['SfpTime'] = '';
			$row['SfpTime'] = date('H:i',$SfpTime);
			
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
		
	
}
