<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_central_airlineslng
*
* DESCRIPTION: 
*	Class for table sys_central_airlineslng
*
* table fields:
*
 `C_AirlinesLngID` int(11) NOT NULL,
 `C_AirlinesLngC_AirlinesID` int(11) NOT NULL,
 `C_AirlinesLngName` varchar(1024) NOT NULL,
 `C_AirlinesLngAddress` varchar(1024) NOT NULL,
 `C_AirlinesLngPhones` varchar(1024) NOT NULL,
 `C_AirlinesLngType` int(11) DEFAULT NULL,
*
*********************************************************************************************/
class DB_sys_central_airlineslng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlineslng::sys_central_airlineslng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_central_airlineslng_get_NewRecordDefValues($appFrw, $params)
	{	
		$C_AirlinesLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_central_airlineslng');
		
		if($C_AirlinesLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["C_AirlinesLngID"] = $C_AirlinesLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_central_airlineslng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlineslng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $C_AirlinesLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT C_AirlinesLngID FROM sys_central_airlineslng WHERE C_AirlinesLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $C_AirlinesLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlineslng::sys_central_airlineslng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlineslng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$C_AirlinesLngID 	= (int)$params["C_AirlinesLngID"];
		
		if($C_AirlinesLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_central_airlineslng::check_RecordExists($appFrw, $C_AirlinesLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$C_AirlinesLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_central_airlineslng
					(
						 C_AirlinesLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $C_AirlinesLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_central_airlineslng::sys_central_airlineslng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlineslng::sys_central_airlineslng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlineslng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$C_AirlinesLngID = (int)$params["C_AirlinesLngID"];
		$C_AirlinesLngType = $params["C_AirlinesLngType"];
		
		if($C_AirlinesLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_central_airlineslng::check_RecordExists($appFrw, $C_AirlinesLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$C_AirlinesLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						C_AirlinesLngID
						,C_AirlinesLngC_AirlinesID 
						,C_AirlinesLngName 
						,C_AirlinesLngPhones 
						,C_AirlinesLngType 
						,C_AirlinesLngDictionaryDescription 
							
					FROM sys_central_airlineslng
					WHERE
					(
						C_AirlinesLngID = ?
					)
					AND
					(
						C_AirlinesLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $C_AirlinesLngID, $C_AirlinesLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlineslng::sys_central_airlineslng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlineslng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$C_AirlinesLngID = (int)$params["C_AirlinesLngID"];
		$C_AirlinesLngType = (isset($params['C_AirlinesLngType'])) ? $params['C_AirlinesLngType'] : $record['C_AirlinesLngType'];
		
		if($C_AirlinesLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_central_airlineslng::sys_central_airlineslng_getRecord($appFrw, array('C_AirlinesLngID'=>$C_AirlinesLngID,'C_AirlinesLngType'=>$C_AirlinesLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$C_AirlinesLngC_AirlinesID 		= (isset($params['C_AirlinesLngC_AirlinesID'])) ? $params['C_AirlinesLngC_AirlinesID'] : $record['C_AirlinesLngC_AirlinesID'];
		$C_AirlinesLngName 			= (isset($params['C_AirlinesLngName'])) ? $params['C_AirlinesLngName'] : $record['C_AirlinesLngName'];
		// $C_AirlinesLngAddress 			= (isset($params['C_AirlinesLngAddress'])) ? $params['C_AirlinesLngAddress'] : $record['C_AirlinesLngAddress'];
		$C_AirlinesLngPhones 			= (isset($params['C_AirlinesLngPhones'])) ? $params['C_AirlinesLngPhones'] : $record['C_AirlinesLngPhones'];
		$C_AirlinesLngDictionaryDescription = (isset($params['C_AirlinesLngDictionaryDescription'])) ? $params['C_AirlinesLngDictionaryDescription'] : $record['C_AirlinesLngDictionaryDescription'];
		
		
		$query = "	UPDATE sys_central_airlineslng SET
						 C_AirlinesLngC_AirlinesID = ?
						,C_AirlinesLngType 			= ?
						,C_AirlinesLngName 		= ?
						,C_AirlinesLngPhones 		= ?
						,C_AirlinesLngDictionaryDescription 		= ?
						
					WHERE
					C_AirlinesLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iisssi",
								 $C_AirlinesLngC_AirlinesID 			
								,$C_AirlinesLngType 
								,$C_AirlinesLngName  
								,$C_AirlinesLngPhones 
								,$C_AirlinesLngDictionaryDescription 
								,$C_AirlinesLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $C_AirlinesLngID;
		return $results;
	}
	
	

	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlineslng::sys_central_airlineslng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlineslng_CheckBeforeInsert($appFrw, $params)
	{
		$C_AirlinesLngC_AirlinesID	= isset($params["C_AirlinesLngC_AirlinesID"]) ? (int)$params["C_AirlinesLngC_AirlinesID"] : 0;
		$C_AirlinesLngType	= isset($params["C_AirlinesLngType"]) ? (int)$params["C_AirlinesLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT C_AirlinesLngID FROM sys_central_airlineslng WHERE C_AirlinesLngC_AirlinesID = ? AND C_AirlinesLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $C_AirlinesLngC_AirlinesID, $C_AirlinesLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlineslng::sys_central_airlineslng_GetC_AirlinesLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlineslng_GetC_AirlinesLngID($appFrw, $params)
	{
		$C_AirlinesLngC_AirlinesID	= isset($params["C_AirlinesLngC_AirlinesID"]) ? (int)$params["C_AirlinesLngC_AirlinesID"] : 0;
		$C_AirlinesLngType	= isset($params["C_AirlinesLngType"]) ? (int)$params["C_AirlinesLngType"] : 0;
		
		$query = "	SELECT
						   C_AirlinesLngID
						   FROM sys_central_airlineslng
							WHERE C_AirlinesLngC_AirlinesID = ? AND C_AirlinesLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $C_AirlinesLngC_AirlinesID, $C_AirlinesLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["C_AirlinesLngID"];
	}


    /*
    * --------------------------------------------------------------------------
    * DB_sys_central_airlineslng::sys_central_airlineslng_UpdateAirlineLngRecord
    * --------------------------------------------------------------------------
    */
    public static function sys_central_airlineslng_UpdateAirlineLngRecord($appFrw, $params)
    {
        $results = array();

        $C_AirlinesLngC_AirlinesID = (isset($params['C_AirlinesLngC_AirlinesID'])) ? $params['C_AirlinesLngC_AirlinesID'] : 0;

        if($C_AirlinesLngC_AirlinesID <= 0)
        {
            $results["success"] = false;
            $results["reason"] = "update_Record: No id found. Can not update record";
            return $results;
        }

        // get param fields
        $C_AirlinesLngName_EN 		            = (isset($params['C_AirlinesLngName_EN'])) ? $params['C_AirlinesLngName_EN'] : '';
        $C_AirlinesLngPhones_EN 		        = (isset($params['C_AirlinesLngPhones_EN'])) ? $params['C_AirlinesLngPhones_EN'] : '';
        $C_AirlinesLngDictionaryDescription_EN 	= (isset($params['C_AirlinesLngDictionaryDescription_EN'])) ? $params['C_AirlinesLngDictionaryDescription_EN'] : '';
        $C_AirlinesLngName_EL 		            = (isset($params['C_AirlinesLngName_EL'])) ? $params['C_AirlinesLngName_EL'] : '';
        $C_AirlinesLngPhones_EL 		        = (isset($params['C_AirlinesLngPhones_EL'])) ? $params['C_AirlinesLngPhones_EL'] : '';
        $C_AirlinesLngDictionaryDescription_EL 	= (isset($params['C_AirlinesLngDictionaryDescription_EL'])) ? $params['C_AirlinesLngDictionaryDescription_EL'] : '';

        $query = "	UPDATE sys_central_airlineslng SET
						 C_AirlinesLngName 		            = ?
						,C_AirlinesLngPhones 		        = ?
						,C_AirlinesLngDictionaryDescription = ?
						
					WHERE
					C_AirlinesLngC_AirlinesID = ?
					AND
					C_AirlinesLngType = 1
		";

        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("sssi",
             $C_AirlinesLngName_EN
            ,$C_AirlinesLngPhones_EN
            ,$C_AirlinesLngDictionaryDescription_EN
            ,$C_AirlinesLngC_AirlinesID
        );

        if(!$stmt->execute())
            exit("update_Record: error at update : ".$stmt->error);

        $stmt->close();

        $query = "	UPDATE sys_central_airlineslng SET
						 C_AirlinesLngName 		            = ?
						,C_AirlinesLngPhones 		        = ?
						,C_AirlinesLngDictionaryDescription = ?
						
					WHERE
					C_AirlinesLngC_AirlinesID = ?
					AND
					C_AirlinesLngType = 2
		";

        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("sssi",
            $C_AirlinesLngName_EL
            ,$C_AirlinesLngPhones_EL
            ,$C_AirlinesLngDictionaryDescription_EL
            ,$C_AirlinesLngC_AirlinesID
        );

        if(!$stmt->execute())
            exit("update_Record: error at update : ".$stmt->error);

        $stmt->close();

        // return
        $results["success"] = true;
        $results["data"] = $C_AirlinesLngC_AirlinesID;
        return $results;
    }
	
	
}
