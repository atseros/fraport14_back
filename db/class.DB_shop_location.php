<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_shop_location
*
* DESCRIPTION: 
*	Class for table shop_location
*
* table fields:
*
 `LocationID` int(11) NOT NULL,
 `LocationImgFilename` varchar(1024) NOT NULL,
 `LocationPhone` varchar(1024) NOT NULL,
 `LocationEmail` varchar(1024) NOT NULL,
 `LocationShopID` int(11) DEFAULT NULL,
 `LocationLevel` int(11) DEFAULT NULL,
 `LocationLatitude` varchar(1024) DEFAULT NULL,
 `LocationLongitude` varchar(1024) DEFAULT NULL,
 `LocationPriority` int(11) DEFAULT NULL,
*
*********************************************************************************************/
class DB_shop_location
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_location::shop_location_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function shop_location_get_NewRecordDefValues($appFrw, $params)
	{
		$ShopID 	= (int)$params["ShopID"];
		
		$LocationID = DB_sys_kxn::get_NextID($appFrw, 'shop_location');
		
		if($LocationID > 0)
		{
			$results["success"] = true;
			$results["data"]["LocationID"] = $LocationID;
			$results["data"]["LocationShopID"] = $ShopID;
			$results["data"]["LocationLevel"] = 0;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table shop_location";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_location::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $LocationID)
	{
		$query = "	SELECT
						   case when( exists (SELECT LocationID FROM shop_location WHERE LocationID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $LocationID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_location::shop_location_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function shop_location_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$LocationID 	= (int)$params["LocationID"];
		
		if($LocationID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_shop_location::check_RecordExists($appFrw, $LocationID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$LocationID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO shop_location
					(
						 LocationID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $LocationID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_shop_location::shop_location_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_shop_location::shop_location_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function shop_location_getRecord($appFrw, $params)
	{
		$results = array();
		
		$LocationID = (int)$params["LocationID"];
		
		if($LocationID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_shop_location::check_RecordExists($appFrw, $LocationID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$LocationID;
			return $results;
		}
		
		$query = "	SELECT
						
						LocationID
						,LocationShopID
						,LocationLevel
						,LocationLatitude
						,LocationLongitude
						,LocationPriority
							
					FROM shop_location
					WHERE
					LocationID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $LocationID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_shop_location::shop_location_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function shop_location_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$LocationID = (int)$params["LocationID"];
		
		
		if($LocationID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_shop_location::shop_location_getRecord($appFrw, array('LocationID'=>$LocationID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$LocationShopID 	= (isset($params['LocationShopID'])) ? $params['LocationShopID'] : $record['LocationShopID'];
		$LocationPriority 	= (isset($params['LocationPriority'])) ? $params['LocationPriority'] : $record['LocationPriority'];
		// $LocationPhone 		= (isset($params['LocationPhone'])) ? $params['LocationPhone'] : $record['LocationPhone'];
		// $LocationEmail 		= (isset($params['LocationEmail'])) ? $params['LocationEmail'] : $record['LocationEmail'];
		$LocationLatitude 		= (isset($params['LocationLatitude'])) ? $params['LocationLatitude'] : $record['LocationLatitude'];
		$LocationLongitude = (isset($params['LocationLongitude'])) ? $params['LocationLongitude'] : $record['LocationLongitude'];
		$LocationLevel 	= (isset($params['LocationLevel'])) ? $params['LocationLevel'] : $record['LocationLevel'];
		//$LocationImgFilename 	= (isset($params['LocationImgFilename'])) ? $params['LocationImgFilename'] : $record['LocationImgFilename'];
		
		$query = "	UPDATE shop_location SET
							
							LocationShopID 	= ?
							,LocationPriority 	= ?
							,LocationLatitude 	= ?
							,LocationLongitude 	= ?
							,LocationLevel 	= ?
						
							WHERE
							LocationID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iissii",
								 $LocationShopID 			
								,$LocationPriority 			
								// ,$LocationPhone 			
								// ,$LocationEmail 			
								,$LocationLatitude 			
								,$LocationLongitude 			
								,$LocationLevel 			
								// ,$LocationImgFilename 			
								,$LocationID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $LocationID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_location::shop_location_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function shop_location_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$LocationID = (int)$params["LocationID"];
		
		if($LocationID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM shop_location WHERE LocationID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $LocationID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $LocationID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_location::shop_location_getList
	* --------------------------------------------------------------------------
	*/
	public static function shop_location_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$ShopID	= isset($params["ShopID"]) ? (int)$params["ShopID"] : 0;
				
		$query = "	SELECT
						
							LocationID
							,LocationShopID
							,LocationLevel
							,LocationLatitude
							,LocationLongitude
							,LocationPriority
						
							FROM shop_location
							
							WHERE
							(
								LocationShopID = ?
							)
							
							ORDER BY LocationPriority ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $ShopID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			//$row['LocationFullUrl']= DB_shop_location::get_shop_location_FullUrl($appFrw, $row['LocationID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_location::get_shop_location_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_shop_location_FullUrl($appFrw, $LocationID)
	{
		$url = FileManager::getTblFolderUrl("shop_location", $LocationID )."/";
		$FulUrl = "";
		
		$imageData = DB_shop_location::shop_location_getRecord($appFrw,array('LocationID'=>$LocationID));
		if( $imageData["success"] == true  && $imageData["data"]["LocationImgFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["LocationImgFilename"];
		}
		return $FulUrl;
	}
	
}
