<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_slider_imageslng
*
* DESCRIPTION: 
*	Class for table sys_slider_imageslng
*
* table fields:
*
 `SliderImagesLngID` int(11) NOT NULL,
 `SliderImagesLngSliderImagesID` int(11) NOT NULL,
 `SliderImagesLngTitle` varchar(1024) NOT NULL,
 `SliderImagesLngSubtitle` varchar(1024) NOT NULL,
 `SliderImagesLngLinkTitle` varchar(1024) NOT NULL,
 `SliderImagesLngType` int(11) DEFAULT NULL,
 `SliderImagesLngExternalLinkTarget` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_slider_imageslng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_imageslng::sys_slider_imageslng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_slider_imageslng_get_NewRecordDefValues($appFrw, $params)
	{	
		$SliderImagesLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_slider_imageslng');
		
		if($SliderImagesLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["SliderImagesLngID"] = $SliderImagesLngID;
            $results["data"]["SliderImagesLngExternalLinkTarget"] = 0;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_slider_imageslng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_imageslng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SliderImagesLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SliderImagesLngID FROM sys_slider_imageslng WHERE SliderImagesLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SliderImagesLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_imageslng::sys_slider_imageslng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_imageslng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderImagesLngID 	= (int)$params["SliderImagesLngID"];
		
		if($SliderImagesLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_slider_imageslng::check_RecordExists($appFrw, $SliderImagesLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SliderImagesLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_slider_imageslng
					(
						 SliderImagesLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SliderImagesLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_slider_imageslng::sys_slider_imageslng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_imageslng::sys_slider_imageslng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_imageslng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderImagesLngID = (int)$params["SliderImagesLngID"];
		$SliderImagesLngType = $params["SliderImagesLngType"];
		
		if($SliderImagesLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_slider_imageslng::check_RecordExists($appFrw, $SliderImagesLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SliderImagesLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						SliderImagesLngID
						,SliderImagesLngSliderImagesID 
						,SliderImagesLngTitle 
						,SliderImagesLngSubtitle 
						,SliderImagesLngLinkTitle 
						,SliderImagesLngType 
						,SliderImagesLngExternalLink
						,SliderImagesLngExternalLinkTarget
							
					FROM sys_slider_imageslng
					WHERE
					(
						SliderImagesLngID = ?
					)
					AND
					(
						SliderImagesLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $SliderImagesLngID, $SliderImagesLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_imageslng::sys_slider_imageslng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_imageslng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderImagesLngID = (int)$params["SliderImagesLngID"];
		$SliderImagesLngType 			= (isset($params['SliderImagesLngType'])) ? $params['SliderImagesLngType'] : $record['SliderImagesLngType'];
		
		if($SliderImagesLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_slider_imageslng::sys_slider_imageslng_getRecord($appFrw, array('SliderImagesLngID'=>$SliderImagesLngID,'SliderImagesLngType'=>$SliderImagesLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}

		// get param fields
		$SliderImagesLngSliderImagesID 	= (isset($params['SliderImagesLngSliderImagesID'])) ? $params['SliderImagesLngSliderImagesID'] : $record['SliderImagesLngSliderImagesID'];
		$SliderImagesLngTitle 	= (isset($params['SliderImagesLngTitle'])) ? $params['SliderImagesLngTitle'] : $record['SliderImagesLngTitle'];
		$SliderImagesLngSubtitle 	= (isset($params['SliderImagesLngSubtitle'])) ? $params['SliderImagesLngSubtitle'] : $record['SliderImagesLngSubtitle'];
		$SliderImagesLngLinkTitle 	= (isset($params['SliderImagesLngLinkTitle'])) ? $params['SliderImagesLngLinkTitle'] : $record['SliderImagesLngLinkTitle'];
		$SliderImagesLngExternalLink 	= (isset($params['SliderImagesLngExternalLink'])) ? $params['SliderImagesLngExternalLink'] : $record['SliderImagesLngExternalLink'];
		$SliderImagesLngExternalLinkTarget 	= (isset($params['SliderImagesLngExternalLinkTarget'])) ? $params['SliderImagesLngExternalLinkTarget'] : $record['SliderImagesLngExternalLinkTarget'];

		$query = "	UPDATE sys_slider_imageslng SET
						 SliderImagesLngSliderImagesID 					= ?
						,SliderImagesLngType 					= ?
						,SliderImagesLngTitle 					= ?
						,SliderImagesLngSubtitle 			= ?
						,SliderImagesLngLinkTitle 			= ?
						,SliderImagesLngExternalLink		= ?
						,SliderImagesLngExternalLinkTarget  = ?
						
					WHERE
					SliderImagesLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iissssii",
								 $SliderImagesLngSliderImagesID 			
								,$SliderImagesLngType 
								,$SliderImagesLngTitle 
								,$SliderImagesLngSubtitle 
								,$SliderImagesLngLinkTitle 
								,$SliderImagesLngExternalLink
                                ,$SliderImagesLngExternalLinkTarget
								,$SliderImagesLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SliderImagesLngID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::RecordCanBeDeleted
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be checked
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_site WHERE SiteID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SiteID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeLngType	= isset($params["filterLanguage"]) ? (int)$params["filterLanguage"] : 1;
				
		$query = "	SELECT
						
							NodeID
							,NodeSiteID
							,NodeNodeID
							,NodeLevel
							,NodeCode
							,NodeParentCode
							,NodeFullCode
							,NodeTitle
							,NodeUrlAlias
							,NodePublished
							,NodeIsHomePage
							,NodeTemplateFile
							,NodeCreatedAt
							,NodeLngTitle
							,NodeLngStory
							,NodeLngIntro
							,CONCAT (NodeFullCode, ' - ', NodeLngTitle) AS NodeLngTitleFullCode
									
							FROM sys_node
							LEFT JOIN sys_nodelng ON ( NodeLngNodeID = NodeID )
							
							WHERE
							(
								NodeLngType = ?
							)
							
							
							ORDER BY NodeFullCode ASC
							
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $NodeLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_imageslng::sys_slider_imageslng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_imageslng_CheckBeforeInsert($appFrw, $params)
	{
		$SliderImagesLngSliderImagesID	= isset($params["SliderImagesLngSliderImagesID"]) ? (int)$params["SliderImagesLngSliderImagesID"] : 0;
		$SliderImagesLngType	= isset($params["SliderImagesLngType"]) ? (int)$params["SliderImagesLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT SliderImagesLngID FROM sys_slider_imageslng WHERE SliderImagesLngSliderImagesID = ? AND SliderImagesLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $SliderImagesLngSliderImagesID, $SliderImagesLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_imageslng::sys_slider_imageslng_GetSliderImagesLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_imageslng_GetSliderImagesLngID($appFrw, $params)
	{
		$SliderImagesLngSliderImagesID	= isset($params["SliderImagesLngSliderImagesID"]) ? (int)$params["SliderImagesLngSliderImagesID"] : 0;
		$SliderImagesLngType	= isset($params["SliderImagesLngType"]) ? (int)$params["SliderImagesLngType"] : 0;
		
		$query = "	SELECT
						   SliderImagesLngID
						   FROM sys_slider_imageslng
							WHERE SliderImagesLngSliderImagesID = ? AND SliderImagesLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $SliderImagesLngSliderImagesID, $SliderImagesLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["SliderImagesLngID"];
	}
	
	
	
}
