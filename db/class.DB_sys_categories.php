<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_categories
*
* DESCRIPTION: 
*	Class for table sys_categories
*
* table fields:
*
 `CtgID` int(11) NOT NULL,
 `CtgTitle` varchar(1024) NOT NULL,
 `CtgSiteID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_categories
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories::sys_categories_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$CtgID = DB_sys_kxn::get_NextID($appFrw, 'sys_categories');
		
		if($CtgID > 0)
		{
			$results["success"] = true;
			$results["data"]["CtgID"] = $CtgID;
			$results["data"]["CtgSiteID"] = $SiteID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_categories";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $CtgID)
	{
		$query = "	SELECT
						   case when( exists (SELECT CtgID FROM sys_categories WHERE CtgID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CtgID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories::sys_categories_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgID 	= (int)$params["CtgID"];
		
		if($CtgID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_categories::check_RecordExists($appFrw, $CtgID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$CtgID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_categories
					(
						 CtgID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CtgID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_categories::sys_categories_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories::sys_categories_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_getRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgID = (int)$params["CtgID"];
		
		if($CtgID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_categories::check_RecordExists($appFrw, $CtgID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$CtgID;
			return $results;
		}
		
		$query = "	SELECT
						
						CtgID
						,CtgTitle
						,CtgSiteID						
							
					FROM sys_categories
					WHERE
					CtgID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CtgID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories::sys_categories_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgID = (int)$params["CtgID"];
		
		
		if($CtgID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_categories::sys_categories_getRecord($appFrw, array('CtgID'=>$CtgID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$CtgTitle 		= (isset($params['CtgTitle'])) ? $params['CtgTitle'] : $record['CtgTitle'];
		$CtgSiteID 		= (isset($params['CtgSiteID'])) ? $params['CtgSiteID'] : $record['CtgSiteID'];
	
		$query = "	UPDATE sys_categories SET
						 CtgTitle 			= ?
						,CtgSiteID 			= ?
						
					WHERE
					CtgID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("sii",
								 $CtgTitle 			
								,$CtgSiteID 
								,$CtgID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $CtgID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories::sys_categories_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgID = (int)$params["CtgID"];
		
		if($CtgID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_categories WHERE CtgID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CtgID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $CtgID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_get_List
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_get_List($appFrw, $params)
	{
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$ExcludeDefault	= isset($params["ExcludeDefault"]) ? (int)$params["ExcludeDefault"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		
		$results['data'] = array();
		
		if ($ExcludeDefault != 1)
		{
			$dtpRow["CtgID"] = 0;
			$dtpRow["CtgTitle"]	= "No Data type";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -1;
			$dtpRow["CtgTitle"]	= "Container";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -2;
			$dtpRow["CtgTitle"]	= "Flights";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -3;
			$dtpRow["CtgTitle"]	= "Airlines";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -4;
			$dtpRow["CtgTitle"]	= "Shops";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -5;
			$dtpRow["CtgTitle"]	= "Restaurants";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -6;
			$dtpRow["CtgTitle"]	= "Duty Free";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -12;
			$dtpRow["CtgTitle"]	= "Business Lounges";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -7;
			$dtpRow["CtgTitle"]	= "Image Galleries";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -8;
			$dtpRow["CtgTitle"]	= "Video Galleries";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -9;
			$dtpRow["CtgTitle"]	= "Publications";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -10;
			$dtpRow["CtgTitle"]	= "Domestic Destinations";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["CtgID"] = -11;
			$dtpRow["CtgTitle"]	= "International Destinations";
			array_push($results['data'], $dtpRow);
			
			
		}	
		
				
		$query = "	SELECT
						
							CtgID
							,CtgTitle
							,CtgSiteID
						
							FROM sys_categories
							
							WHERE 
							(
								CtgSiteID = ?
							)
							AND
							(
								CtgTitle LIKE ?
							)
							
							ORDER BY CtgTitle ASC
							
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_categories_get_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("is", $SiteID, $filterStrToFind);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_categories_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_categories_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
}
