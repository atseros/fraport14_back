<?php /** @noinspection SqlNoDataSourceInspection */
/** @noinspection SqlResolve */

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_airports
*
* DESCRIPTION: 
*	Class for table sys_airports
*
* INSERT INTO `sys_kxn` (`KxnID`, `KxnInc`, `KxnTableName`, `KxnTableDescr`) VALUES (72, 13000, 'sys_airports', 'Airports');
*
*********************************************************************************************/
class DB_sys_airports
{
    /*
	* --------------------------------------------------------------------------
	* DB_sys_airports::sys_airports_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
    public static function sys_airports_get_NewRecordDefValues($appFrw, $params)
    {
        $AirportID = DB_sys_kxn::get_NextID($appFrw, 'sys_airports');

        if($AirportID > 0) {
            $results["success"] = true;
            $results["data"]["AirportID"] = $AirportID;
        }
        else {
            $results["success"] = false;
            $results["reason"] = "failed to get next id for table sys_airports";
        }

        return $results;
    }

    /*
    * --------------------------------------------------------------------------
    * DB_sys_airports::check_RecordExists
    * --------------------------------------------------------------------------
    */
    public static function check_RecordExists($appFrw, $AirportID)
    {
        $query = "	SELECT
						   case when( exists (SELECT AirportID FROM sys_airports WHERE AirportID = ?))
							then 1
							else 0
						end as RecordExists";
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("i", $AirportID);

        if(!$stmt->execute())
            exit("check_RecordExists: error at select : ".$stmt->error);

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
            exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);

        $row = $result->fetch_assoc();
        $result->close();

        return $row["RecordExists"];
    }

    /*
    * --------------------------------------------------------------------------
    * DB_sys_airports::sys_airports_InsertRecord
    * --------------------------------------------------------------------------
    */
    public static function sys_airports_InsertRecord($appFrw, $params)
    {
        $results = array();

        $AirportID = (int)$params["AirportID"];

        if($AirportID <= 0)
        {
            $results["success"] = false;
            $results["reason"] = "No id found for new record";
            return $results;
        }

        if( DB_sys_airports::check_RecordExists($appFrw, $AirportID) ) {
            $results["success"] = false;
            $results["reason"] = "There is already a record with id = ".$AirportID;
            return $results;
        }

        // get param fields
        $AirportName = (isset($params['AirportName'])) ? $params['AirportName'] : '-';
        $AirportCity = (isset($params['AirportCity'])) ? $params['AirportCity'] : '-';
        $AirportCountry = (isset($params['AirportCountry'])) ? $params['AirportCountry'] : '-';
        $AirportIATA = (isset($params['AirportIATA'])) ? $params['AirportIATA'] : '-';
        $AirportICAO = (isset($params['AirportICAO'])) ? $params['AirportICAO'] : '-';
        $AirportLatitude = (isset($params['AirportLatitude'])) ? $params['AirportLatitude'] : '-';
        $AirportLongitude = (isset($params['AirportLongitude'])) ? $params['AirportLongitude'] : '-';
        $AirportAltitude = (isset($params['AirportAltitude'])) ? $params['AirportAltitude'] : '-';
        $AirportTimezone = (isset($params['AirportTimezone'])) ? $params['AirportTimezone'] : '-';
        $AirportDST = (isset($params['AirportDST'])) ? $params['AirportDST'] : '-';
        $AirportTzDbTimeZone = (isset($params['AirportTzDbTimeZone'])) ? $params['AirportTzDbTimeZone'] : '-';
        $AirportType = (isset($params['AirportType'])) ? $params['AirportType'] : '-';
        $AirportSource = (isset($params['AirportSource'])) ? $params['AirportSource'] : '-';
        $AirportFraSHORTNAME = (isset($params['AirportFraSHORTNAME'])) ? $params['AirportFraSHORTNAME'] : '-';
        $AirportFraEN = (isset($params['AirportFraEN'])) ? $params['AirportFraEN'] : '-';
        $AirportFraGR = (isset($params['AirportFraGR'])) ? $params['AirportFraGR'] : '-';
        $AirportCountryLatitude = (isset($params['AirportCountryLatitude'])) ? $params['AirportCountryLatitude'] : '-';
        $AirportCountryLongitude = (isset($params['AirportCountryLongitude'])) ? $params['AirportCountryLongitude'] : '-';


        // insert an empty record
        $query = "	INSERT INTO sys_airports
					(
						 AirportID
                        ,AirportName
                        ,AirportCity
                        ,AirportCountry
                        ,AirportIATA
                        ,AirportICAO
                        ,AirportLatitude
                        ,AirportLongitude
                        ,AirportAltitude
                        ,AirportTimezone
                        ,AirportDST
                        ,AirportTzDbTimeZone
                        ,AirportType
                        ,AirportSource
                        ,AirportFraSHORTNAME
                        ,AirportFraEN
                        ,AirportFraGR
                        ,AirportCountryLatitude
                        ,AirportCountryLongitude
					)
					VALUES
					(
						 ?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
                        ,?
					)";
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ssssssssssssssssssi",
            $AirportID
            ,$AirportName
            ,$AirportCity
            ,$AirportCountry
            ,$AirportIATA
            ,$AirportICAO
            ,$AirportLatitude
            ,$AirportLongitude
            ,$AirportAltitude
            ,$AirportTimezone
            ,$AirportDST
            ,$AirportTzDbTimeZone
            ,$AirportType
            ,$AirportSource
            ,$AirportFraSHORTNAME
            ,$AirportFraEN
            ,$AirportFraGR
            ,$AirportCountryLatitude
            ,$AirportCountryLongitude
        );

        if(!$stmt->execute())
            exit("insert_Record: error at inert : ".$stmt->error);

        $stmt->close();

        // return
        $results["success"] = true;
        $results["data"] = $AirportID;
        return $results;
    }

    /*
    * --------------------------------------------------------------------------
    * DB_sys_airports::sys_airports_getRecord
    * --------------------------------------------------------------------------
    */
    public static function sys_airports_getRecord($appFrw, $params)
    {
        $results = array();

        $AirportID = (int)$params["AirportID"];

        if($AirportID <= 0) {
            $results["success"] = false;
            $results["reason"] = "No id found. Can not get record";
            return $results;
        }

        // Check if record exists
        if( !DB_sys_airports::check_RecordExists($appFrw, $AirportID) )
        {
            $results["success"] = false;
            $results["reason"] = "There is no  record with id = ".$AirportID;
            return $results;
        }

        $query = "	SELECT * FROM sys_airports WHERE AirportID = ? ";

        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("i", $AirportID);

        if(!$stmt->execute())
            exit("get_Record: error at select : ".$stmt->error);

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
            exit("get_Record: error at select : ".$stmt->error);

        $row = $result->fetch_assoc();
        $result->close();


        // return results
        $results["success"] = true;
        $results["data"] = $row;

        return $results;
    }

    /*
    * --------------------------------------------------------------------------
    * DB_sys_airports::sys_airports_UpdateRecord
    * --------------------------------------------------------------------------
    */
    public static function sys_airports_UpdateRecord($appFrw, $params)
    {
        $results = array();

        $AirportID = (int)$params["AirportID"];


        if($AirportID <= 0)
        {
            $results["success"] = false;
            $results["reason"] = "update_Record: No id found. Can not update record";
            return $results;
        }

        // get already saved values
        $tmp_record = DB_sys_airports::sys_airports_getRecord($appFrw, array('AirportID'=>$AirportID));
        if($tmp_record["success"] == true) {
            $record = $tmp_record["data"];
        }
        else {
            $results["success"] = false;
            $results["reason"] = $tmp_record["reason"];
            return $results;
        }

        // get param fields
        $AirportName = (isset($params['AirportName'])) ? $params['AirportName'] : $record['AirportName'];
        $AirportCity = (isset($params['AirportCity'])) ? $params['AirportCity'] : $record['AirportCity'];
        $AirportCountry = (isset($params['AirportCountry'])) ? $params['AirportCountry'] : $record['AirportCountry'];
        $AirportIATA = (isset($params['AirportIATA'])) ? $params['AirportIATA'] : $record['AirportIATA'];
        $AirportICAO = (isset($params['AirportICAO'])) ? $params['AirportICAO'] : $record['AirportICAO'];
        $AirportLatitude = (isset($params['AirportLatitude'])) ? $params['AirportLatitude'] : $record['AirportLatitude'];
        $AirportLongitude = (isset($params['AirportLongitude'])) ? $params['AirportLongitude'] : $record['AirportLongitude'];
        $AirportAltitude = (isset($params['AirportAltitude'])) ? $params['AirportAltitude'] : $record['AirportAltitude'];
        $AirportTimezone = (isset($params['AirportTimezone'])) ? $params['AirportTimezone'] : $record['AirportTimezone'];
        $AirportDST = (isset($params['AirportDST'])) ? $params['AirportDST'] : $record['AirportDST'];
        $AirportTzDbTimeZone = (isset($params['AirportTzDbTimeZone'])) ? $params['AirportTzDbTimeZone'] : $record['AirportTzDbTimeZone'];
        $AirportType = (isset($params['AirportType'])) ? $params['AirportType'] : $record['AirportType'];
        $AirportSource = (isset($params['AirportSource'])) ? $params['AirportSource'] : $record['AirportSource'];
        $AirportFraSHORTNAME = (isset($params['AirportFraSHORTNAME'])) ? $params['AirportFraSHORTNAME'] : $record['AirportFraSHORTNAME'];
        $AirportFraEN = (isset($params['AirportFraEN'])) ? $params['AirportFraEN'] : $record['AirportFraEN'];
        $AirportFraGR = (isset($params['AirportFraGR'])) ? $params['AirportFraGR'] : $record['AirportFraGR'];
        $AirportCountryLatitude = (isset($params['AirportCountryLatitude'])) ? $params['AirportCountryLatitude'] : $record['AirportCountryLatitude'];
        $AirportCountryLongitude = (isset($params['AirportCountryLongitude'])) ? $params['AirportCountryLongitude'] : $record['AirportCountryLongitude'];


        $query = "	
            UPDATE sys_airports SET
                 AirportName = ?
                ,AirportCity = ?
                ,AirportCountry = ?
                ,AirportIATA = ?
                ,AirportICAO = ?
                ,AirportLatitude = ?
                ,AirportLongitude = ?
                ,AirportAltitude = ?
                ,AirportTimezone = ?
                ,AirportDST = ?
                ,AirportTzDbTimeZone = ?
                ,AirportType = ?
                ,AirportSource = ?
                ,AirportFraSHORTNAME = ?
                ,AirportFraEN = ?
                ,AirportFraGR = ?
                ,AirportCountryLatitude = ?
                ,AirportCountryLongitude = ?
            WHERE
                AirportID = ?
		";

        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ssssssssssssssssssi",
             $AirportName
            ,$AirportCity
            ,$AirportCountry
            ,$AirportIATA
            ,$AirportICAO
            ,$AirportLatitude
            ,$AirportLongitude
            ,$AirportAltitude
            ,$AirportTimezone
            ,$AirportDST
            ,$AirportTzDbTimeZone
            ,$AirportType
            ,$AirportSource
            ,$AirportFraSHORTNAME
            ,$AirportFraEN
            ,$AirportFraGR
            ,$AirportCountryLatitude
            ,$AirportCountryLongitude
            ,$AirportID
        );

        if(!$stmt->execute())
            exit("update_Record: error at update : ".$stmt->error);

        $stmt->close();

        // return
        $results["success"] = true;
        $results["data"] = $AirportID;
        return $results;
    }

    /*
    * --------------------------------------------------------------------------
    * DB_sys_airports::sys_airports_get_List
    * --------------------------------------------------------------------------
    */
    public static function sys_airports_get_List($appFrw, $params)
    {
        $results = array();

        $maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
        $filterStrToFind = isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
        $SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : -1;

        $query = "
            SELECT
                AirportID,
                AirportName, 
                AirportCity, 
                AirportCountry, 
                AirportIATA, 
                AirportICAO, 
                AirportLatitude, 
                AirportLongitude, 
                AirportAltitude,
                AirportTimezone, 
                AirportDST, 
                AirportTzDbTimeZone, 
                AirportType, 
                AirportSource, 
                AirportFraSHORTNAME, 
                AirportFraEN, 
                AirportFraGR, 
                AirportCountryLatitude, 
                AirportCountryLongitude
            FROM sys_airports
            WHERE
            (
                AirportName LIKE ?
                OR 
                AirportCity LIKE ?
                OR 
                AirportCountry LIKE ?
            )   
            ORDER BY AirportName ASC
		";

        if($maxRecords)
            $query .= " LIMIT ".$maxRecords;

        //exit($query);
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt) {
            $results["success"] = false;
            $results["reason"] = "get_List: error at prepare statement: ".$appFrw->DB_Link->error;
            return $results;
        }

        $stmt->bind_param("sss", $filterStrToFind, $filterStrToFind, $filterStrToFind);

        if(!$stmt->execute()) {
            $results["success"] = false;
            $results["reason"] = "get_List: error at select: ".$stmt->error;
            return $results;
        }

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result) {
            $results["success"] = false;
            $results["reason"] = "get_List: error at select: ".$stmt->error;
            return $results;
        }

        $results['data'] = array();

        while( $row = $result->fetch_assoc()) {
            // $row['C_AirlinesFullUrl'] = DB_sys_airports::get_sys_airports_FullUrl($appFrw, $row['AirportID']);
            // $row['C_AirlinesFullUrlLogo'] = DB_sys_airports::get_sys_airports_FullUrlLogo($appFrw, $row['AirportID']);
            // $row['C_AirlinesFlightsLogo'] = DB_sys_airports::get_sys_airports_FlightsLogo($appFrw, $row['AirportID']);
            array_push($results['data'], $row);
        }
        $result->close();

        // return results
        $results["success"] = true;
        return $results;
    }

    /*
	* --------------------------------------------------------------------------
	* DB_sys_airports::sys_airports_get_ExportList
	* --------------------------------------------------------------------------
	*/
    public static function sys_airports_get_ExportList($appFrw, $params)
    {
        $results = array();

        $query = "	
            SELECT
                AirportID,
                AirportName, 
                AirportCity, 
                AirportCountry, 
                AirportIATA, 
                AirportICAO, 
                AirportLatitude, 
                AirportLongitude, 
                AirportAltitude,
                AirportTimezone, 
                AirportDST, 
                AirportTzDbTimeZone, 
                AirportType, 
                AirportSource, 
                AirportFraSHORTNAME, 
                AirportFraEN, 
                AirportFraGR, 
                AirportCountryLatitude, 
                AirportCountryLongitude
            FROM sys_airports
		";//exit($query);
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt) {
            $results["success"] = false;
            $results["reason"] = "get_List: error at prepare statement: ".$appFrw->DB_Link->error;
            return $results;
        }

        if(!$stmt->execute()) {
            $results["success"] = false;
            $results["reason"] = "get_List: error at select: ".$stmt->error;
            return $results;
        }

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
        {
            $results["success"] = false;
            $results["reason"] = "get_List: error at select: ".$stmt->error;
            return $results;
        }

        $results['data'] = array();

        while( $row = $result->fetch_assoc())
        {
            array_push($results['data'], $row);
        }
        $result->close();


        // return results
        $results["success"] = true;

        return $results;
    }

}
