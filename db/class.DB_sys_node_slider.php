<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_node_slider
*
* DESCRIPTION: 
*	Class for table sys_node_slider
*
* table fields:
*
 `NodeSliderID` int(11) NOT NULL,
 `NodeSliderNodeID` int(11) NOT NULL,
 `NodeSliderSliderID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_node_slider
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::sys_node_slider_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_slider_get_NewRecordDefValues($appFrw, $params)
	{	
		$NodeSliderID = DB_sys_kxn::get_NextID($appFrw, 'sys_node_slider');
		
		if($NodeSliderID > 0)
		{
			$results["success"] = true;
			$results["data"]["NodeSliderID"] = $NodeSliderID;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_node_slider";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $NodeSliderID)
	{
		$query = "	SELECT
						   case when( exists (SELECT NodeSliderID FROM sys_node_slider WHERE NodeSliderID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeSliderID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::sys_node_slider_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_slider_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeSliderID 	= (int)$params["NodeSliderID"];
		
		if($NodeSliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_node_slider::check_RecordExists($appFrw, $NodeSliderID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$NodeSliderID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_node_slider
					(
						 NodeSliderID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeSliderID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_node_slider::sys_node_slider_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::sys_node_slider_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_slider_getRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeSliderID = (int)$params["NodeSliderID"];
		
		if($NodeSliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_node_slider::check_RecordExists($appFrw, $NodeSliderID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$NodeSliderID;
			return $results;
		}
		
		$query = "	SELECT
						
						NodeSliderID
						,NodeSliderNodeID
						,NodeSliderSliderID
							
					FROM sys_node_slider
					WHERE
					NodeSliderID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeSliderID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::sys_node_slider_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_slider_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeSliderID = (int)$params["NodeSliderID"];
		
		
		if($NodeSliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_node_slider::sys_node_slider_getRecord($appFrw, array('NodeSliderID'=>$NodeSliderID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$NodeSliderNodeID 	= (isset($params['NodeSliderNodeID'])) ? $params['NodeSliderNodeID'] : $record['NodeSliderNodeID'];
		$NodeSliderSliderID 	= (isset($params['NodeSliderSliderID'])) ? $params['NodeSliderSliderID'] : $record['NodeSliderSliderID'];
		
		$query = "	UPDATE sys_node_slider SET
							
							NodeSliderNodeID 	= ?
							,NodeSliderSliderID 	= ?
						
							WHERE
							NodeSliderID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iii",
								 $NodeSliderNodeID 			
								,$NodeSliderSliderID 																
								,$NodeSliderID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $NodeSliderID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::sys_node_slider_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_slider_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeSliderID = (int)$params["NodeSliderID"];
		
		if($NodeSliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_node_slider WHERE NodeSliderID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeSliderID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $NodeSliderID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::sys_node_slider_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_slider_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeID	= isset($params["NodeID"]) ? (int)$params["NodeID"] : 0;
				
		$query = "	SELECT

							NodeSliderID
							,NodeSliderNodeID
							,NodeSliderSliderID
							,SliderTitle
						
							FROM sys_node_slider
							LEFT JOIN sys_slider ON ( SliderID = NodeSliderSliderID )
							LEFT JOIN sys_node ON ( NodeID = NodeSliderNodeID )
							
							WHERE
							(
								NodeSliderNodeID  = ?
							)
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i",$NodeID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
		/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::sys_node_slider_getListSitePages
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_slider_getListSitePages($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeID	= isset($params["NodeID"]) ? (int)$params["NodeID"] : 0;
		$NodeLngType	= isset($params["NodeLngType"]) ? (int)$params["NodeLngType"] : -1;
				
		$query = "	SELECT

							NodeMediaID
							,NodeMediaNodeID
							,NodeMediaMediaID
							,NodeMediaPriority
							,MediaID
							,MediaLngTitle as MediaTitle
							,MediaFilename
							,MediaType
							,MediaSiteID
						
							FROM sys_node_media
							LEFT JOIN sys_media ON ( MediaID = NodeMediaMediaID )
							LEFT JOIN sys_node ON ( NodeID = NodeMediaNodeID )
							LEFT JOIN sys_medialng ON ( MediaLngMediaID = MediaID )
							
							WHERE
							(
								MediaType LIKE ?
							)
							AND
							(
								NodeMediaNodeID  = ?
							)
							AND
							(
								MediaLngType = ?
							)
							AND
							(
								MediaLngTitle != ''
							)
							
							

							ORDER BY NodeMediaPriority ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("iii", $MediaType,$NodeID,$MediaLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
	
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_slider::sys_node_slider_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_slider_CheckBeforeInsert($appFrw, $params)
	{
		$NodeSliderNodeID	= isset($params["NodeSliderNodeID"]) ? (int)$params["NodeSliderNodeID"] : 0;
		$NodeSliderSliderID	= isset($params["NodeSliderSliderID"]) ? (int)$params["NodeSliderSliderID"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT NodeSliderID FROM sys_node_slider WHERE NodeSliderNodeID = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeSliderNodeID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
}
