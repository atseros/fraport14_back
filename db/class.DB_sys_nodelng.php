<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_nodelng
*
* DESCRIPTION: 
*	Class for table sys_nodelng
*
* table fields:
*
 `NodeLngID` int(11) NOT NULL,
 `NodeLngNodeID` int(11) DEFAULT NULL,
 `NodeLngType` int(11) DEFAULT NULL,
 `NodeLngTitle` varchar(1024) DEFAULT NULL,
 `NodeLngStory` text,
 `NodeLngIntro` text,
*
*********************************************************************************************/
class DB_sys_nodelng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_nodelng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_nodelng_get_NewRecordDefValues($appFrw, $params)
	{	
		$NodeLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_nodelng');
		
		if($NodeLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["NodeLngID"] = $NodeLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_node";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $NodeLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT NodeLngID FROM sys_nodelng WHERE NodeLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_nodelng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_nodelng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeLngID 	= (int)$params["NodeLngID"];
		
		if($NodeLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_nodelng::check_RecordExists($appFrw, $NodeLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$NodeLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_nodelng
					(
						 NodeLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_nodelng::sys_nodelng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_nodelng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_nodelng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeLngID = (int)$params["NodeLngID"];
		$NodeLngType = $params["NodeLngType"];
		
		if($NodeLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_nodelng::check_RecordExists($appFrw, $NodeLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$NodeLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						NodeLngID
						,NodeLngNodeID 
						,NodeLngType 
						,NodeLngTitle 
						,NodeLngStory 
						,NodeLngIntro
						,NodeLngMetaTitle
						,NodeLngMetaKeywords
						,NodeLngMetaDescription
						
							
					FROM sys_nodelng
					WHERE
					(
						NodeLngID = ?
					)
					AND
					(
						NodeLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $NodeLngID, $NodeLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_nodelng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_nodelng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeLngID = (int)$params["NodeLngID"];
		$NodeLngType 			= (isset($params['NodeLngType'])) ? $params['NodeLngType'] : $record['NodeLngType'];
		
		if($NodeLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_nodelng::sys_nodelng_getRecord($appFrw, array('NodeLngID'=>$NodeLngID,'NodeLngType'=>$NodeLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$NodeLngNodeID 		= (isset($params['NodeLngNodeID'])) ? $params['NodeLngNodeID'] : $record['NodeLngNodeID'];
		$NodeLngTitle 			= (isset($params['NodeLngTitle'])) ? $params['NodeLngTitle'] : $record['NodeLngTitle'];
		$NodeLngStory 			= (isset($params['NodeLngStory'])) ? $params['NodeLngStory'] : $record['NodeLngStory'];
		$NodeLngIntro 			= (isset($params['NodeLngIntro'])) ? $params['NodeLngIntro'] : $record['NodeLngIntro'];
		$NodeLngMetaTitle 			= (isset($params['NodeLngMetaTitle'])) ? $params['NodeLngMetaTitle'] : $record['NodeLngMetaTitle'];
		$NodeLngMetaKeywords 	= (isset($params['NodeLngMetaKeywords'])) ? $params['NodeLngMetaKeywords'] : $record['NodeLngMetaKeywords'];
		$NodeLngMetaDescription = (isset($params['NodeLngMetaDescription'])) ? $params['NodeLngMetaDescription'] : $record['NodeLngMetaDescription'];
		
		
		$query = "	UPDATE sys_nodelng SET
						 NodeLngNodeID 					= ?
						,NodeLngType 					= ?
						,NodeLngTitle 					= ?
						,NodeLngStory 			= ?
						,NodeLngIntro 				= ?
						,NodeLngMetaTitle 				= ?
						,NodeLngMetaKeywords 				= ?
						,NodeLngMetaDescription 				= ?
						
					WHERE
					NodeLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iissssssi",
								 $NodeLngNodeID 			
								,$NodeLngType 
								,$NodeLngTitle 
								,$NodeLngStory 
								,$NodeLngIntro 
								,$NodeLngMetaTitle 
								,$NodeLngMetaKeywords 
								,$NodeLngMetaDescription 
								,$NodeLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $NodeLngID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::RecordCanBeDeleted
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be checked
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_site WHERE SiteID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SiteID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeLngType	= isset($params["filterLanguage"]) ? (int)$params["filterLanguage"] : 1;
				
		$query = "	SELECT
						
							NodeID
							,NodeSiteID
							,NodeNodeID
							,NodeLevel
							,NodeCode
							,NodeParentCode
							,NodeFullCode
							,NodeTitle
							,NodeUrlAlias
							,NodePublished
							,NodeIsHomePage
							,NodeTemplateFile
							,NodeCreatedAt
							,NodeLngTitle
							,NodeLngStory
							,NodeLngIntro
							,CONCAT (NodeFullCode, ' - ', NodeLngTitle) AS NodeLngTitleFullCode
									
							FROM sys_node
							LEFT JOIN sys_nodelng ON ( NodeLngNodeID = NodeID )
							
							WHERE
							(
								NodeLngType = ?
							)
							
							
							ORDER BY NodeFullCode ASC
							
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $NodeLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getTemplateList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getTemplateList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
				
		$query = "	SELECT
						
							TplID
							,TplName
							
									
							FROM sys_tpl
							
							ORDER BY TplID ASC
							
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		//$stmt->bind_param("i", $NodeLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_nodelng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_nodelng_CheckBeforeInsert($appFrw, $params)
	{
		$NodeLngNodeID	= isset($params["NodeLngNodeID"]) ? (int)$params["NodeLngNodeID"] : 0;
		$NodeLngType	= isset($params["NodeLngType"]) ? (int)$params["NodeLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT NodeLngID FROM sys_nodelng WHERE NodeLngNodeID = ? AND NodeLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $NodeLngNodeID, $NodeLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_nodelng_GetNodeLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_nodelng_GetNodeLngID($appFrw, $params)
	{
		$NodeLngNodeID	= isset($params["NodeLngNodeID"]) ? (int)$params["NodeLngNodeID"] : 0;
		$NodeLngType	= isset($params["NodeLngType"]) ? (int)$params["NodeLngType"] : 0;
		
		$query = "	SELECT
						   NodeLngID
						   FROM sys_nodelng
							WHERE NodeLngNodeID = ? AND NodeLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $NodeLngNodeID, $NodeLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["NodeLngID"];
	}
	
	
	
}
