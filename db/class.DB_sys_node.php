<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");
require_once(realpath(__DIR__)."/class.DB_sys_nodelng.php");

/*********************************************************************************************
* CLASS DB_sys_node
*
* DESCRIPTION: 
*	Class for table sys_site
*
* table fields:
*
 `NodeID` int(11) NOT NULL,
 `NodeSiteID` int(11) DEFAULT NULL,
 `NodeNodeID` int(11) DEFAULT NULL,
 `NodeLevel` int(11) DEFAULT NULL,
 `NodeCode` int(11) NOT NULL DEFAULT '0',
 `NodeParentCode` varchar(1024) DEFAULT NULL,
 `NodeFullCode` varchar(1024) DEFAULT NULL,
 `NodeTitle` varchar(1024) DEFAULT NULL,
 `NodeUrlAlias` varchar(128) DEFAULT NULL,
 `NodePublished` int(11) NOT NULL DEFAULT '0',
 `NodeIsHomePage` int(11) NOT NULL DEFAULT '0',
 `NodeTemplateFile` varchar(1024) DEFAULT NULL,
 `NodeCreatedAt` date NOT NULL,
*
*********************************************************************************************/
class DB_sys_node
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_get_NewRecordDefValues($appFrw, $params)
	{	
		$NodeID = DB_sys_kxn::get_NextID($appFrw, 'sys_node');
		
		$NodeNodeID = isset($params['NodeNodeID']) ? (int)$params['NodeNodeID'] : 0;
		
		if($NodeNodeID > 0)
		{
			$NodeParentDetails = DB_sys_node::sys_node_getParentTitle($appFrw, array('NodeID'=>$NodeNodeID));		
			$NodeParent = $NodeParentDetails["data"]["NodeTitle"];
			$NodeParentCode = $NodeParentDetails["data"]["NodeFullCode"];
			$NodeDetails = DB_sys_node::sys_node_getMaxCode($appFrw, $NodeNodeID);			
			$NodeCode = $NodeDetails["data"]["NodeCode"] + 1;
			
		}
		else
		{
			$NodeParent = "-";
			$NodeParentCode = 0;
			$NodeDetails = DB_sys_node::sys_node_getMaxCode($appFrw, $NodeNodeID);			
			$NodeCode = $NodeDetails["data"]["NodeCode"] + 1;
			
		}
		
		if($NodeID > 0)
		{
			$results["success"] = true;
			$results["data"]["NodeID"] = $NodeID;
			$results["data"]["NodeParent"] = $NodeParent;
			$results["data"]["NodeParentCode"] = $NodeParentCode;
			$results["data"]["NodeCode"] = $NodeCode;
			$results["data"]["NodeIsFlight"] = 0;
			$results["data"]["NodeIsFlightDetailed"] = 0;
			$results["data"]["NodeIsAirline"] = 0;
			$results["data"]["NodeIsAirlineDetailed"] = 0;
			$results["data"]["NodeIsShop"] = 0;
			$results["data"]["NodeIsShopDetailed"] = 0;
			$results["data"]["NodeIsCategory"] = 0;
			$results["data"]["NodeIsCategoryDetailed"] = 0;
			$results["data"]["NodeIsRestaurant"] = 0;
			$results["data"]["NodeIsDutyFree"] = 0;
			$results["data"]["NodeIsDarkSite"] = 0;
			$results["data"]["NodeDataType"] = 0;
			$results["data"]["NodeIsImageGallery"] = 0;
			$results["data"]["NodeIsImageGalleryDetailed"] = 0;
			$results["data"]["NodeIsVideoGallery"] = 0;
			$results["data"]["NodeIsVideoGalleryDetailed"] = 0;
			$results["data"]["NodeIsPublications"] = 0;
			$results["data"]["NodeIsPublicationsDetailed"] = 0;
			$results["data"]["NodeIsSearchPage"] = 0;
			$results["data"]["NodeIsDomesticDestinations"] = 0;
			$results["data"]["NodeIsInternationalDestinations"] = 0;
			$results["data"]["NodeIsDestinations"] = 0;
			$results["data"]["NodeIsWeatherPage"] = 0;
			$results["data"]["NodeTemplateFile"] = 'No Template';
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_node";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $NodeID)
	{
		$query = "	SELECT
						   case when( exists (SELECT NodeID FROM sys_node WHERE NodeID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeID 	= (int)$params["NodeID"];
		
		if($NodeID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_node::check_RecordExists($appFrw, $NodeID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$NodeID;
			return $results;
		}
		
		$NodeCreatedAt 			= (isset($params['NodeCreatedAt'])) ? $params['NodeCreatedAt'] : $record['NodeCreatedAt'];
		$NodeCreatedAt   		= DateTime::createFromFormat('d/m/Y', $NodeCreatedAt)->format('Y-m-d');
		
			
		// insert an empty record
		$query = "	INSERT INTO sys_node
					(
						 NodeID
						 ,NodeCreatedAt

					)
					VALUES
					(
						 ?
						 ,?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("is", $NodeID, $NodeCreatedAt);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_node::sys_node_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeID = (int)$params["NodeID"];
		
		$NodeNodeID	= isset($params["NodeNodeID"]) ? (int)$params["NodeNodeID"] : 0;
		
		
		if($NodeID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_node::check_RecordExists($appFrw, $NodeID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$NodeID;
			return $results;
		}
		
		$query = "	SELECT
						
						NodeID
						,NodeSiteID 
						,NodeNodeID 
						,NodeLevel 
						,NodeCode 
						,NodeParentCode 
						,NodeFullCode 
						,NodeTitle 
						,NodeUrlAlias 
						,NodePublished 
						,NodeIsHomePage 
						,NodeTemplateFile 
						,NodeCreatedAt
						,CONCAT (NodeFullCode, ' - ', NodeTitle) AS NodeTitleFullCode
						,(SELECT NodeTitle FROM sys_node WHERE NodeID = ?) as NodeParent	
						,NodeMenu
						,NodeFullUrlAlias
						,NodeIsFlight
						,NodeIsFlightDetailed
						,NodeIsAirline
						,NodeIsAirlineDetailed
						,NodeIsShop
						,NodeIsShopDetailed
						,NodeIsDarkSite
						,NodeDataType
						,NodeViewType
						,NodeIsCategory
						,NodeIsCategoryDetailed
						,NodeIsRestaurant
						,NodeIsDutyFree
						,CtgTitle as NodeDataTypeDescr
						,NodeIsImageGallery
						,NodeIsImageGalleryDetailed
						,NodeIsVideoGallery
						,NodeIsVideoGalleryDetailed
						,NodeIsPublications
						,NodeIsPublicationsDetailed
						,NodeIsSearchPage
						,NodeIsWeatherPage
						,NodeIsDestinations
						,NodeIsDomesticDestinations
						,NodeIsInternationalDestinations
						
					FROM sys_node
					LEFT JOIN sys_categories ON (CtgID = NodeDataType)
							
					WHERE
					(
						NodeID = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $NodeNodeID, $NodeID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		
		if($row["NodeParent"] == NULL)
			$row["NodeParent"] ="-";
		
		if($row['NodeDataType']==0)
				$row['NodeDataTypeDescr'] = 'No Data type';
			else if($row['NodeDataType']==-1)
				$row['NodeDataTypeDescr'] = 'Container';
			else if($row['NodeDataType']==-2)
				$row['NodeDataTypeDescr'] = 'Flights';
			else if($row['NodeDataType']==-3)
				$row['NodeDataTypeDescr'] = 'Airlines';
			else if($row['NodeDataType']==-4)
				$row['NodeDataTypeDescr'] = 'Shops';
			else if($row['NodeDataType']==-5)
				$row['NodeDataTypeDescr'] = 'Restaurants';
			else if($row['NodeDataType']==-6)
				$row['NodeDataTypeDescr'] = 'Duty Free';
			else if($row['NodeDataType']==-7)
				$row['NodeDataTypeDescr'] = 'Image Gallery';
			else if($row['NodeDataType']==-8)
				$row['NodeDataTypeDescr'] = 'Video Gallery';
			else if($row['NodeDataType']==-9)
				$row['NodeDataTypeDescr'] = 'Publications';
			else if($row['NodeDataType']==-10)
				$row['NodeDataTypeDescr'] = 'Domestic Destinations';
			else if($row['NodeDataType']==-11)
				$row['NodeDataTypeDescr'] = 'International Destinations';
			else if($row['NodeDataType']==-12)
				$row['NodeDataTypeDescr'] = 'Business Lounges';
			
		if($row['NodeViewType']==1)
				$row['NodeViewTypeDescr'] = 'Big Image';
			else if($row['NodeViewType']==3)
				$row['NodeViewTypeDescr'] = 'Grid';
			else if($row['NodeViewType']==4)
				$row['NodeViewTypeDescr'] = 'Grid - 5';
			else if($row['NodeViewType']==5)
				$row['NodeViewTypeDescr'] = 'List with Dictionary';
			else if($row['NodeViewType']==6)
				$row['NodeViewTypeDescr'] = 'List with Photo';
			else if($row['NodeViewType']==7)
				$row['NodeViewTypeDescr'] = 'Irregular List with Photo';
			else if($row['NodeViewType']==8)
				$row['NodeViewTypeDescr'] = 'Table';
			else
				$row['NodeViewTypeDescr'] = '-';	
		
		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
	


	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getParentTitle
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getParentTitle($appFrw, $params)
	{
		$results = array();
		
		$NodeID = (int)$params["NodeID"];
		
		
		if($NodeID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_node::check_RecordExists($appFrw, $NodeID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$NodeID;
			return $results;
		}
		
		$query = "	SELECT
					
						NodeTitle
						,NodeFullCode						
							
					FROM sys_node
							
					WHERE
					(
						NodeID = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeID = (int)$params["NodeID"];
		
		
		if($NodeID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_node::sys_node_getRecord($appFrw, array('NodeID'=>$NodeID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$NodeSiteID 				= (isset($params['NodeSiteID'])) ? $params['NodeSiteID'] : $record['NodeSiteID'];
		$NodeNodeID 				= (isset($params['NodeNodeID'])) ? $params['NodeNodeID'] : $record['NodeNodeID'];
		$NodeLevel 				= (isset($params['NodeLevel'])) ? $params['NodeLevel'] : $record['NodeLevel'];
		$NodeCode 				= (isset($params['NodeCode'])) ? $params['NodeCode'] : $record['NodeCode'];
		$NodeParentCode 		= (isset($params['NodeParentCode'])) ? $params['NodeParentCode'] : $record['NodeParentCode'];
		$NodeFullCode 			=  str_pad($NodeParentCode, 4, '0', STR_PAD_LEFT) . '.' . str_pad($NodeCode, 4, '0', STR_PAD_LEFT) ;
		$NodeTitle 				= (isset($params['NodeTitle'])) ? $params['NodeTitle'] : $record['NodeTitle'];
		$NodeUrlAlias 			= (isset($params['NodeUrlAlias'])) ? $params['NodeUrlAlias'] : $record['NodeUrlAlias'];
		$NodePublished 			= (isset($params['NodePublished'])) ? $params['NodePublished'] : $record['NodePublished'];
		$NodeIsHomePage 		= (isset($params['NodeIsHomePage'])) ? $params['NodeIsHomePage'] : $record['NodeIsHomePage'];
		$NodeTemplateFile 		= (isset($params['NodeTemplateFile'])) ? $params['NodeTemplateFile'] : $record['NodeTemplateFile'];
		$NodeMenu 				= (isset($params['NodeMenu'])) ? $params['NodeMenu'] : $record['NodeMenu'];
		$NodeIsFlight 			= (isset($params['NodeIsFlight'])) ? $params['NodeIsFlight'] : $record['NodeIsFlight'];
		$NodeIsFlightDetailed 	= (isset($params['NodeIsFlightDetailed'])) ? $params['NodeIsFlightDetailed'] : $record['NodeIsFlightDetailed'];
		$NodeIsAirline 			= (isset($params['NodeIsAirline'])) ? $params['NodeIsAirline'] : $record['NodeIsAirline'];
		$NodeIsAirlineDetailed = (isset($params['NodeIsAirlineDetailed'])) ? $params['NodeIsAirlineDetailed'] : $record['NodeIsAirlineDetailed'];
		$NodeIsShop 				= (isset($params['NodeIsShop'])) ? $params['NodeIsShop'] : $record['NodeIsShop'];
		$NodeIsShopDetailed 	= (isset($params['NodeIsShopDetailed'])) ? $params['NodeIsShopDetailed'] : $record['NodeIsShopDetailed'];
		$NodeIsDarkSite 		= (isset($params['NodeIsDarkSite'])) ? $params['NodeIsDarkSite'] : $record['NodeIsDarkSite'];
		$NodeDataType 			= (isset($params['NodeDataType'])) ? $params['NodeDataType'] : $record['NodeDataType'];
		$NodeViewType 			= (isset($params['NodeViewType'])) ? $params['NodeViewType'] : $record['NodeViewType'];
		$NodeIsCategory 		= (isset($params['NodeIsCategory'])) ? $params['NodeIsCategory'] : $record['NodeIsCategory'];
		$NodeIsCategoryDetailed = (isset($params['NodeIsCategoryDetailed'])) ? $params['NodeIsCategoryDetailed'] : $record['NodeIsCategoryDetailed'];
		$NodeIsRestaurant = (isset($params['NodeIsRestaurant'])) ? $params['NodeIsRestaurant'] : $record['NodeIsRestaurant'];
		$NodeIsDutyFree = (isset($params['NodeIsDutyFree'])) ? $params['NodeIsDutyFree'] : $record['NodeIsDutyFree'];
		$NodeIsImageGallery = (isset($params['NodeIsImageGallery'])) ? $params['NodeIsImageGallery'] : $record['NodeIsImageGallery'];
		$NodeIsImageGalleryDetailed = (isset($params['NodeIsImageGalleryDetailed'])) ? $params['NodeIsImageGalleryDetailed'] : $record['NodeIsImageGalleryDetailed'];
		$NodeIsVideoGallery = (isset($params['NodeIsVideoGallery'])) ? $params['NodeIsVideoGallery'] : $record['NodeIsVideoGallery'];
		$NodeIsVideoGalleryDetailed = (isset($params['NodeIsVideoGalleryDetailed'])) ? $params['NodeIsVideoGalleryDetailed'] : $record['NodeIsVideoGalleryDetailed'];
		$NodeIsPublications = (isset($params['NodeIsPublications'])) ? $params['NodeIsPublications'] : $record['NodeIsPublications'];
		$NodeIsPublicationsDetailed = (isset($params['NodeIsPublicationsDetailed'])) ? $params['NodeIsPublicationsDetailed'] : $record['NodeIsPublicationsDetailed'];
		$NodeIsSearchPage = (isset($params['NodeIsSearchPage'])) ? $params['NodeIsSearchPage'] : $record['NodeIsSearchPage'];
		$NodeIsDomesticDestinations = (isset($params['NodeIsDomesticDestinations'])) ? $params['NodeIsDomesticDestinations'] : $record['NodeIsDomesticDestinations'];
		$NodeIsInternationalDestinations = (isset($params['NodeIsInternationalDestinations'])) ? $params['NodeIsInternationalDestinations'] : $record['NodeIsInternationalDestinations'];
		$NodeIsWeatherPage = (isset($params['NodeIsWeatherPage'])) ? $params['NodeIsWeatherPage'] : $record['NodeIsWeatherPage'];
		$NodeIsDestinations = (isset($params['NodeIsDestinations'])) ? $params['NodeIsDestinations'] : $record['NodeIsDestinations'];
		
		if(isset($params['NodeIsFlight']) && $params['NodeIsFlight'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsFlightExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsFlight');
			
			if($NodeIsFlightExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a flight page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsFlightDetailed']) && $params['NodeIsFlightDetailed'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsFlightDetailedExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsFlightDetailed');
			
			if($NodeIsFlightDetailedExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a flight detailed page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsAirline']) && $params['NodeIsAirline'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsAirlineExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsAirline');
			
			if($NodeIsAirlineExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already an airline page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsAirlineDetailed']) && $params['NodeIsAirlineDetailed'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsAirlineDetailedExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsAirlineDetailed');
			
			if($NodeIsAirlineDetailedExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already an airline detailed page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsShopDetailed']) && $params['NodeIsShopDetailed'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsShopDetailedExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsShopDetailed');
			
			if($NodeIsShopDetailedExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a shop detailed page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsShop']) && $params['NodeIsShop'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsShopExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsShop');
			
			if($NodeIsShopExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a shop page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsCategory']) && $params['NodeIsCategory'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsCategoryExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsCategory');
			
			if($NodeIsCategoryExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a category page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsCategoryDetailed']) && $params['NodeIsCategoryDetailed'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsCategoryDetailedExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsCategoryDetailed');
			
			if($NodeIsCategoryDetailedExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a category detailed page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsRestaurant']) && $params['NodeIsRestaurant'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsRestaurantExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsRestaurant');
			
			if($NodeIsRestaurantExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a restaurant page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsDutyFree']) && $params['NodeIsDutyFree'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsDutyFreeExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsDutyFree');
			
			if($NodeIsDutyFreeExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a duty free page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsImageGallery']) && $params['NodeIsImageGallery'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsImageGalleryExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsImageGallery');
			
			if($NodeIsImageGalleryExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already an image gallery page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsImageGalleryDetailed']) && $params['NodeIsImageGalleryDetailed'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsImageGalleryDetailedExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsImageGalleryDetailed');
			
			if($NodeIsImageGalleryDetailedExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already an image gallery detailed page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsVideoGallery']) && $params['NodeIsVideoGallery'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsVideoGalleryExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsVideoGallery');
			
			if($NodeIsVideoGalleryExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a video gallery page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsVideoGalleryDetailed']) && $params['NodeIsVideoGalleryDetailed'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsVideoGalleryDetailedExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsVideoGalleryDetailed');
			
			if($NodeIsVideoGalleryDetailedExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a video gallery detailed page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsPublications']) && $params['NodeIsPublications'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsPublicationsExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsPublications');
			
			if($NodeIsPublicationsExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a publications page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsPublicationsDetailed']) && $params['NodeIsPublicationsDetailed'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsPublicationsDetailedExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsPublicationsDetailed');
			
			if($NodeIsPublicationsDetailedExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a publications detailed page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsSearchPage']) && $params['NodeIsSearchPage'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsSearchPageExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsSearchPage');
			
			if($NodeIsSearchPageExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a search page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsDomesticDestinations']) && $params['NodeIsDomesticDestinations'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsDomesticDestinationsExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsDomesticDestinations');
			
			if($NodeIsDomesticDestinationsExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a domestic destinations page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsInternationalDestinations']) && $params['NodeIsInternationalDestinations'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsInternationalDestinationsExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsInternationalDestinations');
			
			if($NodeIsInternationalDestinationsExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a inetrnational destinations page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsWeatherPage']) && $params['NodeIsWeatherPage'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsWeatherPageExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsWeatherPage');
			
			if($NodeIsWeatherPageExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a weather page.";
				return $results;
			}
		}
		
		if(isset($params['NodeIsDarkSite']) && $params['NodeIsDarkSite'] == 1)
		{
			// DB_sys_node::sys_node_PageTypeExists
			$NodeIsDarkSiteExists = DB_sys_node::sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, 'NodeIsDarkSite');
			
			if($NodeIsDarkSiteExists == 1)
			{
				$results["success"]	= false;
				$results["reason"]		= "sys_node_UpdateRecord: There is already a dark site page.";
				return $results;
			}
		}
		
		if (isset($params['NodeIsHomePage']) && $params['NodeIsHomePage'] ==1)
		{
			// DB_sys_node::sys_node_ResetHomePage
			DB_sys_node::sys_node_ResetHomePage($appFrw, $NodeID, $NodeSiteID);
		}	
		
		$query = "	UPDATE sys_node SET
						 NodeSiteID 					= ?
						,NodeNodeID 					= ?
						,NodeLevel 					= ?
						,NodeCode 					= ?
						,NodeParentCode 			= ?
						,NodeFullCode 				= ?
						,NodeTitle 					  	= ?
						,NodeUrlAlias 					= ?
						,NodePublished 				= ?
						,NodeIsHomePage 			= ?
						,NodeTemplateFile 			= ?
						,NodeMenu 					= ?
						,NodeIsFlight 					= ?
						,NodeIsFlightDetailed 		= ?
						,NodeIsAirline 				= ?
						,NodeIsAirlineDetailed 	= ?
						,NodeIsShop 					= ?
						,NodeIsShopDetailed 		= ?
						,NodeIsDarkSite		 		= ?
						,NodeDataType		 		= ?
						,NodeViewType		 		= ?
						,NodeIsCategory		 		= ?
						,NodeIsCategoryDetailed	= ?
						,NodeIsRestaurant		 	= ?
						,NodeIsDutyFree		 	= ?
						,NodeIsImageGallery		 	= ?
						,NodeIsImageGalleryDetailed		 	= ?
						,NodeIsVideoGallery		 	= ?
						,NodeIsVideoGalleryDetailed		 	= ?
						,NodeIsPublications		 	= ?
						,NodeIsPublicationsDetailed		 	= ?
						,NodeIsSearchPage				= ?
						,NodeIsWeatherPage	= ?
						,NodeIsDestinations	= ?
						,NodeIsDomesticDestinations	= ?
						,NodeIsInternationalDestinations	= ?
						
					WHERE
					NodeID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiiissssiisiiiiiiiiiiiiiiiiiiiiiiiiii",
								 $NodeSiteID 			
								,$NodeNodeID 
								,$NodeLevel 
								,$NodeCode 
								,$NodeParentCode 
								,$NodeFullCode 
								,$NodeTitle 
								,$NodeUrlAlias 
								,$NodePublished 
								,$NodeIsHomePage 
								,$NodeTemplateFile 
								,$NodeMenu 
								,$NodeIsFlight 
								,$NodeIsFlightDetailed 
								,$NodeIsAirline 
								,$NodeIsAirlineDetailed 
								,$NodeIsShop 
								,$NodeIsShopDetailed 
								,$NodeIsDarkSite 
								,$NodeDataType 
								,$NodeViewType 
								,$NodeIsCategory 
								,$NodeIsCategoryDetailed 
								,$NodeIsRestaurant 
								,$NodeIsDutyFree 
								,$NodeIsImageGallery 
								,$NodeIsImageGalleryDetailed 
								,$NodeIsVideoGallery 
								,$NodeIsVideoGalleryDetailed 
								,$NodeIsPublications 
								,$NodeIsPublicationsDetailed 
								,$NodeIsSearchPage
								,$NodeIsWeatherPage
								,$NodeIsDestinations
								,$NodeIsDomesticDestinations
								,$NodeIsInternationalDestinations
								,$NodeID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();	
		
		// DB_sys_node::set_sys_node_NodeFullUrlAlias
		DB_sys_node::set_sys_node_NodeFullUrlAlias($appFrw, $NodeID);
		
		// return
		$results["success"] = true;
		$results["data"] = $NodeID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::set_sys_node_NodeFullUrlAlias
	* --------------------------------------------------------------------------
	*/
	public static function set_sys_node_NodeFullUrlAlias($appFrw, $NodeID)
	{
		if(!$NodeID)
			return;
		
		$tmp_record = DB_sys_node::sys_node_getRecord($appFrw, array('NodeID'=>$NodeID));
		
		if($tmp_record["success"] == false)
			return;

		$NodeRecord = $tmp_record["data"];

		// update node record NodeFullUrlAlias
		if( $NodeRecord["NodeLevel"] == 0)
		{
			$sel_query = "	SELECT
								NodeUrlAlias as NodeFullUrlAlias
							FROM sys_node
							WHERE
							NodeID = ?";
							
			
		}
		else if( $NodeRecord["NodeLevel"] == 1)
		{
			$sel_query = "	SELECT
								CONCAT(level0.NodeUrlAlias, '/' ,level1.NodeUrlAlias)  as NodeFullUrlAlias
							FROM sys_node as level1
							LEFT JOIN sys_node as level0 ON(level0.NodeID = level1.NodeNodeID)
							WHERE
							level1.NodeID = ?";
		}
		else if( $NodeRecord["NodeLevel"] == 2)
		{
			$sel_query = "	SELECT
								CONCAT(level0.NodeUrlAlias, '/' ,level1.NodeUrlAlias , '/', level2.NodeUrlAlias)  as NodeFullUrlAlias
							FROM sys_node as level2
							LEFT JOIN sys_node as level1 ON(level1.NodeID = level2.NodeNodeID)
							LEFT JOIN sys_node as level0 ON(level0.NodeID = level1.NodeNodeID)
							WHERE
							level2.NodeID = ?";			
		}
		else if( $NodeRecord["NodeLevel"] == 3)
		{
			$sel_query = "	SELECT
								CONCAT(level0.NodeUrlAlias, '/' ,level1.NodeUrlAlias , '/', level2.NodeUrlAlias, '/', level3.NodeUrlAlias)  as NodeFullUrlAlias
							FROM sys_node as level3
							LEFT JOIN sys_node as level2 ON(level2.NodeID = level3.NodeNodeID)
							LEFT JOIN sys_node as level1 ON(level1.NodeID = level2.NodeNodeID)
							LEFT JOIN sys_node as level0 ON(level0.NodeID = level1.NodeNodeID)
							WHERE
							level3.NodeID = ?";			
		}
		else if( $NodeRecord["NodeLevel"] == 4)
		{
			$sel_query = "	SELECT
								CONCAT(level0.NodeUrlAlias, '/' ,level1.NodeUrlAlias , '/', level2.NodeUrlAlias, '/', level3.NodeUrlAlias, '/', level4.NodeUrlAlias)  as NodeFullUrlAlias
							FROM sys_node as level4
							LEFT JOIN sys_node as level3 ON(level3.NodeID = level4.NodeNodeID)
							LEFT JOIN sys_node as level2 ON(level2.NodeID = level3.NodeNodeID)
							LEFT JOIN sys_node as level1 ON(level1.NodeID = level2.NodeNodeID)
							LEFT JOIN sys_node as level0 ON(level0.NodeID = level1.NodeNodeID)
							WHERE
							level4.NodeID = ?";			
		}
		else if( $NodeRecord["NodeLevel"] == 5)
		{
			$sel_query = "	SELECT
								CONCAT(level0.NodeUrlAlias, '/' ,level1.NodeUrlAlias , '/', level2.NodeUrlAlias, '/', level3.NodeUrlAlias, '/', level4.NodeUrlAlias, '/', level5.NodeUrlAlias)  as NodeFullUrlAlias
							FROM sys_node as level5
							LEFT JOIN sys_node as level4 ON(level4.NodeID = level5.NodeNodeID)
							LEFT JOIN sys_node as level3 ON(level3.NodeID = level4.NodeNodeID)
							LEFT JOIN sys_node as level2 ON(level2.NodeID = level3.NodeNodeID)
							LEFT JOIN sys_node as level1 ON(level1.NodeID = level2.NodeNodeID)
							LEFT JOIN sys_node as level0 ON(level0.NodeID = level1.NodeNodeID)
							WHERE
							level5.NodeID = ?";			
		}
		else if( $NodeRecord["NodeLevel"] == 6)
		{
			$sel_query = "	SELECT
								CONCAT(level0.NodeUrlAlias, '/' ,level1.NodeUrlAlias , '/', level2.NodeUrlAlias, '/', level3.NodeUrlAlias, '/', level4.NodeUrlAlias, '/', level5.NodeUrlAlias, '/', level6.NodeUrlAlias)  as NodeFullUrlAlias
							FROM sys_node as level6
							LEFT JOIN sys_node as level5 ON(level5.NodeID = level6.NodeNodeID)
							LEFT JOIN sys_node as level4 ON(level4.NodeID = level5.NodeNodeID)
							LEFT JOIN sys_node as level3 ON(level3.NodeID = level4.NodeNodeID)
							LEFT JOIN sys_node as level2 ON(level2.NodeID = level3.NodeNodeID)
							LEFT JOIN sys_node as level1 ON(level1.NodeID = level2.NodeNodeID)
							LEFT JOIN sys_node as level0 ON(level0.NodeID = level1.NodeNodeID)
							WHERE
							level6.NodeID = ?";			
		}
		
		$stmt = $appFrw->DB_Link->prepare($sel_query);
		
		if(!$stmt) exit("Error at set_sys_node_NodeFullUrlAlias: ".$NodeID." ".$appFrw->DB_Link->error." - ".$sel_query);
		
		$stmt->bind_param("i", $NodeID);
		$stmt->execute();
		
		$result = $stmt->get_result();
		
		$stmt->close();
		
		$row = $result->fetch_assoc();
		$NodeFullUrlAlias = $row['NodeFullUrlAlias'];
			
		// UPDATE
		$query = "UPDATE sys_node SET NodeFullUrlAlias = ? WHERE NodeID = ?";
				
		$stmt = $appFrw->DB_Link->prepare($query);
		if(!$stmt) exit("Error at set_sys_node_NodeFullUrlAlias: ".$NodeID." ".$appFrw->DB_Link->error." - ".$query);

		// bind params
		$stmt->bind_param("si",$NodeFullUrlAlias,  $NodeID);

		// execute - get results
		$stmt->execute();
		$stmt->close();
		
						
		// update node children
		$tmp_ret = DB_sys_node::get_sys_node_ChildrenList($appFrw, array( "NodeID" => $NodeID ));
		if( $tmp_ret["success"] == true)
		{
			$children = $tmp_ret["data"];
			foreach( $children as $child)
			{
				DB_sys_node::set_sys_node_NodeFullUrlAlias($appFrw, $child["NodeID"] );
			}
		}
		
	}
	
	/****************************************************************************************
	* DB_sys_node::get_sys_node_ChildrenList
	****************************************************************************************/
	public static function get_sys_node_ChildrenList($appFrw, $params)
	{
		$NodeID = (int) $params["NodeID"];
		
		$results = array();
		
		$query = "
			SELECT
				 NodeID
			FROM sys_node 
			WHERE 
			(
				NodeNodeID = ?
			)
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) exit("Error at get_sys_node_ChildrenList: ".$NodeID." ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeID);
		$stmt->execute();
		$result = $stmt->get_result();
		
		
		if(!$result)
		{
			$results["success"]	= false;
			$results["reason"]	= "get_sys_node_ChildrenList: error at select: ".$stmt->error;
			return $results;
		}
		
		$stmt->close();
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		// return results
		$results["success"] = true;
		
		return $results;
		
		
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_ResetHomePage
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_ResetHomePage($appFrw, $NodeID, $NodeSiteID)
	{
		$results = array();
		
		$NodeIsHomePage = 0;
		
		
		if($NodeID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "resetHomePage: No id found. Can not reset records";
			return $results;
		}	

		$query = "	UPDATE sys_node SET
						 
						 NodeIsHomePage = ?
						
					WHERE
					NodeSiteID = ? AND NodeID != ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iii",
								 $NodeIsHomePage 			
								,$NodeSiteID 	 							
								,$NodeID	
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $NodeID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$NodeID = (int)$params["NodeID"];
		if($NodeID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT NodeID FROM sys_node WHERE NodeNodeID = ?))
							then 1
							else 0
						 end as NodeExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["NodeExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are child nodes related to this node";
			
			return $results;
		}	
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeID = (int)$params["NodeID"];
		
		if($NodeID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		//check if can be deleted
		$canBeDeleted = DB_sys_node::RecordCanBeDeleted($appFrw, array('NodeID'=>$NodeID) );
		if($canBeDeleted["success"] == false)
		{
			$results["success"] = false;
			$results["reason"] = $canBeDeleted["reason"];
			
			return $results;
		}
		
		$query = "DELETE FROM sys_node WHERE NodeID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();	

		$query = "DELETE FROM sys_nodelng WHERE NodeLngNodeID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $NodeID;
		return $results;		
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_MoveRecord
	*
	* Direction is 1 for moving up
	*
	* Direction is 2 for moving down
	*
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_MoveRecord($appFrw, $params)
	{
		$NodeID	= isset($params["NodeID"]) ? (int)$params["NodeID"] : 0;
		$Direction	= isset($params["Direction"]) ? (int)$params["Direction"] : 0;
		
		if($NodeID <= 0)
		{
			$results["success"]	= false;
			$results["reason"]		= "No id found. Can not delete record";
			return $results;
		}
		
		if($Direction <= 0)
		{
			$results["success"]	= false;
			$results["reason"]		= "No direction selected. Can not move record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_node::sys_node_getRecord($appFrw, array('NodeID'=>$NodeID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		$NodeCode			= $record['NodeCode'];
		$NodeParentCode	= $record['NodeParentCode'];
		$NodeNodeID		= $record['NodeNodeID'];
		
		if(($Direction == 1) && ($NodeCode == 1))
		{
			$results["success"] = false;
			$results["reason"] = "Can not move record up";
			return $results;
		}
		
		if($Direction == 1)
		{
			$NewNodeCode = $NodeCode-1;
		}
		if($Direction == 2)
		{
			$NewNodeCode = $NodeCode+1;
		}
		
		//find sibling
		$query = "	SELECT
								 NodeID
								,NodeCode								
							FROM sys_node							
							WHERE
							(
								NodeNodeID = ?
							)
							AND
							(
								NodeCode = ?
							)
							ORDER BY NodeCode ASC
							
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("ii", $NodeNodeID, $NewNodeCode);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at select: ".$stmt->error;
			return $results;
		}
		
		$row = $result->fetch_assoc();
		$result->close();
		
		if(($Direction == 2) && empty($row['NodeID']))
		{
			$results["success"] = false;
			$results["reason"] = "Can not move record down";
			return $results;
		}
		
		$SiblingNodeID		= $row['NodeID'];
		$SiblingNodeCode	= $row['NodeCode'];
		
		//update my NodeCode, NodeFullCode
		$NodeFullCode	=  str_pad($NodeParentCode, 4, '0', STR_PAD_LEFT) . '.' . str_pad($NewNodeCode, 4, '0', STR_PAD_LEFT) ;
		
		$query = "	UPDATE sys_node SET
								 NodeCode			= ?
								,NodeFullCode	= ?
							WHERE NodeID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("sys_node_MoveRecord: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("isi",
								 $NewNodeCode 			
								,$NodeFullCode
								,$NodeID								
						);
		
		if(!$stmt->execute()) 
			exit("sys_node_MoveRecord: error at update : ".$stmt->error);
		
		$stmt->close();
		
		//update sibling NodeCode, NodeFullCode
		if($Direction == 1)
		{
			$NewSiblingNodeCode = $SiblingNodeCode+1;
		}
		if($Direction == 2)
		{
			$NewSiblingNodeCode = $SiblingNodeCode-1;
		}
		
		$SiblingNodeFullCode	=  str_pad($NodeParentCode, 4, '0', STR_PAD_LEFT) . '.' . str_pad($NewSiblingNodeCode, 4, '0', STR_PAD_LEFT) ;
		
		$query = "	UPDATE sys_node SET
								 NodeCode			= ?
								,NodeFullCode	= ?
							WHERE NodeID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("sys_node_MoveRecord: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("isi",
								 $NewSiblingNodeCode 			
								,$SiblingNodeFullCode
								,$SiblingNodeID								
						);
		
		if(!$stmt->execute()) 
			exit("sys_node_MoveRecord: error at update : ".$stmt->error);
		
		$stmt->close();
		
		//update my Children NodeParentCode, NodeFullCode
		$query = "	SELECT
								 NodeID							
							FROM sys_node							
							WHERE NodeNodeID = ?
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $NodeID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at select: ".$stmt->error;
			return $results;
		}
		
		$children['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($children['data'], $row);
		}
		
		$result->close();
		
		$update_child_max_number = 0;
		
		foreach($children['data'] as $each_child)
		{
			$ChildNodeID = $each_child['NodeID'];
			
			$move_child_params = array(
													'NodeID'								=> $ChildNodeID,
													'NodeParentCode'					=> $NodeFullCode,
													'update_child_max_number'	=>$update_child_max_number,
												);
			
			$sys_node_MoveChildRecord = DB_sys_node::sys_node_MoveChildRecord($appFrw, $move_child_params);
		}
		
		//update sibling Children NodeParentCode, NodeFullCode
		$query = "	SELECT
								 NodeID							
							FROM sys_node							
							WHERE NodeNodeID = ?
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $SiblingNodeID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_MoveRecord: error at select: ".$stmt->error;
			return $results;
		}
		
		$silbling_children['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($silbling_children['data'], $row);
		}
		
		$result->close();
		
		$update_sibling_child_max_number = 0;
		
		foreach($silbling_children['data'] as $each_silbling_child)
		{
			$SiblingChildNodeID = $each_silbling_child['NodeID'];
			
			$move_child_params = array(
													'NodeID'								=> $SiblingChildNodeID,
													'NodeParentCode'					=> $SiblingNodeFullCode,
													'update_child_max_number'	=> $update_sibling_child_max_number,
												);
			
			$sys_node_MoveChildRecord = DB_sys_node::sys_node_MoveChildRecord($appFrw, $move_child_params);
		}
		
		// return
		$results["success"]	= true;
		$results["data"]			= $NodeID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_MoveChildRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_MoveChildRecord($appFrw, $params)
	{
		$update_child_max_number = $params['update_child_max_number'];
		$update_child_max_number++;
		
		if($update_child_max_number < 20000)
		{
			$NodeID				= (int)$params['NodeID'];
			$NodeParentCode	= $params['NodeParentCode'];
			
			//find my NodeCode
			$query = "	SELECT
									 NodeCode								
								FROM sys_node							
								WHERE NodeID = ?
			";

			//exit($query);
			$stmt = $appFrw->DB_Link->prepare($query);
			
			if(!$stmt) 
			{
				$results["success"] = false;
				$results["reason"] = "sys_node_MoveChildRecord: error at prepare statement: ".$appFrw->DB_Link->error;
				return $results;
			}
			
			$stmt->bind_param("i", $NodeID);
			
			if(!$stmt->execute()) 
			{
				$results["success"] = false;
				$results["reason"] = "sys_node_MoveChildRecord: error at select: ".$stmt->error;
				return $results;
			}
			
			$result = $stmt->get_result();
			$stmt->close();			
									
			if(!$result)
			{
				$results["success"] = false;
				$results["reason"] = "sys_node_MoveChildRecord: error at select: ".$stmt->error;
				return $results;
			}
			
			$row = $result->fetch_assoc();
			$result->close();
			
			$NodeCode = $row['NodeCode'];
			
			$NodeFullCode	=  $NodeParentCode . '.' . str_pad($NodeCode, 4, '0', STR_PAD_LEFT) ;
			
			//exit();
			$query = "	UPDATE sys_node SET
									 NodeParentCode	= ?
									,NodeFullCode		= ?
								WHERE NodeID = ?
			";
			
			$stmt = $appFrw->DB_Link->prepare($query);
			
			if(!$stmt) 
				exit("sys_node_MoveChildRecord: error at prepare statement: ".$appFrw->DB_Link->error);
			
			$stmt->bind_param("ssi",
									 $NodeParentCode
									,$NodeFullCode
									,$NodeID								
							);
			
			if(!$stmt->execute()) 
				exit("sys_node_MoveChildRecord: error at update : ".$stmt->error);
			
			$stmt->close();
			
			//update my Children NodeParentCode, NodeFullCode
			$query = "	SELECT
									 NodeID							
								FROM sys_node							
								WHERE NodeNodeID = ?
			";

			//exit($query);
			$stmt = $appFrw->DB_Link->prepare($query);
			
			if(!$stmt) 
			{
				$results["success"] = false;
				$results["reason"] = "sys_node_MoveChildRecord: error at prepare statement: ".$appFrw->DB_Link->error;
				return $results;
			}
			
			$stmt->bind_param("i", $NodeID);
			
			if(!$stmt->execute()) 
			{
				$results["success"] = false;
				$results["reason"] = "sys_node_MoveChildRecord: error at select: ".$stmt->error;
				return $results;
			}
			
			$result = $stmt->get_result();
			$stmt->close();			
									
			if(!$result)
			{
				$results["success"] = false;
				$results["reason"] = "sys_node_MoveChildRecord: error at select: ".$stmt->error;
				return $results;
			}
			
			$children1['data'] = array();
			
			while( $row = $result->fetch_assoc())
			{
				array_push($children1['data'], $row);
			}
			
			$result->close();
			
			foreach($children1['data'] as $each_child1)
			{
				$ChildNodeID = $each_child1['NodeID'];
				
				$move_child_params = array(
														'NodeID'								=> $ChildNodeID,
														'NodeParentCode'					=> $NodeFullCode,
														'update_child_max_number'	=>$update_child_max_number,
													);
				
				$sys_node_MoveChildRecord = DB_sys_node::sys_node_MoveChildRecord($appFrw, $move_child_params);
			}
			
			// return
			$results["success"]	= true;
			$results["data"]			= $NodeID;
			return $results;
		}
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getListForCategories
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getListForCategories($appFrw, $params)
	{
		$results = array();

		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
				
		$query = "	SELECT
						
							NodeID
							,NodeSiteID
							,NodeNodeID
							,NodeLevel
							,NodeCode
							,NodeParentCode
							,NodeFullCode
							,NodeTitle
							,NodeUrlAlias
							,NodePublished
							,NodeIsHomePage
							,NodeTemplateFile
							,NodeCreatedAt
							,NodeMenu
							,CONCAT (NodeFullCode, ' - ', NodeTitle) AS NodeTitleFullCode
							,NodeIsFlight
							,NodeIsFlightDetailed
							,NodeIsAirline
							,NodeIsAirlineDetailed
							,NodeIsShop
							,NodeIsShopDetailed
							,NodeIsDarkSite
							,NodeViewType
							,NodeIsCategory
							,NodeIsCategoryDetailed
							,NodeIsRestaurant
							,NodeIsDutyFree
							,NodeDataType
							,NodeIsImageGallery
							,NodeIsImageGalleryDetailed
							,NodeIsVideoGallery
							,NodeIsVideoGalleryDetailed
							,NodeIsPublications
							,NodeIsPublicationsDetailed
							
							FROM sys_node
							
							WHERE
							(
								NodeSiteID = ?
							)
							AND
							(
								NodeIsFlight != 1
							)
							AND
							(
								NodeIsFlightDetailed != 1
							)
							AND
							(
								NodeIsAirline != 1
							)
							AND
							(
								NodeIsAirlineDetailed != 1
							)
							AND
							(
								NodeIsShop != 1
							)
							AND
							(
								NodeIsShopDetailed != 1
							)
							AND
							(
								NodeIsDarkSite != 1
							)
							AND
							(
								NodeIsCategory != 1
							)
							AND
							(
								NodeIsCategoryDetailed != 1
							)
							AND
							(
								NodeIsRestaurant != 1
							)
							AND
							(
								NodeIsDutyFree != 1
							)
							AND
							(
								NodeIsHomePage != 1
							)
							AND
							(
								NodeIsImageGallery != 1
							)
							AND
							(
								NodeIsImageGalleryDetailed != 1
							)
							AND
							(
								NodeIsVideoGallery != 1
							)
							AND
							(
								NodeIsVideoGalleryDetailed != 1
							)
							AND
							(
								NodeIsPublications != 1
							)
							AND
							(
								NodeIsPublicationsDetailed != 1
							)
							
							
							ORDER BY NodeFullCode ASC
							
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_getList: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_getList: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_getList: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getList($appFrw, $params)
	{
		$results = array();

		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? $params["filterStrToFind"] : "";
		
		$RelatedLinkID	= isset($params["RelatedLinkID"]) ? (int)$params["RelatedLinkID"] : 0;
		$SliderID			= isset($params["SliderID"]) ? (int)$params["SliderID"] : 0;
		$MediaID			= isset($params["MediaID"]) ? (int)$params["MediaID"] : 0;
				
		$query = "	SELECT
						
							NodeID
							,NodeSiteID
							,NodeNodeID
							,NodeLevel
							,NodeCode
							,NodeParentCode
							,NodeFullCode
							,NodeTitle
							,NodeUrlAlias
							,NodePublished
							,NodeIsHomePage
							,NodeTemplateFile
							,NodeCreatedAt
							,NodeMenu
							,CONCAT (NodeFullCode, ' - ', NodeTitle) AS NodeTitleFullCode
							,NodeIsFlight
							,NodeIsFlightDetailed
							,NodeIsAirline
							,NodeIsAirlineDetailed
							,NodeIsShop
							,NodeIsShopDetailed
							,NodeIsDarkSite
							,NodeViewType
							,NodeIsCategory
							,NodeIsCategoryDetailed
							,NodeIsRestaurant
							,NodeIsDutyFree
							,CASE 
								WHEN $RelatedLinkID > 0
								THEN (SELECT NodeRelatedLinkID FROM sys_node_related_link WHERE NodeRelatedLinkRelatedLinkID = $RelatedLinkID AND NodeRelatedLinkNodeID = NodeID LIMIT 1)
								WHEN $SliderID > 0
								THEN (SELECT NodeSliderID FROM sys_node_slider WHERE NodeSliderSliderID = $SliderID AND NodeSliderNodeID = NodeID LIMIT 1)
								WHEN $MediaID > 0
								THEN (SELECT NodeMediaID FROM sys_node_media WHERE NodeMediaMediaID = $MediaID AND NodeMediaNodeID = NodeID LIMIT 1)	
							END as RowHighlight
							,NodeIsImageGallery
							,NodeIsImageGalleryDetailed
							,NodeIsVideoGallery
							,NodeIsVideoGalleryDetailed
							,NodeIsPublications
							,NodeIsPublicationsDetailed
							,NodeIsSearchPage
							,NodeIsWeatherPage
							,NodeIsDestinations
							,NodeIsDomesticDestinations
							,NodeIsInternationalDestinations
							,NodeDataType
							
							FROM sys_node
							
							WHERE
							(
								NodeSiteID = ?
							)
							AND
							(
								NodeTitle LIKE '%".$filterStrToFind."%'
							)
							ORDER BY NodeFullCode ASC
							
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_getList: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_getList: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_node_getList: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			if($row['NodeIsFlight']==1)
				$row['NodeSpecial'] = 'Flight List';
			else if($row['NodeIsFlightDetailed']==1)
				$row['NodeSpecial'] = 'Flight Detailed';
			else if($row['NodeIsAirline']==1)
				$row['NodeSpecial'] = 'Airline List';
			else if($row['NodeIsAirlineDetailed']==1)
				$row['NodeSpecial'] = 'Airline Detailed';
			else if($row['NodeIsShop']==1)
				$row['NodeSpecial'] = 'Shop List';
			else if($row['NodeIsShopDetailed']==1)
				$row['NodeSpecial'] = 'Shop Detailed';
			else if($row['NodeIsDarkSite']==1)
				$row['NodeSpecial'] = 'Dark Site';
			else if($row['NodeIsCategory']==1)
				$row['NodeSpecial'] = 'General Category';
			else if($row['NodeIsCategoryDetailed']==1)
				$row['NodeSpecial'] = 'General Category Detailed';
			else if($row['NodeIsRestaurant']==1)
				$row['NodeSpecial'] = 'Restaurant List';
			else if($row['NodeIsDutyFree']==1)
				$row['NodeSpecial'] = 'Duty Free List';
			else if($row['NodeIsImageGallery']==1)
				$row['NodeSpecial'] = 'Image Gallery List';
			else if($row['NodeIsImageGalleryDetailed']==1)
				$row['NodeSpecial'] = 'Image Gallery Detailed';
			else if($row['NodeIsVideoGallery']==1)
				$row['NodeSpecial'] = 'Video Gallery List';
			else if($row['NodeIsVideoGalleryDetailed']==1)
				$row['NodeSpecial'] = 'Video Gallery Detailed';
			else if($row['NodeIsPublications']==1)
				$row['NodeSpecial'] = 'Publications List';
			else if($row['NodeIsPublicationsDetailed']==1)
				$row['NodeSpecial'] = 'Publications Detailed';
			else if($row['NodeIsSearchPage']==1)
				$row['NodeSpecial'] = 'Search Page';
			else if($row['NodeIsDomesticDestinations']==1)
				$row['NodeSpecial'] = 'Domestic Destinations';
			else if($row['NodeIsInternationalDestinations']==1)
				$row['NodeSpecial'] = 'International Destinations';
			else if($row['NodeIsDestinations']==1)
				$row['NodeSpecial'] = 'Destinations';
			else if($row['NodeIsWeatherPage']==1)
				$row['NodeSpecial'] = 'Weather Page';
			else
				$row['NodeSpecial'] = '-';
			
			if($row['NodeViewType']==1)
				$row['NodeViewType'] = 'Big Image';
			else if($row['NodeViewType']==3)
				$row['NodeViewType'] = 'Grid';
			else if($row['NodeViewType']==4)
				$row['NodeViewType'] = 'Grid - 5';
			else if($row['NodeViewType']==5)
				$row['NodeViewType'] = 'List with Dictionary';
			else if($row['NodeViewType']==6)
				$row['NodeViewType'] = 'List with Photo';
			else if($row['NodeViewType']==7)
				$row['NodeViewType'] = 'Irregular List with Photo';
			else if($row['NodeViewType']==8)
				$row['NodeViewType'] = 'Table';
			else
				$row['NodeViewType'] = '-';
			
			if($row['RowHighlight'] > 0 AND $row['RowHighlight'] !=NULL)
				$row['RowHighlight'] = 1;
			else
				$row['RowHighlight'] = 0;
			
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getTemplateList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getTemplateList($appFrw, $params)
	{	
		$results['data'] = array();
		
		$tplRow["TplName"]	= "tpl_airline_detail.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_airline_list.php";

		array_push($results['data'], $tplRow);
		
		
		$tplRow["TplName"]	= "tpl_flight_detail.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_flight_list.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_home_page.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_shop_detail.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_shop_list.php";

		array_push($results['data'], $tplRow);
		
		
		$tplRow["TplName"]	= "tpl_dark_site.php";


		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_general_final.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_general_multi_page.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_categories_list.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_categories_detail.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_contact_form.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_restaurants_list.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_duty_free_list.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_image_gallery_detail.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_video_gallery_detail.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_publications_detail.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_contact_form.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_site_map.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_domestic_destinations_amcharts.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_international_destinations_amchart.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_domestic_destinations_map_test.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_destinations.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "tpl_weather_page.php";

		array_push($results['data'], $tplRow);
		
		$tplRow["TplName"]	= "No Template";

		array_push($results['data'], $tplRow);
		
		
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getNodeViewTypeList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getNodeViewTypeList($appFrw, $params)
	{	
		$results['data'] = array();
		
		$NodeDataTypeSelector	= isset($params["NodeDataTypeSelector"]) ? (int)$params["NodeDataTypeSelector"] : 0;
		
		if ($NodeDataTypeSelector == -4 OR $NodeDataTypeSelector == -5 OR $NodeDataTypeSelector == -6  OR $NodeDataTypeSelector == -7 OR $NodeDataTypeSelector == -8  OR $NodeDataTypeSelector == -9 OR $NodeDataTypeSelector == -12 OR $NodeDataTypeSelector > 0)
		{
			$dtpRow["NodeViewType"] = 1;
			$dtpRow["NodeViewTypeDescr"]	= "Big Image";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 3;
			$dtpRow["NodeViewTypeDescr"]	= "Grid";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 4;
			$dtpRow["NodeViewTypeDescr"]	= "Grid - 5";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 7;
			$dtpRow["NodeViewTypeDescr"]	= "Irregular List with Photo";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 5;
			$dtpRow["NodeViewTypeDescr"]	= "List with Dictionary";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 6;
			$dtpRow["NodeViewTypeDescr"]	= "List with Photo";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 8;
			$dtpRow["NodeViewTypeDescr"]	= "Table";
			array_push($results['data'], $dtpRow);
		}
		else if ($NodeDataTypeSelector == -3)
		{
			$dtpRow["NodeViewType"] = 3;
			$dtpRow["NodeViewTypeDescr"]	= "Grid";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 4;
			$dtpRow["NodeViewTypeDescr"]	= "Grid - 5";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 5;
			$dtpRow["NodeViewTypeDescr"]	= "List with Dictionary";
			array_push($results['data'], $dtpRow);
			
			$dtpRow["NodeViewType"] = 8;
			$dtpRow["NodeViewTypeDescr"]	= "Table";
			array_push($results['data'], $dtpRow);	
		}
		else if ($NodeDataTypeSelector == -10 OR $NodeDataTypeSelector ==-11)
		{
			$dtpRow["NodeViewType"] = 1;
			$dtpRow["NodeViewTypeDescr"]	= "Big Image";
			array_push($results['data'], $dtpRow);
		}
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getMaxParentCode
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getMaxCode($appFrw, $NodeNodeID)
	{
		$results = array();
		
		$query = "	SELECT
						
						MAX(NodeCode) as NodeCode					
							
						FROM sys_node
						
						WHERE
						(
							NodeNodeID = ?
						)
						
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeNodeID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_PageTypeExists
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_PageTypeExists($appFrw, $NodeID, $NodeSiteID, $NodeField)
	{
		$query = "
			SELECT
				COUNT(NodeID) AS CountNode
			FROM sys_node
			WHERE
				".$NodeField." = 1
			AND
				NodeID <> ?
			AND
				NodeSiteID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("sys_node_PageTypeExists: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $NodeID, $NodeSiteID);
		
		if(!$stmt->execute()) 
			exit("sys_node_PageTypeExists: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("sys_node_PageTypeExists: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row['CountNode'];
	}
	

}
