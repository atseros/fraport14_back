<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_cf_subjects
*
* DESCRIPTION: 
*	Class for table cf_subjects
*
* table fields:
*
 `SubID` int(11) NOT NULL,
 `SubSiteID` int(11) NOT NULL,
 `SubTitle` varchar(512) NOT NULL,
 `SubPriority` int(11) NOT NULL,
 `SubStatus` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_cf_subjects
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects::cf_subjects_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$SubID = DB_sys_kxn::get_NextID($appFrw, 'cf_subjects');
		
		if($SubID > 0)
		{
			$results["success"] = true;
			$results["data"]["SubID"] = $SubID;
			$results["data"]["SubSiteID"] = $SiteID;
            $results["data"]["SubStatus"] = 1;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table cf_subjects";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SubID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SubID FROM cf_subjects WHERE SubID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SubID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_cf_subjects_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects::cf_subjects_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SubID 	= (int)$params["SubID"];
		
		if($SubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_cf_subjects::check_RecordExists($appFrw, $SubID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SubID;
			return $results;
		}

        $SubSiteID 		= (isset($params['SubSiteID'])) ? $params['SubSiteID'] : $record['SubSiteID'];
			
		// insert an empty record
		$query = "	INSERT INTO cf_subjects
					(
						  SubID
						 ,SubSiteID
					)
					VALUES
					(
						  ?
						 ,?
					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $SubID, $SubSiteID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_cf_subjects::cf_subjects_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects::cf_subjects_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SubID = (int)$params["SubID"];
		
		if($SubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_cf_subjects::check_RecordExists($appFrw, $SubID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no record with id = ".$SubID;
			return $results;
		}
		
		$query = "	SELECT
						
						 SubID
						,SubTitle
						,SubPriority
						,SubStatus					
							
					FROM cf_subjects
					WHERE
					SubID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SubID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects::cf_subjects_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SubID = (int)$params["SubID"];
		
		
		if($SubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_cf_subjects::cf_subjects_getRecord($appFrw, array('SubID'=>$SubID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
        $SubTitle 		= (isset($params['SubTitle'])) ? $params['SubTitle'] : $record['SubTitle'];
        $SubPriority    = (isset($params['SubPriority'])) ? $params['SubPriority'] : $record['SubPriority'];
        $SubStatus 		= (isset($params['SubStatus'])) ? $params['SubStatus'] : $record['SubStatus'];

        $query = "	UPDATE cf_subjects SET
						 SubTitle 			= ?
						,SubPriority        = ?
						,SubStatus          = ?
					WHERE
					SubID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siii",
							 $SubTitle
                            ,$SubPriority
                            ,$SubStatus
                            ,$SubID
                        );
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SubID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SubID = (int)$params["SubID"];
		if($SubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_cf_subjects_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT SubSubID FROM cf_sub_subject WHERE SubSubSubID = ?))
							then 1
							else 0
						 end as SubSubjectExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SubID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";

		if( $row["SubSubjectExists"] == 1) {
            $results["success"] = false;
            $results["reason"] = "There are sub subjects related to the subject";
            return $results;
        }

		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects::cf_subjects_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SubID = (int)$params["SubID"];
		
		if($SubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		 //check if can be deleted
		 $canBeDeleted = DB_cf_subjects::RecordCanBeDeleted($appFrw, array('SubID'=>$SubID) );
		 if($canBeDeleted["success"] == false)
		 {
			 $results["success"] = false;
			 $results["reason"] = $canBeDeleted["reason"];
			
			 return $results;
		 }
		
		$query = "DELETE FROM cf_subjects WHERE SubID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SubID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SubID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_get_List
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_get_List($appFrw, $params)
	{
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;

		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
        $filterShowAll	    = isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
		
		$results['data'] = array();

		$query = "	SELECT
						
                         SubID
                        ,SubTitle
                        ,SubPriority
                        ,SubStatus
					
					FROM cf_subjects
					
					WHERE 
					(
						SubSiteID = ?
					)
					AND
					(
						SubTitle LIKE ?
					)
					AND
					(
						? = 1
						OR
						SubStatus = 1
					)
					
					ORDER BY SubPriority ASC
							
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "cf_subjects_get_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $SiteID, $filterStrToFind, $filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "cf_subjects_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "cf_subjects_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
}
