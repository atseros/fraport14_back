<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_slider_images
*
* DESCRIPTION: 
*	Class for table sys_slider_images
*
* table fields:
*
 `SliderImagesID` int(11) NOT NULL,
 `SliderImagesSliderID` int(11) NOT NULL,
 `SliderImagesTitle` varchar(1024) NOT NULL,
 `SliderImagesFIlename` varchar(11) NOT NULL,
 `SliderImagesPriority` int(11) NOT NULL,
 `SliderImagesNodeID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_slider_images
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::sys_slider_images_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_images_get_NewRecordDefValues($appFrw, $params)
	{
		$SliderID 	= (int)$params["SliderID"];
		
		$SliderImagesID = DB_sys_kxn::get_NextID($appFrw, 'sys_slider_images');
		
		if($SliderImagesID > 0)
		{
			$results["success"] = true;
			$results["data"]["SliderImagesID"] = $SliderImagesID;
			$results["data"]["SliderImagesSliderID"] = $SliderID;
			$results["data"]["SliderImagesStatus"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_slider_images";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SliderImagesID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SliderImagesID FROM sys_slider_images WHERE SliderImagesID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SliderImagesID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::sys_slider_images_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_images_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderImagesID 	= (int)$params["SliderImagesID"];
		
		if($SliderImagesID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_slider_images::check_RecordExists($appFrw, $SliderImagesID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SliderImagesID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_slider_images
					(
						 SliderImagesID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SliderImagesID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_slider_images::sys_slider_images_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::sys_slider_images_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_images_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderImagesID = (int)$params["SliderImagesID"];
		
		if($SliderImagesID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_slider_images::check_RecordExists($appFrw, $SliderImagesID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SliderImagesID;
			return $results;
		}
		
		$query = "	SELECT
						
						SliderImagesID
						,SliderImagesSliderID
						,SliderImagesTitle
						,SliderImagesFIlename
						,SliderImagesPriority
						,SliderImagesNodeID
						,SliderImagesStatus
						,NodeID as SliderImagesNodeID
						,NodeTitle 
							
					FROM sys_slider_images
					LEFT JOIN sys_node ON (NodeID = SliderImagesNodeID)
					WHERE
					SliderImagesID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SliderImagesID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::sys_slider_images_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_images_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderImagesID = (int)$params["SliderImagesID"];
		
		
		if($SliderImagesID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_slider_images::sys_slider_images_getRecord($appFrw, array('SliderImagesID'=>$SliderImagesID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$SliderImagesSliderID 	= (isset($params['SliderImagesSliderID'])) ? $params['SliderImagesSliderID'] : $record['SliderImagesSliderID'];
		$SliderImagesTitle 		= (isset($params['SliderImagesTitle'])) ? $params['SliderImagesTitle'] : $record['SliderImagesTitle'];
		$SliderImagesFIlename 		= (isset($params['SliderImagesFIlename'])) ? $params['SliderImagesFIlename'] : $record['SliderImagesFIlename'];
		$SliderImagesPriority = (isset($params['SliderImagesPriority'])) ? $params['SliderImagesPriority'] : $record['SliderImagesPriority'];
		$SliderImagesNodeID = (isset($params['SliderImagesNodeID'])) ? $params['SliderImagesNodeID'] : $record['SliderImagesNodeID'];
		$SliderImagesStatus = (isset($params['SliderImagesStatus'])) ? $params['SliderImagesStatus'] : $record['SliderImagesStatus'];
		// $SliderImagesExternalLink = (isset($params['SliderImagesExternalLink'])) ? $params['SliderImagesExternalLink'] : $record['SliderImagesExternalLink'];
		
		$query = "	UPDATE sys_slider_images SET
							
							SliderImagesSliderID 	= ?
							,SliderImagesTitle 	= ?
							,SliderImagesFIlename 	= ?
							,SliderImagesPriority 	= ?
							,SliderImagesNodeID 	= ?
							,SliderImagesStatus 	= ?
						
							WHERE
							SliderImagesID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("issiiii",
								 $SliderImagesSliderID 			
								,$SliderImagesTitle 			
								,$SliderImagesFIlename 			
								,$SliderImagesPriority 						
								,$SliderImagesNodeID 
								,$SliderImagesStatus 
								,$SliderImagesID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SliderImagesID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::sys_slider_images_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_images_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderImagesID = (int)$params["SliderImagesID"];
		
		if($SliderImagesID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_slider_images WHERE SliderImagesID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SliderImagesID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SliderImagesID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::sys_slider_images_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_images_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$SliderID	= isset($params["SliderID"]) ? (int)$params["SliderID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
			
		
		$query = "	SELECT
						
							SliderImagesID
							,SliderImagesSliderID
							,SliderImagesTitle
							,SliderImagesFIlename
							,SliderImagesPriority
							,SliderImagesNodeID
							,SliderImagesStatus
							,NodeTitle
						
							FROM sys_slider_images
							LEFT JOIN sys_node ON (NodeID = SliderImagesNodeID)
							
							WHERE
							(
								SliderImagesSliderID = ?
							)
							AND
							(
								SliderImagesTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								SliderImagesStatus = 1
							)
							

							ORDER BY SliderImagesPriority ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $SliderID, $filterStrToFind, $filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['SliderImagesFullUrl']= DB_sys_slider_images::get_sys_slider_images_FullUrl($appFrw, $row['SliderImagesID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::getSliderImagesListSitePages
	* --------------------------------------------------------------------------
	*/
	public static function getSliderImagesListSitePages($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeSliderNodeID	= isset($params["NodeSliderNodeID"]) ? (int)$params["NodeSliderNodeID"] : 0;
		$SliderImagesLngType	= isset($params["SliderImagesLngType"]) ? (int)$params["SliderImagesLngType"] : 0;
			
		
		$query = "	SELECT 

							SliderImagesLngTitle
							,SliderImagesID

							FROM sys_node_slider 
							LEFT JOIN sys_slider ON (SliderID = NodeSliderSliderID)
							LEFT JOIN sys_node ON (NodeID = NodeSliderNodeID)
							LEFT JOIN sys_slider_images ON (SliderImagesSliderID = SliderID)
							LEFT JOIN sys_slider_imageslng ON (SliderImagesLngSliderImagesID = 	SliderImagesID)


							WHERE 
							(
								NodeSliderNodeID = ?
							)

							AND 
							(
								SliderImagesLngType = ?
							)
							AND
							(
								SliderImagesLngTitle != ''
							)
							

							ORDER BY SliderImagesPriority ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("ii", $NodeSliderNodeID, $SliderImagesLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['SliderImagesFullUrl']= DB_sys_slider_images::get_sys_slider_images_FullUrl($appFrw, $row['SliderImagesID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider_images::get_sys_slider_images_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_slider_images_FullUrl($appFrw, $SliderImagesID)
	{
		$url = FileManager::getTblFolderUrl("sys_slider_images", $SliderImagesID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_slider_images::sys_slider_images_getRecord($appFrw,array('SliderImagesID'=>$SliderImagesID));
		if( $imageData["success"] == true  && $imageData["data"]["SliderImagesFIlename"] )
		{
			$FulUrl = $url.$imageData["data"]["SliderImagesFIlename"];
		}
		return $FulUrl;
	}
	
}
