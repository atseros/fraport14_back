<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_seasonal_flight_plan_version
*
* DESCRIPTION: 
*	Class for table sys_shop
*
* table fields:
*
 `SfpvID` int(11) NOT NULL,
 `SfpvDateAdded` date NOT NULL,
 `SfpvTitle` varchar(1024) NOT NULL,
 `SfpvStatus` int(11) NOT NULL,
 `SfpvSiteID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_seasonal_flight_plan_version
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_seasonal_flight_plan_version_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$SfpvID = DB_sys_kxn::get_NextID($appFrw, 'sys_seasonal_flight_plan_version');
		
		if($SfpvID > 0)
		{
			$results["success"] = true;
			$results["data"]["SfpvID"] = $SfpvID;
			$results["data"]["SfpvSiteID"] = $SiteID;
			$results["data"]["SfpvStatus"] = 2;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_seasonal_flight_plan_version";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan_version::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SfpvID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SfpvID FROM sys_seasonal_flight_plan_version WHERE SfpvID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SfpvID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_seasonal_flight_plan_version_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SfpvID 	= (int)$params["SfpvID"];
		
		if($SfpvID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_seasonal_flight_plan_version::check_RecordExists($appFrw, $SfpvID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SfpvID;
			return $results;
		}
        $SfpvTitle = (isset($params['SfpvTitle'])) ? $params['SfpvTitle'] : 'Title';
        $SfpvStatus = (isset($params['SfpvStatus'])) ? $params['SfpvStatus'] : 2;
        $SfpvSiteID = (isset($params['SfpvSiteID'])) ? $params['SfpvSiteID'] : 1;
        $SfpvDateAdded = date('Y-m-d');

		// insert an empty record
		$query = "	INSERT INTO sys_seasonal_flight_plan_version
					(
						 SfpvID
						,SfpvTitle
						,SfpvDateAdded
						,SfpvStatus
						,SfpvSiteID
					)
					VALUES
					(
						 ?
						 ,?
						 ,?
						 ,?
						 ,?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("issii", $SfpvID, $SfpvTitle, $SfpvDateAdded, $SfpvStatus, $SfpvSiteID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		// $results = DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_UpdateRecord($appFrw, $params);
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_seasonal_flight_plan_version_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SfpvID = (int)$params["SfpvID"];
		
		if($SfpvID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_seasonal_flight_plan_version::check_RecordExists($appFrw, $SfpvID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SfpvID;
			return $results;
		}
		
		$query = "	SELECT
						
						SfpvID
						,SfpvDateAdded
						,SfpvTitle
						,SfpvStatus
						,SfpvSiteID
							
					FROM sys_seasonal_flight_plan_version
					WHERE
					SfpvID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SfpvID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		
		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_seasonal_flight_plan_version_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SfpvID = (int)$params["SfpvID"];
		
		
		if($SfpvID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_getRecord($appFrw, array('SfpvID'=>$SfpvID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$SfpvTitle = (isset($params['SfpvTitle'])) ? $params['SfpvTitle'] : $record['SfpvTitle'];
		$SfpvStatus = (isset($params['SfpvStatus'])) ? $params['SfpvStatus'] : $record['SfpvStatus'];
		$SfpvSiteID 	= (isset($params['SfpvSiteID'])) ? $params['SfpvSiteID'] : $record['SfpvSiteID'];
	
		//$SfpvDateAdded = date('Y-m-d');

		if ($SfpvStatus == 1)
		{
			$query = "	UPDATE sys_seasonal_flight_plan_version SET
							
							SfpvStatus 	= 2

							WHERE
							SfpvID != ? AND SfpvSiteID = ?
						";
			$stmt = $appFrw->DB_Link->prepare($query);
		
			if(!$stmt) 
				exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
			
			$stmt->bind_param("ii",		
									$SfpvID
									,$SfpvSiteID									
			
							);
			
			if(!$stmt->execute()) 
				exit("update_Record: error at update : ".$stmt->error);
			
			$stmt->close();					
					
		}	
		
		$query = "	UPDATE sys_seasonal_flight_plan_version SET
							
							SfpvTitle 	= ?
							,SfpvStatus 	= ?
							,SfpvSiteID 	= ?
							--,SfpvDateAdded 	= ?

						
							WHERE
							SfpvID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siisi",
								 $SfpvTitle 			
								,$SfpvStatus 			
								,$SfpvSiteID 			
								//,$SfpvDateAdded
								,$SfpvID 			
		
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SfpvID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_seasonal_flight_plan_version_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SfpvID = (int)$params["SfpvID"];
		
		if($SfpvID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_seasonal_flight_plan_version WHERE SfpvID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SfpvID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SfpvID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_get_List
	* --------------------------------------------------------------------------
	*/
	public static function sys_seasonal_flight_plan_version_get_List($appFrw, $params)
	{
		$results = array();

		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
				
		$query = "	SELECT
						
							SfpvID
							,SfpvDateAdded
							,SfpvTitle
							,SfpvStatus
							,SfpvSiteID
						
							FROM sys_seasonal_flight_plan_version
							
							WHERE
							(
								SfpvSiteID = ?
							)

							ORDER BY SfpvDateAdded DESC			
		";
		

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	

	
}
