<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_alert
*
* DESCRIPTION: 
*	Class for table sys_shop
*
* table fields:
*
 `AlertID` int(11) NOT NULL,
 `AlertStatus` int(11) NOT NULL,
 `AlertType` int(11) NOT NULL,
 `AlertPriority` int(11) NOT NULL,
 `AlertTitle` varchar(1024) NOT NULL,
 `AlertStartDateTime` datetime NOT NULL,
 `AlertEndDateTime` datetime NOT NULL,
*
*********************************************************************************************/
class DB_sys_alert
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_alert::sys_alert_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_alert_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$AlertID = DB_sys_kxn::get_NextID($appFrw, 'sys_alert');
		
		if($AlertID > 0)
		{
			$results["success"] = true;
			$results["data"]["AlertID"] = $AlertID;
			$results["data"]["AlertSiteID"] = $SiteID;
			$results["data"]["AlertStatus"] = 1;
			$results["data"]["AlertType"] = 1;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_alert";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_alert::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $AlertID)
	{
		$query = "	SELECT
						   case when( exists (SELECT AlertID FROM sys_alert WHERE AlertID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $AlertID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_alert::sys_alert_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_alert_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$AlertID 	= (int)$params["AlertID"];
		
		if($AlertID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_alert::check_RecordExists($appFrw, $AlertID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$AlertID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_alert
					(
						 AlertID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $AlertID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_alert::sys_alert_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_alert::sys_alert_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_alert_getRecord($appFrw, $params)
	{
		$results = array();
		
		$AlertID = (int)$params["AlertID"];
		
		if($AlertID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_alert::check_RecordExists($appFrw, $AlertID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$AlertID;
			return $results;
		}
		
		$query = "	SELECT
						
						AlertID
						,AlertStatus
						,AlertType
						,AlertPriority
						,AlertTitle
						,AlertSiteID
						,AlertStartDate
						,AlertStartTime
						,AlertEndDate
						,AlertEndTime
							
					FROM sys_alert
					WHERE
					AlertID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $AlertID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		
		if($row['AlertStartTime'])
					$row['AlertStartTime'] = date("H:i", strtotime($row['AlertStartTime'] ));
		if($row['AlertEndTime'])
					$row['AlertEndTime'] = date("H:i", strtotime($row['AlertEndTime'] ));
		
		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_alert::sys_alert_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_alert_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$AlertID = (int)$params["AlertID"];
		
		
		if($AlertID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_alert::sys_alert_getRecord($appFrw, array('AlertID'=>$AlertID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$AlertStatus = (isset($params['AlertStatus'])) ? $params['AlertStatus'] : $record['AlertStatus'];
		$AlertType = (isset($params['AlertType'])) ? $params['AlertType'] : $record['AlertType'];
		$AlertPriority 	= (isset($params['AlertPriority'])) ? $params['AlertPriority'] : $record['AlertPriority'];
		$AlertTitle 	= (isset($params['AlertTitle'])) ? $params['AlertTitle'] : $record['AlertTitle'];
		$AlertStartDate 	= (isset($params['AlertStartDate'])) ? $params['AlertStartDate'] : $record['AlertStartDate'];
		$AlertStartTime = (isset($params['AlertStartTime'])) ? $params['AlertStartTime'] : $record['AlertStartTime'];
		$AlertEndDate = (isset($params['AlertEndDate'])) ? $params['AlertEndDate'] : $record['AlertEndDate'];
		$AlertEndTime = (isset($params['AlertEndTime'])) ? $params['AlertEndTime'] : $record['AlertEndTime'];
		$AlertSiteID 	= (isset($params['AlertSiteID'])) ? $params['AlertSiteID'] : $record['AlertSiteID'];
		
		if ($AlertStartDate == NULL)
			$AlertStartDate = NULL;
		else	
		$AlertStartDate = date("Y-m-d", strtotime($AlertStartDate));
	
		if ($AlertEndDate == NULL)
			$AlertEndDate = NULL;
		else	
		$AlertEndDate = date("Y-m-d", strtotime($AlertEndDate));
	
		
		if ($AlertStartTime == NULL)
			$AlertStartTime = NULL;
		else	
		$AlertStartTime = date("H:i", strtotime($AlertStartTime));
	
		if ($AlertEndTime == NULL)
			$AlertEndTime = NULL;
		else	
		$AlertEndTime = date("H:i", strtotime($AlertEndTime));

		
		
		$query = "	UPDATE sys_alert SET
							
							AlertStatus 	= ?
							,AlertType 	= ?
							,AlertPriority 	= ?
							,AlertTitle 	= ?
							,AlertStartDate	 	= ?
							,AlertStartTime 	= ?
							,AlertEndDate	 	= ?
							,AlertEndTime 	= ?
							,AlertSiteID 	= ?
						
							WHERE
							AlertID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiisssssii",
								 $AlertStatus 			
								,$AlertType 			
								,$AlertPriority 			
								,$AlertTitle 			
								,$AlertStartDate 			
								,$AlertStartTime 			
								,$AlertEndDate 			
								,$AlertEndTime 			
								,$AlertSiteID 			
								,$AlertID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $AlertID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_alert::sys_alert_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_alert_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$AlertID = (int)$params["AlertID"];
		
		if($AlertID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_alert WHERE AlertID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $AlertID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $AlertID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_alert::sys_alert_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_alert_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
		$filterType	= isset($params["filterType"]) ? (int)$params["filterType"] : -1;
				
		$query = "	SELECT
						
							AlertID
							,AlertStatus
							,AlertType
							,AlertPriority
							,AlertTitle
							,AlertSiteID
							,AlertStartDate
							,AlertStartTime
							,AlertEndDate
							,AlertEndTime
						
							FROM sys_alert
							
							WHERE
							(
								AlertSiteID = ?
							)
							AND
							(
								AlertTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								AlertStatus = 1
							)
							AND
							(
								? = -1
								OR
								AlertType = ?
							)
							ORDER BY AlertPriority ASC			
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isiii", $SiteID, $filterStrToFind,$filterShowAll,$filterType,$filterType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	

	
}
