<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_footer
*
* DESCRIPTION: 
*	Class for table sys_footer
*
* table fields:
*
 `FtrID` int(11) NOT NULL,
 `FtrSiteID` int(11) NOT NULL,
 `FtrTitle` varchar(1024) NOT NULL,
 `FtrType` int(11) NOT NULL,
 `FtrPriority` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_footer
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::sys_footer_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$FtrID = DB_sys_kxn::get_NextID($appFrw, 'sys_footer');
		
		if($FtrID > 0)
		{
			$results["success"] = true;
			$results["data"]["FtrID"] = $FtrID;
			$results["data"]["FtrSiteID"] = $SiteID;
			$results["data"]["FtrStatus"] = 1;
			$results["data"]["FtrType"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_footer";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $FtrID)
	{
		$query = "	SELECT
						   case when( exists (SELECT FtrID FROM sys_footer WHERE FtrID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FtrID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::sys_footer_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrID 	= (int)$params["FtrID"];
		
		if($FtrID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_footer::check_RecordExists($appFrw, $FtrID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$FtrID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_footer
					(
						 FtrID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FtrID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_footer::sys_footer_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::sys_footer_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_getRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrID = (int)$params["FtrID"];
		
		if($FtrID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_footer::check_RecordExists($appFrw, $FtrID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$FtrID;
			return $results;
		}
		
		$query = "	SELECT
						
						FtrID
						,FtrSiteID
						,FtrTitle						
						,FtrType						
						,FtrPriority						
						,FtrStatus						
							
					FROM sys_footer
					WHERE
					FtrID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FtrID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::sys_footer_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrID = (int)$params["FtrID"];
		
		
		if($FtrID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_footer::sys_footer_getRecord($appFrw, array('FtrID'=>$FtrID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$FtrSiteID 		= (isset($params['FtrSiteID'])) ? $params['FtrSiteID'] : $record['FtrSiteID'];
		$FtrTitle 		= (isset($params['FtrTitle'])) ? $params['FtrTitle'] : $record['FtrTitle'];
		$FtrType 		= (isset($params['FtrType'])) ? $params['FtrType'] : $record['FtrType'];
		$FtrPriority 		= (isset($params['FtrPriority'])) ? $params['FtrPriority'] : $record['FtrPriority'];
		$FtrStatus 		= (isset($params['FtrStatus'])) ? $params['FtrStatus'] : $record['FtrStatus'];
	
		$query = "	UPDATE sys_footer SET
						 FtrSiteID 			= ?
						,FtrTitle 			= ?
						,FtrType 			= ?
						,FtrPriority 			= ?
						,FtrStatus 			= ?
						
					WHERE
					FtrID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("isiiii",
								 $FtrSiteID 			
								,$FtrTitle 
								,$FtrType 
								,$FtrPriority 
								,$FtrStatus 
								,$FtrID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $FtrID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::sys_footer_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrID = (int)$params["FtrID"];
		
		if($FtrID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_footer WHERE FtrID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FtrID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $FtrID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::sys_footer_get_List
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_get_List($appFrw, $params)
	{
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		
		$results['data'] = array();
		
				
		$query = "	SELECT
						
							FtrID
							,FtrSiteID
							,FtrTitle
							,FtrType
							,FtrPriority
							,FtrStatus
						
							FROM sys_footer
							
							WHERE 
							(
								FtrSiteID = ?
							)
							AND
							(
								FtrTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								FtrStatus = 1
							)
							
							ORDER BY FtrPriority ASC
							
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_footer_get_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $SiteID, $filterStrToFind, $filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_footer_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_footer_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
}
