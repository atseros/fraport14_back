<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_shop
*
* DESCRIPTION: 
*	Class for table sys_shop
*
* table fields:
*
 `ShopID` int(11) NOT NULL,
 `ShopTitle` varchar(1024) NOT NULL,
 `ShopSubTitle` varchar(1024) NOT NULL,
 `ShopText` text NOT NULL,
 `ShopPayment` varchar(1024) NOT NULL,
 `ShopImgFilename` varchar(1024) NOT NULL,
 `ShopPriority` int(11) DEFAULT NULL,
 `ShopSiteID` int(11) DEFAULT NULL,
 `ShopStatus` int(11) DEFAULT NULL,
*
*********************************************************************************************/
class DB_sys_shop
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::sys_shop_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_shop_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$ShopID = DB_sys_kxn::get_NextID($appFrw, 'sys_shop');
		
		if($ShopID > 0)
		{
			$results["success"] = true;
			$results["data"]["ShopID"] = $ShopID;
			$results["data"]["ShopSiteID"] = $SiteID;
			$results["data"]["ShopStatus"] = 1;
			$results["data"]["ShopType"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_shop";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $ShopID)
	{
		$query = "	SELECT
						   case when( exists (SELECT ShopID FROM sys_shop WHERE ShopID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $ShopID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::sys_shop_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_shop_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopID 	= (int)$params["ShopID"];
		
		if($ShopID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_shop::check_RecordExists($appFrw, $ShopID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$ShopID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_shop
					(
						 ShopID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $ShopID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_shop::sys_shop_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::sys_shop_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_shop_getRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopID = (int)$params["ShopID"];
		
		if($ShopID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_shop::check_RecordExists($appFrw, $ShopID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$ShopID;
			return $results;
		}
		
		$query = "	SELECT
						
						ShopID
						,ShopTitle
						,ShopImgFilename
						,ShopPriority
						,ShopSiteID
						,ShopStatus
						,ShopType
						,ShopImgFilenameLogo
						,ShopPhone
						,ShopEscapeDefault
							
					FROM sys_shop
					WHERE
					ShopID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $ShopID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::sys_shop_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_shop_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopID = (int)$params["ShopID"];
		
		
		if($ShopID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_shop::sys_shop_getRecord($appFrw, array('ShopID'=>$ShopID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$ShopSiteID 				= (isset($params['ShopSiteID'])) ? $params['ShopSiteID'] : $record['ShopSiteID'];
		$ShopStatus 				= (isset($params['ShopStatus'])) ? $params['ShopStatus'] : $record['ShopStatus'];
		$ShopPriority 			= (isset($params['ShopPriority'])) ? $params['ShopPriority'] : $record['ShopPriority'];
		$ShopTitle 				= (isset($params['ShopTitle'])) ? $params['ShopTitle'] : $record['ShopTitle'];
		// $ShopPayment 			= (isset($params['ShopPayment'])) ? $params['ShopPayment'] : $record['ShopPayment'];
		// $ShopDisabilityAccess = (isset($params['ShopDisabilityAccess'])) ? $params['ShopDisabilityAccess'] : $record['ShopDisabilityAccess'];
		$ShopImgFilename 	= (isset($params['ShopImgFilename'])) ? $params['ShopImgFilename'] : $record['ShopImgFilename'];
		$ShopType 	= (isset($params['ShopType'])) ? $params['ShopType'] : $record['ShopType'];
		$ShopPhone 	= (isset($params['ShopPhone'])) ? $params['ShopPhone'] : $record['ShopPhone'];
		$ShopImgFilenameLogo 	= (isset($params['ShopImgFilenameLogo'])) ? $params['ShopImgFilenameLogo'] : $record['ShopImgFilenameLogo'];
		$ShopEscapeDefault 	= (isset($params['ShopEscapeDefault'])) ? $params['ShopEscapeDefault'] : $record['ShopEscapeDefault'];
		
		$query = "	UPDATE sys_shop SET
							
							ShopSiteID 	= ?
							,ShopStatus 	= ?
							,ShopPriority 	= ?
							,ShopTitle	 	= ?
							,ShopImgFilename 	= ?
							,ShopType 	= ?
							,ShopPhone 	= ?
							,ShopImgFilenameLogo 	= ?
							,ShopEscapeDefault	= ?
						
							WHERE
							ShopID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiississsi",
								 $ShopSiteID 			
								,$ShopStatus 			
								,$ShopPriority 			
								,$ShopTitle 					
								,$ShopImgFilename 			
								,$ShopType 			
								,$ShopPhone 			
								,$ShopImgFilenameLogo 
								,$ShopEscapeDefault
								,$ShopID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $ShopID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::sys_shop_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_shop_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopID = (int)$params["ShopID"];
		
		if($ShopID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_shop WHERE ShopID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $ShopID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $ShopID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::sys_shop_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_shop_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
		$filterType	= isset($params["filterType"]) ? (int)$params["filterType"] : -1;
				
		$query = "	SELECT
						
							ShopID
							,ShopTitle
							,ShopImgFilename
							,ShopPriority
							,ShopSiteID
							,ShopStatus
							,ShopType
							,ShopImgFilenameLogo
							,ShopPhone
							,ShopEscapeDefault
						
							FROM sys_shop
							
							WHERE
							(
								ShopSiteID = ?
							)
							AND
							(
								ShopTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								ShopStatus = 1
							)
							AND
							(
								? = -1
								OR
								ShopType = ?
							)
							ORDER BY ShopPriority ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isiii", $SiteID, $filterStrToFind,$filterShowAll,$filterType,$filterType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['ShopFullUrl']= DB_sys_shop::get_sys_shop_FullUrl($appFrw, $row['ShopID']);
			$row['ShopFullUrlLogo']= DB_sys_shop::get_sys_shop_FullUrlLogo($appFrw, $row['ShopID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::get_sys_shop_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_shop_FullUrl($appFrw, $ShopID)
	{
		$url = FileManager::getTblFolderUrl("sys_shop", $ShopID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_shop::sys_shop_getRecord($appFrw,array('ShopID'=>$ShopID));
		if( $imageData["success"] == true  && $imageData["data"]["ShopImgFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["ShopImgFilename"];
		}
		return $FulUrl;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::get_sys_shop_FullUrlLogo
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_shop_FullUrlLogo($appFrw, $ShopID)
	{
		$url = FileManager::getTblFolderUrl("sys_shop", $ShopID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_shop::sys_shop_getRecord($appFrw,array('ShopID'=>$ShopID));
		if( $imageData["success"] == true  && $imageData["data"]["ShopImgFilenameLogo"] )
		{
			$FulUrl = $url.$imageData["data"]["ShopImgFilenameLogo"];
		}
		return $FulUrl;
	}
	
}
