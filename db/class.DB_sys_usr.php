<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_usr
*
* DESCRIPTION: 
*	Class for table sys_usr. 
*
*
* table fields:
*
*	UsrID			int(11)
*	UsrLogin		varchar(512)
*	UsrPassword		varchar(512)
*	UsrStatus		int(11)
*	UsrRole 		int(11) :: 	1 = User
*								2 = Site Admin
*								3 = Global Admin
*	UsrFirstName	varchar(512)
*	UsrLastName		varchar(512)
*
*********************************************************************************************/
class DB_sys_usr
{
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::sys_usr_get_NewRecordDefValues
	*
	* input params:
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> array[] with return fields
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_usr_get_NewRecordDefValues($appFrw, $params)
	{
		$UsrID = DB_sys_kxn::get_NextID($appFrw, 'sys_usr');
		
		if($UsrID > 0)
		{
			$results["success"] = true;
			$results["data"]["UsrID"] = $UsrID;
			$results["data"]["UsrStatus"] = 1;
			$results["data"]["UsrRole"] = 3;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_usr";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::check_RecordExists
	*
	* input params:
	*		UsrID			int			:: id of the record
	*
	*
	* return:
	*	int								:: 0 = no, 1 = yes
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $UsrID)
	{
		$query = "	SELECT
						   case when( exists (SELECT UsrID FROM sys_usr WHERE UsrID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $UsrID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::sys_usr_InsertRecord
	*
	* input params:
	*	array
	*	[
	*		UsrID			int			:: new record id
	*		...							:: other fields of the table
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_usr_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$UsrID 	= (int)$params["UsrID"];
		
		if($UsrID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_usr::check_RecordExists($appFrw, $UsrID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$UsrID;
			return $results;
		}
		
		$UsrRole 	= (int)$params["UsrRole"];
		$UsrStatus 	= (int)$params["UsrStatus"];
			
		// insert an empty record
		$query = "	INSERT INTO sys_usr
					(
						 UsrID
						 ,UsrRole
						 ,UsrStatus

					)
					VALUES
					(
						 ?
						 ,?
						 ,?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iii", $UsrID, $UsrRole, $UsrStatus);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at insert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_usr::sys_usr_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::sys_usr_getRecord
	*
	* input params:
	*	array
	*	[
	*		UsrID			int			:: id of the record
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> array[] with record fields
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_usr_getRecord($appFrw, $params)
	{
		$results = array();
		
		$UsrID = (int)$params["UsrID"];
		
		if($UsrID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_usr::check_RecordExists($appFrw, $UsrID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$UsrID;
			return $results;
		}
		
		$query = "	SELECT
						
						UsrID
						,UsrLastName
						,UsrFirstName
						,UsrLogin
						,UsrPassword
						,UsrRole
						,UsrStatus	
							
					FROM sys_usr
					WHERE
					UsrID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $UsrID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::sys_usr_UpdateRecord
	*
	* input params:
	*	array
	*	[
	*		UsrID			int			:: id of the record to be updated
	*		...							:: other fields of the record to be updated
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record updated
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_usr_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$UsrID = (int)$params["UsrID"];
		
		
		if($UsrID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_usr::sys_usr_getRecord($appFrw, array('UsrID'=>$UsrID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$UsrFirstName 	= (isset($params['UsrFirstName'])) ? $params['UsrFirstName'] : $record['UsrFirstName'];
		$UsrLastName 	= (isset($params['UsrLastName'])) ? $params['UsrLastName'] : $record['UsrLastName'];
		$UsrLogin 		= (isset($params['UsrLogin'])) ? $params['UsrLogin'] : $record['UsrLogin'];
		$UsrPassword 	= (isset($params['UsrPassword'])) ? $params['UsrPassword'] : $record['UsrPassword'];
		$UsrRole 		= (isset($params['UsrRole'])) ? $params['UsrRole'] : $record['UsrRole'];
		$UsrStatus 		= (isset($params['UsrStatus'])) ? $params['UsrStatus'] : $record['UsrStatus'];
		
		$options = [
		'cost' => 11,
		];


		$hashedpasword = md5($UsrPassword);
		

		$query = "	UPDATE sys_usr SET
						 UsrFirstName = ?
						 ,UsrLastName = ?
						 ,UsrLogin	  = ?
						 ,UsrPassword = ?
						 ,UsrRole     = ?
						 ,UsrStatus   = ?
						
					WHERE
					UsrID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ssssiii",
								 $UsrFirstName 			
								,$UsrLastName 
								,$UsrLogin
								,$hashedpasword
								,$UsrRole
								,$UsrStatus
								,$UsrID								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $UsrID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sup_cmp::get_sup_cmp_RecordCanBeDeleted
	*
	* input params:
	*	array
	*	[
	*		CmpID			int			:: id of the record to be checked
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function get_sup_cmp_RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$CmpID = (int)$params["CmpID"];
		if($CmpID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT PrdID FROM sup_prd WHERE PrdCmpID = ?))
							then 1
							else 0
						 end as PrdExists
				";
		/*
						 case when( exists (SELECT CmpCtgID FROM sup_cmpctg WHERE CmpCtgCmpID = ?))
							then 1
							else 0
						 end as CmpCtgExists
						,case when( exists (SELECT CmpImgID FROM sup_cmpimg WHERE CmpImgCmpID = ?))
							then 1
							else 0
						 end as CmpImgExists
		*/
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_sup_cmp_RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CmpID);
		
		if(!$stmt->execute()) 
			exit("check_sup_cmp_RecordExists: error at select company: ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select company: ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["PrdExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = $results["reason"] + "<br/>" + "There are products related to the company";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::sys_usr_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		UsrID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_usr_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$UsrID = (int)$params["UsrID"];
		
		if($UsrID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// check if can be deleted
		// $canBeDeleted = DB_sup_cmp::get_sup_cmp_RecordCanBeDeleted($appFrw, array('CmpID'=>$CmpID) );
		// if( $canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			// return;
		// }
		
		$query = "DELETE FROM sys_usr WHERE UsrID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $UsrID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $UsrID;
		return $results;		
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::sys_usr_get_List
	*
	* input params:
	*	array
	*	[
	*		showAll			int			:: 0 = show active, 1 = show all
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> array
						[
							0	=>		array[] with first record fields
							...
							i	=>		array[] with i+1 record fields
						]
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_usr_get_List($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
				
		$query = "	SELECT
						
							UsrID
							,UsrLogin
							,UsrPassword
							,UsrFirstName
							,UsrLastName
							,UsrRole
							,UsrStatus
							,CONCAT_WS('', UsrLastName,' ', UsrFirstName) AS UsrFullName 
						
							FROM sys_usr
							
							WHERE
							(
								UsrFirstName LIKE ?
								OR
								UsrLastName LIKE ?
								
							)
							AND
							(
								? = 1
								OR
								UsrStatus = 1
							)
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("ssi", $filterStrToFind, $filterStrToFind, $filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "getList: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	//////////////////// LOG \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::get_LoginUsrID
	*
	* input params:
	*	$username 			string		:: login input
	*	$password 			string		:: password input
	*
	* return:
	*	$UsrID				int			:: user id 
	* --------------------------------------------------------------------------
	*/
	public static function get_LoginUsrID($appFrw, $username, $password)
	{
		$UsrID = 0;
		
		$hashedpassword = md5($password);
		
		$query = "	SELECT
						UsrID
					FROM sys_usr
					WHERE 
					(
						UsrLogin=?
						AND 
						UsrPassword=?
						AND 
						UsrStatus != 2 
					)
					LIMIT 1";
					
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) return 0;

		// bind input vars
		$stmt->bind_param("ss", $username, $hashedpassword);
		
		
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		
		if(!$result) return 0;
		
		$row = $result->fetch_assoc();
		$UsrID = $row['UsrID'];
		$result->close();
		
		return $UsrID;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_usr::get_UsrDetails
	*
	* input params:
	*	$UsrID 			:: user id
	*
	* return:
	*	array
	*	[ 
	*		UsrID		:: user id
	*		UsrLogin	:: user login
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function get_UsrDetails($appFrw, $UsrID)
	{
		if(!isset($UsrID) OR $UsrID <= 0)
			return;
		
		$row = array();
		
		$query = "	SELECT
						 UsrID
						,UsrLogin
						,CONCAT_WS('', UsrLastName,' ', UsrFirstName) AS UsrFullName 
						,UsrRole
					FROM sys_usr
					WHERE UsrID=".(int)$UsrID;
		
		$result = $appFrw->DB_Link->query($query) or die( $appFrw->DB_Link->error);
		
		if ($result) 
		{
			if($result->num_rows == 1)
			{
				$row = mysqli_fetch_assoc($result);				
			}
			$result->close();
		}
		
		return $row;
	}
	
}
