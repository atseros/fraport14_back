<?php

/*********************************************************************************************
* CLASS DB_sys_kxn
*
* DESCRIPTION: 
*	Class for table sys_kxn
*
* table fields:
*
*	KxnID			int(11)
*	KxnInc			int(11)
*	KxnTableName	varchar(64)
*	KxnTableDescr	varchar(512)
*
*********************************************************************************************/
class DB_sys_kxn
{
	/*
	* --------------------------------------------------------------------------
	* DB_sys_kxn::get_NextID
	*
	* input params:
	*	$tableName 		string 		:: db table name
	*
	* return:
	*	$nextID			int			:: next record id 
	* --------------------------------------------------------------------------
	*/
	public static function get_NextID($appFrw, $tableName)
	{	
		$newID = 0;

        // get next id
        $query = "	SELECT KxnInc FROM sys_kxn WHERE KxnTableName=? LIMIT 1";


		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) exit("error at getting new id for table: ".$tableName." ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("s", $tableName);
		
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		
		if(!$result) exit("error at getting new id for table: ".$tableName." ".$stmt->error);
		
		$row = $result->fetch_assoc();
		$newID = $row['KxnInc'];
		$result->close();
		
		if($newID == 0) exit("no table ".$tableName." found in sys_kxn");

		// update next id for next time 
		$nextID = $newID + 1;

		$query ="UPDATE sys_kxn SET KxnInc=? WHERE KxnTableName=?";


		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) exit("error at updating next id for table: ".$tableName." ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("is", $nextID, $tableName);
		
		if(!$stmt->execute())
			 exit("error at updating next id for table: ".$tableName." ".$stmt->error);

		$stmt->close();
		
		
		// return 
		return $newID;
	}

    /*
    * --------------------------------------------------------------------------
    * DB_sys_kxn::get_NextID_mass
    *
    * input params:
    *	$tableName 		string 		:: db table name
    *
    * return:
    *	$nextID			int			:: next record id
    * --------------------------------------------------------------------------
    */
    public static function get_NextID_mass($appFrw, $tableName, $mass)
    {
        $newID = 0;

        // get next id
        $query = "	SELECT KxnInc FROM sys_kxn WHERE KxnTableName=? LIMIT 1";



        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt) exit("error at getting new id for table: ".$tableName." ".$appFrw->DB_Link->error);

        $stmt->bind_param("s", $tableName);

        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        if(!$result) exit("error at getting new id for table: ".$tableName." ".$stmt->error);

        $row = $result->fetch_assoc();
        $newID = $row['KxnInc'];
        $result->close();

        if($newID == 0) exit("no table ".$tableName." found in sys_kxn");

        // update next id for next time
        $nextID = $newID + $mass;

        $query ="UPDATE sys_kxn SET KxnInc=? WHERE KxnTableName=?";



        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt) exit("error at updating next id for table: ".$tableName." ".$appFrw->DB_Link->error);

        $stmt->bind_param("is", $nextID, $tableName);

        if(!$stmt->execute())
            exit("error at updating next id for table: ".$tableName." ".$stmt->error);

        $stmt->close();


        // return
        return $newID;
    }
	
	
	
}
