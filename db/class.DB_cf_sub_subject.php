<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_cf_sub_subject
*
* DESCRIPTION: 
*	Class for table cf_sub_subject
*
* table fields:
*
 `SubSubID` int(11) NOT NULL,
 `SubSubSubID` int(11) NOT NULL,
 `SubSubTitle` varchar(512) NOT NULL,
 `SubSubPriority` int(11) NOT NULL,
 `SubSubStatus` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_cf_sub_subject
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject::cf_sub_subject_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_get_NewRecordDefValues($appFrw, $params)
	{
		$SubID 	= (int)$params["SubID"];
		
		$SubSubID = DB_sys_kxn::get_NextID($appFrw, 'cf_sub_subject');
		
		if($SubSubID > 0)
		{
			$results["success"] = true;
			$results["data"]["SubSubID"]        = $SubSubID;
			$results["data"]["SubSubSubID"]     = $SubID;
			$results["data"]["SubSubStatus"]    = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table cf_sub_subject";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SubSubID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SubSubID FROM cf_sub_subject WHERE SubSubID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SubSubID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject::cf_sub_subject_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SubSubID 	= (int)$params["SubSubID"];
		
		if($SubSubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_cf_sub_subject::check_RecordExists($appFrw, $SubSubID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SubSubID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO cf_sub_subject
					(
						 SubSubID
					)
					VALUES
					(
						 ?
					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SubSubID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_cf_sub_subject::cf_sub_subject_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject::cf_sub_subject_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SubSubID = (int)$params["SubSubID"];
		
		if($SubSubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_cf_sub_subject::check_RecordExists($appFrw, $SubSubID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SubSubID;
			return $results;
		}
		
		$query = "	SELECT
						
						SubSubID
						,SubSubSubID
						,SubSubTitle						
						,SubSubPriority				
						,SubSubStatus				
							
					FROM cf_sub_subject
					
					WHERE
					SubSubID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SubSubID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject::cf_sub_subject_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SubSubID = (int)$params["SubSubID"];
		
		
		if($SubSubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_cf_sub_subject::cf_sub_subject_getRecord($appFrw, array('SubSubID'=>$SubSubID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$SubSubSubID 		= (isset($params['SubSubSubID'])) ? $params['SubSubSubID'] : $record['SubSubSubID'];
		$SubSubTitle 		= (isset($params['SubSubTitle'])) ? $params['SubSubTitle'] : $record['SubSubTitle'];
		$SubSubPriority 	= (isset($params['SubSubPriority'])) ? $params['SubSubPriority'] : $record['SubSubPriority'];
        $SubSubStatus 		= (isset($params['SubSubStatus'])) ? $params['SubSubStatus'] : $record['SubSubStatus'];
	
		$query = "	UPDATE cf_sub_subject SET
						 SubSubSubID 		= ?
						,SubSubTitle 		= ?
						,SubSubPriority 	= ?
						,SubSubStatus 		= ?
					WHERE
					SubSubID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("isiii",
								 $SubSubSubID 			
								,$SubSubTitle 
								,$SubSubPriority 
								,$SubSubStatus
								,$SubSubID 
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SubSubID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
//	public static function RecordCanBeDeleted($appFrw, $params)
//	{
//		$results = array();
//
//		$SubSubID = (int)$params["SubSubID"];
//		if($SubSubID <= 0)
//		{
//			$results["success"] = false;
//			$results["reason"] = "get_cf_sub_subject_lang_RecordCanBeDeleted: No id found. Can not check record";
//			return $results;
//		}
//
//		// Check related records
//		$query = "	SELECT
//						 case when( exists (SELECT SubSubLngID FROM cf_sub_subject_lang WHERE SubSubLngSubSubID = ?))
//							then 1
//							else 0
//						 end as TranslationsExists
//
//						 ,case when( exists (SELECT AnsLngID FROM cf_answers_lang WHERE AnsLngSubSubID = ?))
//							then 1
//							else 0
//						 end as AnswerExists
//
//				";
//
//		$stmt = $appFrw->DB_Link->prepare($query);
//
//		if(!$stmt)
//			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
//
//		$stmt->bind_param("ii", $SubSubID, $SubSubID);
//
//		if(!$stmt->execute())
//			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
//
//		$result = $stmt->get_result();
//		$stmt->close();
//
//		if(!$result)
//			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
//
//		$row = $result->fetch_assoc();
//		$result->close();
//
//		// Reason
//		$results["reason"] = "";
//		if( $row["TranslationsExists"] == 1)
//		{
//			$results["success"] = false;
//			$results["reason"] = "There are translations related to the sub subject";
//
//			return $results;
//		}
//		if( $row["AnswerExists"] == 1)
//		{
//			$results["success"] = false;
//			$results["reason"] = "There are answer related to the sub subject";
//			return $results;
//		}
//
//		// ... continue
//
//		$results["success"] = true;
//		return $results;
//
//	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject::cf_sub_subject_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SubSubID = (int)$params["SubSubID"];
		
		if($SubSubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		 //check if can be deleted
//		 $canBeDeleted = DB_cf_sub_subject::RecordCanBeDeleted($appFrw, array('SubSubID'=>$SubSubID) );
//		 if($canBeDeleted["success"] == false)
//		 {
//			 $results["success"] = false;
//			 $results["reason"] = $canBeDeleted["reason"];
//
//			 return $results;
//		 }
		
		$query = "DELETE FROM cf_sub_subject WHERE SubSubID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SubSubID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SubSubID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject::cf_sub_subject_get_List
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_get_List($appFrw, $params)
	{
		$SubID	            = isset($params["SubID"]) ? (int)$params["SubID"] : 0;
		$filterShowAll	    = isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
		$filterStrToFind    = isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		
		$results['data'] = array();
		
				
		$query = "	SELECT
						
                         SubSubID
                        ,SubSubSubID
                        ,SubSubTitle
                        ,SubSubPriority
                        ,SubSubStatus
					
					FROM cf_sub_subject
					
					WHERE 
					(
						SubSubSubID = ?
					)
					AND
					(
						SubSubTitle LIKE ?
					)
					AND
					(
						? = 1
						OR
						SubSubStatus = 1
					)
					
					ORDER BY SubSubPriority ASC
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "cf_sub_subject_get_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $SubID, $filterStrToFind, $filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "cf_sub_subject_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "cf_sub_subject_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
}
