<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_node_related_link
*
* DESCRIPTION: 
*	Class for table sys_node_related_link
*
* table fields:
*
 `NodeRelatedLinkID` int(11) NOT NULL,
 `NodeRelatedLinkNodeID` int(11) NOT NULL,
 `NodeRelatedLinkRelatedLinkID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_node_related_link
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::sys_node_related_link_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_related_link_get_NewRecordDefValues($appFrw, $params)
	{	
		$NodeRelatedLinkID = DB_sys_kxn::get_NextID($appFrw, 'sys_node_related_link');
		
		if($NodeRelatedLinkID > 0)
		{
			$results["success"] = true;
			$results["data"]["NodeRelatedLinkID"] = $NodeRelatedLinkID;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_node_related_link";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $NodeRelatedLinkID)
	{
		$query = "	SELECT
						   case when( exists (SELECT NodeRelatedLinkID FROM sys_node_related_link WHERE NodeRelatedLinkID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeRelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::sys_node_related_link_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_related_link_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeRelatedLinkID 	= (int)$params["NodeRelatedLinkID"];
		
		if($NodeRelatedLinkID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_node_related_link::check_RecordExists($appFrw, $NodeRelatedLinkID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$NodeRelatedLinkID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_node_related_link
					(
						 NodeRelatedLinkID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeRelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_node_related_link::sys_node_related_link_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::sys_node_related_link_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_related_link_getRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeRelatedLinkID = (int)$params["NodeRelatedLinkID"];
		
		if($NodeRelatedLinkID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_node_related_link::check_RecordExists($appFrw, $NodeRelatedLinkID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$NodeRelatedLinkID;
			return $results;
		}
		
		$query = "	SELECT
						
						NodeRelatedLinkID
						,NodeRelatedLinkNodeID
						,NodeRelatedLinkRelatedLinkID
						,NodeRelatedLinkPriority
							
					FROM sys_node_related_link
					WHERE
					NodeRelatedLinkID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeRelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::sys_node_related_link_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_related_link_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeRelatedLinkID = (int)$params["NodeRelatedLinkID"];
		
		
		if($NodeRelatedLinkID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_node_related_link::sys_node_related_link_getRecord($appFrw, array('NodeRelatedLinkID'=>$NodeRelatedLinkID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$NodeRelatedLinkNodeID 	= (isset($params['NodeRelatedLinkNodeID'])) ? $params['NodeRelatedLinkNodeID'] : $record['NodeRelatedLinkNodeID'];
		$NodeRelatedLinkRelatedLinkID 	= (isset($params['NodeRelatedLinkRelatedLinkID'])) ? $params['NodeRelatedLinkRelatedLinkID'] : $record['NodeRelatedLinkRelatedLinkID'];
		$NodeRelatedLinkPriority 	= (isset($params['NodeRelatedLinkPriority'])) ? $params['NodeRelatedLinkPriority'] : $record['NodeRelatedLinkPriority'];
		
		$query = "	UPDATE sys_node_related_link SET
							
							NodeRelatedLinkNodeID 	= ?
							,NodeRelatedLinkRelatedLinkID 	= ?
							,NodeRelatedLinkPriority 	= ?
						
							WHERE
							NodeRelatedLinkID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiii",
								 $NodeRelatedLinkNodeID 			
								,$NodeRelatedLinkRelatedLinkID 																
								,$NodeRelatedLinkPriority 																
								,$NodeRelatedLinkID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $NodeRelatedLinkID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::sys_node_related_link_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_related_link_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeRelatedLinkID = (int)$params["NodeRelatedLinkID"];
		
		if($NodeRelatedLinkID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		$query = "DELETE FROM sys_node_related_link WHERE NodeRelatedLinkID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeRelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $NodeRelatedLinkID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::sys_node_related_link_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_related_link_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeID	= isset($params["NodeID"]) ? (int)$params["NodeID"] : 0;
				
		$query = "	SELECT

							NodeRelatedLinkID
							,NodeRelatedLinkNodeID
							,NodeRelatedLinkRelatedLinkID
							,NodeRelatedLinkPriority
							,RelatedLinkTitle
							,RelatedLinkFIlename
							,RelatedLinkType
							,RelatedLinkID
							,NodeTitle
						
							FROM sys_node_related_link
							LEFT JOIN sys_related_links ON ( RelatedLinkID = NodeRelatedLinkRelatedLinkID )
							LEFT JOIN sys_node ON ( NodeID = RelatedLinkNodeID )
							
							WHERE
							(
								NodeRelatedLinkNodeID  = ?
							)
							
							ORDER BY NodeRelatedLinkPriority ASC
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i",$NodeID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['RelatedLinkFullUrl']= DB_sys_node_related_link::get_sys_node_related_link_FullUrl($appFrw, $row['RelatedLinkID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::get_sys_node_related_link_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_node_related_link_FullUrl($appFrw, $RelatedLinkID)
	{
		$url = FileManager::getTblFolderUrl("sys_related_links", $RelatedLinkID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_related_links::sys_related_links_getRecord($appFrw,array('RelatedLinkID'=>$RelatedLinkID));
		if( $imageData["success"] == true  && $imageData["data"]["RelatedLinkFIlename"] )
		{
			$FulUrl = $url.$imageData["data"]["RelatedLinkFIlename"];
		}
		return $FulUrl;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::sys_node_related_link_getListSitePages
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_related_link_getListSitePages($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeID	= isset($params["NodeID"]) ? (int)$params["NodeID"] : 0;
		$NodeLngType	= isset($params["NodeLngType"]) ? (int)$params["NodeLngType"] : -1;
				
		$query = "	SELECT

							NodeRelatedLinkID
							,NodeRelatedLinkNodeID
							,NodeRelatedLinkRelatedLinkID
							,NodeRelatedLinkPriority
							,RelatedLinkLngTitle as RelatedLinkTitle
							,RelatedLinkFIlename
							,RelatedLinkType
							,RelatedLinkID
							,NodeTitle
						
							FROM sys_node_related_link
							LEFT JOIN sys_related_links ON ( RelatedLinkID = NodeRelatedLinkRelatedLinkID )
							LEFT JOIN sys_node ON ( NodeID = RelatedLinkNodeID )
							LEFT JOIN sys_related_linkslng ON ( RelatedLinkLngRelatedLinkID = RelatedLinkID )
							
							WHERE
							(
								NodeRelatedLinkNodeID  = ?
							)
							AND
							(
								RelatedLinkLngType = ?
							)
							AND
							(
								RelatedLinkLngTitle != ''
							)
							
							

							ORDER BY NodeRelatedLinkPriority ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("ii", $NodeID,$NodeLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['RelatedLinkFullUrl']= DB_sys_node_related_link::get_sys_node_related_link_FullUrl($appFrw, $row['RelatedLinkID']);
			array_push($results['data'], $row);
		}
		$result->close();
	
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_related_link::sys_node_related_link_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_related_link_CheckBeforeInsert($appFrw, $params)
	{
		$NodeRelatedLinkNodeID	= isset($params["NodeRelatedLinkNodeID"]) ? (int)$params["NodeRelatedLinkNodeID"] : 0;
		$NodeRelatedLinkRelatedLinkID	= isset($params["NodeRelatedLinkRelatedLinkID"]) ? (int)$params["NodeRelatedLinkRelatedLinkID"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT NodeRelatedLinkID FROM sys_node_related_link WHERE NodeRelatedLinkNodeID = ? AND NodeRelatedLinkRelatedLinkID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $NodeRelatedLinkNodeID, $NodeRelatedLinkRelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
}
