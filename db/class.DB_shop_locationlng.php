<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS class.DB_shop_locationlng
*
* DESCRIPTION: 
*	Class for table shop_locationlng
*
* table fields:
*
 `LocationLngID` int(11) NOT NULL,
 `LocationLngLocationID` int(11) NOT NULL,
 `LocationLngType` int(11) NOT NULL,
 `LocationLngLocation` varchar(1024) NOT NULL,
 `LocationLngOpeningTimes` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_shop_locationlng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_locationlng::shop_locationlng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function shop_locationlng_get_NewRecordDefValues($appFrw, $params)
	{	
		$LocationLngID = DB_sys_kxn::get_NextID($appFrw, 'shop_locationlng');
		
		if($LocationLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["LocationLngID"] = $LocationLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table shop_locationlng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_locationlng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $LocationLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT LocationLngID FROM shop_locationlng WHERE LocationLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $LocationLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_locationlng::shop_locationlng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function shop_locationlng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$LocationLngID 	= (int)$params["LocationLngID"];
		
		if($LocationLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_shop_locationlng::check_RecordExists($appFrw, $LocationLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$LocationLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO shop_locationlng
					(
						 LocationLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $LocationLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_shop_locationlng::shop_locationlng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_shop_locationlng::shop_locationlng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function shop_locationlng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$LocationLngID = (int)$params["LocationLngID"];
		$LocationLngType = $params["LocationLngType"];
		
		if($LocationLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_shop_locationlng::check_RecordExists($appFrw, $LocationLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$LocationLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						LocationLngID
						,LocationLngLocationID 
						,LocationLngType 
						,LocationLngLocation 
						
							
					FROM shop_locationlng
					WHERE
					(
						LocationLngID = ?
					)
					AND
					(
						LocationLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $LocationLngID, $LocationLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_shop_locationlng::shop_locationlng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function shop_locationlng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$LocationLngID = (int)$params["LocationLngID"];
		$LocationLngType 			= (isset($params['LocationLngType'])) ? $params['LocationLngType'] : $record['LocationLngType'];
		
		if($LocationLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_shop_locationlng::shop_locationlng_getRecord($appFrw, array('LocationLngID'=>$LocationLngID,'LocationLngType'=>$LocationLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$LocationLngLocationID 	= (isset($params['LocationLngLocationID'])) ? $params['LocationLngLocationID'] : $record['LocationLngLocationID'];
		$LocationLngLocation 	= (isset($params['LocationLngLocation'])) ? $params['LocationLngLocation'] : $record['LocationLngLocation'];
		// $LocationLngOpeningTimes 	= (isset($params['LocationLngOpeningTimes'])) ? $params['LocationLngOpeningTimes'] : $record['LocationLngOpeningTimes'];
		
		$query = "	UPDATE shop_locationlng SET
						 LocationLngLocationID 					= ?
						,LocationLngType 					= ?
						,LocationLngLocation 					= ?
						
						
					WHERE
					LocationLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iisi",
								 $LocationLngLocationID 			
								,$LocationLngType 
								,$LocationLngLocation 
								// ,$LocationLngOpeningTimes 
								,$LocationLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $LocationLngID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::RecordCanBeDeleted
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be checked
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_site WHERE SiteID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SiteID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeLngType	= isset($params["filterLanguage"]) ? (int)$params["filterLanguage"] : 1;
				
		$query = "	SELECT
						
							NodeID
							,NodeSiteID
							,NodeNodeID
							,NodeLevel
							,NodeCode
							,NodeParentCode
							,NodeFullCode
							,NodeTitle
							,NodeUrlAlias
							,NodePublished
							,NodeIsHomePage
							,NodeTemplateFile
							,NodeCreatedAt
							,NodeLngTitle
							,NodeLngStory
							,NodeLngIntro
							,CONCAT (NodeFullCode, ' - ', NodeLngTitle) AS NodeLngTitleFullCode
									
							FROM sys_node
							LEFT JOIN sys_nodelng ON ( NodeLngNodeID = NodeID )
							
							WHERE
							(
								NodeLngType = ?
							)
							
							
							ORDER BY NodeFullCode ASC
							
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $NodeLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_locationlng::shop_locationlng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function shop_locationlng_CheckBeforeInsert($appFrw, $params)
	{
		$LocationLngLocationID	= isset($params["LocationLngLocationID"]) ? (int)$params["LocationLngLocationID"] : 0;
		$LocationLngType	= isset($params["LocationLngType"]) ? (int)$params["LocationLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT LocationLngID FROM shop_locationlng WHERE LocationLngLocationID = ? AND LocationLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $LocationLngLocationID, $LocationLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_shop_locationlng::shop_locationlng_GetLocationLngID
	* --------------------------------------------------------------------------
	*/
	public static function shop_locationlng_GetLocationLngID($appFrw, $params)
	{
		$LocationLngLocationID	= isset($params["LocationLngLocationID"]) ? (int)$params["LocationLngLocationID"] : 0;
		$LocationLngType	= isset($params["LocationLngType"]) ? (int)$params["LocationLngType"] : 0;
		
		$query = "	SELECT
						   LocationLngID
						   FROM shop_locationlng
							WHERE LocationLngLocationID = ? AND LocationLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $LocationLngLocationID, $LocationLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["LocationLngID"];
	}
	
	
	
}
