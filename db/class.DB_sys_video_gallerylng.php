<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_video_gallerylng
*
* DESCRIPTION: 
*	Class for table sys_video_gallerylng
*
* table fields:
*
 `VidLngID` int(11) NOT NULL,
 `VidLngVidID` int(11) NOT NULL,
 `VidLngTitle` varchar(1024) NOT NULL,
 `VidLngType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_video_gallerylng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallerylng::sys_video_gallerylng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_video_gallerylng_get_NewRecordDefValues($appFrw, $params)
	{	
		$VidLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_video_gallerylng');
		
		if($VidLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["VidLngID"] = $VidLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_video_gallerylng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallerylng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $VidLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT VidLngID FROM sys_video_gallerylng WHERE VidLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $VidLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallerylng::sys_video_gallerylng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallerylng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$VidLngID 	= (int)$params["VidLngID"];
		
		if($VidLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_video_gallerylng::check_RecordExists($appFrw, $VidLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$VidLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_video_gallerylng
					(
						 VidLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $VidLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_video_gallerylng::sys_video_gallerylng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallerylng::sys_video_gallerylng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallerylng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$VidLngID = (int)$params["VidLngID"];
		$VidLngType = $params["VidLngType"];
		
		if($VidLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_video_gallerylng::check_RecordExists($appFrw, $VidLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$VidLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						VidLngID
						,VidLngVidID 
						,VidLngTitle 
						,VidLngType 
						,VidLngSubtitle 
						,VidLngDescription 
							
					FROM sys_video_gallerylng
					WHERE
					(
						VidLngID = ?
					)
					AND
					(
						VidLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $VidLngID, $VidLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallerylng::sys_video_gallerylng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallerylng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$VidLngID = (int)$params["VidLngID"];
		$VidLngType 	= (isset($params['VidLngType'])) ? $params['VidLngType'] : $record['VidLngType'];
		
		if($VidLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_video_gallerylng::sys_video_gallerylng_getRecord($appFrw, array('VidLngID'=>$VidLngID,'VidLngType'=>$VidLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$VidLngTitle 	= (isset($params['VidLngTitle'])) ? $params['VidLngTitle'] : $record['VidLngTitle'];
		$VidLngVidID 	= (isset($params['VidLngVidID'])) ? $params['VidLngVidID'] : $record['VidLngVidID'];
		$VidLngSubtitle 	= (isset($params['VidLngSubtitle'])) ? $params['VidLngSubtitle'] : $record['VidLngSubtitle'];
		$VidLngDescription 	= (isset($params['VidLngDescription'])) ? $params['VidLngDescription'] : $record['VidLngDescription'];
		
		$query = "	UPDATE sys_video_gallerylng SET
						 VidLngTitle 					= ?
						,VidLngType 					= ?
						,VidLngVidID 		= ?
						,VidLngSubtitle 		= ?
						,VidLngDescription 		= ?
						
					WHERE
					VidLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siissi",
								 $VidLngTitle 			
								,$VidLngType 
								,$VidLngVidID 
								,$VidLngSubtitle 
								,$VidLngDescription 
								,$VidLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $VidLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallerylng::sys_video_gallerylng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallerylng_CheckBeforeInsert($appFrw, $params)
	{
		$VidLngVidID	= isset($params["VidLngVidID"]) ? (int)$params["VidLngVidID"] : 0;
		$VidLngType	= isset($params["VidLngType"]) ? (int)$params["VidLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT VidLngID FROM sys_video_gallerylng WHERE VidLngVidID = ? AND VidLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $VidLngVidID, $VidLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallerylng::sys_video_gallerylng_GetVidLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallerylng_GetVidLngID($appFrw, $params)
	{
		$VidLngVidID	= isset($params["VidLngVidID"]) ? (int)$params["VidLngVidID"] : 0;
		$VidLngType	= isset($params["VidLngType"]) ? (int)$params["VidLngType"] : 0;
		
		$query = "	SELECT
						   VidLngID
						   FROM sys_video_gallerylng
							WHERE VidLngVidID = ? AND VidLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $VidLngVidID, $VidLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["VidLngID"];
	}
	
	
	
}
