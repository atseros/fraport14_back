<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_dictionarylng
*
* DESCRIPTION: 
*	Class for table sys_dictionarylng
*
* table fields:
*
 `DicLngID` int(11) NOT NULL,
 `DicLngDicID` int(11) NOT NULL,
 `DicLngName` varchar(1024) NOT NULL,
 `DicLngType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_dictionarylng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionarylng::sys_dictionarylng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_dictionarylng_get_NewRecordDefValues($appFrw, $params)
	{	
		$DicLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_dictionarylng');
		
		if($DicLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["DicLngID"] = $DicLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_dictionarylng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionarylng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $DicLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT DicLngID FROM sys_dictionarylng WHERE DicLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $DicLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionarylng::sys_dictionarylng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionarylng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$DicLngID 	= (int)$params["DicLngID"];
		
		if($DicLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_dictionarylng::check_RecordExists($appFrw, $DicLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$DicLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_dictionarylng
					(
						 DicLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $DicLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_dictionarylng::sys_dictionarylng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionarylng::sys_dictionarylng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionarylng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$DicLngID = (int)$params["DicLngID"];
		$DicLngType = $params["DicLngType"];
		
		if($DicLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_dictionarylng::check_RecordExists($appFrw, $DicLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$DicLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						DicLngID
						,DicLngDicID 
						,DicLngName 
						,DicLngType 
							
					FROM sys_dictionarylng
					WHERE
					(
						DicLngID = ?
					)
					AND
					(
						DicLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $DicLngID, $DicLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionarylng::sys_dictionarylng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionarylng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$DicLngID = (int)$params["DicLngID"];
		$DicLngType 	= (isset($params['DicLngType'])) ? $params['DicLngType'] : $record['DicLngType'];
		
		if($DicLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_dictionarylng::sys_dictionarylng_getRecord($appFrw, array('DicLngID'=>$DicLngID,'DicLngType'=>$DicLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$DicLngName 	= (isset($params['DicLngName'])) ? $params['DicLngName'] : $record['DicLngName'];
		$DicLngDicID 	= (isset($params['DicLngDicID'])) ? $params['DicLngDicID'] : $record['DicLngDicID'];
		
		$query = "	UPDATE sys_dictionarylng SET
						 DicLngName 					= ?
						,DicLngType 					= ?
						,DicLngDicID 		= ?
						
					WHERE
					DicLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siii",
								 $DicLngName 			
								,$DicLngType 
								,$DicLngDicID 
								,$DicLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $DicLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionarylng::sys_dictionarylng_CheckBeforeInsert
	*/
	public static function sys_dictionarylng_CheckBeforeInsert($appFrw, $params)
	{
		$DicLngDicID	= isset($params["DicLngDicID"]) ? (int)$params["DicLngDicID"] : 0;
		$DicLngType	= isset($params["DicLngType"]) ? (int)$params["DicLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT DicLngID FROM sys_dictionarylng WHERE DicLngDicID = ? AND DicLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $DicLngDicID, $DicLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionarylng::sys_dictionarylng_GetDicLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionarylng_GetDicLngID($appFrw, $params)
	{
		$DicLngDicID	= isset($params["DicLngDicID"]) ? (int)$params["DicLngDicID"] : 0;
		$DicLngType	= isset($params["DicLngType"]) ? (int)$params["DicLngType"] : 0;
		
		$query = "	SELECT
						   DicLngID
						   FROM sys_dictionarylng
							WHERE DicLngDicID = ? AND DicLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $DicLngDicID, $DicLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["DicLngID"];
	}
	
	
	
}
