<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_video_gallery_datalng
*
* DESCRIPTION: 
*	Class for table sys_video_gallery_datalng
*
* table fields:
*
 `VidDataLngID` int(11) NOT NULL,
 `VidDataLngVidDataID` int(11) NOT NULL,
 `VidDataLngTitle` varchar(1024) NOT NULL,
 `VidDataLngType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_video_gallery_datalng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery_datalng::sys_video_gallery_datalng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_video_gallery_datalng_get_NewRecordDefValues($appFrw, $params)
	{	
		$VidDataLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_video_gallery_datalng');
		
		if($VidDataLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["VidDataLngID"] = $VidDataLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_video_gallery_datalng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery_datalng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $VidDataLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT VidDataLngID FROM sys_video_gallery_datalng WHERE VidDataLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $VidDataLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery_datalng::sys_video_gallery_datalng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_datalng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$VidDataLngID 	= (int)$params["VidDataLngID"];
		
		if($VidDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_video_gallery_datalng::check_RecordExists($appFrw, $VidDataLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$VidDataLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_video_gallery_datalng
					(
						 VidDataLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $VidDataLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery_datalng::sys_video_gallery_datalng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_datalng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$VidDataLngID = (int)$params["VidDataLngID"];
		$VidDataLngType = $params["VidDataLngType"];
		
		if($VidDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_video_gallery_datalng::check_RecordExists($appFrw, $VidDataLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$VidDataLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						VidDataLngID
						,VidDataLngVidDataID 
						,VidDataLngTitle 
						,VidDataLngType 
						,VidDataLngSubtitle 
						,VidDataLngDescription 
							
					FROM sys_video_gallery_datalng
					WHERE
					(
						VidDataLngID = ?
					)
					AND
					(
						VidDataLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $VidDataLngID, $VidDataLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery_datalng::sys_video_gallery_datalng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_datalng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$VidDataLngID = (int)$params["VidDataLngID"];
		$VidDataLngType 	= (isset($params['VidDataLngType'])) ? $params['VidDataLngType'] : $record['VidDataLngType'];
		
		if($VidDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_getRecord($appFrw, array('VidDataLngID'=>$VidDataLngID,'VidDataLngType'=>$VidDataLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$VidDataLngTitle 	= (isset($params['VidDataLngTitle'])) ? $params['VidDataLngTitle'] : $record['VidDataLngTitle'];
		$VidDataLngVidDataID 	= (isset($params['VidDataLngVidDataID'])) ? $params['VidDataLngVidDataID'] : $record['VidDataLngVidDataID'];
		$VidDataLngSubtitle 	= (isset($params['VidDataLngSubtitle'])) ? $params['VidDataLngSubtitle'] : $record['VidDataLngSubtitle'];
		$VidDataLngDescription 	= (isset($params['VidDataLngDescription'])) ? $params['VidDataLngDescription'] : $record['VidDataLngDescription'];
		
		$query = "	UPDATE sys_video_gallery_datalng SET
						 VidDataLngTitle 					= ?
						,VidDataLngType 					= ?
						,VidDataLngVidDataID 		= ?
						,VidDataLngSubtitle 		= ?
						,VidDataLngDescription 		= ?
						
					WHERE
					VidDataLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siissi",
								 $VidDataLngTitle 			
								,$VidDataLngType 
								,$VidDataLngVidDataID 
								,$VidDataLngSubtitle 
								,$VidDataLngDescription 
								,$VidDataLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $VidDataLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery_datalng::sys_video_gallery_datalng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_datalng_CheckBeforeInsert($appFrw, $params)
	{
		$VidDataLngVidDataID	= isset($params["VidDataLngVidDataID"]) ? (int)$params["VidDataLngVidDataID"] : 0;
		$VidDataLngType	= isset($params["VidDataLngType"]) ? (int)$params["VidDataLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT VidDataLngID FROM sys_video_gallery_datalng WHERE VidDataLngVidDataID = ? AND VidDataLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $VidDataLngVidDataID, $VidDataLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery_datalng::sys_video_gallery_datalng_GetVidDataLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_datalng_GetVidDataLngID($appFrw, $params)
	{
		$VidDataLngVidDataID	= isset($params["VidDataLngVidDataID"]) ? (int)$params["VidDataLngVidDataID"] : 0;
		$VidDataLngType	= isset($params["VidDataLngType"]) ? (int)$params["VidDataLngType"] : 0;
		
		$query = "	SELECT
						   VidDataLngID
						   FROM sys_video_gallery_datalng
							WHERE VidDataLngVidDataID = ? AND VidDataLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $VidDataLngVidDataID, $VidDataLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["VidDataLngID"];
	}
	
	
	
}
