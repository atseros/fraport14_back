<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS class.DB_sys_medialng
*
* DESCRIPTION: 
*	Class for table sys_medialng
*
* table fields:
*
 `MediaLngID` int(11) NOT NULL,
 `MediaLngTitle` varchar(1024) NOT NULL,
 `MediaLngType` int(11) NOT NULL,
 `MediaLngMediaID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_medialng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_medialng::sys_medialng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_medialng_get_NewRecordDefValues($appFrw, $params)
	{	
		$MediaLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_medialng');
		
		if($MediaLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["MediaLngID"] = $MediaLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_shoplng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_medialng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $MediaLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT MediaLngID FROM sys_medialng WHERE MediaLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $MediaLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_medialng::sys_medialng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_medialng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$MediaLngID 	= (int)$params["MediaLngID"];
		
		if($MediaLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_medialng::check_RecordExists($appFrw, $MediaLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$MediaLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_medialng
					(
						 MediaLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $MediaLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_medialng::sys_medialng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_medialng::sys_medialng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_medialng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$MediaLngID = (int)$params["MediaLngID"];
		$MediaLngType = $params["MediaLngType"];
		
		if($MediaLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_medialng::check_RecordExists($appFrw, $MediaLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$MediaLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						MediaLngID
						,MediaLngTitle 
						,MediaLngType 
						,MediaLngMediaID 
						,MediaLngDescription 
							
					FROM sys_medialng
					WHERE
					(
						MediaLngID = ?
					)
					AND
					(
						MediaLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $MediaLngID, $MediaLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_medialng::sys_medialng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_medialng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$MediaLngID = (int)$params["MediaLngID"];
		$MediaLngType 	= (isset($params['MediaLngType'])) ? $params['MediaLngType'] : $record['MediaLngType'];
		
		if($MediaLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_medialng::sys_medialng_getRecord($appFrw, array('MediaLngID'=>$MediaLngID,'MediaLngType'=>$MediaLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$MediaLngMediaID 		= (isset($params['MediaLngMediaID'])) ? $params['MediaLngMediaID'] : $record['MediaLngMediaID'];
		$MediaLngTitle 			= (isset($params['MediaLngTitle'])) ? $params['MediaLngTitle'] : $record['MediaLngTitle'];
		$MediaLngDescription 	= (isset($params['MediaLngDescription'])) ? $params['MediaLngDescription'] : $record['MediaLngDescription'];
		
		
		$query = "	UPDATE sys_medialng SET
						 MediaLngMediaID 					= ?
						,MediaLngType 					= ?
						,MediaLngTitle 					= ?
						,MediaLngDescription 					= ?
						
					WHERE
					MediaLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iissi",
								 $MediaLngMediaID 			
								,$MediaLngType 
								,$MediaLngTitle 
								,$MediaLngDescription 
								,$MediaLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $MediaLngID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::RecordCanBeDeleted
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be checked
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_site WHERE SiteID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SiteID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeLngType	= isset($params["filterLanguage"]) ? (int)$params["filterLanguage"] : 1;
				
		$query = "	SELECT
						
							NodeID
							,NodeSiteID
							,NodeNodeID
							,NodeLevel
							,NodeCode
							,NodeParentCode
							,NodeFullCode
							,NodeTitle
							,NodeUrlAlias
							,NodePublished
							,NodeIsHomePage
							,NodeTemplateFile
							,NodeCreatedAt
							,NodeLngTitle
							,NodeLngStory
							,NodeLngIntro
							,CONCAT (NodeFullCode, ' - ', NodeLngTitle) AS NodeLngTitleFullCode
									
							FROM sys_node
							LEFT JOIN sys_nodelng ON ( NodeLngNodeID = NodeID )
							
							WHERE
							(
								NodeLngType = ?
							)
							
							
							ORDER BY NodeFullCode ASC
							
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $NodeLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_medialng::sys_medialng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_medialng_CheckBeforeInsert($appFrw, $params)
	{
		$MediaLngMediaID	= isset($params["MediaLngMediaID"]) ? (int)$params["MediaLngMediaID"] : 0;
		$MediaLngType	= isset($params["MediaLngType"]) ? (int)$params["MediaLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT MediaLngID FROM sys_medialng WHERE MediaLngMediaID = ? AND MediaLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $MediaLngMediaID, $MediaLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_medialng::sys_medialng_GetMediaLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_medialng_GetMediaLngID($appFrw, $params)
	{
		$MediaLngMediaID	= isset($params["MediaLngMediaID"]) ? (int)$params["MediaLngMediaID"] : 0;
		$MediaLngType	= isset($params["MediaLngType"]) ? (int)$params["MediaLngType"] : 0;
		
		$query = "	SELECT
						   MediaLngID
						   FROM sys_medialng
							WHERE MediaLngMediaID = ? AND MediaLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $MediaLngMediaID, $MediaLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["MediaLngID"];
	}
	
	
	
}
