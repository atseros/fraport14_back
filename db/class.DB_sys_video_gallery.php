<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_video_gallery
*
* DESCRIPTION: 
*	Class for table sys_video_gallery
*
* table fields:
*
 `VidID` int(11) NOT NULL,
 `VidSiteID` int(11) NOT NULL,
 `VidTitle` varchar(1024) NOT NULL,
 `VidPriority` int(11) NOT NULL,
 `VidStatus` int(11) NOT NULL,
 `VidFilename` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_sys_video_gallery
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::sys_video_gallery_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$VidID = DB_sys_kxn::get_NextID($appFrw, 'sys_video_gallery');
		
		if($VidID > 0)
		{
			$results["success"] = true;
			$results["data"]["VidID"] = $VidID;
			$results["data"]["VidSiteID"] = $SiteID;
			$results["data"]["VidStatus"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_video_gallery";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $VidID)
	{
		$query = "	SELECT
						   case when( exists (SELECT VidID FROM sys_video_gallery WHERE VidID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $VidID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::sys_video_gallery_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$VidID 	= (int)$params["VidID"];
		
		if($VidID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_video_gallery::check_RecordExists($appFrw, $VidID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$VidID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_video_gallery
					(
						 VidID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $VidID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_video_gallery::sys_video_gallery_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::sys_video_gallery_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_getRecord($appFrw, $params)
	{
		$results = array();
		
		$VidID = (int)$params["VidID"];
		
		if($VidID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_video_gallery::check_RecordExists($appFrw, $VidID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$VidID;
			return $results;
		}
		
		$query = "	SELECT
						
						VidID
						,VidSiteID
						,VidTitle
						,VidPriority
						,VidStatus
						,VidFilename
							
					FROM sys_video_gallery
					WHERE
					VidID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $VidID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::sys_video_gallery_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$VidID = (int)$params["VidID"];
		
		
		if($VidID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_video_gallery::sys_video_gallery_getRecord($appFrw, array('VidID'=>$VidID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$VidSiteID 		= (isset($params['VidSiteID'])) ? $params['VidSiteID'] : $record['VidSiteID'];
		$VidStatus 	= (isset($params['VidStatus'])) ? $params['VidStatus'] : $record['VidStatus'];
		$VidPriority 	= (isset($params['VidPriority'])) ? $params['VidPriority'] : $record['VidPriority'];
		$VidTitle 		= (isset($params['VidTitle'])) ? $params['VidTitle'] : $record['VidTitle'];
		$VidFilename  = (isset($params['VidFilename'])) ? $params['VidFilename'] : $record['VidFilename'];

		
		$query = "	UPDATE sys_video_gallery SET
							
							VidSiteID 	= ?
							,VidStatus 	= ?
							,VidPriority 	= ?
							,VidTitle	 	= ?
							,VidFilename	 	= ?
						
							WHERE
							VidID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiissi",
								 $VidSiteID 			
								,$VidStatus 			
								,$VidPriority 			
								,$VidTitle 					
								,$VidFilename 					
								,$VidID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $VidID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::sys_video_gallery_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$VidID = (int)$params["VidID"];
		
		if($VidID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_video_gallery WHERE VidID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $VidID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $VidID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::sys_video_gallery_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_video_gallery_getList($appFrw, $params)
	{
		$results = array();

		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
				
		$query = "	SELECT
						
							VidID
							,VidSiteID
							,VidTitle
							,VidPriority
							,VidStatus
							,VidFilename
						
							FROM sys_video_gallery
							
							WHERE
							(
								VidSiteID = ?
							)
							AND
							(
								VidTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								VidStatus = 1
							)
							
							ORDER BY VidPriority ASC
					
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $SiteID, $filterStrToFind,$filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['VidFullUrl']= DB_sys_video_gallery::get_sys_video_gallery_FullUrl($appFrw, $row['VidID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::get_sys_video_gallery_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_video_gallery_FullUrl($appFrw, $VidID)
	{
		$url = FileManager::getTblFolderUrl("sys_video_gallery", $VidID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_video_gallery::sys_video_gallery_getRecord($appFrw,array('VidID'=>$VidID));
		if( $imageData["success"] == true  && $imageData["data"]["VidFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["VidFilename"];
		}
		return $FulUrl;
	}
	
}
