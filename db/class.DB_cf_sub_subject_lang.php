<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_cf_sub_subject_lang
*
* DESCRIPTION: 
*	Class for table cf_sub_subject_lang
*
* table fields:
*
 `SubSubLngID` int(11) NOT NULL,
 `SubSubLngSubSubID` int(11) NOT NULL,
 `SubSubLngType` int(11) NOT NULL,
 `SubSubLngTitle` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_cf_sub_subject_lang
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject_lang::cf_sub_subject_lang_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function cf_sub_subject_lang_get_NewRecordDefValues($appFrw, $params)
	{	
		$SubSubLngID = DB_sys_kxn::get_NextID($appFrw, 'cf_sub_subject_lang');
		
		if($SubSubLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["SubSubLngID"] = $SubSubLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table cf_sub_subject_lang";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject_lang::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SubSubLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SubSubLngID FROM cf_sub_subject_lang WHERE SubSubLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SubSubLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject_lang::cf_sub_subject_lang_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_lang_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SubSubLngID 	= (int)$params["SubSubLngID"];
		
		if($SubSubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_cf_sub_subject_lang::check_RecordExists($appFrw, $SubSubLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SubSubLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO cf_sub_subject_lang
					(
						 SubSubLngID
					)
					VALUES
					(
						 ?
					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SubSubLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_cf_sub_subject_lang::cf_sub_subject_lang_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject_lang::cf_sub_subject_lang_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_lang_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SubSubLngID    = (int)$params["SubSubLngID"];
		$SubSubLngType  = $params["SubSubLngType"];
		
		if($SubSubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_cf_sub_subject_lang::check_RecordExists($appFrw, $SubSubLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SubSubLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						 SubSubLngID
						,SubSubLngSubSubID 
						,SubSubLngType 
						,SubSubLngTitle
						
					FROM cf_sub_subject_lang
					WHERE
					(
						SubSubLngID = ?
					)
					AND
					(
						SubSubLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $SubSubLngID, $SubSubLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_sub_subject_lang::cf_sub_subject_lang_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_sub_subject_lang_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SubSubLngID   = (int)$params["SubSubLngID"];
		$SubSubLngType = (isset($params['SubSubLngType'])) ? $params['SubSubLngType'] : $record['SubSubLngType'];
		
		if($SubSubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_cf_sub_subject_lang::cf_sub_subject_lang_getRecord($appFrw, array('SubSubLngID'=>$SubSubLngID,'SubSubLngType'=>$SubSubLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
        $SubSubLngSubSubID 	= (isset($params['SubSubLngSubSubID'])) ? $params['SubSubLngSubSubID'] : $record['SubSubLngSubSubID'];
		$SubSubLngTitle 	= (isset($params['SubSubLngTitle'])) ? $params['SubSubLngTitle'] : $record['SubSubLngTitle'];
		
		$query = "	UPDATE cf_sub_subject_lang SET
						 SubSubLngSubSubID 		= ?
						,SubSubLngType 		= ?
						,SubSubLngTitle 		= ?
					WHERE
					SubSubLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iisi",
                                 $SubSubLngSubSubID
								,$SubSubLngType 
								,$SubSubLngTitle
								,$SubSubLngID
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SubSubLngID;
		return $results;
	}

    /*
    * --------------------------------------------------------------------------
    * DB_cf_sub_subject_lang::cf_sub_subject_lang_CheckBeforeInsert
    * --------------------------------------------------------------------------
    */
    public static function cf_sub_subject_lang_CheckBeforeInsert($appFrw, $params)
    {
        $SubSubLngSubSubID    = isset($params["SubSubLngSubSubID"]) ? (int)$params["SubSubLngSubSubID"] : 0;
        $SubSubLngType	    = isset($params["SubSubLngType"]) ? (int)$params["SubSubLngType"] : 0;

        $query = "	SELECT
						   case when( exists (SELECT SubSubLngID FROM cf_sub_subject_lang WHERE SubSubLngSubSubID = ? AND SubSubLngType = ? ))
							then 1
							else 0
						end as RecordExists";
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ii", $SubSubLngSubSubID, $SubSubLngType);

        if(!$stmt->execute())
            exit("check_RecordExists: error at select : ".$stmt->error);

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
            exit("check_RecordExists: error at select : ".$stmt->error);

        $row = $result->fetch_assoc();
        $result->close();

        return $row["RecordExists"];
    }

    /*
    * --------------------------------------------------------------------------
    * DB_cf_sub_subject_lang::cf_sub_subject_lang_GetSubSubLngID
    * --------------------------------------------------------------------------
    */
    public static function cf_sub_subject_lang_GetSubSubLngID($appFrw, $params)
    {
        $SubSubLngSubSubID    = isset($params["SubSubLngSubSubID"]) ? (int)$params["SubSubLngSubSubID"] : 0;
        $SubSubLngType	    = isset($params["SubSubLngType"]) ? (int)$params["SubSubLngType"] : 0;

        $query = "	SELECT
						   SubSubLngID
					FROM cf_sub_subject_lang
					WHERE SubSubLngSubSubID = ? AND SubSubLngType = ?
				";
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ii", $SubSubLngSubSubID, $SubSubLngType);

        if(!$stmt->execute())
            exit("check_RecordExists: error at select : ".$stmt->error);

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
            exit("check_RecordExists: error at select : ".$stmt->error);

        $row = $result->fetch_assoc();
        $result->close();

        return $row["SubSubLngID"];
    }
	
}