<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_related_links
*
* DESCRIPTION: 
*	Class for table sys_related_links
*
* table fields:
*
 `RelatedLinkID` int(11) NOT NULL,
 `RelatedLinkTitle` varchar(1024) NOT NULL,
 `RelatedLinkFIlename` varchar(1024) NOT NULL,
 `RelatedLinkNodeID` int(11) NOT NULL,
 `RelatedLinkSiteID` int(11) NOT NULL,
 `RelatedLinkType` int(11) DEFAULT NULL,
*
*********************************************************************************************/
class DB_sys_related_links
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::sys_related_links_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_links_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$RelatedLinkID = DB_sys_kxn::get_NextID($appFrw, 'sys_related_links');
		
		if($RelatedLinkID > 0)
		{
			$results["success"] = true;
			$results["data"]["RelatedLinkID"] = $RelatedLinkID;
			$results["data"]["RelatedLinkSiteID"] = $SiteID;
			$results["data"]["RelatedLinkType"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_related_links";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $RelatedLinkID)
	{
		$query = "	SELECT
						   case when( exists (SELECT RelatedLinkID FROM sys_related_links WHERE RelatedLinkID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $RelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::sys_related_links_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_links_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$RelatedLinkID 	= (int)$params["RelatedLinkID"];
		
		if($RelatedLinkID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_related_links::check_RecordExists($appFrw, $RelatedLinkID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$RelatedLinkID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_related_links
					(
						 RelatedLinkID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $RelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_related_links::sys_related_links_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::sys_related_links_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_links_getRecord($appFrw, $params)
	{
		$results = array();
		
		$RelatedLinkID = (int)$params["RelatedLinkID"];
		
		if($RelatedLinkID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_related_links::check_RecordExists($appFrw, $RelatedLinkID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$RelatedLinkID;
			return $results;
		}
		
		$query = "	SELECT
						
						RelatedLinkID
						,RelatedLinkTitle
						,RelatedLinkFIlename
						,RelatedLinkSiteID
						,RelatedLinkType
						,NodeID as RelatedLinkNodeID
						,NodeTitle 
							
					FROM sys_related_links
					LEFT JOIN sys_node ON (NodeID = RelatedLinkNodeID)
					WHERE
					RelatedLinkID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $RelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::sys_related_links_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_links_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$RelatedLinkID = (int)$params["RelatedLinkID"];
		
		
		if($RelatedLinkID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_related_links::sys_related_links_getRecord($appFrw, array('RelatedLinkID'=>$RelatedLinkID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$RelatedLinkSiteID 	= (isset($params['RelatedLinkSiteID'])) ? $params['RelatedLinkSiteID'] : $record['RelatedLinkSiteID'];
		$RelatedLinkType 		= (isset($params['RelatedLinkType'])) ? $params['RelatedLinkType'] : $record['RelatedLinkType'];
		$RelatedLinkTitle 		= (isset($params['RelatedLinkTitle'])) ? $params['RelatedLinkTitle'] : $record['RelatedLinkTitle'];
		$RelatedLinkNodeID = (isset($params['RelatedLinkNodeID'])) ? $params['RelatedLinkNodeID'] : $record['RelatedLinkNodeID'];
		$RelatedLinkFIlename = (isset($params['RelatedLinkFIlename'])) ? $params['RelatedLinkFIlename'] : $record['RelatedLinkFIlename'];
		
		$query = "	UPDATE sys_related_links SET
							
							RelatedLinkSiteID 	= ?
							,RelatedLinkType 	= ?
							,RelatedLinkTitle 	= ?
							,RelatedLinkNodeID 	= ?
							,RelatedLinkFIlename 	= ?
						
							WHERE
							RelatedLinkID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iisisi",
								 $RelatedLinkSiteID 			
								,$RelatedLinkType 			
								,$RelatedLinkTitle 			
								,$RelatedLinkNodeID 						
								,$RelatedLinkFIlename 
								,$RelatedLinkID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $RelatedLinkSiteID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::sys_related_links_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_links_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$RelatedLinkID = (int)$params["RelatedLinkID"];
		
		if($RelatedLinkID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_related_links WHERE RelatedLinkID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $RelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		$query = "DELETE FROM sys_node_related_link WHERE NodeRelatedLinkRelatedLinkID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $RelatedLinkID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $RelatedLinkID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::sys_related_links_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_links_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterType	= isset($params["filterType"]) ? (int)$params["filterType"] : -1;
			
		
		$query = "	SELECT
						
							RelatedLinkID
							,RelatedLinkTitle
							,RelatedLinkFIlename
							,RelatedLinkNodeID
							,RelatedLinkSiteID
							,RelatedLinkType
							,NodeTitle
						
							FROM sys_related_links
							LEFT JOIN sys_node ON (NodeID = RelatedLinkNodeID)
							
							WHERE
							(
								RelatedLinkTitle LIKE ?
							)
							AND
							(
								? = -1
								OR
								RelatedLinkType = ?
							)
							AND
							(
								RelatedLinkSiteID = ?
							)
							

							ORDER BY RelatedLinkTitle ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("siii", $filterStrToFind, $filterType, $filterType, $SiteID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['RelatedLinkFullUrl']= DB_sys_related_links::get_sys_related_links_FullUrl($appFrw, $row['RelatedLinkID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_links::get_sys_related_links_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_related_links_FullUrl($appFrw, $RelatedLinkID)
	{
		$url = FileManager::getTblFolderUrl("sys_related_links", $RelatedLinkID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_related_links::sys_related_links_getRecord($appFrw,array('RelatedLinkID'=>$RelatedLinkID));
		if( $imageData["success"] == true  && $imageData["data"]["RelatedLinkFIlename"] )
		{
			$FulUrl = $url.$imageData["data"]["RelatedLinkFIlename"];
		}
		return $FulUrl;
	}
	
}
