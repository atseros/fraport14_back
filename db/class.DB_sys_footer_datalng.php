<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_footer_datalng
*
* DESCRIPTION: 
*	Class for table sys_footer_datalng
*
* table fields:
*
 `FtrDataLngID` int(11) NOT NULL,
 `FtrDataLngTitle` varchar(1024) NOT NULL,
 `FtrDataLngType` int(11) NOT NULL,
 `FtrDataLngFtrDataID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_footer_datalng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_datalng::sys_footer_datalng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_footer_datalng_get_NewRecordDefValues($appFrw, $params)
	{	
		$FtrDataLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_footer_datalng');
		
		if($FtrDataLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["FtrDataLngID"] = $FtrDataLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_footer_datalng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_datalng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $FtrDataLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT FtrDataLngID FROM sys_footer_datalng WHERE FtrDataLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FtrDataLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_datalng::sys_footer_datalng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_datalng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrDataLngID 	= (int)$params["FtrDataLngID"];
		
		if($FtrDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_footer_datalng::check_RecordExists($appFrw, $FtrDataLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$FtrDataLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_footer_datalng
					(
						 FtrDataLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FtrDataLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_footer_datalng::sys_footer_datalng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_datalng::sys_footer_datalng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_datalng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrDataLngID = (int)$params["FtrDataLngID"];
		$FtrDataLngType = $params["FtrDataLngType"];
		
		if($FtrDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_footer_datalng::check_RecordExists($appFrw, $FtrDataLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$FtrDataLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						FtrDataLngID
						,FtrDataLngTitle 
						,FtrDataLngType 
						,FtrDataLngFtrDataID 
						,FtrDataLngExternalLink
						
					FROM sys_footer_datalng
					WHERE
					(
						FtrDataLngID = ?
					)
					AND
					(
						FtrDataLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $FtrDataLngID, $FtrDataLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_datalng::sys_footer_datalng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_datalng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrDataLngID = (int)$params["FtrDataLngID"];
		$FtrDataLngType 	= (isset($params['FtrDataLngType'])) ? $params['FtrDataLngType'] : $record['FtrDataLngType'];
		
		if($FtrDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_footer_datalng::sys_footer_datalng_getRecord($appFrw, array('FtrDataLngID'=>$FtrDataLngID,'FtrDataLngType'=>$FtrDataLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$FtrDataLngTitle 	= (isset($params['FtrDataLngTitle'])) ? $params['FtrDataLngTitle'] : $record['FtrDataLngTitle'];
		$FtrDataLngFtrDataID 	= (isset($params['FtrDataLngFtrDataID'])) ? $params['FtrDataLngFtrDataID'] : $record['FtrDataLngFtrDataID'];
		$FtrDataLngExternalLink 	= (isset($params['FtrDataLngExternalLink'])) ? $params['FtrDataLngExternalLink'] : $record['FtrDataLngExternalLink'];
		
		$query = "	UPDATE sys_footer_datalng SET
						 FtrDataLngTitle 					= ?
						,FtrDataLngType 					= ?
						,FtrDataLngFtrDataID 		= ?
						,FtrDataLngExternalLink		= ?
						
					WHERE
					FtrDataLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siisi",
								 $FtrDataLngTitle 			
								,$FtrDataLngType 
								,$FtrDataLngFtrDataID
								,$FtrDataLngExternalLink
								,$FtrDataLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $FtrDataLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_datalng::sys_footer_datalng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_datalng_CheckBeforeInsert($appFrw, $params)
	{
		$FtrDataLngFtrDataID	= isset($params["FtrDataLngFtrDataID"]) ? (int)$params["FtrDataLngFtrDataID"] : 0;
		$FtrDataLngType	= isset($params["FtrDataLngType"]) ? (int)$params["FtrDataLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT FtrDataLngID FROM sys_footer_datalng WHERE FtrDataLngFtrDataID = ? AND FtrDataLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $FtrDataLngFtrDataID, $FtrDataLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_datalng::sys_footer_datalng_GetFooterDataLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_datalng_GetFooterDataLngID($appFrw, $params)
	{
		$FtrDataLngFtrDataID	= isset($params["FtrDataLngFtrDataID"]) ? (int)$params["FtrDataLngFtrDataID"] : 0;
		$FtrDataLngType	= isset($params["FtrDataLngType"]) ? (int)$params["FtrDataLngType"] : 0;
		
		$query = "	SELECT
						   FtrDataLngID
						   FROM sys_footer_datalng
							WHERE FtrDataLngFtrDataID = ? AND FtrDataLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $FtrDataLngFtrDataID, $FtrDataLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["FtrDataLngID"];
	}
	
	
	
}
