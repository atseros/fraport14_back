<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_publications_gallery
*
* DESCRIPTION: 
*	Class for table sys_publications_gallery
*
* table fields:
*
 `PubID` int(11) NOT NULL,
 `PubSiteID` int(11) NOT NULL,
 `PubTitle` varchar(1024) NOT NULL,
 `PubPriority` int(11) NOT NULL,
 `PubStatus` int(11) NOT NULL,
 `PubFilename` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_sys_publications_gallery
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery::sys_publications_gallery_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$PubID = DB_sys_kxn::get_NextID($appFrw, 'sys_publications_gallery');
		
		if($PubID > 0)
		{
			$results["success"] = true;
			$results["data"]["PubID"] = $PubID;
			$results["data"]["PubSiteID"] = $SiteID;
			$results["data"]["PubStatus"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_publications_gallery";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $PubID)
	{
		$query = "	SELECT
						   case when( exists (SELECT PubID FROM sys_publications_gallery WHERE PubID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $PubID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery::sys_publications_gallery_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$PubID 	= (int)$params["PubID"];
		
		if($PubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_publications_gallery::check_RecordExists($appFrw, $PubID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$PubID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_publications_gallery
					(
						 PubID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $PubID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_publications_gallery::sys_publications_gallery_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery::sys_publications_gallery_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_getRecord($appFrw, $params)
	{
		$results = array();
		
		$PubID = (int)$params["PubID"];
		
		if($PubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_publications_gallery::check_RecordExists($appFrw, $PubID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$PubID;
			return $results;
		}
		
		$query = "	SELECT
						
						PubID
						,PubSiteID
						,PubTitle
						,PubPriority
						,PubStatus
						,PubFilename
							
					FROM sys_publications_gallery
					WHERE
					PubID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $PubID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery::sys_publications_gallery_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$PubID = (int)$params["PubID"];
		
		
		if($PubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_publications_gallery::sys_publications_gallery_getRecord($appFrw, array('PubID'=>$PubID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$PubSiteID 		= (isset($params['PubSiteID'])) ? $params['PubSiteID'] : $record['PubSiteID'];
		$PubStatus 	= (isset($params['PubStatus'])) ? $params['PubStatus'] : $record['PubStatus'];
		$PubPriority 	= (isset($params['PubPriority'])) ? $params['PubPriority'] : $record['PubPriority'];
		$PubTitle 		= (isset($params['PubTitle'])) ? $params['PubTitle'] : $record['PubTitle'];
		$PubFilename  = (isset($params['PubFilename'])) ? $params['PubFilename'] : $record['PubFilename'];

		
		$query = "	UPDATE sys_publications_gallery SET
							
							PubSiteID 	= ?
							,PubStatus 	= ?
							,PubPriority 	= ?
							,PubTitle	 	= ?
							,PubFilename	 	= ?
						
							WHERE
							PubID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiissi",
								 $PubSiteID 			
								,$PubStatus 			
								,$PubPriority 			
								,$PubTitle 					
								,$PubFilename 					
								,$PubID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $PubID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_video_gallery::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery::sys_publications_gallery_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$PubID = (int)$params["PubID"];
		
		if($PubID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_publications_gallery WHERE PubID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $PubID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $PubID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery::sys_publications_gallery_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_getList($appFrw, $params)
	{
		$results = array();

		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
				
		$query = "	SELECT
						
							PubID
							,PubSiteID
							,PubTitle
							,PubPriority
							,PubStatus
							,PubFilename
						
							FROM sys_publications_gallery
							
							WHERE
							(
								PubSiteID = ?
							)
							AND
							(
								PubTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								PubStatus = 1
							)
							
							ORDER BY PubPriority ASC
					
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $SiteID, $filterStrToFind,$filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['PubFullUrl']= DB_sys_publications_gallery::get_sys_publications_gallery_FullUrl($appFrw, $row['PubID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery::get_sys_publications_gallery_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_publications_gallery_FullUrl($appFrw, $PubID)
	{
		$url = FileManager::getTblFolderUrl("sys_publications_gallery", $PubID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_publications_gallery::sys_publications_gallery_getRecord($appFrw,array('PubID'=>$PubID));
		if( $imageData["success"] == true  && $imageData["data"]["PubFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["PubFilename"];
		}
		return $FulUrl;
	}
	
}
