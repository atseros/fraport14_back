<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_dat_AllTables
*
* DESCRIPTION: 
*	Class for tables . 
*
*********************************************************************************************/
class DB_dat_AllTables
{
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::dat_checkAirportExists
	* --------------------------------------------------------------------------
	*/
	public static function dat_checkAirportExists($appFrw, $params)
	{
		$Aip_Code = $params["Aip_Code"];
		
		$query = "	
			SELECT
				case when( Exists(SELECT AipID FROM dat_airports WHERE Aip_Code = ?) )
					then (SELECT AipID FROM dat_airports WHERE Aip_Code = ? LIMIT 1)
					else 0
				end as AipExists
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		$stmt->bind_param("ss", $Aip_Code, $Aip_Code);
		
		$stmt->execute();
		
		$result = $stmt->get_result();
				
		$row = $result->fetch_assoc();
		
		$stmt->close();
		
		return $row["AipExists"];	
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::dat_checkAirportLangExists
	* --------------------------------------------------------------------------
	*/
	public static function dat_checkAirportLangExists($appFrw, $params)
	{
		$AipID = $params["AipID"];
		$LngType = $params["LngType"];
		
		$query = "	
			SELECT
				case when( Exists(SELECT AipLngID FROM dat_airports_lang WHERE AipLngAipID = ? AND AipLngLngType = ?) )
					then 1
					else 0
				end as AipLngExists
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		$stmt->bind_param("is", $AipID, $LngType);
		
		$stmt->execute();
		
		$result = $stmt->get_result();
				
		$row = $result->fetch_assoc();
		
		$stmt->close();
		
		return $row["AipLngExists"];
	}
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::dat_update_Airport
	* --------------------------------------------------------------------------
	*/
	public static function dat_update_Airport($appFrw, $params)
	{
		$Aip_Code = $params["Aip_Code"];
		$Aip_ICAO = $params["Aip_ICAO"];
		
		$AipID =  DB_dat_AllTables::dat_checkAirportExists($appFrw, array( "Aip_Code" => $Aip_Code) );
		if($AipID == 0)
		{
			$AipID = DB_sys_kxn::get_NextID($appFrw, 'dat_airports');
			
			$ins_query = "INSERT INTO dat_airports( AipID  ) VALUES	(  ? )";
			
			$ins_stmt = $appFrw->DB_Link->prepare($ins_query);
			$ins_stmt->bind_param("i", $AipID);
			$ins_stmt->execute();
			$ins_stmt->close();
		}
		
		$upd_query = "
			UPDATE dat_airports SET 
				 Aip_Code = ?
				,Aip_ICAO = ?
			WHERE  AipID  = ?";
			
		$upd_stmt = $appFrw->DB_Link->prepare($upd_query);
		$upd_stmt->bind_param("ssi", $Aip_Code, $Aip_ICAO, $AipID);
		$upd_stmt->execute();
		$upd_stmt->close();
		
		foreach($params["lang"] as $key => $aiplang)
		{
			$AipLngLngType = $key;
			
			$AipLng_Region = $aiplang["AipLng_Region"];
			$AipLng_Regionorg = $aiplang["AipLng_Regionorg"];
			$AipLng_NameShort = $aiplang["AipLng_NameShort"];
			$AipLng_Name = $aiplang["AipLng_Name"];
			$AipLng_Land = $aiplang["AipLng_Land"];
			
			$AipLngID =  DB_dat_AllTables::dat_checkAirportLangExists($appFrw,  array( "AipID" => $AipID, "LngType" => $AipLngLngType ) );
			
			if($AipLngID == 0)
			{
				$AipLngID = DB_sys_kxn::get_NextID($appFrw, 'dat_airports_lang');
				
				$ins_query = "INSERT INTO dat_airports_lang( AipLngID  ) VALUES	(  ? )";
				
				$ins_stmt = $appFrw->DB_Link->prepare($ins_query);
				$ins_stmt->bind_param("i", $AipLngID);
				$ins_stmt->execute();
				$ins_stmt->close();
			}
		
			$upd_query = "
				UPDATE dat_airports_lang SET 
					 AipLngAipID 				= ?
					,AipLngLngType 		= ?					
					,AipLng_Region 			= ?
					,AipLng_Regionorg 	= ?
					,AipLng_NameShort 	= ?
					,AipLng_Name 			= ?
					,AipLng_Land 			= ?
				WHERE  AipLngID  		= ?
			";
	
			$upd_stmt = $appFrw->DB_Link->prepare($upd_query);
			$upd_stmt->bind_param("iisssssi", $AipID, $AipLngLngType, $AipLng_Region, $AipLng_Regionorg, $AipLng_NameShort, $AipLng_Name, $AipLng_Land, $AipLngID);
			$upd_stmt->execute();
			$upd_stmt->close();
		}		
		
		return $AipID;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::dat_checkAirlineExists
	* --------------------------------------------------------------------------
	*/
	public static function dat_checkAirlineExists($appFrw, $params)
	{
		$Arl_Code = $params["Arl_Code"];
		
		$query = "	
			SELECT
				case when( Exists(SELECT ArlID FROM dat_airlines WHERE Arl_Code = ?) )
					then (SELECT ArlID FROM dat_airlines WHERE Arl_Code = ? LIMIT 1)
					else 0
				end as ArlExists
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		$stmt->bind_param("ss", $Arl_Code, $Arl_Code);
		
		$stmt->execute();
		
		$result = $stmt->get_result();
				
		$row = $result->fetch_assoc();
		
		$stmt->close();
		
		return $row["ArlExists"];	
	}
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::dat_checkAirlineLangExists
	* --------------------------------------------------------------------------
	*/
	public static function dat_checkAirlineLangExists($appFrw, $params)
	{
		$ArlID = $params["ArlID"];
		$LngType = $params["LngType"];
		
		$query = "	
			SELECT
				case when( Exists(SELECT ArlLngID FROM dat_airlines_lang WHERE ArlLngArlID = ? AND ArlLngLngType = ?) )
					then (SELECT ArlLngID FROM dat_airlines_lang WHERE ArlLngArlID = ? AND ArlLngLngType = ?)
					else 0
				end as ArlLngExists
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		$stmt->bind_param("isis", $ArlID, $LngType, $ArlID, $LngType);
		
		$stmt->execute();
		
		$result = $stmt->get_result();
		
		$row = $result->fetch_assoc();
		
		$result->close();
		
		return $row["ArlLngExists"];
	}
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::dat_update_Airline
	* --------------------------------------------------------------------------
	*/
	public static function dat_update_Airline($appFrw, $params)
	{
		$Arl_Code = $params["Arl_Code"];
		
		$ArlID =  DB_dat_AllTables::dat_checkAirlineExists($appFrw, array( "Arl_Code" => $Arl_Code) );
		if($ArlID == 0)
		{
			$ArlID = DB_sys_kxn::get_NextID($appFrw, 'dat_airlines');
			
			$ins_query = "INSERT INTO dat_airlines( ArlID  ) VALUES	(  ? )";
			
			$ins_stmt = $appFrw->DB_Link->prepare($ins_query);
			$ins_stmt->bind_param("i", $ArlID);
			$ins_stmt->execute();
			$ins_stmt->close();
		}
		
		$upd_query = "
			UPDATE dat_airlines SET 
				 Arl_Code = ?
			WHERE  ArlID  = ?";
			
		$upd_stmt = $appFrw->DB_Link->prepare($upd_query);
		$upd_stmt->bind_param("si", $Arl_Code, $ArlID);
		$upd_stmt->execute();
		$upd_stmt->close();
		
		foreach($params["lang"] as $key => $arllang)
		{
			$ArlLngLngType = $key;
			
			$ArlLng_Name = $arllang["ArlLng_Name"];

			$ArlLngID =  DB_dat_AllTables::dat_checkAirlineLangExists($appFrw,  array( "ArlID" => $ArlID, "LngType" => $ArlLngLngType ) );
			
			if($ArlLngID == 0)
			{
				$ArlLngID = DB_sys_kxn::get_NextID($appFrw, 'dat_airlines_lang');
				
				$ins_query = "INSERT INTO dat_airlines_lang( ArlLngID  ) VALUES	(  ? )";
				
				$ins_stmt = $appFrw->DB_Link->prepare($ins_query);
				$ins_stmt->bind_param("i", $ArlLngID);
				$ins_stmt->execute();
				$ins_stmt->close();
			}
		
			$upd_query = "
				UPDATE dat_airlines_lang SET 
					 ArlLngArlID 		= ?
					,ArlLngLngType 	= ?
					,ArlLng_Name 	= ?
				WHERE  ArlLngID  	= ?
			";
					
			$upd_stmt = $appFrw->DB_Link->prepare($upd_query);
			$upd_stmt->bind_param("iisi", $ArlID, $ArlLngLngType, $ArlLng_Name, $ArlLngID);
			$upd_stmt->execute();
			$upd_stmt->close();
		}		
		
		return $ArlID;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::dat_checkFlightExists
	* --------------------------------------------------------------------------
	*/
	public static function dat_checkFlightExists($appFrw, $params)
	{
		
	}
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::dat_update_Flight
	* --------------------------------------------------------------------------
	*/
	public static function dat_update_Flight($appFrw, $params)
	{
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::get_Airports_List
	* --------------------------------------------------------------------------
	*/
	public static function get_Airports_List($appFrw, $params)
	{
		$results = array();
		
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		
		$LangID	= isset($params["LangID"]) ? (int)$params["LangID"] : 1;

		$query = "
			SELECT
				 AipID
				,Aip_Code
				,Aip_ICAO
				,AipSiteID	
				,AipLng_Region
				,AipLng_Regionorg
				,AipLng_NameShort
				,AipLng_Name
				,AipLng_Land
			FROM dat_airports 
			LEFT JOIN dat_airports_lang ON(AipLngAipID = AipID AND AipLngLngType = ?)
			WHERE
			AipSiteID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_Airports_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("ii", $LangID, $SiteID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_Airports_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_Airports_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_dat_AllTables::get_Airlines_List
	* --------------------------------------------------------------------------
	*/
	public static function get_Airlines_List($appFrw, $params)
	{
		$results = array();

		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		
		$LangID	= isset($params["LangID"]) ? (int)$params["LangID"] : 1;
				
		$query = "
			SELECT
				 ArlID
				,Arl_Code
				,ArlSiteID	
				,ArlLng_Name
			FROM dat_airlines
			LEFT JOIN dat_airlines_lang ON(ArlLngArlID = ArlID AND ArlLngLngType = ?)
			WHERE
			ArlSiteID = ?
		";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_Airlines_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("ii", $LangID, $SiteID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_Airlines_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_Airlines_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
}
