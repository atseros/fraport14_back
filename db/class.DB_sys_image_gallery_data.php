<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_image_gallery_data
*
* DESCRIPTION: 
*	Class for table sys_image_gallery_data
*
* table fields:
*
 `ImgDataID` int(11) NOT NULL,
 `ImgDataImgID` int(11) NOT NULL,
 `ImgDataTitle` varchar(1024) NOT NULL,
 `ImgDataPriority` int(11) NOT NULL,
 `ImgDataStatus` int(11) NOT NULL,
 `ImgDataFilename` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_sys_image_gallery_data
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_data::sys_image_gallery_data_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_data_get_NewRecordDefValues($appFrw, $params)
	{
		$ImgID 	= (int)$params["ImgID"];
		
		$ImgDataID = DB_sys_kxn::get_NextID($appFrw, 'sys_image_gallery_data');
		
		if($ImgDataID > 0)
		{
			$results["success"] = true;
			$results["data"]["ImgDataID"] = $ImgDataID;
			$results["data"]["ImgDataImgID"] = $ImgID;
			$results["data"]["ImgDataStatus"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_image_gallery_data";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_data::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $ImgDataID)
	{
		$query = "	SELECT
						   case when( exists (SELECT ImgDataID FROM sys_image_gallery_data WHERE ImgDataID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $ImgDataID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_data::sys_image_gallery_data_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_data_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgDataID 	= (int)$params["ImgDataID"];
		
		if($ImgDataID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_image_gallery_data::check_RecordExists($appFrw, $ImgDataID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$ImgDataID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_image_gallery_data
					(
						 ImgDataID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $ImgDataID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_image_gallery_data::sys_image_gallery_data_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_data::sys_image_gallery_data_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_data_getRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgDataID = (int)$params["ImgDataID"];
		
		if($ImgDataID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_image_gallery_data::check_RecordExists($appFrw, $ImgDataID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$ImgDataID;
			return $results;
		}
		
		$query = "	SELECT
						
						ImgDataID
						,ImgDataImgID
						,ImgDataTitle
						,ImgDataPriority
						,ImgDataStatus
						,ImgDataFilename
							
					FROM sys_image_gallery_data
					WHERE
					ImgDataID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $ImgDataID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_data::sys_image_gallery_data_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_data_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgDataID = (int)$params["ImgDataID"];
		
		
		if($ImgDataID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_image_gallery_data::sys_image_gallery_data_getRecord($appFrw, array('ImgDataID'=>$ImgDataID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$ImgDataImgID 		= (isset($params['ImgDataImgID'])) ? $params['ImgDataImgID'] : $record['ImgDataImgID'];
		$ImgDataStatus 		= (isset($params['ImgDataStatus'])) ? $params['ImgDataStatus'] : $record['ImgDataStatus'];
		$ImgDataPriority 	= (isset($params['ImgDataPriority'])) ? $params['ImgDataPriority'] : $record['ImgDataPriority'];
		$ImgDataTitle 		= (isset($params['ImgDataTitle'])) ? $params['ImgDataTitle'] : $record['ImgDataTitle'];
		$ImgDataFilename 	= (isset($params['ImgDataFilename'])) ? $params['ImgDataFilename'] : $record['ImgDataFilename'];

		
		$query = "	UPDATE sys_image_gallery_data SET
							
							ImgDataImgID 	= ?
							,ImgDataStatus 	= ?
							,ImgDataPriority 	= ?
							,ImgDataTitle	 	= ?
							,ImgDataFilename	 	= ?
						
							WHERE
							ImgDataID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiissi",
								 $ImgDataImgID 			
								,$ImgDataStatus 			
								,$ImgDataPriority 			
								,$ImgDataTitle 					
								,$ImgDataFilename 					
								,$ImgDataID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $ImgDataID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_data::sys_image_gallery_data_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_data_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgDataID = (int)$params["ImgDataID"];
		
		if($ImgDataID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_image_gallery_data WHERE ImgDataID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $ImgDataID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $ImgDataID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery::sys_image_gallery_data_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_data_getList($appFrw, $params)
	{
		$results = array();

		$ImgID	= isset($params["ImgID"]) ? (int)$params["ImgID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
				
		$query = "	SELECT
						
							ImgDataID
							,ImgDataImgID
							,ImgDataTitle
							,ImgDataPriority
							,ImgDataStatus
							,ImgDataFilename
						
							FROM sys_image_gallery_data
							
							WHERE
							(
								ImgDataImgID = ?
							)
							AND
							(
								ImgDataTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								ImgDataStatus = 1
							)
							
							ORDER BY ImgDataPriority ASC
					
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $ImgID, $filterStrToFind,$filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['ImgDataFullUrl']= DB_sys_image_gallery_data::get_sys_image_gallery_data_FullUrl($appFrw, $row['ImgDataID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery::get_sys_image_gallery_data_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_image_gallery_data_FullUrl($appFrw, $ImgDataID)
	{
		$url = FileManager::getTblFolderUrl("sys_image_gallery_data", $ImgDataID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_image_gallery_data::sys_image_gallery_data_getRecord($appFrw,array('ImgDataID'=>$ImgDataID));
		if( $imageData["success"] == true  && $imageData["data"]["ImgDataFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["ImgDataFilename"];
		}
		return $FulUrl;
	}
	
}
