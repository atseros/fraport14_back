<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_media
*
* DESCRIPTION: 
*	Class for table sys_media
*
* table fields:
*
 `MediaID` int(11) NOT NULL,
 `MediaTitle` varchar(1024) NOT NULL,
 `MediaFilename` varchar(1024) NOT NULL,
 `MediaType` int(11) NOT NULL,
 `MediaSiteID` int(11) DEFAULT NULL
*
*********************************************************************************************/
class DB_sys_media
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::sys_media_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_media_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		$MediaType 	= (int)$params["MediaType"];
		
		$MediaID = DB_sys_kxn::get_NextID($appFrw, 'sys_media');
		
		if($MediaID > 0)
		{
			$results["success"] = true;
			$results["data"]["MediaID"] = $MediaID;
			$results["data"]["MediaSiteID"] = $SiteID;
			$results["data"]["MediaType"] = $MediaType;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_media";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $MediaID)
	{
		$query = "	SELECT
						   case when( exists (SELECT MediaID FROM sys_media WHERE MediaID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $MediaID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::sys_media_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_media_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$MediaID 	= (int)$params["MediaID"];
		
		if($MediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_media::check_RecordExists($appFrw, $MediaID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$MediaID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_media
					(
						 MediaID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $MediaID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_media::sys_media_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::sys_media_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_media_getRecord($appFrw, $params)
	{
		$results = array();
		
		$MediaID = (int)$params["MediaID"];
		
		if($MediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_media::check_RecordExists($appFrw, $MediaID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$MediaID;
			return $results;
		}
		
		$query = "	SELECT
						
						MediaID
						,MediaTitle
						,MediaFilename
						,MediaType
						,MediaSiteID
						,CONCAT ('https://www.youtube.com/watch?v=',MediaFilename) as MediaUrl
						,MediaFilesize
						,MediaExtension
							
					FROM sys_media
					WHERE
					MediaID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $MediaID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::sys_media_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_media_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$MediaID = (int)$params["MediaID"];
		
		
		if($MediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_media::sys_media_getRecord($appFrw, array('MediaID'=>$MediaID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$MediaSiteID 	= (isset($params['MediaSiteID'])) ? $params['MediaSiteID'] : $record['MediaSiteID'];
		$MediaType 		= (isset($params['MediaType'])) ? $params['MediaType'] : $record['MediaType'];
		$MediaTitle 		= (isset($params['MediaTitle'])) ? $params['MediaTitle'] : $record['MediaTitle'];
		$MediaFilename = (isset($params['MediaFilename'])) ? $params['MediaFilename'] : $record['MediaFilename'];
		$MediaFilesize = (isset($params['MediaFilesize'])) ? $params['MediaFilesize'] : $record['MediaFilesize'];
		$MediaExtension = (isset($params['MediaExtension'])) ? $params['MediaExtension'] : $record['MediaExtension'];
		
		$query = "	UPDATE sys_media SET
							
							MediaSiteID 	= ?
							,MediaType 	= ?
							,MediaTitle 	= ?
							,MediaFilename 	= ?
							,MediaFilesize 	= ?
							,MediaExtension 	= ?
							
							WHERE
							MediaID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iissssi",
								 $MediaSiteID 			
								,$MediaType 			
								,$MediaTitle 			
								,$MediaFilename 						
								,$MediaFilesize 						
								,$MediaExtension 						
								,$MediaID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $MediaID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::sys_media_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_media_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$MediaID = (int)$params["MediaID"];
		
		if($MediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_media WHERE MediaID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $MediaID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		$query = "DELETE FROM sys_node_media WHERE NodeMediaMediaID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $MediaID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		
		
		// return
		$results["success"] = true;
		$results["data"] = $MediaID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::sys_media_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_media_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$MediaType	= isset($params["MediaType"]) ? (int)$params["MediaType"] : 0;
				
		$query = "	SELECT
						
							MediaID
							,MediaTitle
							,MediaFilename
							,MediaType
							,MediaSiteID
						
							FROM sys_media
							
							WHERE
							(
								MediaSiteID = ?
							)
							AND
							(
								MediaTitle LIKE ?
							)
							AND
							(
								MediaType LIKE ?
							)
							

							ORDER BY MediaID DESC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $SiteID, $filterStrToFind,$MediaType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['MediaFullUrl']= DB_sys_media::get_sys_media_FullUrl($appFrw, $row['MediaID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::get_sys_media_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_media_FullUrl($appFrw, $MediaID)
	{
		$url = FileManager::getTblFolderUrl("sys_media", $MediaID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_media::sys_media_getRecord($appFrw,array('MediaID'=>$MediaID));
		if( $imageData["success"] == true  && $imageData["data"]["MediaFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["MediaFilename"];
		}
		return $FulUrl;
	}
	
}
