<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_categories_records_slider
*
* DESCRIPTION: 
*	Class for table sys_categories_records_slider
*
* table fields:
*
 `CtgRecSliderID` int(11) NOT NULL,
 `CtgRecSliderCtgRecID` int(11) NOT NULL,
 `CtgRecSliderSliderID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_categories_records_slider
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::sys_categories_records_slider_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_slider_get_NewRecordDefValues($appFrw, $params)
	{	
		$CtgRecSliderID = DB_sys_kxn::get_NextID($appFrw, 'sys_categories_records_slider');
		
		if($CtgRecSliderID > 0)
		{
			$results["success"] = true;
			$results["data"]["CtgRecSliderID"] = $CtgRecSliderID;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_categories_records_slider";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $CtgRecSliderID)
	{
		$query = "	SELECT
						   case when( exists (SELECT CtgRecSliderID FROM sys_categories_records_slider WHERE CtgRecSliderID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CtgRecSliderID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::sys_categories_records_slider_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_slider_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecSliderID 	= (int)$params["CtgRecSliderID"];
		
		if($CtgRecSliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_categories_records_slider::check_RecordExists($appFrw, $CtgRecSliderID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$CtgRecSliderID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_categories_records_slider
					(
						 CtgRecSliderID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CtgRecSliderID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_categories_records_slider::sys_categories_records_slider_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::sys_categories_records_slider_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_slider_getRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecSliderID = (int)$params["CtgRecSliderID"];
		
		if($CtgRecSliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_categories_records_slider::check_RecordExists($appFrw, $CtgRecSliderID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$CtgRecSliderID;
			return $results;
		}
		
		$query = "	SELECT
						
						 CtgRecSliderID
						,CtgRecSliderCtgRecID
						,CtgRecSliderSliderID
							
					FROM sys_categories_records_slider
					WHERE
					CtgRecSliderID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CtgRecSliderID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::sys_categories_records_slider_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_slider_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecSliderID = (int)$params["CtgRecSliderID"];
		
		
		if($CtgRecSliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_categories_records_slider::sys_categories_records_slider_getRecord($appFrw, array('CtgRecSliderID'=>$CtgRecSliderID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$CtgRecSliderCtgRecID 	= (isset($params['CtgRecSliderCtgRecID'])) ? $params['CtgRecSliderCtgRecID'] : $record['CtgRecSliderCtgRecID'];
		$CtgRecSliderSliderID 	= (isset($params['CtgRecSliderSliderID'])) ? $params['CtgRecSliderSliderID'] : $record['CtgRecSliderSliderID'];
		
		$query = "	UPDATE sys_categories_records_slider SET
							
							 CtgRecSliderCtgRecID 	= ?
							,CtgRecSliderSliderID 	= ?
						
							WHERE
							CtgRecSliderID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iii",
								 $CtgRecSliderCtgRecID 			
								,$CtgRecSliderSliderID 																
								,$CtgRecSliderID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $CtgRecSliderID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::sys_categories_records_slider_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_slider_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecSliderID = (int)$params["CtgRecSliderID"];
		
		if($CtgRecSliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_categories_records_slider WHERE CtgRecSliderID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CtgRecSliderID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $CtgRecSliderID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::sys_categories_records_slider_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_slider_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$CtgRecID	= isset($params["CtgRecID"]) ? (int)$params["CtgRecID"] : 0;
				
		$query = "	SELECT

						 CtgRecSliderID
						,CtgRecSliderCtgRecID
						,CtgRecSliderSliderID
						,SliderTitle
					
					FROM sys_categories_records_slider
					LEFT JOIN sys_slider ON ( SliderID = CtgRecSliderSliderID )
					LEFT JOIN sys_categories ON ( CtgID = CtgRecSliderCtgRecID )
					
					WHERE
					(
						CtgRecSliderCtgRecID  = ?
					)
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i",$CtgRecID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
		/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::sys_categories_records_slider_getListSitePages
	* --------------------------------------------------------------------------
	*/
	// public static function sys_categories_records_slider_getListSitePages($appFrw, $params)
	// {
		// $results = array();

		// $maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		// $NodeID	= isset($params["NodeID"]) ? (int)$params["NodeID"] : 0;
		// $NodeLngType	= isset($params["NodeLngType"]) ? (int)$params["NodeLngType"] : -1;
				
		// $query = "	SELECT

							// NodeMediaID
							// ,NodeMediaNodeID
							// ,NodeMediaMediaID
							// ,NodeMediaPriority
							// ,MediaID
							// ,MediaLngTitle as MediaTitle
							// ,MediaFilename
							// ,MediaType
							// ,MediaSiteID
						
							// FROM sys_node_media
							// LEFT JOIN sys_media ON ( MediaID = NodeMediaMediaID )
							// LEFT JOIN sys_node ON ( NodeID = NodeMediaNodeID )
							// LEFT JOIN sys_medialng ON ( MediaLngMediaID = MediaID )
							
							// WHERE
							// (
								// MediaType LIKE ?
							// )
							// AND
							// (
								// NodeMediaNodeID  = ?
							// )
							// AND
							// (
								// MediaLngType = ?
							// )
							// AND
							// (
								// MediaLngTitle != ''
							// )
							
							

							// ORDER BY NodeMediaPriority ASC
					
					
		// ";
		
		// if($maxRecords)
			// $query .= " LIMIT ".$maxRecords;

		// //exit($query);
		// $stmt = $appFrw->DB_Link->prepare($query);
		
		// if(!$stmt) 
		// {
			// $results["success"] = false;
			// $results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			// return $results;
		// }
		
		// $stmt->bind_param("iii", $MediaType,$NodeID,$MediaLngType);
		
		// if(!$stmt->execute()) 
		// {
			// $results["success"] = false;
			// $results["reason"] = "error at select: ".$stmt->error;
			// return $results;
		// }
		
		// $result = $stmt->get_result();
		// $stmt->close();			
								
		// if(!$result)
		// {
			// $results["success"] = false;
			// $results["reason"] = "error at select: ".$stmt->error;
			// return $results;
		// }
		
		// $results['data'] = array();
		
		// while( $row = $result->fetch_assoc())
		// {
			// array_push($results['data'], $row);
		// }
		// $result->close();
	
		// // return results
		// $results["success"] = true;
		
		// return $results;
	// }
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records_slider::sys_categories_records_slider_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_slider_CheckBeforeInsert($appFrw, $params)
	{
		$CtgRecSliderCtgRecID		= isset($params["CtgRecSliderCtgRecID"]) ? (int)$params["CtgRecSliderCtgRecID"] : 0;
		$CtgRecSliderSliderID	= isset($params["CtgRecSliderSliderID"]) ? (int)$params["CtgRecSliderSliderID"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT CtgRecSliderID FROM sys_categories_records_slider WHERE CtgRecSliderCtgRecID = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CtgRecSliderCtgRecID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
}
