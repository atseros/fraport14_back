<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_publications_gallery_datalng
*
* DESCRIPTION: 
*	Class for table sys_publications_gallery_datalng
*
* table fields:
*
 `PubDataLngID` int(11) NOT NULL,
 `PubDataLngPubDataID` int(11) NOT NULL,
 `PubDataLngTitle` varchar(1024) NOT NULL,
 `PubDataLngDescription` varchar(1024) NOT NULL,
 `PubDataLngType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_publications_gallery_datalng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_publications_gallery_datalng_get_NewRecordDefValues($appFrw, $params)
	{	
		$PubDataLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_publications_gallery_datalng');
		
		if($PubDataLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["PubDataLngID"] = $PubDataLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_publications_gallery_datalng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery_datalng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $PubDataLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT PubDataLngID FROM sys_publications_gallery_datalng WHERE PubDataLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $PubDataLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_datalng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$PubDataLngID 	= (int)$params["PubDataLngID"];
		
		if($PubDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_publications_gallery_datalng::check_RecordExists($appFrw, $PubDataLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$PubDataLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_publications_gallery_datalng
					(
						 PubDataLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $PubDataLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_datalng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$PubDataLngID = (int)$params["PubDataLngID"];
		$PubDataLngType = $params["PubDataLngType"];
		
		if($PubDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_publications_gallery_datalng::check_RecordExists($appFrw, $PubDataLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$PubDataLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						PubDataLngID
						,PubDataLngPubDataID 
						,PubDataLngTitle 
						,PubDataLngDescription 
						,PubDataLngType 
						,PubDataLngSubtitle 
							
					FROM sys_publications_gallery_datalng
					WHERE
					(
						PubDataLngID = ?
					)
					AND
					(
						PubDataLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $PubDataLngID, $PubDataLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_datalng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$PubDataLngID = (int)$params["PubDataLngID"];
		$PubDataLngType 	= (isset($params['PubDataLngType'])) ? $params['PubDataLngType'] : $record['PubDataLngType'];
		
		if($PubDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_getRecord($appFrw, array('PubDataLngID'=>$PubDataLngID,'PubDataLngType'=>$PubDataLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$PubDataLngTitle 	= (isset($params['PubDataLngTitle'])) ? $params['PubDataLngTitle'] : $record['PubDataLngTitle'];
		$PubDataLngPubDataID 	= (isset($params['PubDataLngPubDataID'])) ? $params['PubDataLngPubDataID'] : $record['PubDataLngPubDataID'];
		$PubDataLngDescription 	= (isset($params['PubDataLngDescription'])) ? $params['PubDataLngDescription'] : $record['PubDataLngDescription'];
		$PubDataLngSubtitle 	= (isset($params['PubDataLngSubtitle'])) ? $params['PubDataLngSubtitle'] : $record['PubDataLngSubtitle'];
		
		$query = "	UPDATE sys_publications_gallery_datalng SET
						 PubDataLngTitle 					= ?
						,PubDataLngType 					= ?
						,PubDataLngPubDataID 		= ?
						,PubDataLngDescription 		= ?
						,PubDataLngSubtitle 		= ?
						
					WHERE
					PubDataLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siissi",
								 $PubDataLngTitle 			
								,$PubDataLngType 
								,$PubDataLngPubDataID 
								,$PubDataLngDescription 
								,$PubDataLngSubtitle 
								,$PubDataLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $PubDataLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_datalng_CheckBeforeInsert($appFrw, $params)
	{
		$PubDataLngPubDataID	= isset($params["PubDataLngPubDataID"]) ? (int)$params["PubDataLngPubDataID"] : 0;
		$PubDataLngType	= isset($params["PubDataLngType"]) ? (int)$params["PubDataLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT PubDataLngID FROM sys_publications_gallery_datalng WHERE PubDataLngPubDataID = ? AND PubDataLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $PubDataLngPubDataID, $PubDataLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_GetPubDataLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallery_datalng_GetPubDataLngID($appFrw, $params)
	{
		$PubDataLngPubDataID	= isset($params["PubDataLngPubDataID"]) ? (int)$params["PubDataLngPubDataID"] : 0;
		$PubDataLngType	= isset($params["PubDataLngType"]) ? (int)$params["PubDataLngType"] : 0;
		
		$query = "	SELECT
						   PubDataLngID
						   FROM sys_publications_gallery_datalng
							WHERE PubDataLngPubDataID = ? AND PubDataLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $PubDataLngPubDataID, $PubDataLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["PubDataLngID"];
	}
	
	
	
}
