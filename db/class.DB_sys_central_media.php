<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_central_media
*
* DESCRIPTION: 
*	Class for table sys_central_media
*
* table fields:
*
 `CentralMediaID` int(11) NOT NULL,
 `CentralMediaTitle` varchar(1024) NOT NULL,
 `CentralMediaFilename` varchar(1024) NOT NULL,
 `CentralMediaType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_central_media
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_media::sys_central_media_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_media_get_NewRecordDefValues($appFrw, $params)
	{
		$CentralMediaType 	= (int)$params["CentralMediaType"];
		
		$CentralMediaID = DB_sys_kxn::get_NextID($appFrw, 'sys_central_media');
		
		if($CentralMediaID > 0)
		{
			$results["success"] = true;
			$results["data"]["CentralMediaID"] = $CentralMediaID;
			$results["data"]["CentralMediaType"] = $CentralMediaType;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_central_media";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $CentralMediaID)
	{
		$query = "	SELECT
						   case when( exists (SELECT CentralMediaID FROM sys_central_media WHERE CentralMediaID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CentralMediaID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_media::sys_central_media_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_media_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$CentralMediaID 	= (int)$params["CentralMediaID"];
		
		if($CentralMediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_central_media::check_RecordExists($appFrw, $CentralMediaID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$CentralMediaID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_central_media
					(
						 CentralMediaID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CentralMediaID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_central_media::sys_central_media_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_media::sys_central_media_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_media_getRecord($appFrw, $params)
	{
		$results = array();
		
		$CentralMediaID = (int)$params["CentralMediaID"];
		
		if($CentralMediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_central_media::check_RecordExists($appFrw, $CentralMediaID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$CentralMediaID;
			return $results;
		}
		
		$query = "	SELECT
						
						CentralMediaID
						,CentralMediaTitle
						,CentralMediaFilename
						,CentralMediaType
						,CONCAT ('https://www.youtube.com/watch?v=',CentralMediaFilename) as CentralMediaUrl
							
					FROM sys_central_media
					WHERE
					CentralMediaID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CentralMediaID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_media::sys_central_media_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_media_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$CentralMediaID = (int)$params["CentralMediaID"];
		
		
		if($CentralMediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_central_media::sys_central_media_getRecord($appFrw, array('CentralMediaID'=>$CentralMediaID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$CentralMediaTitle 	= (isset($params['CentralMediaTitle'])) ? $params['CentralMediaTitle'] : $record['CentralMediaTitle'];
		$CentralMediaFilename 		= (isset($params['CentralMediaFilename'])) ? $params['CentralMediaFilename'] : $record['CentralMediaFilename'];
		$CentralMediaType 		= (isset($params['CentralMediaType'])) ? $params['CentralMediaType'] : $record['CentralMediaType'];
		
		$query = "	UPDATE sys_central_media SET
							
							CentralMediaTitle 	= ?
							,CentralMediaFilename 	= ?
							,CentralMediaType 	= ?
						
							WHERE
							CentralMediaID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ssii",
								 $CentralMediaTitle 			
								,$CentralMediaFilename 			
								,$CentralMediaType 									
								,$CentralMediaID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $CentralMediaID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_media::sys_central_media_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_media_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$CentralMediaID = (int)$params["CentralMediaID"];
		
		if($CentralMediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_central_media WHERE CentralMediaID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CentralMediaID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// $query = "DELETE FROM sys_node_media WHERE NodeMediaMediaID = ?";
		
		// $stmt = $appFrw->DB_Link->prepare($query);
		
		// if(!$stmt) 
			// exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		// $stmt->bind_param("i", $MediaID);
		
		// if(!$stmt->execute()) 
			// exit("delete_Record: error at delete : ".$stmt->error);

		// $stmt->close();			
		
		// return
		$results["success"] = true;
		$results["data"] = $CentralMediaID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_media::sys_central_media_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_media_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$CentralMediaType	= isset($params["CentralMediaType"]) ? (int)$params["CentralMediaType"] : 0;
				
		$query = "	SELECT
						
							CentralMediaID
							,CentralMediaTitle
							,CentralMediaFilename
							,CentralMediaType
						
							FROM sys_central_media
							
							WHERE
							(
								CentralMediaTitle LIKE ?
							)
							AND
							(
								CentralMediaType = ?
							)
							

							ORDER BY CentralMediaTitle ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("si", $filterStrToFind,$CentralMediaType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['CentralMediaFullUrl']= DB_sys_central_media::get_sys_central_media_FullUrl($appFrw, $row['CentralMediaID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_media::get_sys_central_media_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_central_media_FullUrl($appFrw, $CentralMediaID)
	{
		$url = FileManager::getTblFolderUrl("sys_central_media", $CentralMediaID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_central_media::sys_central_media_getRecord($appFrw,array('CentralMediaID'=>$CentralMediaID));
		if( $imageData["success"] == true  && $imageData["data"]["CentralMediaFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["CentralMediaFilename"];
		}
		return $FulUrl;
	}
	
}
