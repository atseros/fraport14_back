<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_slider
*
* DESCRIPTION: 
*	Class for table sys_slider
*
* table fields:
*
 `SliderID` int(11) NOT NULL,
 `SliderSiteID` int(11) NOT NULL,
 `SliderTitle` varchar(1024) NOT NULL,
 `SliderStatus` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_slider
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider::sys_slider_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$SliderID = DB_sys_kxn::get_NextID($appFrw, 'sys_slider');
		
		if($SliderID > 0)
		{
			$results["success"] = true;
			$results["data"]["SliderID"] = $SliderID;
			$results["data"]["SliderSiteID"] = $SiteID;
			$results["data"]["SliderStatus"] = 1;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_slider";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SliderID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SliderID FROM sys_slider WHERE SliderID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SliderID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider::sys_slider_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderID 	= (int)$params["SliderID"];
		
		if($SliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_slider::check_RecordExists($appFrw, $SliderID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SliderID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_slider
					(
						 SliderID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SliderID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_slider::sys_slider_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider::sys_slider_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderID = (int)$params["SliderID"];
		
		if($SliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_slider::check_RecordExists($appFrw, $SliderID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SliderID;
			return $results;
		}
		
		$query = "	SELECT
						
						SliderID
						,SliderSiteID
						,SliderTitle
						,SliderStatus
							
					FROM sys_slider
					WHERE
					SliderID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SliderID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		
		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider::sys_slider_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderID = (int)$params["SliderID"];
		
		
		if($SliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_slider::sys_slider_getRecord($appFrw, array('SliderID'=>$SliderID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$SliderSiteID = (isset($params['SliderSiteID'])) ? $params['SliderSiteID'] : $record['SliderSiteID'];
		$SliderTitle = (isset($params['SliderTitle'])) ? $params['SliderTitle'] : $record['SliderTitle'];
		$SliderStatus 	= (isset($params['SliderStatus'])) ? $params['SliderStatus'] : $record['SliderStatus'];

		$query = "	UPDATE sys_slider SET
							
							SliderSiteID 	= ?
							,SliderTitle 	= ?
							,SliderStatus 	= ?
						
							WHERE
							SliderID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("isii",
								 $SliderSiteID 			
								,$SliderTitle 			
								,$SliderStatus 			
								,$SliderID 					
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SliderID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SliderID = (int)$params["SliderID"];
		if($SliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT SliderImagesID FROM sys_slider_images WHERE SliderImagesSliderID = ?))
							then 1
							else 0
						 end as SliderImageExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SliderID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["SliderImageExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are images attached to the slider";
			
			return $results;
		}
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider::sys_slider_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SliderID = (int)$params["SliderID"];
		
		if($SliderID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		//check if can be deleted
		$canBeDeleted = DB_sys_slider::RecordCanBeDeleted($appFrw, array('SliderID'=>$SliderID) );
		if($canBeDeleted["success"] == false)
		{
			$results["success"] = false;
			$results["reason"] = $canBeDeleted["reason"];
			
			return $results;
		}
		
		//delete slider from sys_node_slider
		$query = "DELETE FROM sys_node_slider WHERE NodeSliderSliderID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SliderID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();
		
		//delete slider from sys_categories_records_slider
		$query = "DELETE FROM sys_categories_records_slider WHERE CtgRecSliderSliderID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SliderID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();
				
		//delete slider from sys_slider
		$query = "DELETE FROM sys_slider WHERE SliderID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SliderID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SliderID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_slider::sys_slider_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_slider_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
				
		$query = "	SELECT
						
							SliderID
							,SliderSiteID
							,SliderTitle
							,SliderStatus
						
							FROM sys_slider
							
							WHERE
							(
								SliderSiteID = ?
							)
							AND
							(
								SliderTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								SliderStatus = 1
							)

							ORDER BY SliderTitle ASC			
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $SiteID, $filterStrToFind,$filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	
}
