<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_footerlng
*
* DESCRIPTION: 
*	Class for table sys_footerlng
*
* table fields:
*
 `FtrLngID` int(11) NOT NULL,
 `FtrLngTitle` varchar(1024) NOT NULL,
 `FtrLngType` int(11) NOT NULL,
 `FtrLngFtrID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_footerlng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footerlng::sys_footerlng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_footerlng_get_NewRecordDefValues($appFrw, $params)
	{	
		$FtrLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_footerlng');
		
		if($FtrLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["FtrLngID"] = $FtrLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_footerlng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footerlng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $FtrLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT FtrLngID FROM sys_footerlng WHERE FtrLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FtrLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footerlng::sys_footerlng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footerlng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrLngID 	= (int)$params["FtrLngID"];
		
		if($FtrLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_footerlng::check_RecordExists($appFrw, $FtrLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$FtrLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_footerlng
					(
						 FtrLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FtrLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_footerlng::sys_footerlng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footerlng::sys_footerlng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footerlng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrLngID = (int)$params["FtrLngID"];
		$FtrLngType = $params["FtrLngType"];
		
		if($FtrLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_footerlng::check_RecordExists($appFrw, $FtrLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$FtrLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						FtrLngID
						,FtrLngTitle 
						,FtrLngType 
						,FtrLngFtrID 
							
					FROM sys_footerlng
					WHERE
					(
						FtrLngID = ?
					)
					AND
					(
						FtrLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $FtrLngID, $FtrLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footerlng::sys_footerlng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footerlng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrLngID = (int)$params["FtrLngID"];
		$FtrLngType 	= (isset($params['FtrLngType'])) ? $params['FtrLngType'] : $record['FtrLngType'];
		
		if($FtrLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_footerlng::sys_footerlng_getRecord($appFrw, array('FtrLngID'=>$FtrLngID,'FtrLngType'=>$FtrLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$FtrLngTitle 	= (isset($params['FtrLngTitle'])) ? $params['FtrLngTitle'] : $record['FtrLngTitle'];
		$FtrLngFtrID 	= (isset($params['FtrLngFtrID'])) ? $params['FtrLngFtrID'] : $record['FtrLngFtrID'];
		
		$query = "	UPDATE sys_footerlng SET
						 FtrLngTitle 					= ?
						,FtrLngType 					= ?
						,FtrLngFtrID 		= ?
						
					WHERE
					FtrLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siii",
								 $FtrLngTitle 			
								,$FtrLngType 
								,$FtrLngFtrID 
								,$FtrLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $FtrLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footerlng::sys_sys_footerlng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_sys_footerlng_CheckBeforeInsert($appFrw, $params)
	{
		$FtrLngFtrID	= isset($params["FtrLngFtrID"]) ? (int)$params["FtrLngFtrID"] : 0;
		$FtrLngType	= isset($params["FtrLngType"]) ? (int)$params["FtrLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT FtrLngID FROM sys_footerlng WHERE FtrLngFtrID = ? AND FtrLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $FtrLngFtrID, $FtrLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footerlng::sys_footerlng_GetFooterLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_footerlng_GetFooterLngID($appFrw, $params)
	{
		$FtrLngFtrID	= isset($params["FtrLngFtrID"]) ? (int)$params["FtrLngFtrID"] : 0;
		$FtrLngType	= isset($params["FtrLngType"]) ? (int)$params["FtrLngType"] : 0;
		
		$query = "	SELECT
						   FtrLngID
						   FROM sys_footerlng
							WHERE FtrLngFtrID = ? AND FtrLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $FtrLngFtrID, $FtrLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["FtrLngID"];
	}
	
	
	
}
