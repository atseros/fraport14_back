<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_dictionary
*
* DESCRIPTION: 
*	Class for table sys_dictionary
*
* table fields:
*
* DicID
* DicName
*
*********************************************************************************************/
class DB_sys_dictionary
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionary::sys_dictionary_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionary_get_NewRecordDefValues($appFrw, $params)
	{
		$DicID = DB_sys_kxn::get_NextID($appFrw, 'sys_dictionary');
		
		if($DicID > 0)
		{
			$results["success"] = true;
			$results["data"]["DicID"] = $DicID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_dictionary";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionary::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $DicID)
	{
		$query = "	SELECT
						   case when( exists (SELECT DicID FROM sys_dictionary WHERE DicID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $DicID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionary::sys_dictionary_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionary_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$DicID 	= (int)$params["DicID"];
		
		if($DicID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_dictionary::check_RecordExists($appFrw, $DicID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$DicID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_dictionary
					(
						 DicID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $DicID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_dictionary::sys_dictionary_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionary::sys_dictionary_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionary_getRecord($appFrw, $params)
	{
		$results = array();
		
		$DicID = (int)$params["DicID"];
		
		if($DicID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_dictionary::check_RecordExists($appFrw, $DicID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$DicID;
			return $results;
		}
		
		$query = "	SELECT
						
						DicID
						,DicName					
							
					FROM sys_dictionary
					WHERE
					DicID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $DicID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionary::sys_dictionary_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionary_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$DicID = (int)$params["DicID"];
		
		
		if($DicID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_dictionary::sys_dictionary_getRecord($appFrw, array('DicID'=>$DicID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$DicName 		= (isset($params['DicName'])) ? $params['DicName'] : $record['DicName'];
		
		$query = "	UPDATE sys_dictionary SET
						 DicName 			= ?
						
					WHERE
					DicID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("si",
								 $DicName 			
								,$DicID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $DicID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionary::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionary::sys_dictionary_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionary_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$DicID = (int)$params["DicID"];
		
		if($DicID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_dictionary WHERE DicID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $DicID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $DicID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_dictionary::sys_dictionary_get_List
	* --------------------------------------------------------------------------
	*/
	public static function sys_dictionary_get_List($appFrw, $params)
	{
		$results = array();
		
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
				
		$query = "	SELECT
						
							DicID
							,DicName
						
							FROM sys_dictionary
							
							WHERE
							(
								DicName LIKE ?
							)
							
							ORDER BY DicName ASC
							
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_dictionary_get_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("s", $filterStrToFind);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_dictionary_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_dictionary_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
}
