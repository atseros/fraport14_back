<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");
require_once(realpath(__DIR__)."/class.DB_sys_site_airlinelng.php");

/*********************************************************************************************
* CLASS DB_sys_site_airline
*
* DESCRIPTION: 
*	Class for table sys_site_airline
*
* table fields:
*
 `NodeMediaID` int(11) NOT NULL,
 `NodeMediaNodeID` int(11) NOT NULL,
 `NodeMediaMediaID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_site_airline
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_get_NewRecordDefValues($appFrw, $params)
	{	
		$SiteAirlineID = DB_sys_kxn::get_NextID($appFrw, 'sys_site_airline');
		
		if($SiteAirlineID > 0)
		{
			$results["success"] = true;
			$results["data"]["SiteAirlineID"] = $SiteAirlineID;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_site_airline";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SiteAirlineID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SiteAirlineID FROM sys_site_airline WHERE SiteAirlineID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteAirlineID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteAirlineID 	= (int)$params["SiteAirlineID"];
		
		if($SiteAirlineID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_site_airline::check_RecordExists($appFrw, $SiteAirlineID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SiteAirlineID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_site_airline
					(
						 SiteAirlineID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SiteAirlineID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_site_airline::sys_site_airline_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteAirlineID = (int)$params["SiteAirlineID"];
		
		if($SiteAirlineID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_site_airline::check_RecordExists($appFrw, $SiteAirlineID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SiteAirlineID;
			return $results;
		}
		
		$query = "	SELECT
						
						SiteAirlineID
						,SiteAirlineAirlineID
						,SiteAirlineSiteID
							
					FROM sys_site_airline
					WHERE
					SiteAirlineID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SiteAirlineID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;

		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteAirlineID = (int)$params["SiteAirlineID"];
		
		
		if($SiteAirlineID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_site_airline::sys_site_airline_getRecord($appFrw, array('SiteAirlineID'=>$SiteAirlineID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$SiteAirlineAirlineID 	= (isset($params['SiteAirlineAirlineID'])) ? $params['SiteAirlineAirlineID'] : $record['SiteAirlineAirlineID'];
		$SiteAirlineSiteID 	= (isset($params['SiteAirlineSiteID'])) ? $params['SiteAirlineSiteID'] : $record['SiteAirlineSiteID'];
		
		$query = "	UPDATE sys_site_airline SET
							
							SiteAirlineAirlineID 	= ?
							,SiteAirlineSiteID 	= ?
						
							WHERE
							SiteAirlineID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iii",
								 $SiteAirlineAirlineID 			
								,$SiteAirlineSiteID 															
								,$SiteAirlineID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SiteAirlineID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteAirlineID = (int)$params["SiteAirlineID"];
		
		if($SiteAirlineID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}

		$query = "DELETE FROM sys_site_airline WHERE SiteAirlineID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteAirlineID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SiteAirlineID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_get_List
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_get_List($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
				
		$query = "	SELECT

							SiteAirlineID
							,SiteAirlineAirlineID
							,SiteAirlineSiteID
							,C_AirlinesID
							,C_AirlinesName
							,C_AirlinesWebsite
							,C_AirlinesFileName
							,C_AirlinesE_Booking
							,C_AirlinesE_Check_In
							,C_AirlinesEscapeDefault
							,C_AirlinesOnlineContact
							
							FROM sys_site_airline
							LEFT JOIN sys_central_airlines ON ( C_AirlinesID = SiteAirlineAirlineID )
							
							WHERE
							(
								SiteAirlineSiteID = ?
							)
							AND
							(
								C_AirlinesName LIKE ?
							)
							

							ORDER BY C_AirlinesName ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("is", $SiteID, $filterStrToFind);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			// $row['C_AirlinesFullUrl']= DB_sys_site_airline::sys_site_airline_FullUrl($appFrw, $row['C_AirlinesID']);
			$row['C_AirlinesLogoFullUrl']= DB_sys_site_airline::sys_site_airline_LogoFullUrl($appFrw, $row['C_AirlinesID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_FullUrl($appFrw, $C_AirlinesID)
	{
		$url = FileManager::getTblFolderUrl("sys_central_airlines", $C_AirlinesID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_central_airlines::sys_central_airlines_getRecord($appFrw,array('C_AirlinesID'=>$C_AirlinesID));
		if( $imageData["success"] == true  && $imageData["data"]["C_AirlinesFileName"] )
		{
			$FulUrl = $url.$imageData["data"]["C_AirlinesFileName"];
		}
		return $FulUrl;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_LogoFullUrl
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_LogoFullUrl($appFrw, $C_AirlinesID)
	{
		$url = FileManager::getTblFolderUrl("sys_central_airlines", $C_AirlinesID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_central_airlines::sys_central_airlines_getRecord($appFrw,array('C_AirlinesID'=>$C_AirlinesID));
		if( $imageData["success"] == true  && $imageData["data"]["C_AirlinesFileNameLogo"] )
		{
			$FulUrl = $url.$imageData["data"]["C_AirlinesFileNameLogo"];
		}
		return $FulUrl;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airline_CheckBeforeInsert($appFrw, $params)
	{
		$SiteAirlineSiteID	= isset($params["SiteAirlineSiteID"]) ? (int)$params["SiteAirlineSiteID"] : 0;
		$SiteAirlineAirlineID	= isset($params["SiteAirlineAirlineID"]) ? (int)$params["SiteAirlineAirlineID"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT SiteAirlineID FROM sys_site_airline WHERE SiteAirlineSiteID = ? AND SiteAirlineAirlineID = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $SiteAirlineSiteID, $SiteAirlineAirlineID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}

    /*
    * --------------------------------------------------------------------------
    * DB_sys_site_airline::sys_site_airline_get_ExportList
    * --------------------------------------------------------------------------
    */
    public static function sys_site_airline_get_ExportList($appFrw, $params)
    {
        $results = array();

        $SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;

        $query = "	SELECT

                         SiteAirlineID
                        ,C_AirlinesName
                        ,C_AirlinesWebsite
                        ,C_AirlinesOnlineContact
                        ,C_AirlinesE_Check_In
                        ,C_AirlinesE_Booking
                        ,C_AirlinesEscapeDefault
					
					    ,(
					        SELECT sys_site_airlinelng_phone_en.SiteAirlineLngAirportPhones
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_en
					        WHERE
					        sys_site_airlinelng_phone_en.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_en.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngAirportPhones_EN
					    ,(
					        SELECT sys_site_airlinelng_phone_en.SiteAirlineLngReservations
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_en
					        WHERE
					        sys_site_airlinelng_phone_en.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_en.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngReservations_EN
					    ,(
					        SELECT sys_site_airlinelng_phone_en.SiteAirlineLngLostAndFound
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_en
					        WHERE
					        sys_site_airlinelng_phone_en.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_en.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngLostAndFound_EN
					    ,(
					        SELECT sys_site_airlinelng_phone_en.SiteAirlineLngHandler
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_en
					        WHERE
					        sys_site_airlinelng_phone_en.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_en.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngHandler_EN
					    ,(
					        SELECT sys_site_airlinelng_phone_en.SiteAirlineLngHandlerAirportPhone
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_en
					        WHERE
					        sys_site_airlinelng_phone_en.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_en.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngHandlerAirportPhone_EN
					    ,(
					        SELECT sys_site_airlinelng_phone_en.SiteAirlineLngEmail
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_en
					        WHERE
					        sys_site_airlinelng_phone_en.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_en.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngEmail_EN
					    
					    
					    ,(
					        SELECT sys_site_airlinelng_phone_el.SiteAirlineLngAirportPhones
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_el
					        WHERE
					        sys_site_airlinelng_phone_el.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_el.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngAirportPhones_EL
					    ,(
					        SELECT sys_site_airlinelng_phone_el.SiteAirlineLngReservations
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_el
					        WHERE
					        sys_site_airlinelng_phone_el.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_el.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngReservations_EL
					    ,(
					        SELECT sys_site_airlinelng_phone_el.SiteAirlineLngLostAndFound
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_el
					        WHERE
					        sys_site_airlinelng_phone_el.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_el.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngLostAndFound_EL
					    ,(
					        SELECT sys_site_airlinelng_phone_el.SiteAirlineLngHandler
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_el
					        WHERE
					        sys_site_airlinelng_phone_el.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_el.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngHandler_EL
					    ,(
					        SELECT sys_site_airlinelng_phone_el.SiteAirlineLngHandlerAirportPhone
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_el
					        WHERE
					        sys_site_airlinelng_phone_el.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_el.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngHandlerAirportPhone_EL
					    ,(
					        SELECT sys_site_airlinelng_phone_el.SiteAirlineLngEmail
					        FROM sys_site_airlinelng AS sys_site_airlinelng_phone_el
					        WHERE
					        sys_site_airlinelng_phone_el.SiteAirlineLngType = 1
					        AND 
					        sys_site_airlinelng_phone_el.SiteAirlineLngSiteAirlineID = SiteAirlineID
					        
					        LIMIT 1
					    ) AS SiteAirlineLngEmail_EL
					    
					FROM sys_site_airline
					LEFT JOIN sys_central_airlines ON ( C_AirlinesID = SiteAirlineAirlineID )
					
					WHERE
					(
						SiteAirlineSiteID = ?
					)
					
					ORDER BY C_AirlinesName ASC
		";

        //exit($query);
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
        {
            $results["success"] = false;
            $results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
            return $results;
        }

        $stmt->bind_param("i", $SiteID);

        if(!$stmt->execute())
        {
            $results["success"] = false;
            $results["reason"] = "error at select: ".$stmt->error;
            return $results;
        }

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
        {
            $results["success"] = false;
            $results["reason"] = "error at select: ".$stmt->error;
            return $results;
        }

        $results['data'] = array();

        while( $row = $result->fetch_assoc())
        {
            array_push($results['data'], $row);
        }
        $result->close();

        // return results
        $results["success"] = true;

        return $results;
    }

    /*
	* --------------------------------------------------------------------------
	* DB_sys_site_airline::sys_site_airline_update_site_airlines
	* --------------------------------------------------------------------------
	*/
    public static function sys_site_airline_update_site_airlines($appFrw, $params)
    {
        require_once(realpath(__DIR__."/../libphp")."/PHPExcel_1.8.0_doc/PHPExcel/IOFactory.php");
        require_once(realpath(__DIR__."/../libphp")."/PHPExcel_1.8.0_doc/PHPExcel/Calculation/DateTime.php");

        $SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;

        $filename   = $params["ExcelAirlines"]["ExcelAirlines"]["tmp_name"];
        $fileType   = 'Excel5';


        $objReader = PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel = $objReader->load($filename);

        $objPHPExcel->setActiveSheetIndex(0);
        $sheet1 = $objPHPExcel->getActiveSheet();

        $count = 0;
        foreach ($objPHPExcel->setActiveSheetIndex(0)->getRowIterator() as $row)
        {
            $count++;
        }

        for($i = 2;$i<=$count; $i++)
        {
            $SiteAirlineID = 0;

            $SiteAirlineID = $sheet1->getCell('A'.$i)->getValue();
            $SiteAirlineID = addslashes ($SiteAirlineID);
            $SiteAirlineID = str_replace("\n", "", $SiteAirlineID);
            $SiteAirlineID = str_replace("\r", "", $SiteAirlineID);

            $SiteAirlineID = (int)$SiteAirlineID;

            if($SiteAirlineID > 0){
                if( DB_sys_site_airline::check_RecordExists($appFrw, $SiteAirlineID) )
                {
                    //find C_AirlinesID from sys_site_airline table where SiteAirlineID and SiteID
                    $params = array();
                    $params['SiteAirlineID'] = $SiteAirlineID;

                    $results = DB_sys_site_airline::sys_site_airline_getRecord($appFrw, $params);

                    if($SiteID == $results["data"]["SiteAirlineSiteID"]) {
                        $C_AirlinesID = $results["data"]["SiteAirlineAirlineID"];

                        //update sys_central_airlines table
                        $params = array();

                        $params['C_AirlinesID'] = $C_AirlinesID;

                        $params['C_AirlinesName'] = $sheet1->getCell('B' . $i)->getValue();
                        $params['C_AirlinesName'] = addslashes($params['C_AirlinesName']);
                        $params['C_AirlinesName'] = str_replace("\n", "", $params['C_AirlinesName']);
                        $params['C_AirlinesName'] = str_replace("\r", "", $params['C_AirlinesName']);

                        $params['C_AirlinesWebsite'] = $sheet1->getCell('C' . $i)->getValue();
                        $params['C_AirlinesWebsite'] = addslashes($params['C_AirlinesWebsite']);
                        $params['C_AirlinesWebsite'] = str_replace("\n", "", $params['C_AirlinesWebsite']);
                        $params['C_AirlinesWebsite'] = str_replace("\r", "", $params['C_AirlinesWebsite']);

                        $params['C_AirlinesOnlineContact'] = $sheet1->getCell('D' . $i)->getValue();
                        $params['C_AirlinesOnlineContact'] = addslashes($params['C_AirlinesOnlineContact']);
                        $params['C_AirlinesOnlineContact'] = str_replace("\n", "", $params['C_AirlinesOnlineContact']);
                        $params['C_AirlinesOnlineContact'] = str_replace("\r", "", $params['C_AirlinesOnlineContact']);

                        $params['C_AirlinesE_Check_In'] = $sheet1->getCell('E' . $i)->getValue();
                        $params['C_AirlinesE_Check_In'] = addslashes($params['C_AirlinesE_Check_In']);
                        $params['C_AirlinesE_Check_In'] = str_replace("\n", "", $params['C_AirlinesE_Check_In']);
                        $params['C_AirlinesE_Check_In'] = str_replace("\r", "", $params['C_AirlinesE_Check_In']);

                        $params['C_AirlinesE_Booking'] = $sheet1->getCell('F' . $i)->getValue();
                        $params['C_AirlinesE_Booking'] = addslashes($params['C_AirlinesE_Booking']);
                        $params['C_AirlinesE_Booking'] = str_replace("\n", "", $params['C_AirlinesE_Booking']);
                        $params['C_AirlinesE_Booking'] = str_replace("\r", "", $params['C_AirlinesE_Booking']);

                        $params['C_AirlinesEscapeDefault'] = $sheet1->getCell('G' . $i)->getValue();
                        $params['C_AirlinesEscapeDefault'] = addslashes($params['C_AirlinesEscapeDefault']);
                        $params['C_AirlinesEscapeDefault'] = str_replace("\n", "", $params['C_AirlinesEscapeDefault']);
                        $params['C_AirlinesEscapeDefault'] = str_replace("\r", "", $params['C_AirlinesEscapeDefault']);

                        $results = DB_sys_central_airlines::sys_central_airlines_UpdateRecord($appFrw, $params);

                        //update sys_site_airlinelng table
                        $params = array();
                        $params['SiteAirlineLngSiteAirlineID'] = $SiteAirlineID;

                        $params['SiteAirlineLngAirportPhones_EN'] = $sheet1->getCell('H' . $i)->getValue();
                        $params['SiteAirlineLngAirportPhones_EN'] = addslashes($params['SiteAirlineLngAirportPhones_EN']);
                        $params['SiteAirlineLngAirportPhones_EN'] = str_replace("\n", "", $params['SiteAirlineLngAirportPhones_EN']);
                        $params['SiteAirlineLngAirportPhones_EN'] = str_replace("\r", "", $params['SiteAirlineLngAirportPhones_EN']);

                        $params['SiteAirlineLngReservations_EN'] = $sheet1->getCell('I' . $i)->getValue();
                        $params['SiteAirlineLngReservations_EN'] = addslashes($params['SiteAirlineLngReservations_EN']);
                        $params['SiteAirlineLngReservations_EN'] = str_replace("\n", "", $params['SiteAirlineLngReservations_EN']);
                        $params['SiteAirlineLngReservations_EN'] = str_replace("\r", "", $params['SiteAirlineLngReservations_EN']);

                        $params['SiteAirlineLngLostAndFound_EN'] = $sheet1->getCell('J' . $i)->getValue();
                        $params['SiteAirlineLngLostAndFound_EN'] = addslashes($params['SiteAirlineLngLostAndFound_EN']);
                        $params['SiteAirlineLngLostAndFound_EN'] = str_replace("\n", "", $params['SiteAirlineLngLostAndFound_EN']);
                        $params['SiteAirlineLngLostAndFound_EN'] = str_replace("\r", "", $params['SiteAirlineLngLostAndFound_EN']);

                        $params['SiteAirlineLngHandler_EN'] = $sheet1->getCell('K' . $i)->getValue();
                        $params['SiteAirlineLngHandler_EN'] = addslashes($params['SiteAirlineLngHandler_EN']);
                        $params['SiteAirlineLngHandler_EN'] = str_replace("\n", "", $params['SiteAirlineLngHandler_EN']);
                        $params['SiteAirlineLngHandler_EN'] = str_replace("\r", "", $params['SiteAirlineLngHandler_EN']);

                        $params['SiteAirlineLngHandlerAirportPhone_EN'] = $sheet1->getCell('L' . $i)->getValue();
                        $params['SiteAirlineLngHandlerAirportPhone_EN'] = addslashes($params['SiteAirlineLngHandlerAirportPhone_EN']);
                        $params['SiteAirlineLngHandlerAirportPhone_EN'] = str_replace("\n", "", $params['SiteAirlineLngHandlerAirportPhone_EN']);
                        $params['SiteAirlineLngHandlerAirportPhone_EN'] = str_replace("\r", "", $params['SiteAirlineLngHandlerAirportPhone_EN']);

                        $params['SiteAirlineLngEmail_EN'] = $sheet1->getCell('M' . $i)->getValue();
                        $params['SiteAirlineLngEmail_EN'] = addslashes($params['SiteAirlineLngEmail_EN']);
                        $params['SiteAirlineLngEmail_EN'] = str_replace("\n", "", $params['SiteAirlineLngEmail_EN']);
                        $params['SiteAirlineLngEmail_EN'] = str_replace("\r", "", $params['SiteAirlineLngEmail_EN']);

                        $params['SiteAirlineLngAirportPhones_EL'] = $sheet1->getCell('N' . $i)->getValue();
                        $params['SiteAirlineLngAirportPhones_EL'] = addslashes($params['SiteAirlineLngAirportPhones_EL']);
                        $params['SiteAirlineLngAirportPhones_EL'] = str_replace("\n", "", $params['SiteAirlineLngAirportPhones_EL']);
                        $params['SiteAirlineLngAirportPhones_EL'] = str_replace("\r", "", $params['SiteAirlineLngAirportPhones_EL']);

                        $params['SiteAirlineLngReservations_EL'] = $sheet1->getCell('O' . $i)->getValue();
                        $params['SiteAirlineLngReservations_EL'] = addslashes($params['SiteAirlineLngReservations_EL']);
                        $params['SiteAirlineLngReservations_EL'] = str_replace("\n", "", $params['SiteAirlineLngReservations_EL']);
                        $params['SiteAirlineLngReservations_EL'] = str_replace("\r", "", $params['SiteAirlineLngReservations_EL']);

                        $params['SiteAirlineLngLostAndFound_EL'] = $sheet1->getCell('P' . $i)->getValue();
                        $params['SiteAirlineLngLostAndFound_EL'] = addslashes($params['SiteAirlineLngLostAndFound_EL']);
                        $params['SiteAirlineLngLostAndFound_EL'] = str_replace("\n", "", $params['SiteAirlineLngLostAndFound_EL']);
                        $params['SiteAirlineLngLostAndFound_EL'] = str_replace("\r", "", $params['SiteAirlineLngLostAndFound_EL']);

                        $params['SiteAirlineLngHandler_EL'] = $sheet1->getCell('Q' . $i)->getValue();
                        $params['SiteAirlineLngHandler_EL'] = addslashes($params['SiteAirlineLngHandler_EL']);
                        $params['SiteAirlineLngHandler_EL'] = str_replace("\n", "", $params['SiteAirlineLngHandler_EL']);
                        $params['SiteAirlineLngHandler_EL'] = str_replace("\r", "", $params['SiteAirlineLngHandler_EL']);

                        $params['SiteAirlineLngHandlerAirportPhone_EL'] = $sheet1->getCell('R' . $i)->getValue();
                        $params['SiteAirlineLngHandlerAirportPhone_EL'] = addslashes($params['SiteAirlineLngHandlerAirportPhone_EL']);
                        $params['SiteAirlineLngHandlerAirportPhone_EL'] = str_replace("\n", "", $params['SiteAirlineLngHandlerAirportPhone_EL']);
                        $params['SiteAirlineLngHandlerAirportPhone_EL'] = str_replace("\r", "", $params['SiteAirlineLngHandlerAirportPhone_EL']);

                        $params['SiteAirlineLngEmail_EL'] = $sheet1->getCell('S' . $i)->getValue();
                        $params['SiteAirlineLngEmail_EL'] = addslashes($params['SiteAirlineLngEmail_EL']);
                        $params['SiteAirlineLngEmail_EL'] = str_replace("\n", "", $params['SiteAirlineLngEmail_EL']);
                        $params['SiteAirlineLngEmail_EL'] = str_replace("\r", "", $params['SiteAirlineLngEmail_EL']);

                        $results = DB_sys_site_airlinelng::sys_site_airlinelng_UpdateAirlineLngRecord($appFrw, $params);
                    }
                }
            }
        }

        $results = array();

        $results["success"] = true;
        $results["data"]    = $sheet1->getCell('A2')->getValue();

        return $results;

    }
}
