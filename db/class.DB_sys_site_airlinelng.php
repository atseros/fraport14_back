<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_site_airlinelng
*
* DESCRIPTION: 
*	Class for table sys_site_airlinelng
*
* table fields:
*
 `SiteAirlineLngID` int(11) NOT NULL,
 `SiteAirlineLngSiteAirlineID` int(11) NOT NULL,
 `SiteAirlineLngCheckInCounters` varchar(2048) NOT NULL,
 `SiteAirlineLngAirportPhones` varchar(2048) NOT NULL,
 `SiteAirlineLngType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_site_airlinelng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airlinelng::sys_site_airlinelng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_site_airlinelng_get_NewRecordDefValues($appFrw, $params)
	{	
		$SiteAirlineLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_site_airlinelng');
		
		if($SiteAirlineLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["SiteAirlineLngID"] = $SiteAirlineLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_site_airlinelng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airlinelng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SiteAirlineLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SiteAirlineLngID FROM sys_site_airlinelng WHERE SiteAirlineLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteAirlineLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airlinelng::sys_site_airlinelng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airlinelng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteAirlineLngID 	= (int)$params["SiteAirlineLngID"];
		
		if($SiteAirlineLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_site_airlinelng::check_RecordExists($appFrw, $SiteAirlineLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SiteAirlineLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_site_airlinelng
					(
						 SiteAirlineLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SiteAirlineLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_site_airlinelng::sys_site_airlinelng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airlinelng::sys_site_airlinelng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airlinelng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteAirlineLngID = (int)$params["SiteAirlineLngID"];
		$SiteAirlineLngType = $params["SiteAirlineLngType"];
		
		if($SiteAirlineLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_site_airlinelng::check_RecordExists($appFrw, $SiteAirlineLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SiteAirlineLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						SiteAirlineLngID
						,SiteAirlineLngSiteAirlineID  
						,SiteAirlineLngAirportPhones 
						,SiteAirlineLngType 
						,SiteAirlineLngReservations 
						,SiteAirlineLngLostAndFound 
						,SiteAirlineLngHandler
						,SiteAirlineLngHandlerAirportPhone
						,SiteAirlineLngEmail
							
					FROM sys_site_airlinelng
					WHERE
					(
						SiteAirlineLngID = ?
					)
					AND
					(
						SiteAirlineLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $SiteAirlineLngID, $SiteAirlineLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airlinelng::sys_site_airlinelng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airlinelng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteAirlineLngID = (int)$params["SiteAirlineLngID"];
		$SiteAirlineLngType = (isset($params['SiteAirlineLngType'])) ? $params['SiteAirlineLngType'] : $record['SiteAirlineLngType'];
		
		if($SiteAirlineLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_site_airlinelng::sys_site_airlinelng_getRecord($appFrw, array('SiteAirlineLngID'=>$SiteAirlineLngID,'SiteAirlineLngType'=>$SiteAirlineLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$SiteAirlineLngSiteAirlineID 		= (isset($params['SiteAirlineLngSiteAirlineID'])) ? $params['SiteAirlineLngSiteAirlineID'] : $record['SiteAirlineLngSiteAirlineID'];
		// $SiteAirlineLngCheckInCounters 	= (isset($params['SiteAirlineLngCheckInCounters'])) ? $params['SiteAirlineLngCheckInCounters'] : $record['SiteAirlineLngCheckInCounters'];
		$SiteAirlineLngAirportPhones 		= (isset($params['SiteAirlineLngAirportPhones'])) ? $params['SiteAirlineLngAirportPhones'] : $record['SiteAirlineLngAirportPhones'];
		$SiteAirlineLngReservations 		= (isset($params['SiteAirlineLngReservations'])) ? $params['SiteAirlineLngReservations'] : $record['SiteAirlineLngReservations'];
		$SiteAirlineLngLostAndFound 		= (isset($params['SiteAirlineLngLostAndFound'])) ? $params['SiteAirlineLngLostAndFound'] : $record['SiteAirlineLngLostAndFound'];
		$SiteAirlineLngHandler 				= (isset($params['SiteAirlineLngHandler'])) ? $params['SiteAirlineLngHandler'] : $record['SiteAirlineLngHandler'];
		$SiteAirlineLngHandlerAirportPhone 	= (isset($params['SiteAirlineLngHandlerAirportPhone'])) ? $params['SiteAirlineLngHandlerAirportPhone'] : $record['SiteAirlineLngHandlerAirportPhone'];
		$SiteAirlineLngEmail 				= (isset($params['SiteAirlineLngEmail'])) ? $params['SiteAirlineLngEmail'] : $record['SiteAirlineLngEmail'];
	
		
		$query = "	UPDATE sys_site_airlinelng SET
		
						 SiteAirlineLngSiteAirlineID = ?
						,SiteAirlineLngType 			= ?
						,SiteAirlineLngAirportPhones 		= ?
						,SiteAirlineLngReservations 		= ?
						,SiteAirlineLngLostAndFound 		= ?
						,SiteAirlineLngHandler 		= ?
						,SiteAirlineLngHandlerAirportPhone 		= ?
						,SiteAirlineLngEmail 		= ?
						
					WHERE
					SiteAirlineLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iissssssi",
								 $SiteAirlineLngSiteAirlineID 			
								,$SiteAirlineLngType 
								,$SiteAirlineLngAirportPhones 
								,$SiteAirlineLngReservations 
								,$SiteAirlineLngLostAndFound 
								,$SiteAirlineLngHandler 
								,$SiteAirlineLngHandlerAirportPhone 
								,$SiteAirlineLngEmail 
								,$SiteAirlineLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SiteAirlineLngID;
		return $results;
	}
	
	

	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airlinelng::sys_site_airlinelng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airlinelng_CheckBeforeInsert($appFrw, $params)
	{
		$SiteAirlineLngSiteAirlineID	= isset($params["SiteAirlineLngSiteAirlineID"]) ? (int)$params["SiteAirlineLngSiteAirlineID"] : 0;
		$SiteAirlineLngType	= isset($params["SiteAirlineLngType"]) ? (int)$params["SiteAirlineLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT SiteAirlineLngID FROM sys_site_airlinelng WHERE SiteAirlineLngSiteAirlineID = ? AND SiteAirlineLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $SiteAirlineLngSiteAirlineID, $SiteAirlineLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site_airlinelng::sys_site_airlinelng_GetSiteAirlineLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_airlinelng_GetSiteAirlineLngID($appFrw, $params)
	{
		$SiteAirlineLngSiteAirlineID	= isset($params["SiteAirlineLngSiteAirlineID"]) ? (int)$params["SiteAirlineLngSiteAirlineID"] : 0;
		$SiteAirlineLngType	= isset($params["SiteAirlineLngType"]) ? (int)$params["SiteAirlineLngType"] : 0;
		
		$query = "	SELECT
						   SiteAirlineLngID
						   FROM sys_site_airlinelng
							WHERE SiteAirlineLngSiteAirlineID = ? AND SiteAirlineLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $SiteAirlineLngSiteAirlineID, $SiteAirlineLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["SiteAirlineLngID"];
	}

    /*
    * --------------------------------------------------------------------------
    * DB_sys_site_airlinelng::sys_site_airlinelng_UpdateAirlineLngRecord
    * --------------------------------------------------------------------------
    */
    public static function sys_site_airlinelng_UpdateAirlineLngRecord($appFrw, $params)
    {
        $results = array();

        $SiteAirlineLngSiteAirlineID = (isset($params['SiteAirlineLngSiteAirlineID'])) ? $params['SiteAirlineLngSiteAirlineID'] : 0;

        if($SiteAirlineLngSiteAirlineID <= 0)
        {
            $results["success"] = false;
            $results["reason"] = "update_Record: No id found. Can not update record";
            return $results;
        }

        // get param fields
        $SiteAirlineLngAirportPhones_EN 		= (isset($params['SiteAirlineLngAirportPhones_EN'])) ? $params['SiteAirlineLngAirportPhones_EN'] : '';
        $SiteAirlineLngReservations_EN 		    = (isset($params['SiteAirlineLngReservations_EN'])) ? $params['SiteAirlineLngReservations_EN'] : '';
        $SiteAirlineLngLostAndFound_EN 	        = (isset($params['SiteAirlineLngLostAndFound_EN'])) ? $params['SiteAirlineLngLostAndFound_EN'] : '';
        $SiteAirlineLngHandler_EN 		        = (isset($params['SiteAirlineLngHandler_EN'])) ? $params['SiteAirlineLngHandler_EN'] : '';
        $SiteAirlineLngHandlerAirportPhone_EN   = (isset($params['SiteAirlineLngHandlerAirportPhone_EN'])) ? $params['SiteAirlineLngHandlerAirportPhone_EN'] : '';
        $SiteAirlineLngEmail_EN 	            = (isset($params['SiteAirlineLngEmail_EN'])) ? $params['SiteAirlineLngEmail_EN'] : '';

        $SiteAirlineLngAirportPhones_EL 		= (isset($params['SiteAirlineLngAirportPhones_EL'])) ? $params['SiteAirlineLngAirportPhones_EL'] : '';
        $SiteAirlineLngReservations_EL 		    = (isset($params['SiteAirlineLngReservations_EL'])) ? $params['SiteAirlineLngReservations_EL'] : '';
        $SiteAirlineLngLostAndFound_EL 	        = (isset($params['SiteAirlineLngLostAndFound_EL'])) ? $params['SiteAirlineLngLostAndFound_EL'] : '';
        $SiteAirlineLngHandler_EL		        = (isset($params['SiteAirlineLngHandler_EL'])) ? $params['SiteAirlineLngHandler_EL'] : '';
        $SiteAirlineLngHandlerAirportPhone_EL   = (isset($params['SiteAirlineLngHandlerAirportPhone_EL'])) ? $params['SiteAirlineLngHandlerAirportPhone_EL'] : '';
        $SiteAirlineLngEmail_EL	                = (isset($params['SiteAirlineLngEmail_EL'])) ? $params['SiteAirlineLngEmail_EL'] : '';

        $query = "	UPDATE sys_site_airlinelng SET
						 SiteAirlineLngAirportPhones 		= ?
						,SiteAirlineLngReservations 		= ?
						,SiteAirlineLngLostAndFound         = ?
						,SiteAirlineLngHandler              = ?
						,SiteAirlineLngHandlerAirportPhone  = ?
						,SiteAirlineLngEmail                = ?						
					WHERE
					SiteAirlineLngSiteAirlineID = ?
					AND
					SiteAirlineLngType = 1
		";

        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ssssssi",
             $SiteAirlineLngAirportPhones_EN
            ,$SiteAirlineLngReservations_EN
            ,$SiteAirlineLngLostAndFound_EN
            ,$SiteAirlineLngHandler_EN
            ,$SiteAirlineLngHandlerAirportPhone_EN
            ,$SiteAirlineLngEmail_EN
            ,$SiteAirlineLngSiteAirlineID
        );

        if(!$stmt->execute())
            exit("update_Record: error at update : ".$stmt->error);

        $stmt->close();

        $query = "	UPDATE sys_site_airlinelng SET
						 SiteAirlineLngAirportPhones 		= ?
						,SiteAirlineLngReservations 		= ?
						,SiteAirlineLngLostAndFound         = ?
						,SiteAirlineLngHandler              = ?
						,SiteAirlineLngHandlerAirportPhone  = ?
						,SiteAirlineLngEmail                = ?						
					WHERE
					SiteAirlineLngSiteAirlineID = ?
					AND
					SiteAirlineLngType = 2
		";

        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ssssssi",
             $SiteAirlineLngAirportPhones_EL
            ,$SiteAirlineLngReservations_EL
            ,$SiteAirlineLngLostAndFound_EL
            ,$SiteAirlineLngHandler_EL
            ,$SiteAirlineLngHandlerAirportPhone_EL
            ,$SiteAirlineLngEmail_EL
            ,$SiteAirlineLngSiteAirlineID
        );

        if(!$stmt->execute())
            exit("update_Record: error at update : ".$stmt->error);

        $stmt->close();

        // return
        $results["success"] = true;
        $results["data"]    = $SiteAirlineLngSiteAirlineID;
        return $results;
    }
	
	
}
