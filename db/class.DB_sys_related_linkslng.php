<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_related_linkslng
*
* DESCRIPTION: 
*	Class for table sys_related_linkslng
*
* table fields:
*
 `RelatedLinkLngID` int(11) NOT NULL,
 `RelatedLinkLngTitle` varchar(1024) NOT NULL,
 `RelatedLinkLngType` int(11) NOT NULL,
 `RelatedLinkLngRelatedLinkID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_related_linkslng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_linkslng::sys_related_linkslng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_related_linkslng_get_NewRecordDefValues($appFrw, $params)
	{	
		$RelatedLinkLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_related_linkslng');
		
		if($RelatedLinkLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["RelatedLinkLngID"] = $RelatedLinkLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_related_linkslng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_linkslng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $RelatedLinkLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT RelatedLinkLngID FROM sys_related_linkslng WHERE RelatedLinkLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $RelatedLinkLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_linkslng::sys_related_linkslng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_linkslng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$RelatedLinkLngID 	= (int)$params["RelatedLinkLngID"];
		
		if($RelatedLinkLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_related_linkslng::check_RecordExists($appFrw, $RelatedLinkLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$RelatedLinkLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_related_linkslng
					(
						 RelatedLinkLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $RelatedLinkLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_related_linkslng::sys_related_linkslng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_linkslng::sys_related_linkslng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_linkslng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$RelatedLinkLngID = (int)$params["RelatedLinkLngID"];
		$RelatedLinkLngType = $params["RelatedLinkLngType"];
		
		if($RelatedLinkLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_related_linkslng::check_RecordExists($appFrw, $RelatedLinkLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$RelatedLinkLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						RelatedLinkLngID
						,RelatedLinkLngTitle 
						,RelatedLinkLngType 
						,RelatedLinkLngRelatedLinkID 
							
					FROM sys_related_linkslng
					WHERE
					(
						RelatedLinkLngID = ?
					)
					AND
					(
						RelatedLinkLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $RelatedLinkLngID, $RelatedLinkLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_linkslng::sys_related_linkslng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_linkslng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$RelatedLinkLngID = (int)$params["RelatedLinkLngID"];
		$RelatedLinkLngType 	= (isset($params['RelatedLinkLngType'])) ? $params['RelatedLinkLngType'] : $record['RelatedLinkLngType'];
		
		if($RelatedLinkLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_related_linkslng::sys_related_linkslng_getRecord($appFrw, array('RelatedLinkLngID'=>$RelatedLinkLngID,'RelatedLinkLngType'=>$RelatedLinkLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$RelatedLinkLngTitle 	= (isset($params['RelatedLinkLngTitle'])) ? $params['RelatedLinkLngTitle'] : $record['RelatedLinkLngTitle'];
		$RelatedLinkLngRelatedLinkID 	= (isset($params['RelatedLinkLngRelatedLinkID'])) ? $params['RelatedLinkLngRelatedLinkID'] : $record['RelatedLinkLngRelatedLinkID'];
		
		$query = "	UPDATE sys_related_linkslng SET
						 RelatedLinkLngTitle 					= ?
						,RelatedLinkLngType 					= ?
						,RelatedLinkLngRelatedLinkID 		= ?
						
					WHERE
					RelatedLinkLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siii",
								 $RelatedLinkLngTitle 			
								,$RelatedLinkLngType 
								,$RelatedLinkLngRelatedLinkID 
								,$RelatedLinkLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $RelatedLinkLngID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::RecordCanBeDeleted
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be checked
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_site WHERE SiteID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SiteID;
		return $results;		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_linkslng::sys_related_linkslng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_linkslng_CheckBeforeInsert($appFrw, $params)
	{
		$RelatedLinkLngRelatedLinkID	= isset($params["RelatedLinkLngRelatedLinkID"]) ? (int)$params["RelatedLinkLngRelatedLinkID"] : 0;
		$RelatedLinkLngType	= isset($params["RelatedLinkLngType"]) ? (int)$params["RelatedLinkLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT RelatedLinkLngID FROM sys_related_linkslng WHERE RelatedLinkLngRelatedLinkID = ? AND RelatedLinkLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $RelatedLinkLngRelatedLinkID, $RelatedLinkLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_related_linkslng::sys_related_linkslng_GetRelatedLinkLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_related_linkslng_GetRelatedLinkLngID($appFrw, $params)
	{
		$RelatedLinkLngRelatedLinkID	= isset($params["RelatedLinkLngRelatedLinkID"]) ? (int)$params["RelatedLinkLngRelatedLinkID"] : 0;
		$RelatedLinkLngType	= isset($params["RelatedLinkLngType"]) ? (int)$params["RelatedLinkLngType"] : 0;
		
		$query = "	SELECT
						   RelatedLinkLngID
						   FROM sys_related_linkslng
							WHERE RelatedLinkLngRelatedLinkID = ? AND RelatedLinkLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $RelatedLinkLngRelatedLinkID, $RelatedLinkLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RelatedLinkLngID"];
	}
	
	
	
}
