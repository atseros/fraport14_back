<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_cf_subjects_lang
*
* DESCRIPTION: 
*	Class for table cf_subjects_lang
*
* table fields:
*
 `SubLngID` int(11) NOT NULL,
 `SubLngSubID` int(11) NOT NULL,
 `SubLngType` int(11) NOT NULL,
 `SubLngTitle` varchar(1024) NOT NULL,
 `SubLngShowOther` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_cf_subjects_lang
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects_lang::cf_subjects_lang_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function cf_subjects_lang_get_NewRecordDefValues($appFrw, $params)
	{	
		$SubLngID = DB_sys_kxn::get_NextID($appFrw, 'cf_subjects_lang');
		
		if($SubLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["SubLngID"] = $SubLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table cf_subjects_lang";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects_lang::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SubLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SubLngID FROM cf_subjects_lang WHERE SubLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SubLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects_lang::cf_subjects_lang_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_lang_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SubLngID 	= (int)$params["SubLngID"];
		
		if($SubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_cf_subjects_lang::check_RecordExists($appFrw, $SubLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SubLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO cf_subjects_lang
					(
						 SubLngID
					)
					VALUES
					(
						 ?
					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SubLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_cf_subjects_lang::cf_subjects_lang_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects_lang::cf_subjects_lang_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_lang_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SubLngID = (int)$params["SubLngID"];
		$SubLngType = $params["SubLngType"];
		
		if($SubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_cf_subjects_lang::check_RecordExists($appFrw, $SubLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SubLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						 SubLngID
						,SubLngSubID 
						,SubLngType 
						,SubLngTitle
						,SubLngShowOther
						
					FROM cf_subjects_lang
					WHERE
					(
						SubLngID = ?
					)
					AND
					(
						SubLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $SubLngID, $SubLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_subjects_lang::cf_subjects_lang_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_subjects_lang_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SubLngID   = (int)$params["SubLngID"];
		$SubLngType = (isset($params['SubLngType'])) ? $params['SubLngType'] : $record['SubLngType'];
		
		if($SubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_cf_subjects_lang::cf_subjects_lang_getRecord($appFrw, array('SubLngID'=>$SubLngID,'SubLngType'=>$SubLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
        $SubLngSubID 	    = (isset($params['SubLngSubID'])) ? $params['SubLngSubID'] : $record['SubLngSubID'];
		$SubLngTitle 	    = (isset($params['SubLngTitle'])) ? $params['SubLngTitle'] : $record['SubLngTitle'];
		$SubLngShowOther    = (isset($params['SubLngShowOther'])) ? $params['SubLngShowOther'] : $record['SubLngShowOther'];
		
		$query = "	UPDATE cf_subjects_lang SET
						 SubLngSubID 		= ?
						,SubLngType 		= ?
						,SubLngTitle 		= ?
						,SubLngShowOther    = ?
					WHERE
					SubLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iisii",
                                 $SubLngSubID
								,$SubLngType 
								,$SubLngTitle
                                ,$SubLngShowOther
								,$SubLngID
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SubLngID;
		return $results;
	}

    /*
    * --------------------------------------------------------------------------
    * DB_cf_subjects_lang::cf_subjects_lang_CheckBeforeInsert
    * --------------------------------------------------------------------------
    */
    public static function cf_subjects_lang_CheckBeforeInsert($appFrw, $params)
    {
        $SubLngSubID    = isset($params["SubLngSubID"]) ? (int)$params["SubLngSubID"] : 0;
        $SubLngType	    = isset($params["SubLngType"]) ? (int)$params["SubLngType"] : 0;

        $query = "	SELECT
						   case when( exists (SELECT SubLngID FROM cf_subjects_lang WHERE SubLngSubID = ? AND SubLngType = ? ))
							then 1
							else 0
						end as RecordExists";
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ii", $SubLngSubID, $SubLngType);

        if(!$stmt->execute())
            exit("check_RecordExists: error at select : ".$stmt->error);

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
            exit("check_RecordExists: error at select : ".$stmt->error);

        $row = $result->fetch_assoc();
        $result->close();

        return $row["RecordExists"];
    }

    /*
    * --------------------------------------------------------------------------
    * DB_cf_subjects_lang::cf_subjects_lang_GetSubLngID
    * --------------------------------------------------------------------------
    */
    public static function cf_subjects_lang_GetSubLngID($appFrw, $params)
    {
        $SubLngSubID    = isset($params["SubLngSubID"]) ? (int)$params["SubLngSubID"] : 0;
        $SubLngType	    = isset($params["SubLngType"]) ? (int)$params["SubLngType"] : 0;

        $query = "	SELECT
						   SubLngID
					FROM cf_subjects_lang
					WHERE SubLngSubID = ? AND SubLngType = ?
				";
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ii", $SubLngSubID, $SubLngType);

        if(!$stmt->execute())
            exit("check_RecordExists: error at select : ".$stmt->error);

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
            exit("check_RecordExists: error at select : ".$stmt->error);

        $row = $result->fetch_assoc();
        $result->close();

        return $row["SubLngID"];
    }
	
}