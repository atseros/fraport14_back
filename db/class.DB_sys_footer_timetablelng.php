<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_footer_timetablelng
*
* DESCRIPTION: 
*	Class for table sys_footer_timetablelng
*
* table fields:
*
`FooterTimetableLngID` int(11) NOT NULL,
`FooterTimetableLngIDFooterTimetableID` int(11) NOT NULL,
`FooterTimetableLngTitle` varchar(1024) NOT NULL,
`FooterTimetableLngContent` text NOT NULL,
`FooterTimetableLngType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_footer_timetablelng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetablelng::sys_alertlng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function get_NewRecordDefValues($appFrw, $params)
	{	
		$FooterTimetableLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_footer_timetablelng');
		
		if($FooterTimetableLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["FooterTimetableLngID"] = $FooterTimetableLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for sys_footer_timetablelng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetablelng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $FooterTimetableLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT FooterTimetableLngID FROM sys_footer_timetablelng WHERE FooterTimetableLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FooterTimetableLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetablelng::InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$FooterTimetableLngID 	= (int)$params["FooterTimetableLngID"];
		
		if($FooterTimetableLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_footer_timetablelng::check_RecordExists($appFrw, $FooterTimetableLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$FooterTimetableLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_footer_timetablelng
					(
						 FooterTimetableLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FooterTimetableLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_footer_timetablelng::UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetablelng::getRecord
	* --------------------------------------------------------------------------
	*/
	public static function getRecord($appFrw, $params)
	{
		$results = array();
		
		$FooterTimetableLngID = (int)$params["FooterTimetableLngID"];
		$FooterTimetableLngType = $params["FooterTimetableLngType"];
		
		if($FooterTimetableLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_footer_timetablelng::check_RecordExists($appFrw, $FooterTimetableLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$FooterTimetableLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						FooterTimetableLngID
						,FooterTimetableLngIDFooterTimetableID 
						,FooterTimetableLngTitle 
						,FooterTimetableLngContent 
						,FooterTimetableLngType 
							
					FROM sys_footer_timetablelng
					WHERE
					(
						FooterTimetableLngID = ?
					)
					AND
					(
						FooterTimetableLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $FooterTimetableLngID, $FooterTimetableLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetablelng::UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$FooterTimetableLngID = (int)$params["FooterTimetableLngID"];
		$FooterTimetableLngType = (isset($params['FooterTimetableLngType'])) ? $params['FooterTimetableLngType'] : $record['FooterTimetableLngType'];
		
		if($FooterTimetableLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_footer_timetablelng::getRecord($appFrw, array('FooterTimetableLngID'=>$FooterTimetableLngID,'FooterTimetableLngType'=>$FooterTimetableLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$FooterTimetableLngIDFooterTimetableID		= (isset($params['FooterTimetableLngIDFooterTimetableID'])) ? $params['FooterTimetableLngIDFooterTimetableID'] : $record['FooterTimetableLngIDFooterTimetableID'];
		$FooterTimetableLngTitle 			= (isset($params['FooterTimetableLngTitle'])) ? $params['FooterTimetableLngTitle'] : $record['FooterTimetableLngTitle'];
		$FooterTimetableLngContent 		= (isset($params['FooterTimetableLngContent'])) ? $params['FooterTimetableLngContent'] : $record['FooterTimetableLngContent'];
		
		$query = "	UPDATE sys_footer_timetablelng SET
						 FooterTimetableLngIDFooterTimetableID 		= ?
						,FooterTimetableLngType 		= ?
						,FooterTimetableLngTitle 			= ?
						,FooterTimetableLngContent 	= ?
						
					WHERE
					FooterTimetableLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iissi",
                                $FooterTimetableLngIDFooterTimetableID
								,$FooterTimetableLngType
								,$FooterTimetableLngTitle
								,$FooterTimetableLngContent
								,$FooterTimetableLngID
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $FooterTimetableLngID;
		return $results;
	}
	

	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetablelng::CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function CheckBeforeInsert($appFrw, $params)
	{
		$FooterTimetableLngIDFooterTimetableID	= isset($params["FooterTimetableLngIDFooterTimetableID"]) ? (int)$params["FooterTimetableLngIDFooterTimetableID"] : 0;
		$FooterTimetableLngType		= isset($params["FooterTimetableLngType"]) ? (int)$params["FooterTimetableLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT FooterTimetableLngID FROM sys_footer_timetablelng WHERE FooterTimetableLngIDFooterTimetableID = ? AND FooterTimetableLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $FooterTimetableLngIDFooterTimetableID, $FooterTimetableLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetablelng::GetLngID
	* --------------------------------------------------------------------------
	*/
	public static function GetLngID($appFrw, $params)
	{
		$FooterTimetableLngIDFooterTimetableID	= isset($params["FooterTimetableLngIDFooterTimetableID"]) ? (int)$params["FooterTimetableLngIDFooterTimetableID"] : 0;
		$FooterTimetableLngType		= isset($params["FooterTimetableLngType"]) ? (int)$params["FooterTimetableLngType"] : 0;
		
		$query = "	SELECT
						   FooterTimetableLngID
						   FROM sys_footer_timetablelng
							WHERE FooterTimetableLngIDFooterTimetableID = ? AND FooterTimetableLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $FooterTimetableLngIDFooterTimetableID, $FooterTimetableLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["FooterTimetableLngID"];
	}
	
	
	
}
