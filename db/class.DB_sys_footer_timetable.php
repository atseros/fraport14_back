<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_footer_timetable
*
* DESCRIPTION: 
*	Class for table sys_footer_timetable
*
* table fields:
*
`FooterTimetableID` int(11) NOT NULL,
`FooterTimetableStatus` int(11) NOT NULL,
`FooterTimetableTitle` varchar(1024) NOT NULL,
`FooterTimetableSiteID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_footer_timetable
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetable::get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID 	= (int)$params["SiteID"];
		
		$FooterTimetableID = DB_sys_kxn::get_NextID($appFrw, 'sys_footer_timetable');
		
		if($FooterTimetableID > 0)
		{
			$results["success"] = true;
			$results["data"]["FooterTimetableID"] = $FooterTimetableID;
			$results["data"]["FooterTimetableSiteID"] = $SiteID;
			$results["data"]["FooterTimetableStatus"] = 1;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_footer_timetable";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetable::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $FooterTimetableID)
	{
		$query = "	SELECT
						   case when( exists (SELECT FooterTimetableID FROM sys_footer_timetable WHERE FooterTimetableID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FooterTimetableID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetable::InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$FooterTimetableID 	= (int)$params["FooterTimetableID"];
		
		if($FooterTimetableID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_footer_timetable::check_RecordExists($appFrw, $FooterTimetableID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$FooterTimetableID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_footer_timetable
					(
						 FooterTimetableID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FooterTimetableID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_footer_timetable::UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetable::getRecord
	* --------------------------------------------------------------------------
	*/
	public static function getRecord($appFrw, $params)
	{
		$results = array();

        $FooterTimetableID = (int)$params["FooterTimetableID"];
		
		if($FooterTimetableID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_footer_timetable::check_RecordExists($appFrw, $FooterTimetableID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$FooterTimetableID;
			return $results;
		}
		
		$query = "	SELECT
						
						FooterTimetableID
						,FooterTimetableSiteID
						,FooterTimetableStatus
						,FooterTimetableTitle
							
					FROM sys_footer_timetable
					WHERE
					FooterTimetableID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FooterTimetableID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetable::UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function UpdateRecord($appFrw, $params)
	{
		$results = array();

        $FooterTimetableID = (int)$params["FooterTimetableID"];
		
		
		if($FooterTimetableID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_footer_timetable::getRecord($appFrw, array('FooterTimetableID'=>$FooterTimetableID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$FooterTimetableStatus = (isset($params['FooterTimetableStatus'])) ? $params['FooterTimetableStatus'] : $record['FooterTimetableStatus'];
		$FooterTimetableTitle	= (isset($params['FooterTimetableTitle'])) ? $params['FooterTimetableTitle'] : $record['FooterTimetableTitle'];
        $FooterTimetableSiteID 	= (isset($params['FooterTimetableSiteID'])) ? $params['FooterTimetableSiteID'] : $record['FooterTimetableSiteID'];
		
		
		$query = "	UPDATE sys_footer_timetable SET
							
							FooterTimetableStatus 	= ?
							,FooterTimetableTitle 	= ?
							,FooterTimetableSiteID 	= ?
						
							WHERE
							FooterTimetableID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("isii",
                                $FooterTimetableStatus
								,$FooterTimetableTitle
								,$FooterTimetableSiteID
								,$FooterTimetableID
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $FooterTimetableID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetable::DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$FooterTimetableID = (int)$params["FooterTimetableID"];
		
		if($FooterTimetableID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_footer_timetable WHERE FooterTimetableID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FooterTimetableID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $FooterTimetableID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_timetable::getList
	* --------------------------------------------------------------------------
	*/
	public static function getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : 0;

		$query = "	SELECT
						
							FooterTimetableID
							,FooterTimetableStatus
							,FooterTimetableTitle
							,FooterTimetableSiteID
						
							FROM sys_footer_timetable
							
							WHERE
							(
								FooterTimetableSiteID = ?
							)
							
							ORDER BY FooterTimetableID DESC			
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	

	
}
