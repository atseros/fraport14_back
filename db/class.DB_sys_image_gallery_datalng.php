<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_image_gallery_datalng
*
* DESCRIPTION: 
*	Class for table sys_image_gallery_datalng
*
* table fields:
*
 `ImgDataID` int(11) NOT NULL,
 `ImgDataImgID` int(11) NOT NULL,
 `ImgDataTitle` varchar(1024) NOT NULL,
 `ImgDataPriority` int(11) NOT NULL,
 `ImgDataStatus` int(11) NOT NULL,
 `ImgDataFilename` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_sys_image_gallery_datalng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_datalng::sys_image_gallery_datalng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_image_gallery_datalng_get_NewRecordDefValues($appFrw, $params)
	{	
		$ImgDataLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_image_gallery_datalng');
		
		if($ImgDataLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["ImgDataLngID"] = $ImgDataLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_image_gallery_datalng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_datalng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $ImgDataLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT ImgDataLngID FROM sys_image_gallery_datalng WHERE ImgDataLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $ImgDataLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_datalng::sys_image_gallery_datalng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_datalng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgDataLngID 	= (int)$params["ImgDataLngID"];
		
		if($ImgDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_image_gallery_datalng::check_RecordExists($appFrw, $ImgDataLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$ImgDataLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_image_gallery_datalng
					(
						 ImgDataLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $ImgDataLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_datalng::sys_image_gallery_datalng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_datalng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgDataLngID = (int)$params["ImgDataLngID"];
		$ImgDataLngType = $params["ImgDataLngType"];
		
		if($ImgDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_image_gallery_datalng::check_RecordExists($appFrw, $ImgDataLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$ImgDataLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						ImgDataLngID
						,ImgDataLngImgDataID 
						,ImgDataLngTitle 
						,ImgDataLngType 
						,ImgDataLngSubtitle 
						,ImgDataLngDescription 
							
					FROM sys_image_gallery_datalng
					WHERE
					(
						ImgDataLngID = ?
					)
					AND
					(
						ImgDataLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $ImgDataLngID, $ImgDataLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_datalng::sys_image_gallery_datalng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_datalng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgDataLngID = (int)$params["ImgDataLngID"];
		$ImgDataLngType 	= (isset($params['ImgDataLngType'])) ? $params['ImgDataLngType'] : $record['ImgDataLngType'];
		
		if($ImgDataLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_getRecord($appFrw, array('ImgDataLngID'=>$ImgDataLngID,'ImgDataLngType'=>$ImgDataLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$ImgDataLngTitle 	= (isset($params['ImgDataLngTitle'])) ? $params['ImgDataLngTitle'] : $record['ImgDataLngTitle'];
		$ImgDataLngImgDataID 	= (isset($params['ImgDataLngImgDataID'])) ? $params['ImgDataLngImgDataID'] : $record['ImgDataLngImgDataID'];
		$ImgDataLngSubtitle 	= (isset($params['ImgDataLngSubtitle'])) ? $params['ImgDataLngSubtitle'] : $record['ImgDataLngSubtitle'];
		$ImgDataLngDescription 	= (isset($params['ImgDataLngDescription'])) ? $params['ImgDataLngDescription'] : $record['ImgDataLngDescription'];
		
		$query = "	UPDATE sys_image_gallery_datalng SET
						 ImgDataLngTitle 					= ?
						,ImgDataLngType 					= ?
						,ImgDataLngImgDataID 		= ?
						,ImgDataLngSubtitle 		= ?
						,ImgDataLngDescription 		= ?
						
					WHERE
					ImgDataLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siissi",
								 $ImgDataLngTitle 			
								,$ImgDataLngType 
								,$ImgDataLngImgDataID 
								,$ImgDataLngSubtitle 
								,$ImgDataLngDescription 
								,$ImgDataLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $ImgDataLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_datalng::sys_image_gallery_datalng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_datalng_CheckBeforeInsert($appFrw, $params)
	{
		$ImgDataLngImgDataID	= isset($params["ImgDataLngImgDataID"]) ? (int)$params["ImgDataLngImgDataID"] : 0;
		$ImgDataLngType	= isset($params["ImgDataLngType"]) ? (int)$params["ImgDataLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT ImgDataLngID FROM sys_image_gallery_datalng WHERE ImgDataLngImgDataID = ? AND ImgDataLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $ImgDataLngImgDataID, $ImgDataLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallery_datalng::sys_image_gallery_datalng_GetImgDataLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallery_datalng_GetImgDataLngID($appFrw, $params)
	{
		$ImgDataLngImgDataID	= isset($params["ImgDataLngImgDataID"]) ? (int)$params["ImgDataLngImgDataID"] : 0;
		$ImgDataLngType	= isset($params["ImgDataLngType"]) ? (int)$params["ImgDataLngType"] : 0;
		
		$query = "	SELECT
						   ImgDataLngID
						   FROM sys_image_gallery_datalng
							WHERE ImgDataLngImgDataID = ? AND ImgDataLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $ImgDataLngImgDataID, $ImgDataLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["ImgDataLngID"];
	}
	
	
	
}
