<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_publications_gallerylng
*
* DESCRIPTION: 
*	Class for table sys_publications_gallerylng
*
* table fields:
*
 `PubLngID` int(11) NOT NULL,
 `PubLngPubID` int(11) NOT NULL,
 `PubLngTitle` varchar(1024) NOT NULL,
 `PubLngType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_publications_gallerylng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallerylng::sys_publications_gallerylng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_publications_gallerylng_get_NewRecordDefValues($appFrw, $params)
	{	
		$PubLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_publications_gallerylng');
		
		if($PubLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["PubLngID"] = $PubLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_publications_gallerylng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallerylng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $PubLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT PubLngID FROM sys_publications_gallerylng WHERE PubLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $PubLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallerylng::sys_publications_gallerylng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallerylng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$PubLngID 	= (int)$params["PubLngID"];
		
		if($PubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_publications_gallerylng::check_RecordExists($appFrw, $PubLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$PubLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_publications_gallerylng
					(
						 PubLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $PubLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_publications_gallerylng::sys_publications_gallerylng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallerylng::sys_publications_gallerylng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallerylng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$PubLngID = (int)$params["PubLngID"];
		$PubLngType = $params["PubLngType"];
		
		if($PubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_publications_gallerylng::check_RecordExists($appFrw, $PubLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$PubLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						PubLngID
						,PubLngPubID 
						,PubLngTitle 
						,PubLngType 
						,PubLngSubtitle 
						,PubLngDescription 
							
					FROM sys_publications_gallerylng
					WHERE
					(
						PubLngID = ?
					)
					AND
					(
						PubLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $PubLngID, $PubLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallerylng::sys_publications_gallerylng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallerylng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$PubLngID = (int)$params["PubLngID"];
		$PubLngType 	= (isset($params['PubLngType'])) ? $params['PubLngType'] : $record['PubLngType'];
		
		if($PubLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_publications_gallerylng::sys_publications_gallerylng_getRecord($appFrw, array('PubLngID'=>$PubLngID,'PubLngType'=>$PubLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$PubLngTitle 	= (isset($params['PubLngTitle'])) ? $params['PubLngTitle'] : $record['PubLngTitle'];
		$PubLngPubID 	= (isset($params['PubLngPubID'])) ? $params['PubLngPubID'] : $record['PubLngPubID'];
		$PubLngSubtitle 	= (isset($params['PubLngSubtitle'])) ? $params['PubLngSubtitle'] : $record['PubLngSubtitle'];
		$PubLngDescription 	= (isset($params['PubLngDescription'])) ? $params['PubLngDescription'] : $record['PubLngDescription'];
		
		$query = "	UPDATE sys_publications_gallerylng SET
						 PubLngTitle 					= ?
						,PubLngType 					= ?
						,PubLngPubID 		= ?
						,PubLngSubtitle 		= ?
						,PubLngDescription 		= ?
						
					WHERE
					PubLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siissi",
								 $PubLngTitle 			
								,$PubLngType 
								,$PubLngPubID 
								,$PubLngSubtitle 
								,$PubLngDescription 
								,$PubLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $PubLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallerylng::sys_publications_gallerylng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallerylng_CheckBeforeInsert($appFrw, $params)
	{
		$PubLngPubID	= isset($params["PubLngPubID"]) ? (int)$params["PubLngPubID"] : 0;
		$PubLngType	= isset($params["PubLngType"]) ? (int)$params["PubLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT PubLngID FROM sys_publications_gallerylng WHERE PubLngPubID = ? AND PubLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $PubLngPubID, $PubLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_publications_gallerylng::sys_publications_gallerylng_GetPubLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_publications_gallerylng_GetPubLngID($appFrw, $params)
	{
		$PubLngPubID	= isset($params["PubLngPubID"]) ? (int)$params["PubLngPubID"] : 0;
		$PubLngType	= isset($params["PubLngType"]) ? (int)$params["PubLngType"] : 0;
		
		$query = "	SELECT
						   PubLngID
						   FROM sys_publications_gallerylng
							WHERE PubLngPubID = ? AND PubLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $PubLngPubID, $PubLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["PubLngID"];
	}
	
	
	
}
