<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_footer_data
*
* DESCRIPTION: 
*	Class for table sys_footer_data
*
* table fields:
*
 `FtrDataID` int(11) NOT NULL,
 `FtrDataFtrID` int(11) NOT NULL,
 `FtrDataTitle` varchar(1024) NOT NULL,
 `FtrDataNodeID` int(11) NOT NULL,
 `FtrDataExternalLink` varchar(1024) NOT NULL,
 `FtrDataIcon` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_sys_footer_data
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_data::sys_footer_data_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_data_get_NewRecordDefValues($appFrw, $params)
	{
		$FtrID 	= (int)$params["FtrID"];
		
		$FtrDataID = DB_sys_kxn::get_NextID($appFrw, 'sys_footer_data');
		
		if($FtrDataID > 0)
		{
			$results["success"] = true;
			$results["data"]["FtrDataID"] = $FtrDataID;
			$results["data"]["FtrDataFtrID"] = $FtrID;
			$results["data"]["FtrDataStatus"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_footer_data";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_data::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $FtrDataID)
	{
		$query = "	SELECT
						   case when( exists (SELECT FtrDataID FROM sys_footer_data WHERE FtrDataID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FtrDataID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_data::sys_footer_data_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_data_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrDataID 	= (int)$params["FtrDataID"];
		
		if($FtrDataID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_footer_data::check_RecordExists($appFrw, $FtrDataID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$FtrDataID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_footer_data
					(
						 FtrDataID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FtrDataID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_footer_data::sys_footer_data_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_data::sys_footer_data_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_data_getRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrDataID = (int)$params["FtrDataID"];
		
		if($FtrDataID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_footer_data::check_RecordExists($appFrw, $FtrDataID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$FtrDataID;
			return $results;
		}
		
		$query = "	SELECT
						
						FtrDataID
						,FtrDataFtrID
						,FtrDataTitle						
						,FtrDataNodeID						
						,FtrDataIcon						
						,FtrDataStatus						
						,FtrDataPriority
						,NodeTitle						
							
					FROM sys_footer_data
					LEFT JOIN sys_node ON (NodeID = FtrDataNodeID)
					
					WHERE
					FtrDataID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $FtrDataID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_data::sys_footer_data_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_data_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrDataID = (int)$params["FtrDataID"];
		
		
		if($FtrDataID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_footer_data::sys_footer_data_getRecord($appFrw, array('FtrDataID'=>$FtrDataID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$FtrDataFtrID 		= (isset($params['FtrDataFtrID'])) ? $params['FtrDataFtrID'] : $record['FtrDataFtrID'];
		$FtrDataStatus 		= (isset($params['FtrDataStatus'])) ? $params['FtrDataStatus'] : $record['FtrDataStatus'];
		$FtrDataPriority 		= (isset($params['FtrDataPriority'])) ? $params['FtrDataPriority'] : $record['FtrDataPriority'];
		$FtrDataTitle 		= (isset($params['FtrDataTitle'])) ? $params['FtrDataTitle'] : $record['FtrDataTitle'];
		$FtrDataNodeID 		= (isset($params['FtrDataNodeID'])) ? $params['FtrDataNodeID'] : $record['FtrDataNodeID'];
		// $FtrDataExternalLink 		= (isset($params['FtrDataExternalLink'])) ? $params['FtrDataExternalLink'] : $record['FtrDataExternalLink'];
		$FtrDataIcon 		= (isset($params['FtrDataIcon'])) ? $params['FtrDataIcon'] : $record['FtrDataIcon'];
	
		$query = "	UPDATE sys_footer_data SET
						 FtrDataFtrID 			= ?
						,FtrDataStatus 			= ?
						,FtrDataPriority 			= ?
						,FtrDataTitle 			= ?
						,FtrDataNodeID 			= ?
						,FtrDataIcon 			= ?
						
					WHERE
					FtrDataID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiisisi",
								 $FtrDataFtrID 			
								,$FtrDataStatus 
								,$FtrDataPriority 
								,$FtrDataTitle 
								,$FtrDataNodeID 
								,$FtrDataIcon 
								,$FtrDataID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $FtrDataID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_data::sys_footer_data_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_data_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$FtrDataID = (int)$params["FtrDataID"];
		
		if($FtrDataID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_footer_data WHERE FtrDataID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $FtrDataID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $FtrDataID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_footer_data::sys_footer_data_get_List
	* --------------------------------------------------------------------------
	*/
	public static function sys_footer_data_get_List($appFrw, $params)
	{
		$FtrID	= isset($params["FtrID"]) ? (int)$params["FtrID"] : 0;
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		
		$results['data'] = array();
		
				
		$query = "	SELECT
						
							FtrDataID
							,FtrDataFtrID
							,FtrDataTitle
							,FtrDataNodeID
							,FtrDataIcon
							,FtrDataStatus
							,FtrDataPriority
							,NodeTitle
						
							FROM sys_footer_data
							LEFT JOIN sys_node ON (NodeID = FtrDataNodeID)
							
							WHERE 
							(
								FtrDataFtrID = ?
							)
							AND
							(
								FtrDataTitle LIKE ?
							)
							AND
							(
								? = 1
								OR
								FtrDataStatus = 1
							)
							
							ORDER BY FtrDataPriority ASC
							
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_footer_data_get_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("isi", $FtrID, $filterStrToFind, $filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "sys_footer_data_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "sys_footer_data_get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
}
