<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_categories_records
*
* DESCRIPTION: 
*	Class for table sys_categories_records
*
* table fields:
*
 `CtgRecID` int(11) NOT NULL,
 `CtgRecCtgID` int(11) NOT NULL,
 `CtgRecPriority` int(11) NOT NULL,
 `CtgRecTitle` varchar(1024) NOT NULL,
 `CtgRecStartDate` date NOT NULL,
 `CtgRecStartTime` time NOT NULL,
 `CtgRecEndDate` date NOT NULL,
 `CtgRecEndTime` time NOT NULL,
 `CtgRecTimer` int(11) NOT NULL,
 `CtgRecFilename` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_sys_categories_records
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records::sys_categories_records_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_get_NewRecordDefValues($appFrw, $params)
	{
		$CtgID 	= (int)$params["CtgID"];
		
		$CtgRecID = DB_sys_kxn::get_NextID($appFrw, 'sys_categories_records');
		
		if($CtgRecID > 0)
		{
			$results["success"] = true;
			$results["data"]["CtgRecID"] = $CtgRecID;
			$results["data"]["CtgRecCtgID"] = $CtgID;
			$results["data"]["CtgRecTimer"] = 2;
			$results["data"]["CtgRecStatus"] = 1;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_categories_records";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $CtgRecID)
	{
		$query = "	SELECT
						   case when( exists (SELECT CtgRecID FROM sys_categories_records WHERE CtgRecID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CtgRecID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records::sys_categories_records_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecID 	= (int)$params["CtgRecID"];
		
		if($CtgRecID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_categories_records::check_RecordExists($appFrw, $CtgRecID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$CtgRecID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_categories_records
					(
						 CtgRecID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CtgRecID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_categories_records::sys_categories_records_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records::sys_categories_records_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_getRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecID = (int)$params["CtgRecID"];
		
		if($CtgRecID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_categories_records::check_RecordExists($appFrw, $CtgRecID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$CtgRecID;
			return $results;
		}
		
		$query = "	SELECT
						
						CtgRecID
						,CtgRecCtgID
						,CtgRecPriority
						,CtgRecTitle
						,CtgRecStartDate
						,CtgRecStartTime
						,CtgRecEndDate
						,CtgRecEndTime
						,CtgRecTimer
						,CtgRecFilename
						,CtgRecStatus
						,CtgRecEscapeDefault
							
					FROM sys_categories_records
					WHERE
					CtgRecID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CtgRecID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		
		
		if($row['CtgRecStartTime'])
					$row['CtgRecStartTime'] = date("H:i", strtotime($row['CtgRecStartTime'] ));
		if($row['CtgRecEndTime'])
					$row['CtgRecEndTime'] = date("H:i", strtotime($row['CtgRecEndTime'] ));
		
		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records::sys_categories_records_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecID = (int)$params["CtgRecID"];
		
		
		if($CtgRecID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_categories_records::sys_categories_records_getRecord($appFrw, array('CtgRecID'=>$CtgRecID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$CtgRecCtgID = (isset($params['CtgRecCtgID'])) ? $params['CtgRecCtgID'] : $record['CtgRecCtgID'];
		$CtgRecPriority = (isset($params['CtgRecPriority'])) ? $params['CtgRecPriority'] : $record['CtgRecPriority'];
		$CtgRecTitle = (isset($params['CtgRecTitle'])) ? $params['CtgRecTitle'] : $record['CtgRecTitle'];
		$CtgRecStartDate 	= (isset($params['CtgRecStartDate'])) ? $params['CtgRecStartDate'] : $record['CtgRecStartDate'];
		$CtgRecStartTime 	= (isset($params['CtgRecStartTime'])) ? $params['CtgRecStartTime'] : $record['CtgRecStartTime'];
		$CtgRecEndDate 	= (isset($params['CtgRecEndDate'])) ? $params['CtgRecEndDate'] : $record['CtgRecEndDate'];
		$CtgRecEndTime = (isset($params['CtgRecEndTime'])) ? $params['CtgRecEndTime'] : $record['CtgRecEndTime'];
		$CtgRecTimer = (isset($params['CtgRecTimer'])) ? $params['CtgRecTimer'] : $record['CtgRecTimer'];
		$CtgRecFilename = (isset($params['CtgRecFilename'])) ? $params['CtgRecFilename'] : $record['CtgRecFilename'];
		$CtgRecStatus = (isset($params['CtgRecStatus'])) ? $params['CtgRecStatus'] : $record['CtgRecStatus'];
		$CtgRecEscapeDefault = (isset($params['CtgRecEscapeDefault'])) ? $params['CtgRecEscapeDefault'] : $record['CtgRecEscapeDefault'];
		
		if ($CtgRecStartDate == NULL)
			$CtgRecStartDate = NULL;
		else	
		$CtgRecStartDate = date("Y-m-d", strtotime($CtgRecStartDate));
	
		if ($CtgRecEndDate == NULL)
			$CtgRecEndDate = NULL;
		else	
		$CtgRecEndDate = date("Y-m-d", strtotime($CtgRecEndDate));
	
		
		if ($CtgRecStartTime == NULL)
			$CtgRecStartTime = NULL;
		else	
		$CtgRecStartTime = date("H:i", strtotime($CtgRecStartTime));
	
		if ($CtgRecEndTime == NULL)
			$CtgRecEndTime = NULL;
		else	
		$CtgRecEndTime = date("H:i", strtotime($CtgRecEndTime));
		
		$query = "	UPDATE sys_categories_records SET
							
							CtgRecCtgID 	= ?
							,CtgRecPriority 	= ?
							,CtgRecTitle 	= ?
							,CtgRecStartDate	 	= ?
							,CtgRecStartTime 	= ?
							,CtgRecEndDate 	= ?
							,CtgRecEndTime 	= ?
							,CtgRecTimer 	= ?
							,CtgRecFilename 	= ?
							,CtgRecStatus 	= ?
							,CtgRecEscapeDefault	= ?
						
							WHERE
							CtgRecID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iisssssisisi",
								 $CtgRecCtgID 			
								,$CtgRecPriority 			
								,$CtgRecTitle 			
								,$CtgRecStartDate 			
								,$CtgRecStartTime 			
								,$CtgRecEndDate 			
								,$CtgRecEndTime 			
								,$CtgRecTimer 			
								,$CtgRecFilename 		
								,$CtgRecStatus 	
								,$CtgRecEscapeDefault
								,$CtgRecID 	
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $CtgRecID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records::sys_categories_records_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecID = (int)$params["CtgRecID"];
		
		if($CtgRecID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_categories_records WHERE CtgRecID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CtgRecID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $CtgRecID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records::sys_categories_records_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_records_getList($appFrw, $params)
	{
		$results = array();
		
		$CtgID	= isset($params["CtgID"]) ? (int)$params["CtgID"] : 0;
		$SliderID	= isset($params["SliderID"]) ? (int)$params["SliderID"] : 0;
		$filterStrToFind	= isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$filterShowAll	= isset($params["filterShowAll"]) ? (int)$params["filterShowAll"] : 0;
		
		$query = "	SELECT
					
						CtgRecID
						,CtgRecCtgID
						,CtgRecPriority
						,CtgRecTitle
						,CtgRecStartDate
						,CtgRecStartTime
						,CtgRecEndDate
						,CtgRecEndTime
						,CtgRecTimer
						,CtgRecFilename
						,CtgRecStatus
						,CtgRecEscapeDefault
						,case when( exists (SELECT CtgRecSliderID FROM sys_categories_records_slider WHERE CtgRecSliderCtgRecID = CtgRecID AND CtgRecSliderSliderID = ?))
							then 1
							else 0
						 end as RowHighlight
					
					FROM sys_categories_records
					
					WHERE
					(
						CtgRecCtgID = ?
					)
					AND
					(
						CtgRecTitle LIKE ?
					)
					AND
					(
						? = 1
						OR
						CtgRecStatus = 1
					)

					ORDER BY CtgRecPriority ASC
					
					
		";

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("iisi", $SliderID, $CtgID, $filterStrToFind,$filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['CtgRecFullUrl']= DB_sys_categories_records::get_sys_categories_records_FullUrl($appFrw, $row['CtgRecID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_records::get_sys_categories_records_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_categories_records_FullUrl($appFrw, $CtgRecID)
	{
		$url = FileManager::getTblFolderUrl("sys_categories_records", $CtgRecID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_categories_records::sys_categories_records_getRecord($appFrw,array('CtgRecID'=>$CtgRecID));
		if( $imageData["success"] == true  && $imageData["data"]["CtgRecFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["CtgRecFilename"];
		}
		return $FulUrl;
	}
	
}
