<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");
require_once(realpath(__DIR__)."/class.DB_sys_central_airlineslng.php");

/*********************************************************************************************
* CLASS DB_sys_central_airlines
*
* DESCRIPTION: 
*	Class for table sys_central_airlines
*
* table fields:
*
 `C_AirlinesID` int(11) NOT NULL,
 `C_AirlinesName` varchar(1024) NOT NULL,
 `C_AirlinesWebsite` varchar(1024) NOT NULL,
 `C_AirlinesEmail` varchar(1024) NOT NULL,
*
*********************************************************************************************/
class DB_sys_central_airlines
{
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::sys_central_airlines_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlines_get_NewRecordDefValues($appFrw, $params)
	{
		$C_AirlinesID = DB_sys_kxn::get_NextID($appFrw, 'sys_central_airlines');
		
		if($C_AirlinesID > 0)
		{
			$results["success"] = true;
			$results["data"]["C_AirlinesID"] = $C_AirlinesID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_central_airlines";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $C_AirlinesID)
	{
		$query = "	SELECT
						   case when( exists (SELECT C_AirlinesID FROM sys_central_airlines WHERE C_AirlinesID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $C_AirlinesID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}

	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::sys_central_airlines_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlines_InsertRecord($appFrw, $params)
	{
		$results = array();

		$C_AirlinesID 	= (int)$params["C_AirlinesID"];
		
		if($C_AirlinesID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_central_airlines::check_RecordExists($appFrw, $C_AirlinesID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$C_AirlinesID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_central_airlines
					(
						 C_AirlinesID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $C_AirlinesID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_central_airlines::sys_central_airlines_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::sys_central_airlines_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlines_getRecord($appFrw, $params)
	{
		$results = array();
		
		$C_AirlinesID = (int)$params["C_AirlinesID"];
		
		if($C_AirlinesID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_central_airlines::check_RecordExists($appFrw, $C_AirlinesID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$C_AirlinesID;
			return $results;
		}
		
		$query = "	SELECT
						
						C_AirlinesID
						,C_AirlinesName 
						,C_AirlinesWebsite
						,C_AirlinesFileName 
						,C_AirlinesIATA 
						,C_AirlinesICAO 
						,C_AirlinesE_Check_In 
						,C_AirlinesE_Booking 
						,C_AirlinesFileNameLogo 
						,C_AirlinesEscapeDefault
						,C_AirlinesOnlineContact
						
					FROM sys_central_airlines
					WHERE
					C_AirlinesID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $C_AirlinesID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::sys_central_airlines_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlines_UpdateRecord($appFrw, $params)
	{
		$results = array();

		$C_AirlinesID = (int)$params["C_AirlinesID"];
		
		
		if($C_AirlinesID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_central_airlines::sys_central_airlines_getRecord($appFrw, array('C_AirlinesID'=>$C_AirlinesID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$C_AirlinesName 		= (isset($params['C_AirlinesName'])) ? $params['C_AirlinesName'] : $record['C_AirlinesName'];
		$C_AirlinesWebsite 	= (isset($params['C_AirlinesWebsite'])) ? $params['C_AirlinesWebsite'] : $record['C_AirlinesWebsite'];
		// $C_AirlinesEmail 		= (isset($params['C_AirlinesEmail'])) ? $params['C_AirlinesEmail'] : $record['C_AirlinesEmail'];
		$C_AirlinesIATA 		= (isset($params['C_AirlinesIATA'])) ? $params['C_AirlinesIATA'] : $record['C_AirlinesIATA'];
		$C_AirlinesICAO 		= (isset($params['C_AirlinesICAO'])) ? $params['C_AirlinesICAO'] : $record['C_AirlinesICAO'];
		$C_AirlinesFileName 	= (isset($params['C_AirlinesFileName'])) ? $params['C_AirlinesFileName'] : $record['C_AirlinesFileName'];
		$C_AirlinesE_Check_In 	= (isset($params['C_AirlinesE_Check_In'])) ? $params['C_AirlinesE_Check_In'] : $record['C_AirlinesE_Check_In'];
		$C_AirlinesE_Booking 	= (isset($params['C_AirlinesE_Booking'])) ? $params['C_AirlinesE_Booking'] : $record['C_AirlinesE_Booking'];
		$C_AirlinesFileNameLogo 	= (isset($params['C_AirlinesFileNameLogo'])) ? $params['C_AirlinesFileNameLogo'] : $record['C_AirlinesFileNameLogo'];
		$C_AirlinesEscapeDefault 	= (isset($params['C_AirlinesEscapeDefault'])) ? $params['C_AirlinesEscapeDefault'] : $record['C_AirlinesEscapeDefault'];
		// $C_AirlinesFlightsLogo 	= (isset($params['C_AirlinesFlightsLogo'])) ? $params['C_AirlinesFlightsLogo'] : $record['C_AirlinesFlightsLogo'];
		$C_AirlinesOnlineContact 	= (isset($params['C_AirlinesOnlineContact'])) ? $params['C_AirlinesOnlineContact'] : $record['C_AirlinesOnlineContact'];

		
		$query = "	UPDATE sys_central_airlines SET
						 
						 C_AirlinesName 					= ?
						 ,C_AirlinesWebsite 			= ?
						 ,C_AirlinesIATA 			= ?
						 ,C_AirlinesICAO 			= ?
						,C_AirlinesFileName 			= ?
						,C_AirlinesE_Check_In 			= ?
						,C_AirlinesE_Booking 			= ?
						,C_AirlinesFileNameLogo 			= ?
						,C_AirlinesEscapeDefault		= ?
						,C_AirlinesOnlineContact		= ?
						WHERE
						C_AirlinesID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ssssssssssi",
								 $C_AirlinesName 			
								,$C_AirlinesWebsite
								,$C_AirlinesIATA
								,$C_AirlinesICAO
								,$C_AirlinesFileName
								,$C_AirlinesE_Check_In
								,$C_AirlinesE_Booking
								,$C_AirlinesFileNameLogo
								,$C_AirlinesEscapeDefault
								,$C_AirlinesOnlineContact
								,$C_AirlinesID
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $C_AirlinesID;
		return $results;
	}

	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::sys_central_airlines_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlines_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$C_AirlinesID = (int)$params["C_AirlinesID"];
		
		if($C_AirlinesID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		$query = "DELETE FROM sys_central_airlines WHERE C_AirlinesID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $C_AirlinesID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();	

		$query = "DELETE FROM sys_site_airline WHERE SiteAirlineAirlineID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $C_AirlinesID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $C_AirlinesID;
		return $results;		
	}

	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::sys_central_airlines_get_List
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_airlines_get_List($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$filterStrToFind = isset($params["filterStrToFind"]) ? '%'.$params["filterStrToFind"].'%' : '';
		$SiteID	= isset($params["SiteID"]) ? (int)$params["SiteID"] : -1;	

		$query = "	
            SELECT		
                C_AirlinesID
                ,C_AirlinesName
                ,C_AirlinesWebsite
                ,C_AirlinesFileName
                ,C_AirlinesIATA
                ,C_AirlinesICAO
                ,C_AirlinesE_Check_In
                ,C_AirlinesE_Booking
                ,C_AirlinesFileNameLogo
                ,C_AirlinesEscapeDefault
                ,C_AirlinesOnlineContact            
            FROM sys_central_airlines
            WHERE
            (
                C_AirlinesName LIKE ?
            )
            AND
            (
                C_AirlinesID NOT IN (SELECT SiteAirlineAirlineID FROM sys_site_airline WHERE SiteAirlineAirlineID = C_AirlinesID AND SiteAirlineSiteID = $SiteID )
            )
                                
            ORDER BY C_AirlinesName ASC
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) {
			$results["success"] = false;
			$results["reason"] = "get_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("s", $filterStrToFind);
		
		if(!$stmt->execute()) {
			$results["success"] = false;
			$results["reason"] = "get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result) {
			$results["success"] = false;
			$results["reason"] = "get_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['C_AirlinesFullUrl']= DB_sys_central_airlines::get_sys_central_airlines_FullUrl($appFrw, $row['C_AirlinesID']);
			$row['C_AirlinesFullUrlLogo']= DB_sys_central_airlines::get_sys_central_airlines_FullUrlLogo($appFrw, $row['C_AirlinesID']);
			// $row['C_AirlinesFlightsLogo']= DB_sys_central_airlines::get_sys_central_airlines_FlightsLogo($appFrw, $row['C_AirlinesID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::get_sys_central_airlines_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_central_airlines_FullUrl($appFrw, $C_AirlinesID)
	{
		$url = FileManager::getTblFolderUrl("sys_central_airlines", $C_AirlinesID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_central_airlines::sys_central_airlines_getRecord($appFrw,array('C_AirlinesID'=>$C_AirlinesID));
		if( $imageData["success"] == true  && $imageData["data"]["C_AirlinesFileName"] )
		{
			$FulUrl = $url.$imageData["data"]["C_AirlinesFileName"];
		}
		return $FulUrl;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::get_sys_central_airlines_FullUrlLogo
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_central_airlines_FullUrlLogo($appFrw, $C_AirlinesID)
	{
		$url = FileManager::getTblFolderUrl("sys_central_airlines", $C_AirlinesID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_central_airlines::sys_central_airlines_getRecord($appFrw,array('C_AirlinesID'=>$C_AirlinesID));
		if( $imageData["success"] == true  && $imageData["data"]["C_AirlinesFileNameLogo"] )
		{
			$FulUrl = $url.$imageData["data"]["C_AirlinesFileNameLogo"];
		}
		return $FulUrl;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::get_sys_central_airlines_FlightsLogo
	* --------------------------------------------------------------------------
	*/
	// public static function get_sys_central_airlines_FlightsLogo($appFrw, $C_AirlinesID)
	// {
		// $url = FileManager::getTblFolderUrl("sys_central_airlines", $C_AirlinesID )."/";
		// $FulUrl = "";
		
		// $imageData = DB_sys_central_airlines::sys_central_airlines_getRecord($appFrw,array('C_AirlinesID'=>$C_AirlinesID));
		// if( $imageData["success"] == true  && $imageData["data"]["C_AirlinesFlightsLogo"] )
		// {
			// $FulUrl = $url.$imageData["data"]["C_AirlinesFlightsLogo"];
		// }
		// return $FulUrl;
	// }

    /*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::sys_central_airlines_get_ExportList
	* --------------------------------------------------------------------------
	*/
    public static function sys_central_airlines_get_ExportList($appFrw, $params)
    {
        $results = array();

        $query = "	SELECT
						
                        C_AirlinesID
                        ,C_AirlinesName
                        ,C_AirlinesWebsite
                        ,C_AirlinesIATA
                        ,C_AirlinesICAO
                        ,C_AirlinesE_Check_In
                        ,C_AirlinesE_Booking
                        ,C_AirlinesEscapeDefault
                        ,C_AirlinesOnlineContact
                        
					    ,(
					        SELECT sys_central_airlineslng_name_en.C_AirlinesLngName
					        FROM sys_central_airlineslng AS sys_central_airlineslng_name_en
					        WHERE
					        sys_central_airlineslng_name_en.C_AirlinesLngType = 1
					        AND 
					        sys_central_airlineslng_name_en.C_AirlinesLngC_AirlinesID = C_AirlinesID
					    ) AS C_AirlinesLngName_EN
					    ,(
					        SELECT sys_central_airlineslng_phone_en.C_AirlinesLngPhones
					        FROM sys_central_airlineslng AS sys_central_airlineslng_phone_en
					        WHERE
					        sys_central_airlineslng_phone_en.C_AirlinesLngType = 1
					        AND 
					        sys_central_airlineslng_phone_en.C_AirlinesLngC_AirlinesID = C_AirlinesID
					    ) AS C_AirlinesLngPhones_EN
					    ,(
					        SELECT sys_central_airlineslng_subtitle_en.C_AirlinesLngDictionaryDescription
					        FROM sys_central_airlineslng AS sys_central_airlineslng_subtitle_en
					        WHERE
					        sys_central_airlineslng_subtitle_en.C_AirlinesLngType = 1
					        AND 
					        sys_central_airlineslng_subtitle_en.C_AirlinesLngC_AirlinesID = C_AirlinesID
					    ) AS C_AirlinesLngDictionaryDescription_EN
					    
					    
					    ,(
					        SELECT sys_central_airlineslng_name_el.C_AirlinesLngName
					        FROM sys_central_airlineslng AS sys_central_airlineslng_name_el
					        WHERE
					        sys_central_airlineslng_name_el.C_AirlinesLngType = 2
					        AND 
					        sys_central_airlineslng_name_el.C_AirlinesLngC_AirlinesID = C_AirlinesID
					    ) AS C_AirlinesLngName_EL
					    ,(
					        SELECT sys_central_airlineslng_phone_el.C_AirlinesLngPhones
					        FROM sys_central_airlineslng AS sys_central_airlineslng_phone_el
					        WHERE
					        sys_central_airlineslng_phone_el.C_AirlinesLngType = 2
					        AND 
					        sys_central_airlineslng_phone_el.C_AirlinesLngC_AirlinesID = C_AirlinesID
					    ) AS C_AirlinesLngPhones_EL
					    ,(
					        SELECT sys_central_airlineslng_subtitle_el.C_AirlinesLngDictionaryDescription
					        FROM sys_central_airlineslng AS sys_central_airlineslng_subtitle_el
					        WHERE
					        sys_central_airlineslng_subtitle_el.C_AirlinesLngType = 2
					        AND 
					        sys_central_airlineslng_subtitle_el.C_AirlinesLngC_AirlinesID = C_AirlinesID
					    ) AS C_AirlinesLngDictionaryDescription_EL
					
					FROM sys_central_airlines
									
					ORDER BY C_AirlinesName ASC
							
					
		";

        //exit($query);
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
        {
            $results["success"] = false;
            $results["reason"] = "get_List: error at prepare statement: ".$appFrw->DB_Link->error;
            return $results;
        }

        if(!$stmt->execute())
        {
            $results["success"] = false;
            $results["reason"] = "get_List: error at select: ".$stmt->error;
            return $results;
        }

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
        {
            $results["success"] = false;
            $results["reason"] = "get_List: error at select: ".$stmt->error;
            return $results;
        }

        $results['data'] = array();

        while( $row = $result->fetch_assoc())
        {
            array_push($results['data'], $row);
        }
        $result->close();


        // return results
        $results["success"] = true;

        return $results;
    }

    /*
	* --------------------------------------------------------------------------
	* DB_sys_central_airlines::sys_central_airlines_update_airlines
	* --------------------------------------------------------------------------
	*/
    public static function sys_central_airlines_update_airlines($appFrw, $params)
    {
        require_once(realpath(__DIR__."/../libphp")."/PHPExcel_1.8.0_doc/PHPExcel/IOFactory.php");
        require_once(realpath(__DIR__."/../libphp")."/PHPExcel_1.8.0_doc/PHPExcel/Calculation/DateTime.php");

        $filename   = $params["ExcelAirlines"]["ExcelAirlines"]["tmp_name"];
        $fileType   = 'Excel5';


        $objReader = PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel = $objReader->load($filename);

        $objPHPExcel->setActiveSheetIndex(0);
        $sheet1 = $objPHPExcel->getActiveSheet();

        $count = 0;
        foreach ($objPHPExcel->setActiveSheetIndex(0)->getRowIterator() as $row)
        {
            $count++;
        }

        for($i = 2;$i<=$count; $i++)
        {
            $params = array();

            $C_AirlinesID = 0;

            $C_AirlinesID = $sheet1->getCell('A'.$i)->getValue();
            $C_AirlinesID = addslashes ($C_AirlinesID);
            $C_AirlinesID = str_replace("\n", "", $C_AirlinesID);
            $C_AirlinesID = str_replace("\r", "", $C_AirlinesID);

            if($C_AirlinesID > 0){
                if( DB_sys_central_airlines::check_RecordExists($appFrw, $C_AirlinesID) ){
                    $params['C_AirlinesID'] = $C_AirlinesID;

                    $params['C_AirlinesName'] = $sheet1->getCell('B'.$i)->getValue();
                    $params['C_AirlinesName'] = addslashes ($params['C_AirlinesName']);
                    $params['C_AirlinesName'] = str_replace("\n", "", $params['C_AirlinesName']);
                    $params['C_AirlinesName'] = str_replace("\r", "", $params['C_AirlinesName']);

                    $params['C_AirlinesWebsite'] = $sheet1->getCell('C'.$i)->getValue();
                    $params['C_AirlinesWebsite'] = addslashes ($params['C_AirlinesWebsite']);
                    $params['C_AirlinesWebsite'] = str_replace("\n", "", $params['C_AirlinesWebsite']);
                    $params['C_AirlinesWebsite'] = str_replace("\r", "", $params['C_AirlinesWebsite']);

//            $params['C_AirlinesIATA'] = $sheet1->getCell('D'.$i)->getValue();
//            $params['C_AirlinesIATA'] = addslashes ($params['C_AirlinesIATA']);
//            $params['C_AirlinesIATA'] = str_replace("\n", "", $params['C_AirlinesIATA']);
//            $params['C_AirlinesIATA'] = str_replace("\r", "", $params['C_AirlinesIATA']);

//            $params['C_AirlinesICAO'] = $sheet1->getCell('E'.$i)->getValue();
//            $params['C_AirlinesICAO'] = addslashes ($params['C_AirlinesICAO']);
//            $params['C_AirlinesICAO'] = str_replace("\n", "", $params['C_AirlinesICAO']);
//            $params['C_AirlinesICAO'] = str_replace("\r", "", $params['C_AirlinesICAO']);

                    $params['C_AirlinesOnlineContact'] = $sheet1->getCell('F'.$i)->getValue();
                    $params['C_AirlinesOnlineContact'] = addslashes ($params['C_AirlinesOnlineContact']);
                    $params['C_AirlinesOnlineContact'] = str_replace("\n", "", $params['C_AirlinesOnlineContact']);
                    $params['C_AirlinesOnlineContact'] = str_replace("\r", "", $params['C_AirlinesOnlineContact']);

                    $params['C_AirlinesE_Check_In'] = $sheet1->getCell('G'.$i)->getValue();
                    $params['C_AirlinesE_Check_In'] = addslashes ($params['C_AirlinesE_Check_In']);
                    $params['C_AirlinesE_Check_In'] = str_replace("\n", "", $params['C_AirlinesE_Check_In']);
                    $params['C_AirlinesE_Check_In'] = str_replace("\r", "", $params['C_AirlinesE_Check_In']);

                    $params['C_AirlinesE_Booking'] = $sheet1->getCell('H'.$i)->getValue();
                    $params['C_AirlinesE_Booking'] = addslashes ($params['C_AirlinesE_Booking']);
                    $params['C_AirlinesE_Booking'] = str_replace("\n", "", $params['C_AirlinesE_Booking']);
                    $params['C_AirlinesE_Booking'] = str_replace("\r", "", $params['C_AirlinesE_Booking']);

                    $params['C_AirlinesEscapeDefault'] = $sheet1->getCell('I'.$i)->getValue();
                    $params['C_AirlinesEscapeDefault'] = addslashes ($params['C_AirlinesEscapeDefault']);
                    $params['C_AirlinesEscapeDefault'] = str_replace("\n", "", $params['C_AirlinesEscapeDefault']);
                    $params['C_AirlinesEscapeDefault'] = str_replace("\r", "", $params['C_AirlinesEscapeDefault']);

                    $results = DB_sys_central_airlines::sys_central_airlines_UpdateRecord($appFrw, $params);

                    $params = array();

                    $params['C_AirlinesLngC_AirlinesID'] = $sheet1->getCell('A'.$i)->getValue();
                    $params['C_AirlinesLngC_AirlinesID'] = addslashes ($params['C_AirlinesLngC_AirlinesID']);
                    $params['C_AirlinesLngC_AirlinesID'] = str_replace("\n", "", $params['C_AirlinesLngC_AirlinesID']);
                    $params['C_AirlinesLngC_AirlinesID'] = str_replace("\r", "", $params['C_AirlinesLngC_AirlinesID']);

                    $params['C_AirlinesLngName_EN'] = $sheet1->getCell('J'.$i)->getValue();
                    $params['C_AirlinesLngName_EN'] = addslashes ($params['C_AirlinesLngName_EN']);
                    $params['C_AirlinesLngName_EN'] = str_replace("\n", "", $params['C_AirlinesLngName_EN']);
                    $params['C_AirlinesLngName_EN'] = str_replace("\r", "", $params['C_AirlinesLngName_EN']);

                    $params['C_AirlinesLngPhones_EN'] = $sheet1->getCell('K'.$i)->getValue();
                    $params['C_AirlinesLngPhones_EN'] = addslashes ($params['C_AirlinesLngPhones_EN']);
                    $params['C_AirlinesLngPhones_EN'] = str_replace("\n", "", $params['C_AirlinesLngPhones_EN']);
                    $params['C_AirlinesLngPhones_EN'] = str_replace("\r", "", $params['C_AirlinesLngPhones_EN']);

                    $params['C_AirlinesLngDictionaryDescription_EN'] = $sheet1->getCell('L'.$i)->getValue();
                    $params['C_AirlinesLngDictionaryDescription_EN'] = addslashes ($params['C_AirlinesLngDictionaryDescription_EN']);
                    $params['C_AirlinesLngDictionaryDescription_EN'] = str_replace("\n", "", $params['C_AirlinesLngDictionaryDescription_EN']);
                    $params['C_AirlinesLngDictionaryDescription_EN'] = str_replace("\r", "", $params['C_AirlinesLngDictionaryDescription_EN']);

                    $params['C_AirlinesLngName_EL'] = $sheet1->getCell('M'.$i)->getValue();
                    $params['C_AirlinesLngName_EL'] = addslashes ($params['C_AirlinesLngName_EL']);
                    $params['C_AirlinesLngName_EL'] = str_replace("\n", "", $params['C_AirlinesLngName_EL']);
                    $params['C_AirlinesLngName_EL'] = str_replace("\r", "", $params['C_AirlinesLngName_EL']);

                    $params['C_AirlinesLngPhones_EL'] = $sheet1->getCell('N'.$i)->getValue();
                    $params['C_AirlinesLngPhones_EL'] = addslashes ($params['C_AirlinesLngPhones_EL']);
                    $params['C_AirlinesLngPhones_EL'] = str_replace("\n", "", $params['C_AirlinesLngPhones_EL']);
                    $params['C_AirlinesLngPhones_EL'] = str_replace("\r", "", $params['C_AirlinesLngPhones_EL']);

                    $params['C_AirlinesLngDictionaryDescription_EL'] = $sheet1->getCell('O'.$i)->getValue();
                    $params['C_AirlinesLngDictionaryDescription_EL'] = addslashes ($params['C_AirlinesLngDictionaryDescription_EL']);
                    $params['C_AirlinesLngDictionaryDescription_EL'] = str_replace("\n", "", $params['C_AirlinesLngDictionaryDescription_EL']);
                    $params['C_AirlinesLngDictionaryDescription_EL'] = str_replace("\r", "", $params['C_AirlinesLngDictionaryDescription_EL']);


                    $results = DB_sys_central_airlineslng::sys_central_airlineslng_UpdateAirlineLngRecord($appFrw, $params);
                }
            }
        }

        $results = array();

        $results["success"] = true;
        $results["data"]    = $sheet1->getCell('A2')->getValue();

        return $results;

    }
	
}
