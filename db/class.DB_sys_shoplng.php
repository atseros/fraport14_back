<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS class.DB_sys_shoplng
*
* DESCRIPTION: 
*	Class for table sys_shoplng
*
* table fields:
*
 `ShopLngID` int(11) NOT NULL,
 `ShopLngShopID` int(11) NOT NULL,
 `ShopLngType` int(11) NOT NULL,
 `ShopLngTitle` varchar(1024) NOT NULL,
 `ShopLngSubtitle` varchar(1024) NOT NULL,
 `ShopLngText` text NOT NULL,
*
*********************************************************************************************/
class DB_sys_shoplng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::sys_shoplng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_shoplng_get_NewRecordDefValues($appFrw, $params)
	{	
		$ShopLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_shoplng');
		
		if($ShopLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["ShopLngID"] = $ShopLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_shoplng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shoplng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $ShopLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT ShopLngID FROM sys_shoplng WHERE ShopLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $ShopLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shoplng::sys_shoplng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_shoplng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopLngID 	= (int)$params["ShopLngID"];
		
		if($ShopLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_shoplng::check_RecordExists($appFrw, $ShopLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$ShopLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_shoplng
					(
						 ShopLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $ShopLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_shoplng::sys_shoplng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_shoplng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_shoplng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopLngID = (int)$params["ShopLngID"];
		$ShopLngType = $params["ShopLngType"];
		
		if($ShopLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_shoplng::check_RecordExists($appFrw, $ShopLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$ShopLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						ShopLngID
						,ShopLngShopID 
						,ShopLngType 
						,ShopLngTitle 
						,ShopLngSubtitle 
						,ShopLngText
						,ShopLngImgFilenameLogo
							
					FROM sys_shoplng
					WHERE
					(
						ShopLngID = ?
					)
					AND
					(
						ShopLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $ShopLngID, $ShopLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		
		if( $row["ShopLngImgFilenameLogo"] != '')
		{
			$url = FileManager::getTblFolderUrl("sys_shoplng", $row["ShopLngID"] )."/";
			
			$row["ShopLngImgFilenameLogoPath"] = $url.'logo-thumb-'.$row["ShopLngImgFilenameLogo"];
		}
		else
		{
			$row["ShopLngImgFilenameLogoPath"] = "";
		}

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_shoplng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_shoplng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopLngID = (int)$params["ShopLngID"];
		$ShopLngType 			= (isset($params['ShopLngType'])) ? $params['ShopLngType'] : $record['ShopLngType'];
		
		if($ShopLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_shoplng::sys_shoplng_getRecord($appFrw, array('ShopLngID'=>$ShopLngID,'ShopLngType'=>$ShopLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$ShopLngShopID 		= (isset($params['ShopLngShopID'])) ? $params['ShopLngShopID'] : $record['ShopLngShopID'];
		$ShopLngTitle 			= (isset($params['ShopLngTitle'])) ? $params['ShopLngTitle'] : (isset($record['ShopLngTitle']) ? $record['ShopLngTitle'] : '');
		$ShopLngSubtitle 			= (isset($params['ShopLngSubtitle'])) ? $params['ShopLngSubtitle'] : (isset($record['ShopLngSubtitle']) ? $record['ShopLngSubtitle'] : '');
		$ShopLngText 			= (isset($params['ShopLngText'])) ? $params['ShopLngText'] : (isset($record['ShopLngText']) ? $record['ShopLngText'] : '');
		
		$query = "	UPDATE sys_shoplng SET
						 ShopLngShopID 					= ?
						,ShopLngType 					= ?
						,ShopLngTitle 					= ?
						,ShopLngSubtitle 			= ?
						,ShopLngText 				= ?
						
						
					WHERE
					ShopLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iisssi",
								 $ShopLngShopID 			
								,$ShopLngType 
								,$ShopLngTitle 
								,$ShopLngSubtitle 
								,$ShopLngText 
								,$ShopLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();	
		
		// return
		$results["success"] = true;
		$results["data"] = $ShopLngID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::RecordCanBeDeleted
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be checked
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_site WHERE SiteID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SiteID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node::sys_node_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeLngType	= isset($params["filterLanguage"]) ? (int)$params["filterLanguage"] : 1;
				
		$query = "	SELECT
						
							NodeID
							,NodeSiteID
							,NodeNodeID
							,NodeLevel
							,NodeCode
							,NodeParentCode
							,NodeFullCode
							,NodeTitle
							,NodeUrlAlias
							,NodePublished
							,NodeIsHomePage
							,NodeTemplateFile
							,NodeCreatedAt
							,NodeLngTitle
							,NodeLngStory
							,NodeLngIntro
							,CONCAT (NodeFullCode, ' - ', NodeLngTitle) AS NodeLngTitleFullCode
									
							FROM sys_node
							LEFT JOIN sys_nodelng ON ( NodeLngNodeID = NodeID )
							
							WHERE
							(
								NodeLngType = ?
							)
							
							
							ORDER BY NodeFullCode ASC
							
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("i", $NodeLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shoplng::sys_shoplng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_shoplng_CheckBeforeInsert($appFrw, $params)
	{
		$ShopLngShopID	= isset($params["ShopLngShopID"]) ? (int)$params["ShopLngShopID"] : 0;
		$ShopLngType	= isset($params["ShopLngType"]) ? (int)$params["ShopLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT ShopLngID FROM sys_shoplng WHERE ShopLngShopID = ? AND ShopLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $ShopLngShopID, $ShopLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shoplng::sys_shoplng_GetShopLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_shoplng_GetShopLngID($appFrw, $params)
	{
		$ShopLngShopID	= isset($params["ShopLngShopID"]) ? (int)$params["ShopLngShopID"] : 0;
		$ShopLngType	= isset($params["ShopLngType"]) ? (int)$params["ShopLngType"] : 0;
		
		$query = "	SELECT
						   ShopLngID
						   FROM sys_shoplng
							WHERE ShopLngShopID = ? AND ShopLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $ShopLngShopID, $ShopLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["ShopLngID"];
	}
	
	
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_shoplng_getLogoRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_shoplng_getLogoRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopLngID = (int)$params["ShopLngID"];
		
		if($ShopLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_shoplng::check_RecordExists($appFrw, $ShopLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$ShopLngID;
			return $results;
		}
		
		$query = "	SELECT
								 ShopLngID
								,ShopLngType 
								,ShopLngImgFilenameLogo
							FROM sys_shoplng
							WHERE
							(
								ShopLngID = ?
							)					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("sys_shoplng_getLogoRecord: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $ShopLngID);
		
		if(!$stmt->execute()) 
			exit("sys_shoplng_getLogoRecord: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("sys_shoplng_getLogoRecord: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_nodelng::sys_shoplng_UpdateLogoRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_shoplng_UpdateLogoRecord($appFrw, $params)
	{
		$results = array();
		
		$ShopLngID 		= (int)$params["ShopLngID"];
		$ShopLngType 	= (isset($params['ShopLngType'])) ? $params['ShopLngType'] : 1;
		
		if($ShopLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "sys_shoplng_UpdateLogoRecord: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_shoplng::sys_shoplng_getRecord($appFrw, array('ShopLngID'=>$ShopLngID,'ShopLngType'=>$ShopLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$ShopLngImgFilenameLogo	= (isset($params['ShopLngImgFilenameLogo'])) ? $params['ShopLngImgFilenameLogo'] : $record['ShopLngImgFilenameLogo'];
		
		$query = "	UPDATE sys_shoplng SET
								ShopLngImgFilenameLogo	= ?
							WHERE
								ShopLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("sys_shoplng_UpdateLogoRecord: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("si",
								 $ShopLngImgFilenameLogo
								,$ShopLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("sys_shoplng_UpdateLogoRecord: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $ShopLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shoplng::get_sys_shoplng_FullUrlLogo
	* --------------------------------------------------------------------------
	*/
	/* public static function get_sys_shoplng_FullUrlLogo($appFrw, $ShopLngID)
	{
		$url = FileManager::getTblFolderUrl("sys_shoplng", $ShopLngID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_shoplng::sys_shoplng_getLogoRecord($appFrw,array('ShopLngID'=>$ShopLngID));
		if( $imageData["success"] == true  && $imageData["data"]["ShopLngImgFilenameLogo"] )
		{
			$FulUrl = $url.$imageData["data"]["ShopLngImgFilenameLogo"];
		}
		return $FulUrl;
	} */
	
}
