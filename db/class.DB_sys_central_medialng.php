<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_central_medialng
*
* DESCRIPTION: 
*	Class for table sys_central_medialng
*
* table fields:
*
 `CentralMediaLngID` int(11) NOT NULL,
 `CentralMediaLngTitle` varchar(1024) NOT NULL,
 `CentralMediaLngType` int(11) NOT NULL,
 `CentralMediaLngCentralMediaID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_central_medialng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_medialng::sys_central_medialng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_central_medialng_get_NewRecordDefValues($appFrw, $params)
	{	
		$CentralMediaLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_central_medialng');
		
		if($CentralMediaLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["CentralMediaLngID"] = $CentralMediaLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_central_medialng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_medialng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $CentralMediaLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT CentralMediaLngID FROM sys_central_medialng WHERE CentralMediaLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CentralMediaLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_medialng::sys_central_medialng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_medialng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$CentralMediaLngID 	= (int)$params["CentralMediaLngID"];
		
		if($CentralMediaLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_central_medialng::check_RecordExists($appFrw, $CentralMediaLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$CentralMediaLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_central_medialng
					(
						 CentralMediaLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CentralMediaLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_central_medialng::sys_central_medialng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_medialng::sys_central_medialng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_medialng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$CentralMediaLngID = (int)$params["CentralMediaLngID"];
		$CentralMediaLngType = $params["CentralMediaLngType"];
		
		if($CentralMediaLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_central_medialng::check_RecordExists($appFrw, $CentralMediaLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$CentralMediaLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						CentralMediaLngID
						,CentralMediaLngTitle 
						,CentralMediaLngType 
						,CentralMediaLngCentralMediaID 
							
					FROM sys_central_medialng
					WHERE
					(
						CentralMediaLngID = ?
					)
					AND
					(
						CentralMediaLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $CentralMediaLngID, $CentralMediaLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_medialng::sys_central_medialng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_medialng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$CentralMediaLngID = (int)$params["CentralMediaLngID"];
		$CentralMediaLngType 	= (isset($params['CentralMediaLngType'])) ? $params['CentralMediaLngType'] : $record['CentralMediaLngType'];
		
		if($CentralMediaLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_central_medialng::sys_central_medialng_getRecord($appFrw, array('CentralMediaLngID'=>$CentralMediaLngID,'CentralMediaLngType'=>$CentralMediaLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$CentralMediaLngCentralMediaID 		= (isset($params['CentralMediaLngCentralMediaID'])) ? $params['CentralMediaLngCentralMediaID'] : $record['CentralMediaLngCentralMediaID'];
		$CentralMediaLngTitle 			= (isset($params['CentralMediaLngTitle'])) ? $params['CentralMediaLngTitle'] : $record['CentralMediaLngTitle'];
	
		
		$query = "	UPDATE sys_central_medialng SET
						 CentralMediaLngCentralMediaID 					= ?
						,CentralMediaLngType 					= ?
						,CentralMediaLngTitle 					= ?
						
					WHERE
					CentralMediaLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iisi",
								 $CentralMediaLngCentralMediaID 			
								,$CentralMediaLngType 
								,$CentralMediaLngTitle 
								,$CentralMediaLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $CentralMediaLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_medialng::sys_central_medialng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_medialng_CheckBeforeInsert($appFrw, $params)
	{
		$CentralMediaLngCentralMediaID	= isset($params["CentralMediaLngCentralMediaID"]) ? (int)$params["CentralMediaLngCentralMediaID"] : 0;
		$CentralMediaLngType	= isset($params["CentralMediaLngType"]) ? (int)$params["CentralMediaLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT CentralMediaLngID FROM sys_central_medialng WHERE CentralMediaLngCentralMediaID = ? AND CentralMediaLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $CentralMediaLngCentralMediaID, $CentralMediaLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_central_medialng::sys_central_medialng_GetCentralMediaLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_central_medialng_GetCentralMediaLngID($appFrw, $params)
	{
		$CentralMediaLngCentralMediaID	= isset($params["CentralMediaLngCentralMediaID"]) ? (int)$params["CentralMediaLngCentralMediaID"] : 0;
		$CentralMediaLngType	= isset($params["CentralMediaLngType"]) ? (int)$params["CentralMediaLngType"] : 0;
		
		$query = "	SELECT
						   CentralMediaLngID
						   FROM sys_central_medialng
							WHERE CentralMediaLngCentralMediaID = ? AND CentralMediaLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $CentralMediaLngCentralMediaID, $CentralMediaLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["CentralMediaLngID"];
	}
	
	
	
}
