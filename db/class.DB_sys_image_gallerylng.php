<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_image_gallerylng
*
* DESCRIPTION: 
*	Class for table sys_image_gallerylng
*
* table fields:
*
 `ImgLngID` int(11) NOT NULL,
 `ImgLngImgID` int(11) NOT NULL,
 `ImgLngTitle` varchar(1024) NOT NULL,
 `ImgLngType` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_image_gallerylng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallerylng::sys_image_gallerylng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_image_gallerylng_get_NewRecordDefValues($appFrw, $params)
	{	
		$ImgLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_image_gallerylng');
		
		if($ImgLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["ImgLngID"] = $ImgLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_image_gallerylng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallerylng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $ImgLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT ImgLngID FROM sys_image_gallerylng WHERE ImgLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $ImgLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallerylng::sys_image_gallerylng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallerylng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgLngID 	= (int)$params["ImgLngID"];
		
		if($ImgLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_image_gallerylng::check_RecordExists($appFrw, $ImgLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$ImgLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_image_gallerylng
					(
						 ImgLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $ImgLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_image_gallerylng::sys_image_gallerylng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallerylng::sys_image_gallerylng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallerylng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgLngID = (int)$params["ImgLngID"];
		$ImgLngType = $params["ImgLngType"];
		
		if($ImgLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_image_gallerylng::check_RecordExists($appFrw, $ImgLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$ImgLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						ImgLngID
						,ImgLngImgID 
						,ImgLngTitle 
						,ImgLngType 
						,ImgLngSubtitle 
						,ImgLngDescription 
							
					FROM sys_image_gallerylng
					WHERE
					(
						ImgLngID = ?
					)
					AND
					(
						ImgLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $ImgLngID, $ImgLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallerylng::sys_image_gallerylng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallerylng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$ImgLngID = (int)$params["ImgLngID"];
		$ImgLngType 	= (isset($params['ImgLngType'])) ? $params['ImgLngType'] : $record['ImgLngType'];
		
		if($ImgLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_image_gallerylng::sys_image_gallerylng_getRecord($appFrw, array('ImgLngID'=>$ImgLngID,'ImgLngType'=>$ImgLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$ImgLngTitle 	= (isset($params['ImgLngTitle'])) ? $params['ImgLngTitle'] : $record['ImgLngTitle'];
		$ImgLngImgID 	= (isset($params['ImgLngImgID'])) ? $params['ImgLngImgID'] : $record['ImgLngImgID'];
		$ImgLngSubtitle 	= (isset($params['ImgLngSubtitle'])) ? $params['ImgLngSubtitle'] : $record['ImgLngSubtitle'];
		$ImgLngDescription 	= (isset($params['ImgLngDescription'])) ? $params['ImgLngDescription'] : $record['ImgLngDescription'];
		
		$query = "	UPDATE sys_image_gallerylng SET
						 ImgLngTitle 					= ?
						,ImgLngType 					= ?
						,ImgLngImgID 		= ?
						,ImgLngSubtitle 		= ?
						,ImgLngDescription 		= ?
						
					WHERE
					ImgLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("siissi",
								 $ImgLngTitle 			
								,$ImgLngType 
								,$ImgLngImgID 
								,$ImgLngSubtitle 
								,$ImgLngDescription 
								,$ImgLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $ImgLngID;
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallerylng::sys_image_gallerylng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallerylng_CheckBeforeInsert($appFrw, $params)
	{
		$ImgLngImgID	= isset($params["ImgLngImgID"]) ? (int)$params["ImgLngImgID"] : 0;
		$ImgLngType	= isset($params["ImgLngType"]) ? (int)$params["ImgLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT ImgLngID FROM sys_image_gallerylng WHERE ImgLngImgID = ? AND ImgLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $ImgLngImgID, $ImgLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_image_gallerylng::sys_image_gallerylng_GetImgLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_image_gallerylng_GetImgLngID($appFrw, $params)
	{
		$ImgLngImgID	= isset($params["ImgLngImgID"]) ? (int)$params["ImgLngImgID"] : 0;
		$ImgLngType	= isset($params["ImgLngType"]) ? (int)$params["ImgLngType"] : 0;
		
		$query = "	SELECT
						   ImgLngID
						   FROM sys_image_gallerylng
							WHERE ImgLngImgID = ? AND ImgLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $ImgLngImgID, $ImgLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["ImgLngID"];
	}
	
	
	
}
