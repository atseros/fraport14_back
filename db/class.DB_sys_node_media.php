<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_node_media
*
* DESCRIPTION: 
*	Class for table sys_node_media
*
* table fields:
*
 `NodeMediaID` int(11) NOT NULL,
 `NodeMediaNodeID` int(11) NOT NULL,
 `NodeMediaMediaID` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_sys_node_media
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_media::sys_node_media_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_media_get_NewRecordDefValues($appFrw, $params)
	{	
		$NodeMediaID = DB_sys_kxn::get_NextID($appFrw, 'sys_node_media');
		
		if($NodeMediaID > 0)
		{
			$results["success"] = true;
			$results["data"]["NodeMediaID"] = $NodeMediaID;
			
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_node_media";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_media::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $NodeMediaID)
	{
		$query = "	SELECT
						   case when( exists (SELECT NodeMediaID FROM sys_node_media WHERE NodeMediaID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeMediaID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_media::sys_node_media_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_media_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeMediaID 	= (int)$params["NodeMediaID"];
		
		if($NodeMediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_node_media::check_RecordExists($appFrw, $NodeMediaID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$NodeMediaID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_node_media
					(
						 NodeMediaID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeMediaID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_node_media::sys_node_media_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_media::sys_node_media_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_media_getRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeMediaID = (int)$params["NodeMediaID"];
		
		if($NodeMediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		//Check if record exists
		if( !DB_sys_node_media::check_RecordExists($appFrw, $NodeMediaID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$NodeMediaID;
			return $results;
		}
		
		$query = "	SELECT
						
						NodeMediaID
						,NodeMediaNodeID
						,NodeMediaMediaID
						,NodeMediaPriority
							
					FROM sys_node_media
					WHERE
					NodeMediaID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $NodeMediaID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_media::sys_node_media_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_media_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeMediaID = (int)$params["NodeMediaID"];
		
		
		if($NodeMediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_node_media::sys_node_media_getRecord($appFrw, array('NodeMediaID'=>$NodeMediaID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$NodeMediaNodeID 	= (isset($params['NodeMediaNodeID'])) ? $params['NodeMediaNodeID'] : $record['NodeMediaNodeID'];
		$NodeMediaMediaID 	= (isset($params['NodeMediaMediaID'])) ? $params['NodeMediaMediaID'] : $record['NodeMediaMediaID'];
		$NodeMediaPriority 	= (isset($params['NodeMediaPriority'])) ? $params['NodeMediaPriority'] : $record['NodeMediaPriority'];
		
		$query = "	UPDATE sys_node_media SET
							
							NodeMediaNodeID 	= ?
							,NodeMediaMediaID 	= ?
							,NodeMediaPriority 	= ?
						
							WHERE
							NodeMediaID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iiii",
								 $NodeMediaNodeID 			
								,$NodeMediaMediaID 								
								,$NodeMediaPriority 								
								,$NodeMediaID 			
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $NodeMediaID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_shop::RecordCanBeDeleted
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_media::sys_node_media_DeleteRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_media_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$NodeMediaID = (int)$params["NodeMediaID"];
		
		if($NodeMediaID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_node_media WHERE NodeMediaID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $NodeMediaID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $NodeMediaID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_node_media::sys_node_media_getList
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_media_getList($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeID	= isset($params["NodeID"]) ? (int)$params["NodeID"] : 0;
		$MediaType	= isset($params["MediaType"]) ? (int)$params["MediaType"] : 0;
				
		$query = "	SELECT

							NodeMediaID
							,NodeMediaNodeID
							,NodeMediaMediaID
							,NodeMediaPriority
							,MediaID
							,MediaTitle
							,MediaFilename
							,MediaType
							,MediaSiteID
						
							FROM sys_node_media
							LEFT JOIN sys_media ON ( MediaID = NodeMediaMediaID )
							LEFT JOIN sys_node ON ( NodeID = NodeMediaNodeID )
							
							WHERE
							(
								MediaType LIKE ?
							)
							AND
							(
								NodeMediaNodeID  = ?
							)
							

							ORDER BY NodeMediaPriority ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("ii", $MediaType,$NodeID);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['MediaFullUrl']= DB_sys_media::get_sys_media_FullUrl($appFrw, $row['MediaID']);
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
		/*
	* --------------------------------------------------------------------------
	* DB_sys_node_media::sys_node_media_getListSitePages
	* --------------------------------------------------------------------------
	*/
	public static function sys_node_media_getListSitePages($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
		$NodeID	= isset($params["NodeID"]) ? (int)$params["NodeID"] : 0;
		$MediaType	= isset($params["MediaType"]) ? (int)$params["MediaType"] : 0;
		$MediaLngType	= isset($params["NodeLngType"]) ? (int)$params["NodeLngType"] : -1;
				
		$query = "	SELECT

							NodeMediaID
							,NodeMediaNodeID
							,NodeMediaMediaID
							,NodeMediaPriority
							,MediaID
							,MediaLngTitle as MediaTitle
							,MediaFilename
							,MediaType
							,MediaSiteID
						
							FROM sys_node_media
							LEFT JOIN sys_media ON ( MediaID = NodeMediaMediaID )
							LEFT JOIN sys_node ON ( NodeID = NodeMediaNodeID )
							LEFT JOIN sys_medialng ON ( MediaLngMediaID = MediaID )
							
							WHERE
							(
								MediaType LIKE ?
							)
							AND
							(
								NodeMediaNodeID  = ?
							)
							AND
							(
								MediaLngType = ?
							)
							AND
							(
								MediaLngTitle != ''
							)
							
							

							ORDER BY NodeMediaPriority ASC
					
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		$stmt->bind_param("iii", $MediaType,$NodeID,$MediaLngType);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			$row['MediaFullUrl']= DB_sys_media::get_sys_media_FullUrl($appFrw, $row['MediaID']);
			array_push($results['data'], $row);
		}
		$result->close();
	
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_media::get_sys_media_FullUrl
	* --------------------------------------------------------------------------
	*/
	public static function get_sys_media_FullUrl($appFrw, $MediaID)
	{
		$url = FileManager::getTblFolderUrl("sys_media", $MediaID )."/";
		$FulUrl = "";
		
		$imageData = DB_sys_media::sys_media_getRecord($appFrw,array('MediaID'=>$MediaID));
		if( $imageData["success"] == true  && $imageData["data"]["MediaFilename"] )
		{
			$FulUrl = $url.$imageData["data"]["MediaFilename"];
		}
		return $FulUrl;
	}
	
}
