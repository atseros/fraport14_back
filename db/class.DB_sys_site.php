<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_site
*
* DESCRIPTION: 
*	Class for table sys_site
*
* table fields:
*
* SiteID
* SiteDomainName
*
*********************************************************************************************/
class DB_sys_site
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_get_NewRecordDefValues
	*
	* input params:
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> array[] with return fields
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_get_NewRecordDefValues($appFrw, $params)
	{
		$SiteID = DB_sys_kxn::get_NextID($appFrw, 'sys_site');
		
		if($SiteID > 0)
		{
			$results["success"] = true;
			$results["data"]["SiteID"] = $SiteID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_sites";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::check_RecordExists
	*
	* input params:
	*		SiteID			int			:: id of the record
	*
	*
	* return:
	*	int								:: 0 = no, 1 = yes
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $SiteID)
	{
		$query = "	SELECT
						   case when( exists (SELECT SiteID FROM sys_site WHERE SiteID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_sup_cmp_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_InsertRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: new record id
	*		...							:: other fields of the table
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID 	= (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_site::check_RecordExists($appFrw, $SiteID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$SiteID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_site
					(
						 SiteID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_site::sys_Site_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_site_getRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> array[] with record fields
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_getRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_site::check_RecordExists($appFrw, $SiteID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$SiteID;
			return $results;
		}
		
		$query = "	SELECT
						
						SiteID
						,SiteDomainName
						,SiteUrlTitle						
						,SiteAirportCode						
						,SiteXmlFilename						
							
					FROM sys_site
					WHERE
					SiteID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_UpdateRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be updated
	*		...							:: other fields of the record to be updated
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record updated
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_site::sys_site_getRecord($appFrw, array('SiteID'=>$SiteID));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$SiteDomainName 		= (isset($params['SiteDomainName'])) ? $params['SiteDomainName'] : $record['SiteDomainName'];
		$SiteUrlTitle 				= (isset($params['SiteUrlTitle'])) ? $params['SiteUrlTitle'] : $record['SiteUrlTitle'];
		$SiteAirportCode 		= (isset($params['SiteAirportCode'])) ? $params['SiteAirportCode'] : $record['SiteAirportCode'];
		$SiteXmlFilename 		= (isset($params['SiteXmlFilename'])) ? $params['SiteXmlFilename'] : $record['SiteXmlFilename'];

		$query = "	UPDATE sys_site SET
						 SiteDomainName 			= ?
						,SiteUrlTitle 					= ?
						,SiteAirportCode 					= ?
						,SiteXmlFilename 					= ?
						
					WHERE
					SiteID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ssssi",
								 $SiteDomainName 			
								,$SiteUrlTitle 
								,$SiteAirportCode 
								,$SiteXmlFilename 
								,$SiteID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $SiteID;
		return $results;
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::RecordCanBeDeleted
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be checked
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function RecordCanBeDeleted($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_RecordCanBeDeleted: No id found. Can not check record";
			return $results;
		}

		// Check related records		
		$query = "	SELECT		
						 case when( exists (SELECT TplID FROM sys_tpl WHERE TplSiteID = ?))
							then 1
							else 0
						 end as HdrExists
						 
						 ,case when( exists (SELECT LangID FROM sys_lang WHERE LangSiteID = ?))
							then 1
							else 0
						 end as LangExists
						 
				";

		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("RecordCanBeDeleted: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("iiii", $SiteID, $SiteID, $SiteID, $SiteID);
		
		if(!$stmt->execute()) 
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("RecordCanBeDeleted: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		// Reason
		$results["reason"] = "";
		if( $row["HdrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are headers related to the site";
			
			return $results;
		}
		if( $row["FtrExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are footers related to the site";
			return $results;
		}
		if( $row["MenuExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are menus related to the site";
			return $results;
		}
		if( $row["LangExists"] == 1)
		{
			$results["success"] = false;
			$results["reason"] = "There are languages related to the site";
			return $results;
		}
				
		
		// ... continue
		
		$results["success"] = true;
		return $results;
		
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_DeleteRecord
	*
	* input params:
	*	array
	*	[
	*		SiteID			int			:: id of the record to be deleted
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> id of record deleted
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_DeleteRecord($appFrw, $params)
	{
		$results = array();
		
		$SiteID = (int)$params["SiteID"];
		
		if($SiteID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not delete record";
			return $results;
		}
		
		// //check if can be deleted
		// $canBeDeleted = DB_sys_site::RecordCanBeDeleted($appFrw, array('SiteID'=>$SiteID) );
		// if($canBeDeleted["success"] == false)
		// {
			// $results["success"] = false;
			// $results["reason"] = $canBeDeleted["reason"];
			
			// return $results;
		// }
		
		$query = "DELETE FROM sys_site WHERE SiteID = ?";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("delete_Record: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $SiteID);
		
		if(!$stmt->execute()) 
			exit("delete_Record: error at delete : ".$stmt->error);

		$stmt->close();			

		// return
		$results["success"] = true;
		$results["data"] = $SiteID;
		return $results;		
	}
	
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_Site_get_List
	*
	* input params:
	*	array
	*	[
	*		showAll			int			:: 0 = show active, 1 = show all
	*	]
	*
	*
	* failure return:
	*	array
	*	[
	*		success => false
	*		reason 	=> explanation string
	*	]
	* success return:
	*	array
	*	[
	*		success => true 
	*		data 	=> array
						[
							0	=>		array[] with first record fields
							...
							i	=>		array[] with i+1 record fields
						]
	*	]
	* --------------------------------------------------------------------------
	*/
	public static function sys_Site_get_List($appFrw, $params)
	{
		$results = array();

		$maxRecords	= isset($params["maxRecords"]) ? (int)$params["maxRecords"] : 0;
				
		$query = "	SELECT
						
							SiteID
							,SiteDomainName
							,SiteUrlTitle
							,SiteAirportCode
							,SiteXmlFilename
						
							FROM sys_site
							
					
		";
		
		if($maxRecords)
			$query .= " LIMIT ".$maxRecords;

		//exit($query);
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at prepare statement: ".$appFrw->DB_Link->error;
			return $results;
		}
		
		//$stmt->bind_param("is", $filterStrToFind, $filterShowAll);
		
		if(!$stmt->execute()) 
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
		{
			$results["success"] = false;
			$results["reason"] = "get_sup_cmp_List: error at select: ".$stmt->error;
			return $results;
		}
		
		$results['data'] = array();
		
		while( $row = $result->fetch_assoc())
		{
			array_push($results['data'], $row);
		}
		$result->close();
		
		
		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_site::sys_site_getAirportList
	* --------------------------------------------------------------------------
	*/
	public static function sys_site_getAirportList($appFrw, $params)
	{	
		$results['data'] = array();
		

		$dtpRow["SiteAirportName"] = 'Thessaloniki Macedonia International Airport';
		$dtpRow["SiteAirportCode"]	= "SKG";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Aktion National Airport';
		$dtpRow["SiteAirportCode"]	= "PVK";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Dionysios Solomos Airport';
		$dtpRow["SiteAirportCode"]	= "ZTH";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Alexander the Great International Airport';
		$dtpRow["SiteAirportCode"]	= "KVA";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Ioannis Kapodistrias International Airport';
		$dtpRow["SiteAirportCode"]	= "CFU";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Kefallinia Airport';
		$dtpRow["SiteAirportCode"]	= "EFL";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Kos Airport';
		$dtpRow["SiteAirportCode"]	= "KGS";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Mytilene International Airport';
		$dtpRow["SiteAirportCode"]	= "MJT";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Mikonos Airport';
		$dtpRow["SiteAirportCode"]	= "JMK";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Diagoras Airport';
		$dtpRow["SiteAirportCode"]	= "RHO";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Samos Airport';
		$dtpRow["SiteAirportCode"]	= "SMI";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Santorini Airport';
		$dtpRow["SiteAirportCode"]	= "JTR";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Skiathos Island National Airport';
		$dtpRow["SiteAirportCode"]	= "JSI";
		array_push($results['data'], $dtpRow);

		$dtpRow["SiteAirportName"] = 'Chania International Airport';
		$dtpRow["SiteAirportCode"]	= "CHQ";
		array_push($results['data'], $dtpRow);

		// return results
		$results["success"] = true;
		
		return $results;
	}
	
	
}
