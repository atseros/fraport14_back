<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_cf_answers_lang
*
* DESCRIPTION: 
*	Class for table cf_answers_lang
*
* table fields:
*
 `AnsLngID` int(11) NOT NULL,
 `AnsLngSubSubID` int(11) NOT NULL,
 `AnsLngType` int(11) NOT NULL,
 `AnsLngTitle` varchar(512) NOT NULL,
 `AnsLngDescr` varchar(1024) NOT NULL,
 `AnsLngShowCnt` int(11) NOT NULL,
*
*********************************************************************************************/
class DB_cf_answers_lang
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_answers_lang::cf_answers_lang_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function cf_answers_lang_get_NewRecordDefValues($appFrw, $params)
	{	
		$AnsLngID = DB_sys_kxn::get_NextID($appFrw, 'cf_answers_lang');
		
		if($AnsLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["AnsLngID"] = $AnsLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table cf_answers_lang";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_answers_lang::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $AnsLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT AnsLngID FROM cf_answers_lang WHERE AnsLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $AnsLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_cf_answers_lang::cf_answers_lang_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_answers_lang_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$AnsLngID 	= (int)$params["AnsLngID"];
		
		if($AnsLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_cf_answers_lang::check_RecordExists($appFrw, $AnsLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$AnsLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO cf_answers_lang
					(
						 AnsLngID
					)
					VALUES
					(
						 ?
					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $AnsLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_cf_answers_lang::cf_answers_lang_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_answers_lang::cf_answers_lang_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_answers_lang_getRecord($appFrw, $params)
	{
		$results = array();
		
		$AnsLngID    = (int)$params["AnsLngID"];
		$AnsLngType  = $params["AnsLngType"];
		
		if($AnsLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_cf_answers_lang::check_RecordExists($appFrw, $AnsLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$AnsLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						 AnsLngID
						,AnsLngSubSubID 
						,AnsLngType 
						,AnsLngTitle
						,AnsLngDescr
						,AnsLngShowCnt
						
					FROM cf_answers_lang
					WHERE
					(
						AnsLngID = ?
					)
					AND
					(
						AnsLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $AnsLngID, $AnsLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_cf_answers_lang::cf_answers_lang_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function cf_answers_lang_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$AnsLngID   = (int)$params["AnsLngID"];
		$AnsLngType = (isset($params['AnsLngType'])) ? $params['AnsLngType'] : $record['AnsLngType'];
		
		if($AnsLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_cf_answers_lang::cf_answers_lang_getRecord($appFrw, array('AnsLngID'=>$AnsLngID,'AnsLngType'=>$AnsLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
        $AnsLngSubSubID = (isset($params['AnsLngSubSubID'])) ? $params['AnsLngSubSubID'] : $record['AnsLngSubSubID'];
		$AnsLngTitle 	= (isset($params['AnsLngTitle'])) ? $params['AnsLngTitle'] : $record['AnsLngTitle'];
        $AnsLngDescr 	= (isset($params['AnsLngDescr'])) ? $params['AnsLngDescr'] : $record['AnsLngDescr'];
        $AnsLngShowCnt  = (isset($params['AnsLngShowCnt'])) ? $params['AnsLngShowCnt'] : $record['AnsLngShowCnt'];
		
		$query = "	UPDATE cf_answers_lang SET
						 AnsLngSubSubID 	= ?
						,AnsLngType 		= ?
						,AnsLngTitle 		= ?
						,AnsLngDescr        = ?
						,AnsLngShowCnt      = ?
					WHERE
					AnsLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("iissii",
                                 $AnsLngSubSubID
								,$AnsLngType 
								,$AnsLngTitle
                                ,$AnsLngDescr
                                ,$AnsLngShowCnt
								,$AnsLngID
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $AnsLngID;
		return $results;
	}

    /*
    * --------------------------------------------------------------------------
    * DB_cf_answers_lang::cf_answers_lang_CheckBeforeInsert
    * --------------------------------------------------------------------------
    */
    public static function cf_answers_lang_CheckBeforeInsert($appFrw, $params)
    {
        $AnsLngSubSubID    = isset($params["AnsLngSubSubID"]) ? (int)$params["AnsLngSubSubID"] : 0;
        $AnsLngType	    = isset($params["AnsLngType"]) ? (int)$params["AnsLngType"] : 0;

        $query = "	SELECT
						   case when( exists (SELECT AnsLngID FROM cf_answers_lang WHERE AnsLngSubSubID = ? AND AnsLngType = ? ))
							then 1
							else 0
						end as RecordExists";
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ii", $AnsLngSubSubID, $AnsLngType);

        if(!$stmt->execute())
            exit("check_RecordExists: error at select : ".$stmt->error);

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
            exit("check_RecordExists: error at select : ".$stmt->error);

        $row = $result->fetch_assoc();
        $result->close();

        return $row["RecordExists"];
    }

    /*
    * --------------------------------------------------------------------------
    * DB_cf_answers_lang::cf_answers_lang_GetAnsLngID
    * --------------------------------------------------------------------------
    */
    public static function cf_answers_lang_GetAnsLngID($appFrw, $params)
    {
        $AnsLngSubSubID    = isset($params["AnsLngSubSubID"]) ? (int)$params["AnsLngSubSubID"] : 0;
        $AnsLngType	    = isset($params["AnsLngType"]) ? (int)$params["AnsLngType"] : 0;

        $query = "	SELECT
						   AnsLngID
					FROM cf_answers_lang
					WHERE AnsLngSubSubID = ? AND AnsLngType = ?
				";
        $stmt = $appFrw->DB_Link->prepare($query);

        if(!$stmt)
            exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);

        $stmt->bind_param("ii", $AnsLngSubSubID, $AnsLngType);

        if(!$stmt->execute())
            exit("check_RecordExists: error at select : ".$stmt->error);

        $result = $stmt->get_result();
        $stmt->close();

        if(!$result)
            exit("check_RecordExists: error at select : ".$stmt->error);

        $row = $result->fetch_assoc();
        $result->close();

        return $row["AnsLngID"];
    }
	
}