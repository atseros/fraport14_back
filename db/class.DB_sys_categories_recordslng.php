<?php

require_once(realpath(__DIR__)."/class.DB_sys_kxn.php");

/*********************************************************************************************
* CLASS DB_sys_categories_recordslng
*
* DESCRIPTION: 
*	Class for table sys_categories_recordslng
*
* table fields:
*
 `CtgRecLngID` int(11) NOT NULL,
 `CtgRecLngCtgRecID` int(11) NOT NULL,
 `CtgRecLngType` int(11) NOT NULL,
 `CtgRecLngTitle` varchar(1024) NOT NULL,
 `CtgRecLngSubtitle` varchar(1024) NOT NULL,
 `CtgRecLngText` text NOT NULL,
 `CtgRecLngDictionaryDescription` varchar(1024) DEFAULT NULL,
*
*********************************************************************************************/
class DB_sys_categories_recordslng
{
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_recordslng::sys_categories_recordslng_get_NewRecordDefValues
	* --------------------------------------------------------------------------
	*/
	
	public static function sys_categories_recordslng_get_NewRecordDefValues($appFrw, $params)
	{	
		$CtgRecLngID = DB_sys_kxn::get_NextID($appFrw, 'sys_categories_recordslng');
		
		if($CtgRecLngID > 0)
		{
			$results["success"] = true;
			$results["data"]["CtgRecLngID"] = $CtgRecLngID;
		}
		else
		{
			$results["success"] = false;
			$results["reason"] = "failed to get next id for table sys_categories_recordslng";
		}	
		
		return $results;
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_recordslng::check_RecordExists
	* --------------------------------------------------------------------------
	*/
	public static function check_RecordExists($appFrw, $CtgRecLngID)
	{
		$query = "	SELECT
						   case when( exists (SELECT CtgRecLngID FROM sys_categories_recordslng WHERE CtgRecLngID = ?))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("i", $CtgRecLngID);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_recordslng::sys_categories_recordslng_InsertRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_recordslng_InsertRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecLngID 	= (int)$params["CtgRecLngID"];
		
		if($CtgRecLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found for new record";
			return $results;
		}
		
		if( DB_sys_categories_recordslng::check_RecordExists($appFrw, $CtgRecLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is already a record with id = ".$CtgRecLngID;
			return $results;
		}
			
		// insert an empty record
		$query = "	INSERT INTO sys_categories_recordslng
					(
						 CtgRecLngID

					)
					VALUES
					(
						 ?

					)";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("insert_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("i", $CtgRecLngID);
		
		if(!$stmt->execute()) 
			exit("insert_Record: error at inert : ".$stmt->error);
		
		$stmt->close();
		
		// update with params
		$results = DB_sys_categories_recordslng::sys_categories_recordslng_UpdateRecord($appFrw, $params);
		
		return $results;
		
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_recordslng::sys_categories_recordslng_getRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_recordslng_getRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecLngID = (int)$params["CtgRecLngID"];
		$CtgRecLngType = $params["CtgRecLngType"];
		
		if($CtgRecLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "No id found. Can not get record";
			return $results;
		}
		
		// Check if record exists
		if( !DB_sys_categories_recordslng::check_RecordExists($appFrw, $CtgRecLngID) )
		{
			$results["success"] = false;
			$results["reason"] = "There is no  record with id = ".$CtgRecLngID;
			return $results;
		}
		
		$query = "	SELECT
						
						CtgRecLngID
						,CtgRecLngCtgRecID 
						,CtgRecLngType 
						,CtgRecLngTitle 
						,CtgRecLngSubtitle 
						,CtgRecLngText 
						,CtgRecLngDictionaryDescription 
							
					FROM sys_categories_recordslng
					WHERE
					(
						CtgRecLngID = ?
					)
					AND
					(
						CtgRecLngType = ?
					)
					
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("get_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ii", $CtgRecLngID, $CtgRecLngType);
		
		if(!$stmt->execute()) 
			exit("get_Record: error at select : ".$stmt->error);
		
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("get_Record: error at select : ".$stmt->error);
							
		$row = $result->fetch_assoc();		
		$result->close();
		

		// return results
		$results["success"] = true;
		$results["data"] = $row;
		
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_recordslng::sys_categories_recordslng_UpdateRecord
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_recordslng_UpdateRecord($appFrw, $params)
	{
		$results = array();
		
		$CtgRecLngID = (int)$params["CtgRecLngID"];
		$CtgRecLngType 	= (isset($params['CtgRecLngType'])) ? $params['CtgRecLngType'] : $record['CtgRecLngType'];
		
		if($CtgRecLngID <= 0)
		{
			$results["success"] = false;
			$results["reason"] = "update_Record: No id found. Can not update record";
			return $results;
		}
		
		// get already saved values
		$tmp_record = DB_sys_categories_recordslng::sys_categories_recordslng_getRecord($appFrw, array('CtgRecLngID'=>$CtgRecLngID,'CtgRecLngType'=>$CtgRecLngType));
		if($tmp_record["success"] == true)
		{
			$record = $tmp_record["data"];
		}			
		else
		{
			$results["success"] = false;
			$results["reason"] = $tmp_record["reason"];
			return $results;
		}
		
		// get param fields
		$CtgRecLngTitle 	= (isset($params['CtgRecLngTitle'])) ? $params['CtgRecLngTitle'] : $record['CtgRecLngTitle'];
		$CtgRecLngSubtitle 	= (isset($params['CtgRecLngSubtitle'])) ? $params['CtgRecLngSubtitle'] : $record['CtgRecLngSubtitle'];
		$CtgRecLngDictionaryDescription 	= (isset($params['CtgRecLngDictionaryDescription'])) ? $params['CtgRecLngDictionaryDescription'] : $record['CtgRecLngDictionaryDescription'];
		$CtgRecLngText 	= (isset($params['CtgRecLngText'])) ? $params['CtgRecLngText'] : $record['CtgRecLngText'];
		$CtgRecLngCtgRecID 	= (isset($params['CtgRecLngCtgRecID'])) ? $params['CtgRecLngCtgRecID'] : $record['CtgRecLngCtgRecID'];
		
		$query = "	UPDATE sys_categories_recordslng SET
						 CtgRecLngTitle 					= ?
						,CtgRecLngSubtitle 					= ?
						,CtgRecLngDictionaryDescription 		= ?
						,CtgRecLngText 		= ?
						,CtgRecLngType 		= ?
						,CtgRecLngCtgRecID 		= ?
						
					WHERE
					CtgRecLngID = ?
		";
		
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("update_Record: error at prepare statement: ".$appFrw->DB_Link->error);
		
		$stmt->bind_param("ssssiii",
								 $CtgRecLngTitle 			
								,$CtgRecLngSubtitle 
								,$CtgRecLngDictionaryDescription 
								,$CtgRecLngText 
								,$CtgRecLngType 
								,$CtgRecLngCtgRecID 
								,$CtgRecLngID 
								
						);
		
		if(!$stmt->execute()) 
			exit("update_Record: error at update : ".$stmt->error);
		
		$stmt->close();					
		
		// return
		$results["success"] = true;
		$results["data"] = $CtgRecLngID;
		return $results;
	}
		
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_recordslng::sys_categories_recordslng_CheckBeforeInsert
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_recordslng_CheckBeforeInsert($appFrw, $params)
	{
		$CtgRecLngCtgRecID	= isset($params["CtgRecLngCtgRecID"]) ? (int)$params["CtgRecLngCtgRecID"] : 0;
		$CtgRecLngType	= isset($params["CtgRecLngType"]) ? (int)$params["CtgRecLngType"] : 0;
		
		$query = "	SELECT
						   case when( exists (SELECT CtgRecLngID FROM sys_categories_recordslng WHERE CtgRecLngCtgRecID = ? AND CtgRecLngType = ? ))
							then 1
							else 0
						end as RecordExists";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $CtgRecLngCtgRecID, $CtgRecLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["RecordExists"];
	}
	
	/*
	* --------------------------------------------------------------------------
	* DB_sys_categories_recordslng::sys_categories_recordslng_GetCategoriesRecordsLngID
	* --------------------------------------------------------------------------
	*/
	public static function sys_categories_recordslng_GetCategoriesRecordsLngID($appFrw, $params)
	{
		$CtgRecLngCtgRecID	= isset($params["CtgRecLngCtgRecID"]) ? (int)$params["CtgRecLngCtgRecID"] : 0;
		$CtgRecLngType	= isset($params["CtgRecLngType"]) ? (int)$params["CtgRecLngType"] : 0;
		
		$query = "	SELECT
						   CtgRecLngID
						   FROM sys_categories_recordslng
							WHERE CtgRecLngCtgRecID = ? AND CtgRecLngType = ?
						";
		$stmt = $appFrw->DB_Link->prepare($query);
		
		if(!$stmt) 
			exit("check_RecordExists: error at prepare statement: ".$appFrw->DB_Link->error);
				
		$stmt->bind_param("ii", $CtgRecLngCtgRecID, $CtgRecLngType);
		
		if(!$stmt->execute()) 
			exit("check_RecordExists: error at select : ".$stmt->error);
				
		$result = $stmt->get_result();
		$stmt->close();			
								
		if(!$result)
			exit("check_RecordExists: error at select : ".$stmt->error);
									
		$row = $result->fetch_assoc();		
		$result->close();
		
		return $row["CtgRecLngID"];
	}
	
	
	
}
