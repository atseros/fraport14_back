<?php



/*
function libxml_display_error($error)
{
    $return = "<br/>\n";
    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "<b>Warning $error->code</b>: ";
            break;
        case LIBXML_ERR_ERROR:
            $return .= "<b>Error $error->code</b>: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "<b>Fatal Error $error->code</b>: ";
            break;
    }
    $return .= trim($error->message);
    if ($error->file) {
        $return .=    " in <b>$error->file</b>";
    }
    $return .= " on line <b>$error->line</b>\n";

    return $return;
}

function libxml_display_errors() {
    $errors = libxml_get_errors();
    foreach ($errors as $error) {
        print libxml_display_error($error);
    }
    libxml_clear_errors();
}

*/
/*
if (!$xml->schemaValidate($xsdfile)) {
    print '<b>DOMDocument::schemaValidate() Generated Errors!</b>';
    libxml_display_errors();
}
*/
// Enable user error handling
//libxml_use_internal_errors(true);



$xmlfile = __DIR__.'/flighttracker_SKG.xml'; 
//$xsdfile = __DIR__.'/demo_flightlist.xsd'; 



$xml_flights_array = parse_xml_flights($xmlfile);

if (is_array($xml_flights_array))
foreach ($xml_flights_array as $fltdata)
{
	if(isset($fltdata['Type'])) { $FltType = $fltdata['Type']; } else { $FltType = "" ;}
	if(isset($fltdata['FlightNumber'])) { $FltFlightNumber = $fltdata['FlightNumber']; } else { $FltFlightNumber = "" ;}
	if(isset($fltdata['FltFlightNumber2'])) { $FltFlightNumber2 = $fltdata['FltFlightNumber2']; } else { $FltFlightNumber2 = "" ;}
	if(isset($fltdata['FltFlightNumber3'])) { $FltFlightNumber3 = $fltdata['FltFlightNumber3']; } else { $FltFlightNumber3 = "" ;}
	if(isset($fltdata['FltFlightNumber4'])) { $FltFlightNumber4 = $fltdata['FltFlightNumber4']; } else { $FltFlightNumber4 = "" ;}
	if(isset($fltdata['FltFlightNumber5'])) { $FltFlightNumber5 = $fltdata['FltFlightNumber5']; } else { $FltFlightNumber5 = "" ;}
	if(isset($fltdata['FltAirlineCode'])) { $FltAirlineCode = $fltdata['FltAirlineCode']; } else { $FltAirlineCode = "" ;}
	if(isset($fltdata['FltAirlineCarrier'])) { $FltAirlineCarrier = $fltdata['FltAirlineCarrier']; } else { $FltAirlineCarrier = "" ;}
	if(isset($fltdata['FltAirlineIATA'])) { $FltAirlineIATA = $fltdata['FltAirlineIATA']; } else { $FltAirlineIATA = "" ;}
	if(isset($fltdata['FltAirlineICAO'])) { $FltAirlineICAO = $fltdata['FltAirlineICAO']; } else { $FltAirlineICAO = "" ;}
	if(isset($fltdata['FltScheduledTime'])) { $FltScheduledTime = $fltdata['FltScheduledTime']; } else { $FltScheduledTime = "" ;}
	if(isset($fltdata['FltEstimatedTime'])) { $FltEstimatedTime = $fltdata['FltEstimatedTime']; } else { $FltEstimatedTime = "" ;}
	if(isset($fltdata['FltActualTime'])) { $FltActualTime = $fltdata['FltActualTime']; } else { $FltActualTime = "" ;}
	if(isset($fltdata['FltAirportName'])) { $FltAirportName = $fltdata['FltAirportName']; } else { $FltAirportName = "" ;}
	if(isset($fltdata['FltAirportShortName'])) { $FltAirportShortName = $fltdata['FltAirportShortName']; } else { $FltAirportShortName = "" ;}
	if(isset($fltdata['FltAirportIATA'])) { $FltAirportIATA = $fltdata['FltAirportIATA']; } else { $FltAirportIATA = "" ;}
	if(isset($fltdata['FltAirportICAO'])) { $FltAirportICAO = $fltdata['FltAirportICAO']; } else { $FltAirportICAO = "" ;}
	if(isset($fltdata['FltVia1Name'])) { $FltVia1Name = $fltdata['FltVia1Name']; } else { $FltVia1Name = "" ;}
	if(isset($fltdata['FltVia1ShortName'])) { $FltVia1ShortName = $fltdata['FltVia1ShortName']; } else { $FltVia1ShortName = "" ;}
	if(isset($fltdata['FltVia1IATA'])) { $FltVia1IATA = $fltdata['FltVia1IATA']; } else { $FltVia1IATA = "" ;}
	if(isset($fltdata['FltVia1ICAO'])) { $FltVia1ICAO = $fltdata['FltVia1ICAO']; } else { $FltVia1ICAO = "" ;}
	if(isset($fltdata['FltVia2Name'])) { $FltVia2Name = $fltdata['FltVia2Name']; } else { $FltVia2Name = "" ;}
	if(isset($fltdata['FltVia2ShortName'])) { $FltVia2ShortName = $fltdata['FltVia2ShortName']; } else { $FltVia2ShortName = "" ;}
	if(isset($fltdata['FltVia2IATA'])) { $FltVia2IATA = $fltdata['FltVia2IATA']; } else { $FltVia2IATA = "" ;}
	if(isset($fltdata['FltVia2ICAO'])) { $FltVia2ICAO = $fltdata['FltVia2ICAO']; } else { $FltVia2ICAO = "" ;}
	if(isset($fltdata['FltVia3Name'])) { $FltVia3Name = $fltdata['FltVia3Name']; } else { $FltVia3Name = "" ;}
	if(isset($fltdata['FltVia3ShortName'])) { $FltVia3ShortName = $fltdata['FltVia3ShortName']; } else { $FltVia3ShortName = "" ;}
	if(isset($fltdata['FltVia3IATA'])) { $FltVia3IATA = $fltdata['FltVia3IATA']; } else { $FltVia3IATA = "" ;}
	if(isset($fltdata['FltVia3ICAO'])) { $FltVia3ICAO = $fltdata['FltVia3ICAO']; } else { $FltVia3ICAO = "" ;}
	if(isset($fltdata['FltAircraftType'])) { $FltAircraftType = $fltdata['FltAircraftType']; } else { $FltAircraftType = "" ;}
	if(isset($fltdata['FltKindOfFlight'])) { $FltKindOfFlight = $fltdata['FltKindOfFlight']; } else { $FltKindOfFlight = "" ;}
	if(isset($fltdata['FltTerminal'])) { $FltTerminal = $fltdata['FltTerminal']; } else { $FltTerminal = "" ;}
	if(isset($fltdata['FltGate'])) { $FltGate = $fltdata['FltGate']; } else { $FltGate = "" ;}
	if(isset($fltdata['FltCheckIn'])) { $FltCheckIn = $fltdata['FltCheckIn']; } else { $FltCheckIn = "" ;}
	if(isset($fltdata['FltRemarkFids'])) { $FltRemarkFids = $fltdata['FltRemarkFids']; } else { $FltRemarkFids = "" ;}
	
}


echo "<pre>";
print_r($xml_flights_array);
echo "</pre>";

exit();


function parse_xml_flights($xmlfile)
{
	$flights_array = array();

	$xml = new DOMDocument(); 
	$xml->load($xmlfile); 

	$x = $xml->documentElement;

	if($x->hasAttributes())
	foreach ($x->attributes as $attr) 
	{
		if($attr->nodeName == "timestamp")
			$flights_array["timestamp"] = $attr->nodeValue;
	}
	
		$flight_count=1;
	foreach ($x->childNodes AS $flight) 
	{
		if($flight->nodeName == "Flight")
		{			
			$flights_array[$flight_count] = array();
			
			
			if($flight->childNodes)
			{
				foreach ($flight->childNodes AS $flight_elem) 
				{
					if($flight_elem->nodeName == "Type")
						$flights_array[$flight_count]["Type"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber")
						$flights_array[$flight_count]["FlightNumber"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber2")
						$flights_array[$flight_count]["FlightNumber2"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber3")
						$flights_array[$flight_count]["FlightNumber3"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber4")
						$flights_array[$flight_count]["FlightNumber4"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "FlightNumber5")
						$flights_array[$flight_count]["FlightNumber5"] = $flight_elem->nodeValue;

					// AIRLINE
					else if($flight_elem->nodeName == "Airline")
					{
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "code")
								$flights_array[$flight_count]["Airline_code"] = $attr->nodeValue;
						}
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Carrier" )
								$flights_array[$flight_count]["Airline_Carrier"] = $child->nodeValue;
							else if($child->nodeName == "IATA" )
								$flights_array[$flight_count]["Airline_IATA"] = $child->nodeValue;
							else if($child->nodeName == "ICAO" )
								$flights_array[$flight_count]["Airline_ICAO"] = $child->nodeValue;
						}
					}
					else if($flight_elem->nodeName == "ScheduledTime")
					{					
						date_default_timezone_set('UTC');
						$timestamp = strtotime($flight_elem->nodeValue);					
						date_default_timezone_set('Europe/Athens') ;
						
						$flights_array[$flight_count]["ScheduledTime"] = date( 'Y-m-d H:i', $timestamp);
					}
					else if($flight_elem->nodeName == "EstimatedTime")
					{
						date_default_timezone_set('UTC');
						$timestamp = strtotime($flight_elem->nodeValue);					
						date_default_timezone_set('Europe/Athens') ;
						
						$flights_array[$flight_count]["EstimatedTime"] = date( 'Y-m-d H:i', $timestamp);
					}
					else if($flight_elem->nodeName == "ActualTime")
					{
						date_default_timezone_set('UTC');
						$timestamp = strtotime($flight_elem->nodeValue);					
						date_default_timezone_set('Europe/Athens') ;
						
						$flights_array[$flight_count]["ActualTime"] = date( 'Y-m-d H:i', $timestamp);
					}
					
					// AIRPORT
					else if($flight_elem->nodeName == "Airport")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "iata")
								$flights_array[$flight_count]["Airport_iata"] = $attr->nodeValue;
							else if($attr->nodeName == "icao")
								$flights_array[$flight_count]["Airport_icao"] = $attr->nodeValue;
							else if($attr->nodeName == "name")
								$flights_array[$flight_count]["Airport_name"] = $attr->nodeValue;
							else if($attr->nodeName == "shortName")
								$flights_array[$flight_count]["Airport_shortname"] = $attr->nodeValue;
						}
						
						$translation_count = 1;
						$flights_array[$flight_count]["Airport_Translations"][$translation_count] = array();
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$flights_array[$flight_count]["Airport_Translations"][$translation_count]["name"] = $child->nodeValue;
								
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
										$flights_array[$flight_count]["Airport_Translations"][$translation_count]["lang"] = $attr->nodeValue;
								}
								
								$translation_count++;
							}
						}
					}
					// Via1
					else if($flight_elem->nodeName == "Via1")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "iata")
								$flights_array[$flight_count]["Via1_iata"] = $attr->nodeValue;
							else if($attr->nodeName == "icao")
								$flights_array[$flight_count]["Via1_icao"] = $attr->nodeValue;
							else if($attr->nodeName == "name")
								$flights_array[$flight_count]["Via1_name"] = $attr->nodeValue;
							else if($attr->nodeName == "shortName")
								$flights_array[$flight_count]["Via1_shortname"] = $attr->nodeValue;
						}
						
						$translation_count = 1;
						$flights_array[$flight_count]["Via1_Translations"][$translation_count] = array();
						
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$flights_array[$flight_count]["Via1_Translations"][$translation_count]["name"] = $child->nodeValue;
									
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
										$flights_array[$flight_count]["Via1_Translations"][$translation_count]["lang"] = $attr->nodeValue;
								}
								
								$translation_count++;
							}
						}
					}
					// Via2
					else if($flight_elem->nodeName == "Via2")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "iata")
								$flights_array[$flight_count]["Via2_iata"] = $attr->nodeValue;
							else if($attr->nodeName == "icao")
								$flights_array[$flight_count]["Via2_icao"] = $attr->nodeValue;
							else if($attr->nodeName == "name")
								$flights_array[$flight_count]["Via2_name"] = $attr->nodeValue;
							else if($attr->nodeName == "shortName")
								$flights_array[$flight_count]["Via2_shortname"] = $attr->nodeValue;
						}
						
						$translation_count = 1;
						$flights_array[$flight_count]["Via2_Translations"][$translation_count] = array();
						
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$flights_array[$flight_count]["Via2_Translations"][$translation_count]["name"] = $child->nodeValue;
									
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
										$flights_array[$flight_count]["Via2_Translations"][$translation_count]["lang"] = $attr->nodeValue;
								}
								
								$translation_count++;
							}
						}
					}
					
					// Via3
					else if($flight_elem->nodeName == "Via3")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "iata")
								$flights_array[$flight_count]["Via3_iata"] = $attr->nodeValue;
							else if($attr->nodeName == "icao")
								$flights_array[$flight_count]["Via3_icao"] = $attr->nodeValue;
							else if($attr->nodeName == "name")
								$flights_array[$flight_count]["Via3_name"] = $attr->nodeValue;
							else if($attr->nodeName == "shortName")
								$flights_array[$flight_count]["Via3_shortname"] = $attr->nodeValue;
						}
						
						$translation_count = 1;
						$flights_array[$flight_count]["Via3_Translations"][$translation_count] = array();
						
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$flights_array[$flight_count]["Via3_Translations"][$translation_count]["name"] = $child->nodeValue;
								
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
										$flights_array[$flight_count]["Via3_Translations"][$translation_count]["lang"] = $attr->nodeValue;
								}
								
								$translation_count++;
							}
						}
					}
					else if($flight_elem->nodeName == "AircraftType")
						$flights_array[$flight_count]["AircraftType"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "KindOfFlight")
						$flights_array[$flight_count]["KindOfFlight"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "Terminal")
						$flights_array[$flight_count]["Terminal"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "Gate")
						$flights_array[$flight_count]["Gate"] = $flight_elem->nodeValue;
					else if($flight_elem->nodeName == "CheckIn")
						$flights_array[$flight_count]["CheckIn"] = $flight_elem->nodeValue;
					
					
					// RemarkFids
					else if($flight_elem->nodeName == "RemarkFids")
					{
						
						if ($flight_elem->hasAttributes()) 
						foreach ($flight_elem->attributes as $attr) 
						{
							if($attr->nodeName == "code")
								$flights_array[$flight_count]["RemarkFids_code"] = $attr->nodeValue;
						}
						
						$translation_count = 1;
						$flights_array[$flight_count]["RemarkFids_Translations"][$translation_count] = array();
						
						
						if($flight_elem->childNodes)
						foreach ($flight_elem->childNodes AS $child) 
						{
							if($child->nodeName == "Translation" )
							{
								$flights_array[$flight_count]["RemarkFids_Translations"][$translation_count]["name"] = $child->nodeValue;
									
								if($child->hasAttributes())
								foreach ($child->attributes as $attr) 
								{
									if($attr->nodeName == "language")
										$flights_array[$flight_count]["RemarkFids_Translations"][$translation_count]["lang"] = $attr->nodeValue;
								}
								
								$translation_count++;
							}
						}
					}				
				}
			}		
			
			$flight_count++;
		}
	  
	}
	return $flights_array;
}




















