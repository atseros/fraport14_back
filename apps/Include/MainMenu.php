<?php
$UsrData = DB_sys_usr::sys_usr_getRecord($appFrw, array("UsrID"=>$appFrw->UsrID));
$UsrRole = $UsrData["data"]["UsrRole"];
?>
<script type="text/javascript">
Ext.apply(Ext.EventManager,{      
	normalizeEvent: function(eventName, fn) 
	{
		//start fix
		var EventManager = Ext.EventManager,
		supports = Ext.supports;
		if(Ext.chromeVersion >=43 && eventName == 'mouseover')
		{
			var origFn = fn;
			fn = function()
			{
				var me = this,
				args = arguments;
				setTimeout( function(){ origFn.apply(me || Ext.global, args); }, 0); 
			};
		} 
		//end fix
		if (EventManager.mouseEnterLeaveRe.test(eventName) && !supports.MouseEnterLeave) 
		{
			if (fn) { fn = Ext.Function.createInterceptor(fn, EventManager.contains); }
			eventName = eventName == 'mouseenter' ? 'mouseover' : 'mouseout';
		} 
		else if (eventName == 'mousewheel' && !supports.MouseWheel && !Ext.isOpera) 
		{
			eventName = 'DOMMouseScroll';
		}
		return{ eventName: eventName, fn: fn };
	}
});
		
Ext.define('Include.view.Menu', 
{
	extend: 'Ext.button.Button',
	alias: 'widget.menubutton',
	itemId: 'btnOMenu',
	id:'btnOMenu',
	text: '<?php echo($menuCurrentPage)?>',
	cls: 'menuBtn',
	overCls: 'menuBtn-over',
	
	initComponent: function() 
	{
		this.menu = 
		[
			<?php 
			//if($UsrRole > 1 ) 
			{
			?>
				// Home
				{	
					xtype:	'button',
					iconCls:	'fa fa-asterisk',
					text: 'Airports',
					tooltip: 'Airports',
					handler: function()
					{
						location.href = 'appMain.php?appPage=Home';
					}
				},
				// Airlines
				{	
					xtype:	'button',
					iconCls:	'fa fa-plane',
					text: 'Central Airlines',
					tooltip: 'Central Airlines Page',
					handler: function()
					{
						location.href = 'appMain.php?appPage=Airlines';
					}
				},
				// AirportDetails
				{
					xtype:	'button',
					iconCls:	'fa fa-flag',
					text: 'Airport Details',
					tooltip: 'Airport Details',
					handler: function()
					{
						location.href = 'appMain.php?appPage=AirportDetails';
					}
				},
				// // Central Media Gallery
				// {	
					// xtype:	'button',
					// iconCls:	'fa fa-files-o',
					// text: 'Central Media Gallery',
					// tooltip: 'Central Media Gallery Page',
					// handler: function()
					// {
						// location.href = 'appMain.php?appPage=CentralMediaGallery';
					// }
				// },
				// Seasonal Flight Plan
				{	
					xtype:	'button',
					iconCls:	'fa fa-info',
					text: 'Seasonal Flight Plan',
					tooltip: 'Seasonal Flight Plan',
					handler: function()
					{
						location.href = 'appMain.php?appPage=FlightPlan';
					}
				},
				// // AirportData
				// {	
					// xtype:	'button',
					// iconCls:	'fa fa-table',
					// text: 'Airport Data',
					// tooltip: 'Airport Data Page',
					// handler: function()
					// {
						// location.href = 'appMain.php?appPage=AirportData';
					// }
				// },
				// Dictionary
				{	
					xtype:	'button',
					iconCls:	'fa fa-globe',
					text: 'Dictionary',
					tooltip: 'Dictionary Page',
					handler: function()
					{
						location.href = 'appMain.php?appPage=Dictionary';
					}
				},
				// Users
				{
					xtype:	'button',
					iconCls:	'fa fa-users',
					text: 'Users',
					tooltip: 'Users Page',
					handler: function()
					{
						location.href = 'appMain.php?appPage=Users';
					}
				},				
			<?php 
			}
			?>
		];

		this.callParent(arguments);
	}
});

</script>



