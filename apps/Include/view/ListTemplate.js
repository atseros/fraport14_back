﻿
Ext.define('Include.view.ListTemplate' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.ListTemplate',
		
	
	initComponent: function() 
	{
													
		this.callParent(arguments);		
		
		// add toolbar and buttons
		this.MessageToolbar = Ext.create('Ext.toolbar.Toolbar', { dock: 'top', hidden: true }); 
		this.addDocked(this.MessageToolbar); 

		// Message Label
		this.MessageLabel = Ext.create('Ext.form.Label', 
		{
			text: '',
			height: 15,
			margins: '0 0 0 10',
			style: {color: 'red', fontWeight:'bold', textAlign:'center' }
		});	
		this.MessageToolbar.insert(this.MessageLabel);	

	},
		
	
	/**************************************************************************************************
	* getSelectedRecordID
	*
	***************************************************************************************************/
	getSelectedRecordID: function()
	{
		var thisgrid = this;
		var fieldID = 0;
		
		var sr = thisgrid.getSelectionModel().getSelection();
						
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				fieldID = selectedRecord.get(this.fieldNameID);
			}
		}
		
		return fieldID;
	},
	
	/**************************************************************************************************
	* getSelectedRecordField
	*
	***************************************************************************************************/
	getSelectedRecordField: function(fieldName)
	{
		var thisgrid 		= this;
		var fieldValue	 	= null;
		
		var sr = thisgrid.getSelectionModel().getSelection();
						
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				fieldValue = selectedRecord.get(fieldName);
			}
		}
		
		return fieldValue;
	},
	
	/**************************************************************************************************
	* clearData
	***************************************************************************************************/	
	clearData: function()
	{
		var me = this;
	
		me.getStore().removeAll();	
		if (typeof (me.callbackClearRelatedBeforeLoad) == 'function')
		{
			me.callbackClearRelatedBeforeLoad();
		}
	},
	
	/**************************************************************************************************
	* loadData
	*
	***************************************************************************************************/		
	loadData: function()
	{								
		var me = this;

		me.rememberSelection();		
		
		//first Clear Related Views Before Load
		if (typeof (me.callbackClearRelatedBeforeLoad) == 'function')
		{
			me.callbackClearRelatedBeforeLoad();
		}
		
		// init - empty message 		
		if(me.MessageToolbar && me.MessageLabel)
		{
			me.MessageLabel.setText('');
			me.MessageToolbar.hide();	
		}
			
		
		// Mask
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();
		

		// suspend Events to prevent selection change fire before new data loaded
		me.suspendEvents( false );
		
		var params = me.getLoadParams();
		
		me.getStore().load({
			url: 'appAjax.php',
			method: 'post',
			params: params,				
			callback: function()
			{				
				me.resumeEvents();
				me.refreshSelection();
				myMask.hide();							
				
				var countRecords = me.getStore().getCount();
				
				if (me.maxRecords && me.maxRecords > 0 && me.getStore().getCount() == me.maxRecords) 
				{
					// set message 
					if(me.MessageToolbar && me.MessageLabel)
					{
						me.MessageToolbar.show();
						me.MessageLabel.setText('Only the first '+me.maxRecords+' records are shown. Set filters to reduce the result!');
					}
				}	
			}
		});
	},
	
	/**************************************************************************************************
	* rememberSelection, refreshSelection
	*
	* Functions for keeping focused and selected rows
	*
	***************************************************************************************************/
	cachedSelectedRecords: [],
	
	rememberSelection: function() 
	{
		this.cachedSelectedRecords = this.getSelectionModel().getSelection();
        this.getView().saveScrollState();
	},
	
	refreshSelection: function() 
	{		
		var newRecordsToSelect = [];
		
		if (this.cachedSelectedRecords.length == 0) 
		{
			if( this.getStore().getCount() > 0 )
			{
				var sm = this.getSelectionModel();
				var record = sm.select(0);
			}
			return
		}

		for (var i = 0; i < this.cachedSelectedRecords.length; i++) 
		{
			if (!Ext.isEmpty(this.cachedSelectedRecords[i])) 
			{
				var Id = this.cachedSelectedRecords[i].get(this.fieldNameID);
			
				record = this.getStore().findRecord(this.fieldNameID, Id);
			
				if (!Ext.isEmpty(record)) 
				{
					newRecordsToSelect.push(record);
				}
			}
		}
        if (!Ext.isEmpty( newRecordsToSelect )) 
		{
			this.getSelectionModel().select(newRecordsToSelect);   
		}
		else
		{
			if( this.getStore().getCount() > 0 )
			{
				var sm = this.getSelectionModel();
				var record = sm.select(0);
			}
		}
	},
	 
	
});





