﻿Ext.define('Include.view.FormTemplate', {
    extend: 'Ext.Panel',
    alias: 'widget.FormTemplate',
    

    initComponent: function () 
	{	
			    
		this.callbackRefresh = '';
		
		this.callParent(arguments);
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  afterLoadDefault function
	// default - to be overriden
	//////////////////////////////////////////////////////////////////////////////////////////////
	afterLoadDefault: function(data)
	{
		var me = this;
		var thisf = me.query('form')[0];
		
		thisf.getForm().setValues(data);
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  clearData function
	//////////////////////////////////////////////////////////////////////////////////////////////
	clearData: function()
	{	
	
		var me = this;				
		
		var thisf = this.query('form')[0];
		thisf.getForm().reset();	

		return;
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  loadData function
	//////////////////////////////////////////////////////////////////////////////////////////////
	loadData: function()
	{								
		var me = this;				
								
		// Load Mask
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();

		var ajaxParams;
		
		ajaxParams = me.getLoadParams();
		
		Ext.Ajax.request(
		{				
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			params: ajaxParams,			
			timeout: 60000,
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{	
					me.afterLoadDefault(obj.data);
				
					myMask.hide();
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{
				myMask.hide();	
				me.close();
				Ext.Msg.alert('Error', 'Error in retrieving data !');				
				return;				
			}   								
		});
		
		
	}, // End: loadData()
	


});



