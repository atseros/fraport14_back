﻿
Ext.define('Include.view.Wnd_NewEditTemplate', {
    extend: 'Ext.window.Window',
    alias: 'widget.Wnd_NewEditTemplate',
		
	// Buttons Save Cancel
	dockedItems: 
	[
        {
			xtype: 'toolbar',
			dock: 'bottom',
            layout: { pack: 'end', type: 'hbox' },
            items: 
			[
               {
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'fa fa-floppy-o',
                    handler: function () {
                        var wnd = this.up('.window');
						wnd.save();
                    }
                },
				{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'fa fa-ban',
                    handler: function () 
					{										
                        var wnd = this.up('.window');
						wnd.cancel();
                    }
                },
                
            ]
        }
    ],
    

    initComponent: function () 
	{
		this.editRecID = 0;		
			    
		this.callbackRefresh = '';
		
		this.callParent(arguments);
	},
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  afterLoadRecord function
	// default - to be overriden
	//////////////////////////////////////////////////////////////////////////////////////////////
	afterLoadRecord: function(data)
	{
		var me = this;
		var thisf = me.query('form')[0];
		
		thisf.getForm().setValues(data);
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  afterLoadDefault function
	// default - to be overriden
	//////////////////////////////////////////////////////////////////////////////////////////////
	afterLoadDefault: function(data)
	{
		var me = this;
		var thisf = me.query('form')[0];
		
		thisf.getForm().setValues(data);
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  loadData function
	//////////////////////////////////////////////////////////////////////////////////////////////
	loadData: function()
	{								
		var me = this;				
		
		// Check that getLoadRecordParams is defined
		if (typeof (me.getLoadRecordParams) != 'function')
		{
			Ext.Msg.alert('function missing', 'Define function: getLoadRecordParams');
			me.close();			
		}
		// Check that getLoadDefaultParams is defined
		if (typeof (me.getLoadDefaultParams) != 'function')
		{
			Ext.Msg.alert('function missing', 'Define function: getLoadDefaultParams');
			me.close();			
		}
								
		// Load Mask
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();

		var ajaxParams;
		if( me.editRecID <= 0 )
			ajaxParams = me.getLoadDefaultParams();
		else 
			ajaxParams = me.getLoadRecordParams();
		
		Ext.Ajax.request(
		{				
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			params: ajaxParams,			
			timeout: 60000,
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{
					if ( me.editRecID <= 0 ) 
						me.afterLoadDefault(obj.data);
					else 
						me.afterLoadRecord(obj.data);
					
					myMask.hide();
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{
				Ext.Msg.alert('Error', 'Error in retrieving data !');
				myMask.hide();	
				return;				
			}   								
		});
		
		
	}, // End: loadData()
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;

		// Check that getUpdateParams is defined
		if (typeof (me.getUpdateParams) != 'function')
		{
			Ext.Msg.alert('function missing', 'Define function: getUpdateParams');
			me.close();			
		}
		// Check that getInsertParams is defined
		if (typeof (me.getInsertParams) != 'function')
		{
			Ext.Msg.alert('function missing', 'Define function: getInsertParams');
			me.close();			
		}
		
		// Load Mask
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();
		
		var ajaxParams;
		if( me.editRecID <= 0 )
			ajaxParams = me.getInsertParams();
		else 
			ajaxParams = me.getUpdateParams();
		
		Ext.Ajax.request(
		{				
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			params: ajaxParams,			
			timeout: 60000,
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{
					myMask.hide();
					me.close();
					console.log(obj.data);
					me.callbackRefresh(obj.data);
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{				
				myMask.hide();	
				me.close();
				Ext.Msg.alert('Error', 'Error in saving data !');
				return;				
			}   								
		});

    },
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  cancel    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	cancel: function () 
	{	
		 this.close();
    },	

	
	
	
	
	
	
	
	


});



