﻿Ext.require([ 'Home.view.ComboGrid_SelectViewType_ViewTypeList']);

Ext.define('Home.view.ComboGrid_SelectViewType' ,{
    extend: 'Ext.window.Window',
    alias : 'widget.ComboGrid_SelectViewType',
	itemId: 'ComboGrid_SelectViewType',
	
	modal: true,
    height: 400,
    width:  400,
	resizable:false,
	queryMode: 'local',  
	multiSelect: false,

    layout: {
        align: 'stretch',
        type: 'hbox'
    },
	
	title: 'View Type Selection',
	
	items:
	[
		{
			xtype: 'ComboGrid_SelectViewType_ViewTypeList',
			itemid: 'ComboGrid_SelectViewType_ViewTypeList',
			layout: 'border',
			region: 'center',
			split:true,
			width: 1200,
			listeners: 
			{
				itemdblclick: function(dv, record, item, index, e) 
				{
					// find selection - NodeDataType and NodeDataTypeDescr
					var sr = this.getSelectionModel().getSelection();
					if(sr.length > 0)
					{
						var selectedRecord = sr[0];
						var selID = selectedRecord.get('NodeViewType');
						var selName = selectedRecord.get('NodeViewTypeDescr');
						
						// callBack to update form fields
						var wnd = Ext.ComponentQuery.query('#ComboGrid_SelectViewType')[0];
						wnd.callbackAfterSelect(selID, selName);
						// and close selection window
						wnd.close();
					}
				}
			}
		}
	],
	
	initComponent: function () 
	{
		this.SelectedRecordID	= 0;
		this.NodeDataTypeSelector	= 0;
		this.openerComponent	= 0;
		this.callbackAfterSelect = "";
	
		this.callParent(arguments);
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  loadData     function
	//////////////////////////////////////////////////////////////////////////////////////////////
	loadData: function()
	{
		// get direct child of window with xtype = ComboGrid_SelectViewType_ViewTypeList
		var grid = Ext.ComponentQuery.query('#ComboGrid_SelectViewType > ComboGrid_SelectViewType_ViewTypeList')[0];	
		
		// if there is Selection (pre saved NodeID)
		if(this.SelectedRecordID > 0)
			grid.initSelectedRow = this.SelectedRecordID;
			grid.NodeDataTypeSelector = this.NodeDataTypeSelector;
		grid.loadData();
		
	}
	
});





