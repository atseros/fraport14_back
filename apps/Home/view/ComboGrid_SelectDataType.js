﻿Ext.require([ 'Home.view.ComboGrid_SelectDataType_DataTypeList']);

Ext.define('Home.view.ComboGrid_SelectDataType' ,{
    extend: 'Ext.window.Window',
    alias : 'widget.ComboGrid_SelectDataType',
	itemId: 'ComboGrid_SelectDataType',
	
	modal: true,
    height: 800,
    width:  500,
	resizable:true,
	queryMode: 'local',  
	multiSelect: false,


    layout: {
        align: 'stretch',
        type: 'hbox'
    },
	
	title: 'Data Type Selection',
	
	items:
	[
		{
			xtype: 'ComboGrid_SelectDataType_DataTypeList',
			itemid: 'ComboGrid_SelectDataType_DataTypeList',
			layout: 'border',
			region: 'center',
			split:true,
			width: 1200,
			listeners: 
			{
				itemdblclick: function(dv, record, item, index, e) 
				{
					// find selection - NodeDataType and NodeDataTypeDescr
					var sr = this.getSelectionModel().getSelection();
					if(sr.length > 0)
					{
						var selectedRecord = sr[0];
						var selID = selectedRecord.get('CtgID');
						var selName = selectedRecord.get('CtgTitle');
						
						// callBack to update form fields
						var wnd = Ext.ComponentQuery.query('#ComboGrid_SelectDataType')[0];
						wnd.callbackAfterSelect(selID, selName);
						// and close selection window
						wnd.close();
					}
				}
			}
		}
	],
	
	initComponent: function () 
	{
		this.SelectedRecordID	= 0;
		this.openerComponent	= 0;
		this.callbackAfterSelect = "";
		
		this.SiteID = 0;
	
		this.callParent(arguments);
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  loadData     function
	//////////////////////////////////////////////////////////////////////////////////////////////
	loadData: function()
	{
		// get direct child of window with xtype = ComboGrid_SelectDataType_DataTypeList
		var grid = Ext.ComponentQuery.query('#ComboGrid_SelectDataType > ComboGrid_SelectDataType_DataTypeList')[0];	
		
		// if there is Selection (pre saved NodeID)
		if(this.SelectedRecordID > 0)
			grid.initSelectedRow = this.SelectedRecordID;
			grid.SiteID = this.SiteID;
		grid.loadData();
		
	}
	
});





