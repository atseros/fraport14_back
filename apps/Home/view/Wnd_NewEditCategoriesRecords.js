﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditCategoriesRecords', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditCategoriesRecords',
		
	layout: 'border',
	title: 'Category Record Data',
    height: 320,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[
				// Hidden field:  CtgRecID
				{
					xtype: 'hiddenfield',
					name: 'CtgRecID',
				},
				// Hidden field:  CtgRecCtgID
				{
					xtype: 'hiddenfield',
					name: 'CtgRecCtgID',
				},
				//Image
				{
					xtype: 'filefield',
					name: 'File',
					fieldLabel: 'Image',
					border: 0,
					layout: 'border',
					region :'north',
									
				},
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},
				// Combo field: CtgRecStatus
				{
					xtype: 'combo',    
					name: 'CtgRecStatus',
					forceSelection: true,
					fieldLabel: 'Status',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},					
				// Text field:  CtgRecPriority
				{
					xtype: 'numberfield',
					name: 'CtgRecPriority',
					fieldLabel: 'Priority',
					border: 0,
					layout: 'border',
					value : 100,
				},
				// Text field:  CtgRecTitle
				{
					xtype: 'textfield',
					name: 'CtgRecTitle',
					fieldLabel: 'Title',
				},
				// Text field:  CtgRecEscapeDefault
				{
					xtype: 'textfield',
					name: 'CtgRecEscapeDefault',
					fieldLabel: 'Escape Default',
				},
				// Combo field: CtgRecTimer
				{
					xtype: 'combo',    
					name: 'CtgRecTimer',
					forceSelection: true,
					fieldLabel: 'Timer',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Yes'},
							{ id: 2, Descr: 'No'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},		
				{
					xtype: 'container',
					region :'north',
					layout: 'hbox',
					items: 
					[
						// CtgRecStartDate
						{
							xtype: 'datefield',
							name: 'CtgRecStartDate',
							fieldLabel: 'Date From',
							format:'d-m-Y',
							editable: false,
							flex:1,
						},
						// CtgRecStartTime
						{
							xtype: 'timefield',
							name: 'CtgRecStartTime',
							fieldLabel: 'Time From',
							format: 'H:i',
							altFormats:'H:i',
							editable: false,
							flex:1,
							labelAlign : 'right',
						},
					]
				},
				{
					xtype: 'container',
					region :'north',
					layout: 'hbox',
					items: 
					[
						// CtgRecEndDate
						{
							xtype: 'datefield',
							name: 'CtgRecEndDate',
							fieldLabel: 'Date To',
							format:'d-m-Y',
							editable: false,
							flex:1,
						},
						// CtgRecEndTime
						{
							xtype: 'timefield',
							name: 'CtgRecEndTime',
							fieldLabel: 'Time To',
							format: 'H:i',
							altFormats:'H:i',
							editable: false,
							flex:1,
							labelAlign : 'right',
						},
					]
				},				
				
				
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.CtgID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod	: 'get_CategoriesRecords_NewRecordDefValues',
			CtgID			:this.CtgID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage			: 'Home',
			appMethod		: 'get_CategoriesRecordsRecord',
			CtgRecID	 		: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;
		
		me.DataAndImage();
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* DataAndImage
	*
	* function that uploades Images and add data
	*
	***************************************************************************************************/
	DataAndImage: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();
		
		
		if (me.editRecID == 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=InsertCategoriesRecordsRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been added.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not add.');                    
					}
				});
			}
			
		}
		else	if (me.editRecID > 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=UpdateCategoriesRecordsRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been updated.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not update.');                    
					}
				});
			}
			
		}			

        
	},
	
});