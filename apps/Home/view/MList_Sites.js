﻿// Require Popup Form of Sites (Wnd for New - Edit)
Ext.require([ 'Home.view.Wnd_NewEditSites']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Sites' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Sites',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	// dockedItems: 
	// [
	
		// // Toolbar  : Add New, Edit, Delete
		// {
			// xtype: 'toolbar',
			// dock: 'top',
			// layout: { type: 'hbox' },
			// items: 
			// [	
				// {
					// xtype: 'tbfill'
				// },
				// {
					// xtype: 'button',
					// text: 'Add',
					// iconCls: 'fa fa-plus',
					// handler: function() 
					// {
						// var thisgrid = this.up('panel');
						// thisgrid.addRecord();
					// }					
				// },															
				// {
					// xtype: 'button',
					// text: 'Edit',
					// iconCls: 'fa fa-pencil-square-o',
					// handler: function() 
					// { 
						// var thisgrid = this.up('panel');
						// thisgrid.editRecord();
					// }					
				// },
				// {
					// xtype: 'button',
					// text: 'Delete',
					// iconCls: 'fa fa-trash',
					// handler: function() 
					// { 
						// var thisgrid = this.up('panel');
						// thisgrid.deleteRecord();
					// }					
				// }
			// ]
		// },
	// ],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind = '';
		this.filterShowAll = 0;		
		this.maxRecords = 100;
				
		this.fieldNameID = 'SiteID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('SiteID') > 1 ? 'expanded_row' : 'expanded_row';
			}					
		};		
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'SiteID', type: 'number', defaultValue: 0 }
				,{name: 'SiteDomainName', type: 'string'}
				,{name: 'SiteUrlTitle', type: 'string'}
				,{name: 'SiteAirportCode', type: 'string'}
				
			],			
			proxy: 
			{
				type: 'ajax',
				url: '', 
				method : 'POST',
				timeout : 60000,						
				headers : { 'Content-Type' : 'application/json' },
				reader: {
					type: 'json',
					root: 'data'
				},
				simpleSortMode: true,				
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: true,
			},
			items: 
			[
				{header: ' Domain', dataIndex: 'SiteDomainName',  flex: 50/100}
				// ,{header: 'URL Title', dataIndex: 'SiteUrlTitle',  flex: 30/100}
				// ,{header: 'Airport Code', dataIndex: 'SiteAirportCode',  flex: 20/100}
				
			]
		};
		
		// Double Click - Open form for Edit
		//this.on( 'itemdblclick', this.editRecord, this) ;

		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Home',
			appMethod: 'get_SiteList',
			maxRecords: me.maxRecords,

		};
		
		return params;
	},
	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditSites; 		
		x.editRecID = 0;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
			
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditSites; 
			x.editRecID = RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();		
		
		if(RecID > 0)
		{
			params = 
			{
				appPage		: 'Home',
				appMethod	: 'DeleteSiteRecord',
				SiteID		: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
		
	},
	
	
	
	
		
	
	 
	
});





