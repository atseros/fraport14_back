﻿// // Require Popup Form of Categories Records (Wnd for New Record)
// Ext.require([ 'Home.view.Wnd_NewEditCategoriesRecords']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Categories_Records_Sliders' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Categories_Records_Sliders',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					xtype: 'button',
					text: 'Show All',
					itemId: '',
					iconCls: 'fa fa-filter',
					enableToggle: true,
					pressed: false,
					handler: function(button) 
					{ 
						var gridpanel = this.up('panel'); 
						
						if(!button.pressed)
							gridpanel.filterShowAll = 0;
						else 
							gridpanel.filterShowAll = 1;
						gridpanel.loadData();	
					}					
				},
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				250 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 5'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				}
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterShowAll	= 0;	
		this.filterType = -1;		
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'CtgRecID';
		
		this.CtgRecID = 0;
		this.SliderID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				// return value.get('CtgRecID') > 1 ? 'expanded_row' : 'expanded_row';
				return value.get('RowHighlight') == 1 ? 'expanded_row highlighted' : 'expanded_row ';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'CtgRecID', type: 'number', defaultValue: 0 }
				,{name: 'CtgRecCtgID', type: 'number', defaultValue: 0 }
				,{name: 'CtgRecPriority', type: 'number', defaultValue: 0 }
				,{name: 'CtgRecTitle', type: 'string' }
				,{name: 'CtgRecFilename', type: 'string' }
				,{name: 'CtgRecFullUrl', type: 'string'}
				,{name: 'CtgRecStatus', type: 'number', defaultValue: 0 }
				,{name: 'RowHighlight', type: 'integer'}
				
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Status', dataIndex: 'CtgRecStatus',  width: 40, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 2) 
						{
							return '<i class="fa fa-times" aria-hidden="true" style="font-size:16px;color:red"></i>';
						}
						else 
						{
							return '<i class="fa fa-check" aria-hidden="true" style="font-size:16px;color:green"></i>';
						}
							
					}
				},	
				{header: 'Image', dataIndex: 'CtgRecFullUrl',  width: 50, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="26" width="26" /></a>';
				
					}
				 },				
				{ header: 'Priority', dataIndex: 'CtgRecPriority', width: 50,},
				{ header: 'Title', dataIndex: 'CtgRecTitle', flex: 20/100,},		
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getCategoriesRecordsList',
			filterStrToFind		: me.filterStrToFind,
			filterShowAll		: me.filterShowAll,
			CtgID				: me.CtgID,	
			SliderID			: me.SliderID,	
		};
		
		return params;
	},
});