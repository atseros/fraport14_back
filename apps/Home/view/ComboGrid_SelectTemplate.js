﻿Ext.require([ 'Home.view.ComboGrid_SelectTemplate_TemplateList']);

Ext.define('Home.view.ComboGrid_SelectTemplate' ,{
    extend: 'Ext.window.Window',
    alias : 'widget.ComboGrid_SelectTemplate',
	itemId: 'ComboGrid_SelectTemplate',
	
	modal: true,
    height: 600,
    width:  1000,
	resizable:false,
	queryMode: 'local',  
	multiSelect: false,

    layout: {
        align: 'stretch',
        type: 'hbox'
    },
	
	title: 'Template Selection',
	
	items:
	[
		{
			xtype: 'ComboGrid_SelectTemplate_TemplateList',
			itemid: 'ComboGrid_SelectTemplate_TemplateList',
			layout: 'border',
			region: 'center',
			split:true,
			width: 1000,
			listeners: 
			{
				itemdblclick: function(dv, record, item, index, e) 
				{
					// find selection - SclID and SclName
					var sr = this.getSelectionModel().getSelection();
					if(sr.length > 0)
					{
						var selectedRecord = sr[0];
						var selName = selectedRecord.get('TplName');
						
						// callBack to update form fields
						var wnd = Ext.ComponentQuery.query('#ComboGrid_SelectTemplate')[0];
						wnd.callbackAfterSelect(selName);
						// and close selection window
						wnd.close();
					}
				}
			}
		}
	],
	
	initComponent: function () 
	{
		this.SelectedRecordID	= 0;
		this.openerComponent	= 0;
		this.callbackAfterSelect = "";
	
		this.callParent(arguments);
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  loadData     function
	//////////////////////////////////////////////////////////////////////////////////////////////
	loadData: function()
	{
		// get direct child of window with xtype = ComboGrid_SelectTemplate_TemplateList
		var grid = Ext.ComponentQuery.query('#ComboGrid_SelectTemplate > ComboGrid_SelectTemplate_TemplateList')[0];	
		
		// if there is Selection (pre saved AppID)
		if(this.SelectedRecordID > 0)
			grid.initSelectedRow = this.SelectedRecordID;
		
		grid.loadData();
		
	}
	
});





