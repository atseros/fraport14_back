﻿// // Require Popup Form of Related Links (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditRelatedLinks']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Related_Links' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Related_Links',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				250 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 0'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				},
				{
					xtype: 'combo',    
					fieldLabel: '',														
					width:150,
					value:"-1",
					margin: '0 0 0 5',
					store:
					{
						fields: 
						[
							{name: 'id', type: 'string'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: -1, Descr: 'All'}
							,{ id: 1, Descr: 'With Image'}
							,{ id: 2, Descr: 'Without Image'}
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr',
					listeners:
					{
						'change': function(field, newValue)
						{
							var gridpanel = this.up('panel'); 
							gridpanel.filterType = newValue;
							gridpanel.loadData();
						}
					}
				}
				,{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},		
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				},
				{
					 xtype:		'button'
					,text:		'Add Link To Node'
					,iconCls: 	'fa fa-arrow-right'
					,margin:		'0 0 0 10'
					,handler:	function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.AddLinkToNode();
					}				
				},	
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterType = -1;	
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'RelatedLinkID';
		
		this.SiteID = 0;
		
		this.NodeID = 0;
		this.NodeTitle = "";
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('RelatedLinkID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'RelatedLinkID', type: 'number', defaultValue: 0 }
				,{name: 'RelatedLinkSiteID', type: 'number', defaultValue: 0 }
				,{name: 'RelatedLinkTitle', type: 'string' }
				,{name: 'RelatedLinkFIlename', type: 'string' }
				,{name: 'RelatedLinkType', type: 'number', defaultValue: 0 }
				,{name: 'RelatedLinkFullUrl', type: 'string'}
				,{name: 'NodeTitle', type: 'string'}
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Image', dataIndex: 'RelatedLinkFullUrl',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
						if (value != "") 
						{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
						}	
						else
						{
							return '';
						}
					}
				 },				
				{ header: 'Title', dataIndex: 'RelatedLinkTitle', flex: 75/100,},
				{header: 'Type', dataIndex: 'RelatedLinkType',  width: 70, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 1) 
						{
							return 'With Image';
						}
						else 
						{
							return 'Without Image';
						}
							
					}
				},
				{ header: 'Link', dataIndex: 'NodeTitle', flex: 35/100,},
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getRelatedLinkList',
			maxRecords		: me.maxRecords,
			filterStrToFind		: me.filterStrToFind,
			SiteID				: me.SiteID,
			filterType			: me.filterType,	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditRelatedLinks; 		
		x.editRecID = 0;
		x.SiteID = thisgrid.SiteID;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditRelatedLinks; 
			x.editRecID 	= RecID;
			x.SiteID = thisgrid.SiteID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); Ext.ComponentManager.get('MList_Node_RelatedLinks').loadData();};  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteRelatedLinkRecord'
				,RelatedLinkID		: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
								Ext.ComponentManager.get('MList_Node_RelatedLinks').loadData();					
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
	
	/**************************************************************************************************
	* AddLinkToNode
	*
	* Button handler for AddLinkToNode
	*
	***************************************************************************************************/
	AddLinkToNode: function()
	{
		var thisgrid = this;
		
		var NodeID  	= thisgrid.NodeID;
		var RelatedLinkID 	= thisgrid.getSelectedRecordID();

		params = 
		{
			appPage		: 'Home',
			appMethod		: 'AddLinkToNode',
			NodeRelatedLinkNodeID	: NodeID,
			NodeRelatedLinkRelatedLinkID	: RelatedLinkID,
			
		};
		
		
		Ext.Msg.confirm('Confirmation', 'Add Link to '+thisgrid.NodeTitle+' ?', 
		function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Ajax.request({
						url: 'appAjax.php',
						method: 'post',
						params:  params,
						success: function(response)
						{
							var text = response.responseText;
							var obj = Ext.JSON.decode(text);
							if (!obj.data) 
							{										
								Ext.Msg.alert('Not added!', obj.error);
								return;
							}
							//Ext.ComponentManager.get('MList_Nodes_Media').loadData();
							Ext.ComponentManager.get('MList_Node_RelatedLinks').loadData();	
						}	
					});
					return true;
				} 
				else 
				{
					return false;
				}
			}
		);	
		
	},
});