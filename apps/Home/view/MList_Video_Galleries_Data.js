﻿// // Require Popup Form of Video Gallery Data (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditVideoGalleryData']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Video_Galleries_Data' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Video_Galleries_Data',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					xtype: 'button',
					text: 'Show All',
					itemId: '',
					iconCls: 'fa fa-filter',
					enableToggle: true,
					pressed: false,
					handler: function(button) 
					{ 
						var gridpanel = this.up('panel'); 
						
						if(!button.pressed)
							gridpanel.filterShowAll = 0;
						else 
							gridpanel.filterShowAll = 1;
						gridpanel.loadData();	
					}					
				},
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				200 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 5'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				},
				{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},		
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				}
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterShowAll	= 0;	
		this.fieldNameID = 'VidDataID';
		
		this.VidID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('VidDataID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'VidDataID', type: 'number', defaultValue: 0 }
				,{name: 'VidDataVidID', type: 'number', defaultValue: 0 }
				,{name: 'VidDataTitle', type: 'string' }
				,{name: 'VidDataPriority', type: 'number', defaultValue: 0 }
				,{name: 'VidDataStatus', type: 'number', defaultValue: 0 }
				,{name: 'VidDataFilename', type: 'string' }
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Status', dataIndex: 'VidDataStatus',  width: 40, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 2) 
						{
							return '<i class="fa fa-times" aria-hidden="true" style="font-size:16px;color:red"></i>';
						}
						else 
						{
							return '<i class="fa fa-check" aria-hidden="true" style="font-size:16px;color:green"></i>';
						}
							
					}
				},
				{header: 'Thumb', dataIndex: 'VidDataFilename',  width:50, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="http://img.youtube.com/vi/'+value+'/default.jpg" target="_blank"><img src="http://img.youtube.com/vi/'+value+'/default.jpg" height="32" width="32" /></a>';
				
					}
				},				
				{ header: 'Priority', dataIndex: 'VidDataPriority', width: 50,},
				{ header: 'Title', dataIndex: 'VidDataTitle', flex: 20/100,},
				{header: 'Link', dataIndex: 'VidDataFilename',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="https://www.youtube.com/watch?v='+value+'" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size:24px;color:#2f3070;"></i></a>';
				
					}
				 },
				
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getVideoGalleryDataList',
			filterStrToFind		: me.filterStrToFind,
			filterShowAll		: me.filterShowAll,
			VidID					: me.VidID,	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditVideoGalleryData; 		
		x.editRecID = 0;
		x.VidID = thisgrid.VidID;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditVideoGalleryData; 
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteVideoGalleryDataRecord'
				,VidDataID		: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
});