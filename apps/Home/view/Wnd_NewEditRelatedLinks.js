﻿// // Require Combo of Nodes 
Ext.require([ 'Home.view.ComboGrid_SelectNode']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditRelatedLinks', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditRelatedLinks',
		
	layout: 'border',
	title: 'Media Data',
    height: 210,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 60,
				region :'north',
           },
			items: 
			[
				// Hidden field:  RelatedLinkID
				{
					xtype: 'hiddenfield',
					name: 'RelatedLinkID',
				},
				// Hidden field:  RelatedLinkSiteID
				{
					xtype: 'hiddenfield',
					name: 'RelatedLinkSiteID',
				},
				// Combo field: RelatedLinkType
				{
					xtype: 'combo',    
					name: 'RelatedLinkType',
					forceSelection: true,
					fieldLabel: 'Type',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'With Image'},
							{ id: 2, Descr: 'Without Image'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr',
					listeners: 
					{
						select: function(combo) 
						{
							var form = combo.up('form');
							var File = form.getForm().findField("File");
							
							if( combo.getValue() == 2)
							{
								File.hide();
								File.reset();
							}
							else
							{
								File.show();
							}
						}
					}
				},	
				//File
				{
					xtype: 'filefield',
					name: 'File',
					fieldLabel: 'File',
					border: 0,
					layout: 'border',
					region :'north',
					regex: /^.*\.(jpg|JPG|gif|GIF|png|PNG|bmp|BMP)$/,
					regexText: ''
				},
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},			
				// Text field:  RelatedLinkTitle
				{
					xtype: 'textfield',
					name: 'RelatedLinkTitle',
					fieldLabel: 'Title',
				},
				// Combo field:  RelatedLinkNodeID
				{
					xtype: 'container',
					region :'north',
					layout: { type: 'hbox' },
					items:
					[
							{
								xtype: 'hiddenfield',
								fieldLabel: 'RelatedLinkNodeID',
								name: 'RelatedLinkNodeID',
								itemId: 'RelatedLinkNodeID',
								value: 0
							},  
							{
								xtype: 'textfield',
								fieldLabel: 'Link',
								name: 'NodeTitle',
								itemId: 'NodeTitle',                    
								flex: 19,
								labelWidth: 60,
								allowBlank: true,
								readOnly: true,
								maxLength: 255,
								cls: 'readonly_combos',

							},
							{
								xtype: 'button',
								text: '.',
								itemId: 'btnSelect',	
								width: 18,											
								handler: function() 
								{
									var x = new Home.view.ComboGrid_SelectNode;
									var thisgrid = this.up('form').up('panel');
									x.SiteID = thisgrid.SiteID;					
									x.show(this);
									// if in Edit Mode, set already saved RelatedLinkNodeID
									var thisform = this.up('form');
									x.SelectedRecordID = thisform.getValues().RelatedLinkNodeID;
									
									x.callbackAfterSelect = function(id, name)
															{ 
																thisform.getForm().findField('RelatedLinkNodeID').setValue(id);
																thisform.getForm().findField('NodeTitle').setValue(name);
															};
									x.loadData();
								}					
							},
								
					]
				},	
				
											
				
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.SiteID = 0;	
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_RelatedLink_NewRecordDefValues',
			SiteID			:this.SiteID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
		
		var form = this.query('form')[0];
			
		var values = form.getValues();
	
		
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_RelatedLinkRecord',
			RelatedLinkID			: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		var form = this.query('form')[0];
			
		var values = form.getValues();
		
		var File = this.query("filefield[name=File]")[0];
		
		if (values.RelatedLinkType == 1)
		{
			File.show();
		}
		else if (values.RelatedLinkType == 2)
		{
			File.hide();
			File.reset();
		}
		
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;
		
		me.DataAndImage();
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* DataAndImage
	*
	* function that uploades Images and add data
	*
	***************************************************************************************************/
	DataAndImage: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();
		
		
		if (me.editRecID == 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=InsertRelatedLinkRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been added.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not add.');                    
					}
				});
			}
			
		}
		else	if (me.editRecID > 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=UpdateRelatedLinkRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been updated.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not update.');                    
					}
				});
			}
			
		}			

        
	},
	
});