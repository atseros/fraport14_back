﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditVideoGalleryData', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditVideoGalleryData',
		
	layout: 'border',
	title: 'Video Gallery Data',
    height: 210,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[
				// Hidden field:  VidDataID
				{
					xtype: 'hiddenfield',
					name: 'VidDataID',
				},
				// Hidden field:  VidDataVidID
				{
					xtype: 'hiddenfield',
					name: 'VidDataVidID',
				},
				// Text field:  VidDataFilename
				{
					xtype: 'textfield',
					name: 'VidDataFilename',
					fieldLabel: 'URL',
					emptyText : 'Youtube url ie https://www.youtube.com/watch?v=UvI7YhTSQNs',
					allowBlank: false
				},	
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},
				// Combo field: VidDataStatus
				{
					xtype: 'combo',    
					name: 'VidDataStatus',
					forceSelection: true,
					fieldLabel: 'Status',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},				
				// Text field:  VidDataPriority
				{
					xtype: 'numberfield',
					name: 'VidDataPriority',
					fieldLabel: 'Priority',
					border: 0,
					layout: 'border',
					value : 100,
				},
				// Text field:  VidDataTitle
				{
					xtype: 'textfield',
					name: 'VidDataTitle',
					fieldLabel: 'Title',
				},									
				
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.VidID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_VideoGalleryData_NewRecordDefValues',
			VidID				:this.VidID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_VideoGalleryDataRecord',
			VidDataID		: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;
		
		me.DataAndImage();
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* DataAndImage
	*
	* function that uploades Images and add data
	*
	***************************************************************************************************/
	DataAndImage: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();
		
		
		if (me.editRecID == 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=InsertVideoGalleryDataRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been added.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not add.');                    
					}
				});
			}
			
		}
		else	if (me.editRecID > 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=UpdateVideoGalleryDataRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been updated.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not update.');                    
					}
				});
			}
			
		}			

        
	},
	
});