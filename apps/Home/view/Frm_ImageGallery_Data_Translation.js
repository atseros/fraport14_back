﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.FormTemplate']);

Ext.define('Home.view.Frm_ImageGallery_Data_Translation' ,{
    extend: 'Include.view.FormTemplate',
    alias : 'widget.Frm_ImageGallery_Data_Translation',
	
	autoScroll: false,                
	layout: 'border',
	
	
	
	/**************************************************************************************************
	* Items
	***************************************************************************************************/
	items: 
	[
        {
            xtype: 'form',
			//frame: true,
			layout: 'border',
			margin: '5 5 5 5',
			border:0,
			region: 'center',
            items: 
			[	
				{
					xtype: 'hiddenfield',
					name: 'ImgDataLngID',
				},
				{
					
					xtype: 'radiogroup',    
					fieldLabel: 'Language Selection',
					labelWidth: 150,
					layout: 'hbox',
					region: 'north',
					defaults: 
					{
						flex: 1
					},
					items: 
					[
						{
							xtype: 'radiogroup',
							columns: 4,
							items: 
							[
								{
									xtype: 'radiofield',
									name: 'ImgDataLngType',
									boxLabel: 'English',
									checked: true,
									inputValue: 1
								},
								{
									xtype: 'radiofield',
									name: 'ImgDataLngType',
									boxLabel: 'Greek',
									inputValue: 2
								},
								{
									xtype: 'radiofield',
									name: 'ImgDataLngType',
									boxLabel: 'Deutch',
									inputValue: 3
								},
								{
									xtype: 'radiofield',
									name: 'ImgDataLngType',
									boxLabel: 'Russian',
									inputValue: 4
								},
								
							],
							listeners: 
							{
								change : function(obj, value)
								{ 
									var formPanel = this.up('form').up('panel');
									
									var val = value.ImgDataLngType;              
									formPanel.ImgDataLngType = val;
									formPanel.loadData();
								}
							}
							
						}
					]
					
				},
				{
					xtype: 'panel',
					border: 0,
					layout: 'border',
					region: 'center',
					items:
					[	
						// Line Separator
						{
							xtype: 'container',
							html: '<hr>',
							height:10
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '20 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Title',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  ImgDataLngTitle
						{
							xtype: 'textfield',
							name: 'ImgDataLngTitle',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '5 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Subtitle',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  ImgDataLngSubtitle
						{
							xtype: 'textfield',
							name: 'ImgDataLngSubtitle',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '5 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Description',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  ImgDataLngDescription
						{
							xtype: 'textfield',
							name: 'ImgDataLngDescription',
							border: 0,
							layout: 'border',
							region :'north',
						},
						
					]
				},
				{
					xtype: 'container',
					layout: 'hbox',
					region: 'south',
					margin: '5 0 0 0',
					height: 30,
					items:
					[
						
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'fa fa-floppy-o',
							margin: '0 0 0 0',
							text: 'Save',
							handler: function() 
							{
								var formPanel = this.up('form').up('panel');
								formPanel.Save();
							}
						}
					]
				},
				
			]	
        }       
    ],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.ImgDataID = 0;		
		
		this.ImgDataLngID = 0;		
		
		this.ImgDataLngType = 1;	
				
		this.fieldNameID = 'ImgDataLngID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				 
			}					
		};						

		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Home',
			appMethod: 'get_ImageGalleryDataLngRecord',
			ImgDataLngImgDataID: me.ImgDataID,
			ImgDataLngType: me.ImgDataLngType,
			
		};
		
		return params;
	},
	
	
	/**************************************************************************************************
	* afterLoadDefault
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		var me = this;
		var thisf = me.query('form')[0];
		
		thisf.getForm().setValues(data);
		
		// var id = thisf.getForm().getValues().ShopLngID;
		// var tinymce_editor1 = this.query('my_tinymce_textarea[name=ShopLngText]')[0];
		// tinymce_editor1.submitUrl='appAjax.php?appPage=Home&appMethod=handleTinyMCE_UploadImage&table_name=sys_shoplng&id=' + id;

	},
	
	/**************************************************************************************************
	* Save
	***************************************************************************************************/
	Save: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var me = this;
		
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();
		
		// Update Current Record 
		Ext.Ajax.request({
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			timeout: 60000,
			params: 
			{
				appPage		: 'Home',
				appMethod		: 'UpdateImageGalleryDataLngRecord',
				ImgDataLngID				: values.ImgDataLngID,
				ImgDataLngType				: this.ImgDataLngType,					
				ImgDataLngImgDataID	: this.ImgDataID,
				ImgDataLngTitle				: values.ImgDataLngTitle,					
				ImgDataLngSubtitle			: values.ImgDataLngSubtitle,					
				ImgDataLngDescription	: values.ImgDataLngDescription,					
			
			},
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{
					myMask.hide();
					//me.close();
					console.log(obj.data);
					//me.callbackRefresh(obj.data);
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				//me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{				
				myMask.hide();	
				//me.close();
				Ext.Msg.alert('Error', 'Error in saving data !');
				return;				
			}   		
		});
	},
	

	
});





