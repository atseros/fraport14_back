﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditAlert', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditAlert',
		
	layout: 'border',
	title: 'Alert Data',
    height: 250,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[
				// Hidden field:  AlertID
				{
					xtype: 'hiddenfield',
					name: 'AlertID',
					fieldLabel: 'AlertID',
				},
				// Hidden field:  AlertSiteID
				{
					xtype: 'hiddenfield',
					name: 'AlertSiteID',
					fieldLabel: 'AlertSiteID',
				},
				// Combo field: AlertStatus
				{
					xtype: 'combo',    
					name: 'AlertStatus',
					forceSelection: true,
					fieldLabel: 'Status',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},				
				// Text field:  AlertPriority
				{
					xtype: 'numberfield',
					name: 'AlertPriority',
					fieldLabel: 'Priority',
					border: 0,
					layout: 'border',
					value : 100,
				},
				// Combo field: AlertType
				{
					xtype: 'combo',    
					name: 'AlertType',
					forceSelection: true,
					fieldLabel: 'Type',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Default'},
							// { id: 2, Descr: 'Webover'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},	
				// Text field:  AlertTitle
				{
					xtype: 'textfield',
					name: 'AlertTitle',
					fieldLabel: 'Title',
				},				
				{
					xtype: 'container',
					region :'north',
					layout: 'hbox',
					items: 
					[
						// AlertStartDate
						{
							xtype: 'datefield',
							name: 'AlertStartDate',
							fieldLabel: 'Date From',
							format:'d-m-Y',
							editable: false,
							flex:1,
						},
						// AlertStartTime
						{
							xtype: 'timefield',
							name: 'AlertStartTime',
							fieldLabel: 'Time From',
							format: 'H:i',
							altFormats:'H:i',
							editable: false,
							flex:1,
							labelAlign : 'right',
						},
					]
				},
				{
					xtype: 'container',
					region :'north',
					layout: 'hbox',
					items: 
					[
						// AlertEndDate
						{
							xtype: 'datefield',
							name: 'AlertEndDate',
							fieldLabel: 'Date To',
							format:'d-m-Y',
							editable: false,
							flex:1,
						},
						// AlertEndTime
						{
							xtype: 'timefield',
							name: 'AlertEndTime',
							fieldLabel: 'Time To',
							format: 'H:i',
							altFormats:'H:i',
							editable: false,
							flex:1,
							labelAlign : 'right',
						},
					]
				},
								
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.SiteID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_Alert_NewRecordDefValues',
			SiteID			:this.SiteID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_AlertRecord',
			AlertID			: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'InsertAlertRecord',
			AlertID:						values.AlertID,
			AlertSiteID:				values.AlertSiteID,
			AlertStatus:				values.AlertStatus,
			AlertPriority:				values.AlertPriority,
			AlertType:					values.AlertType,
			AlertTitle:					values.AlertTitle,
			AlertStartDate:			values.AlertStartDate,
			AlertStartTime:			values.AlertStartTime,
			AlertEndDate:				values.AlertEndDate,
			AlertEndTime:				values.AlertEndTime,
			
		};

		return params;
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'UpdateAlertRecord',
			AlertID:						values.AlertID,
			AlertSiteID:				values.AlertSiteID,
			AlertStatus:				values.AlertStatus,
			AlertPriority:				values.AlertPriority,
			AlertType:					values.AlertType,
			AlertTitle:					values.AlertTitle,
			AlertStartDate:			values.AlertStartDate,
			AlertStartTime:			values.AlertStartTime,
			AlertEndDate:				values.AlertEndDate,
			AlertEndTime:				values.AlertEndTime,

		};
		
		return params;
		
	},
	
});