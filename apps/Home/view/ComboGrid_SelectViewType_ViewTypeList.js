﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.ComboGrid_SelectViewType_ViewTypeList' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.ComboGrid_SelectViewType_ViewTypeList',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterLanguage 	= 1;		
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'NodeID';
		
		this.NodeDataTypeSelector = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				//return value.get('NodeID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'NodeViewType', type: 'number', defaultValue: 0 }
				,{name: 'NodeViewTypeDescr', type: 'string' }
					
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'View Type', dataIndex: 'NodeViewTypeDescr',  flex: 100/100, resizable: false, align:'left'}
				
			]
		};
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Home',
			appMethod: 'getNodeViewTypeList',
			NodeDataTypeSelector: me.NodeDataTypeSelector,
		};
		
		return params;
	},
	
	

	
});





