﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.FormTemplate']);

Ext.define('Home.view.Frm_Slider_Images_Translation' ,{
    extend: 'Include.view.FormTemplate',
    alias : 'widget.Frm_Slider_Images_Translation',
	
	autoScroll: false,                
	layout: 'border',
	
	
	
	/**************************************************************************************************
	* Items
	***************************************************************************************************/
	items: 
	[
        {
            xtype: 'form',
			//frame: true,
			layout: 'border',
			margin: '5 5 5 5',
			border:0,
			region: 'center',
            items: 
			[	
				{
					xtype: 'hiddenfield',
					name: 'SliderImagesLngID',
				},
				{
					
					xtype: 'radiogroup',    
					fieldLabel: 'Language Selection',
					labelWidth: 150,
					layout: 'hbox',
					region: 'north',
					defaults: 
					{
						flex: 1
					},
					items: 
					[
						{
							xtype: 'radiogroup',
							columns: 4,
							items: 
							[
								{
									xtype: 'radiofield',
									name: 'SliderImagesLngType',
									boxLabel: 'English',
									checked: true,
									inputValue: 1
								},
								{
									xtype: 'radiofield',
									name: 'SliderImagesLngType',
									boxLabel: 'Greek',
									inputValue: 2
								},
								{
									xtype: 'radiofield',
									name: 'SliderImagesLngType',
									boxLabel: 'Deutch',
									inputValue: 3
								},
								{
									xtype: 'radiofield',
									name: 'SliderImagesLngType',
									boxLabel: 'Russian',
									inputValue: 4
								},
								
							],
							listeners: 
							{
								change : function(obj, value)
								{ 
									var formPanel = this.up('form').up('panel');
									
									var val = value.SliderImagesLngType;              
									formPanel.SliderImagesLngType = val;
									formPanel.loadData();
								}
							}
							
						}
					]
					
				},
				{
					xtype: 'panel',
					border: 0,
					layout: 'border',
					region: 'center',
					items:
					[	
						// Line Separator
						{
							xtype: 'container',
							html: '<hr>',
							height:10
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '10 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Title',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SliderImagesLngTitle
						{
							xtype: 'textfield',
							name: 'SliderImagesLngTitle',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '5 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Subtitle',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SliderImagesLngSubtitle
						{
							xtype: 'textfield',
							name: 'SliderImagesLngSubtitle',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '5 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Link title',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SliderImagesLngLinkTitle
						{
							xtype: 'textfield',
							name: 'SliderImagesLngLinkTitle',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '5 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'External link',
									cls: 'boldtitle'
								},
							]
						},
                        {
                            xtype: 'container',
                            layout: {type: 'hbox'},
                            region: 'north',
                            items:
							[
                            	// Text field:  SliderImagesLngExternalLink
                                {
                                	xtype: 'textfield',
									name: 'SliderImagesLngExternalLink',
									border: 0,
									layout: 'border',
									region: 'center',
									flex: 9,
                                },
                                {
                                    xtype: 'tbfill'
                                },
                                // Combo field: SliderImagesLngExternalLinkTarget
                                {
                                    xtype: 'combo',
                                    name: 'SliderImagesLngExternalLinkTarget',
                                    forceSelection: true,
                                    fieldLabel: 'Target',
                                    cls: 'readonly_combos',
                                    editable: false,
                                    labelWidth: 40,
                                    labelAlign: 'right',
									region: 'east',
									flex: 2,
                                    store:
                                    {
                                        fields:
                                        [
                                            {name: 'id', type: 'int'},
                                            {name: 'Descr', type: 'string'}
                                        ],
                                        data:
                                        [
                                            {id: 1, Descr: 'Yes'},
                                            {id: 0, Descr: 'No'},
                                        ],
                                        autoLoad: true
                                    },
                                    queryMode: 'local',
                                    valueField: 'id',
                                    displayField: 'Descr',
                                }
                            ],
                        }
					]
				},
				{
					xtype: 'container',
					layout: 'hbox',
					region: 'south',
					margin: '5 0 0 0',
					height: 30,
					items:
					[
						
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'fa fa-floppy-o',
							margin: '0 0 0 0',
							text: 'Save',
							handler: function() 
							{
								var formPanel = this.up('form').up('panel');
								formPanel.Save();
							}
						}
					]
				},
				
			]	
        }       
    ],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.SliderImagesID = 0;		
		
		this.SliderImagesLngID = 0;		
		
		this.SliderImagesLngType = 1;	
				
		this.fieldNameID = 'SliderImagesLngID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				 
			}					
		};						

		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Home',
			appMethod: 'get_SliderImagesLngRecord',
			SliderImagesLngSliderImagesID: me.SliderImagesID,
			SliderImagesLngType: me.SliderImagesLngType,
			
		};
		
		return params;
	},
	
	
	/**************************************************************************************************
	* afterLoadDefault
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		var me = this;
		var thisf = me.query('form')[0];
		
		thisf.getForm().setValues(data);
		
		// var id = thisf.getForm().getValues().ShopLngID;
		// var tinymce_editor1 = this.query('my_tinymce_textarea[name=ShopLngText]')[0];
		// tinymce_editor1.submitUrl='appAjax.php?appPage=Home&appMethod=handleTinyMCE_UploadImage&table_name=sys_shoplng&id=' + id;

	},
	
	/**************************************************************************************************
	* Save
	***************************************************************************************************/
	Save: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var me = this;
		
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();
		
		// Update Current Record 
		Ext.Ajax.request({
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			timeout: 60000,
			params: 
			{
				appPage				: 'Home',
				appMethod				: 'UpdateSliderImagesLngRecord',
				SliderImagesLngID			: values.SliderImagesLngID,
				SliderImagesLngType			: this.SliderImagesLngType,					
				SliderImagesLngSliderImagesID	: this.SliderImagesID,
				SliderImagesLngTitle			: values.SliderImagesLngTitle,					
				SliderImagesLngSubtitle			: values.SliderImagesLngSubtitle,					
				SliderImagesLngLinkTitle			: values.SliderImagesLngLinkTitle,
				SliderImagesLngExternalLink	:		values.SliderImagesLngExternalLink,
                SliderImagesLngExternalLinkTarget:	values.SliderImagesLngExternalLinkTarget,
			
			},
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{
					myMask.hide();
					//me.close();
					console.log(obj.data);
					//me.callbackRefresh(obj.data);
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				//me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{				
				myMask.hide();	
				//me.close();
				Ext.Msg.alert('Error', 'Error in saving data !');
				return;				
			}   		
		});
	},
	

	
});





