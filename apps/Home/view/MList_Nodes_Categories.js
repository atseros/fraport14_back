﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Nodes_Categories' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Nodes_Categories',
	
	autoScroll: false,                
	layout: 'border',
		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterLanguage 	= 1;		
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'NodeID';
		
		this.SiteID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('NodeID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'NodeID', type: 'number', defaultValue: 0 }
				,{name: 'NodeSiteID', type: 'number', defaultValue: 0 }
				,{name: 'NodeNodeID', type: 'number', defaultValue: 0 }
				,{name: 'NodeLevel', type: 'number', defaultValue: 0 }
				,{name: 'NodeCode', type: 'number', defaultValue: 0 }
				,{name: 'NodeParentCode', type: 'string' }
				,{name: 'NodeTitle', type: 'string' }
				,{name: 'NodeUrlAlias', type: 'string'}
				,{name: 'NodePublished', type: 'string'}
				,{name: 'NodeIsHomePage', type: 'number', defaultValue: 0}
				,{name: 'NodeMenu', type: 'number', defaultValue: 0}
				,{name: 'NodeTemplateFile', type: 'string'}
				,{name: 'NodeCreatedAt', type: 'date'}
				,{name: 'NodeLngTitle', type: 'string'}
				,{name: 'NodeTitleFullCode', type: 'string'}
				,{name: 'NodeLngID', type: 'number', defaultValue: 0 }
				,{name: 'NodeSpecial', type: 'string'}
				,{name: 'NodeViewType', type: 'int'}
				,{name: 'NodeDataType', type: 'int'}
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{ header: 'Pages', dataIndex: 'NodeTitle', flex: 60/100,
					renderer: function(value, p, record) 
						{
							var Level = record.get('NodeLevel');

							if (Level == 0) 
								return '<b>' + value + '</b>';
							else if (Level == 1) 
								return '<span style="margin-left:15px;"><i style ="color:blue" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level == 2) 
								return '<span style="margin-left:30px;"><i style ="color:green" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level == 3) 
								return '<span style="margin-left:45px;"><i style ="color:orange" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level == 4) 
								return '<span style="margin-left:60px;"><i style ="color:purple" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level == 5) 
								return '<span style="margin-left:75px;"><i style ="color:red" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level >= 6) 
								return '<span style="margin-left:90px;"><i style ="color:#42dcf4" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else
								return value;
								
						}
				}
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getNodesListForCategories',
			maxRecords		: me.maxRecords,
			filterStrToFind		: me.filterStrToFind,
			SiteID				: me.SiteID,	
		};
		
		return params;
	},
	

});