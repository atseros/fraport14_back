﻿// // Require Combo of Templates 
Ext.require([ 'Home.view.ComboGrid_SelectTemplate']);
// // Require Combo of DataType 
Ext.require([ 'Home.view.ComboGrid_SelectDataType']);
// // Require Combo of View Type 
Ext.require([ 'Home.view.ComboGrid_SelectViewType']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditNodes', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditNodes',
		
	layout: 'border',
	title: 'Node Data',
    height: 680,
    width: 700,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 150,
				region :'north',
           },
			items: 
			[
				// Hidden field:  NodeID
				{
					xtype: 'hiddenfield',
					name: 'NodeID',
					fieldLabel: 'NodeID',
				},
				// Text field:  NodeParentCode
				{
					xtype: 'hiddenfield',
					name: 'NodeParentCode',
					readOnly: true,
					cls: 'readonly'
				},
				// Text field:  NodeCode
				{
					xtype: 'hiddenfield',
					name: 'NodeCode',
					fieldLabel: 'Parent',
				},
				// Text field:  NodeParent
				{
					xtype: 'textfield',
					name: 'NodeParent',
					fieldLabel: 'Parent',
					readOnly: true,
					cls: 'readonly'
				},
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},	
				// Text field:  NodeCreatedAt
				{
					xtype: 'datefield',
					name: 'NodeCreatedAt',
					fieldLabel: 'Date',
					format: 'd/m/Y',
					value: new Date(),
					readOnly: true,
					cls: 'readonly',
				},
				// Text field:  NodeTitle
				{
					xtype: 'textfield',
					name: 'NodeTitle',
					fieldLabel: 'Title',
					listeners: 
					{
						change: 
						{
							fn: function(field,e) 
							{
								var thisform = this.up('form');
								var val = field.getValue();
								var letters = 
								{
									"α" : "a","Α" : "a","β" : "b","Β" : "b","γ" : "g","Γ" : "g","δ" : "d","Δ" : "d","ε" : "e","Ε" : "e","ζ" : "z","Ζ" : "z","η" : "i","Η" : "i","θ" : "th","Θ" : "th","ι" : "i","Ι" : "i",
									"κ" : "k","Κ" : "k","λ" : "l","Λ" : "l","μ" : "m","Μ" : "m","ν" : "n","Ν" : "n","ξ" : "x","Ξ" : "x","ο" : "o","Ο" : "o","π" : "p","Π" : "p","ρ" : "r","Ρ" : "r","σ" : "s","Σ" : "s",
									"τ" : "t","Τ" : "t","υ" : "y","Υ" : "y","φ" : "f","Φ" : "f","χ" : "x","Χ" : "x",	"ψ" : "ps","Ψ" : "ps",	"ω" : "o","Ω" : "o","ς" : "s",
									"a" : "a","A" : "a","b" : "b","B" : "b","c" : "c","C" : "c","d" : "d","D" : "d","e" : "e","E" : "e","f" : "f","F" : "f","g" : "g","G" : "g","h" : "h","H" : "h","i" : "i","I" : "i",
									"j" : "j","J" : "j","k" : "k","K" : "k","l" : "l","L" : "l","m" : "m","M" : "m","n" : "n","N" : "n","o" : "o","O" : "o","p" : "P","P" : "p","q" : "q","Q" : "q","r" : "r","R" : "r",		
									"s" : "s","S" : "s","t" : "t","T" : "t","u" : "u","U" : "u","v" : "v","V" : "v",	"w" : "w","W" : "w","x" : "x","X" : "x","y" : "y","Y" : "y","z" : "z","Z" : "z",						
									" " : "-","1" : "1","2" : "2","3" : "3","4" : "4","5" : "5","6" : "6","7" : "7","8" : "8","9" : "9","0" : "0"	,
									"ά" : "a","Ά" : "a","έ" : "e","Έ" : "e","ή" : "i","Ή" : "i","ό" : "O","Ό" : "o","ύ" : "i","Ύ" : "i","ώ" : "o","Ώ" : "o"	
						
								};
							  var newStr = '';
							  for (var substr in val){
								  newStr += (letters.hasOwnProperty(val[substr])) ? letters[val[substr]] : '';
							  }								
								thisform.getForm().findField('NodeUrlAlias').setValue(newStr.toLowerCase());	
								
							}
  
						}
					}	
				},
				// Text field:  NodeUrlAlias
				{
					xtype: 'textfield',
					name: 'NodeUrlAlias',
					fieldLabel: 'Url Alias',
				},
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},
				// Combo box:  Data & View			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						{
							xtype: 'hiddenfield',
							fieldLabel: 'NodeDataType',
							name: 'NodeDataType',
							value: 0
						},  
						{
							xtype: 'textfield',
							fieldLabel: 'Data Type',
							name: 'NodeDataTypeDescr',                  
							flex: 19,
							labelWidth: 150,
							allowBlank: true,
							readOnly: true,
							maxLength: 255,
							cls: 'readonly_combos',

						},
						{
							xtype: 'button',
							text: '.',
							width: 16,											
							handler: function() 
							{
								var x = new Home.view.ComboGrid_SelectDataType;				
								var thisgrid = this.up('form').up('panel');
								x.SiteID = thisgrid.SiteID;	
								x.show(this);
								// if in Edit Mode, set already saved NodeDataType
								var thisform = this.up('form');
								x.SelectedRecordID = thisform.getValues().NodeDataType;
								
								x.callbackAfterSelect = function(id, name)
														{ 
															thisform.getForm().findField('NodeDataType').setValue(id);
															thisform.getForm().findField('NodeDataTypeDescr').setValue(name);
															thisform.getForm().findField('NodeDataTypeSelector').setValue(id);
															if (id == -3 || id == -2 || id == -1 || id == 0)
															{
																thisform.getForm().findField('NodeViewType').setValue(0);
																thisform.getForm().findField('NodeViewTypeDescr').setValue();
															}	
														};
								x.loadData();
							},	
						},
						// Line Separator
						{
							xtype: 'tbfill'
						},	
						{
							xtype: 'hiddenfield',
							fieldLabel: 'NodeViewType',
							name: 'NodeViewType',
							value: 0
						},  
						{
							xtype: 'hiddenfield',
							fieldLabel: 'NodeDataTypeSelector',
							name: 'NodeDataTypeSelector',
							value: 0
						},  
						{
							xtype: 'textfield',
							fieldLabel: 'View Type',
							name: 'NodeViewTypeDescr',                  
							flex: 19,
							labelWidth: 150,
							allowBlank: true,
							readOnly: true,
							maxLength: 255,
							cls: 'readonly_combos',
							labelAlign: 'right',

						},
						{
							xtype: 'button',
							text: '.',
							width: 16,											
							handler: function() 
							{
								var x = new Home.view.ComboGrid_SelectViewType;				
								x.show(this);
								// if in Edit Mode, set already saved NodeViewType
								var thisform = this.up('form');
								x.SelectedRecordID = thisform.getValues().NodeViewType;
								x.NodeDataTypeSelector = thisform.getValues().NodeDataTypeSelector;
								
								x.callbackAfterSelect = function(id, name)
														{ 
															thisform.getForm().findField('NodeViewType').setValue(id);
															thisform.getForm().findField('NodeViewTypeDescr').setValue(name);
														};
								x.loadData();
							}					
						},	
					]
				},
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},
				// Combo field:  Flight			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsFlight
						{
							xtype: 'combo',    
							name: 'NodeIsFlight',
							forceSelection: true,
							fieldLabel: 'Flight list',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									if( combo.getValue() == 1)
									{
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);	
										NodeIsSearchPage.setValue(0);
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}										
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsFlightDetailed
						{
							xtype: 'combo',    
							name: 'NodeIsFlightDetailed',
							forceSelection: true,
							fieldLabel: 'Flight Detailed',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);										
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},				
				// Combo field:  Airline			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsAirline
						{
							xtype: 'combo',    
							name: 'NodeIsAirline',
							forceSelection: true,
							fieldLabel: 'Airline list',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);										
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsAirlineDetailed
						{
							xtype: 'combo',    
							name: 'NodeIsAirlineDetailed',
							forceSelection: true,
							fieldLabel: 'Airline Detailed',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);										
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},
				// Combo field:  Shop			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsShop
						{
							xtype: 'combo',    
							name: 'NodeIsShop',
							forceSelection: true,
							fieldLabel: 'Shop List',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);	
										NodeIsSearchPage.setValue(0);											
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsShopDetailed
						{
							xtype: 'combo',    
							name: 'NodeIsShopDetailed',
							forceSelection: true,
							fieldLabel: 'Shop/Rest/D.Free Detailed',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);										
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},
				// Combo field:  Shop			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsRestaurant
						{
							xtype: 'combo',    
							name: 'NodeIsRestaurant',
							forceSelection: true,
							fieldLabel: 'Restaurant List',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);										
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},
						// Combo field: NodeIsDutyFree
						{
							xtype: 'combo',    
							name: 'NodeIsDutyFree',
							forceSelection: true,
							fieldLabel: 'Duty Free List',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);										
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},
				// Combo field:  Category			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsCategory
						{
							xtype: 'combo',    
							name: 'NodeIsCategory',
							forceSelection: true,
							fieldLabel: 'Category List',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);										
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsCategoryDetailed
						{
							xtype: 'combo',    
							name: 'NodeIsCategoryDetailed',
							forceSelection: true,
							fieldLabel: 'Category Detailed',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);										
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},
				// Combo field:  Image Gallery			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsImageGallery
						{
							xtype: 'combo',    
							name: 'NodeIsImageGallery',
							forceSelection: true,
							fieldLabel: 'Image Gallery List',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);				
										NodeIsSearchPage.setValue(0);								
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsImageGalleryDetailed
						{
							xtype: 'combo',    
							name: 'NodeIsImageGalleryDetailed',
							forceSelection: true,
							fieldLabel: 'Image Gallery Detailed',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);			
										NodeIsSearchPage.setValue(0);									
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},
				// Combo field:  Video Gallery			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsVideoGallery
						{
							xtype: 'combo',    
							name: 'NodeIsVideoGallery',
							forceSelection: true,
							fieldLabel: 'Video Gallery List',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);				
										NodeIsSearchPage.setValue(0);								
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsVideoGalleryDetailed
						{
							xtype: 'combo',    
							name: 'NodeIsVideoGalleryDetailed',
							forceSelection: true,
							fieldLabel: 'Video Gallery Detailed',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);			
										NodeIsSearchPage.setValue(0);									
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},
				// Combo field:  Publications List			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsPublications
						{
							xtype: 'combo',    
							name: 'NodeIsPublications',
							forceSelection: true,
							fieldLabel: 'Publications List',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										PublicationsDetailed.setValue(0);			
										NodeIsSearchPage.setValue(0);									
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsPublicationsDetailed
						{
							xtype: 'combo',    
							name: 'NodeIsPublicationsDetailed',
							forceSelection: true,
							fieldLabel: 'Publications Detailed',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);		
										NodeIsSearchPage.setValue(0);
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},
				// Combo field:  Destinations List			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsDomesticDestinations
						{
							xtype: 'combo',    
							name: 'NodeIsDomesticDestinations',
							forceSelection: true,
							fieldLabel: 'Domestic Destinations',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);			
										PublicationsDetailed.setValue(0);			
										NodeIsSearchPage.setValue(0);									
										DarkSite.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsInternationalDestinations
						{
							xtype: 'combo',    
							name: 'NodeIsInternationalDestinations',
							forceSelection: true,
							fieldLabel: 'International Destinations',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);													
										PublicationsDetailed.setValue(0);		
										NodeIsSearchPage.setValue(0);
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},	
					]
				},
				// Combo field:  Search Page	
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsSearchPage
						{
							xtype: 'combo',    
							name: 'NodeIsSearchPage',
							forceSelection: true,
							fieldLabel: 'Search Page',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);												
										PublicationsDetailed.setValue(0);											
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						// Combo field: NodeIsWeatherPage
						{
							xtype: 'combo',    
							name: 'NodeIsWeatherPage',
							forceSelection: true,
							fieldLabel: 'Weather Page',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							labelAlign: 'right',
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);												
										PublicationsDetailed.setValue(0);											
										DarkSite.setValue(0);	
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);										
										NodeIsSearchPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
					]
				},
				// Combo field:  Destinations
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsDestinations
						{
							xtype: 'combo',    
							name: 'NodeIsDestinations',
							forceSelection: true,
							fieldLabel: 'Destinations Page',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var DarkSite = form.getForm().findField("NodeIsDarkSite");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");									
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);												
										PublicationsDetailed.setValue(0);											
										DarkSite.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsSearchPage.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},
					]
				},
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},
				// Combo field:  Shop			
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						// Combo field: NodeIsDarkSite
						{
							xtype: 'combo',    
							name: 'NodeIsDarkSite',
							forceSelection: true,
							fieldLabel: 'Is Dark Site',
							cls: 'readonly_combos',
							editable: false,
							labelWidth: 150,
							store:
							{
								fields: 
								[
									{name: 'id', type: 'int'},
									{name: 'Descr', type: 'string'}
								],
								data: 
								[
									{ id: 1, Descr: 'Yes'},
									{ id: 0, Descr: 'No'},
								],
								autoLoad: true
							},
							queryMode:'local',
							valueField: 'id',
							displayField: 'Descr',
							listeners: 
							{
								select: function(combo) 
								{
									var form = combo.up('form');
									var Flight = form.getForm().findField("NodeIsFlight");
									var FlightDetailed = form.getForm().findField("NodeIsFlightDetailed");
									var Airline = form.getForm().findField("NodeIsAirline");
									var AirlineDetailed = form.getForm().findField("NodeIsAirlineDetailed");
									var Shop = form.getForm().findField("NodeIsShop");
									var ShopDetailed = form.getForm().findField("NodeIsShopDetailed");
									var Restaurant = form.getForm().findField("NodeIsRestaurant");
									var DutyFree = form.getForm().findField("NodeIsDutyFree");
									var Category = form.getForm().findField("NodeIsCategory");
									var CategoryDetailed = form.getForm().findField("NodeIsCategoryDetailed");
									var ImageGallery = form.getForm().findField("NodeIsImageGallery");
									var ImageGalleryDetailed = form.getForm().findField("NodeIsImageGalleryDetailed");
									var VideoGallery = form.getForm().findField("NodeIsVideoGallery");
									var VideoGalleryDetailed = form.getForm().findField("NodeIsVideoGalleryDetailed");
									var Publications = form.getForm().findField("NodeIsPublications");
									var PublicationsDetailed = form.getForm().findField("NodeIsPublicationsDetailed");
									var DomesticDestinations = form.getForm().findField("NodeIsDomesticDestinations");
									var InternationalDestinations = form.getForm().findField("NodeIsInternationalDestinations");
									var NodeIsSearchPage = form.getForm().findField("NodeIsSearchPage");
									var NodeIsWeatherPage = form.getForm().findField("NodeIsWeatherPage");
									var NodeIsDestinations = form.getForm().findField("NodeIsDestinations");
									
									if( combo.getValue() == 1)
									{
										Flight.setValue(0);
										FlightDetailed.setValue(0);
										Airline.setValue(0);
										AirlineDetailed.setValue(0);	
										Shop.setValue(0);
										ShopDetailed.setValue(0);
										Restaurant.setValue(0);
										DutyFree.setValue(0);
										Category.setValue(0);	
										CategoryDetailed.setValue(0);											
										ImageGallery.setValue(0);	
										ImageGalleryDetailed.setValue(0);											
										VideoGallery.setValue(0);	
										VideoGalleryDetailed.setValue(0);											
										Publications.setValue(0);	
										PublicationsDetailed.setValue(0);
										NodeIsSearchPage.setValue(0);
										DomesticDestinations.setValue(0);
										InternationalDestinations.setValue(0);
										NodeIsWeatherPage.setValue(0);											
										NodeIsDestinations.setValue(0);
									}
								}
							}
						},
						{
							xtype: 'tbfill'
						},	
						
					]
				},	
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},						
				// Combo field:  NodeTemplateFile
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
							{
								xtype: 'textfield',
								fieldLabel: 'Template',
								name: 'NodeTemplateFile',
								itemId: 'NodeTemplateFile',                    
								flex: 19,
								labelWidth: 150,
								allowBlank: true,
								readOnly: true,
								maxLength: 255

							},
							{
								xtype: 'button',
								text: '...',	
								flex: 1,										
								handler: function() 
								{
									var x = new Home.view.ComboGrid_SelectTemplate; 
									x.show(this);
									// if in Edit Mode, set already saved AvlSclID
									var thisform = this.up('form');
									x.SelectedRecordID = thisform.getValues().NodeTemplate;
									
									x.callbackAfterSelect = function(name)
															{ 
																thisform.getForm().findField('NodeTemplateFile').setValue(name);
															};
									x.loadData();
								}					
							}								
					]
				},
				
				
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.SiteID = 0;		

		this.NodeLevel = 0;	

		this.NodeNodeID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_Node_NewRecordDefValues',
			NodeNodeID	:this.NodeNodeID,
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
		
		var form = this.query('form')[0];
			
		var values = form.getValues();
	
		// do anything else here
		var NodeDataType = this.query("hiddenfield[name=NodeDataType]")[0];
		var NodeDataTypeDescr = this.query("textfield[name=NodeDataTypeDescr]")[0];
		
		NodeDataType.setValue(0);
		NodeDataTypeDescr.setValue('No Data Type');
	
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_NodeRecord',
			NodeID			: this.editRecID,
			NodeNodeID 	: this.NodeNodeID 
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		var form = this.query('form')[0];
			
		var values = form.getValues();
		
		var NodeDataType = this.query("hiddenfield[name=NodeDataType]")[0];
		var NodeDataTypeSelector = this.query("hiddenfield[name=NodeDataTypeSelector]")[0];
		
		NodeDataTypeSelector.setValue(NodeDataType.value);

	},
	
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'InsertNodeRecord',
			NodeID:						 values.NodeID,
			NodeCreatedAt:			 values.NodeCreatedAt,
			NodeParentCode:		 values.NodeParentCode,
			NodeCode:					 values.NodeCode,
			NodeTitle:					 values.NodeTitle,
			NodeUrlAlias:				 values.NodeUrlAlias,
			NodeTemplateFile:		 values.NodeTemplateFile,
			NodeIsFlight:		 		 values.NodeIsFlight,
			NodeIsFlightDetailed:	 values.NodeIsFlightDetailed,
			NodeIsAirline:			  	 values.NodeIsAirline,
			NodeIsAirlineDetailed:	 values.NodeIsAirlineDetailed,
			NodeIsShop:			  	 values.NodeIsShop,
			NodeIsShopDetailed:	 values.NodeIsShopDetailed,
			NodeIsDarkSite:			 values.NodeIsDarkSite,
			NodeDataType:			 values.NodeDataType,
			NodeViewType:			 values.NodeViewType,
			NodeIsCategory:			 values.NodeIsCategory,
			NodeIsCategoryDetailed:values.NodeIsCategoryDetailed,
			NodeIsRestaurant:		 values.NodeIsRestaurant,
			NodeIsDutyFree:			 values.NodeIsDutyFree,
			NodeIsImageGallery:	values.NodeIsImageGallery,
			NodeIsImageGalleryDetailed:values.NodeIsImageGalleryDetailed,
			NodeIsVideoGallery:	values.NodeIsVideoGallery,
			NodeIsVideoGalleryDetailed:	values.NodeIsVideoGalleryDetailed,
			NodeIsPublications:		values.NodeIsPublications,
			NodeIsPublicationsDetailed:values.NodeIsPublicationsDetailed,
			NodeIsSearchPage:			values.NodeIsSearchPage,
			NodeIsDomesticDestinations:			values.NodeIsDomesticDestinations,
			NodeIsInternationalDestinations:	values.NodeIsInternationalDestinations,
			NodeIsWeatherPage:	values.NodeIsWeatherPage,
			NodeIsDestinations:	values.NodeIsDestinations,
			NodeSiteID:			 	 this.SiteID,
			NodeLevel:			 		 this.NodeLevel,
			NodeNodeID:			 	 this.NodeNodeID,
			
		};

		return params;
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'UpdateNodeRecord',
			NodeID:						 values.NodeID,
			NodeParentCode:		values.NodeParentCode,
			NodeCode:					 values.NodeCode,
			NodeTitle:					 values.NodeTitle,
			NodeUrlAlias:				 values.NodeUrlAlias,
			NodeTemplateFile:		 values.NodeTemplateFile,
			NodeIsFlight:		 		 values.NodeIsFlight,
			NodeIsFlightDetailed:	 values.NodeIsFlightDetailed,
			NodeIsAirline:			  	 values.NodeIsAirline,
			NodeIsAirlineDetailed:	 values.NodeIsAirlineDetailed,
			NodeIsShop:			  	 values.NodeIsShop,
			NodeIsShopDetailed:	 values.NodeIsShopDetailed,
			NodeIsDarkSite:			 values.NodeIsDarkSite,
			NodeDataType:			 values.NodeDataType,
			NodeViewType:			 values.NodeViewType,
			NodeIsCategory:			 values.NodeIsCategory,
			NodeIsCategoryDetailed:values.NodeIsCategoryDetailed,
			NodeIsRestaurant:		 values.NodeIsRestaurant,
			NodeIsDutyFree:			 values.NodeIsDutyFree,
			NodeIsImageGallery:	values.NodeIsImageGallery,
			NodeIsImageGalleryDetailed:values.NodeIsImageGalleryDetailed,
			NodeIsVideoGallery:	values.NodeIsVideoGallery,
			NodeIsVideoGalleryDetailed:	values.NodeIsVideoGalleryDetailed,
			NodeIsPublications:		values.NodeIsPublications,
			NodeIsPublicationsDetailed:values.NodeIsPublicationsDetailed,
			NodeIsSearchPage:				values.NodeIsSearchPage,
			NodeIsDomesticDestinations:			values.NodeIsDomesticDestinations,
			NodeIsInternationalDestinations:	values.NodeIsInternationalDestinations,
			NodeIsWeatherPage:	values.NodeIsWeatherPage,
			NodeIsDestinations:	values.NodeIsDestinations,
			
		};
		
		return params;
	},
	
});