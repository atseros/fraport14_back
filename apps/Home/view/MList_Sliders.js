﻿// // Require Popup Form of Slider (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditSlider']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Sliders' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Sliders',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					xtype: 'button',
					text: 'Show All',
					itemId: '',
					iconCls: 'fa fa-filter',
					enableToggle: true,
					pressed: false,
					handler: function(button) 
					{ 
						var gridpanel = this.up('panel'); 
						
						if(!button.pressed)
							gridpanel.filterShowAll = 0;
						else 
							gridpanel.filterShowAll = 1;
						gridpanel.loadData();	
					}					
				},
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				200 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 5'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				},
				{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},		
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				},
				// {
					 // xtype:		'button'
					// ,text:		'Add Slider To Node'
					// ,iconCls: 	'fa fa-arrow-right'
					// ,margin:		'0 0 0 10'
					// ,handler:	function() 
					// {
						// var thisgrid = this.up('panel');
						// thisgrid.AddSliderToNode();
					// }				
				// },
				{
					text: 'Add Slider',
					tooltip: 'Add Slider',	
					iconCls: 'fa fa-bolt',					
					menu: 
					{
						xtype: 'menu',                          
						items: 
						[
							{
								 xtype:		'button'
								,text:		'Add Slider To Node'
								,iconCls: 	'fa fa-arrow-right'
								,handler:	function() 
								{
									var thisgrid = this.up('menu').up('panel');
									thisgrid.AddSliderToNode();
								}
							},
							{
								 xtype:		'button'
								,text:		'Add Slider To Category'
								,iconCls: 	'fa fa-arrow-right'
								,handler: function() 
								{ 
									var thisgrid = this.up('menu').up('panel');
									thisgrid.AddSliderToCategory();
								}			
							}	
						]                          
					}
				},
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterShowAll	= 0;	
		this.maxRecords 	= 500;
		this.fieldNameID = 'SliderID';
		
		this.SiteID = 0;
		
		this.NodeID = 0;
		this.NodeTitle = "";
		
		this.CtgRecID = 0;
		this.CtgRecTitle = "";
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				//return value.get('SliderID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'SliderID', type: 'number', defaultValue: 0 }
				,{name: 'SliderSiteID', type: 'number', defaultValue: 0 }
				,{name: 'SliderTitle', type: 'string' }				
				,{name: 'SliderStatus', type: 'number', defaultValue: 0 }
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Status', dataIndex: 'SliderStatus',  width: 40, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 2) 
						{
							return '<i class="fa fa-times" aria-hidden="true" style="font-size:16px;color:red"></i>';
						}
						else 
						{
							return '<i class="fa fa-check" aria-hidden="true" style="font-size:16px;color:green"></i>';
						}
							
					}
				},				
				{ header: 'Title', dataIndex: 'SliderTitle', flex: 100/100,},
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getSliderList',
			maxRecords		: me.maxRecords,
			filterStrToFind		: me.filterStrToFind,
			filterShowAll		: me.filterShowAll,
			SiteID				: me.SiteID,	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditSlider; 		
		x.editRecID = 0;
		x.SiteID = thisgrid.SiteID;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
	
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditSlider; 
			
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); Ext.ComponentManager.get('MList_Node_Sliders').loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteSliderRecord'
				,SliderID		: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
								Ext.ComponentManager.get('MList_Node_Sliders').loadData();					
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
	
	/**************************************************************************************************
	* AddSliderToNode
	*
	* Button handler for AddSliderToNode
	*
	***************************************************************************************************/
	AddSliderToNode: function()
	{
		var thisgrid = this;
		
		var NodeID  	= thisgrid.NodeID;
		var SliderID 	= thisgrid.getSelectedRecordID();

		params = 
		{
			appPage		: 'Home',
			appMethod		: 'AddSliderToNode',
			NodeSliderNodeID: NodeID,
			NodeSliderSliderID: SliderID,
			
		};
		
		
		Ext.Msg.confirm('Confirmation', 'Add Slider to '+thisgrid.NodeTitle+' ?', 
		function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Ajax.request({
						url: 'appAjax.php',
						method: 'post',
						params:  params,
						success: function(response)
						{
							var text = response.responseText;
							var obj = Ext.JSON.decode(text);
							if (!obj.data) 
							{										
								Ext.Msg.alert('Not added!', obj.error);
								return;
							}
							Ext.ComponentManager.get('MList_Nodes_Media4').loadData();
							// Ext.ComponentManager.get('MList_CentralImageGalleryDetailed').loadData();	
						}	
					});
					return true;
				} 
				else 
				{
					return false;
				}
			}
		);	
		
	},	
	
	/**************************************************************************************************
	* AddSliderToCategory
	*
	* Button handler for AddSliderToCategory
	*
	***************************************************************************************************/
	AddSliderToCategory: function()
	{
		var thisgrid = this;
		
		var CtgRecID  	= thisgrid.CtgRecID;
		var SliderID 	= thisgrid.getSelectedRecordID();

		params = 
		{
			appPage		: 'Home',
			appMethod		: 'AddSliderToCategory',
			CtgRecSliderCtgRecID: CtgRecID,
			CtgRecSliderSliderID: SliderID,
			
		};
		
		
		Ext.Msg.confirm('Confirmation', 'Add Slider to '+thisgrid.CtgRecTitle+' ?', 
		function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Ajax.request({
						url: 'appAjax.php',
						method: 'post',
						params:  params,
						success: function(response)
						{
							var text = response.responseText;
							var obj = Ext.JSON.decode(text);
							if (!obj.data) 
							{										
								Ext.Msg.alert('Not added!', obj.error);
								return;
							}
							Ext.ComponentManager.get('MList_Categories_Records_Sliders').loadData();
							// Ext.ComponentManager.get('MList_CentralImageGalleryDetailed').loadData();	
						}	
					});
					return true;
				} 
				else 
				{
					return false;
				}
			}
		);	
		
	},
});