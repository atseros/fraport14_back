﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.FormTemplate']);

Ext.define('Home.view.Frm_Subject_Answer_Translation' ,{
    extend: 'Include.view.FormTemplate',
    alias : 'widget.Frm_Subject_Answer_Translation',
	
	autoScroll: false,                
	layout: 'border',
	
	
	
	/**************************************************************************************************
	* Items
	***************************************************************************************************/
	items: 
	[
        {
            xtype: 'form',
			//frame: true,
			layout: 'border',
			margin: '5 5 5 5',
			border:0,
			region: 'center',
            items: 
			[	
				{
					xtype: 'hiddenfield',
					name: 'AnsLngID',
				},
				{
					
					xtype: 'radiogroup',    
					fieldLabel: 'Language Selection',
					labelWidth: 150,
					layout: 'hbox',
					region: 'north',
					defaults: 
					{
						flex: 1
					},
					items: 
					[
						{
							xtype: 'radiogroup',
							columns: 4,
							items: 
							[
								{
									xtype: 'radiofield',
									name: 'AnsLngType',
									boxLabel: 'English',
									checked: true,
									inputValue: 1
								},
								{
									xtype: 'radiofield',
									name: 'AnsLngType',
									boxLabel: 'Greek',
									inputValue: 2
								},
								{
									xtype: 'radiofield',
									name: 'AnsLngType',
									boxLabel: 'Deutch',
									inputValue: 3
								},
								{
									xtype: 'radiofield',
									name: 'AnsLngType',
									boxLabel: 'Russian',
									inputValue: 4
								},
								
							],
							listeners: 
							{
								change : function(obj, value)
								{ 
									var formPanel = this.up('form').up('panel');
									
									var val = value.AnsLngType;              
									formPanel.AnsLngType = val;
									formPanel.loadData();
								}
							}
							
						}
					]
					
				},
				{
					xtype: 'panel',
					border: 0,
					layout: 'border',
					region: 'center',
					items:
					[	
						// Line Separator
						{
							xtype: 'container',
							html: '<hr>',
							height:10
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '20 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Title',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  AnsLngTitle
						{
							xtype: 'textfield',
							name: 'AnsLngTitle',
							border: 0,
							layout: 'border',
							region :'north',
						},
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            region: 'north',
                            margin: '20 0 0 0',
                            height: 22,
                            items:
                                [
                                    {
                                        xtype: 'label',
                                        text: 'Show Contact Form',
                                        cls: 'boldtitle'
                                    },
                                ]
                        },
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            region: 'north',
                            margin: '0 0 0 0',
                            height: 22,
                            items:
                                [
                                    // Text field:  AnsLngShowCnt
                                    {
                                        xtype: 'combo',
                                        name: 'AnsLngShowCnt',
                                        forceSelection: true,
                                        cls: 'readonly_combos',
                                        editable: false,
                                        labelAlign: 'right',
                                        region: 'center',
                                        flex: 1,
                                        store:
                                            {
                                                fields:
                                                    [
                                                        {name: 'id', type: 'int'},
                                                        {name: 'Descr', type: 'string'}
                                                    ],
                                                data:
                                                    [
                                                        {id: 1, Descr: 'No'},
                                                        {id: 2, Descr: 'Yes'},
                                                    ],
                                                autoLoad: true
                                            },
                                        queryMode: 'local',
                                        valueField: 'id',
                                        displayField: 'Descr',
                                    },
                                ]
                        },
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            region: 'north',
                            margin: '20 0 0 0',
                            height: 22,
                            items:
                                [
                                    {
                                        xtype: 'label',
                                        text: 'Description',
                                        cls: 'boldtitle'
                                    },
                                ]
                        },
                        // Text field:  AnsLngDescr
                        {
                            xtype: 'my_tinymce_textarea',
                            flex: 1,
                            name: 'AnsLngDescr',
                            border: 0,
                            layout: 'border',
                            region :'center',
                            split: true,

                        },
					]
				},
				{
					xtype: 'container',
					layout: 'hbox',
					region: 'south',
					margin: '5 0 0 0',
					height: 30,
					items:
					[
						
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'fa fa-floppy-o',
							margin: '0 0 0 0',
							text: 'Save',
							handler: function() 
							{
								var formPanel = this.up('form').up('panel');
								formPanel.Save();
							}
						}
					]
				},
				
			]	
        }       
    ],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.SubSubID = 0;		
		
		this.AnsLngID = 0;		
		
		this.AnsLngType = 1;	
				
		this.fieldNameID = 'AnsLngID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				 
			}					
		};						

		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 			'Home',
			appMethod: 			'get_AnswerLngRecord',
			AnsLngSubSubID: 	me.SubSubID,
			AnsLngType: 		me.AnsLngType,
			
		};
		
		return params;
	},
	
	
	/**************************************************************************************************
	* afterLoadDefault
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		var me = this;
		var thisf = me.query('form')[0];
		
		thisf.getForm().setValues(data);
		
		var id = thisf.getForm().getValues().AnsLngID;
		var tinymce_editor1 = this.query('my_tinymce_textarea[name=AnsLngDescr]')[0];
		tinymce_editor1.submitUrl='appAjax.php?appPage=Home&appMethod=handleTinyMCE_UploadImage&table_name=cf_answers_lang&id=' + id;

	},
	
	/**************************************************************************************************
	* Save
	***************************************************************************************************/
	Save: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var me = this;
		
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();
		
		// Update Current Record 
		Ext.Ajax.request({
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			timeout: 60000,
			params: 
			{
				appPage				: 'Home',
				appMethod			: 'UpdateAnswerLngRecord',
				AnsLngID			: values.AnsLngID,
				AnsLngType			: this.AnsLngType,
				AnsLngSubSubID		: this.SubSubID,
				AnsLngTitle			: values.AnsLngTitle,
                AnsLngDescr			: values.AnsLngDescr,
                AnsLngShowCnt		: values.AnsLngShowCnt,
			},
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{
					myMask.hide();
					//me.close();
					console.log(obj.data);
					//me.callbackRefresh(obj.data);
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				//me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{				
				myMask.hide();	
				//me.close();
				Ext.Msg.alert('Error', 'Error in saving data !');
				return;				
			}   		
		});
	},
	

	
});





