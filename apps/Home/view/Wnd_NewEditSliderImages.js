﻿// // Require Combo of Nodes 
Ext.require([ 'Home.view.ComboGrid_SelectNode']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditSliderImages', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditSliderImages',
		
	layout: 'border',
	title: 'Slider Image Data',
    height: 240,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 60,
				region :'north',
           },
			items: 
			[
				// Hidden field:  SliderImagesID
				{
					xtype: 'hiddenfield',
					name: 'SliderImagesID',
				},
				// Hidden field:  SliderImagesSliderID
				{
					xtype: 'hiddenfield',
					name: 'SliderImagesSliderID',
				},
				//File
				{
					xtype: 'filefield',
					name: 'File',
					fieldLabel: 'File',
					border: 0,
					layout: 'border',
					region :'north',
					regex: / /,
					regexText: ''
				},
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},
				// Combo field: SliderImagesStatus
				{
					xtype: 'combo',    
					name: 'SliderImagesStatus',
					forceSelection: true,
					fieldLabel: 'Status',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},					
				// Text field:  SliderImagesPriority
				{
					xtype: 'numberfield',
					name: 'SliderImagesPriority',
					fieldLabel: 'Priority',
					border: 0,
					layout: 'border',
					value : 100,
				},				
				// Text field:  SliderImagesTitle
				{
					xtype: 'textfield',
					name: 'SliderImagesTitle',
					fieldLabel: 'Title',
				},	
				// Combo field:  SliderImagesNodeID
				{
					xtype: 'container',
					region :'north',
					layout: { type: 'hbox' },
					items:
					[
							{
								xtype: 'hiddenfield',
								fieldLabel: 'SliderImagesNodeID',
								name: 'SliderImagesNodeID',
								itemId: 'SliderImagesNodeID',
								value: 0
							},  
							{
								xtype: 'textfield',
								fieldLabel: 'Link',
								name: 'NodeTitle',
								itemId: 'NodeTitle',                    
								flex: 19,
								labelWidth: 60,
								allowBlank: true,
								readOnly: true,
								maxLength: 255,
								cls: 'readonly_combos',

							},
							{
								xtype: 'button',
								text: '.',
								itemId: 'btnSelect',	
								width: 18,											
								handler: function() 
								{
									var x = new Home.view.ComboGrid_SelectNode;
									var thisgrid = this.up('form').up('panel');
									x.SiteID = thisgrid.SiteID;					
									x.show(this);
									// if in Edit Mode, set already saved SliderImagesNodeID
									var thisform = this.up('form');
									x.SelectedRecordID = thisform.getValues().SliderImagesNodeID;
									
									x.callbackAfterSelect = function(id, name)
															{ 
																thisform.getForm().findField('SliderImagesNodeID').setValue(id);
																thisform.getForm().findField('NodeTitle').setValue(name);
															};
									x.loadData();
								}					
							},
								
					]
				},								
				// Text field:  SliderImagesExternalLink
				// {
					// xtype: 'textfield',
					// name: 'SliderImagesExternalLink',
					// fieldLabel: 'External Link',
				// },	
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.SliderID = 0;

		this.SiteID = 0;	
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_SliderImages_NewRecordDefValues',
			SliderID			:this.SliderID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
		
		var form = this.query('form')[0];
			
		var values = form.getValues();
	
		// do anything else here
		var File = this.query("filefield[name=File]")[0];

		File.regex = /^.*\.(jpg|JPG|gif|GIF|png|PNG|bmp|BMP)$/;
		File.regexText = 'Only JPG,GIF,PNG,BMP files allowed';

		
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_SliderImagesRecord',
			SliderImagesID			: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		var form = this.query('form')[0];
			
		var values = form.getValues();

		var File = this.query("filefield[name=File]")[0];

		File.regex = /^.*\.(jpg|JPG|gif|GIF|png|PNG|bmp|BMP)$/;
		File.regexText = 'Only JPG,GIF,PNG,BMP files allowed';
	
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;
		
		me.DataAndImage();
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* DataAndImage
	*
	* function that uploades Images and add data
	*
	***************************************************************************************************/
	DataAndImage: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();
		
		
		if (me.editRecID == 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=InsertSliderImagesRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been added.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not add.');                    
					}
				});
			}
			
		}
		else	if (me.editRecID > 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=UpdateSliderImagesRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been updated.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not update.');                    
					}
				});
			}
			
		}			

        
	},
	
});