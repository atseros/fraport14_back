﻿// // Require Popup Form of Slider Image (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditSliderImages']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Node_Slider_Images' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Node_Slider_Images',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterShowAll	= 0;
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'SliderImagesID';
		
		this.NodeID = 0;
		
		this.NodeLngType = 1;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('SliderImagesID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				
				{name: 'SliderImagesLngTitle', type: 'string' }
				,{name: 'SliderImagesFullUrl', type: 'string'}
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Image', dataIndex: 'SliderImagesFullUrl',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					}
				},
				{ header: 'Title', dataIndex: 'SliderImagesLngTitle', flex: 75/100,},
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getSliderImagesListSitePages',
			maxRecords		: me.maxRecords,
			NodeSliderNodeID	: me.NodeID,
			NodeLangType		: me.NodeLngType,
			
		};
		
		return params;
	},

	

});