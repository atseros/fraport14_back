﻿// // Require Popup Form of Nodes (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditNodes']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Nodes' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Nodes',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				// {
					 // xtype:				'textfield'
					// ,labelWidth:		40
					// ,allowBlank:		true
					// ,width:				250 
					// ,fieldLabel:		'Search'
					// ,margin:			'0 0 0 0'
					// ,enableKeyEvents:	true
					// ,listeners: 
					// {
						// specialkey: function(field,e)
						// {
							// if (e.getKey() == e.ENTER) 
							// {
								// var gridpanel = this.up('panel'); 
								// var val = field.getValue(); 		
								// gridpanel.filterStrToFind = val;
								// gridpanel.loadData();
							// }
						// },
						// keyup: 
						// {
							// fn: function(field,e) 
							// {
								// var gridpanel = this.up('panel'); 
								// var val = field.getValue();                
								// gridpanel.filterStrToFind = val;
							// }
  
						// }
					// }					
				// }
				// ,{
					 // xtype:		'button'
					// ,text:		''
					// ,tooltip:	'search'
					// ,iconCls:	'search-btn'
					// ,scale:		'small'
					// ,listeners: 
					// {
						// click: function() 
						// {
							// var gridpanel = this.up('panel');
							// gridpanel.loadData();
						// }
					// }					
				// },
				// {
					
					// xtype: 'combo',    
					// fieldLabel: 'Language',
					// labelWidth: 60,
					// width:150,
					// value:"1",
					// margin: '0 0 0 20',
					// store:
					// {
						// fields: 
						// [
							// {name: 'id', type: 'string'},
							// {name: 'Descr', type: 'string'}
						// ],
						// data: 
						// [
							// { id: 1, Descr: 'English'}
							// ,{ id: 2, Descr: 'Greek'}
							// ,{ id: 3, Descr: 'Deutch'}
							// ,{ id: 4, Descr: 'Russian'}
						// ],
						// autoLoad: true
					// },
					// queryMode:'local',
					// valueField: 'id',
					// displayField: 'Descr',
					// listeners:
					// {
						// 'change': function(field, newValue)
						// {
							// var thisgrid = this.up('panel');	
							// thisgrid.filterLanguage = newValue;
							// thisgrid.loadData();
						// }
				    // }
				// }
				,{
					xtype: 'tbfill'
				},
				{
					text: 'Actions',
					tooltip: 'Actions',	
					iconCls: 'fa fa-bolt',					
					menu: 
					{
						xtype: 'menu',                          
						items: 
						[
							{
								 xtype:		'button'
								,text:		'Home Page'
								,iconCls: 	'fa fa-home'
								,handler:	function() 
								{
									var thisgrid = this.up('menu').up('panel');
									thisgrid.homepageRecord();
								}
							},
							{
								 xtype:		'button'
								,text:		'Publish'
								,iconCls: 	'fa fa-bullseye'
								,handler: function() 
								{ 
									var thisgrid = this.up('menu').up('panel');
									thisgrid.publishRecord();
								}			
							},
							{
								 xtype:		'button'
								,text:		'Menu'
								,iconCls: 	'fa fa-bars'
								,handler:	function() 
								{
									var thisgrid = this.up('menu').up('panel');
									thisgrid.menuRecord();
								}				
							},
							 							
						]                          
					}
				},
				{
					text: 'Move',
					tooltip: 'Move',	
					iconCls: 'fa fa-arrows',					
					menu: 
					{
						xtype: 'menu',                          
						items: 
						[
							{
								 xtype:		'button'
								,text:		'Up'
								,iconCls: 	'fa fa-arrow-up'
								,handler:	function() 
								{
									var thisgrid = this.up('menu').up('panel');
									thisgrid.moveRecord(1);
								}
							},
							{
								 xtype:		'button'
								,text:		'Down'
								,iconCls: 	'fa fa-arrow-down'
								,handler: function() 
								{ 
									var thisgrid = this.up('menu').up('panel');
									thisgrid.moveRecord(2);
								}			
							},
						]                          
					}
				},
				{
					text: 'Add',
					tooltip: 'Add',	
					iconCls: 'fa fa-plus',					
					menu: 
					{
						xtype: 'menu',                          
						items: 
						[
							{
								 xtype:		'button'
								,text:		'Add'
								,iconCls: 	'fa fa-level-up'
								,handler:	function() 
								{
									var thisgrid = this.up('menu').up('panel');
									thisgrid.addParent();
								}
							},
							{
								 xtype:		'button'
								,text:		'Add Child'
								,iconCls: 	'fa fa-level-down'
								,handler:	function() 
								{
									var thisgrid = this.up('menu').up('panel');
									thisgrid.addChild();
								}				
							},						
						]                          
					}
				}			
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				// ,{
					 // xtype:		'button'
					// ,text:		'Move'
					// ,iconCls: 	'fa fa-arrows'
					// ,handler:	function() 
					// { 
						// var thisgrid = this.up('panel');
						// thisgrid.moveRecord();
					// }					
				// }	
				// ,{
					 // xtype:		'button'
					// ,text:		'Content'
					// ,iconCls: 	'fa fa-qrcode'
					// ,handler:	function() 
					// { 
						// var thisgrid = this.up('panel');
						// thisgrid.contentRecord();
					// }					
				// }	
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				}
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterLanguage 	= 1;		
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'NodeID';
		
		this.SiteID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('NodeID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'NodeID', type: 'number', defaultValue: 0 }
				,{name: 'NodeSiteID', type: 'number', defaultValue: 0 }
				,{name: 'NodeNodeID', type: 'number', defaultValue: 0 }
				,{name: 'NodeLevel', type: 'number', defaultValue: 0 }
				,{name: 'NodeCode', type: 'number', defaultValue: 0 }
				,{name: 'NodeParentCode', type: 'string' }
				,{name: 'NodeTitle', type: 'string' }
				,{name: 'NodeUrlAlias', type: 'string'}
				,{name: 'NodePublished', type: 'string'}
				,{name: 'NodeIsHomePage', type: 'number', defaultValue: 0}
				,{name: 'NodeMenu', type: 'number', defaultValue: 0}
				,{name: 'NodeTemplateFile', type: 'string'}
				,{name: 'NodeCreatedAt', type: 'date'}
				,{name: 'NodeLngTitle', type: 'string'}
				,{name: 'NodeTitleFullCode', type: 'string'}
				,{name: 'NodeLngID', type: 'number', defaultValue: 0 }
				,{name: 'NodeSpecial', type: 'string'}
				,{name: 'NodeViewType', type: 'string'}
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Date', dataIndex: 'NodeCreatedAt',  flex: 15/100, renderer: Ext.util.Format.dateRenderer('d/m/y')}
				,{ header: 'Pages', dataIndex: 'NodeTitle', flex: 60/100,
					renderer: function(value, p, record) 
						{
							var Level = record.get('NodeLevel');

							if (Level == 0) 
								return '<b>' + value + '</b>';
							else if (Level == 1) 
								return '<span style="margin-left:15px;"><i style ="color:blue" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level == 2) 
								return '<span style="margin-left:30px;"><i style ="color:green" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level == 3) 
								return '<span style="margin-left:45px;"><i style ="color:orange" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level == 4) 
								return '<span style="margin-left:60px;"><i style ="color:purple" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level == 5) 
								return '<span style="margin-left:75px;"><i style ="color:red" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else if (Level >= 6) 
								return '<span style="margin-left:90px;"><i style ="color:#42dcf4" class="fa fa-level-down" aria-hidden="true"></i> '+value+"</span>";
							else
								return value;
								
						}
				}
				,{header: 'Home', dataIndex: 'NodeIsHomePage',  width: 50, resizable: false, align:'center',
						renderer: function(value, p, record) 
						{
							if (value == 1) 
							{
								return '<i class="fa fa-check" aria-hidden="true" style="font-size:13px;color:#000091"></i>';
							}
							else 
							{
								return '';
							}
						
						}
				}
				,{header: 'Published', dataIndex: 'NodePublished',  width: 60, resizable: false, align:'center',
						renderer: function(value, p, record) 
						{
							if (value == 1) 
							{
								return '<i class="fa fa-check" aria-hidden="true" style="font-size:13px;color:green"></i>';
							}
							else 
							{
								return '';
							}
						
						}
				}
				,{header: 'Menu', dataIndex: 'NodeMenu',  width: 50, resizable: false, align:'center',
						renderer: function(value, p, record) 
						{
							if (value == 1) 
							{
								return '<i class="fa fa-check" aria-hidden="true" style="font-size:13px;color:orange"></i>';
							}
							else 
							{
								return '';
							}
						
						}
				}
				,{header: 'Special', dataIndex: 'NodeSpecial',  width: 80, resizable: false, align:'center',}
				,{header: 'View type', dataIndex: 'NodeViewType',  width: 80, resizable: false, align:'center',}
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getNodesList',
			maxRecords		: me.maxRecords,
			filterStrToFind		: me.filterStrToFind,
			SiteID				: me.SiteID,	
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* homepageRecord
	*
	* Button handler for homepageRecord
	*
	***************************************************************************************************/
	homepageRecord: function()
	{
		var thisgrid = this;
		
		var RecID = thisgrid.getSelectedRecordID();
		var HomePageStatus = thisgrid.getSelectedRecordField('NodeIsHomePage');
		var NodeLevel = thisgrid.getSelectedRecordField('NodeLevel');
		var NodeSiteID = thisgrid.getSelectedRecordField('NodeSiteID');
		
		if(HomePageStatus == 1)
		{
			return false;
		}
		
		if(NodeLevel != 1)
		{
			Ext.Msg.alert('Attention','Only the second level can be set as Home Page!');
			return false;
		}
		
		var actiontext 	= '';
		var responsetext= '';
		var NodeIsHomePage = 0;

		if(RecID <= 0)
		{
			return;
		}
		
		if(HomePageStatus==0)
		{
			actiontext = 'Home Page';
			responsetext = 'set as Home Page';
			var NodeIsHomePage = 1;	
		}

		
		params = 
		{
			appPage			: 'Home',
			appMethod		: 'UpdateNodeRecord',
			NodeID				: RecID,
			NodeIsHomePage	 :NodeIsHomePage,
			NodeSiteID : NodeSiteID
		};
		
		
		Ext.Msg.confirm('Confirm Action', 'Are you sure you want to set this record as your '+actiontext+' ?', 
			function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Ajax.request({
						url: 'appAjax.php',
						method: 'post',
						params:  params,
						success: function(response)
						{
							var text = response.responseText;
							var obj = Ext.JSON.decode(text);
							if (!obj.data) 
							{										
								Ext.Msg.alert('Not '+responsetext+'!', obj.error);
								return;
							}
													
							thisgrid.loadData();
						}
					});
					return true;
				} 
				else 
				{
					return false;
				}
			}
		);		
	},
	
	
	/**************************************************************************************************
	* publishRecord
	*
	* Button handler for publishRecord
	*
	***************************************************************************************************/
	publishRecord: function()
	{
		var thisgrid = this;
		
		var RecID = thisgrid.getSelectedRecordID();
		var PublishStatus = thisgrid.getSelectedRecordField('NodePublished');
		
		var actiontext 	= '';
		var responsetext= '';
		var NodePublished = 0;
		

		if(RecID <= 0)
		{
			return;
		}
		
		if(PublishStatus==0)
		{
			actiontext = 'Publish';
			responsetext = 'Published';
			var NodePublished = 1;
			
		}
		else	
		{
			actiontext = 'Unpublish';
			responsetext = 'Unublished';
		}			
		
		params = 
		{
			appPage			: 'Home',
			appMethod		: 'UpdateNodeRecord',
			NodeID				: RecID,
			NodePublished	: NodePublished,
		};
		
		
		Ext.Msg.confirm('Confirm Action', 'Are you sure you want to '+actiontext+' this record?', 
			function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Ajax.request({
						url: 'appAjax.php',
						method: 'post',
						params:  params,
						success: function(response)
						{
							var text = response.responseText;
							var obj = Ext.JSON.decode(text);
							if (!obj.data) 
							{										
								Ext.Msg.alert('Not '+responsetext+'!', obj.error);
								return;
							}
													
							thisgrid.loadData();
						}
					});
					return true;
				} 
				else 
				{
					return false;
				}
			}
		);		
	},
	
	/**************************************************************************************************
	* menuRecord
	*
	* Button handler for menuRecord
	*
	***************************************************************************************************/
	menuRecord: function()
	{
		var thisgrid = this;
		
		var RecID = thisgrid.getSelectedRecordID();
		var NodeMenu = thisgrid.getSelectedRecordField('NodeMenu');
		
		var actiontext 	= '';
		var responsetext= '';

		if(RecID <= 0)
		{
			return false;
		}
		
		if(NodeMenu==0)
		{
			actiontext = 'Menu';
			responsetext = 'set as Menu';
			var NodeMenu = 1;
			
		}
		else	
		{
			actiontext = 'not Menu';
			responsetext = 'set as not Menu';
			var NodeMenu = 0;
		}	

		
		params = 
		{
			appPage			: 'Home',
			appMethod			: 'UpdateNodeRecord',
			NodeID				: RecID,
			NodeMenu			: NodeMenu,
		};
		
		
		Ext.Msg.confirm('Confirm Action', 'Are you sure you want to set this record as  '+actiontext+' ?', 
			function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Ajax.request({
						url: 'appAjax.php',
						method: 'post',
						params:  params,
						success: function(response)
						{
							var text = response.responseText;
							var obj = Ext.JSON.decode(text);
							if (!obj.data) 
							{										
								Ext.Msg.alert('Not '+responsetext+'!', obj.error);
								return;
							}
													
							thisgrid.loadData();
						}
					});
					return true;
				} 
				else 
				{
					return false;
				}
			}
		);		
	},
	
	/**************************************************************************************************
	* addParent
	*
	* Button handler for Add Parent
	*
	***************************************************************************************************/
	addParent: function(type)
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditNodes; 
		
		x.editRecID		= 0;
		x.SiteID  = thisgrid.SiteID;
		x.NodeLngType = thisgrid.filterLanguage;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };			
		x.show(thisgrid);
		x.loadData();
	},
	
	/**************************************************************************************************
	* addChild
	*
	* Button handler for addChild
	*
	***************************************************************************************************/
	addChild: function(type)
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditNodes; 
		
		x.editRecID		= 0;
		x.SiteID  = thisgrid.SiteID;
		x.NodeLevel  = thisgrid.getSelectedRecordField('NodeLevel') + 1;
		x.NodeNodeID = thisgrid.getSelectedRecordID();
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };			
		x.show(thisgrid);
		x.loadData();
	},
			
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			
			var x = new Home.view.Wnd_NewEditNodes; 
			
			x.editRecID 	= RecID;
			x.SiteID  = thisgrid.SiteID;
			x.NodeNodeID = thisgrid.getSelectedRecordField('NodeNodeID');
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteNodeRecord'
				,NodeID			: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
	
	/**************************************************************************************************
	* getMoveRecordParams
	*
	* params for Ajax request that moves a record
	*
	* Direction is 1 for moving up
	*
	* Direction is 2 for moving down
	*
	***************************************************************************************************/
	getMoveRecordParams: function (direction) 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'MoveNodeRecord'
				,NodeID		: RecID
				,Direction		: direction
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* moveRecord
	*
	* Button handler for Move
	*
	***************************************************************************************************/
	moveRecord: function(direction)
	{
		var thisgrid = this;
		
		var params = thisgrid.getMoveRecordParams(direction);
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Moving', 'Are you sure you want to move this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not moved!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
});