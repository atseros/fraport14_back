﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.FormTemplate']);

Ext.define('Home.view.Frm_Airlines_Translation' ,{
    extend: 'Include.view.FormTemplate',
    alias : 'widget.Frm_Airlines_Translation',
	
	autoScroll: false,                
	layout: 'border',
	
	
	
	/**************************************************************************************************
	* Items
	***************************************************************************************************/
	items: 
	[
        {
            xtype: 'form',
			//frame: true,
			layout: 'border',
			margin: '5 5 5 5',
			border:0,
			region: 'center',
            items: 
			[	
				{
					xtype: 'hiddenfield',
					name: 'SiteAirlineLngID',
				},
				{
					
					xtype: 'radiogroup',    
					fieldLabel: 'Language Selection',
					labelWidth: 150,
					layout: 'hbox',
					region: 'north',
					defaults: 
					{
						flex: 1
					},
					items: 
					[
						{
							xtype: 'radiogroup',
							columns: 4,
							items: 
							[
								{
									xtype: 'radiofield',
									name: 'SiteAirlineLngType',
									boxLabel: 'English',
									checked: true,
									inputValue: 1
								},
								{
									xtype: 'radiofield',
									name: 'SiteAirlineLngType',
									boxLabel: 'Greek',
									inputValue: 2
								},
								{
									xtype: 'radiofield',
									name: 'SiteAirlineLngType',
									boxLabel: 'Deutch',
									inputValue: 3
								},
								{
									xtype: 'radiofield',
									name: 'SiteAirlineLngType',
									boxLabel: 'Russian',
									inputValue: 4
								},
								
							],
							listeners: 
							{
								change : function(obj, value)
								{ 
									var formPanel = this.up('form').up('panel');
									
									var val = value.SiteAirlineLngType;              
									formPanel.SiteAirlineLngType = val;
									formPanel.loadData();
								}
							}
							
						}
					]
					
				},
				{
					xtype: 'panel',
					border: 0,
					layout: 'border',
					region: 'center',
					items:
					[	
						// Line Separator
						{
							xtype: 'container',
							html: '<hr>',
							height:10
						},
						// {
							// xtype: 'container',
							// layout: 'hbox',
							// region: 'north',
							// margin: '10 0 0 0',
							// height: 22,
							// items:
							// [
								// {
									// xtype: 'label',
									// text: 'Check In Counters',
									// cls: 'boldtitle'
								// },
							// ]
						// },
						// // Text field:  SiteAirlineLngCheckInCounters
						// {
							// xtype: 'textfield',
							// name: 'SiteAirlineLngCheckInCounters',
							// border: 0,
							// layout: 'border',
							// region :'north',
						// },
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '10 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Airport Phones',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SiteAirlineLngAirportPhones
						{
							xtype: 'textfield',
							name: 'SiteAirlineLngAirportPhones',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '10 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Cargo Services',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SiteAirlineLngReservations
						{
							xtype: 'textfield',
							name: 'SiteAirlineLngReservations',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '10 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Lost & Found',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SiteAirlineLngLostAndFound
						{
							xtype: 'textfield',
							name: 'SiteAirlineLngLostAndFound',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '10 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Handler',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SiteAirlineLngHandler
						{
							xtype: 'textfield',
							name: 'SiteAirlineLngHandler',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '10 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Handler Airport Phone',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SiteAirlineLngHandlerAirportPhone
						{
							xtype: 'textfield',
							name: 'SiteAirlineLngHandlerAirportPhone',
							border: 0,
							layout: 'border',
							region :'north',
						},
						{
							xtype: 'container',
							layout: 'hbox',
							region: 'north',
							margin: '10 0 0 0',
							height: 22,
							items:
							[
								{
									xtype: 'label',
									text: 'Email',
									cls: 'boldtitle'
								},
							]
						},
						// Text field:  SiteAirlineLngEmail
						{
							xtype: 'textfield',
							name: 'SiteAirlineLngEmail',
							border: 0,
							layout: 'border',
							region :'north',
						},
					]
				},
				{
					xtype: 'container',
					layout: 'hbox',
					region: 'south',
					margin: '5 0 0 0',
					height: 30,
					items:
					[
						
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'fa fa-floppy-o',
							margin: '0 0 0 0',
							text: 'Save',
							handler: function() 
							{
								var formPanel = this.up('form').up('panel');
								formPanel.Save();
							}
						}
					]
				},
				
			]	
        }       
    ],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.SiteAirlineID = 0;		
		
		this.SiteAirlineLngID = 0;		
		
		this.SiteAirlineLngType = 1;	
				
		this.fieldNameID = 'SiteAirlineLngID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				 
			}					
		};						

		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Home',
			appMethod: 'get_SiteAirlineLngRecord',
			SiteAirlineLngSiteAirlineID: me.SiteAirlineID,
			SiteAirlineLngType: me.SiteAirlineLngType,
			
		};
		
		return params;
	},
	
	
	/**************************************************************************************************
	* afterLoadDefault
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		var me = this;
		var thisf = me.query('form')[0];
		
		thisf.getForm().setValues(data);
		
		// var id = thisf.getForm().getValues().ShopLngID;
		// var tinymce_editor1 = this.query('my_tinymce_textarea[name=ShopLngText]')[0];
		// tinymce_editor1.submitUrl='appAjax.php?appPage=Home&appMethod=handleTinyMCE_UploadImage&table_name=sys_shoplng&id=' + id;

	},
	
	/**************************************************************************************************
	* Save
	***************************************************************************************************/
	Save: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var me = this;
		
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();
		
		// Update Current Record 
		Ext.Ajax.request({
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			timeout: 60000,
			params: 
			{
				appPage			: 'Home',
				appMethod			: 'UpdateSiteAirlineLngRecord',
				SiteAirlineLngID		: values.SiteAirlineLngID,
				SiteAirlineLngType		: this.SiteAirlineLngType,					
				SiteAirlineLngSiteAirlineID: this.SiteAirlineID,
				// SiteAirlineLngCheckInCounters		: values.SiteAirlineLngCheckInCounters,					
				SiteAirlineLngAirportPhones		: values.SiteAirlineLngAirportPhones,					
				SiteAirlineLngReservations		: values.SiteAirlineLngReservations,					
				SiteAirlineLngLostAndFound		: values.SiteAirlineLngLostAndFound,					
				SiteAirlineLngHandler		: values.SiteAirlineLngHandler,					
				SiteAirlineLngHandlerAirportPhone		: values.SiteAirlineLngHandlerAirportPhone,					
				SiteAirlineLngEmail		: values.SiteAirlineLngEmail,					
			
			},
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{
					myMask.hide();
					//me.close();
					console.log(obj.data);
					//me.callbackRefresh(obj.data);
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				//me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{				
				myMask.hide();	
				//me.close();
				Ext.Msg.alert('Error', 'Error in saving data !');
				return;				
			}   		
		});
	},
	

	
});





