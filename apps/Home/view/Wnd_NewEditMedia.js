﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditMedia', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditMedia',
		
	layout: 'border',
	title: 'Media Data',
    height: 160,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 60,
				region :'north',
           },
			items: 
			[
				// Hidden field:  MediaID
				{
					xtype: 'hiddenfield',
					name: 'MediaID',
				},
				// Hidden field:  MediaSiteID
				{
					xtype: 'hiddenfield',
					name: 'MediaSiteID',
				},
				// Hidden field:  MediaType
				{
					xtype: 'hiddenfield',
					name: 'MediaType',
				},
				//File
				{
					xtype: 'filefield',
					name: 'File',
					fieldLabel: 'File',
					border: 0,
					layout: 'border',
					region :'north',
					regex: / /,
					regexText: '',
					allowBlank: false
				},
				// Text field:  MediaUrl
				{
					xtype: 'textfield',
					name: 'MediaUrl',
					fieldLabel: 'Full Url',
					emptyText : 'Youtube url ie https://www.youtube.com/watch?v=UvI7YhTSQNs',
					allowBlank: false
				},
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},				
				// Text field:  MediaTitle
				{
					xtype: 'textfield',
					name: 'MediaTitle',
					fieldLabel: 'Title',
					allowBlank: false
				},	
				
											
				
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.SiteID = 0;

		this.MediaType = 0;		
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod	: 'get_Media_NewRecordDefValues',
			SiteID			:this.SiteID,
			MediaType		:this.MediaType,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
		
		var form = this.query('form')[0];
			
		var values = form.getValues();
	
		// do anything else here
		var File = this.query("filefield[name=File]")[0];
		var VideoUrl = this.query("textfield[name=MediaUrl]")[0];
		
		if (values.MediaType == 1)
		{
			File.regex = /^.*\.(jpg|JPG|gif|GIF|png|PNG|bmp|BMP)$/;
			File.regexText = 'Only JPG,GIF,PNG,BMP files allowed';
			VideoUrl.allowBlank = true;
			VideoUrl.hide();
		}
		else if (values.MediaType == 2)
		{
			File.hide();
			File.allowBlank = true;
			VideoUrl.show();
		}
		else if (values.MediaType == 3)
		{
			File.regex = /^.*\.(doc|DOC|docx|DOCX|xls|XLS|xlsx|XLSX|ppt|PPT|pps|PPS|pdf|PDF|rtf|RTF)$/;
			File.regexText = 'Only DOC,DOCX,XLS,XLSX,PPT,PPS,PDF,RTF files allowed';
			VideoUrl.allowBlank = true;
			VideoUrl.hide();
		}
	
		
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod	: 'get_MediaRecord',
			MediaID		: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		var form = this.query('form')[0];
			
		var values = form.getValues();

		var File = this.query("filefield[name=File]")[0];
		var VideoUrl = this.query("textfield[name=MediaUrl]")[0];
		
		if (values.MediaType == 1)
		{
			File.regex = /^.*\.(jpg|JPG|gif|GIF|png|PNG|bmp|BMP)$/;
			File.regexText = 'Only JPG,GIF,PNG,BMP files allowed';
			VideoUrl.hide();
		}
		else if (values.MediaType == 2)
		{
			File.hide();
			VideoUrl.show();
		}
		else if (values.MediaType == 3)
		{
			File.regex = /^.*\.(doc|DOC|docx|DOCX|xls|XLS|xlsx|XLSX|ppt|PPT|pps|PPS|pdf|PDF|rtf|RTF)$/;
			File.regexText = 'Only DOC,DOCX,XLS,XLSX,PPT,PPS,PDF,RTF files allowed';
			VideoUrl.hide();
		}
	
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;
		
		me.DataAndImage();
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* DataAndImage
	*
	* function that uploades Images and add data
	*
	***************************************************************************************************/
	DataAndImage: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();
		
		
		if (me.editRecID == 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=InsertMediaRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been added.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not add.');                    
					}
				});
			}
			
		}
		else	if (me.editRecID > 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=UpdateMediaRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been updated.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not update.');                    
					}
				});
			}
			
		}			

        
	},
	
});