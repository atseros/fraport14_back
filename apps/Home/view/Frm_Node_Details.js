﻿// Require Popup Form of Basic Info
//Ext.require([ 'ProposalEvaluation.view.Wnd_NewEditBasicInfo']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.FormTemplate']);

Ext.define('Home.view.Frm_Node_Details' ,{
    extend: 'Include.view.FormTemplate',
    alias : 'widget.Frm_Node_Details',
	
	autoScroll: false,                
	layout: 'border',
	
	
	
	/**************************************************************************************************
	* Items
	***************************************************************************************************/
	items: 
	[
        {
            xtype: 'form',
			//frame: true,
			layout: 'border',
			margin: '5 5 5 5',
			border:0,
			region: 'center',
            items: 
			[	
				{
					xtype: 'hiddenfield',
					name: 'NodeLngID',
				},
				{
					
					xtype: 'radiogroup',    
					fieldLabel: 'Language Selection',
					labelWidth: 150,
					layout: 'hbox',
					region: 'north',
					defaults: 
					{
						flex: 1
					},
					items: 
					[
						{
							xtype: 'radiogroup',
							columns: 4,
							items: 
							[
								{
									xtype: 'radiofield',
									name: 'NodeLangType',
									boxLabel: 'English',
									checked: true,
									inputValue: 1
								},
								{
									xtype: 'radiofield',
									name: 'NodeLangType',
									boxLabel: 'Greek',
									inputValue: 2
								},
								{
									xtype: 'radiofield',
									name: 'NodeLangType',
									boxLabel: 'Deutch',
									inputValue: 3
								},
								{
									xtype: 'radiofield',
									name: 'NodeLangType',
									boxLabel: 'Russian',
									inputValue: 4
								},
								
							],
							listeners: 
							{
								change : function(obj, value)
								{ 
									var formPanel = this.up('form').up('panel');
									
									var val = value.NodeLangType;              
									formPanel.NodeLngType = val;
									formPanel.loadData();
									// Load Images List
									Ext.ComponentManager.get('MList_Node_ImagesSitePages').NodeLngType = val;
									Ext.ComponentManager.get('MList_Node_ImagesSitePages').loadData();
									// Load Videos List
									Ext.ComponentManager.get('MList_Node_VideosSitePages').NodeLngType = val;
									Ext.ComponentManager.get('MList_Node_VideosSitePages').loadData();
									// Load Images List
									Ext.ComponentManager.get('MList_Node_DocumentsSitePages').NodeLngType = val;
									Ext.ComponentManager.get('MList_Node_DocumentsSitePages').loadData();
									// Load Slider List
									Ext.ComponentManager.get('MList_Node_Slider_Images').NodeLngType = val;
									Ext.ComponentManager.get('MList_Node_Slider_Images').loadData();
									// Load Related Link List
									Ext.ComponentManager.get('MList_Node_RelatedLinksSitePages').NodeLngType = val;
									Ext.ComponentManager.get('MList_Node_RelatedLinksSitePages').loadData();
									
								}
							}
							
						}
					]
					
				},
				{
					xtype: 'tabpanel',
					layout: 'border',
					region: 'center',
					deferredRender: false,
					title: 'Content',
					split:true,
					items:
					[
						
						{
							xtype: 'panel',
							border: 0,
							layout: 'border',
							title: 'Basic details',
							items:
							[	
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Title',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  NodeLngTitle
								{
									xtype: 'textfield',
									name: 'NodeLngTitle',
									border: 0,
									layout: 'border',
									region :'north',
								},
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Intro',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  NodeLngIntro
								{
									xtype: 'textfield',
									name: 'NodeLngIntro',
									border: 0,
									layout: 'border',
									region :'north',
								},
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Story',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  NodeLngStory
								{
									xtype: 'my_tinymce_textarea',
									flex: 1,
									name: 'NodeLngStory',
									border: 0,
									layout: 'border',
									region :'center',
									split: true,
									
								},
							]
						},
						{
							xtype: 'panel',
							id: '',
							border: 0,
							layout: 'border',
							title: 'Slider',
							items:
							[	
								{
									xtype: 'MList_Node_Slider_Images',
									id: 'MList_Node_Slider_Images',
									title: 'Slider Images',
									layout: 'border',
									region: 'center',
									split: true,
									flex: 1,
								},
							]	
						},	
						{
							xtype: 'panel',
							id: '',
							border: 0,
							layout: 'border',
							title: 'Media',
							items:
							[
								{
									xtype: 'MList_Node_Images',
									id: 'MList_Node_ImagesSitePages',
									title: 'Node Images',
									layout: 'border',
									region: 'north',
									split: true,
									flex: 1,
								},
								{
									xtype: 'MList_Node_Videos',
									id: 'MList_Node_VideosSitePages',
									title: 'Node Videos',
									layout: 'border',
									region: 'center',
									split: true,
									flex: 1,
								},
								{
									xtype: 'MList_Node_Documents',
									id: 'MList_Node_DocumentsSitePages',
									title: 'Node Documents',
									layout: 'border',
									region: 'south',
									split: true,
									flex: 1,
								},
								
								
							]
							
						},
						{
							xtype: 'panel',
							id: '',
							border: 0,
							layout: 'border',
							title: 'Related Links',
							items:
							[	
								{
									xtype: 'MList_Node_RelatedLinks',
									id: 'MList_Node_RelatedLinksSitePages',
									title: 'Related Links',
									layout: 'border',
									region: 'center',
									split: true,
									flex: 1,
								},
							]	
						},	
						{
							xtype: 'panel',
							id: '',
							border: 0,
							layout: 'border',
							title: 'Meta',
							items:
							[
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Meta Title',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  NodeLngMetaTitle
								{
									xtype: 'textfield',
									name: 'NodeLngMetaTitle',
									border: 0,
									layout: 'border',
									region :'north',
								},
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Meta Keywords',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  NodeLngMetaKeywords
								{
									xtype: 'textfield',
									name: 'NodeLngMetaKeywords',
									border: 0,
									layout: 'border',
									region :'north',
								},
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Meta Description',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  NodeLngMetaDescription
								{
									xtype: 'textfield',
									name: 'NodeLngMetaDescription',
									border: 0,
									layout: 'border',
									region :'north',
								},
								
							]
						},
						
					]
				},	
				{
					xtype: 'container',
					layout: 'hbox',
					region: 'south',
					margin: '5 0 0 0',
					height: 30,
					items:
					[
						
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'fa fa-floppy-o',
							margin: '0 0 0 0',
							text: 'Save',
							handler: function() 
							{
								var formPanel = this.up('form').up('panel');
								formPanel.Save();
							}
						}
					]
				},
				
			]	
        }       
    ],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.NodeID = 0;		
		
		this.NodeLngID = 0;		
		
		this.NodeLngType = 1;	
				
		this.fieldNameID = 'NodeLngID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				 
			}					
		};						

		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Home',
			appMethod: 'get_NodeLngRecord',
			NodeLngNodeID: me.NodeID,
			NodeLngType: me.NodeLngType,
			
		};
		
		return params;
	},
	
	
	/**************************************************************************************************
	* afterLoadDefault
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		var me = this;
		var thisf = me.query('form')[0];
		
		thisf.getForm().setValues(data);
		
		var id = thisf.getForm().getValues().NodeLngID;
		var tinymce_editor1 = this.query('my_tinymce_textarea[name=NodeLngStory]')[0];
		tinymce_editor1.submitUrl='appAjax.php?appPage=Home&appMethod=handleTinyMCE_UploadImage&table_name=sys_nodelng&id=' + id;
		
		if (me.NodeLngType == 1)
		{
			// Load Images List
			Ext.ComponentManager.get('MList_Node_ImagesSitePages').NodeLngType = 1;
			Ext.ComponentManager.get('MList_Node_ImagesSitePages').loadData();
			// Load Videos List
			Ext.ComponentManager.get('MList_Node_VideosSitePages').NodeLngType = 1;
			Ext.ComponentManager.get('MList_Node_VideosSitePages').loadData();
			// Load Images List
			Ext.ComponentManager.get('MList_Node_DocumentsSitePages').NodeLngType = 1;
			Ext.ComponentManager.get('MList_Node_DocumentsSitePages').loadData();

			// Load Slider List
			Ext.ComponentManager.get('MList_Node_Slider_Images').NodeLngType = 1;
			Ext.ComponentManager.get('MList_Node_Slider_Images').loadData();

			// Load Related Link List
			Ext.ComponentManager.get('MList_Node_RelatedLinksSitePages').NodeLngType = 1;
			Ext.ComponentManager.get('MList_Node_RelatedLinksSitePages').loadData();

		}	
		
	},
	
	/**************************************************************************************************
	* Save
	***************************************************************************************************/
	Save: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var me = this;
		
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();
		
		// Update Current Record 
		Ext.Ajax.request({
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			timeout: 60000,
			params: 
			{
				appPage			: 'Home',
				appMethod			: 'UpdateNodeLngRecord',
				NodeLngID			: values.NodeLngID,
				NodeLngType		: this.NodeLngType,					
				NodeLngNodeID	: this.NodeID,
				NodeLngTitle		: values.NodeLngTitle,
				NodeLngIntro		: values.NodeLngIntro,						
				NodeLngStory		: values.NodeLngStory,	
				NodeLngMetaTitle			: values.NodeLngMetaTitle,						
				NodeLngMetaKeywords		: values.NodeLngMetaKeywords,						
				NodeLngMetaDescription	: values.NodeLngMetaDescription						
			
			},
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{
					myMask.hide();
					//me.close();
					console.log(obj.data);
					//me.callbackRefresh(obj.data);
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				//me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{				
				myMask.hide();	
				//me.close();
				Ext.Msg.alert('Error', 'Error in saving data !');
				return;				
			}   		
		});
	},
	

	
});





