﻿// // Require Combo of Nodes 
Ext.require([ 'Home.view.ComboGrid_SelectNode']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditFooterData', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditFooterData',
		
	layout: 'border',
	title: 'Footer Data',
    height: 220,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[
				// Hidden field:  FtrDataID
				{
					xtype: 'hiddenfield',
					name: 'FtrDataID',
					fieldLabel: 'FtrDataID',
				},
				// Hidden field:  FtrDataFtrID
				{
					xtype: 'hiddenfield',
					name: 'FtrDataFtrID',
					fieldLabel: 'FtrDataFtrID',
				},
				// Combo field: FtrDataStatus
				{
					xtype: 'combo',    
					name: 'FtrDataStatus',
					forceSelection: true,
					fieldLabel: 'Status',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},				
				// Text field:  FtrDataPriority
				{
					xtype: 'numberfield',
					name: 'FtrDataPriority',
					fieldLabel: 'Priority',
					border: 0,
					layout: 'border',
					value : 100,
				},
				// Text field:  FtrDataTitle
				{
					xtype: 'textfield',
					name: 'FtrDataTitle',
					fieldLabel: 'Title',
				},
				// Combo field:  FtrDataNodeID
				{
					xtype: 'container',
					region :'north',
					layout: { type: 'hbox' },
					items:
					[
							{
								xtype: 'hiddenfield',
								fieldLabel: 'FtrDataNodeID',
								name: 'FtrDataNodeID',
								itemId: 'FtrDataNodeID',
								value: 0
							},  
							{
								xtype: 'textfield',
								fieldLabel: 'Link',
								name: 'NodeTitle',
								itemId: 'NodeTitle',                    
								flex: 19,
								labelWidth: 100,
								allowBlank: true,
								readOnly: true,
								maxLength: 255,
								cls: 'readonly_combos',

							},
							{
								xtype: 'button',
								text: '.',
								itemId: 'btnSelect',	
								width: 18,											
								handler: function() 
								{
									var x = new Home.view.ComboGrid_SelectNode;
									var thisgrid = this.up('form').up('panel');
									x.SiteID = thisgrid.SiteID;					
									x.show(this);
									// if in Edit Mode, set already saved RelatedLinkNodeID
									var thisform = this.up('form');
									x.SelectedRecordID = thisform.getValues().FtrDataNodeID;
									
									x.callbackAfterSelect = function(id, name)
															{ 
																thisform.getForm().findField('FtrDataNodeID').setValue(id);
																thisform.getForm().findField('NodeTitle').setValue(name);
															};
									x.loadData();
								}					
							},
								
					]
				},				
				// Text field:  FtrDataExternalLink
				// {
					// xtype: 'textfield',
					// name: 'FtrDataExternalLink',
					// fieldLabel: 'External link',
				// },				
				// Text field:  FtrDataIcon
				{
					xtype: 'textfield',
					name: 'FtrDataIcon',
					fieldLabel: 'Icon',
				},				
				
								
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.FtrID = 0;			
			    
		this.SiteID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_FooterData_NewRecordDefValues',
			FtrID				:this.FtrID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_FooterDataRecord',
			FtrDataID		: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'InsertFooterDataRecord',
			FtrDataID:					values.FtrDataID,
			FtrDataFtrID:				values.FtrDataFtrID,
			FtrDataStatus:			values.FtrDataStatus,
			FtrDataPriority:			values.FtrDataPriority,
			FtrDataTitle:				values.FtrDataTitle,
			FtrDataNodeID:			values.FtrDataNodeID,
			// FtrDataExternalLink:	values.FtrDataExternalLink,
			FtrDataIcon:				values.FtrDataIcon,
		
		};

		return params;
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'UpdateFooterDataRecord',
			FtrDataID:					values.FtrDataID,
			FtrDataStatus:			values.FtrDataStatus,
			FtrDataPriority:			values.FtrDataPriority,
			FtrDataTitle:				values.FtrDataTitle,
			FtrDataNodeID:			values.FtrDataNodeID,
			// FtrDataExternalLink:	values.FtrDataExternalLink,
			FtrDataIcon:				values.FtrDataIcon,

		};
		
		return params;
		
	},
	
});