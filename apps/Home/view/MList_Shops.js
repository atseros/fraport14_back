﻿// // Require Popup Form of Shop (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditShop']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Shops' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Shops',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					xtype: 'button',
					text: 'Show All',
					itemId: '',
					iconCls: 'fa fa-filter',
					enableToggle: true,
					pressed: false,
					handler: function(button) 
					{ 
						var gridpanel = this.up('panel'); 
						
						if(!button.pressed)
							gridpanel.filterShowAll = 0;
						else 
							gridpanel.filterShowAll = 1;
						gridpanel.loadData();	
					}					
				},
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				250 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 20'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				},
				{
					xtype: 'combo',    
					fieldLabel: '',														
					width:150,
					value:"-1",
					margin: '0 0 0 5',
					store:
					{
						fields: 
						[
							{name: 'id', type: 'string'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: -1, Descr: 'All'}
							,{ id: 1, Descr: 'Shops'}
							,{ id: 2, Descr: 'Restaurants'}
							,{ id: 3, Descr: 'Duty Free'}
							,{ id: 4, Descr: 'Business Lounge'}
							
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr',
					listeners:
					{
						'change': function(field, newValue)
						{
							var gridpanel = this.up('panel'); 
							gridpanel.filterType = newValue;
							gridpanel.loadData();
						}
					}
				}
				,{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},		
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				}
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterShowAll	= 0;	
		this.filterType = -1;		
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'ShopID';
		
		this.SiteID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('ShopID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'ShopID', type: 'number', defaultValue: 0 }
				,{name: 'ShopSiteID', type: 'number', defaultValue: 0 }
				,{name: 'ShopTitle', type: 'string' }
				,{name: 'ShopPayment', type: 'string'}
				,{name: 'ShopImgFilename', type: 'string'}
				,{name: 'ShopPriority', type: 'number', defaultValue: 0 }
				,{name: 'ShopStatus', type: 'number', defaultValue: 0 }
				,{name: 'ShopDisabilityAccess', type: 'number', defaultValue: 0 }
				,{name: 'ShopType', type: 'number', defaultValue: 0 }
				,{name: 'ShopFullUrl', type: 'string'}
				,{name: 'ShopFullUrlLogo', type: 'string'}
				,{name: 'ShopPayment', type: 'string'}
				,{name: 'ShopPhone', type: 'string'}
				,{name: 'ShopEscapeDefault', type: 'string'}
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Status', dataIndex: 'ShopStatus',  width: 40, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 2) 
						{
							return '<i class="fa fa-times" aria-hidden="true" style="font-size:16px;color:red"></i>';
						}
						else 
						{
							return '<i class="fa fa-check" aria-hidden="true" style="font-size:16px;color:green"></i>';
						}
							
					}
				},
				// {header: 'Logo', dataIndex: 'ShopFullUrlLogo',  width: 40, resizable:false,
					// renderer: function(value, p, record) 
					// {
							// return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					// }
				 // },
				{header: 'Image', dataIndex: 'ShopFullUrl',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					}
				 },								
				{ header: 'Priority', dataIndex: 'ShopPriority', width: 50,},
				{header: 'Category', dataIndex: 'ShopType',  width: 70, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 1) 
						{
							return 'Shop';
						}
						else if (value == 2) 
						{
							return 'Restaurant';
						}
						else if (value == 3) 
						{
							return 'Duty Free';
						}
						else if (value == 4) 
						{
							return 'Business Lounge';
						}
							
					}
				},
				{ header: 'Title', dataIndex: 'ShopTitle', flex: 20/100,},
				{ header: 'Phone', dataIndex: 'ShopPhone', flex: 20/100,},
				{ header: 'Escape Default', dataIndex: 'ShopEscapeDefault', flex: 20/100,},
				// { header: 'Payment', dataIndex: 'ShopPayment', flex: 15/100,},
				// {header: 'D.A.', dataIndex: 'ShopDisabilityAccess',  width: 35, resizable: false,
					// renderer: function(value, p, record) 
					// {
						// if (value == 0) 
						// {
							// return 'No';
						// }
						// else 
						// {
							// return 'Yes';
						// }
							
					// }
				// }
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getShopsList',
			maxRecords		: me.maxRecords,
			filterStrToFind		: me.filterStrToFind,
			filterShowAll		: me.filterShowAll,
			filterType			: me.filterType,
			SiteID				: me.SiteID,	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditShop; 		
		x.editRecID = 0;
		x.SiteID = thisgrid.SiteID;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditShop; 
			
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteShopRecord'
				,ShopID			: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
});