﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.ComboGrid_SelectAirport_AirportList' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.ComboGrid_SelectAirport_AirportList',
	
	autoScroll: false,                
	layout: 'border',
		
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind = '';
		this.maxRecords = 200;
				
		this.fieldNameID = 'AirportID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				 
			}					
		};		
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				{name: 'SiteAirportName', type: 'string'}
				,{name: 'SiteAirportCode', type: 'string'}
				
			],			
			proxy: 
			{
				type: 'ajax',
				url: '', 
				method : 'POST',
				timeout : 60000,						
				headers : { 'Content-Type' : 'application/json' },
				reader: {
					type: 'json',
					root: 'data'
				},
				simpleSortMode: true,				
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: true,
			},
			items: 
			[					
					{header: 'Airport Name', dataIndex: 'SiteAirportName',  flex: 70/100},
					{header: 'Airport Code', dataIndex: 'SiteAirportCode',  flex: 30/100},
					
			]
		};
		

		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Home',
			appMethod: 'get_SiteAirportsList',
		};
		
		return params;
	},
		
});





