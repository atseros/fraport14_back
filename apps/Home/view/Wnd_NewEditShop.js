﻿// // Require Combo of Templates 
Ext.require([ 'Home.view.ComboGrid_SelectTemplate']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditShop', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditShop',
		
	layout: 'border',
	title: 'Shop / Restaurant Data',
    height: 320,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[
				// Hidden field:  ShopID
				{
					xtype: 'hiddenfield',
					name: 'ShopID',
					fieldLabel: 'ShopID',
				},
				// Hidden field:  ShopSiteID
				{
					xtype: 'hiddenfield',
					name: 'ShopSiteID',
					fieldLabel: 'ShopID',
				},
				//Logo
				// {
					// xtype: 'filefield',
					// name: 'Logo',
					// fieldLabel: 'Logo',
					// border: 0,
					// layout: 'border',
					// region :'north',
									
				// },
				//Image
				{
					xtype: 'filefield',
					name: 'File',
					fieldLabel: 'Image',
					border: 0,
					layout: 'border',
					region :'north',
									
				},
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},
				// Combo field: ShopStatus
				{
					xtype: 'combo',    
					name: 'ShopStatus',
					forceSelection: true,
					fieldLabel: 'Status',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},				
				// Text field:  ShopPriority
				{
					xtype: 'numberfield',
					name: 'ShopPriority',
					fieldLabel: 'Priority',
					border: 0,
					layout: 'border',
					value : 100,
				},
				// Combo field: ShopType
				{
					xtype: 'combo',    
					name: 'ShopType',
					forceSelection: true,
					fieldLabel: 'Category',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Shop'},
							{ id: 2, Descr: 'Restaurant'},
							{ id: 3, Descr: 'Duty Free'},
							{ id: 4, Descr: 'Business Lounge'},
						
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},	
				// Text field:  ShopTitle
				{
					xtype: 'textfield',
					name: 'ShopTitle',
					fieldLabel: 'Title',
				},				
				// Text field:  ShopPhone
				{
					xtype: 'textfield',
					name: 'ShopPhone',
					fieldLabel: 'Phone',
				},				
				// Text field:  ShopEscapeDefault
				{
					xtype: 'textfield',
					name: 'ShopEscapeDefault',
					fieldLabel: 'Escape Default',
				},				
				// // Text field:  ShopPayment
				// {
					// xtype: 'textfield',
					// name: 'ShopPayment',
					// fieldLabel: 'Payment',
				// },
				// // Combo field: ShopDisabilityAccess
				// {
					// xtype: 'combo',    
					// name: 'ShopDisabilityAccess',
					// forceSelection: true,
					// fieldLabel: 'Disability Access',
					// cls: 'readonly_combos',
					// editable: false,
					// store:
					// {
						// fields: 
						// [
							// {name: 'id', type: 'int'},
							// {name: 'Descr', type: 'string'}
						// ],
						// data: 
						// [
							// { id: 1, Descr: 'Yes'},
							// { id: 0, Descr: 'No'},
						// ],
						// autoLoad: true
					// },
					// queryMode:'local',
					// valueField: 'id',
					// displayField: 'Descr'
				// },					
				
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.SiteID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_Shop_NewRecordDefValues',
			SiteID			:this.SiteID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_ShopRecord',
			ShopID			: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;
		
		me.DataAndImage();
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* DataAndImage
	*
	* function that uploades Images and add data
	*
	***************************************************************************************************/
	DataAndImage: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();
		
		
		if (me.editRecID == 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=InsertShopRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been added.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not add.');                    
					}
				});
			}
			
		}
		else	if (me.editRecID > 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Home&appMethod=UpdateShopRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been updated.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not update.');                    
					}
				});
			}
			
		}			

        
	},
	
});