﻿// Require Popup Form of Shop (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditShopLng']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.FormTemplate']);

Ext.define('Home.view.Frm_Shop_Details' ,{
    extend: 'Include.view.FormTemplate',
    alias : 'widget.Frm_Shop_Details',
	
	autoScroll: false,                
	layout: 'border',
	
	
	
	/**************************************************************************************************
	* Items
	***************************************************************************************************/
	items: 
	[
        {
            xtype: 'form',
			//frame: true,
			layout: 'border',
			margin: '5 5 5 5',
			border:0,
			region: 'center',
            items: 
			[	
				{
					xtype: 'hiddenfield',
					name: 'ShopLngID',
				},
				// Hidden field:  ShopLngImgFilenameLogoPath
				{
					xtype: 'hiddenfield',
					name: 'ShopLngImgFilenameLogoPath',
				},
				{
					
					xtype: 'radiogroup',    
					fieldLabel: 'Language Selection',
					labelWidth: 150,
					layout: 'hbox',
					region: 'north',
					defaults: 
					{
						flex: 1
					},
					items: 
					[
						{
							xtype: 'radiogroup',
							columns: 4,
							items: 
							[
								{
									xtype: 'radiofield',
									name: 'ShopLngType',
									boxLabel: 'English',
									checked: true,
									inputValue: 1
								},
								{
									xtype: 'radiofield',
									name: 'ShopLngType',
									boxLabel: 'Greek',
									inputValue: 2
								},
								{
									xtype: 'radiofield',
									name: 'ShopLngType',
									boxLabel: 'Deutch',
									inputValue: 3
								},
								{
									xtype: 'radiofield',
									name: 'ShopLngType',
									boxLabel: 'Russian',
									inputValue: 4
								},
								
							],
							listeners: 
							{
								change : function(obj, value)
								{ 
									var formPanel = this.up('form').up('panel');
									
									var val = value.ShopLngType;              
									formPanel.ShopLngType = val;
									formPanel.loadData();
								}
							}
							
						}
					]
					
				},
				
						
						{
							xtype: 'panel',
							border: 0,
							layout: 'border',
							title: 'Basic details',
							region: 'center',
							items:
							[	
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 50,
									items:
									[
										{
											xtype: 'label',
											text: 'Logo',
											cls: 'boldtitle'
										},
										{
											xtype: 'tbfill'
										},
										// container:  ShopLngImgFilenameLogoShow
										{
											xtype: 'image',
											margin: '0 10 0 10',
											name: 'ShopLngImgFilenameLogoShow',
											height: 50,
											autoEl: 'div',
											style: 'overflow:hidden;',
											autoScroll: true
										},
										{
											xtype: 'tbfill'
										},
										{
											 xtype:		'button'
											,text:		'Edit'
											,iconCls: 'fa fa-pencil-square-o'
											,handler:	function() 
											{ 
												var formPanel = this.up('form').up('panel');
												
												formPanel.editRecord();
											}					
										},
									]
								},
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Title',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  ShopLngTitle
								{
									xtype: 'textfield',
									name: 'ShopLngTitle',
									border: 0,
									layout: 'border',
									region :'north',
								},
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Subtitle',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  ShopLngSubtitle
								{
									xtype: 'textfield',
									name: 'ShopLngSubtitle',
									border: 0,
									layout: 'border',
									region :'north',
								},
								// {
									// xtype: 'container',
									// layout: 'hbox',
									// region: 'north',
									// margin: '5 0 0 0',
									// height: 22,
									// items:
									// [
										// {
											// xtype: 'label',
											// text: 'Description',
											// cls: 'boldtitle'
										// },
									// ]
								// },
								// // Text field: ShopLngDictionaryDescription
								// {
									// xtype: 'textfield',
									// name: 'ShopLngDictionaryDescription',
									// border: 0,
									// layout: 'border',
									// region :'north',
								// },
								{
									xtype: 'container',
									layout: 'hbox',
									region: 'north',
									margin: '5 0 0 0',
									height: 22,
									items:
									[
										{
											xtype: 'label',
											text: 'Text',
											cls: 'boldtitle'
										},
									]
								},
								// Text field:  ShopLngText
								{
									xtype: 'my_tinymce_textarea',
									flex: 1,
									name: 'ShopLngText',
									border: 0,
									layout: 'border',
									region :'center',
									split: true,
									
								},
							]
						},
			
				{
					xtype: 'container',
					layout: 'hbox',
					region: 'south',
					margin: '5 0 0 0',
					height: 30,
					items:
					[
						
						{
							xtype: 'tbfill'
						},
						{
							xtype: 'button',
							iconCls: 'fa fa-floppy-o',
							margin: '0 0 0 0',
							text: 'Save',
							handler: function() 
							{
								var formPanel = this.up('form').up('panel');
								formPanel.Save();
							}
						}
					]
				},
				
			]	
        }       
    ],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.ShopID = 0;		
		
		this.ShopLngID = 0;		
		
		this.ShopLngType = 1;	
				
		this.fieldNameID = 'ShopLngID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				 
			}					
		};						

		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Home',
			appMethod: 'get_ShopLngRecord',
			ShopLngShopID: me.ShopID,
			ShopLngType: me.ShopLngType,
			
		};
		
		return params;
	},
	
	
	/**************************************************************************************************
	* afterLoadDefault
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
		
		var me = this;
		var thisf = me.query('form')[0];
		var values = thisf.getValues();
		
		thisf.getForm().setValues(data);
		
		var id = thisf.getForm().getValues().ShopLngID;
		var tinymce_editor1 = this.query('my_tinymce_textarea[name=ShopLngText]')[0];
		tinymce_editor1.submitUrl='appAjax.php?appPage=Home&appMethod=handleTinyMCE_UploadImage&table_name=sys_shoplng&id=' + id;
		
		var Img = Ext.ComponentQuery.query('image[name=ShopLngImgFilenameLogoShow]')[0]; 
		
		var ImgPath = values.ShopLngImgFilenameLogoPath;
				
		if(ImgPath != '')
			Img.setSrc(''+ImgPath+'');
		else
			Img.setSrc('');
	},
	
	/**************************************************************************************************
	* Save
	***************************************************************************************************/
	Save: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var me = this;
		
		var myMask = new Ext.LoadMask(me, {msg:"Please wait..."});
		myMask.show();
		
		// Update Current Record 
		Ext.Ajax.request({
			scope: this,
			url: 'appAjax.php',
			method: 'post',
			timeout: 60000,
			params: 
			{
				appPage							: 'Home',
				appMethod						: 'UpdateShopLngRecord',
				ShopLngID						: values.ShopLngID,
				ShopLngType						: this.ShopLngType,					
				ShopLngShopID					: this.ShopID,
				ShopLngTitle						: values.ShopLngTitle,
				ShopLngSubtitle				: values.ShopLngSubtitle,						
				ShopLngText						: values.ShopLngText,			
									
			
			},
			success: function (response) 
			{								
				var text = response.responseText;
				
				// check that response is in JSON format
				try{
					var obj = Ext.JSON.decode(text);
				}
				catch(e){
					myMask.hide();
					me.close();
					Ext.Msg.alert('Error', 'Data not in json format');
					return;
				}

				// Parse response
				var obj = Ext.JSON.decode(text);

				if (obj.data) 
				{
					myMask.hide();
					//me.close();
					console.log(obj.data);
					//me.callbackRefresh(obj.data);
					return;
				} 
				
				// Error Handling
				var errormsg = "No data or error field found in json";
				
				if (obj.error) errormsg = obj.error;

				myMask.hide();
				//me.close();
				Ext.Msg.alert('Error', obj.error);
				
				return;
			},
			failure: function (response) 
			{				
				myMask.hide();	
				//me.close();
				Ext.Msg.alert('Error', 'Error in saving data !');
				return;				
			}   		
		});
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		//console.log(values.ShopLngID);
		
		var thisgrid	= this;
		
		//var RecID 	= thisgrid.getSelectedRecordID();
		var RecID 	= values.ShopLngID;
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditShopLng; 
			
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
});





