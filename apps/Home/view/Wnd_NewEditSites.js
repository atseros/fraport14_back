﻿// // Require Combo of Templates 
Ext.require([ 'Home.view.ComboGrid_SelectAirport']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditSites', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditSites',
		
	layout: 'border',
	title: 'Site Data',
    height: 190,
    width: 400,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[
				// Hidden field:  SiteID
				{
					xtype: 'hiddenfield',
					name: 'SiteID',
					fieldLabel: 'SiteID',
					readOnly: true,
					cls: 'readonly'
				},
				// Text field:  SiteDomainName
				{
					xtype: 'textfield',
					name: 'SiteDomainName',
					fieldLabel: 'Domain',
					allowBlank: false
				},
				// Text field:  SiteUrlName
				{
					xtype: 'textfield',
					name: 'SiteUrlTitle',
					fieldLabel: 'Url Title',
					allowBlank: false
				},
				// Text field:  SiteXmlFilename
				{
					xtype: 'textfield',
					name: 'SiteXmlFilename',
					fieldLabel: 'XML FIlename',
					allowBlank: false
				},
				// Combo field:  NodeTemplateFile
				{
					xtype: 'container',
					layout: { type: 'hbox' },
					items: 
					[
						{
							xtype: 'textfield',
							fieldLabel: 'Airport Code',
							name: 'SiteAirportCode',
							itemId: 'SiteAirportCode',                    
							flex: 19,
							labelWidth: 100,
							allowBlank: false,
							readOnly: true,
							maxLength: 255

						},
						{
							xtype: 'button',
							text: '...',	
							flex: 1,										
							handler: function() 
							{
								var x = new Home.view.ComboGrid_SelectAirport; 
								x.show(this);
								// if in Edit Mode, set already saved AvlSclID
								var thisform = this.up('form');
								x.SelectedRecordID = thisform.getValues().SiteAirportCode;
								
								x.callbackAfterSelect = function(name)
														{ 
															thisform.getForm().findField('SiteAirportCode').setValue(name);
														};
								x.loadData();
							}					
						}								
					]
				},
				
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod	: 'get_Site_NewRecordDefValues',
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod	: 'get_SiteRecord',
			SiteID		: this.editRecID
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'InsertSiteRecord',
			SiteID:						values.SiteID,
			SiteDomainName:		values.SiteDomainName,
			SiteUrlTitle:				values.SiteUrlTitle,
			SiteAirportCode:			values.SiteAirportCode,
			SiteXmlFilename:		values.SiteXmlFilename,
			
		};

		return params;
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'UpdateSiteRecord',
			SiteID:						values.SiteID,
			SiteDomainName:		values.SiteDomainName,
			SiteUrlTitle:				values.SiteUrlTitle,
			SiteAirportCode:			values.SiteAirportCode,
			SiteXmlFilename:			values.SiteXmlFilename,

		};
		
		return params;
	},
	
});