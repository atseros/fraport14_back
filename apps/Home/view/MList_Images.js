﻿// // Require Popup Form of Image (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditMedia']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Images' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Images',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				250 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 0'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				}
				,{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},		
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				},
				{
					 xtype:		'button'
					,text:		'Add Image To Node'
					,iconCls: 	'fa fa-arrow-right'
					,margin:		'0 0 0 10'
					,handler:	function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.AddImageToNode();
					}				
				},	
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'MediaID';
		
		this.SiteID = 0;
		
		this.NodeID = 0;
		this.NodeTitle = "";
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('MediaID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'MediaID', type: 'number', defaultValue: 0 }
				,{name: 'MediaSiteID', type: 'number', defaultValue: 0 }
				,{name: 'MediaTitle', type: 'string' }
				,{name: 'MediaFilename', type: 'string' }
				,{name: 'MediaType', type: 'number', defaultValue: 0 }
				,{name: 'MediaFullUrl', type: 'string'}
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Image', dataIndex: 'MediaFullUrl',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					}
				 },				
				{ header: 'Title', dataIndex: 'MediaTitle', flex: 75/100,},
				{ header: 'Filename', dataIndex: 'MediaFilename', flex: 25/100,},
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod		: 'getMediaList',
			maxRecords		: me.maxRecords,
			filterStrToFind	: me.filterStrToFind,
			SiteID				: me.SiteID,
			MediaType			: 1	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditMedia; 		
		x.editRecID = 0;
		x.SiteID = thisgrid.SiteID;
		x.MediaType = 1;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditMedia; 
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); Ext.ComponentManager.get('MList_Nodes_Media').loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteMediaRecord'
				,MediaID		: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
								Ext.ComponentManager.get('MList_Nodes_Media').loadData();					
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
	
	/**************************************************************************************************
	* AddImageToNode
	*
	* Button handler for AddImageToNode
	*
	***************************************************************************************************/
	AddImageToNode: function()
	{
		var thisgrid = this;
		
		var NodeID  	= thisgrid.NodeID;
		var MediaID 	= thisgrid.getSelectedRecordID();

		params = 
		{
			appPage		: 'Home',
			appMethod		: 'AddMediaToNode',
			NodeMediaNodeID	: NodeID,
			NodeMediaMediaID	: MediaID,
			
		};
		
		
		Ext.Msg.confirm('Confirmation', 'Add Image to '+thisgrid.NodeTitle+' ?', 
		function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Ajax.request({
						url: 'appAjax.php',
						method: 'post',
						params:  params,
						success: function(response)
						{
							var text = response.responseText;
							var obj = Ext.JSON.decode(text);
							if (!obj.data) 
							{										
								Ext.Msg.alert('Not added!', obj.error);
								return;
							}
							Ext.ComponentManager.get('MList_Nodes_Media').loadData();
							// Ext.ComponentManager.get('MList_CentralImageGalleryDetailed').loadData();	
						}	
					});
					return true;
				} 
				else 
				{
					return false;
				}
			}
		);	
		
	},
});