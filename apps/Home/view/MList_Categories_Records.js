﻿// // Require Popup Form of Categories Records (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditCategoriesRecords']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Categories_Records' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Categories_Records',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					xtype: 'button',
					text: 'Show All',
					itemId: '',
					iconCls: 'fa fa-filter',
					enableToggle: true,
					pressed: false,
					handler: function(button) 
					{ 
						var gridpanel = this.up('panel'); 
						
						if(!button.pressed)
							gridpanel.filterShowAll = 0;
						else 
							gridpanel.filterShowAll = 1;
						gridpanel.loadData();	
					}					
				},
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				250 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 5'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				},
				{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},		
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				}
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterShowAll	= 0;	
		this.filterType = -1;		
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'CtgRecID';
		
		this.CtgID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('CtgRecID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'CtgRecID', type: 'number', defaultValue: 0 }
				,{name: 'CtgRecCtgID', type: 'number', defaultValue: 0 }
				,{name: 'CtgRecPriority', type: 'number', defaultValue: 0 }
				,{name: 'CtgRecTitle', type: 'string' }
				,{name: 'CtgRecStartDate', type: 'date' }
				,{name: 'CtgRecStartTime', type: 'string' }
				,{name: 'CtgRecEndDate', type: 'date' }
				,{name: 'CtgRecEndTime', type: 'string' }
				,{name: 'CtgRecTimer', type: 'number', defaultValue: 0 }
				,{name: 'CtgRecFilename', type: 'string' }
				,{name: 'CtgRecFullUrl', type: 'string'}
				,{name: 'CtgRecStatus', type: 'number', defaultValue: 0 }
				,{name: 'CtgRecEscapeDefault', type: 'string'}
				
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Status', dataIndex: 'CtgRecStatus',  width: 40, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 2) 
						{
							return '<i class="fa fa-times" aria-hidden="true" style="font-size:16px;color:red"></i>';
						}
						else 
						{
							return '<i class="fa fa-check" aria-hidden="true" style="font-size:16px;color:green"></i>';
						}
							
					}
				},	
				{header: 'Image', dataIndex: 'CtgRecFullUrl',  width: 50, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					}
				 },				
				{ header: 'Priority', dataIndex: 'CtgRecPriority', width: 50,},
				{ header: 'Title', dataIndex: 'CtgRecTitle', flex: 20/100,},
				{header: 'Timer', dataIndex: 'CtgRecTimer',  width: 50, resizable: false, align:'center',
						renderer: function(value, p, record) 
						{
							if (value == 1) 
							{
								return '<i class="fa fa-check" aria-hidden="true" style="font-size:13px;color:green"></i>';
							}
							else 
							{
								return '<i class="fa fa-times" aria-hidden="true" style="font-size:13px;color:red"></i>';
							}
						
						}
				},
				{header: 'Start Date', dataIndex: 'CtgRecStartDate',  flex: 10/100, renderer: Ext.util.Format.dateRenderer('d/m/y')},
				{header: 'Start Time', dataIndex: 'CtgRecStartTime',  flex: 10/100,},
				{header: 'End Date', dataIndex: 'CtgRecEndDate',  flex: 10/100, renderer: Ext.util.Format.dateRenderer('d/m/y')},
				{header: 'End Time', dataIndex: 'CtgRecEndTime',  flex: 10/100,},
				{ header: 'Escape Default', dataIndex: 'CtgRecEscapeDefault', flex: 20/100,},
				
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getCategoriesRecordsList',
			filterStrToFind		: me.filterStrToFind,
			filterShowAll		: me.filterShowAll,
			CtgID				: me.CtgID,	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditCategoriesRecords; 		
		x.editRecID = 0;
		x.CtgID = thisgrid.CtgID;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditCategoriesRecords; 
			
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteCategoriesRecordsRecord'
				,CtgRecID		: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
});