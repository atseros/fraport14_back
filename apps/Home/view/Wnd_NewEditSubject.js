﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditSubject', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditSubject',
		
	layout: 'border',
	title: 'Subject Data',
    height: 170,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[
				// Hidden field:  SubID
				{
					xtype: 'hiddenfield',
					name: 'SubID',
					fieldLabel: 'SubID',
				},
				// // Hidden field:  SubSiteID
				// {
				// 	xtype: 'hiddenfield',
				// 	name: 'SubSiteID',
				// 	fieldLabel: 'SubSiteID',
				// },
				// Combo field: SubStatus
				{
					xtype: 'combo',    
					name: 'SubStatus',
					forceSelection: true,
					fieldLabel: 'Status',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},				
				// Text field:  SubPriority
				{
					xtype: 'numberfield',
					name: 'SubPriority',
					fieldLabel: 'Priority',
					border: 0,
					layout: 'border',
					value : 100,
				},
				// Text field:  SubTitle
				{
					xtype: 'textfield',
					name: 'SubTitle',
					fieldLabel: 'Title',
				},
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;
			    
		this.SiteID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod	: 'get_Subject_NewRecordDefValues',
            SiteID		:this.SiteID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod	: 'get_SubjectRecord',
			SubID		: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:			'Home',
			appMethod:			'InsertSubjectRecord',
			SubID:				values.SubID,
			SubSiteID:			this.SiteID,
			SubStatus:			values.SubStatus,
			SubPriority:		values.SubPriority,
			SubTitle:			values.SubTitle,
		
		};

		return params;
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:			'Home',
			appMethod:			'UpdateSubjectRecord',
			SubID:				values.SubID,
			SubStatus:			values.SubStatus,
			SubPriority:		values.SubPriority,
			SubTitle:			values.SubTitle,

		};
		
		return params;
		
	},
	
});