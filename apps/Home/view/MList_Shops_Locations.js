﻿// // Require Popup Form of Shop Location (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditShopLocation']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Shops_Locations' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Shops_Locations',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},		
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				}
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'LocationID';
		
		this.ShopID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('LocationID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'LocationID', type: 'number', defaultValue: 0 }
				,{name: 'LocationShopID', type: 'number', defaultValue: 0 }
				,{name: 'LocationPhone', type: 'string' }
				,{name: 'LocationEmail', type: 'string'}
				,{name: 'LocationImgFilename', type: 'string'}
				,{name: 'LocationPriority', type: 'number', defaultValue: 0 }
				,{name: 'LocationLevel', type: 'number', defaultValue: 0 }
				,{name: 'LocationLatitude', type: 'string'}
				,{name: 'LocationLongitude', type: 'string'}
				,{name: 'LocationFullUrl', type: 'string'}
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{ header: 'Priority', dataIndex: 'LocationPriority', flex: 7/100,},
				// {header: 'Image', dataIndex: 'LocationFullUrl',  width: 40, resizable:false,
					// renderer: function(value, p, record) 
					// {
							// return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					// }
				// },				
				// { header: 'Phone', dataIndex: 'LocationPhone', flex: 25/100,},
				// { header: 'Email', dataIndex: 'LocationEmail', flex: 25/100,},
				{ header: 'Latitude', dataIndex: 'LocationLatitude', flex: 25/100,},
				{ header: 'Longitude', dataIndex: 'LocationLongitude', flex: 25/100,},
				{header: 'Level/Floor', dataIndex: 'LocationLevel',  flex: 10/100, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 0) 
						{
							return 'Ground floor';
						}
						else if (value == 1) 
						{
							return '1st floor';
						}
						else if (value == 2) 
						{
							return '2nd floor';
						}
							
					}
				}
				
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getShopsLocationsList',
			maxRecords		: me.maxRecords,
			ShopID				: me.ShopID,	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditShopLocation; 		
		x.editRecID = 0;
		x.ShopID = thisgrid.ShopID;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditShopLocation; 
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteShopLocationRecord'
				,LocationID	: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
});