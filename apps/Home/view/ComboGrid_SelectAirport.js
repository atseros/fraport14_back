﻿Ext.require([ 'Home.view.ComboGrid_SelectAirport_AirportList']);

Ext.define('Home.view.ComboGrid_SelectAirport' ,{
    extend: 'Ext.window.Window',
    alias : 'widget.ComboGrid_SelectAirport',
	itemId: 'ComboGrid_SelectAirport',
	
	modal: true,
    height: 600,
    width:  1000,
	resizable:false,
	queryMode: 'local',  
	multiSelect: false,

    layout: {
        align: 'stretch',
        type: 'hbox'
    },
	
	title: 'Airport Selection',
	
	items:
	[
		{
			xtype: 'ComboGrid_SelectAirport_AirportList',
			itemid: 'ComboGrid_SelectAirport_AirportList',
			layout: 'border',
			region: 'center',
			split:true,
			width: 1000,
			listeners: 
			{
				itemdblclick: function(dv, record, item, index, e) 
				{
					// find selection - SclID and SclName
					var sr = this.getSelectionModel().getSelection();
					if(sr.length > 0)
					{
						var selectedRecord = sr[0];
						var selName = selectedRecord.get('SiteAirportCode');
						
						// callBack to update form fields
						var wnd = Ext.ComponentQuery.query('#ComboGrid_SelectAirport')[0];
						wnd.callbackAfterSelect(selName);
						// and close selection window
						wnd.close();
					}
				}
			}
		}
	],
	
	initComponent: function () 
	{
		this.SelectedRecordID	= 0;
		this.openerComponent	= 0;
		this.callbackAfterSelect = "";
	
		this.callParent(arguments);
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  loadData     function
	//////////////////////////////////////////////////////////////////////////////////////////////
	loadData: function()
	{
		// get direct child of window with xtype = ComboGrid_SelectTemplate_TemplateList
		var grid = Ext.ComponentQuery.query('#ComboGrid_SelectAirport > ComboGrid_SelectAirport_AirportList')[0];	
		
		// if there is Selection (pre saved AppID)
		if(this.SelectedRecordID > 0)
			grid.initSelectedRow = this.SelectedRecordID;
		
		grid.loadData();
		
	}
	
});





