﻿Ext.require([ 'Home.view.ComboGrid_SelectNode_NodeList']);

Ext.define('Home.view.ComboGrid_SelectNode' ,{
    extend: 'Ext.window.Window',
    alias : 'widget.ComboGrid_SelectNode',
	itemId: 'ComboGrid_SelectNode',
	
	modal: true,
    height: 500,
    width:  1200,
	resizable:false,
	queryMode: 'local',  
	multiSelect: false,

    layout: {
        align: 'stretch',
        type: 'hbox'
    },
	
	title: 'Node Selection',
	
	items:
	[
		{
			xtype: 'ComboGrid_SelectNode_NodeList',
			itemid: 'ComboGrid_SelectNode_NodeList',
			layout: 'border',
			region: 'center',
			split:true,
			width: 1200,
			listeners: 
			{
				itemdblclick: function(dv, record, item, index, e) 
				{
					// find selection - NodeID and NodeTitle
					var sr = this.getSelectionModel().getSelection();
					if(sr.length > 0)
					{
						var selectedRecord = sr[0];
						var selID = selectedRecord.get('NodeID');
						var selName = selectedRecord.get('NodeTitle');
						
						// callBack to update form fields
						var wnd = Ext.ComponentQuery.query('#ComboGrid_SelectNode')[0];
						wnd.callbackAfterSelect(selID, selName);
						// and close selection window
						wnd.close();
					}
				}
			}
		}
	],
	
	initComponent: function () 
	{
		this.SelectedRecordID	= 0;
		this.openerComponent	= 0;
		this.callbackAfterSelect = "";
		
		this.SiteID = 0;
	
		this.callParent(arguments);
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  loadData     function
	//////////////////////////////////////////////////////////////////////////////////////////////
	loadData: function()
	{
		// get direct child of window with xtype = ComboGrid_SelectNode_NodeList
		var grid = Ext.ComponentQuery.query('#ComboGrid_SelectNode > ComboGrid_SelectNode_NodeList')[0];	
		
		// if there is Selection (pre saved NodeID)
		if(this.SelectedRecordID > 0)
			grid.initSelectedRow = this.SelectedRecordID;
		grid.SiteID = this.SiteID;
		grid.loadData();
		
	}
	
});





