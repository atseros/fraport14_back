﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Home.view.Wnd_NewEditDisaster', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditDisaster',
		
	layout: 'border',
	title: 'Disaster Mode Data',
    height: 110,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 60,
				region :'north',
           },
			items: 
			[
				// Hidden field:  DisasterID
				{
					xtype: 'hiddenfield',
					name: 'DisasterID',
					fieldLabel: 'DisasterID',
				},
				// Hidden field:  DisasterSiteID
				{
					xtype: 'hiddenfield',
					name: 'DisasterSiteID',
					fieldLabel: 'DisasterSiteID',
				},
				// Text field:  DisasterTitle
				{
					xtype: 'textfield',
					name: 'DisasterTitle',
					fieldLabel: 'Title',
				},				
								
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.SiteID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_Disaster_NewRecordDefValues',
			SiteID			:this.SiteID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Home',
			appMethod		: 'get_DisasterRecord',
			DisasterID		: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'InsertDisasterRecord',
			DisasterID:				values.DisasterID,
			DisasterSiteID:			values.DisasterSiteID,
			DisasterTitle:				values.DisasterTitle,
			
		};

		return params;
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Home',
			appMethod:				'UpdateDisasterRecord',
			DisasterID:				values.DisasterID,
			DisasterSiteID:			values.DisasterSiteID,
			DisasterTitle:				values.DisasterTitle,

		};
		
		return params;
		
	},
	
});