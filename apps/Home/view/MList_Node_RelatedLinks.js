﻿// // Require Popup Form of Node Related Links (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditNodeRelatedLinkPriority']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Node_RelatedLinks' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Node_RelatedLinks',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[				
				{
					xtype: 'tbfill'
				}
				,{
					 xtype:		'button'
					,text:		'Edit Priority'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				},
			
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.maxRecords 		= 500;
		this.fieldNameID 	= 'NodeRelatedLinkID';
		
		this.SiteID = 0;
		
		this.NodeID = 0;
		this.NodeTitle = "";
		
		this.NodeLngType = -1;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('NodeRelatedLinkID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'RelatedLinkID', type: 'number', defaultValue: 0 }
				,{name: 'RelatedLinkSiteID', type: 'number', defaultValue: 0 }
				,{name: 'NodeRelatedLinkPriority', type: 'number', defaultValue: 0 }
				,{name: 'NodeRelatedLinkID', type: 'number', defaultValue: 0 }
				,{name: 'RelatedLinkTitle', type: 'string' }
				,{name: 'RelatedLinkFIlename', type: 'string' }
				,{name: 'RelatedLinkType', type: 'number', defaultValue: 0 }
				,{name: 'RelatedLinkFullUrl', type: 'string'}
				,{name: 'NodeTitle', type: 'string'}
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{ header: 'Priority', dataIndex: 'NodeRelatedLinkPriority', width: 50,},	
				{header: 'Image', dataIndex: 'RelatedLinkFullUrl',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
						if (value != "") 
						{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
						}	
						else
						{
							return '';
						}
					}
				 },				
				{ header: 'Title', dataIndex: 'RelatedLinkTitle', flex: 75/100,},
				{header: 'Type', dataIndex: 'RelatedLinkType',  width: 70, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 1) 
						{
							return 'With Image';
						}
						else 
						{
							return 'Without Image';
						}
							
					}
				},
				{ header: 'Link', dataIndex: 'NodeTitle', flex: 35/100,},
				
				
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getNodeRelatedLinkList',
			maxRecords		: me.maxRecords,
			NodeID				: me.NodeID,
			NodeLngType		: me.NodeLngType	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditNodeRelatedLinkPriority; 
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteNodeRelatedLinkRecord'
				,NodeRelatedLinkID	: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
	
});