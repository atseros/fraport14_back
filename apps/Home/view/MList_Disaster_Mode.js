﻿// // Require Popup Form of Disaster (Wnd for New Record)
Ext.require([ 'Home.view.Wnd_NewEditDisaster']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Home.view.MList_Disaster_Mode' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Disaster_Mode',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},		
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				},
				{
					xtype: 'tbfill'
				},
				{
					 xtype:		'button'
					,text:		'Enable Disaster Mode'
					,iconCls: 	'fa fa-thumbs-up'
					,margin:		'0 0 0 0'
					,handler:	function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.EnableDisasterMode();
					}				
				},	
				{
					 xtype:		'button'
					,text:		'Disable Disaster Mode'
					,iconCls: 	'fa fa-thumbs-down'
					,margin:		'0 0 0 10'
					,handler:	function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.DisableDisasterMode();
					}				
				},	
				
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.maxRecords 	= 500;
		this.fieldNameID = 'DisasterID';
		
		this.SiteID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('DisasterID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'DisasterID', type: 'number', defaultValue: 0 }
				,{name: 'DisasterStatus', type: 'number', defaultValue: 0 }
				,{name: 'DisasterSiteID', type: 'number', defaultValue: 0 }
				,{name: 'DisasterTitle', type: 'string' }
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Status', dataIndex: 'DisasterStatus',  width: 40, resizable: false,
					renderer: function(value, p, record) 
					{
						if (value == 2) 
						{
							return '<i class="fa fa-times" aria-hidden="true" style="font-size:16px;color:red"></i>';
						}
						else 
						{
							return '<i class="fa fa-check" aria-hidden="true" style="font-size:16px;color:green"></i>';
						}
							
					}
				},				
				{ header: 'Title', dataIndex: 'DisasterTitle', flex: 28/100,},
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Home',
			appMethod			: 'getDisasterList',
			maxRecords		: me.maxRecords,
			SiteID				: me.SiteID,	
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Home.view.Wnd_NewEditDisaster; 		
		x.editRecID = 0;
		x.SiteID = thisgrid.SiteID;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Home.view.Wnd_NewEditDisaster; 
			
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Home'
				,appMethod	: 'DeleteDisasterRecord'
				,DisasterID		: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
	
	/**************************************************************************************************
	* EnableDisasterMode
	*
	* Button handler for EnableDisasterMode
	*
	***************************************************************************************************/
	EnableDisasterMode: function()
	{
		var thisgrid = this;
		
		var DisasterID 	= thisgrid.getSelectedRecordID();

		params = 
		{
			appPage		: 'Home',
			appMethod		: 'UpdateDisasterRecord',
			DisasterID		: DisasterID,
			DisasterStatus	: 1,
			
		};
		
		
		Ext.Msg.confirm('Confirmation', 'Are you sure that you want to enable <b>DISASTER MODE</b> for the selected record ?', 
		function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Msg.confirm('Confirmation', 'Are you <b>REALLY</b> sure that you want to enable <b>DISASTER MODE</b> for the selected record ?', 
					function(btn) 
						{
							if (btn === 'yes') 
							{
								Ext.Ajax.request({
									url: 'appAjax.php',
									method: 'post',
									params:  params,
									success: function(response)
									{
										var text = response.responseText;
										var obj = Ext.JSON.decode(text);
										if (!obj.data) 
										{										
											Ext.Msg.alert('Not added!', obj.error);
											return;
										}
										
										thisgrid.loadData();
									}	
								});
								return true;
							} 
							else 
							{
								return false;
							}
						}
					);	
				} 
				else 
				{
					return false;
				}
			}
		);	
		
	},
	/**************************************************************************************************
	* DisableDisasterMode
	*
	* Button handler for DisableDisasterMode
	*
	***************************************************************************************************/
	DisableDisasterMode: function()
	{
		var thisgrid = this;
		
		var DisasterID 	= thisgrid.getSelectedRecordID();
		var DisasterListStatus = thisgrid.getSelectedRecordField('DisasterStatus');
		
		params = 
		{
			appPage		: 'Home',
			appMethod		: 'UpdateDisasterRecord',
			DisasterID		: DisasterID,
			DisasterStatus	: 2,
			
		};
		
		if (DisasterListStatus == 1)
		{
			Ext.Msg.confirm('Confirmation', 'Are you sure that you want to disable <b>DISASTER MODE</b> for the selected record ?', 
			function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not added!', obj.error);
									return;
								}
								
								thisgrid.loadData();
							}	
						});
						return true;
					} 
					else 
					{
						return false;
					}
				}
			);	
		}
		
		
	},
	
	
});