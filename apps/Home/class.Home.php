<?php

require_once(realpath(__DIR__."/../../db")."/class.DB_sys_site.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_node.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_nodelng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_shop.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_shoplng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_shop_location.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_shop_locationlng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_media.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_medialng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_node_media.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_alert.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_alertlng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_slider.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_slider_images.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_slider_imageslng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_node_slider.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_disaster.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_disasterlng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_site_airlinelng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_related_links.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_related_linkslng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_node_related_link.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_categories.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_categories_records.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_categories_recordslng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_categories_records_slider.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_footer.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_footerlng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_footer_data.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_footer_datalng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_image_gallery.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_image_gallerylng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_image_gallery_data.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_image_gallery_datalng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_video_gallery.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_video_gallerylng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_video_gallery_data.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_video_gallery_datalng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_publications_gallery.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_publications_gallerylng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_publications_gallery_data.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_publications_gallery_datalng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_cf_subjects.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_cf_subjects_lang.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_cf_sub_subject.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_cf_sub_subject_lang.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_cf_answers_lang.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_footer_timetable.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_footer_timetablelng.php");

/*********************************************************************************************
* CLASS Home
*
* DESCRIPTION: 
*	Class that handles requests from Home page 
*
*********************************************************************************************/
class Home
{
	private $appFrw;
	
	/****************************************************************************************
	* Home::CONSTRUCTOR
	****************************************************************************************/
	function Home($appFrw)
	{
		$this->appFrw = $appFrw;
	}
	
	/*
		SITES
	*/
	
	
	/****************************************************************************************
	* Home::get_Site_NewRecordDefValues
	****************************************************************************************/
	function get_Site_NewRecordDefValues()
	{
		$params = array();
		
		$results = DB_sys_Site::sys_Site_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertSiteRecord
	****************************************************************************************/
	function InsertSiteRecord()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		$params["SiteDomainName"] = $_REQUEST["SiteDomainName"];
		$params["SiteUrlTitle"] = $_REQUEST["SiteUrlTitle"];
		$params["SiteAirportCode"] = $_REQUEST["SiteAirportCode"];
		$params["SiteXmlFilename"] = $_REQUEST["SiteXmlFilename"];
		
		$results = DB_sys_Site::sys_Site_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_SiteRecord
	****************************************************************************************/
	function get_SiteRecord()
	{
		$params["SiteID"] = $_REQUEST["SiteID"];
						
		$results = DB_sys_site::sys_site_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateSiteRecord
	****************************************************************************************/
	function UpdateSiteRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['SiteID'])) { $params['SiteID'] = $_REQUEST['SiteID']; }
		if(isset($_REQUEST['SiteDomainName'])) { $params['SiteDomainName'] = $_REQUEST['SiteDomainName']; }
		if(isset($_REQUEST['SiteUrlTitle'])) { $params['SiteUrlTitle'] = $_REQUEST['SiteUrlTitle']; }
		if(isset($_REQUEST['SiteAirportCode'])) { $params['SiteAirportCode'] = $_REQUEST['SiteAirportCode']; }
		if(isset($_REQUEST['SiteXmlFilename'])) { $params['SiteXmlFilename'] = $_REQUEST['SiteXmlFilename']; }
		
		$results = DB_sys_site::sys_Site_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteSiteRecord
	****************************************************************************************/
	function DeleteSiteRecord()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_site::sys_Site_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_SiteList
	****************************************************************************************/
	function get_SiteList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		
		$results = DB_sys_Site::sys_Site_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_SiteAirportsList
	****************************************************************************************/
	function get_SiteAirportsList()
	{
		$params = array();
		
		$results = DB_sys_site::sys_site_getAirportList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		NODES
	*/
	
	/****************************************************************************************
	* Home::get_Node_NewRecordDefValues
	****************************************************************************************/
	function get_Node_NewRecordDefValues()
	{
		$params = array();
		
		$params["NodeNodeID"] = $_REQUEST["NodeNodeID"];
		
		$results = DB_sys_node::sys_node_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertNodeRecord
	****************************************************************************************/
	function InsertNodeRecord()
	{
		$params = array();
		
		$params["NodeID"] = $_REQUEST["NodeID"];
		$params["NodeCreatedAt"] = $_REQUEST["NodeCreatedAt"];
		$params["NodeParentCode"] = $_REQUEST["NodeParentCode"];
		$params["NodeCode"] = $_REQUEST["NodeCode"];
		$params["NodeTitle"] = $_REQUEST["NodeTitle"];
		$params["NodeUrlAlias"] = $_REQUEST["NodeUrlAlias"];
		$params["NodeTemplateFile"] = $_REQUEST["NodeTemplateFile"];
		$params["NodeSiteID"] = $_REQUEST["NodeSiteID"];
		$params["NodeLevel"] = $_REQUEST["NodeLevel"];
		$params["NodeNodeID"] = $_REQUEST["NodeNodeID"];
		$params["NodeMenu"] = 1;
		$params["NodeIsFlight"] = $_REQUEST["NodeIsFlight"];
		$params["NodeIsFlightDetailed"] = $_REQUEST["NodeIsFlightDetailed"];
		$params["NodeIsAirline"] = $_REQUEST["NodeIsAirline"];
		$params["NodeIsAirlineDetailed"] = $_REQUEST["NodeIsAirlineDetailed"];
		$params["NodeIsShop"] = $_REQUEST["NodeIsShop"];
		$params["NodeIsShopDetailed"] = $_REQUEST["NodeIsShopDetailed"];
		$params["NodeIsDarkSite"] = $_REQUEST["NodeIsDarkSite"];
		$params["NodeDataType"] = $_REQUEST["NodeDataType"];
		$params["NodeViewType"] = $_REQUEST["NodeViewType"];
		$params["NodeIsCategory"] = $_REQUEST["NodeIsCategory"];
		$params["NodeIsCategoryDetailed"] = $_REQUEST["NodeIsCategoryDetailed"];
		$params["NodeIsRestaurant"] = $_REQUEST["NodeIsRestaurant"];
		$params["NodeIsDutyFree"] = $_REQUEST["NodeIsDutyFree"];
		$params["NodeIsImageGallery"] = $_REQUEST["NodeIsImageGallery"];
		$params["NodeIsImageGalleryDetailed"] = $_REQUEST["NodeIsImageGalleryDetailed"];
		$params["NodeIsVideoGallery"] = $_REQUEST["NodeIsVideoGallery"];
		$params["NodeIsVideoGalleryDetailed"] = $_REQUEST["NodeIsVideoGalleryDetailed"];
		$params["NodeIsPublications"] = $_REQUEST["NodeIsPublications"];
		$params["NodeIsPublicationsDetailed"] = $_REQUEST["NodeIsPublicationsDetailed"];
		$params["NodeIsSearchPage"] = $_REQUEST["NodeIsSearchPage"];
		$params["NodeIsDomesticDestinations"] = $_REQUEST["NodeIsDomesticDestinations"];
		$params["NodeIsInternationalDestinations"] = $_REQUEST["NodeIsInternationalDestinations"];
		$params["NodeIsWeatherPage"] = $_REQUEST["NodeIsWeatherPage"];
		$params["NodeIsDestinations"] = $_REQUEST["NodeIsDestinations"];
		
		$results = DB_sys_node::sys_node_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::get_SiteRecord
	****************************************************************************************/
	function get_NodeRecord()
	{
		$params["NodeID"] = $_REQUEST["NodeID"];
		$params["NodeNodeID"] = $_REQUEST["NodeNodeID"];
			
		$results = DB_sys_node::sys_node_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateNodeRecord
	****************************************************************************************/
	function UpdateNodeRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['NodeID'])) { $params['NodeID'] = $_REQUEST['NodeID']; }
		if(isset($_REQUEST['NodeCode'])) { $params['NodeCode'] = $_REQUEST['NodeCode']; }
		if(isset($_REQUEST['NodeTitle'])) { $params['NodeTitle'] = $_REQUEST['NodeTitle']; }
		if(isset($_REQUEST['NodeUrlAlias'])) { $params['NodeUrlAlias'] = $_REQUEST['NodeUrlAlias']; }
		if(isset($_REQUEST['NodeTemplateFile'])) { $params['NodeTemplateFile'] = $_REQUEST['NodeTemplateFile']; }
		if(isset($_REQUEST['NodeParentCode'])) { $params['NodeParentCode'] = $_REQUEST['NodeParentCode']; }
		if(isset($_REQUEST['NodeIsHomePage'])) { $params['NodeIsHomePage'] = $_REQUEST['NodeIsHomePage']; }
		if(isset($_REQUEST['NodePublished'])) { $params['NodePublished'] = $_REQUEST['NodePublished']; }
		if(isset($_REQUEST['NodeSiteID'])) { $params['NodeSiteID'] = $_REQUEST['NodeSiteID']; }
		if(isset($_REQUEST['NodeMenu'])) { $params['NodeMenu'] = $_REQUEST['NodeMenu']; }
		if(isset($_REQUEST['NodeIsFlight'])) { $params['NodeIsFlight'] = $_REQUEST['NodeIsFlight']; }
		if(isset($_REQUEST['NodeIsFlightDetailed'])) { $params['NodeIsFlightDetailed'] = $_REQUEST['NodeIsFlightDetailed']; }
		if(isset($_REQUEST['NodeIsAirline'])) { $params['NodeIsAirline'] = $_REQUEST['NodeIsAirline']; }
		if(isset($_REQUEST['NodeIsAirlineDetailed'])) { $params['NodeIsAirlineDetailed'] = $_REQUEST['NodeIsAirlineDetailed']; }
		if(isset($_REQUEST['NodeIsShop'])) { $params['NodeIsShop'] = $_REQUEST['NodeIsShop']; }
		if(isset($_REQUEST['NodeIsShopDetailed'])) { $params['NodeIsShopDetailed'] = $_REQUEST['NodeIsShopDetailed']; }
		if(isset($_REQUEST['NodeIsRestaurant'])) { $params['NodeIsRestaurant'] = $_REQUEST['NodeIsRestaurant']; }
		if(isset($_REQUEST['NodeIsDarkSite'])) { $params['NodeIsDarkSite'] = $_REQUEST['NodeIsDarkSite']; }
		if(isset($_REQUEST['NodeDataType'])) { $params['NodeDataType'] = $_REQUEST['NodeDataType']; }
		if(isset($_REQUEST['NodeViewType'])) { $params['NodeViewType'] = $_REQUEST['NodeViewType']; }
		if(isset($_REQUEST['NodeIsCategory'])) { $params['NodeIsCategory'] = $_REQUEST['NodeIsCategory']; }
		if(isset($_REQUEST['NodeIsCategoryDetailed'])) { $params['NodeIsCategoryDetailed'] = $_REQUEST['NodeIsCategoryDetailed']; }
		if(isset($_REQUEST['NodeIsRestaurant'])) { $params['NodeIsRestaurant'] = $_REQUEST['NodeIsRestaurant']; }
		if(isset($_REQUEST['NodeIsDutyFree'])) { $params['NodeIsDutyFree'] = $_REQUEST['NodeIsDutyFree']; }
		if(isset($_REQUEST['NodeIsImageGallery'])) { $params['NodeIsImageGallery'] = $_REQUEST['NodeIsImageGallery']; }
		if(isset($_REQUEST['NodeIsImageGalleryDetailed'])) { $params['NodeIsImageGalleryDetailed'] = $_REQUEST['NodeIsImageGalleryDetailed']; }
		if(isset($_REQUEST['NodeIsVideoGallery'])) { $params['NodeIsVideoGallery'] = $_REQUEST['NodeIsVideoGallery']; }
		if(isset($_REQUEST['NodeIsVideoGalleryDetailed'])) { $params['NodeIsVideoGalleryDetailed'] = $_REQUEST['NodeIsVideoGalleryDetailed']; }
		if(isset($_REQUEST['NodeIsPublications'])) { $params['NodeIsPublications'] = $_REQUEST['NodeIsPublications']; }
		if(isset($_REQUEST['NodeIsPublicationsDetailed'])) { $params['NodeIsPublicationsDetailed'] = $_REQUEST['NodeIsPublicationsDetailed']; }
		if(isset($_REQUEST['NodeIsSearchPage'])) { $params['NodeIsSearchPage'] = $_REQUEST['NodeIsSearchPage']; }
		if(isset($_REQUEST['NodeIsDomesticDestinations'])) { $params['NodeIsDomesticDestinations'] = $_REQUEST['NodeIsDomesticDestinations']; }
		if(isset($_REQUEST['NodeIsInternationalDestinations'])) { $params['NodeIsInternationalDestinations'] = $_REQUEST['NodeIsInternationalDestinations']; }
		if(isset($_REQUEST['NodeIsWeatherPage'])) { $params['NodeIsWeatherPage'] = $_REQUEST['NodeIsWeatherPage']; }
		if(isset($_REQUEST['NodeIsDestinations'])) { $params['NodeIsDestinations'] = $_REQUEST['NodeIsDestinations']; }
		
		$results = DB_sys_node::sys_node_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteNodeRecord
	****************************************************************************************/
	function DeleteNodeRecord()
	{
		$params = array();
		
		$params["NodeID"] = $_REQUEST["NodeID"];
		
		$results = DB_sys_node::sys_node_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::MoveNodeRecord
	****************************************************************************************/
	function MoveNodeRecord()
	{
		$params = array();
		
		$params["NodeID"]	= $_REQUEST["NodeID"];
		$params["Direction"]	= $_REQUEST["Direction"];
		
		$results = DB_sys_node::sys_node_MoveRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getNodesList
	****************************************************************************************/
	function getNodesList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['RelatedLinkID'] 	= isset($_REQUEST['RelatedLinkID']) ? $_REQUEST['RelatedLinkID'] : 0;
		$params['SliderID'] 	= isset($_REQUEST['SliderID']) ? $_REQUEST['SliderID'] : 0;
		$params['MediaID'] 	= isset($_REQUEST['MediaID']) ? $_REQUEST['MediaID'] : 0;
		
		$results = DB_sys_node::sys_node_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getNodesListForCategories
	****************************************************************************************/
	function getNodesListForCategories()
	{
		$params = array();
		
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		
		$results = DB_sys_node::sys_node_getListForCategories($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/****************************************************************************************
	* Home::getNodeViewTypeList
	****************************************************************************************/
	function getNodeViewTypeList()
	{
		$params = array();
		
		$params['NodeDataTypeSelector'] 	= isset($_REQUEST['NodeDataTypeSelector']) ? $_REQUEST['NodeDataTypeSelector'] : 0;
		
		$results = DB_sys_node::sys_node_getNodeViewTypeList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	
	/*
		NODE LNG
	*/
	
	/****************************************************************************************
	* Home::get_NodeLngRecord
	****************************************************************************************/
	function get_NodeLngRecord()
	{
		//Check if Record Exists
		$params["NodeLngNodeID"] = $_REQUEST["NodeLngNodeID"];
		$params["NodeLngType"] = $_REQUEST["NodeLngType"];
		
		
		$CheckIfNodeLngExists = DB_sys_nodelng::sys_nodelng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfNodeLngExists == 0)
		{
			// // Insert NodeLng
			$NodeLngDefValues = DB_sys_nodelng::sys_nodelng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["NodeLngID"] = $NodeLngDefValues['data']['NodeLngID'];
			
			$result = DB_sys_nodelng::sys_nodelng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_nodelng::sys_nodelng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$NodeLngID = DB_sys_nodelng::sys_nodelng_GetNodeLngID($this->appFrw, $params);
			
			$params["NodeLngID"] = $NodeLngID; 
			
			$results = DB_sys_nodelng::sys_nodelng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
		
		
	}
	
	/****************************************************************************************
	* Home::UpdateNodeRecord
	****************************************************************************************/
	function UpdateNodeLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['NodeLngID'])) { $params['NodeLngID'] = $_REQUEST['NodeLngID']; }
		if(isset($_REQUEST['NodeLngType'])) { $params['NodeLngType'] = $_REQUEST['NodeLngType']; }
		if(isset($_REQUEST['NodeLngNodeID'])) { $params['NodeLngNodeID'] = $_REQUEST['NodeLngNodeID']; }
		if(isset($_REQUEST['NodeLngTitle'])) { $params['NodeLngTitle'] = $_REQUEST['NodeLngTitle']; }
		if(isset($_REQUEST['NodeLngIntro'])) { $params['NodeLngIntro'] = $_REQUEST['NodeLngIntro']; }
		if(isset($_REQUEST['NodeLngStory'])) { $params['NodeLngStory'] = $_REQUEST['NodeLngStory']; }
		if(isset($_REQUEST['NodeLngMetaTitle'])) { $params['NodeLngMetaTitle'] = $_REQUEST['NodeLngMetaTitle']; }
		if(isset($_REQUEST['NodeLngMetaKeywords'])) { $params['NodeLngMetaKeywords'] = $_REQUEST['NodeLngMetaKeywords']; }
		if(isset($_REQUEST['NodeLngMetaDescription'])) { $params['NodeLngMetaDescription'] = $_REQUEST['NodeLngMetaDescription']; }
		
		
		$results = DB_sys_nodelng::sys_nodelng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/*
		TEMPLATES
	*/
	
	/****************************************************************************************
	* Home::getNodesList
	****************************************************************************************/
	function get_TemplateList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		
		$results = DB_sys_node::sys_node_getTemplateList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		SHOPS
	*/
	
	/****************************************************************************************
	* Home::get_Shop_NewRecordDefValues
	****************************************************************************************/
	function get_Shop_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_shop::sys_shop_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertShopRecord
	****************************************************************************************/
	function InsertShopRecord()
	{
		$params = array();
		
		$params["ShopID"] = $_REQUEST["ShopID"];
		$params["ShopSiteID"] = $_REQUEST["ShopSiteID"];
		$params["ShopStatus"] = $_REQUEST["ShopStatus"];
		$params["ShopPriority"] = $_REQUEST["ShopPriority"];
		$params["ShopTitle"] = $_REQUEST["ShopTitle"];
		// $params["ShopPayment"] = $_REQUEST["ShopPayment"];
		// $params["ShopDisabilityAccess"] = $_REQUEST["ShopDisabilityAccess"];
		$params["ShopType"] = $_REQUEST["ShopType"];
		$params["ShopPhone"] = $_REQUEST["ShopPhone"];
		$params["ShopEscapeDefault"] = $_REQUEST["ShopEscapeDefault"];

		
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["ShopImgFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_shop', $params["ShopID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$mdnewsize = 'mdnew-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$mdnewsize");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		if(!empty($_FILES["Logo"]["tmp_name"]))
		{
			$params["ShopImgFilenameLogo"] = $_FILES["Logo"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_shop', $params["ShopID"] )."/";
			
			$tmp_uploaded_file = $_FILES["Logo"]["tmp_name"];
			$uploaded_filename = $_FILES["Logo"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'logo-thumb-' . $_FILES["Logo"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'logo-md-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'logo-md3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'logo-sd-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'logo-sd3-1-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'logo-sd3-2-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'logo-xs-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'logo-xs3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'logo-rectangle-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		$result = DB_sys_shop::sys_shop_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
		
			
		
	}
	
	/****************************************************************************************
	* Home::get_ShopRecord
	****************************************************************************************/
	function get_ShopRecord()
	{
		$params["ShopID"] = $_REQUEST["ShopID"];
			
		$results = DB_sys_shop::sys_shop_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateShopRecord
	****************************************************************************************/
	function UpdateShopRecord()
	{
		$params = array();

		if(isset($_REQUEST['ShopID'])) { $params['ShopID'] = $_REQUEST['ShopID']; }
		if(isset($_REQUEST['ShopSiteID'])) { $params['ShopSiteID'] = $_REQUEST['ShopSiteID']; }
		if(isset($_REQUEST['ShopStatus'])) { $params['ShopStatus'] = $_REQUEST['ShopStatus']; }
		if(isset($_REQUEST['ShopPriority'])) { $params['ShopPriority'] = $_REQUEST['ShopPriority']; }
		if(isset($_REQUEST['ShopTitle'])) { $params['ShopTitle'] = $_REQUEST['ShopTitle']; }
		// if(isset($_REQUEST['ShopPayment'])) { $params['ShopPayment'] = $_REQUEST['ShopPayment']; }
		// if(isset($_REQUEST['ShopDisabilityAccess'])) { $params['ShopDisabilityAccess'] = $_REQUEST['ShopDisabilityAccess']; }
		if(isset($_REQUEST['ShopType'])) { $params['ShopType'] = $_REQUEST['ShopType']; }
		if(isset($_REQUEST['ShopPhone'])) { $params['ShopPhone'] = $_REQUEST['ShopPhone']; }
		if(isset($_REQUEST['ShopEscapeDefault'])) { $params['ShopEscapeDefault'] = $_REQUEST['ShopEscapeDefault']; }
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["ShopImgFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_shop', $params["ShopID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$mdnewsize = 'mdnew-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$mdnewsize");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}

		if(!empty($_FILES["Logo"]["tmp_name"]))
		{
			$params["ShopImgFilenameLogo"] = $_FILES["Logo"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_shop', $params["ShopID"] )."/";
			
			$tmp_uploaded_file = $_FILES["Logo"]["tmp_name"];
			$uploaded_filename = $_FILES["Logo"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'logo-thumb-' . $_FILES["Logo"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'logo-md-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'logo-md3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'logo-sd-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'logo-sd3-1-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'logo-sd3-2-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'logo-xs-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'logo-xs3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'logo-rectangle-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}			
		
		$result = DB_sys_shop::sys_shop_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteNodeRecord
	****************************************************************************************/
	function DeleteShopRecord()
	{
		$params = array();
		
		$params["ShopID"] = $_REQUEST["ShopID"];
		
		$results = DB_sys_shop::sys_shop_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getShopsList
	****************************************************************************************/
	function getShopsList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		$params['filterType'] 	= isset($_REQUEST['filterType']) ? $_REQUEST['filterType'] : -1;
		
		
		$results = DB_sys_shop::sys_shop_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/*
		SHOP LNG
	*/
	
	/****************************************************************************************
	* Home::get_ShopLngRecord
	****************************************************************************************/
	function get_ShopLngRecord()
	{
		//Check if Record Exists
		$params["ShopLngShopID"] = $_REQUEST["ShopLngShopID"];
		$params["ShopLngType"] = $_REQUEST["ShopLngType"];
		
		
		$CheckIfShopLngExists = DB_sys_shoplng::sys_shoplng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfShopLngExists == 0)
		{
			// // Insert NodeLng
			$ShopLngDefValues = DB_sys_shoplng::sys_shoplng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["ShopLngID"] = $ShopLngDefValues['data']['ShopLngID'];
			
			$result = DB_sys_shoplng::sys_shoplng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_shoplng::sys_shoplng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$ShopLngID = DB_sys_shoplng::sys_shoplng_GetShopLngID($this->appFrw, $params);
			
			$params["ShopLngID"] = $ShopLngID; 
			
			$results = DB_sys_shoplng::sys_shoplng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
		
		
	}
	
	/****************************************************************************************
	* Home::UpdateShopLngRecord
	****************************************************************************************/
	function UpdateShopLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['ShopLngID'])) { $params['ShopLngID'] = $_REQUEST['ShopLngID']; }
		if(isset($_REQUEST['ShopLngType'])) { $params['ShopLngType'] = $_REQUEST['ShopLngType']; }
		if(isset($_REQUEST['ShopLngShopID'])) { $params['ShopLngShopID'] = $_REQUEST['ShopLngShopID']; }
		if(isset($_REQUEST['ShopLngTitle'])) { $params['ShopLngTitle'] = $_REQUEST['ShopLngTitle']; }
		if(isset($_REQUEST['ShopLngSubtitle'])) { $params['ShopLngSubtitle'] = $_REQUEST['ShopLngSubtitle']; }
		if(isset($_REQUEST['ShopLngText'])) { $params['ShopLngText'] = $_REQUEST['ShopLngText']; }
	
		$results = DB_sys_shoplng::sys_shoplng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_ShopLngLogoRecord
	****************************************************************************************/
	function get_ShopLngLogoRecord()
	{
		//Check if Record Exists
		$params["ShopLngID"] = $_REQUEST["ShopLngID"];
		
		$results = DB_sys_shoplng::sys_shoplng_getLogoRecord($this->appFrw, $params);
		
		if ($results['success']==true)
		return json_encode((array('data' => $results["data"])));
		else 
		return json_encode((array('error' => $results["reason"])));		
	}
	
	/****************************************************************************************
	* Home::UpdateShopLngLogoRecord
	****************************************************************************************/
	function UpdateShopLngLogoRecord()
	{
		$params = array();

		if(isset($_REQUEST['ShopLngID'])) { $params['ShopLngID'] = $_REQUEST['ShopLngID']; }
		if(isset($_REQUEST['ShopLngType'])) { $params['ShopLngType'] = $_REQUEST['ShopLngType']; }
		
		if(!empty($_FILES["Logo"]["tmp_name"]))
		{
			$params["ShopLngImgFilenameLogo"] = $_FILES["Logo"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_shoplng', $params["ShopLngID"] )."/";
			
			$tmp_uploaded_file = $_FILES["Logo"]["tmp_name"];
			$uploaded_filename = $_FILES["Logo"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'logo-thumb-' . $_FILES["Logo"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'logo-md-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'logo-md3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'logo-sd-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'logo-sd3-1-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'logo-sd3-2-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'logo-xs-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'logo-xs3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'logo-rectangle-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}			
		
		$result = DB_sys_shoplng::sys_shoplng_UpdateLogoRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/*
		SHOP LOCATIONS
	*/
	
	/****************************************************************************************
	* Home::get_shop_location_NewRecordDefValues
	****************************************************************************************/
	function get_shop_location_NewRecordDefValues()
	{
		$params = array();
		
		$params["ShopID"] = $_REQUEST["ShopID"];
		
		$results = DB_shop_location::shop_location_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertShopLocationRecord
	****************************************************************************************/
	function InsertShopLocationRecord()
	{
		$params = array();
		
		$params["LocationID"] = $_REQUEST["LocationID"];
		$params["LocationShopID"] = $_REQUEST["LocationShopID"];
		$params["LocationPriority"] = $_REQUEST["LocationPriority"];
		// $params["LocationPhone"] = $_REQUEST["LocationPhone"];
		// $params["LocationEmail"] = $_REQUEST["LocationEmail"];
		$params["LocationLatitude"] = $_REQUEST["LocationLatitude"];
		$params["LocationLongitude"] = $_REQUEST["LocationLongitude"];
		$params["LocationLevel"] = $_REQUEST["LocationLevel"];

		
		
		// if(!empty($_FILES["File"]["tmp_name"]))
		// {
			// $params["LocationImgFilename"] = $_FILES["File"]["name"];
			
			// $folder = FileManager::getTblFolderPath('shop_location', $params["LocationID"] )."/";
			
			// $tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			// $uploaded_filename = $_FILES["File"]["name"];
			
			// move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			// $thumb = 'thumb-' . $_FILES["File"]["name"];
			
			// FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			// $md = 'md-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			// $md3 = 'md3-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			// $sd = 'sd-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			// $sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			// $sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			// $xs = 'xs-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			// $xs3 = 'xs3-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			// $rectangle = 'rectangle-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
			
		// }	
		
		$result = DB_shop_location::shop_location_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_ShopLocationRecord
	****************************************************************************************/
	function get_ShopLocationRecord()
	{
		$params["LocationID"] = $_REQUEST["LocationID"];
			
		$results = DB_shop_location::shop_location_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateShopLocationRecord
	****************************************************************************************/
	function UpdateShopLocationRecord()
	{
		$params = array();

		if(isset($_REQUEST['LocationID'])) { $params['LocationID'] = $_REQUEST['LocationID']; }
		if(isset($_REQUEST['LocationShopID'])) { $params['LocationShopID'] = $_REQUEST['LocationShopID']; }
		if(isset($_REQUEST['LocationPriority'])) { $params['LocationPriority'] = $_REQUEST['LocationPriority']; }
		// if(isset($_REQUEST['LocationPhone'])) { $params['LocationPhone'] = $_REQUEST['LocationPhone']; }
		// if(isset($_REQUEST['LocationEmail'])) { $params['LocationEmail'] = $_REQUEST['LocationEmail']; }
		if(isset($_REQUEST['LocationLatitude'])) { $params['LocationLatitude'] = $_REQUEST['LocationLatitude']; }
		if(isset($_REQUEST['LocationLongitude'])) { $params['LocationLongitude'] = $_REQUEST['LocationLongitude']; }
		if(isset($_REQUEST['LocationLevel'])) { $params['LocationLevel'] = $_REQUEST['LocationLevel']; }
		
		// if(!empty($_FILES["File"]["tmp_name"]))
		// {
			// $params["LocationImgFilename"] = $_FILES["File"]["name"];
			
			// $folder = FileManager::getTblFolderPath('shop_location', $params["LocationID"] )."/";
			
			// $tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			// $uploaded_filename = $_FILES["File"]["name"];
			
			// move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			// $thumb = 'thumb-' . $_FILES["File"]["name"];
			
			// FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			// $md = 'md-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			// $md3 = 'md3-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			// $sd = 'sd-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			// $sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			// $sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			// $xs = 'xs-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			// $xs3 = 'xs3-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			// $rectangle = 'rectangle-' . $_FILES["File"]["name"];
			// FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
				
			
		// }	
		
		$result = DB_shop_location::shop_location_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteShopLocationRecord
	****************************************************************************************/
	function DeleteShopLocationRecord()
	{
		$params = array();
		
		$params["LocationID"] = $_REQUEST["LocationID"];
		
		$results = DB_shop_location::shop_location_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getShopsLocationsList
	****************************************************************************************/
	function getShopsLocationsList()
	{
		$params = array();

		$params['ShopID'] 	= isset($_REQUEST['ShopID']) ? $_REQUEST['ShopID'] : 0;

		$results = DB_shop_location::shop_location_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		SHOP LOCATIONS LNG
	*/
	
	/****************************************************************************************
	* Home::get_LocationLngRecord
	****************************************************************************************/
	function get_LocationLngRecord()
	{
		//Check if Record Exists
		$params["LocationLngLocationID"] = $_REQUEST["LocationLngLocationID"];
		$params["LocationLngType"] = $_REQUEST["LocationLngType"];
		
		
		$CheckIfLocationLngExists = DB_shop_locationlng::shop_locationlng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfLocationLngExists == 0)
		{
			// // Insert NodeLng
			$LocationLngDefValues = DB_shop_locationlng::shop_locationlng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["LocationLngID"] = $LocationLngDefValues['data']['LocationLngID'];
			
			$result = DB_shop_locationlng::shop_locationlng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_shop_locationlng::shop_locationlng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$LocationLngID = DB_shop_locationlng::shop_locationlng_GetLocationLngID($this->appFrw, $params);
			
			$params["LocationLngID"] = $LocationLngID; 
			
			$results = DB_shop_locationlng::shop_locationlng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
		
		
	}
	
	/****************************************************************************************
	* Home::UpdateLocationLngRecord
	****************************************************************************************/
	function UpdateLocationLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['LocationLngID'])) { $params['LocationLngID'] = $_REQUEST['LocationLngID']; }
		if(isset($_REQUEST['LocationLngType'])) { $params['LocationLngType'] = $_REQUEST['LocationLngType']; }
		if(isset($_REQUEST['LocationLngLocationID'])) { $params['LocationLngLocationID'] = $_REQUEST['LocationLngLocationID']; }
		if(isset($_REQUEST['LocationLngLocation'])) { $params['LocationLngLocation'] = $_REQUEST['LocationLngLocation']; }
		// if(isset($_REQUEST['LocationLngOpeningTimes'])) { $params['LocationLngOpeningTimes'] = $_REQUEST['LocationLngOpeningTimes']; }
		
		$results = DB_shop_locationlng::shop_locationlng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		MEDIA
	*/
	
	/****************************************************************************************
	* Home::get_Media_NewRecordDefValues
	****************************************************************************************/
	function get_Media_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		$params["MediaType"] = $_REQUEST["MediaType"];
		
		$results = DB_sys_media::sys_media_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertMediaRecord
	****************************************************************************************/
	function InsertMediaRecord()
	{
		$params = array();
		
		$params["MediaID"] = $_REQUEST["MediaID"];
		$params["MediaSiteID"] = $_REQUEST["MediaSiteID"];
		$params["MediaType"] = $_REQUEST["MediaType"];
		$params["MediaTitle"] = $_REQUEST["MediaTitle"];

		
		if(!empty($_FILES["File"]["tmp_name"]) && $params["MediaType"] == 1)
		{
			$params["MediaFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_media', $params["MediaID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$menu = 'menu-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(560, 315, "$folder/$uploaded_filename", "$folder/$menu");
			
			$fullmenu = 'fullmenu-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(1160, 315, "$folder/$uploaded_filename", "$folder/$fullmenu");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}
		else if($_REQUEST["MediaUrl"] !="" && $params["MediaType"] == 2 )
		{
			// Get Youtube Code
			parse_str(parse_url($_REQUEST["MediaUrl"], PHP_URL_QUERY), $ID_youtube);
			$MediaUrl = $ID_youtube['v'];
			
			// Get Still Image
			//$url   = 'http://img.youtube.com/vi/'.$CvdUrl.'/maxresdefault.jpg';
			
			$params["MediaFilename"] = $MediaUrl;
			
		}	
		else if(!empty($_FILES["File"]["tmp_name"]) && $params["MediaType"] == 3)
		{
			$params["MediaFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_media', $params["MediaID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			$file_dimensions = getimagesize($tmp_uploaded_file);
			$file_size = filesize($tmp_uploaded_file);
			$file_size = $file_size / '1048576';
			$file_size = number_format($file_size, 2, '.', '');
			$extension = pathinfo($uploaded_filename, PATHINFO_EXTENSION);

			$params['MediaFilesize'] 	 = $file_size . ' MB';
			$params['MediaExtension'] = $extension;
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
		}		
			
		
		$result = DB_sys_media::sys_media_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_MediaRecord
	****************************************************************************************/
	function get_MediaRecord()
	{
		$params["MediaID"] = $_REQUEST["MediaID"];
			
		$results = DB_sys_media::sys_media_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateMediaRecord
	****************************************************************************************/
	function UpdateMediaRecord()
	{
		$params = array();

		if(isset($_REQUEST['MediaID'])) { $params['MediaID'] = $_REQUEST['MediaID']; }
		if(isset($_REQUEST['MediaSiteID'])) { $params['MediaSiteID'] = $_REQUEST['MediaSiteID']; }
		if(isset($_REQUEST['MediaType'])) { $params['MediaType'] = $_REQUEST['MediaType']; }
		if(isset($_REQUEST['MediaTitle'])) { $params['MediaTitle'] = $_REQUEST['MediaTitle']; }
	
			
		if(!empty($_FILES["File"]["tmp_name"]) && $params["MediaType"] == 1)
		{
			$params["MediaFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_media', $params["MediaID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$menu = 'menu-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(560, 315, "$folder/$uploaded_filename", "$folder/$menu");
			
			$fullmenu = 'fullmenu-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(1160, 315, "$folder/$uploaded_filename", "$folder/$fullmenu");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}
		else if($_REQUEST["MediaUrl"] !="" && $params["MediaType"] == 2 )
		{
			// Get Youtube Code
			parse_str(parse_url($_REQUEST["MediaUrl"], PHP_URL_QUERY), $ID_youtube);
			$MediaUrl = $ID_youtube['v'];
			
			// Get Still Image
			//$url   = 'http://img.youtube.com/vi/'.$CvdUrl.'/maxresdefault.jpg';
			
			$params["MediaFilename"] = $MediaUrl;
			
		}	
		else if(!empty($_FILES["File"]["tmp_name"]) && $params["MediaType"] == 3)
		{
			$params["MediaFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_media', $params["MediaID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			$file_dimensions = getimagesize($tmp_uploaded_file);
			$file_size = filesize($tmp_uploaded_file);
			$file_size = $file_size / '1048576';
			$file_size = number_format($file_size, 2, '.', '');
			$extension = pathinfo($uploaded_filename, PATHINFO_EXTENSION);

			$params['MediaFilesize'] 	 = $file_size . ' MB';
			$params['MediaExtension'] = $extension;
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
		}				
		
		$result = DB_sys_media::sys_media_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteMediaRecord
	****************************************************************************************/
	function DeleteMediaRecord()
	{
		$params = array();
		
		$params["MediaID"] = $_REQUEST["MediaID"];
		
		$results = DB_sys_media::sys_media_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::getMediaList
	****************************************************************************************/
	function getMediaList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['MediaType'] 	= isset($_REQUEST['MediaType']) ? $_REQUEST['MediaType'] : 0;
		
		$results = DB_sys_media::sys_media_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		NODE MEDIA
	*/
	
	/****************************************************************************************
	* Home::AddMediaToNode
	****************************************************************************************/
	function AddMediaToNode()
	{
		$params = array();
		
		$params["NodeMediaNodeID"] = $_REQUEST["NodeMediaNodeID"];
		$params["NodeMediaMediaID"] = $_REQUEST["NodeMediaMediaID"];
		
		$NodeMediaDefValues = DB_sys_node_media::sys_node_media_get_NewRecordDefValues($this->appFrw, $params);
		
		$NodeMediaID = $NodeMediaDefValues['data']['NodeMediaID'];
		
		$params["NodeMediaID"] = $NodeMediaID;
		$params["NodeMediaPriority"] = 100;
		
		$result = DB_sys_node_media::sys_node_media_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));	
		
	}
	
	/****************************************************************************************
	* Home::get_NodeMediaRecord
	****************************************************************************************/
	function get_NodeMediaRecord()
	{
		$params["NodeMediaID"] = $_REQUEST["NodeMediaID"];
			
		$results = DB_sys_node_media::sys_node_media_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateNodeMediaRecord
	****************************************************************************************/
	function UpdateNodeMediaRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['NodeMediaID'])) { $params['NodeMediaID'] = $_REQUEST['NodeMediaID']; }
		if(isset($_REQUEST['NodeMediaPriority'])) { $params['NodeMediaPriority'] = $_REQUEST['NodeMediaPriority']; }
		
		$results = DB_sys_node_media::sys_node_media_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteNodeMediaRecord
	****************************************************************************************/
	function DeleteNodeMediaRecord()
	{
		$params = array();
		
		$params["NodeMediaID"] = $_REQUEST["NodeMediaID"];
		
		$results = DB_sys_node_media::sys_node_media_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::getNodeMediaList
	****************************************************************************************/
	function getNodeMediaList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['NodeID'] 	= isset($_REQUEST['NodeID']) ? $_REQUEST['NodeID'] : 0;
		$params['MediaType'] 	= isset($_REQUEST['MediaType']) ? $_REQUEST['MediaType'] : 0;
		$params['NodeLngType'] 	= isset($_REQUEST['NodeLngType']) ? $_REQUEST['NodeLngType'] : -1;
		
		if ($_REQUEST['NodeLngType'] == -1)
		{
			$results = DB_sys_node_media::sys_node_media_getList($this->appFrw, $params);
		}
		else
		{
			$results = DB_sys_node_media::sys_node_media_getListSitePages($this->appFrw, $params);
		}
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		MEDIA LNG
	*/
	
	/****************************************************************************************
	* Home::get_MediaLngRecord
	****************************************************************************************/
	function get_MediaLngRecord()
	{
		//Check if Record Exists
		$params["MediaLngMediaID"] = $_REQUEST["MediaLngMediaID"];
		$params["MediaLngType"] = $_REQUEST["MediaLngType"];
		
		
		$CheckIfMediaLngExists = DB_sys_medialng::sys_medialng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfMediaLngExists == 0)
		{
			// // Insert NodeLng
			$MediaLngDefValues = DB_sys_medialng::sys_medialng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["MediaLngID"] = $MediaLngDefValues['data']['MediaLngID'];
			
			$result = DB_sys_medialng::sys_medialng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_medialng::sys_medialng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$MediaLngID = DB_sys_medialng::sys_medialng_GetMediaLngID($this->appFrw, $params);
			
			$params["MediaLngID"] = $MediaLngID; 
			
			$results = DB_sys_medialng::sys_medialng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
		
		
	}
	
	/****************************************************************************************
	* Home::UpdateMediaLngRecord
	****************************************************************************************/
	function UpdateMediaLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['MediaLngID'])) { $params['MediaLngID'] = $_REQUEST['MediaLngID']; }
		if(isset($_REQUEST['MediaLngType'])) { $params['MediaLngType'] = $_REQUEST['MediaLngType']; }
		if(isset($_REQUEST['MediaLngMediaID'])) { $params['MediaLngMediaID'] = $_REQUEST['MediaLngMediaID']; }
		if(isset($_REQUEST['MediaLngTitle'])) { $params['MediaLngTitle'] = $_REQUEST['MediaLngTitle']; }
		if(isset($_REQUEST['MediaLngDescription'])) { $params['MediaLngDescription'] = $_REQUEST['MediaLngDescription']; }
		
		$results = DB_sys_medialng::sys_medialng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		ALERTS
	*/
	
	/****************************************************************************************
	* Home::get_Alert_NewRecordDefValues
	****************************************************************************************/
	function get_Alert_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_alert::sys_alert_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertAlertRecord
	****************************************************************************************/
	function InsertAlertRecord()
	{
		$params = array();
		
		$params["AlertID"] = $_REQUEST["AlertID"];
		$params["AlertSiteID"] = $_REQUEST["AlertSiteID"];
		$params["AlertStatus"] = $_REQUEST["AlertStatus"];
		$params["AlertPriority"] = $_REQUEST["AlertPriority"];
		$params["AlertType"] = $_REQUEST["AlertType"];
		$params["AlertTitle"] = $_REQUEST["AlertTitle"];
		$params["AlertStartDate"] = $_REQUEST["AlertStartDate"];
		$params["AlertStartTime"] = $_REQUEST["AlertStartTime"];
		$params["AlertEndDate"] = $_REQUEST["AlertEndDate"];
		$params["AlertEndTime"] = $_REQUEST["AlertEndTime"];
		
		$results = DB_sys_alert::sys_alert_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_AlertRecord
	****************************************************************************************/
	function get_AlertRecord()
	{
		$params["AlertID"] = $_REQUEST["AlertID"];
			
		$results = DB_sys_alert::sys_alert_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateAlertRecord
	****************************************************************************************/
	function UpdateAlertRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['AlertID'])) { $params['AlertID'] = $_REQUEST['AlertID']; }
		if(isset($_REQUEST['AlertSiteID'])) { $params['AlertSiteID'] = $_REQUEST['AlertSiteID']; }
		if(isset($_REQUEST['AlertStatus'])) { $params['AlertStatus'] = $_REQUEST['AlertStatus']; }
		if(isset($_REQUEST['AlertPriority'])) { $params['AlertPriority'] = $_REQUEST['AlertPriority']; }
		if(isset($_REQUEST['AlertType'])) { $params['AlertType'] = $_REQUEST['AlertType']; }
		if(isset($_REQUEST['AlertTitle'])) { $params['AlertTitle'] = $_REQUEST['AlertTitle']; }
		if(isset($_REQUEST['AlertStartDate'])) { $params['AlertStartDate'] = $_REQUEST['AlertStartDate']; }
		if(isset($_REQUEST['AlertStartTime'])) { $params['AlertStartTime'] = $_REQUEST['AlertStartTime']; }
		if(isset($_REQUEST['AlertEndDate'])) { $params['AlertEndDate'] = $_REQUEST['AlertEndDate']; }
		if(isset($_REQUEST['AlertEndTime'])) { $params['AlertEndTime'] = $_REQUEST['AlertEndTime']; }
		
		$results = DB_sys_alert::sys_alert_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteAlertRecord
	****************************************************************************************/
	function DeleteAlertRecord()
	{
		$params = array();
		
		$params["AlertID"] = $_REQUEST["AlertID"];
		
		$results = DB_sys_alert::sys_alert_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getAlertsList
	****************************************************************************************/
	function getAlertList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		$params['filterType'] 	= isset($_REQUEST['filterType']) ? $_REQUEST['filterType'] : -1;

		
		$results = DB_sys_alert::sys_alert_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		ALERT LNG
	*/
	
	/****************************************************************************************
	* Home::get_AlertLngRecord
	****************************************************************************************/
	function get_AlertLngRecord()
	{
		//Check if Record Exists
		$params["AlertLngAlertID"] = $_REQUEST["AlertLngAlertID"];
		$params["AlertLngType"] = $_REQUEST["AlertLngType"];
		
		
		$CheckIfAlertLngExists = DB_sys_alertlng::sys_alertlng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfAlertLngExists == 0)
		{
			// // Insert NodeLng
			$AlertLngDefValues = DB_sys_alertlng::sys_alertlng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["AlertLngID"] = $AlertLngDefValues['data']['AlertLngID'];
			
			$result = DB_sys_alertlng::sys_alertlng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_alertlng::sys_alertlng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$AlertLngID = DB_sys_alertlng::sys_alertlng_GetAlertLngID($this->appFrw, $params);
			
			$params["AlertLngID"] = $AlertLngID; 
			
			$results = DB_sys_alertlng::sys_alertlng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	/****************************************************************************************
	* Home::UpdateAlertLngRecord
	****************************************************************************************/
	function UpdateAlertLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['AlertLngID'])) { $params['AlertLngID'] = $_REQUEST['AlertLngID']; }
		if(isset($_REQUEST['AlertLngType'])) { $params['AlertLngType'] = $_REQUEST['AlertLngType']; }
		if(isset($_REQUEST['AlertLngAlertID'])) { $params['AlertLngAlertID'] = $_REQUEST['AlertLngAlertID']; }
		if(isset($_REQUEST['AlertLngTitle'])) { $params['AlertLngTitle'] = $_REQUEST['AlertLngTitle']; }
		if(isset($_REQUEST['AlertLngContent'])) { $params['AlertLngContent'] = $_REQUEST['AlertLngContent']; }
		
		$results = DB_sys_alertlng::sys_alertlng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		SLIDERS
	*/
	
	/****************************************************************************************
	* Home::get_Slider_NewRecordDefValues
	****************************************************************************************/
	function get_Slider_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_slider::sys_slider_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertSliderRecord
	****************************************************************************************/
	function InsertSliderRecord()
	{
		$params = array();
		
		$params["SliderID"] = $_REQUEST["SliderID"];
		$params["SliderSiteID"] = $_REQUEST["SliderSiteID"];
		$params["SliderStatus"] = $_REQUEST["SliderStatus"];
		$params["SliderTitle"] = $_REQUEST["SliderTitle"];
		
		$results = DB_sys_slider::sys_slider_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_SliderRecord
	****************************************************************************************/
	function get_SliderRecord()
	{
		$params["SliderID"] = $_REQUEST["SliderID"];
			
		$results = DB_sys_slider::sys_slider_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateSliderRecord
	****************************************************************************************/
	function UpdateSliderRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['SliderID'])) { $params['SliderID'] = $_REQUEST['SliderID']; }
		if(isset($_REQUEST['SliderSiteID'])) { $params['SliderSiteID'] = $_REQUEST['SliderSiteID']; }
		if(isset($_REQUEST['SliderStatus'])) { $params['SliderStatus'] = $_REQUEST['SliderStatus']; }
		if(isset($_REQUEST['SliderTitle'])) { $params['SliderTitle'] = $_REQUEST['SliderTitle']; }
		
		$results = DB_sys_slider::sys_slider_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteSliderRecord
	****************************************************************************************/
	function DeleteSliderRecord()
	{
		$params = array();
		
		$params["SliderID"] = $_REQUEST["SliderID"];
		
		$results = DB_sys_slider::sys_slider_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getSliderList
	****************************************************************************************/
	function getSliderList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		$results = DB_sys_slider::sys_slider_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		SLIDER IMAGES
	*/
	
	
	/****************************************************************************************
	* Home::get_SliderImages_NewRecordDefValues
	****************************************************************************************/
	function get_SliderImages_NewRecordDefValues()
	{
		$params = array();
		
		$params["SliderID"] = $_REQUEST["SliderID"];
		
		$results = DB_sys_slider_images::sys_slider_images_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertSliderImagesRecord
	****************************************************************************************/
	function InsertSliderImagesRecord()
	{
		$params = array();
		
		$params["SliderImagesID"] = $_REQUEST["SliderImagesID"];
		$params["SliderImagesSliderID"] = $_REQUEST["SliderImagesSliderID"];
		$params["SliderImagesPriority"] = $_REQUEST["SliderImagesPriority"];
		$params["SliderImagesTitle"] = $_REQUEST["SliderImagesTitle"];
		$params["SliderImagesNodeID"] = $_REQUEST["SliderImagesNodeID"];
		$params["SliderImagesStatus"] = $_REQUEST["SliderImagesStatus"];
		//$params["SliderImagesExternalLink"] = $_REQUEST["SliderImagesExternalLink"];

		
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["SliderImagesFIlename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_slider_images', $params["SliderImagesID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
			
			$mdnew = 'mdnew-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$mdnew");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
		
			
		}	
		
		$result = DB_sys_slider_images::sys_slider_images_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_SliderImagesRecord
	****************************************************************************************/
	function get_SliderImagesRecord()
	{
		$params["SliderImagesID"] = $_REQUEST["SliderImagesID"];
			
		$results = DB_sys_slider_images::sys_slider_images_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateSliderImagesRecord
	****************************************************************************************/
	function UpdateSliderImagesRecord()
	{
		$params = array();

		if(isset($_REQUEST['SliderImagesID'])) { $params['SliderImagesID'] = $_REQUEST['SliderImagesID']; }
		if(isset($_REQUEST['SliderImagesSliderID'])) { $params['SliderImagesSliderID'] = $_REQUEST['SliderImagesSliderID']; }
		if(isset($_REQUEST['SliderImagesPriority'])) { $params['SliderImagesPriority'] = $_REQUEST['SliderImagesPriority']; }
		if(isset($_REQUEST['SliderImagesTitle'])) { $params['SliderImagesTitle'] = $_REQUEST['SliderImagesTitle']; }
		if(isset($_REQUEST['SliderImagesNodeID'])) { $params['SliderImagesNodeID'] = $_REQUEST['SliderImagesNodeID']; }
		if(isset($_REQUEST['SliderImagesStatus'])) { $params['SliderImagesStatus'] = $_REQUEST['SliderImagesStatus']; }
		//if(isset($_REQUEST['SliderImagesExternalLink'])) { $params['SliderImagesExternalLink'] = $_REQUEST['SliderImagesExternalLink']; }
	
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["SliderImagesFIlename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_slider_images', $params["SliderImagesID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$mdnew = 'mdnew-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$mdnew");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
		
		}	
		
		$result = DB_sys_slider_images::sys_slider_images_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteSliderImagesRecord
	****************************************************************************************/
	function DeleteSliderImagesRecord()
	{
		$params = array();
		
		$params["SliderImagesID"] = $_REQUEST["SliderImagesID"];
		
		$results = DB_sys_slider_images::sys_slider_images_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getSliderImagesList
	****************************************************************************************/
	function getSliderImagesList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SliderID'] 	= isset($_REQUEST['SliderID']) ? $_REQUEST['SliderID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_slider_images::sys_slider_images_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getSliderImagesListSitePages
	****************************************************************************************/
	function getSliderImagesListSitePages()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['NodeSliderNodeID'] 	= isset($_REQUEST['NodeSliderNodeID']) ? $_REQUEST['NodeSliderNodeID'] : 0;
		$params['SliderImagesLngType'] 	= isset($_REQUEST['NodeLangType']) ? $_REQUEST['NodeLangType'] : 0;
		
		
		$results = DB_sys_slider_images::getSliderImagesListSitePages($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		SLIDER IMAGES LNG
	*/
	
	/****************************************************************************************
	* Home::get_SliderImagesLngRecord
	****************************************************************************************/
	function get_SliderImagesLngRecord()
	{
		//Check if Record Exists
		$params["SliderImagesLngSliderImagesID"] = $_REQUEST["SliderImagesLngSliderImagesID"];
		$params["SliderImagesLngType"] = $_REQUEST["SliderImagesLngType"];
		
		
		$CheckIfSliderImagesLngExists = DB_sys_slider_imageslng::sys_slider_imageslng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfSliderImagesLngExists == 0)
		{
			// // Insert NodeLng
			$SliderImagesLngDefValues = DB_sys_slider_imageslng::sys_slider_imageslng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["SliderImagesLngID"] = $SliderImagesLngDefValues['data']['SliderImagesLngID'];
			
			$result = DB_sys_slider_imageslng::sys_slider_imageslng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_slider_imageslng::sys_slider_imageslng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$SliderImagesLngID = DB_sys_slider_imageslng::sys_slider_imageslng_GetSliderImagesLngID($this->appFrw, $params);
			
			$params["SliderImagesLngID"] = $SliderImagesLngID; 
			
			$results = DB_sys_slider_imageslng::sys_slider_imageslng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
		
		
	}
	
	/****************************************************************************************
	* Home::UpdateSliderImagesLngRecord
	****************************************************************************************/
	function UpdateSliderImagesLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['SliderImagesLngID'])) { $params['SliderImagesLngID'] = $_REQUEST['SliderImagesLngID']; }
		if(isset($_REQUEST['SliderImagesLngType'])) { $params['SliderImagesLngType'] = $_REQUEST['SliderImagesLngType']; }
		if(isset($_REQUEST['SliderImagesLngSliderImagesID'])) { $params['SliderImagesLngSliderImagesID'] = $_REQUEST['SliderImagesLngSliderImagesID']; }
		if(isset($_REQUEST['SliderImagesLngTitle'])) { $params['SliderImagesLngTitle'] = $_REQUEST['SliderImagesLngTitle']; }
		if(isset($_REQUEST['SliderImagesLngSubtitle'])) { $params['SliderImagesLngSubtitle'] = $_REQUEST['SliderImagesLngSubtitle']; }
		if(isset($_REQUEST['SliderImagesLngLinkTitle'])) { $params['SliderImagesLngLinkTitle'] = $_REQUEST['SliderImagesLngLinkTitle']; }
		if(isset($_REQUEST['SliderImagesLngExternalLink'])) { $params['SliderImagesLngExternalLink'] = $_REQUEST['SliderImagesLngExternalLink']; }
        if(isset($_REQUEST['SliderImagesLngExternalLinkTarget'])) { $params['SliderImagesLngExternalLinkTarget'] = $_REQUEST['SliderImagesLngExternalLinkTarget']; }

        $results = DB_sys_slider_imageslng::sys_slider_imageslng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		NODE SLIDER
	*/
	
	/****************************************************************************************
	* Home::AddSliderToNode
	****************************************************************************************/
	function AddSliderToNode()
	{
		$params = array();
		
		$params["NodeSliderNodeID"] = $_REQUEST["NodeSliderNodeID"];
		$params["NodeSliderSliderID"] = $_REQUEST["NodeSliderSliderID"];
		
		//Check if Slider is already added
		$CheckIfSliderIsAlreadyAdded = DB_sys_node_slider::sys_node_slider_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfSliderIsAlreadyAdded == 0)
		{
				$NodeSliderDefValues = DB_sys_node_slider::sys_node_slider_get_NewRecordDefValues($this->appFrw, $params);
		
				$NodeSliderID = $NodeSliderDefValues['data']['NodeSliderID'];
				
				$params["NodeSliderID"] = $NodeSliderID;
				
				$result = DB_sys_node_slider::sys_node_slider_InsertRecord($this->appFrw, $params);
				
				if ($result['success']==true)
					return json_encode($result) ;
				else 
					return json_encode((array('error' => $result["reason"])));	
		}
		else
		{
			return json_encode((array('error' => 'You can not add a second slider to the selected node')));	
		}	
		
		
		
	}
	
	/****************************************************************************************
	* Home::get_NodeSliderRecord
	****************************************************************************************/
	function get_NodeSliderRecord()
	{
		// $params["NodeMediaID"] = $_REQUEST["NodeMediaID"];
			
		// $results = DB_sys_node_media::sys_node_media_getRecord($this->appFrw, $params);
		
		// if ($results['success']==true)
			// return json_encode((array('data' => $results["data"])));
		// else 
			// return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateNodeSliderRecord
	****************************************************************************************/
	function UpdateNodeSliderRecord()
	{
		// $params = array();
		
		// if(isset($_REQUEST['NodeMediaID'])) { $params['NodeMediaID'] = $_REQUEST['NodeMediaID']; }
		// if(isset($_REQUEST['NodeMediaPriority'])) { $params['NodeMediaPriority'] = $_REQUEST['NodeMediaPriority']; }
		
		// $results = DB_sys_node_media::sys_node_media_UpdateRecord($this->appFrw, $params);
		
		// if ($results['success']==true)
			// return json_encode((array('data' => $results["data"])));
		// else 
			// return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteNodeSliderRecord
	****************************************************************************************/
	function DeleteNodeSliderRecord()
	{
		$params = array();
		
		$params["NodeSliderID"] = $_REQUEST["NodeSliderID"];
		
		$results = DB_sys_node_slider::sys_node_slider_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::getNodeSliderList
	****************************************************************************************/
	function getNodeSliderList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['NodeID'] 	= isset($_REQUEST['NodeID']) ? $_REQUEST['NodeID'] : 0;
		$params['NodeLngType'] 	= isset($_REQUEST['NodeLngType']) ? $_REQUEST['NodeLngType'] : -1;
		

		$results = DB_sys_node_slider::sys_node_slider_getList($this->appFrw, $params);

	
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		CATEGORY SLIDER
	*/
	
	/****************************************************************************************
	* Home::AddSliderToCategory
	****************************************************************************************/
	function AddSliderToCategory()
	{
		$params = array();
		
		$params["CtgRecSliderCtgRecID"] = $_REQUEST["CtgRecSliderCtgRecID"];
		$params["CtgRecSliderSliderID"] = $_REQUEST["CtgRecSliderSliderID"];
		
		//Check if Slider is already added
		$CheckIfSliderIsAlreadyAdded = DB_sys_categories_records_slider::sys_categories_records_slider_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfSliderIsAlreadyAdded == 0)
		{
				$NodeSliderDefValues = DB_sys_categories_records_slider::sys_categories_records_slider_get_NewRecordDefValues($this->appFrw, $params);
		
				$CtgRecSliderID = $NodeSliderDefValues['data']['CtgRecSliderID'];
				
				$params["CtgRecSliderID"] = $CtgRecSliderID;
				
				$result = DB_sys_categories_records_slider::sys_categories_records_slider_InsertRecord($this->appFrw, $params);
				
				if ($result['success']==true)
					return json_encode($result) ;
				else 
					return json_encode((array('error' => $result["reason"])));	
		}
		else
		{
			return json_encode((array('error' => 'You can not add a second slider to the selected category')));	
		}	
		
		
		
	}
	
	/****************************************************************************************
	* Home::get_NodeSliderRecord
	****************************************************************************************/
	// function get_NodeSliderRecord()
	// {
		// // $params["NodeMediaID"] = $_REQUEST["NodeMediaID"];
			
		// // $results = DB_sys_node_media::sys_node_media_getRecord($this->appFrw, $params);
		
		// // if ($results['success']==true)
			// // return json_encode((array('data' => $results["data"])));
		// // else 
			// // return json_encode((array('error' => $results["reason"])));
	// }
	
	/****************************************************************************************
	* Home::UpdateNodeSliderRecord
	****************************************************************************************/
	// function UpdateNodeSliderRecord()
	// {
		// // $params = array();
		
		// // if(isset($_REQUEST['NodeMediaID'])) { $params['NodeMediaID'] = $_REQUEST['NodeMediaID']; }
		// // if(isset($_REQUEST['NodeMediaPriority'])) { $params['NodeMediaPriority'] = $_REQUEST['NodeMediaPriority']; }
		
		// // $results = DB_sys_node_media::sys_node_media_UpdateRecord($this->appFrw, $params);
		
		// // if ($results['success']==true)
			// // return json_encode((array('data' => $results["data"])));
		// // else 
			// // return json_encode((array('error' => $results["reason"])));
	// }
	
	/****************************************************************************************
	* Home::DeleteCategorySliderRecord
	****************************************************************************************/
	function DeleteCategorySliderRecord()
	{
		$params = array();
		
		$params["CtgRecSliderID"] = $_REQUEST["CtgRecSliderID"];
		
		$results = DB_sys_categories_records_slider::sys_categories_records_slider_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::getCategorySliderList
	****************************************************************************************/
	function getCategorySliderList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['CtgRecID'] 	= isset($_REQUEST['CtgRecID']) ? $_REQUEST['CtgRecID'] : 0;
		// $params['NodeLngType'] 	= isset($_REQUEST['NodeLngType']) ? $_REQUEST['NodeLngType'] : -1;
		

		$results = DB_sys_categories_records_slider::sys_categories_records_slider_getList($this->appFrw, $params);

	
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		DISASTER MODE
	*/
	
	/****************************************************************************************
	* Home::get_Disaster_NewRecordDefValues
	****************************************************************************************/
	function get_Disaster_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_disaster::sys_disaster_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertDisasterRecord
	****************************************************************************************/
	function InsertDisasterRecord()
	{
		$params = array();
		
		$params["DisasterID"] = $_REQUEST["DisasterID"];
		$params["DisasterSiteID"] = $_REQUEST["DisasterSiteID"];
		$params["DisasterTitle"] = $_REQUEST["DisasterTitle"];
		$params["DisasterStatus"] = 2;
		
		$results = DB_sys_disaster::sys_disaster_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_DisasterRecord
	****************************************************************************************/
	function get_DisasterRecord()
	{
		$params["DisasterID"] = $_REQUEST["DisasterID"];
			
		$results = DB_sys_disaster::sys_disaster_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateDisasterRecord
	****************************************************************************************/
	function UpdateDisasterRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['DisasterID'])) { $params['DisasterID'] = $_REQUEST['DisasterID']; }
		if(isset($_REQUEST['DisasterSiteID'])) { $params['DisasterSiteID'] = $_REQUEST['DisasterSiteID']; }
		if(isset($_REQUEST['DisasterTitle'])) { $params['DisasterTitle'] = $_REQUEST['DisasterTitle']; }
		if(isset($_REQUEST['DisasterStatus'])) { $params['DisasterStatus'] = $_REQUEST['DisasterStatus']; }
	
		$results = DB_sys_disaster::sys_disaster_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteDisasterRecord
	****************************************************************************************/
	function DeleteDisasterRecord()
	{
		$params = array();
		
		$params["DisasterID"] = $_REQUEST["DisasterID"];
		
		$results = DB_sys_disaster::sys_disaster_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getDisasterList
	****************************************************************************************/
	function getDisasterList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;

		
		$results = DB_sys_disaster::sys_disaster_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		DISASTER LNG
	*/
	
	/****************************************************************************************
	* Home::get_DisasterLngRecord
	****************************************************************************************/
	function get_DisasterLngRecord()
	{
		//Check if Record Exists
		$params["DisasterLngDisasterID"] = $_REQUEST["DisasterLngDisasterID"];
		$params["DisasterLngType"] = $_REQUEST["DisasterLngType"];
		
		
		$CheckIDisasterLngExists = DB_sys_disasterlng::sys_disasterlng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIDisasterLngExists == 0)
		{
			$DisasterLngDefValues = DB_sys_disasterlng::sys_disasterlng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["DisasterLngID"] = $DisasterLngDefValues['data']['DisasterLngID'];
			
			$result = DB_sys_disasterlng::sys_disasterlng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_disasterlng::sys_disasterlng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$DisasterLngID = DB_sys_disasterlng::sys_disasterlng_GetDisasterLngID($this->appFrw, $params);
			
			$params["DisasterLngID"] = $DisasterLngID; 
			
			$results = DB_sys_disasterlng::sys_disasterlng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	/****************************************************************************************
	* Home::UpdateDisasterLngRecord
	****************************************************************************************/
	function UpdateDisasterLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['DisasterLngID'])) { $params['DisasterLngID'] = $_REQUEST['DisasterLngID']; }
		if(isset($_REQUEST['DisasterLngType'])) { $params['DisasterLngType'] = $_REQUEST['DisasterLngType']; }
		if(isset($_REQUEST['DisasterLngDisasterID'])) { $params['DisasterLngDisasterID'] = $_REQUEST['DisasterLngDisasterID']; }
		if(isset($_REQUEST['DisasterLngTitle'])) { $params['DisasterLngTitle'] = $_REQUEST['DisasterLngTitle']; }
		if(isset($_REQUEST['DisasterLngContent'])) { $params['DisasterLngContent'] = $_REQUEST['DisasterLngContent']; }
		
		$results = DB_sys_disasterlng::sys_disasterlng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		Airlines LNG
	*/
	
	/****************************************************************************************
	* Home::get_SiteAirlineLngRecord
	****************************************************************************************/
	function get_SiteAirlineLngRecord()
	{
		//Check if Record Exists
		$params["SiteAirlineLngSiteAirlineID"] = $_REQUEST["SiteAirlineLngSiteAirlineID"];
		$params["SiteAirlineLngType"] = $_REQUEST["SiteAirlineLngType"];
		
		
		$CheckIfSiteAirlineLngExists = DB_sys_site_airlinelng::sys_site_airlinelng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfSiteAirlineLngExists == 0)
		{
			$SiteAirlineLngDefValues = DB_sys_site_airlinelng::sys_site_airlinelng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["SiteAirlineLngID"] = $SiteAirlineLngDefValues['data']['SiteAirlineLngID'];
			
			$result = DB_sys_site_airlinelng::sys_site_airlinelng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_site_airlinelng::sys_site_airlinelng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$SiteAirlineLngID = DB_sys_site_airlinelng::sys_site_airlinelng_GetSiteAirlineLngID($this->appFrw, $params);
			
			$params["SiteAirlineLngID"] = $SiteAirlineLngID; 
			
			$results = DB_sys_site_airlinelng::sys_site_airlinelng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	/****************************************************************************************
	* Home::UpdateSiteAirlineLngRecord
	****************************************************************************************/
	function UpdateSiteAirlineLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['SiteAirlineLngID'])) { $params['SiteAirlineLngID'] = $_REQUEST['SiteAirlineLngID']; }
		if(isset($_REQUEST['SiteAirlineLngType'])) { $params['SiteAirlineLngType'] = $_REQUEST['SiteAirlineLngType']; }
		if(isset($_REQUEST['SiteAirlineLngSiteAirlineID'])) { $params['SiteAirlineLngSiteAirlineID'] = $_REQUEST['SiteAirlineLngSiteAirlineID']; }
		// if(isset($_REQUEST['SiteAirlineLngCheckInCounters'])) { $params['SiteAirlineLngCheckInCounters'] = $_REQUEST['SiteAirlineLngCheckInCounters']; }
		if(isset($_REQUEST['SiteAirlineLngAirportPhones'])) { $params['SiteAirlineLngAirportPhones'] = $_REQUEST['SiteAirlineLngAirportPhones']; }
		if(isset($_REQUEST['SiteAirlineLngReservations'])) { $params['SiteAirlineLngReservations'] = $_REQUEST['SiteAirlineLngReservations']; }
		if(isset($_REQUEST['SiteAirlineLngLostAndFound'])) { $params['SiteAirlineLngLostAndFound'] = $_REQUEST['SiteAirlineLngLostAndFound']; }
		if(isset($_REQUEST['SiteAirlineLngHandler'])) { $params['SiteAirlineLngHandler'] = $_REQUEST['SiteAirlineLngHandler']; }
		if(isset($_REQUEST['SiteAirlineLngHandlerAirportPhone'])) { $params['SiteAirlineLngHandlerAirportPhone'] = $_REQUEST['SiteAirlineLngHandlerAirportPhone']; }
		if(isset($_REQUEST['SiteAirlineLngEmail'])) { $params['SiteAirlineLngEmail'] = $_REQUEST['SiteAirlineLngEmail']; }
		
		$results = DB_sys_site_airlinelng::sys_site_airlinelng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		RELATED LINKS
	*/
	
	/****************************************************************************************
	* Home::get_RelatedLink_NewRecordDefValues
	****************************************************************************************/
	function get_RelatedLink_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_related_links::sys_related_links_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertRelatedLinkRecord
	****************************************************************************************/
	function InsertRelatedLinkRecord()
	{
		$params = array();
		
		$params["RelatedLinkID"] = $_REQUEST["RelatedLinkID"];
		$params["RelatedLinkSiteID"] = $_REQUEST["RelatedLinkSiteID"];
		$params["RelatedLinkType"] = $_REQUEST["RelatedLinkType"];
		$params["RelatedLinkTitle"] = $_REQUEST["RelatedLinkTitle"];
		$params["RelatedLinkNodeID"] = $_REQUEST["RelatedLinkNodeID"];

		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["RelatedLinkFIlename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_related_links', $params["RelatedLinkID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
			
			$large_thumb = 'large_thumb-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(220, 220, "$folder/$uploaded_filename", "$folder/$large_thumb");
		}	
		
		$result = DB_sys_related_links::sys_related_links_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_RelatedLinkRecord
	****************************************************************************************/
	function get_RelatedLinkRecord()
	{
		$params["RelatedLinkID"] = $_REQUEST["RelatedLinkID"];
			
		$results = DB_sys_related_links::sys_related_links_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateRelatedLinkRecord
	****************************************************************************************/
	function UpdateRelatedLinkRecord()
	{
		$params = array();

		if(isset($_REQUEST['RelatedLinkID'])) { $params['RelatedLinkID'] = $_REQUEST['RelatedLinkID']; }
		if(isset($_REQUEST['RelatedLinkSiteID'])) { $params['RelatedLinkSiteID'] = $_REQUEST['RelatedLinkSiteID']; }
		if(isset($_REQUEST['RelatedLinkType'])) { $params['RelatedLinkType'] = $_REQUEST['RelatedLinkType']; }
		if(isset($_REQUEST['RelatedLinkTitle'])) { $params['RelatedLinkTitle'] = $_REQUEST['RelatedLinkTitle']; }
		if(isset($_REQUEST['RelatedLinkNodeID'])) { $params['RelatedLinkNodeID'] = $_REQUEST['RelatedLinkNodeID']; }
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["RelatedLinkFIlename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_related_links', $params["RelatedLinkID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
			
			$large_thumb = 'large_thumb-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(220, 220, "$folder/$uploaded_filename", "$folder/$large_thumb");
		}	
		
		$result = DB_sys_related_links::sys_related_links_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteRelatedLinkRecord
	****************************************************************************************/
	function DeleteRelatedLinkRecord()
	{
		$params = array();
		
		$params["RelatedLinkID"] = $_REQUEST["RelatedLinkID"];
		
		$results = DB_sys_related_links::sys_related_links_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getRelatedLinkList
	****************************************************************************************/
	function getRelatedLinkList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterType'] 	= isset($_REQUEST['filterType']) ? $_REQUEST['filterType'] : -1;

		
		$results = DB_sys_related_links::sys_related_links_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		RELATED LINKS LNG
	*/
	
	/****************************************************************************************
	* Home::get_RelatedLinkLngRecord
	****************************************************************************************/
	function get_RelatedLinkLngRecord()
	{	
		//Check if Record Exists
		$params["RelatedLinkLngRelatedLinkID"] = $_REQUEST["RelatedLinkLngRelatedLinkID"];
		$params["RelatedLinkLngType"] = $_REQUEST["RelatedLinkLngType"];
		
		
		$CheckIfRelatedLinkLngExists = DB_sys_related_linkslng::sys_related_linkslng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfRelatedLinkLngExists == 0)
		{
			$RelatedLinkLngDefValues = DB_sys_related_linkslng::sys_related_linkslng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["RelatedLinkLngID"] = $RelatedLinkLngDefValues['data']['RelatedLinkLngID'];
			
			$result = DB_sys_related_linkslng::sys_related_linkslng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_related_linkslng::sys_related_linkslng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$RelatedLinkLngID = DB_sys_related_linkslng::sys_related_linkslng_GetRelatedLinkLngID($this->appFrw, $params);
			
			$params["RelatedLinkLngID"] = $RelatedLinkLngID; 
			
			$results = DB_sys_related_linkslng::sys_related_linkslng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	/****************************************************************************************
	* Home::UpdateRelatedLinkLngRecord
	****************************************************************************************/
	function UpdateRelatedLinkLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['RelatedLinkLngID'])) { $params['RelatedLinkLngID'] = $_REQUEST['RelatedLinkLngID']; }
		if(isset($_REQUEST['RelatedLinkLngType'])) { $params['RelatedLinkLngType'] = $_REQUEST['RelatedLinkLngType']; }
		if(isset($_REQUEST['RelatedLinkLngRelatedLinkID'])) { $params['RelatedLinkLngRelatedLinkID'] = $_REQUEST['RelatedLinkLngRelatedLinkID']; }
		if(isset($_REQUEST['RelatedLinkLngTitle'])) { $params['RelatedLinkLngTitle'] = $_REQUEST['RelatedLinkLngTitle']; }
		
		$results = DB_sys_related_linkslng::sys_related_linkslng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		NODE RELATED LINKS
	*/
	
	/****************************************************************************************
	* Home::AddLinkToNode
	****************************************************************************************/
	function AddLinkToNode()
	{
		$params = array();
		
		$params["NodeRelatedLinkNodeID"] = $_REQUEST["NodeRelatedLinkNodeID"];
		$params["NodeRelatedLinkRelatedLinkID"] = $_REQUEST["NodeRelatedLinkRelatedLinkID"];
		
		//Check if Slider is already added
		$CheckIfRelatedLinkIsAlreadyAdded = DB_sys_node_related_link::sys_node_related_link_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfRelatedLinkIsAlreadyAdded == 0)
		{
				$NodeRelatedLinkDefValues = DB_sys_node_related_link::sys_node_related_link_get_NewRecordDefValues($this->appFrw, $params);
		
				$NodeRelatedLinkID = $NodeRelatedLinkDefValues['data']['NodeRelatedLinkID'];
				
				$params["NodeRelatedLinkID"] = $NodeRelatedLinkID;
				$params["NodeRelatedLinkPriority"] = 100;
				
				$result = DB_sys_node_related_link::sys_node_related_link_InsertRecord($this->appFrw, $params);
				
				if ($result['success']==true)
					return json_encode($result) ;
				else 
					return json_encode((array('error' => $result["reason"])));	
		}
		else
		{
			return json_encode((array('error' => 'You can not add the link to the selected node, twice !')));	
		}	
		
		
		
	}
	
	/****************************************************************************************
	* Home::get_NodeRelatedLinkRecord
	****************************************************************************************/
	function get_NodeRelatedLinkRecord()
	{
		$params["NodeRelatedLinkID"] = $_REQUEST["NodeRelatedLinkID"];
			
		$results = DB_sys_node_related_link::sys_node_related_link_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateNodeRelatedLinkRecord
	****************************************************************************************/
	function UpdateNodeRelatedLinkRecord()
	{
		$params = array();

		if(isset($_REQUEST['NodeRelatedLinkID'])) { $params['NodeRelatedLinkID'] = $_REQUEST['NodeRelatedLinkID']; }
		if(isset($_REQUEST['NodeRelatedLinkPriority'])) { $params['NodeRelatedLinkPriority'] = $_REQUEST['NodeRelatedLinkPriority']; }
		
		$results = DB_sys_node_related_link::sys_node_related_link_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::DeleteNodeRelatedLinkRecord
	****************************************************************************************/
	function DeleteNodeRelatedLinkRecord()
	{
		$params = array();
		
		$params["NodeRelatedLinkID"] = $_REQUEST["NodeRelatedLinkID"];
		
		$results = DB_sys_node_related_link::sys_node_related_link_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::getNodeRelatedLinkList
	****************************************************************************************/
	function getNodeRelatedLinkList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['NodeID'] 	= isset($_REQUEST['NodeID']) ? $_REQUEST['NodeID'] : 0;
		$params['NodeLngType'] 	= isset($_REQUEST['NodeLngType']) ? $_REQUEST['NodeLngType'] : -1;
		
		if ($params['NodeLngType'] == -1)
		{
			$results = DB_sys_node_related_link::sys_node_related_link_getList($this->appFrw, $params);
		}
		else
		{
			$results = DB_sys_node_related_link::sys_node_related_link_getListSitePages($this->appFrw, $params);
		}
	
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/*
		Categories
	*/
	
	/****************************************************************************************
	* Home::get_Category_NewRecordDefValues
	****************************************************************************************/
	function get_Category_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_categories::sys_categories_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertCategoryRecord
	****************************************************************************************/
	function InsertCategoryRecord()
	{
		$params = array();
		
		$params["CtgID"] = $_REQUEST["CtgID"];
		$params["CtgSiteID"] = $_REQUEST["CtgSiteID"];
		$params["CtgTitle"] = $_REQUEST["CtgTitle"];
		
		$results = DB_sys_categories::sys_categories_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_CategoryRecord
	****************************************************************************************/
	function get_CategoryRecord()
	{
		$params["CtgID"] = $_REQUEST["CtgID"];
			
		$results = DB_sys_categories::sys_categories_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateCategoryRecord
	****************************************************************************************/
	function UpdateCategoryRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['CtgID'])) { $params['CtgID'] = $_REQUEST['CtgID']; }
		if(isset($_REQUEST['CtgTitle'])) { $params['CtgTitle'] = $_REQUEST['CtgTitle']; }
		
		$results = DB_sys_categories::sys_categories_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteCategory
	****************************************************************************************/
	function DeleteCategory()
	{
		$params = array();
		
		$params["CtgID"] = $_REQUEST["CtgID"];
		
		$results = DB_sys_categories::sys_categories_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getCategoriesList
	****************************************************************************************/
	function getCategoriesList()
	{
		$params = array();
		
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['ExcludeDefault'] 	= isset($_REQUEST['ExcludeDefault']) ? $_REQUEST['ExcludeDefault'] : 0;
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		
		
		$results = DB_sys_categories::sys_categories_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		CATEGORIES RECORDS
	*/
	
	/****************************************************************************************
	* Home::get_CategoriesRecords_NewRecordDefValues
	****************************************************************************************/
	function get_CategoriesRecords_NewRecordDefValues()
	{
		$params = array();
		
		$params["CtgID"] = $_REQUEST["CtgID"];
		
		$results = DB_sys_categories_records::sys_categories_records_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertCategoriesRecordsRecord
	****************************************************************************************/
	function InsertCategoriesRecordsRecord()
	{
		$params = array();
		
		$params["CtgRecID"] = $_REQUEST["CtgRecID"];
		$params["CtgRecCtgID"] = $_REQUEST["CtgRecCtgID"];
		$params["CtgRecPriority"] = $_REQUEST["CtgRecPriority"];
		$params["CtgRecTitle"] = $_REQUEST["CtgRecTitle"];
		$params["CtgRecTimer"] = $_REQUEST["CtgRecTimer"];
		$params["CtgRecStatus"] = $_REQUEST["CtgRecStatus"];
		if(isset($_REQUEST['CtgRecStartDate'])) { $params['CtgRecStartDate'] = $_REQUEST['CtgRecStartDate']; }
		if(isset($_REQUEST['CtgRecStartTime'])) { $params['CtgRecStartTime'] = $_REQUEST['CtgRecStartTime']; }
		if(isset($_REQUEST['CtgRecEndDate'])) { $params['CtgRecEndDate'] = $_REQUEST['CtgRecEndDate']; }
		if(isset($_REQUEST['CtgRecEndTime'])) { $params['CtgRecEndTime'] = $_REQUEST['CtgRecEndTime']; }
		$params["CtgRecEscapeDefault"] = $_REQUEST["CtgRecEscapeDefault"];
		
		
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["CtgRecFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_categories_records', $params["CtgRecID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$mdnewsize = 'mdnew-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$mdnewsize");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}	
		
		$result = DB_sys_categories_records::sys_categories_records_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_CategoriesRecordsRecord
	****************************************************************************************/
	function get_CategoriesRecordsRecord()
	{
		$params["CtgRecID"] = $_REQUEST["CtgRecID"];
			
		$results = DB_sys_categories_records::sys_categories_records_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateCategoriesRecordsRecord
	****************************************************************************************/
	function UpdateCategoriesRecordsRecord()
	{
		$params = array();

		if(isset($_REQUEST['CtgRecID'])) { $params['CtgRecID'] = $_REQUEST['CtgRecID']; }
		if(isset($_REQUEST['CtgRecCtgID'])) { $params['CtgRecCtgID'] = $_REQUEST['CtgRecCtgID']; }
		if(isset($_REQUEST['CtgRecPriority'])) { $params['CtgRecPriority'] = $_REQUEST['CtgRecPriority']; }
		if(isset($_REQUEST['CtgRecTitle'])) { $params['CtgRecTitle'] = $_REQUEST['CtgRecTitle']; }
		if(isset($_REQUEST['CtgRecStartDate'])) { $params['CtgRecStartDate'] = $_REQUEST['CtgRecStartDate']; }
		if(isset($_REQUEST['CtgRecStartTime'])) { $params['CtgRecStartTime'] = $_REQUEST['CtgRecStartTime']; }
		if(isset($_REQUEST['CtgRecEndDate'])) { $params['CtgRecEndDate'] = $_REQUEST['CtgRecEndDate']; }
		if(isset($_REQUEST['CtgRecEndTime'])) { $params['CtgRecEndTime'] = $_REQUEST['CtgRecEndTime']; }
		if(isset($_REQUEST['CtgRecTimer'])) { $params['CtgRecTimer'] = $_REQUEST['CtgRecTimer']; }
		if(isset($_REQUEST['CtgRecStatus'])) { $params['CtgRecStatus'] = $_REQUEST['CtgRecStatus']; }
		if(isset($_REQUEST['CtgRecEscapeDefault'])) { $params['CtgRecEscapeDefault'] = $_REQUEST['CtgRecEscapeDefault']; }
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["CtgRecFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_categories_records', $params["CtgRecID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$mdnewsize = 'mdnew-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$mdnewsize");
			
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}
		
		$result = DB_sys_categories_records::sys_categories_records_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteCategoriesRecordsRecord
	****************************************************************************************/
	function DeleteCategoriesRecordsRecord()
	{
		$params = array();
		
		$params["CtgRecID"] = $_REQUEST["CtgRecID"];
		
		$results = DB_sys_categories_records::sys_categories_records_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getCategoriesRecordsList
	****************************************************************************************/
	function getCategoriesRecordsList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		$params['CtgID'] 	= isset($_REQUEST['CtgID']) ? $_REQUEST['CtgID'] : 0;
		$params['SliderID'] 	= isset($_REQUEST['SliderID']) ? $_REQUEST['SliderID'] : 0;
		
		$results = DB_sys_categories_records::sys_categories_records_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/*
		CATEGORIES RECORDS LNG
	*/
	
	/****************************************************************************************
	* Home::get_CategoriesRecordsLngRecord
	****************************************************************************************/
	function get_CategoriesRecordsLngRecord()
	{	
		//Check if Record Exists
		$params["CtgRecLngCtgRecID"] = $_REQUEST["CtgRecLngCtgRecID"];
		$params["CtgRecLngType"] = $_REQUEST["CtgRecLngType"];
		
		
		$CheckIfCategoriesRecordsLngExists = DB_sys_categories_recordslng::sys_categories_recordslng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfCategoriesRecordsLngExists == 0)
		{
			$CategoriesRecordsLngDefValues = DB_sys_categories_recordslng::sys_categories_recordslng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["CtgRecLngID"] = $CategoriesRecordsLngDefValues['data']['CtgRecLngID'];
			
			$result = DB_sys_categories_recordslng::sys_categories_recordslng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_categories_recordslng::sys_categories_recordslng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$CtgRecLngID = DB_sys_categories_recordslng::sys_categories_recordslng_GetCategoriesRecordsLngID($this->appFrw, $params);
			
			$params["CtgRecLngID"] = $CtgRecLngID; 
			
			$results = DB_sys_categories_recordslng::sys_categories_recordslng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdateCategoriesRecordsLngRecord
	// ****************************************************************************************/
	function UpdateCategoriesRecordsLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['CtgRecLngID'])) { $params['CtgRecLngID'] = $_REQUEST['CtgRecLngID']; }
		if(isset($_REQUEST['CtgRecLngType'])) { $params['CtgRecLngType'] = $_REQUEST['CtgRecLngType']; }
		if(isset($_REQUEST['CtgRecLngCtgRecID'])) { $params['CtgRecLngCtgRecID'] = $_REQUEST['CtgRecLngCtgRecID']; }
		if(isset($_REQUEST['CtgRecLngTitle'])) { $params['CtgRecLngTitle'] = $_REQUEST['CtgRecLngTitle']; }
		if(isset($_REQUEST['CtgRecLngSubtitle'])) { $params['CtgRecLngSubtitle'] = $_REQUEST['CtgRecLngSubtitle']; }
		if(isset($_REQUEST['CtgRecLngDictionaryDescription'])) { $params['CtgRecLngDictionaryDescription'] = $_REQUEST['CtgRecLngDictionaryDescription']; }
		if(isset($_REQUEST['CtgRecLngText'])) { $params['CtgRecLngText'] = $_REQUEST['CtgRecLngText']; }

		
		$results = DB_sys_categories_recordslng::sys_categories_recordslng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		Footer
	*/
	
	/****************************************************************************************
	* Home::get_Footer_NewRecordDefValues
	****************************************************************************************/
	function get_Footer_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_footer::sys_footer_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertFooterRecord
	****************************************************************************************/
	function InsertFooterRecord()
	{
		$params = array();
		
		$params["FtrID"] = $_REQUEST["FtrID"];
		$params["FtrSiteID"] = $_REQUEST["FtrSiteID"];
		$params["FtrStatus"] = $_REQUEST["FtrStatus"];
		$params["FtrPriority"] = $_REQUEST["FtrPriority"];
		$params["FtrType"] = $_REQUEST["FtrType"];
		$params["FtrTitle"] = $_REQUEST["FtrTitle"];
		
		$results = DB_sys_footer::sys_footer_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_FooterRecord
	****************************************************************************************/
	function get_FooterRecord()
	{
		$params["FtrID"] = $_REQUEST["FtrID"];
			
		$results = DB_sys_footer::sys_footer_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateFooterRecord
	****************************************************************************************/
	function UpdateFooterRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['FtrID'])) { $params['FtrID'] = $_REQUEST['FtrID']; }
		if(isset($_REQUEST['FtrStatus'])) { $params['FtrStatus'] = $_REQUEST['FtrStatus']; }
		if(isset($_REQUEST['FtrPriority'])) { $params['FtrPriority'] = $_REQUEST['FtrPriority']; }
		if(isset($_REQUEST['FtrType'])) { $params['FtrType'] = $_REQUEST['FtrType']; }
		if(isset($_REQUEST['FtrTitle'])) { $params['FtrTitle'] = $_REQUEST['FtrTitle']; }
		
		$results = DB_sys_footer::sys_footer_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteFooterRecord
	****************************************************************************************/
	function DeleteFooterRecord()
	{
		$params = array();
		
		$params["FtrID"] = $_REQUEST["FtrID"];
		
		$results = DB_sys_footer::sys_footer_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getFooterList
	****************************************************************************************/
	function getFooterList()
	{
		$params = array();
		
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_footer::sys_footer_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/*
		FOOTER LNG
	*/
	
	/****************************************************************************************
	* Home::get_FtrLngRecord
	****************************************************************************************/
	function get_FtrLngRecord()
	{	
		//Check if Record Exists
		$params["FtrLngFtrID"] = $_REQUEST["FtrLngFtrID"];
		$params["FtrLngType"] = $_REQUEST["FtrLngType"];
		
		
		$CheckIfFooterLngExists = DB_sys_footerlng::sys_sys_footerlng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfFooterLngExists == 0)
		{
			$FooterLngDefValues = DB_sys_footerlng::sys_footerlng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["FtrLngID"] = $FooterLngDefValues['data']['FtrLngID'];
			
			$result = DB_sys_footerlng::sys_footerlng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_footerlng::sys_footerlng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$FtrLngID = DB_sys_footerlng::sys_footerlng_GetFooterLngID($this->appFrw, $params);
			
			$params["FtrLngID"] = $FtrLngID; 
			
			$results = DB_sys_footerlng::sys_footerlng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdateFtrLngRecord
	// ****************************************************************************************/
	function UpdateFtrLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['FtrLngID'])) { $params['FtrLngID'] = $_REQUEST['FtrLngID']; }
		if(isset($_REQUEST['FtrLngType'])) { $params['FtrLngType'] = $_REQUEST['FtrLngType']; }
		if(isset($_REQUEST['FtrLngFtrID'])) { $params['FtrLngFtrID'] = $_REQUEST['FtrLngFtrID']; }
		if(isset($_REQUEST['FtrLngTitle'])) { $params['FtrLngTitle'] = $_REQUEST['FtrLngTitle']; }
			
		$results = DB_sys_footerlng::sys_footerlng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/*
		Footer Data
	*/
	
	/****************************************************************************************
	* Home::get_FooterData_NewRecordDefValues
	****************************************************************************************/
	function get_FooterData_NewRecordDefValues()
	{
		$params = array();
		
		$params["FtrID"] = $_REQUEST["FtrID"];
		
		$results = DB_sys_footer_data::sys_footer_data_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertFooterDataRecord
	****************************************************************************************/
	function InsertFooterDataRecord()
	{
		$params = array();
		
		$params["FtrDataID"] = $_REQUEST["FtrDataID"];
		$params["FtrDataFtrID"] = $_REQUEST["FtrDataFtrID"];
		$params["FtrDataStatus"] = $_REQUEST["FtrDataStatus"];
		$params["FtrDataPriority"] = $_REQUEST["FtrDataPriority"];
		$params["FtrDataTitle"] = $_REQUEST["FtrDataTitle"];
		$params["FtrDataNodeID"] = $_REQUEST["FtrDataNodeID"];
		// $params["FtrDataExternalLink"] = $_REQUEST["FtrDataExternalLink"];
		$params["FtrDataIcon"] = $_REQUEST["FtrDataIcon"];
		
		$results = DB_sys_footer_data::sys_footer_data_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_FooterDataRecord
	****************************************************************************************/
	function get_FooterDataRecord()
	{
		$params["FtrDataID"] = $_REQUEST["FtrDataID"];
			
		$results = DB_sys_footer_data::sys_footer_data_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateFooterDataRecord
	****************************************************************************************/
	function UpdateFooterDataRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['FtrDataID'])) { $params['FtrDataID'] = $_REQUEST['FtrDataID']; }
		if(isset($_REQUEST['FtrDataStatus'])) { $params['FtrDataStatus'] = $_REQUEST['FtrDataStatus']; }
		if(isset($_REQUEST['FtrDataPriority'])) { $params['FtrDataPriority'] = $_REQUEST['FtrDataPriority']; }
		if(isset($_REQUEST['FtrDataTitle'])) { $params['FtrDataTitle'] = $_REQUEST['FtrDataTitle']; }
		if(isset($_REQUEST['FtrDataNodeID'])) { $params['FtrDataNodeID'] = $_REQUEST['FtrDataNodeID']; }
		// if(isset($_REQUEST['FtrDataExternalLink'])) { $params['FtrDataExternalLink'] = $_REQUEST['FtrDataExternalLink']; }
		if(isset($_REQUEST['FtrDataIcon'])) { $params['FtrDataIcon'] = $_REQUEST['FtrDataIcon']; }
		
		$results = DB_sys_footer_data::sys_footer_data_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteFooterDataRecord
	****************************************************************************************/
	function DeleteFooterDataRecord()
	{
		$params = array();
		
		$params["FtrDataID"] = $_REQUEST["FtrDataID"];
		
		$results = DB_sys_footer_data::sys_footer_data_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getFooterDataList
	****************************************************************************************/
	function getFooterDataList()
	{
		$params = array();
		
		$params['FtrID'] = isset($_REQUEST['FtrID']) ? $_REQUEST['FtrID'] : 0;
		$params['filterStrToFind'] = isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['filterShowAll'] = isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_footer_data::sys_footer_data_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}	
	
		/*
		FOOTER DATA LNG
	*/
	
	/****************************************************************************************
	* Home::get_FtrDataLngRecord
	****************************************************************************************/
	function get_FtrDataLngRecord()
	{	
		//Check if Record Exists
		$params["FtrDataLngFtrDataID"] = $_REQUEST["FtrDataLngFtrDataID"];
		$params["FtrDataLngType"] = $_REQUEST["FtrDataLngType"];
		
		
		$CheckIfFooterDataLngExists = DB_sys_footer_datalng::sys_footer_datalng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfFooterDataLngExists == 0)
		{
			$FooterDataLngDefValues = DB_sys_footer_datalng::sys_footer_datalng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["FtrDataLngID"] = $FooterDataLngDefValues['data']['FtrDataLngID'];
			
			$result = DB_sys_footer_datalng::sys_footer_datalng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_footer_datalng::sys_footer_datalng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$FtrDataLngID = DB_sys_footer_datalng::sys_footer_datalng_GetFooterDataLngID($this->appFrw, $params);
			
			$params["FtrDataLngID"] = $FtrDataLngID; 
			
			$results = DB_sys_footer_datalng::sys_footer_datalng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdateFtrDataLngRecord
	// ****************************************************************************************/
	function UpdateFtrDataLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['FtrDataLngID'])) { $params['FtrDataLngID'] = $_REQUEST['FtrDataLngID']; }
		if(isset($_REQUEST['FtrDataLngType'])) { $params['FtrDataLngType'] = $_REQUEST['FtrDataLngType']; }
		if(isset($_REQUEST['FtrDataLngFtrDataID'])) { $params['FtrDataLngFtrDataID'] = $_REQUEST['FtrDataLngFtrDataID']; }
		if(isset($_REQUEST['FtrDataLngTitle'])) { $params['FtrDataLngTitle'] = $_REQUEST['FtrDataLngTitle']; }
		if(isset($_REQUEST['FtrDataLngExternalLink'])) { $params['FtrDataLngExternalLink'] = $_REQUEST['FtrDataLngExternalLink']; }
			
		$results = DB_sys_footer_datalng::sys_footer_datalng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

    /*
        FOOTER TIMETABLE
    */

    /****************************************************************************************
     * Home::get_FooterTimetable_NewRecordDefValues
     ****************************************************************************************/
    function get_FooterTimetable_NewRecordDefValues()
    {
        $params = array();

        $params["SiteID"] = $_REQUEST["SiteID"];

        $results = DB_sys_footer_timetable::get_NewRecordDefValues($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::InsertFooterTimetableRecord
     ****************************************************************************************/
    function InsertFooterTimetableRecord()
    {
        $params = array();

        $params["FooterTimetableID"] = $_REQUEST["FooterTimetableID"];
        $params["FooterTimetableSiteID"] = $_REQUEST["FooterTimetableSiteID"];
        $params["FooterTimetableStatus"] = $_REQUEST["FooterTimetableStatus"];
        $params["FooterTimetableTitle"] = $_REQUEST["FooterTimetableTitle"];

        $results = DB_sys_footer_timetable::InsertRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::get_FooterTimetableRecord
     ****************************************************************************************/
    function get_FooterTimetableRecord()
    {
        $params["FooterTimetableID"] = $_REQUEST["FooterTimetableID"];

        $results = DB_sys_footer_timetable::getRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::UpdateFooterTimetableRecord
     ****************************************************************************************/
    function UpdateFooterTimetableRecord()
    {
        $params = array();

        if(isset($_REQUEST['FooterTimetableID'])) { $params['FooterTimetableID'] = $_REQUEST['FooterTimetableID']; }
        if(isset($_REQUEST['FooterTimetableSiteID'])) { $params['FooterTimetableSiteID'] = $_REQUEST['FooterTimetableSiteID']; }
        if(isset($_REQUEST['FooterTimetableStatus'])) { $params['FooterTimetableStatus'] = $_REQUEST['FooterTimetableStatus']; }
        if(isset($_REQUEST['FooterTimetableTitle'])) { $params['FooterTimetableTitle'] = $_REQUEST['FooterTimetableTitle']; }

        $results = DB_sys_footer_timetable::UpdateRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::DeleteFooterTimetableRecord
     ****************************************************************************************/
    function DeleteFooterTimetableRecord()
    {
        $params = array();

        $params["FooterTimetableID"] = $_REQUEST["FooterTimetableID"];

        $results = DB_sys_footer_timetable::DeleteRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::getFooterTimetableList
     ****************************************************************************************/
    function getFooterTimetableList()
    {
        $params = array();

        $params["maxRecords"]	= $_REQUEST["maxRecords"];
        $params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;


        $results = DB_sys_footer_timetable::getList($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /*
        FOOTER TIMETABLE
    */

    /****************************************************************************************
     * Home::get_AlertLngRecord
     ****************************************************************************************/
    function get_FooterTimetableLngRecord()
    {
        //Check if Record Exists
        $params["FooterTimetableLngIDFooterTimetableID"] = $_REQUEST["FooterTimetableLngIDFooterTimetableID"];
        $params["FooterTimetableLngType"] = $_REQUEST["FooterTimetableLngType"];


        $CheckIfLngExists = DB_sys_footer_timetablelng::CheckBeforeInsert($this->appFrw, $params);

        if ($CheckIfLngExists == 0)
        {
            // // Insert NodeLng
            $LngDefValues = DB_sys_footer_timetablelng::get_NewRecordDefValues($this->appFrw, $params);

            $params["FooterTimetableLngID"] = $LngDefValues['data']['FooterTimetableLngID'];

            $result = DB_sys_footer_timetablelng::InsertRecord($this->appFrw, $params);

            if($result > 0)
            {
                $results = DB_sys_footer_timetablelng::getRecord($this->appFrw, $params);

                if ($results['success']==true)
                    return json_encode((array('data' => $results["data"])));
                else
                    return json_encode((array('error' => $results["reason"])));
            }

        }
        else
        {
            $LngID = DB_sys_footer_timetablelng::GetLngID($this->appFrw, $params);

            $params["FooterTimetableLngID"] = $LngID;

            $results = DB_sys_footer_timetablelng::getRecord($this->appFrw, $params);

            if ($results['success']==true)
                return json_encode((array('data' => $results["data"])));
            else
                return json_encode((array('error' => $results["reason"])));
        }
    }

    /****************************************************************************************
     * Home::UpdateFooterTimetableLngRecord
     ****************************************************************************************/
    function UpdateFooterTimetableLngRecord()
    {
        $params = array();

        if(isset($_REQUEST['FooterTimetableLngID'])) { $params['FooterTimetableLngID'] = $_REQUEST['FooterTimetableLngID']; }
        if(isset($_REQUEST['FooterTimetableLngType'])) { $params['FooterTimetableLngType'] = $_REQUEST['FooterTimetableLngType']; }
        if(isset($_REQUEST['FooterTimetableLngIDFooterTimetableID'])) { $params['FooterTimetableLngIDFooterTimetableID'] = $_REQUEST['FooterTimetableLngIDFooterTimetableID']; }
        if(isset($_REQUEST['FooterTimetableLngTitle'])) { $params['FooterTimetableLngTitle'] = $_REQUEST['FooterTimetableLngTitle']; }
        if(isset($_REQUEST['FooterTimetableLngContent'])) { $params['FooterTimetableLngContent'] = $_REQUEST['FooterTimetableLngContent']; }

        $results = DB_sys_footer_timetablelng::UpdateRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

	/*
		IMAGE GALLERY
	*/
	
	/****************************************************************************************
	* Home::get_ImageGallery_NewRecordDefValues
	****************************************************************************************/
	function get_ImageGallery_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_image_gallery::sys_image_gallery_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertImageGalleryRecord
	****************************************************************************************/
	function InsertImageGalleryRecord()
	{
		$params = array();
		
		$params["ImgID"] = $_REQUEST["ImgID"];
		$params["ImgSiteID"] = $_REQUEST["ImgSiteID"];
		$params["ImgTitle"] = $_REQUEST["ImgTitle"];
		$params["ImgPriority"] = $_REQUEST["ImgPriority"];
		$params["ImgStatus"] = $_REQUEST["ImgStatus"];	
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["ImgFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_image_gallery', $params["ImgID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		$result = DB_sys_image_gallery::sys_image_gallery_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_ImageGalleryRecord
	****************************************************************************************/
	function get_ImageGalleryRecord()
	{
		$params["ImgID"] = $_REQUEST["ImgID"];
			
		$results = DB_sys_image_gallery::sys_image_gallery_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateImageGalleryRecord
	****************************************************************************************/
	function UpdateImageGalleryRecord()
	{
		$params = array();

		if(isset($_REQUEST['ImgID'])) { $params['ImgID'] = $_REQUEST['ImgID']; }
		if(isset($_REQUEST['ImgSiteID'])) { $params['ImgSiteID'] = $_REQUEST['ImgSiteID']; }
		if(isset($_REQUEST['ImgStatus'])) { $params['ImgStatus'] = $_REQUEST['ImgStatus']; }
		if(isset($_REQUEST['ImgPriority'])) { $params['ImgPriority'] = $_REQUEST['ImgPriority']; }
		if(isset($_REQUEST['ImgTitle'])) { $params['ImgTitle'] = $_REQUEST['ImgTitle']; }
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["ImgFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_image_gallery', $params["ImgID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		$result = DB_sys_image_gallery::sys_image_gallery_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteImageGalleryRecord
	****************************************************************************************/
	function DeleteImageGalleryRecord()
	{
		$params = array();
		
		$params["ImgID"] = $_REQUEST["ImgID"];
		
		$results = DB_sys_image_gallery::sys_image_gallery_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getImageGalleryList
	****************************************************************************************/
	function getImageGalleryList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_image_gallery::sys_image_gallery_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		IMAGE GALLERY LNG
	*/
	
	/****************************************************************************************
	* Home::get_ImageGalleryLngRecord
	****************************************************************************************/
	function get_ImageGalleryLngRecord()
	{	
		//Check if Record Exists
		$params["ImgLngImgID"] = $_REQUEST["ImgLngImgID"];
		$params["ImgLngType"] = $_REQUEST["ImgLngType"];
		
		
		$CheckIfImageGalleryLngExists = DB_sys_image_gallerylng::sys_image_gallerylng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfImageGalleryLngExists == 0)
		{
			$ImageGalleryLngDefValues = DB_sys_image_gallerylng::sys_image_gallerylng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["ImgLngID"] = $ImageGalleryLngDefValues['data']['ImgLngID'];
			
			$result = DB_sys_image_gallerylng::sys_image_gallerylng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_image_gallerylng::sys_image_gallerylng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$ImgLngID = DB_sys_image_gallerylng::sys_image_gallerylng_GetImgLngID($this->appFrw, $params);
			
			$params["ImgLngID"] = $ImgLngID; 
			
			$results = DB_sys_image_gallerylng::sys_image_gallerylng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdateImageGalleryLngRecord
	// ****************************************************************************************/
	function UpdateImageGalleryLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['ImgLngID'])) { $params['ImgLngID'] = $_REQUEST['ImgLngID']; }
		if(isset($_REQUEST['ImgLngType'])) { $params['ImgLngType'] = $_REQUEST['ImgLngType']; }
		if(isset($_REQUEST['ImgLngImgID'])) { $params['ImgLngImgID'] = $_REQUEST['ImgLngImgID']; }
		if(isset($_REQUEST['ImgLngTitle'])) { $params['ImgLngTitle'] = $_REQUEST['ImgLngTitle']; }
		if(isset($_REQUEST['ImgLngSubtitle'])) { $params['ImgLngSubtitle'] = $_REQUEST['ImgLngSubtitle']; }
		if(isset($_REQUEST['ImgLngDescription'])) { $params['ImgLngDescription'] = $_REQUEST['ImgLngDescription']; }
			
		$results = DB_sys_image_gallerylng::sys_image_gallerylng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/*
		IMAGE GALLERY DATA
	*/
	
	/****************************************************************************************
	* Home::get_ImageGalleryData_NewRecordDefValues
	****************************************************************************************/
	function get_ImageGalleryData_NewRecordDefValues()
	{
		$params = array();
		
		$params["ImgID"] = $_REQUEST["ImgID"];
		
		$results = DB_sys_image_gallery_data::sys_image_gallery_data_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertImageGalleryDataRecord
	****************************************************************************************/
	function InsertImageGalleryDataRecord()
	{
		$params = array();
		
		$params["ImgDataID"] = $_REQUEST["ImgDataID"];
		$params["ImgDataImgID"] = $_REQUEST["ImgDataImgID"];
		$params["ImgDataTitle"] = $_REQUEST["ImgDataTitle"];
		$params["ImgDataPriority"] = $_REQUEST["ImgDataPriority"];
		$params["ImgDataStatus"] = $_REQUEST["ImgDataStatus"];	
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["ImgDataFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_image_gallery_data', $params["ImgDataID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}	
		
		$result = DB_sys_image_gallery_data::sys_image_gallery_data_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_ImageGalleryDataRecord
	****************************************************************************************/
	function get_ImageGalleryDataRecord()
	{
		$params["ImgDataID"] = $_REQUEST["ImgDataID"];
			
		$results = DB_sys_image_gallery_data::sys_image_gallery_data_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateImageGalleryDataRecord
	****************************************************************************************/
	function UpdateImageGalleryDataRecord()
	{
		$params = array();

		if(isset($_REQUEST['ImgDataID'])) { $params['ImgDataID'] = $_REQUEST['ImgDataID']; }
		if(isset($_REQUEST['ImgDataImgID'])) { $params['ImgDataImgID'] = $_REQUEST['ImgDataImgID']; }
		if(isset($_REQUEST['ImgDataTitle'])) { $params['ImgDataTitle'] = $_REQUEST['ImgDataTitle']; }
		if(isset($_REQUEST['ImgDataPriority'])) { $params['ImgDataPriority'] = $_REQUEST['ImgDataPriority']; }
		if(isset($_REQUEST['ImgDataStatus'])) { $params['ImgDataStatus'] = $_REQUEST['ImgDataStatus']; }
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["ImgDataFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_image_gallery_data', $params["ImgDataID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		$result = DB_sys_image_gallery_data::sys_image_gallery_data_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteImageGalleryDataRecord
	****************************************************************************************/
	function DeleteImageGalleryDataRecord()
	{
		$params = array();
		
		$params["ImgDataID"] = $_REQUEST["ImgDataID"];
		
		$results = DB_sys_image_gallery_data::sys_image_gallery_data_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getImageGalleryDataList
	****************************************************************************************/
	function getImageGalleryDataList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['ImgID'] 	= isset($_REQUEST['ImgID']) ? $_REQUEST['ImgID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_image_gallery_data::sys_image_gallery_data_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/*
		IMAGE GALLERY DATA LNG
	*/
	
	/****************************************************************************************
	* Home::get_ImageGalleryDataLngRecord
	****************************************************************************************/
	function get_ImageGalleryDataLngRecord()
	{	
		//Check if Record Exists
		$params["ImgDataLngImgDataID"] = $_REQUEST["ImgDataLngImgDataID"];
		$params["ImgDataLngType"] = $_REQUEST["ImgDataLngType"];
		
		
		$CheckIfImageGalleryDataLngExists = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfImageGalleryDataLngExists == 0)
		{
			$ImageGalleryDataLngDefValues = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["ImgDataLngID"] = $ImageGalleryDataLngDefValues['data']['ImgDataLngID'];
			
			$result = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$ImgDataLngID = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_GetImgDataLngID($this->appFrw, $params);
			
			$params["ImgDataLngID"] = $ImgDataLngID; 
			
			$results = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdateImageGalleryDataLngRecord
	// ****************************************************************************************/
	function UpdateImageGalleryDataLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['ImgDataLngID'])) { $params['ImgDataLngID'] = $_REQUEST['ImgDataLngID']; }
		if(isset($_REQUEST['ImgDataLngImgDataID'])) { $params['ImgDataLngImgDataID'] = $_REQUEST['ImgDataLngImgDataID']; }
		if(isset($_REQUEST['ImgDataLngTitle'])) { $params['ImgDataLngTitle'] = $_REQUEST['ImgDataLngTitle']; }
		if(isset($_REQUEST['ImgDataLngType'])) { $params['ImgDataLngType'] = $_REQUEST['ImgDataLngType']; }
		if(isset($_REQUEST['ImgDataLngSubtitle'])) { $params['ImgDataLngSubtitle'] = $_REQUEST['ImgDataLngSubtitle']; }
		if(isset($_REQUEST['ImgDataLngDescription'])) { $params['ImgDataLngDescription'] = $_REQUEST['ImgDataLngDescription']; }
			
		$results = DB_sys_image_gallery_datalng::sys_image_gallery_datalng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}	
	
	/*
		VIDEO GALLERY
	*/
	
	/****************************************************************************************
	* Home::get_VideoGallery_NewRecordDefValues
	****************************************************************************************/
	function get_VideoGallery_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_video_gallery::sys_video_gallery_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertVideoGalleryRecord
	****************************************************************************************/
	function InsertVideoGalleryRecord()
	{
		$params = array();
		
		$params["VidID"] = $_REQUEST["VidID"];
		$params["VidSiteID"] = $_REQUEST["VidSiteID"];
		$params["VidTitle"] = $_REQUEST["VidTitle"];
		$params["VidPriority"] = $_REQUEST["VidPriority"];
		$params["VidStatus"] = $_REQUEST["VidStatus"];	
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["VidFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_video_gallery', $params["VidID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		$result = DB_sys_video_gallery::sys_video_gallery_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_VideoGalleryRecord
	****************************************************************************************/
	function get_VideoGalleryRecord()
	{
		$params["VidID"] = $_REQUEST["VidID"];
			
		$results = DB_sys_video_gallery::sys_video_gallery_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateVideoGalleryRecord
	****************************************************************************************/
	function UpdateVideoGalleryRecord()
	{
		$params = array();

		if(isset($_REQUEST['VidID'])) { $params['VidID'] = $_REQUEST['VidID']; }
		if(isset($_REQUEST['VidSiteID'])) { $params['VidSiteID'] = $_REQUEST['VidSiteID']; }
		if(isset($_REQUEST['VidTitle'])) { $params['VidTitle'] = $_REQUEST['VidTitle']; }
		if(isset($_REQUEST['VidPriority'])) { $params['VidPriority'] = $_REQUEST['VidPriority']; }
		if(isset($_REQUEST['VidStatus'])) { $params['VidStatus'] = $_REQUEST['VidStatus']; }
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["VidFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_video_gallery', $params["VidID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		$result = DB_sys_video_gallery::sys_video_gallery_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteVideoGalleryRecord
	****************************************************************************************/
	function DeleteVideoGalleryRecord()
	{
		$params = array();
		
		$params["VidID"] = $_REQUEST["VidID"];
		
		$results = DB_sys_video_gallery::sys_video_gallery_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getVideoGalleryList
	****************************************************************************************/
	function getVideoGalleryList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_video_gallery::sys_video_gallery_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/*
		VIDEO GALLERY LNG
	*/
	
	/****************************************************************************************
	* Home::get_VideoGalleryLngRecord
	****************************************************************************************/
	function get_VideoGalleryLngRecord()
	{	
		//Check if Record Exists
		$params["VidLngVidID"] = $_REQUEST["VidLngVidID"];
		$params["VidLngType"] = $_REQUEST["VidLngType"];
		
		
		$CheckIfVideoGalleryLngExists = DB_sys_video_gallerylng::sys_video_gallerylng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfVideoGalleryLngExists == 0)
		{
			$VideoGalleryLngDefValues = DB_sys_video_gallerylng::sys_video_gallerylng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["VidLngID"] = $VideoGalleryLngDefValues['data']['VidLngID'];
			
			$result = DB_sys_video_gallerylng::sys_video_gallerylng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_video_gallerylng::sys_video_gallerylng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$VidLngID = DB_sys_video_gallerylng::sys_video_gallerylng_GetVidLngID($this->appFrw, $params);
			
			$params["VidLngID"] = $VidLngID; 
			
			$results = DB_sys_video_gallerylng::sys_video_gallerylng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdateVideoGalleryLngRecord
	// ****************************************************************************************/
	function UpdateVideoGalleryLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['VidLngID'])) { $params['VidLngID'] = $_REQUEST['VidLngID']; }
		if(isset($_REQUEST['VidLngType'])) { $params['VidLngType'] = $_REQUEST['VidLngType']; }
		if(isset($_REQUEST['VidLngVidID'])) { $params['VidLngVidID'] = $_REQUEST['VidLngVidID']; }
		if(isset($_REQUEST['VidLngTitle'])) { $params['VidLngTitle'] = $_REQUEST['VidLngTitle']; }
		if(isset($_REQUEST['VidLngSubtitle'])) { $params['VidLngSubtitle'] = $_REQUEST['VidLngSubtitle']; }
		if(isset($_REQUEST['VidLngDescription'])) { $params['VidLngDescription'] = $_REQUEST['VidLngDescription']; }
			
		$results = DB_sys_video_gallerylng::sys_video_gallerylng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}	
	
	/*
		VIDEO GALLERY DATA
	*/
	
	/****************************************************************************************
	* Home::get_VideoGalleryData_NewRecordDefValues
	****************************************************************************************/
	function get_VideoGalleryData_NewRecordDefValues()
	{
		$params = array();
		
		$params["VidID"] = $_REQUEST["VidID"];
		
		$results = DB_sys_video_gallery_data::sys_video_gallery_data_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertVideoGalleryDataRecord
	****************************************************************************************/
	function InsertVideoGalleryDataRecord()
	{
		$params = array();
		
		$params["VidDataID"] = $_REQUEST["VidDataID"];
		$params["VidDataVidID"] = $_REQUEST["VidDataVidID"];
		$params["VidDataTitle"] = $_REQUEST["VidDataTitle"];
		$params["VidDataPriority"] = $_REQUEST["VidDataPriority"];
		$params["VidDataStatus"] = $_REQUEST["VidDataStatus"];	
		
		// Get Youtube Code
			parse_str(parse_url($_REQUEST["VidDataFilename"], PHP_URL_QUERY), $ID_youtube);
			$MediaUrl = $ID_youtube['v'];
			
			// Get Still Image
			//$url   = 'http://img.youtube.com/vi/'.$CvdUrl.'/maxresdefault.jpg';
			
			$params["VidDataFilename"] = $MediaUrl;
		
		$result = DB_sys_video_gallery_data::sys_video_gallery_data_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_VideoGalleryDataRecord
	****************************************************************************************/
	function get_VideoGalleryDataRecord()
	{
		$params["VidDataID"] = $_REQUEST["VidDataID"];
			
		$results = DB_sys_video_gallery_data::sys_video_gallery_data_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateVideoGalleryDataRecord
	****************************************************************************************/
	function UpdateVideoGalleryDataRecord()
	{
		$params = array();

		if(isset($_REQUEST['VidDataID'])) { $params['VidDataID'] = $_REQUEST['VidDataID']; }
		if(isset($_REQUEST['VidDataVidID'])) { $params['VidDataVidID'] = $_REQUEST['VidDataVidID']; }
		if(isset($_REQUEST['VidDataTitle'])) { $params['VidDataTitle'] = $_REQUEST['VidDataTitle']; }
		if(isset($_REQUEST['VidDataPriority'])) { $params['VidDataPriority'] = $_REQUEST['VidDataPriority']; }
		if(isset($_REQUEST['VidDataStatus'])) { $params['VidDataStatus'] = $_REQUEST['VidDataStatus']; }
		
		if(isset($_REQUEST['VidDataFilename'])) 
		{
			// Get Youtube Code
			parse_str(parse_url($_REQUEST["VidDataFilename"], PHP_URL_QUERY), $ID_youtube);
			$MediaUrl = $ID_youtube['v'];
			
			// Get Still Image
			//$url   = 'http://img.youtube.com/vi/'.$CvdUrl.'/maxresdefault.jpg';
			
			$params["VidDataFilename"] = $MediaUrl;
		}
		
		$result = DB_sys_video_gallery_data::sys_video_gallery_data_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteVideoGalleryDataRecord
	****************************************************************************************/
	function DeleteVideoGalleryDataRecord()
	{
		$params = array();
		
		$params["VidDataID"] = $_REQUEST["VidDataID"];
		
		$results = DB_sys_video_gallery_data::sys_video_gallery_data_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getVideoGalleryDataList
	****************************************************************************************/
	function getVideoGalleryDataList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['VidID'] 	= isset($_REQUEST['VidID']) ? $_REQUEST['VidID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_video_gallery_data::sys_video_gallery_data_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/*
		VIDEO GALLERY DATA LNG
	*/
	
	/****************************************************************************************
	* Home::get_VideoGalleryDataLngRecord
	****************************************************************************************/
	function get_VideoGalleryDataLngRecord()
	{	
		//Check if Record Exists
		$params["VidDataLngVidDataID"] = $_REQUEST["VidDataLngVidDataID"];
		$params["VidDataLngType"] = $_REQUEST["VidDataLngType"];
		
		
		$CheckIfVideoDataGalleryLngExists = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfVideoDataGalleryLngExists == 0)
		{
			$VideoDataGalleryLngDefValues = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["VidDataLngID"] = $VideoDataGalleryLngDefValues['data']['VidDataLngID'];
			
			$result = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$VidDataLngID = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_GetVidDataLngID($this->appFrw, $params);
			
			$params["VidDataLngID"] = $VidDataLngID; 
			
			$results = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdateVideoGalleryDataLngRecord
	// ****************************************************************************************/
	function UpdateVideoGalleryDataLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['VidDataLngID'])) { $params['VidDataLngID'] = $_REQUEST['VidDataLngID']; }
		if(isset($_REQUEST['VidDataLngType'])) { $params['VidDataLngType'] = $_REQUEST['VidDataLngType']; }
		if(isset($_REQUEST['VidDataLngVidDataID'])) { $params['VidDataLngVidDataID'] = $_REQUEST['VidDataLngVidDataID']; }
		if(isset($_REQUEST['VidDataLngTitle'])) { $params['VidDataLngTitle'] = $_REQUEST['VidDataLngTitle']; }
		if(isset($_REQUEST['VidDataLngSubtitle'])) { $params['VidDataLngSubtitle'] = $_REQUEST['VidDataLngSubtitle']; }
		if(isset($_REQUEST['VidDataLngDescription'])) { $params['VidDataLngDescription'] = $_REQUEST['VidDataLngDescription']; }
			
		
		$results = DB_sys_video_gallery_datalng::sys_video_gallery_datalng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}	
	
	/*
		PUBLICATIONS GALLERY
	*/
	
	/****************************************************************************************
	* Home::get_PublicationsGallery_NewRecordDefValues
	****************************************************************************************/
	function get_PublicationsGallery_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_publications_gallery::sys_publications_gallery_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertPublicationsGalleryRecord
	****************************************************************************************/
	function InsertPublicationsGalleryRecord()
	{
		$params = array();
		
		$params["PubID"] = $_REQUEST["PubID"];
		$params["PubSiteID"] = $_REQUEST["PubSiteID"];
		$params["PubTitle"] = $_REQUEST["PubTitle"];
		$params["PubPriority"] = $_REQUEST["PubPriority"];
		$params["PubStatus"] = $_REQUEST["PubStatus"];	
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["PubFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_publications_gallery', $params["PubID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		$result = DB_sys_publications_gallery::sys_publications_gallery_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_PublicationsGalleryRecord
	****************************************************************************************/
	function get_PublicationsGalleryRecord()
	{
		$params["PubID"] = $_REQUEST["PubID"];
			
		$results = DB_sys_publications_gallery::sys_publications_gallery_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdatePublicationsGalleryRecord
	****************************************************************************************/
	function UpdatePublicationsGalleryRecord()
	{
		$params = array();

		if(isset($_REQUEST['PubID'])) { $params['PubID'] = $_REQUEST['PubID']; }
		if(isset($_REQUEST['PubSiteID'])) { $params['PubSiteID'] = $_REQUEST['PubSiteID']; }
		if(isset($_REQUEST['PubTitle'])) { $params['PubTitle'] = $_REQUEST['PubTitle']; }
		if(isset($_REQUEST['PubPriority'])) { $params['PubPriority'] = $_REQUEST['PubPriority']; }
		if(isset($_REQUEST['PubStatus'])) { $params['PubStatus'] = $_REQUEST['PubStatus']; }
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["PubFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_publications_gallery', $params["PubID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		
		}	
		
		$result = DB_sys_publications_gallery::sys_publications_gallery_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeletePublicationsGalleryRecord
	****************************************************************************************/
	function DeletePublicationsGalleryRecord()
	{
		$params = array();
		
		$params["PubID"] = $_REQUEST["PubID"];
		
		$results = DB_sys_publications_gallery::sys_publications_gallery_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getPublicationsGalleryList
	****************************************************************************************/
	function getPublicationsGalleryList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_publications_gallery::sys_publications_gallery_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		PUBLICATIONS GALLERY  LNG
	*/
	
	/****************************************************************************************
	* Home::get_PublicationsGalleryLngRecord
	****************************************************************************************/
	function get_PublicationsGalleryLngRecord()
	{	
		//Check if Record Exists
		$params["PubLngPubID"] = $_REQUEST["PubLngPubID"];
		$params["PubLngType"] = $_REQUEST["PubLngType"];
		
		
		$CheckIfPublicationsGalleryLngExists = DB_sys_publications_gallerylng::sys_publications_gallerylng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfPublicationsGalleryLngExists == 0)
		{
			$PublicationsGalleryLngDefValues = DB_sys_publications_gallerylng::sys_publications_gallerylng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["PubLngID"] = $PublicationsGalleryLngDefValues['data']['PubLngID'];
			
			$result = DB_sys_publications_gallerylng::sys_publications_gallerylng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_publications_gallerylng::sys_publications_gallerylng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$PubLngID = DB_sys_publications_gallerylng::sys_publications_gallerylng_GetPubLngID($this->appFrw, $params);
			
			$params["PubLngID"] = $PubLngID; 
			
			$results = DB_sys_publications_gallerylng::sys_publications_gallerylng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdatePublicationsGalleryLngRecord
	// ****************************************************************************************/
	function UpdatePublicationsGalleryLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['PubLngID'])) { $params['PubLngID'] = $_REQUEST['PubLngID']; }
		if(isset($_REQUEST['PubLngType'])) { $params['PubLngType'] = $_REQUEST['PubLngType']; }
		if(isset($_REQUEST['PubLngPubID'])) { $params['PubLngPubID'] = $_REQUEST['PubLngPubID']; }
		if(isset($_REQUEST['PubLngTitle'])) { $params['PubLngTitle'] = $_REQUEST['PubLngTitle']; }
		if(isset($_REQUEST['PubLngSubtitle'])) { $params['PubLngSubtitle'] = $_REQUEST['PubLngSubtitle']; }
		if(isset($_REQUEST['PubLngDescription'])) { $params['PubLngDescription'] = $_REQUEST['PubLngDescription']; }
			
		$results = DB_sys_publications_gallerylng::sys_publications_gallerylng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}	
	
	/*
		PUBLICATIONS GALLERY DATA
	*/
	
	/****************************************************************************************
	* Home::get_PublicationsGalleryData_NewRecordDefValues
	****************************************************************************************/
	function get_PublicationsGalleryData_NewRecordDefValues()
	{
		$params = array();
		
		$params["PubID"] = $_REQUEST["PubID"];
		
		$results = DB_sys_publications_gallery_data::sys_publications_gallery_data_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* Home::InsertPublicationsGalleryDataRecord
	****************************************************************************************/
	function InsertPublicationsGalleryDataRecord()
	{
		$params = array();
		
		$params["PubDataID"] = $_REQUEST["PubDataID"];
		$params["PubDataPubID"] = $_REQUEST["PubDataPubID"];
		$params["PubDataTitle"] = $_REQUEST["PubDataTitle"];
		$params["PubDataPriority"] = $_REQUEST["PubDataPriority"];
		$params["PubDataStatus"] = $_REQUEST["PubDataStatus"];	
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["PubDataFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_publications_gallery_data', $params["PubDataID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			$file_dimensions = getimagesize($tmp_uploaded_file);
			$file_size = filesize($tmp_uploaded_file);
			$file_size = $file_size / '1048576';
			$file_size = number_format($file_size, 2, '.', '');
			$extension = pathinfo($uploaded_filename, PATHINFO_EXTENSION);

			$params['PubDataFilesize'] 	 = $file_size . ' MB';
			$params['PubDataExtension'] = $extension;
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
		}		
		
		$result = DB_sys_publications_gallery_data::sys_publications_gallery_data_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* Home::get_PublicationsGalleryDataRecord
	****************************************************************************************/
	function get_PublicationsGalleryDataRecord()
	{
		$params["PubDataID"] = $_REQUEST["PubDataID"];
			
		$results = DB_sys_publications_gallery_data::sys_publications_gallery_data_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdatePublicationsGalleryDataRecord
	****************************************************************************************/
	function UpdatePublicationsGalleryDataRecord()
	{
		$params = array();

		if(isset($_REQUEST['PubDataID'])) { $params['PubDataID'] = $_REQUEST['PubDataID']; }
		if(isset($_REQUEST['PubDataPubID'])) { $params['PubDataPubID'] = $_REQUEST['PubDataPubID']; }
		if(isset($_REQUEST['PubDataTitle'])) { $params['PubDataTitle'] = $_REQUEST['PubDataTitle']; }
		if(isset($_REQUEST['PubDataPriority'])) { $params['PubDataPriority'] = $_REQUEST['PubDataPriority']; }
		if(isset($_REQUEST['PubDataStatus'])) { $params['PubDataStatus'] = $_REQUEST['PubDataStatus']; }
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["PubDataFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_publications_gallery_data', $params["PubDataID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			$file_dimensions = getimagesize($tmp_uploaded_file);
			$file_size = filesize($tmp_uploaded_file);
			$file_size = $file_size / '1048576';
			$file_size = number_format($file_size, 2, '.', '');
			$extension = pathinfo($uploaded_filename, PATHINFO_EXTENSION);

			$params['PubDataFilesize'] 	 = $file_size . ' MB';
			$params['PubDataExtension'] = $extension;
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
		}	
		
		$result = DB_sys_publications_gallery_data::sys_publications_gallery_data_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeletePublicationsGalleryDataRecord
	****************************************************************************************/
	function DeletePublicationsGalleryDataRecord()
	{
		$params = array();
		
		$params["PubDataID"] = $_REQUEST["PubDataID"];
		
		$results = DB_sys_publications_gallery_data::sys_publications_gallery_data_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::getPublicationsGalleryDataList
	****************************************************************************************/
	function getPublicationsGalleryDataList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['PubID'] 	= isset($_REQUEST['PubID']) ? $_REQUEST['PubID'] : 0;
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		
		$results = DB_sys_publications_gallery_data::sys_publications_gallery_data_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

		/*
		PUBLICATIONS GALLERY  LNG
	*/
	
	/****************************************************************************************
	* Home::get_PublicationsGalleryDataLngRecord
	****************************************************************************************/
	function get_PublicationsGalleryDataLngRecord()
	{	
		//Check if Record Exists
		$params["PubDataLngPubDataID"] = $_REQUEST["PubDataLngPubDataID"];
		$params["PubDataLngType"] = $_REQUEST["PubDataLngType"];
		
		
		$CheckIfPublicationsGalleryDataLngExists = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfPublicationsGalleryDataLngExists == 0)
		{
			$PublicationsGalleryDataLngDefValues = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["PubDataLngID"] = $PublicationsGalleryDataLngDefValues['data']['PubDataLngID'];
			
			$result = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$PubDataLngID = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_GetPubDataLngID($this->appFrw, $params);
			
			$params["PubDataLngID"] = $PubDataLngID; 
			
			$results = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdatePublicationsGalleryDataLngRecord
	// ****************************************************************************************/
	function UpdatePublicationsGalleryDataLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['PubDataLngID'])) { $params['PubDataLngID'] = $_REQUEST['PubDataLngID']; }
		if(isset($_REQUEST['PubDataLngType'])) { $params['PubDataLngType'] = $_REQUEST['PubDataLngType']; }
		if(isset($_REQUEST['PubDataLngPubDataID'])) { $params['PubDataLngPubDataID'] = $_REQUEST['PubDataLngPubDataID']; }
		if(isset($_REQUEST['PubDataLngTitle'])) { $params['PubDataLngTitle'] = $_REQUEST['PubDataLngTitle']; }
		if(isset($_REQUEST['PubDataLngSubtitle'])) { $params['PubDataLngSubtitle'] = $_REQUEST['PubDataLngSubtitle']; }
		if(isset($_REQUEST['PubDataLngDescription'])) { $params['PubDataLngDescription'] = $_REQUEST['PubDataLngDescription']; }
			
		$results = DB_sys_publications_gallery_datalng::sys_publications_gallery_datalng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

    /*
        Subjects
    */

    /****************************************************************************************
     * Home::get_Subject_NewRecordDefValues
     ****************************************************************************************/
    function get_Subject_NewRecordDefValues()
    {
        $params = array();

        $params["SiteID"] = $_REQUEST["SiteID"];

        $results = DB_cf_subjects::cf_subjects_get_NewRecordDefValues($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::InsertSubjectRecord
     ****************************************************************************************/
    function InsertSubjectRecord()
    {
        $params = array();

        $params["SubID"]        = $_REQUEST["SubID"];
        $params["SubSiteID"]    = $_REQUEST["SubSiteID"];
        $params["SubTitle"]     = $_REQUEST["SubTitle"];
        $params["SubPriority"]  = $_REQUEST["SubPriority"];
        $params["SubStatus"]    = $_REQUEST["SubStatus"];

        $results = DB_cf_subjects::cf_subjects_InsertRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }


    /****************************************************************************************
     * Home::get_SubjectRecord
     ****************************************************************************************/
    function get_SubjectRecord()
    {
        $params["SubID"] = $_REQUEST["SubID"];

        $results = DB_cf_subjects::cf_subjects_getRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::UpdateSubjectRecord
     ****************************************************************************************/
    function UpdateSubjectRecord()
    {
        $params = array();

        $params['SubID']        = isset($_REQUEST['SubID']) ? $_REQUEST['SubID'] : 0;
        $params['SubTitle']     = isset($_REQUEST['SubTitle']) ? $_REQUEST['SubTitle'] : "";
        $params['SubPriority']  = isset($_REQUEST['SubPriority']) ? $_REQUEST['SubPriority'] : 0;
        $params['SubStatus']    = isset($_REQUEST['SubStatus']) ? $_REQUEST['SubStatus'] : 0;

        $results = DB_cf_subjects::cf_subjects_UpdateRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::DeleteSubjectRecord
     ****************************************************************************************/
    function DeleteSubjectRecord()
    {
        $params = array();

        $params["SubID"] = $_REQUEST["SubID"];

        $results = DB_cf_subjects::cf_subjects_DeleteRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::getSubjectsList
     ****************************************************************************************/
    function getSubjectsList()
    {
        $params = array();

        $params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
        $params['SiteID'] 	        = isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
        $params['filterShowAll']    = isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;

        $results = DB_cf_subjects::cf_subjects_get_List($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /*
        Subject LNG
    */

    /****************************************************************************************
     * Home::get_SubjectLngRecord
     ****************************************************************************************/
    function get_SubjectLngRecord()
    {
        //Check if Record Exists
        $params["SubLngSubID"]  = $_REQUEST["SubLngSubID"];
        $params["SubLngType"]   = $_REQUEST["SubLngType"];


        $CheckIfSubjectLngExists = DB_cf_subjects_lang::cf_subjects_lang_CheckBeforeInsert($this->appFrw, $params);

        if ($CheckIfSubjectLngExists == 0)
        {
            // Insert NodeLng
            $SubjectLngDefValues = DB_cf_subjects_lang::cf_subjects_lang_get_NewRecordDefValues($this->appFrw, $params);

            $params["SubLngID"] = $SubjectLngDefValues['data']['SubLngID'];

            $result = DB_cf_subjects_lang::cf_subjects_lang_InsertRecord($this->appFrw, $params);

            if($result > 0)
            {
                $results = DB_cf_subjects_lang::cf_subjects_lang_getRecord($this->appFrw, $params);

                if ($results['success']==true)
                    return json_encode((array('data' => $results["data"])));
                else
                    return json_encode((array('error' => $results["reason"])));
            }

        }
        else
        {
            $SubLngID = DB_cf_subjects_lang::cf_subjects_lang_GetSubLngID($this->appFrw, $params);

            $params["SubLngID"] = $SubLngID;

            $results = DB_cf_subjects_lang::cf_subjects_lang_getRecord($this->appFrw, $params);

            if ($results['success']==true)
                return json_encode((array('data' => $results["data"])));
            else
                return json_encode((array('error' => $results["reason"])));
        }


    }

    /****************************************************************************************
     * Home::UpdateSubjectLngRecord
     ****************************************************************************************/
    function UpdateSubjectLngRecord()
    {
        $params = array();

        $params['SubLngID']         = isset($_REQUEST['SubLngID']) ? $_REQUEST['SubLngID'] : 0;
        $params['SubLngSubID']      = isset($_REQUEST['SubLngSubID']) ? $_REQUEST['SubLngSubID'] : 0;
        $params['SubLngType']       = isset($_REQUEST['SubLngType']) ? $_REQUEST['SubLngType'] : 0;
        $params['SubLngTitle']      = isset($_REQUEST['SubLngTitle']) ? $_REQUEST['SubLngTitle'] : 0;
        $params['SubLngShowOther']  = isset($_REQUEST['SubLngShowOther']) ? $_REQUEST['SubLngShowOther'] : 0;

        $results = DB_cf_subjects_lang::cf_subjects_lang_UpdateRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /*
		SubSubject
	*/

    /****************************************************************************************
     * Home::get_SubSubject_NewRecordDefValues
     ****************************************************************************************/
    function get_SubSubject_NewRecordDefValues()
    {
        $params = array();

        $params["SubID"] = $_REQUEST["SubID"];

        $results = DB_cf_sub_subject::cf_sub_subject_get_NewRecordDefValues($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::InsertSubSubjectRecord
     ****************************************************************************************/
    function InsertSubSubjectRecord()
    {
        $params = array();

        $params["SubSubID"]         = $_REQUEST["SubSubID"];
        $params["SubSubSubID"]      = $_REQUEST["SubSubSubID"];
        $params["SubSubTitle"]      = $_REQUEST["SubSubTitle"];
        $params["SubSubPriority"]   = $_REQUEST["SubSubPriority"];
        $params["SubSubStatus"]     = $_REQUEST["SubSubStatus"];

        $results = DB_cf_sub_subject::cf_sub_subject_InsertRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::get_SubSubjectRecord
     ****************************************************************************************/
    function get_SubSubjectRecord()
    {
        $params["SubSubID"] = $_REQUEST["SubSubID"];

        $results = DB_cf_sub_subject::cf_sub_subject_getRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::UpdateSubSubjectRecord
     ****************************************************************************************/
    function UpdateSubSubjectRecord()
    {
        $params = array();

        $params['SubSubID']         = isset($_REQUEST['SubSubID']) ? $_REQUEST['SubSubID'] : 0;
        $params['SubSubSubID']      = isset($_REQUEST['SubSubSubID']) ? $_REQUEST['SubSubSubID'] : 0;
        $params['SubSubTitle']      = isset($_REQUEST['SubSubTitle']) ? $_REQUEST['SubSubTitle'] : "";
        $params['SubSubPriority']   = isset($_REQUEST['SubSubPriority']) ? $_REQUEST['SubSubPriority'] : 0;
        $params['SubSubStatus']     = isset($_REQUEST['SubSubStatus']) ? $_REQUEST['SubSubStatus'] : 0;

        $results = DB_cf_sub_subject::cf_sub_subject_UpdateRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::DeleteSubSubjectRecord
     ****************************************************************************************/
    function DeleteSubSubjectRecord()
    {
        $params = array();

        $params["SubSubID"] = $_REQUEST["SubSubID"];

        $results = DB_cf_sub_subject::cf_sub_subject_DeleteRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Home::getSubSubjectsList
     ****************************************************************************************/
    function getSubSubjectsList()
    {
        $params = array();

        $params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
        $params['SubID'] 	        = isset($_REQUEST['SubID']) ? $_REQUEST['SubID'] : 0;
        $params['filterShowAll']    = isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;

        $results = DB_cf_sub_subject::cf_sub_subject_get_List($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /*
        SubSubject LNG
    */

    /****************************************************************************************
     * Home::get_SubSubjectLngRecord
     ****************************************************************************************/
    function get_SubSubjectLngRecord()
    {
        //Check if Record Exists
        $params["SubSubLngSubSubID"]    = $_REQUEST["SubSubLngSubSubID"];
        $params["SubSubLngType"]        = $_REQUEST["SubSubLngType"];


        $CheckIfSubSubLngExists = DB_cf_sub_subject_lang::cf_sub_subject_lang_CheckBeforeInsert($this->appFrw, $params);

        if ($CheckIfSubSubLngExists == 0)
        {
            // Insert NodeLng
            $SubSubLngDefValues = DB_cf_sub_subject_lang::cf_sub_subject_lang_get_NewRecordDefValues($this->appFrw, $params);

            $params["SubSubLngID"] = $SubSubLngDefValues['data']['SubSubLngID'];

            $result = DB_cf_sub_subject_lang::cf_sub_subject_lang_InsertRecord($this->appFrw, $params);

            if($result > 0)
            {
                $results = DB_cf_sub_subject_lang::cf_sub_subject_lang_getRecord($this->appFrw, $params);

                if ($results['success']==true)
                    return json_encode((array('data' => $results["data"])));
                else
                    return json_encode((array('error' => $results["reason"])));
            }

        }
        else
        {
            $SubSubLngID = DB_cf_sub_subject_lang::cf_sub_subject_lang_GetSubSubLngID($this->appFrw, $params);

            $params["SubSubLngID"] = $SubSubLngID;

            $results = DB_cf_sub_subject_lang::cf_sub_subject_lang_getRecord($this->appFrw, $params);

            if ($results['success']==true)
                return json_encode((array('data' => $results["data"])));
            else
                return json_encode((array('error' => $results["reason"])));
        }


    }

    /****************************************************************************************
     * Home::UpdateSubSubjectLngRecord
     ****************************************************************************************/
    function UpdateSubSubjectLngRecord()
    {
        $params = array();

        $params['SubSubLngID']          = isset($_REQUEST['SubSubLngID']) ? $_REQUEST['SubSubLngID'] : 0;
        $params['SubSubLngSubSubID']    = isset($_REQUEST['SubSubLngSubSubID']) ? $_REQUEST['SubSubLngSubSubID'] : 0;
        $params['SubSubLngTitle']       = isset($_REQUEST['SubSubLngTitle']) ? $_REQUEST['SubSubLngTitle'] : "";
        $params['SubSubLngType']        = isset($_REQUEST['SubSubLngType']) ? $_REQUEST['SubSubLngType'] : 0;

        $results = DB_cf_sub_subject_lang::cf_sub_subject_lang_UpdateRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /*
        Answer LNG
    */

    /****************************************************************************************
     * Home::get_AnswerLngRecord
     ****************************************************************************************/
    function get_AnswerLngRecord()
    {
        //Check if Record Exists
        $params["AnsLngSubSubID"]   = $_REQUEST["AnsLngSubSubID"];
        $params["AnsLngType"]       = $_REQUEST["AnsLngType"];


        $CheckIfAnswerLngExists = DB_cf_answers_lang::cf_answers_lang_CheckBeforeInsert($this->appFrw, $params);

        if ($CheckIfAnswerLngExists == 0)
        {
            // Insert NodeLng
            $AnswerLngDefValues = DB_cf_answers_lang::cf_answers_lang_get_NewRecordDefValues($this->appFrw, $params);

            $params["AnsLngID"] = $AnswerLngDefValues['data']['AnsLngID'];

            $result = DB_cf_answers_lang::cf_answers_lang_InsertRecord($this->appFrw, $params);

            if($result > 0)
            {
                $results = DB_cf_answers_lang::cf_answers_lang_getRecord($this->appFrw, $params);

                if ($results['success']==true)
                    return json_encode((array('data' => $results["data"])));
                else
                    return json_encode((array('error' => $results["reason"])));
            }

        }
        else
        {
            $AnsLngID = DB_cf_answers_lang::cf_answers_lang_GetAnsLngID($this->appFrw, $params);

            $params["AnsLngID"] = $AnsLngID;

            $results = DB_cf_answers_lang::cf_answers_lang_getRecord($this->appFrw, $params);

            if ($results['success']==true)
                return json_encode((array('data' => $results["data"])));
            else
                return json_encode((array('error' => $results["reason"])));
        }


    }

    /****************************************************************************************
     * Home::UpdateAnswerLngRecord
     ****************************************************************************************/
    function UpdateAnswerLngRecord()
    {
        $params = array();

        $params['AnsLngID']         = isset($_REQUEST['AnsLngID']) ? $_REQUEST['AnsLngID'] : 0;
        $params['AnsLngSubSubID']   = isset($_REQUEST['AnsLngSubSubID']) ? $_REQUEST['AnsLngSubSubID'] : 0;
        $params['AnsLngType']       = isset($_REQUEST['AnsLngType']) ? $_REQUEST['AnsLngType'] : 0;
        $params['AnsLngTitle']      = isset($_REQUEST['AnsLngTitle']) ? $_REQUEST['AnsLngTitle'] : "";
        $params['AnsLngDescr']      = isset($_REQUEST['AnsLngDescr']) ? $_REQUEST['AnsLngDescr'] : "";
        $params['AnsLngShowCnt']    = isset($_REQUEST['AnsLngShowCnt']) ? $_REQUEST['AnsLngShowCnt'] : "";

        $results = DB_cf_answers_lang::cf_answers_lang_UpdateRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

	/*
		TINY MCE
	*/
	
	/****************************************************************************************
	* Home::handleTinyMCE_UploadImage
	****************************************************************************************/
	function handleTinyMCE_UploadImage()
	{
		$id 					= $_REQUEST["id"];
		$table_name 	= $_REQUEST["table_name"];
		$fileype 			= $_REQUEST["tinymcefileype"];
				
		$filename 	= $_FILES['tinymceupload']['name'];				 // news.jpg
		$filetype	= $_FILES['tinymceupload']['type']; 				//  image/jpeg
        $tmpfile	= $_FILES['tinymceupload']['tmp_name']; 			//  C:\wamp\tmp\php8146.tmp
		$error 		= $_FILES['tinymceupload']['error']; 				//  0
		$filesize	= $_FILES['tinymceupload']['size'];					 //  9490
		
		//print_r($_REQUEST);
		//print_r($_FILES);
		
		$targetFolder = FileManager::getTblFolderPath($table_name, $id );
		$relativeUrl = FileManager::getTblFolderUrl($table_name, $id );
		
		move_uploaded_file($tmpfile, "$targetFolder/$filename");
		
		$ret['success']		= true;
		$ret['fileUrl']			= $relativeUrl.'/'.$filename;
		
		echo json_encode($ret);
		exit();
	}

	
}
?>