﻿Ext.define('Home.controller.Home', {
    extend: 'Ext.app.Controller',
    alias: 'widget.HomeControler',

    models: [],
    stores: [],
    views: [
				'MList_Sites'
				,'MList_Nodes'
				,'Frm_Node_Details'
				,'MList_Shops'
				,'Frm_Shop_Details'
				,'MList_Shops_Locations'
				,'Frm_Shop_Location_Details'
				,'MList_Images'
				,'Frm_Images_Translation'
				,'MList_Videos'
				,'Frm_Videos_Translation'
				,'MList_Documents'
				,'Frm_Documents_Translation'
				,'MList_Nodes_Media'
				,'MList_Node_Images'
				,'MList_Node_Videos'
				,'MList_Node_Documents'
				,'MList_Alerts'
				,'Frm_Alerts_Translation'
				,'MList_Sliders'
				,'MList_SliderImages'
				,'Frm_Slider_Images_Translation'
				,'MList_Node_Sliders'
				,'MList_Node_Slider_Images'
				,'MList_Disaster_Mode'
				,'Frm_Disaster_Mode_Translation'
				,'Frm_Airlines_Translation'
				,'MList_Related_Links'
				,'Frm_Related_Links_Translation'
				,'MList_Node_RelatedLinks'
				,'MList_Categories'
				,'MList_Categories_Records'
				,'Frm_Categories_Records_Details'
				,'MList_Nodes_Categories'
				,'MList_Footer'
				,'Frm_Footer_Translation'
				,'MList_Footer_Data'
				,'Frm_Footer_Data_Translation'
				,'MList_Image_Galleries'
				,'Frm_ImageGallery_Translation'
				,'MList_Image_Galleries_Data'
				,'Frm_ImageGallery_Data_Translation'
				,'Frm_Categories_Translation'
				,'MList_Video_Galleries'
				,'Frm_VideoGallery_Translation'
				,'MList_Video_Galleries_Data'
				,'Frm_VideoGallery_Data_Translation'
				,'MList_Publications_Galleries'
				,'Frm_PublicationsGallery_Translation'
				,'MList_Publications_Data'
				,'Frm_PublicationsGallery_Data_Translation'
				,'MList_Categories_Records_Sliders'
				,'MList_Category_Sliders'
				,'MList_Subject'
				,'Frm_Subject_Translation'
				,'MList_Sub_Subject'
				,'Frm_Sub_Subject_Translation'
				,'Frm_Subject_Answer_Translation'
				,'MList_Footer_Timetable'
				,'Frm_FooterTimetable_Translation'

			],

	/*******************************************************************
	* Sites_ClearRelatedBeforeReload
	********************************************************************/
	Sites_ClearRelatedBeforeReload: function(grid)
	{
		// Reset Node  Site Title
		Ext.ComponentManager.get('TabNode').setTitle('Management');

		// Clear Nodes List
		Ext.ComponentManager.get('MList_Nodes').clearData();

		// Clear Shops List
		Ext.ComponentManager.get('MList_Nodes').clearData();

		// Clear Images List
		Ext.ComponentManager.get('MList_Images').clearData();

		// Clear Videos List
		Ext.ComponentManager.get('MList_Videos').clearData();

		// Clear Documents List
		Ext.ComponentManager.get('MList_Documents').clearData();

		// Clear Alerts List
		Ext.ComponentManager.get('MList_Alerts').clearData();

		// Clear Sliders List
		Ext.ComponentManager.get('MList_Sliders').clearData();

		// Clear Disaster Mode List
		Ext.ComponentManager.get('MList_Disaster_Mode').clearData();

		// Clear Airlines List
		Ext.ComponentManager.get('MList_Site_Airlines2').clearData();

		// Clear Related Links List
		Ext.ComponentManager.get('MList_Related_Links').clearData();

		// Clear Categories List
		Ext.ComponentManager.get('MList_Nodes_Categories').clearData();

		// Clear Footer List
		Ext.ComponentManager.get('MList_Footer').clearData();

		// Clear Image Gallery List
		Ext.ComponentManager.get('MList_Image_Galleries').clearData();

		// Clear Video Gallery List
		Ext.ComponentManager.get('MList_Video_Galleries').clearData();

		// Clear Publications Gallery List
		Ext.ComponentManager.get('MList_Publications_Galleries').clearData();

        // Clear Subject List
        Ext.ComponentManager.get('MList_Subject').clearData();

        // Clear Subject List
        Ext.ComponentManager.get('MList_Footer_Timetable').clearData();


	},

	/*******************************************************************
	* Sites_OnSelectionChange
	********************************************************************/
	Sites_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				// Set Node  Site Title
				Ext.ComponentManager.get('TabNode').setTitle(selectedRecord.get('SiteDomainName'));	

				var selected_SiteID = selectedRecord.get('SiteID');

				//Load Nodes List
				Ext.ComponentManager.get('MList_Nodes').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Nodes').loadData();

				//Load Shops List
				Ext.ComponentManager.get('MList_Shops').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Shops').loadData();

				//Load Images List
				Ext.ComponentManager.get('MList_Images').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Images').loadData();

				//Load Videos List
				Ext.ComponentManager.get('MList_Videos').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Videos').loadData();

				//Load Documents List
				Ext.ComponentManager.get('MList_Documents').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Documents').loadData();

				//Load Alerts List
				Ext.ComponentManager.get('MList_Alerts').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Alerts').loadData();

				//Load Sliders List
				Ext.ComponentManager.get('MList_Sliders').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Sliders').loadData();

				//Load Disaster Mode List
				Ext.ComponentManager.get('MList_Disaster_Mode').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Disaster_Mode').loadData();

				//Load Airlines List
				Ext.ComponentManager.get('MList_Site_Airlines2').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Site_Airlines2').loadData();

				//Load Related Links List
				Ext.ComponentManager.get('MList_Related_Links').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Related_Links').loadData();

				//Load Categories List
				Ext.ComponentManager.get('MList_Nodes_Categories').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Nodes_Categories').loadData();

				//Load Footer List
				Ext.ComponentManager.get('MList_Footer').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Footer').loadData();

				//Load Image Galleries List
				Ext.ComponentManager.get('MList_Image_Galleries').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Image_Galleries').loadData();

				//Load Video Galleries List
				Ext.ComponentManager.get('MList_Video_Galleries').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Video_Galleries').loadData();

				//Load Publications Galleries List
				Ext.ComponentManager.get('MList_Publications_Galleries').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Publications_Galleries').loadData();

                //Load Subject List
                Ext.ComponentManager.get('MList_Subject').SiteID = selected_SiteID;
                Ext.ComponentManager.get('MList_Subject').loadData();

                //Load Footer Timetable
                Ext.ComponentManager.get('MList_Footer_Timetable').SiteID = selected_SiteID;
                Ext.ComponentManager.get('MList_Footer_Timetable').loadData();

			}
		}
	},

	/*******************************************************************
	* Nodes_ClearRelatedBeforeReload
	********************************************************************/
	Nodes_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Language Selection Form
		Ext.ComponentManager.get('Frm_Node_Details').clearData();

		// Clear Images List
		Ext.ComponentManager.get('MList_Node_ImagesSitePages').clearData();

		// Clear Videos List
		Ext.ComponentManager.get('MList_Node_VideosSitePages').clearData();

		// Clear Documents List
		Ext.ComponentManager.get('MList_Node_DocumentsSitePages').clearData();

		// Clear SliderImages List
		Ext.ComponentManager.get('MList_Node_Slider_Images').clearData();

		// Clear SliderImages List
		Ext.ComponentManager.get('MList_Node_RelatedLinksSitePages').clearData();



	},

	/*******************************************************************
	* Nodes_OnSelectionChange
	********************************************************************/
	Nodes_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_NodeID = selectedRecord.get('NodeID');

				//Load Node Details
				Ext.ComponentManager.get('Frm_Node_Details').NodeID = selected_NodeID;
				Ext.ComponentManager.get('Frm_Node_Details').loadData();

				// Load Images List
				Ext.ComponentManager.get('MList_Node_ImagesSitePages').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_ImagesSitePages').loadData();

				// Load Videos List
				Ext.ComponentManager.get('MList_Node_VideosSitePages').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_VideosSitePages').loadData();

				// Load Documents List
				Ext.ComponentManager.get('MList_Node_DocumentsSitePages').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_DocumentsSitePages').loadData();

				// Load SliderImages List
				Ext.ComponentManager.get('MList_Node_Slider_Images').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_Slider_Images').loadData();

				// Load SliderImages List
				Ext.ComponentManager.get('MList_Node_RelatedLinksSitePages').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_RelatedLinksSitePages').loadData();



			}
		}
	},

	/*******************************************************************
	* Shops_ClearRelatedBeforeReload
	********************************************************************/
	Shops_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Shop Details
		Ext.ComponentManager.get('Frm_Shop_Details').clearData();

		// Clear Shop Locations
		Ext.ComponentManager.get('MList_Shops_Locations').clearData();


	},

	/*******************************************************************
	* Shops_OnSelectionChange
	********************************************************************/
	Shops_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_ShopID = selectedRecord.get('ShopID');

				// Load Shop Details
				Ext.ComponentManager.get('Frm_Shop_Details').ShopID = selected_ShopID;
				Ext.ComponentManager.get('Frm_Shop_Details').loadData();

				// Load Shop Locations
				Ext.ComponentManager.get('MList_Shops_Locations').ShopID = selected_ShopID;
				Ext.ComponentManager.get('MList_Shops_Locations').loadData();


			}
		}
	},


	/*******************************************************************
	* ShopsLocations_ClearRelatedBeforeReload
	********************************************************************/
	ShopsLocations_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Location Details
		Ext.ComponentManager.get('Frm_Shop_Location_Details').clearData();

	},

	/*******************************************************************
	* ShopsLocations_OnSelectionChange
	********************************************************************/
	ShopsLocations_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_LocationID = selectedRecord.get('LocationID');

				// Load Location Details
				Ext.ComponentManager.get('Frm_Shop_Location_Details').LocationID = selected_LocationID;
				Ext.ComponentManager.get('Frm_Shop_Location_Details').loadData();


			}
		}
	},

	/*******************************************************************
	* Images_ClearRelatedBeforeReload
	********************************************************************/
	Images_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Images Translation
		Ext.ComponentManager.get('Frm_Images_Translation').clearData();

		// Clear Nodes List
		Ext.ComponentManager.get('MList_Nodes_Media').clearData();

	},

	/*******************************************************************
	* Images_OnSelectionChange
	********************************************************************/
	Images_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_MediaID = selectedRecord.get('MediaID');
				var selected_SiteID = selectedRecord.get('MediaSiteID');

				// Load Images Translation
				Ext.ComponentManager.get('Frm_Images_Translation').MediaID = selected_MediaID;
				Ext.ComponentManager.get('Frm_Images_Translation').loadData();

				// Load Nodes List
				Ext.ComponentManager.get('MList_Nodes_Media').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Nodes_Media').MediaID = selected_MediaID;
				Ext.ComponentManager.get('MList_Nodes_Media').loadData();


			}
		}
	},

	/*******************************************************************
	* Videos_ClearRelatedBeforeReload
	********************************************************************/
	Videos_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Videos Translations
		Ext.ComponentManager.get('Frm_Videos_Translation').clearData();

		// Clear Videos Translations
		Ext.ComponentManager.get('MList_Nodes_Media2').clearData();

	},

	/*******************************************************************
	* Videos_OnSelectionChange
	********************************************************************/
	Videos_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_MediaID = selectedRecord.get('MediaID');
				var selected_SiteID = selectedRecord.get('MediaSiteID');

				// Load Videos Translation
				Ext.ComponentManager.get('Frm_Videos_Translation').MediaID = selected_MediaID;
				Ext.ComponentManager.get('Frm_Videos_Translation').loadData();

				// Load Nodes List
				Ext.ComponentManager.get('MList_Nodes_Media2').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Nodes_Media2').MediaID = selected_MediaID;
				Ext.ComponentManager.get('MList_Nodes_Media2').loadData();


			}
		}
	},

	/*******************************************************************
	* Documents_ClearRelatedBeforeReload
	********************************************************************/
	Documents_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Documents Details
		Ext.ComponentManager.get('Frm_Documents_Translation').clearData();

		// Clear Nodes Lists
		Ext.ComponentManager.get('MList_Nodes_Media3').clearData();

	},

	/*******************************************************************
	* Documents_OnSelectionChange
	********************************************************************/
	Documents_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_MediaID = selectedRecord.get('MediaID');
				var selected_SiteID = selectedRecord.get('MediaSiteID');

				// Load Documents Details
				Ext.ComponentManager.get('Frm_Documents_Translation').MediaID = selected_MediaID;
				Ext.ComponentManager.get('Frm_Documents_Translation').loadData();

				// Load Nodes List
				Ext.ComponentManager.get('MList_Nodes_Media3').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Nodes_Media3').MediaID = selected_MediaID;
				Ext.ComponentManager.get('MList_Nodes_Media3').loadData();


			}
		}
	},

	/*******************************************************************
	* Nodes_Media_ClearRelatedBeforeReload
	********************************************************************/
	Nodes_Media_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Images Data
		Ext.ComponentManager.get('MList_Images').NodeID = 0;

		// Set Node Title
		Ext.ComponentManager.get('MList_Node_Images').setTitle('Node Images');

		// Clear Node Images
		Ext.ComponentManager.get('MList_Node_Images').clearData();

	},

	/*******************************************************************
	* Nodes_Media_OnSelectionChange
	********************************************************************/
	Nodes_Media_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_NodeID = selectedRecord.get('NodeID');
				var selected_NodeTitle = selectedRecord.get('NodeTitle');

				// Set Images Data
				Ext.ComponentManager.get('MList_Images').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Images').NodeTitle = selected_NodeTitle;

				// Set Node Title
				Ext.ComponentManager.get('MList_Node_Images').setTitle('Node Images of :: '+selectedRecord.get('NodeTitle'));

				// Load Node Images
				Ext.ComponentManager.get('MList_Node_Images').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_Images').loadData();


			}
		}
	},

	/*******************************************************************
	* Nodes_Media2_ClearRelatedBeforeReload
	********************************************************************/
	Nodes_Media2_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Images Data
		Ext.ComponentManager.get('MList_Videos').NodeID = 0;

		// Clear Node Title
		Ext.ComponentManager.get('MList_Node_Videos').setTitle('Node Videos');


		// Clear Node Videos
		Ext.ComponentManager.get('MList_Node_Images').clearData();

	},

	/*******************************************************************
	* Nodes_Media2_OnSelectionChange
	********************************************************************/
	Nodes_Media2_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_NodeID = selectedRecord.get('NodeID');
				var selected_NodeTitle = selectedRecord.get('NodeTitle');

				// Set Video Data
				Ext.ComponentManager.get('MList_Videos').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Videos').NodeTitle = selected_NodeTitle;

				// Set Node Title
				Ext.ComponentManager.get('MList_Node_Videos').setTitle('Node Images of :: '+selectedRecord.get('NodeTitle'));


				// Load Node Videos
				Ext.ComponentManager.get('MList_Node_Videos').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_Videos').loadData();


			}
		}
	},

	/*******************************************************************
	* Nodes_Media3_ClearRelatedBeforeReload
	********************************************************************/
	Nodes_Media3_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Images Data
		Ext.ComponentManager.get('MList_Documents').NodeID = 0;

		// Clear Node Title
		Ext.ComponentManager.get('MList_Node_Documents').setTitle('Node');


		// Clear Node Images
		Ext.ComponentManager.get('MList_Node_Images').clearData();

	},

	/*******************************************************************
	* Nodes_Media3_OnSelectionChange
	********************************************************************/
	Nodes_Media3_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_NodeID = selectedRecord.get('NodeID');
				var selected_NodeTitle = selectedRecord.get('NodeTitle');

				// Set Video Data
				Ext.ComponentManager.get('MList_Documents').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Documents').NodeTitle = selected_NodeTitle;

				// Set Node Title
				Ext.ComponentManager.get('MList_Node_Documents').setTitle('Node Documents of :: '+selectedRecord.get('NodeTitle'));


				// Load Node Videos
				Ext.ComponentManager.get('MList_Node_Documents').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_Documents').loadData();


			}
		}
	},

	/*******************************************************************
	* Sliders_ClearRelatedBeforeReload
	********************************************************************/
	Sliders_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Slider Images List
		Ext.ComponentManager.get('MList_SliderImages').clearData();

		// Clear Nodes List
		Ext.ComponentManager.get('MList_Nodes_Media4').clearData();


	},

	/*******************************************************************
	* Sliders_OnSelectionChange
	********************************************************************/
	Sliders_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_SliderID = selectedRecord.get('SliderID');
				var selected_SiteID = selectedRecord.get('SliderSiteID');

				// Load Slider Images List
				Ext.ComponentManager.get('MList_SliderImages').SliderID = selected_SliderID;
				Ext.ComponentManager.get('MList_SliderImages').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_SliderImages').loadData();

				// Load Nodes List
				Ext.ComponentManager.get('MList_Nodes_Media4').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Nodes_Media4').SliderID = selected_SliderID;
				Ext.ComponentManager.get('MList_Nodes_Media4').loadData();

				// Load Categories Records Sliders List
				Ext.ComponentManager.get('MList_Categories_Records_Sliders').SliderID = selected_SliderID;
				Ext.ComponentManager.get('MList_Categories_Records_Sliders').loadData();


			}
		}
	},
    /*******************************************************************
	* SliderImages_ClearRelatedBeforeReload
	********************************************************************/
	SliderImages_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Slider Images List
		Ext.ComponentManager.get('Frm_Slider_Images_Translation').clearData();


	},

	/*******************************************************************
	* SliderImages_OnSelectionChange
	********************************************************************/
	SliderImages_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_SliderImagesID = selectedRecord.get('SliderImagesID');

				// Load Slider Images List
				Ext.ComponentManager.get('Frm_Slider_Images_Translation').SliderImagesID = selected_SliderImagesID;
				Ext.ComponentManager.get('Frm_Slider_Images_Translation').loadData();


			}
		}
	},

	/*******************************************************************
	* Nodes_Media4_ClearRelatedBeforeReload
	********************************************************************/
	Nodes_Media4_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Slider Data
		Ext.ComponentManager.get('MList_Documents').NodeID = 0;
		Ext.ComponentManager.get('MList_Documents').NodeTitle = "";

		// Set Node Title
		Ext.ComponentManager.get('MList_Node_Sliders').setTitle('Node Sliders');

		// Clear Node Slider List
		Ext.ComponentManager.get('MList_Node_Sliders').clearData();

		// Clear Node Categories List
		Ext.ComponentManager.get('MList_Categories_Records_Sliders').clearData();

	},

	/*******************************************************************
	* Nodes_Media4_OnSelectionChange
	********************************************************************/
	Nodes_Media4_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_NodeID = selectedRecord.get('NodeID');
				var selected_NodeTitle = selectedRecord.get('NodeTitle');
				var selected_NodeDataType = selectedRecord.get('NodeDataType');

				// Set Slider Data
				Ext.ComponentManager.get('MList_Sliders').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Sliders').NodeTitle = selected_NodeTitle;

				// Set Node Title
				Ext.ComponentManager.get('MList_Node_Sliders').setTitle('Node Sliders of :: '+selectedRecord.get('NodeTitle'));


				// Load Node Slider List
				Ext.ComponentManager.get('MList_Node_Sliders').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_Sliders').loadData();


				// Load Node Categories List
				Ext.ComponentManager.get('MList_Categories_Records_Sliders').CtgID = selected_NodeDataType;
				Ext.ComponentManager.get('MList_Categories_Records_Sliders').loadData();


			}
		}
	},

	/*******************************************************************
	* CategoriesRecordsSliders_ClearRelatedBeforeReload
	********************************************************************/
	CategoriesRecordsSliders_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Node Categories Slider List
		Ext.ComponentManager.get('MList_Category_Sliders').clearData();

	},

	/*******************************************************************
	* CategoriesRecordsSliders_OnSelectionChange
	********************************************************************/
	CategoriesRecordsSliders_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_CtgRecID = selectedRecord.get('CtgRecID');
				var selected_CtgRecTitle = selectedRecord.get('CtgRecTitle');

				// Set Slider Data
				Ext.ComponentManager.get('MList_Sliders').CtgRecID = selected_CtgRecID;
				Ext.ComponentManager.get('MList_Sliders').CtgRecTitle = selected_CtgRecTitle;

				// Set Node Title
				Ext.ComponentManager.get('MList_Category_Sliders').setTitle('Category Sliders of :: '+selectedRecord.get('CtgRecTitle'));

				// Load Node Categories Slider List
				Ext.ComponentManager.get('MList_Category_Sliders').CtgRecID = selected_CtgRecID;
				Ext.ComponentManager.get('MList_Category_Sliders').loadData();


			}
		}
	},

	/*******************************************************************
	* Alerts_ClearRelatedBeforeReload
	********************************************************************/
	Alerts_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Alert Details
		Ext.ComponentManager.get('Frm_Alerts_Translation').clearData();

	},

	/*******************************************************************
	* Alerts_OnSelectionChange
	********************************************************************/
	Alerts_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_AlertID = selectedRecord.get('AlertID');

				// Load Alert Details
				Ext.ComponentManager.get('Frm_Alerts_Translation').AlertID = selected_AlertID;
				Ext.ComponentManager.get('Frm_Alerts_Translation').loadData();

			}
		}
	},

	/*******************************************************************
	* Disaster_ClearRelatedBeforeReload
	********************************************************************/
	Disaster_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Alert Details
		Ext.ComponentManager.get('Frm_Disaster_Mode_Translation').clearData();

	},

	/*******************************************************************
	* Disaster_OnSelectionChange
	********************************************************************/
	Disaster_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_DisasterID = selectedRecord.get('DisasterID');

				// Load Alert Details
				Ext.ComponentManager.get('Frm_Disaster_Mode_Translation').DisasterID = selected_DisasterID;
				Ext.ComponentManager.get('Frm_Disaster_Mode_Translation').loadData();

			}
		}
	},

	/*******************************************************************
	* Airlines_ClearRelatedBeforeReload
	********************************************************************/
	Airlines_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Alert Details
		Ext.ComponentManager.get('Frm_Airlines_Translation').clearData();

	},

	/*******************************************************************
	* Airlines_OnSelectionChange
	********************************************************************/
	Airlines_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_SiteAirlineID = selectedRecord.get('SiteAirlineID');

				// Load Alert Details
				Ext.ComponentManager.get('Frm_Airlines_Translation').SiteAirlineID = selected_SiteAirlineID;
				Ext.ComponentManager.get('Frm_Airlines_Translation').loadData();

			}
		}
	},

	/*******************************************************************
	* RelatedLinks_ClearRelatedBeforeReload
	********************************************************************/
	RelatedLinks_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Related Links
		Ext.ComponentManager.get('Frm_Related_Links_Translation').clearData();

		// Clear Nodes List
		Ext.ComponentManager.get('MList_Nodes_Media5').clearData();

	},

	/*******************************************************************
	* RelatedLinks_OnSelectionChange
	********************************************************************/
	RelatedLinks_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_RelatedLinkID = selectedRecord.get('RelatedLinkID');
				var selected_SiteID = selectedRecord.get('RelatedLinkSiteID');

				// Load Related Links
				Ext.ComponentManager.get('Frm_Related_Links_Translation').RelatedLinkID = selected_RelatedLinkID;
				Ext.ComponentManager.get('Frm_Related_Links_Translation').loadData();

				// Load Nodes List
				Ext.ComponentManager.get('MList_Nodes_Media5').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Nodes_Media5').RelatedLinkID = selected_RelatedLinkID;
				Ext.ComponentManager.get('MList_Nodes_Media5').loadData();

			}
		}
	},

	/*******************************************************************
	* Nodes_Media5_ClearRelatedBeforeReload
	********************************************************************/
	Nodes_Media5_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Related_Links Data
		Ext.ComponentManager.get('MList_Related_Links').NodeID = 0;
		Ext.ComponentManager.get('MList_Related_Links').NodeTitle = "";

		// Set Node Title
		Ext.ComponentManager.get('MList_Node_RelatedLinks').setTitle('Node Related Links');

		// ClearNode Related Links List
		Ext.ComponentManager.get('MList_Node_RelatedLinks').clearData();

	},

	/*******************************************************************
	* Nodes_Media5_OnSelectionChange
	********************************************************************/
	Nodes_Media5_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_NodeID = selectedRecord.get('NodeID');
				var selected_NodeTitle = selectedRecord.get('NodeTitle');

				// Set Related_Links Data
				Ext.ComponentManager.get('MList_Related_Links').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Related_Links').NodeTitle = selected_NodeTitle;

				// Set Node Title
				Ext.ComponentManager.get('MList_Node_RelatedLinks').setTitle('Node Related Links of :: '+selectedRecord.get('NodeTitle'));

				// Load Node Related Links List
				Ext.ComponentManager.get('MList_Node_RelatedLinks').NodeID = selected_NodeID;
				Ext.ComponentManager.get('MList_Node_RelatedLinks').loadData();


			}
		}
	},

	/*******************************************************************
	* Categories_ClearRelatedBeforeReload
	********************************************************************/
	Categories_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Categories Records
		Ext.ComponentManager.get('MList_Categories_Records').clearData();

	},

	/*******************************************************************
	* Categories_OnSelectionChange
	********************************************************************/
	Categories_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_CtgID = selectedRecord.get('NodeDataType');

				if (selected_CtgID > 0)
				{
					// Load Categories Records
					Ext.ComponentManager.get('MList_Categories_Records').CtgID = selected_CtgID;
					Ext.ComponentManager.get('MList_Categories_Records').loadData();
				}
				else
				{
					// Clear Categories Records
					Ext.ComponentManager.get('MList_Categories_Records').clearData();
				}


			}
		}
	},

    /*******************************************************************
	* CategoriesRecords_ClearRelatedBeforeReload
	********************************************************************/
	CategoriesRecords_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Categories Records Translation
		Ext.ComponentManager.get('Frm_Categories_Records_Details').clearData();

		// Clear Categories Records Slider
		Ext.ComponentManager.get('MList_Content_Category_Sliders').clearData();

	},

	/*******************************************************************
	* CategoriesRecords_OnSelectionChange
	********************************************************************/
	CategoriesRecords_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_CtgRecID = selectedRecord.get('CtgRecID');

				// Load Categories Records Translation
				Ext.ComponentManager.get('Frm_Categories_Records_Details').CtgRecID = selected_CtgRecID;
				Ext.ComponentManager.get('Frm_Categories_Records_Details').loadData();


				// Load Categories Records Slider
				Ext.ComponentManager.get('MList_Content_Category_Sliders').CtgRecID = selected_CtgRecID;
				Ext.ComponentManager.get('MList_Content_Category_Sliders').loadData();

			}
		}
	},

    /*******************************************************************
	* FooterHeadline_ClearRelatedBeforeReload
	********************************************************************/
	FooterHeadline_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Footer headline Translation
		Ext.ComponentManager.get('Frm_Footer_Translation').clearData();

		// Clear Footer Data
		Ext.ComponentManager.get('MList_Footer_Data').clearData();

	},

	/*******************************************************************
	* FooterHeadline_OnSelectionChange
	********************************************************************/
	FooterHeadline_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_FtrID = selectedRecord.get('FtrID');
				var selected_SiteID = selectedRecord.get('FtrSiteID');

				// Load Footer headline Translation
				Ext.ComponentManager.get('Frm_Footer_Translation').FtrID = selected_FtrID;
				Ext.ComponentManager.get('Frm_Footer_Translation').loadData();

				// Load Footer Data
				Ext.ComponentManager.get('MList_Footer_Data').FtrID = selected_FtrID;
				Ext.ComponentManager.get('MList_Footer_Data').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Footer_Data').loadData();

			}
		}
	},

    /*******************************************************************
	* FooterData_ClearRelatedBeforeReload
	********************************************************************/
	FooterData_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Footer headline Translation
		Ext.ComponentManager.get('Frm_Footer_Data_Translation').clearData();
	},

	/*******************************************************************
	* FooterData_OnSelectionChange
	********************************************************************/
	FooterData_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_FtrDataID = selectedRecord.get('FtrDataID');

				// Load Footer Dats Translation
				Ext.ComponentManager.get('Frm_Footer_Data_Translation').FtrDataID = selected_FtrDataID;
				Ext.ComponentManager.get('Frm_Footer_Data_Translation').loadData();

			}
		}
	},

	/*******************************************************************
	* ImageGalleries_ClearRelatedBeforeReload
	********************************************************************/
	ImageGalleries_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Image Galllery Translation
		Ext.ComponentManager.get('Frm_ImageGallery_Translation').clearData();

		// Clear Image Galllery Data
		Ext.ComponentManager.get('MList_Image_Galleries_Data').clearData();

	},

	/*******************************************************************
	* ImageGalleries_OnSelectionChange
	********************************************************************/
	ImageGalleries_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_ImgID = selectedRecord.get('ImgID');

				// Load Image gallery Translation
				Ext.ComponentManager.get('Frm_ImageGallery_Translation').ImgID = selected_ImgID;
				Ext.ComponentManager.get('Frm_ImageGallery_Translation').loadData();

				// Load Image gallery Data
				Ext.ComponentManager.get('MList_Image_Galleries_Data').ImgID = selected_ImgID;
				Ext.ComponentManager.get('MList_Image_Galleries_Data').loadData();

			}
		}
	},

    /*******************************************************************
	* ImageGalleriesData_ClearRelatedBeforeReload
	********************************************************************/
	ImageGalleriesData_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Image Galllery Data Translation
		Ext.ComponentManager.get('Frm_ImageGallery_Data_Translation').clearData();
	},

	/*******************************************************************
	* ImageGalleriesData_OnSelectionChange
	********************************************************************/
	ImageGalleriesData_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_ImgDataID = selectedRecord.get('ImgDataID');

				// Load Image gallery data Translation
				Ext.ComponentManager.get('Frm_ImageGallery_Data_Translation').ImgDataID = selected_ImgDataID;
				Ext.ComponentManager.get('Frm_ImageGallery_Data_Translation').loadData();


			}
		}
	},

    /*******************************************************************
	* VideoGalleries_ClearRelatedBeforeReload
	********************************************************************/
	VideoGalleries_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Video Galllery  Translation
		Ext.ComponentManager.get('Frm_VideoGallery_Translation').clearData();

		// Clear VIdeo Galllery Data
		Ext.ComponentManager.get('MList_Video_Galleries_Data').clearData();

	},

	/*******************************************************************
	* VideoGalleries_OnSelectionChange
	********************************************************************/
	VideoGalleries_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_VidID = selectedRecord.get('VidID');

				// Load Video gallery  Translation
				Ext.ComponentManager.get('Frm_VideoGallery_Translation').VidID = selected_VidID;
				Ext.ComponentManager.get('Frm_VideoGallery_Translation').loadData();

				// Load Video gallery data
				Ext.ComponentManager.get('MList_Video_Galleries_Data').VidID = selected_VidID;
				Ext.ComponentManager.get('MList_Video_Galleries_Data').loadData();


			}
		}
	},

	 /*******************************************************************
	* VideoGalleriesData_ClearRelatedBeforeReload
	********************************************************************/
	VideoGalleriesData_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Image Galllery Data Translation
		Ext.ComponentManager.get('Frm_VideoGallery_Data_Translation').clearData();
	},

	/*******************************************************************
	* VideoGalleriesData_OnSelectionChange
	********************************************************************/
	VideoGalleriesData_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_VidDataID = selectedRecord.get('VidDataID');

				// Load Image gallery data Translation
				Ext.ComponentManager.get('Frm_VideoGallery_Data_Translation').VidDataID = selected_VidDataID;
				Ext.ComponentManager.get('Frm_VideoGallery_Data_Translation').loadData();


			}
		}
	},

    /*******************************************************************
	* PublicationsGalleries_ClearRelatedBeforeReload
	********************************************************************/
	PublicationsGalleries_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Publications Galllery  Translation
		Ext.ComponentManager.get('Frm_PublicationsGallery_Translation').clearData();

		// Clear Publications Galllery Data
		Ext.ComponentManager.get('MList_Publications_Data').clearData();

	},

	/*******************************************************************
	* PublicationsGalleries_OnSelectionChange
	********************************************************************/
	PublicationsGalleries_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_PubID = selectedRecord.get('PubID');

				// Load Publications gallery  Translation
				Ext.ComponentManager.get('Frm_PublicationsGallery_Translation').PubID = selected_PubID;
				Ext.ComponentManager.get('Frm_PublicationsGallery_Translation').loadData();

				// Load Publications gallery data
				Ext.ComponentManager.get('MList_Publications_Data').PubID = selected_PubID;
				Ext.ComponentManager.get('MList_Publications_Data').loadData();


			}
		}
	},

	 /*******************************************************************
	* PublicationsGalleriesData_ClearRelatedBeforeReload
	********************************************************************/
	PublicationsGalleriesData_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Image Galllery Data Translation
		Ext.ComponentManager.get('Frm_PublicationsGallery_Data_Translation').clearData();
	},

	/*******************************************************************
	* PublicationsGalleriesData_OnSelectionChange
	********************************************************************/
	PublicationsGalleriesData_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_PubDataID = selectedRecord.get('PubDataID');

				// Load Image gallery data Translation
				Ext.ComponentManager.get('Frm_PublicationsGallery_Data_Translation').PubDataID = selected_PubDataID;
				Ext.ComponentManager.get('Frm_PublicationsGallery_Data_Translation').loadData();


			}
		}
	},

    /*******************************************************************
     * Subject_ClearRelatedBeforeReload
     ********************************************************************/
    Subject_ClearRelatedBeforeReload: function(grid)
    {
        // Clear Subject Translation
        Ext.ComponentManager.get('Frm_Subject_Translation').clearData();

        // Clear Sub Subject Data
        Ext.ComponentManager.get('MList_Sub_Subject').clearData();

    },

    /*******************************************************************
     * Subject_OnSelectionChange
     ********************************************************************/
    Subject_OnSelectionChange: function(grid)
    {
        var sr = grid.getSelectionModel().getSelection();
        var selectedRecord;
        if(sr.length > 0)
        {
            var selectedRecord = sr[0];
            if (!Ext.isEmpty(selectedRecord))
            {
                var selected_SubID = selectedRecord.get('SubID');

                // Load Subject headline Translation
                Ext.ComponentManager.get('Frm_Subject_Translation').SubID = selected_SubID;
                Ext.ComponentManager.get('Frm_Subject_Translation').loadData();

                // Load Sub Subject Data
                Ext.ComponentManager.get('MList_Sub_Subject').SubID = selected_SubID;
                Ext.ComponentManager.get('MList_Sub_Subject').loadData();

            }
        }
    },

    /*******************************************************************
     * SubSubject_ClearRelatedBeforeReload
     ********************************************************************/
    SubSubject_ClearRelatedBeforeReload: function(grid)
    {
        // Clear Sub Subject Translation
        Ext.ComponentManager.get('Frm_Sub_Subject_Translation').clearData();

        // Clear Sub Subject Answer Data
        Ext.ComponentManager.get('Frm_Subject_Answer_Translation').clearData();

    },

    /*******************************************************************
     * SubSubject_OnSelectionChange
     ********************************************************************/
    SubSubject_OnSelectionChange: function(grid)
    {
        var sr = grid.getSelectionModel().getSelection();
        var selectedRecord;
        if(sr.length > 0)
        {
            var selectedRecord = sr[0];
            if (!Ext.isEmpty(selectedRecord))
            {
                var selected_SubSubID = selectedRecord.get('SubSubID');

                // Load Sub Subject headline Translation
                Ext.ComponentManager.get('Frm_Sub_Subject_Translation').SubSubID = selected_SubSubID;
                Ext.ComponentManager.get('Frm_Sub_Subject_Translation').loadData();

                // Load Sub Subject Answer Data
                Ext.ComponentManager.get('Frm_Subject_Answer_Translation').SubSubID = selected_SubSubID;
                Ext.ComponentManager.get('Frm_Subject_Answer_Translation').loadData();

            }
        }
    },

	/*******************************************************************
	 * FooterTimetable_ClearRelatedBeforeReload
	 ********************************************************************/
	FooterTimetable_ClearRelatedBeforeReload: function(grid)
	{
		// Clear Alert Details
		Ext.ComponentManager.get('Frm_FooterTimetable_Translation').clearData();

	},

	/*******************************************************************
	 * FooterTimetable_OnSelectionChange
	 ********************************************************************/
	FooterTimetable_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord))
			{
				var selected_FooterTimetableID = selectedRecord.get('FooterTimetableID');

				// Load Alert Details
				Ext.ComponentManager.get('Frm_FooterTimetable_Translation').FooterTimetableID = selected_FooterTimetableID;
				Ext.ComponentManager.get('Frm_FooterTimetable_Translation').loadData();

			}
		}
	},

});
