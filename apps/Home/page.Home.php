<?php
//If it is not called at root appMain.php do  nothing
if(!isset($appFrw)) exit();


require_once(realpath(__DIR__."/../../db")."/class.DB_sys_usr.php");

$UsrDetails = DB_sys_usr::get_UsrDetails($appFrw, $appFrw->UsrID);
//if($UsrDetails['UsrRole'] == 1)	exit();

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="PRAGMA" content="NO-CACHE">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Home</title>
	<script type="text/javascript" src="libjs/ext-4/ext-all.js"></script>
	<link rel="stylesheet" type="text/css" href="libjs/ext-4/resources/css/ext-all-gray.css">
	<link rel="stylesheet" type="text/css" href="resources/css/header.css?dc=<?php echo microtime(true)?>">
	<link rel="stylesheet" type="text/css" href="resources/css/general.css?dc=<?php echo microtime(true)?>">
	<link rel="stylesheet" type="text/css" href="resources/css/icon.css">
	<link rel="shortcut icon" href="resources/images/favicon/favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome-4.7.0/css/font-awesome.min.css">

	<script type="text/javascript" src="libjs/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="libjs/TinyMCETextArea/TinyMCETextArea.js"></script>
	<script type="text/javascript" src="libjs/TinyMCETextArea/My_TinyMCETextArea.js"></script>

	<?php

	$menuCurrentPage = 'Airports';
	include('apps/Include/MainMenu.php');

	$header_username = $UsrDetails['UsrFullName'];
	include('apps/Include/Header.php');

	?>

	<script type="text/javascript">
	Ext.Loader.setConfig( {enabled: true, disableCaching: true} );

	Ext.Loader.setPath('Ext.ux', "libjs/ext-4.ux");

	Ext.require([
					'Ext.form.Panel',
					'Ext.ux.form.MultiSelect',
					'Ext.ux.form.ItemSelector',
					'Ext.tip.QuickTipManager',
					'Ext.ux.ajax.JsonSimlet',
					'Ext.ux.ajax.SimManager',
					'Ext.ux.grid.FiltersFeature',
					'Ext.ux.grid.menu.ListMenu',
					'Ext.ux.grid.menu.RangeMenu',
					'Ext.ux.grid.filter.BooleanFilter',
					'Ext.ux.grid.filter.DateFilter',
					'Ext.ux.grid.filter.ListFilter',
					'Ext.ux.grid.filter.NumericFilter',
					'Ext.ux.grid.filter.StringFilter',
					'Ext.ux.grid.images.*',
					'Ext.ux.CheckColumn',
					'Ext.selection.CellModel',
					'Ext.grid.plugin.CellEditing'
	]);

	Ext.Loader.setPath('FileMgr', 'apps/FileMgr');
	Ext.require([ 'FileMgr.view.ExtFileList']);
	Ext.require([ 'FileMgr.view.ShowImageFile']);

	Ext.Loader.setPath('Airlines', 'apps/Airlines');
	Ext.require([ 'Airlines.view.MList_Site_Airlines']);

	var Home = Ext.application({
		name: 'Home',
		appFolder: 'apps/Home',

		controllers: ['Home'],

		launch: function()
		{
			var myApp = this;

			Ext.tip.QuickTipManager.init();

			Ext.util.Format.decimalSeparator = ',';
			Ext.util.Format.thousandSeparator = '.';

			Ext.create('Ext.container.Viewport',
			{
				border: 0,
				layout: 'border',
				items:
				[

					{
						xtype: 'Header',
						region: 'north',
					},
					{
						xtype: 'panel',
						region: 'center',
						layout: 'border',
						items:
						[
							{
								xtype: 'MList_Sites',
								id: 'MList_Sites',
								region: 'west',
								title: 'Sites',
								width: 350,
								split: true,
								collapsible: true,
								collapsed: false,
								callbackClearRelatedBeforeLoad: function()
								{
									var grid = this;
									myApp.getController('Home').Sites_ClearRelatedBeforeReload(grid);


								},
								listeners:
								{
									selectionchange: function ()
									{
										var grid = this;
										myApp.getController('Home').Sites_OnSelectionChange(grid);


									}
								}
							},
							{
								xtype: 'tabpanel',
								id: 'TabNode',
								layout: 'border',
								region: 'center',
								deferredRender: false,
								title: 'Management',
								cls: 'websitename',
								split:true,
								items:
								[
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Nodes',
										items:
										[
											{
												xtype: 'MList_Nodes',
												id: 'MList_Nodes',
												region: 'west',
												title: 'Site Pages',
												flex: 2,
												split: true,
												callbackClearRelatedBeforeLoad: function()
												{
													var grid = this;
													myApp.getController('Home').Nodes_ClearRelatedBeforeReload(grid);


												},
												listeners:
												{
													selectionchange: function ()
													{
														var grid = this;
														myApp.getController('Home').Nodes_OnSelectionChange(grid);


													}
												}
											},
											{
												xtype: 'panel',
												region: 'center',
												layout: 'border',
												flex: 2,
												split: true,
												items:
												[
													{
														xtype: 'Frm_Node_Details',
														id: 'Frm_Node_Details',
														layout: 'border',
														region: 'center',
														split: true,
													},

												]

											},
										]
									},
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Airlines',
										split: true,
										items:
										[
											{
												xtype: 'MList_Site_Airlines',
												id: 'MList_Site_Airlines2',
												region: 'center',
												layout: 'border',
												title: 'Site Airlines',
												flex: 5,
												split: true,
												callbackClearRelatedBeforeLoad: function()
												{
													var grid = this;
													myApp.getController('Home').Airlines_ClearRelatedBeforeReload(grid);


												},
												listeners:
												{
													selectionchange: function ()
													{
														var grid = this;
														myApp.getController('Home').Airlines_OnSelectionChange(grid);


													}
												}
											},
											{
												xtype: 'Frm_Airlines_Translation',
												id: 'Frm_Airlines_Translation',
												region: 'south',
												layout: 'border',
												title: 'Details',
												height: 430,
												split: true,

											},

										]
									},
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Data with Location',
										split: true,
										items:
										[
											{
												xtype: 'panel',
												border: 0,
												layout: 'border',
												region: 'west',
												split: true,
												flex: 1,
												items:
												[
													{
														xtype: 'MList_Shops',
														id: 'MList_Shops',
														region: 'center',
														title: 'Shops / Restaurants / Duty Free / Business Lounges',
														flex: 2,
														split: true,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').Shops_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').Shops_OnSelectionChange(grid);


															}
														}
													},
													{
														xtype: 'MList_Shops_Locations',
														id: 'MList_Shops_Locations',
														region: 'south',
														title: 'Locations Data',
														height:220,
														split: true,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').ShopsLocations_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').ShopsLocations_OnSelectionChange(grid);


															}
														}
													},
												]
											},
											{
												xtype: 'panel',
												region: 'center',
												layout: 'border',
												flex: 1,
												split: true,
												items:
												[
													{
														xtype: 'Frm_Shop_Details',
														id: 'Frm_Shop_Details',
														title: 'Shop / Restaurant Details',
														layout: 'border',
														region: 'center',
														split: true,
														flex: 2,
													},
													{
														xtype: 'Frm_Shop_Location_Details',
														id: 'Frm_Shop_Location_Details',
														region: 'south',
														title: 'Location Details',
														height:220,
														split: true,
													},

												]

											},
										]
									},
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Content Categories',
										items:
										[

											{
												xtype: 'panel',
												border: 0,
												layout: 'border',
												region: 'center',
												split: true,
												flex: 6,
												items:
												[
													{
														xtype: 'MList_Nodes_Categories',
														id: 'MList_Nodes_Categories',
														title: 'Categories',
														layout: 'border',
														region: 'west',
														split: true,
														flex: 1.5,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').Categories_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').Categories_OnSelectionChange(grid);


															}
														}
													},
													{
														xtype: 'panel',
														border: 0,
														layout: 'border',
														region: 'center',
														split: true,
														flex: 5,
														items:
														[
															// {
																// xtype: 'Frm_Categories_Translation',
																// id: 'Frm_Categories_Translation',
																// title: 'Category Title Translation',
																// layout: 'border',
																// region: 'north',
																// split: true,
																// height: 170,
															// },
															{
																xtype: 'MList_Categories_Records',
																id: 'MList_Categories_Records',
																title: 'Categories Records',
																layout: 'border',
																region: 'center',
																split: true,
																flex: 5,
																callbackClearRelatedBeforeLoad: function()
																{
																	var grid = this;
																	myApp.getController('Home').CategoriesRecords_ClearRelatedBeforeReload(grid);


																},
																listeners:
																{
																	selectionchange: function ()
																	{
																		var grid = this;
																		myApp.getController('Home').CategoriesRecords_OnSelectionChange(grid);


																	}
																}
															},
															{
																xtype: 'MList_Category_Sliders',
																id: 'MList_Content_Category_Sliders',
																title: 'Category SLiders',
																layout: 'border',
																region: 'south',
																split: true,
																flex: 1.5,
															},
														]
													},

												]
											},
											{
												xtype: 'Frm_Categories_Records_Details',
												id: 'Frm_Categories_Records_Details',
												title: 'Records Details',
												layout: 'border',
												region: 'east',
												split: true,
												flex: 4,
											},

										]
									},
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Sliders',
										items:
										[
											{
												xtype: 'panel',
												region: 'west',
												layout: 'border',
												flex: 2,
												split: true,
												items:
												[
													{
														xtype: 'MList_Sliders',
														id: 'MList_Sliders',
														title: 'Site Sliders',
														layout: 'border',
														region: 'north',
														split: true,
														flex: 1,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').Sliders_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').Sliders_OnSelectionChange(grid);


															}
														}
													},
													{
														xtype: 'MList_SliderImages',
														id: 'MList_SliderImages',
														title: 'Slider Images',
														layout: 'border',
														region: 'center',
														split: true,
														flex: 1,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').SliderImages_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').SliderImages_OnSelectionChange(grid);


															}
														}
													},
													{
														xtype: 'Frm_Slider_Images_Translation',
														id: 'Frm_Slider_Images_Translation',
														title: 'Slider Images Translation',
														layout: 'border',
														region: 'south',
														split: true,
														height: 310,
													},

												]
											},
											{
												xtype: 'panel',
												region: 'center',
												layout: 'border',
												flex: 1,
												split: true,
												items:
												[
													{
														xtype: 'MList_Nodes_Media',
														id: 'MList_Nodes_Media4',
														title: 'Nodes',
														layout: 'border',
														region: 'center',
														split: true,
														flex: 5,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').Nodes_Media4_ClearRelatedBeforeReload(grid);

														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').Nodes_Media4_OnSelectionChange(grid);

															}
														}
													},
													{
														xtype: 'MList_Node_Sliders',
														id: 'MList_Node_Sliders',
														title: 'Node SLiders',
														layout: 'border',
														region: 'south',
														split: true,
														flex: 1.5,
													},
												]
											},
											{
												xtype: 'panel',
												region: 'east',
												layout: 'border',
												flex: 1,
												split: true,
												items:
												[
													{
														xtype: 'MList_Categories_Records_Sliders',
														id: 'MList_Categories_Records_Sliders',
														title: 'Categories',
														layout: 'border',
														region: 'center',
														split: true,
														flex: 5,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').CategoriesRecordsSliders_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').CategoriesRecordsSliders_OnSelectionChange(grid);


															}
														}
													},
													{
														xtype: 'MList_Category_Sliders',
														id: 'MList_Category_Sliders',
														title: 'Category SLiders',
														layout: 'border',
														region: 'south',
														split: true,
														flex: 1.5,
													},
												]
											}
										]
									},
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Media',
										items:
										[
											{
												xtype: 'tabpanel',
												layout: 'border',
												region: 'center',
												deferredRender: false,
												split:true,
												items:
												[
													{
														xtype: 'panel',
														border: 0,
														layout: 'border',
														title: 'Media-Images',
														items:
														[
															{
																xtype: 'panel',
																region: 'center',
																layout: 'border',
																flex: 1,
																split: true,
																items:
																[
																	{
																		xtype: 'MList_Images',
																		id: 'MList_Images',
																		title: 'Images',
																		layout: 'border',
																		region: 'center',
																		split: true,
																		flex: 5,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').Images_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').Images_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_Images_Translation',
																		id: 'Frm_Images_Translation',
																		title: 'Image Translation',
																		layout: 'border',
																		region: 'south',
																		split: true,
																		flex: 1.5,
																	},
																]
															},
															{
																xtype: 'panel',
																region: 'east',
																layout: 'border',
																flex: 1,
																split: true,
																items:
																[
																	{
																		xtype: 'MList_Nodes_Media',
																		id: 'MList_Nodes_Media',
																		title: 'Nodes',
																		layout: 'border',
																		region: 'center',
																		split: true,
																		flex: 5,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').Nodes_Media_ClearRelatedBeforeReload(grid);

																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').Nodes_Media_OnSelectionChange(grid);

																			}
																		}
																	},
																	{
																		xtype: 'MList_Node_Images',
																		id: 'MList_Node_Images',
																		title: 'Node Images',
																		layout: 'border',
																		region: 'south',
																		split: true,
																		flex: 1.5,
																	},
																]
															}

														]
													},
													{
														xtype: 'panel',
														border: 0,
														layout: 'border',
														title: 'Media-Videos',
														items:
														[
															{
																xtype: 'panel',
																region: 'center',
																layout: 'border',
																flex: 1,
																split: true,
																items:
																[
																	{
																		xtype: 'MList_Videos',
																		id: 'MList_Videos',
																		title: 'Videos',
																		layout: 'border',
																		region: 'center',
																		split: true,
																		flex: 5,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').Videos_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').Videos_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_Videos_Translation',
																		id: 'Frm_Videos_Translation',
																		title: 'Video Translation',
																		layout: 'border',
																		region: 'south',
																		split: true,
																		flex: 1.5,
																	},
																]
															},
															{
																xtype: 'panel',
																region: 'east',
																layout: 'border',
																flex: 1,
																split: true,
																items:
																[
																	{
																		xtype: 'MList_Nodes_Media',
																		id: 'MList_Nodes_Media2',
																		title: 'Nodes',
																		layout: 'border',
																		region: 'center',
																		split: true,
																		flex: 5,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').Nodes_Media2_ClearRelatedBeforeReload(grid);

																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').Nodes_Media2_OnSelectionChange(grid);

																			}
																		}
																	},
																	{
																		xtype: 'MList_Node_Videos',
																		id: 'MList_Node_Videos',
																		title: 'Node Videos',
																		layout: 'border',
																		region: 'south',
																		split: true,
																		flex: 1.5,
																	},
																]
															}
														]
													},
													{
														xtype: 'panel',
														border: 0,
														layout: 'border',
														title: 'Media-Documents',
														items:
														[
															{
																xtype: 'panel',
																region: 'center',
																layout: 'border',
																flex: 1,
																split: true,
																items:
																[
																	{
																		xtype: 'MList_Documents',
																		id: 'MList_Documents',
																		title: 'Documents',
																		layout: 'border',
																		region: 'center',
																		split: true,
																		flex: 5,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').Documents_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').Documents_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_Documents_Translation',
																		id: 'Frm_Documents_Translation',
																		title: 'Document Translation',
																		layout: 'border',
																		region: 'south',
																		split: true,
																		height: 200,
																	},
																]
															},
															{
																xtype: 'panel',
																region: 'east',
																layout: 'border',
																flex: 1,
																split: true,
																items:
																[
																	{
																		xtype: 'MList_Nodes_Media',
																		id: 'MList_Nodes_Media3',
																		title: 'Nodes',
																		layout: 'border',
																		region: 'center',
																		split: true,
																		flex: 5,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').Nodes_Media3_ClearRelatedBeforeReload(grid);

																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').Nodes_Media3_OnSelectionChange(grid);

																			}
																		}
																	},
																	{
																		xtype: 'MList_Node_Documents',
																		id: 'MList_Node_Documents',
																		title: 'Node Documents',
																		layout: 'border',
																		region: 'south',
																		split: true,
																		flex: 1.5,
																	},
																]
															}

														]
													},

												]
											}
										]
									},
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Galleries',
										items:
										[
											{
												xtype: 'tabpanel',
												layout: 'border',
												region: 'center',
												deferredRender: false,
												split:true,
												items:
												[
													{
														xtype: 'panel',
														border: 0,
														layout: 'border',
														title: 'Images',
														items:
														[
															{
																xtype: 'panel',
																border: 0,
																layout: 'border',
																region: 'west',
																split: true,
																flex: 2,
																items:
																[
																	{
																		xtype: 'MList_Image_Galleries',
																		id: 'MList_Image_Galleries',
																		region: 'center',
																		title: 'Image Gallery List',
																		flex: 1,
																		split: true,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').ImageGalleries_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').ImageGalleries_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_ImageGallery_Translation',
																		id: 'Frm_ImageGallery_Translation',
																		region: 'south',
																		title: 'Image Gallery Translation',
																		height:270,
																		split: true,
																	},
																]
															},
															{
																xtype: 'panel',
																border: 0,
																layout: 'border',
																region: 'center',
																split: true,
																flex: 3,
																items:
																[
																	{
																		xtype: 'MList_Image_Galleries_Data',
																		id: 'MList_Image_Galleries_Data',
																		region: 'center',
																		title: 'Image Gallery Data',
																		flex: 1,
																		split: true,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').ImageGalleriesData_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').ImageGalleriesData_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_ImageGallery_Data_Translation',
																		id: 'Frm_ImageGallery_Data_Translation',
																		region: 'south',
																		title: 'Image Gallery Data Translation',
																		height:270,
																		split: true,
																	},
																]
															},

														]
													},
													{
														xtype: 'panel',
														border: 0,
														layout: 'border',
														title: 'Videos',
														items:
														[
															{
																xtype: 'panel',
																border: 0,
																layout: 'border',
																region: 'west',
																split: true,
																flex: 2,
																items:
																[
																	{
																		xtype: 'MList_Video_Galleries',
																		id: 'MList_Video_Galleries',
																		region: 'center',
																		title: 'Video Gallery List',
																		flex: 1,
																		split: true,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').VideoGalleries_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').VideoGalleries_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_VideoGallery_Translation',
																		id: 'Frm_VideoGallery_Translation',
																		region: 'south',
																		title: 'Video Gallery Translation',
																		height:270,
																		split: true,
																	},
																]
															},
															{
																xtype: 'panel',
																border: 0,
																layout: 'border',
																region: 'center',
																split: true,
																flex: 3,
																items:
																[
																	{
																		xtype: 'MList_Video_Galleries_Data',
																		id: 'MList_Video_Galleries_Data',
																		region: 'center',
																		title: 'Video Gallery Data',
																		flex: 1,
																		split: true,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').VideoGalleriesData_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').VideoGalleriesData_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_VideoGallery_Data_Translation',
																		id: 'Frm_VideoGallery_Data_Translation',
																		region: 'south',
																		title: 'Video Gallery Data Translation',
																		height:270,
																		split: true,
																	},
																]
															},
														]
													},
													{
														xtype: 'panel',
														border: 0,
														layout: 'border',
														title: 'Publications',
														items:
														[
															{
																xtype: 'panel',
																border: 0,
																layout: 'border',
																region: 'west',
																split: true,
																flex: 2,
																items:
																[
																	{
																		xtype: 'MList_Publications_Galleries',
																		id: 'MList_Publications_Galleries',
																		region: 'center',
																		title: 'Publications Gallery List',
																		flex: 1,
																		split: true,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').PublicationsGalleries_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').PublicationsGalleries_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_PublicationsGallery_Translation',
																		id: 'Frm_PublicationsGallery_Translation',
																		region: 'south',
																		title: 'Publications Gallery Translation',
																		height:270,
																		split: true,
																	},
																]
															},
															{
																xtype: 'panel',
																border: 0,
																layout: 'border',
																region: 'center',
																split: true,
																flex: 3,
																items:
																[
																	{
																		xtype: 'MList_Publications_Data',
																		id: 'MList_Publications_Data',
																		region: 'center',
																		title: 'Publications Gallery Data',
																		flex: 1,
																		split: true,
																		callbackClearRelatedBeforeLoad: function()
																		{
																			var grid = this;
																			myApp.getController('Home').PublicationsGalleriesData_ClearRelatedBeforeReload(grid);


																		},
																		listeners:
																		{
																			selectionchange: function ()
																			{
																				var grid = this;
																				myApp.getController('Home').PublicationsGalleriesData_OnSelectionChange(grid);


																			}
																		}
																	},
																	{
																		xtype: 'Frm_PublicationsGallery_Data_Translation',
																		id: 'Frm_PublicationsGallery_Data_Translation',
																		region: 'south',
																		title: 'Publications Gallery Data Translation',
																		height:270,
																		split: true,
																	},
																]
															},
														]
													},

												]
											}
										]
									},
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Related Links',
										split: true,
										items:
										[
											{
												xtype: 'panel',
												region: 'center',
												layout: 'border',
												flex: 1,
												split: true,
												items:
												[
													{
														xtype: 'MList_Related_Links',
														id: 'MList_Related_Links',
														title: 'Related Links',
														layout: 'border',
														region: 'center',
														split: true,
														flex: 5,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').RelatedLinks_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').RelatedLinks_OnSelectionChange(grid);


															}
														}
													},
													{
														xtype: 'Frm_Related_Links_Translation',
														id: 'Frm_Related_Links_Translation',
														title: 'Related Links Translation',
														layout: 'border',
														region: 'south',
														split: true,
														flex: 1.5,
													},
												]
											},
											{
												xtype: 'panel',
												region: 'east',
												layout: 'border',
												flex: 1,
												split: true,
												items:
												[
													{
														xtype: 'MList_Nodes_Media',
														id: 'MList_Nodes_Media5',
														title: 'Nodes',
														layout: 'border',
														region: 'center',
														split: true,
														flex: 5,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').Nodes_Media5_ClearRelatedBeforeReload(grid);

														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').Nodes_Media5_OnSelectionChange(grid);

															}
														}
													},
													{
														xtype: 'MList_Node_RelatedLinks',
														id: 'MList_Node_RelatedLinks',
														title: 'Node Related Links',
														layout: 'border',
														region: 'south',
														split: true,
														flex: 1.5,
													},
												]
											}
										]
									},
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Footer',
										items:
										[
											{
												xtype: 'panel',
												region: 'west',
												layout: 'border',
												flex: 1,
												split: true,
												items:
												[
													{
														xtype: 'MList_Footer',
														id: 'MList_Footer',
														title: 'Footer Headline',
														layout: 'border',
														region: 'center',
														split: true,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').FooterHeadline_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').FooterHeadline_OnSelectionChange(grid);


															}
														}
													},
													{
														xtype: 'Frm_Footer_Translation',
														id: 'Frm_Footer_Translation',
														title: 'Footer Headline Translation',
														layout: 'border',
														region: 'south',
														split: true,
														height: 170
													},
												]
											},
											{
												xtype: 'panel',
												region: 'center',
												layout: 'border',
												flex: 2,
												split: true,
												items:
												[
													{
														xtype: 'MList_Footer_Data',
														id: 'MList_Footer_Data',
														title: 'Footer Data',
														layout: 'border',
														region: 'center',
														split: true,
														callbackClearRelatedBeforeLoad: function()
														{
															var grid = this;
															myApp.getController('Home').FooterData_ClearRelatedBeforeReload(grid);


														},
														listeners:
														{
															selectionchange: function ()
															{
																var grid = this;
																myApp.getController('Home').FooterData_OnSelectionChange(grid);


															}
														}
													},
													{
														xtype: 'Frm_Footer_Data_Translation',
														id: 'Frm_Footer_Data_Translation',
														title: 'Footer Data Translation',
														layout: 'border',
														region: 'south',
														split: true,
														height: 240
													},
												]
											},

										]
									},
                                    {
                                        xtype: 'panel',
                                        border: 0,
                                        layout: 'border',
                                        title: 'Footer Timetable',
                                        items:
                                            [
                                                {
                                                    xtype: 'MList_Footer_Timetable',
                                                    id: 'MList_Footer_Timetable',
                                                    title: 'Timetable',
                                                    layout: 'border',
                                                    region: 'center',
                                                    split: true,
                                                    flex: 3,
                                                    callbackClearRelatedBeforeLoad: function()
                                                    {
                                                        var grid = this;
                                                        myApp.getController('Home').FooterTimetable_ClearRelatedBeforeReload(grid);


                                                    },
                                                    listeners:
                                                        {
                                                            selectionchange: function ()
                                                            {
                                                                var grid = this;
                                                                myApp.getController('Home').FooterTimetable_OnSelectionChange(grid);


                                                            }
                                                        }
                                                },
                                                {
                                                    xtype: 'Frm_FooterTimetable_Translation',
                                                    id: 'Frm_FooterTimetable_Translation',
                                                    title: 'Details',
                                                    layout: 'border',
                                                    region: 'east',
                                                    split: true,
                                                    flex: 4,
                                                },

                                            ]
                                    },
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Alerts/Notifications',
										items:
										[
											{
												xtype: 'MList_Alerts',
												id: 'MList_Alerts',
												title: 'Alerts',
												layout: 'border',
												region: 'center',
												split: true,
												flex: 3,
												callbackClearRelatedBeforeLoad: function()
												{
													var grid = this;
													myApp.getController('Home').Alerts_ClearRelatedBeforeReload(grid);


												},
												listeners:
												{
													selectionchange: function ()
													{
														var grid = this;
														myApp.getController('Home').Alerts_OnSelectionChange(grid);


													}
												}
											},
											{
												xtype: 'Frm_Alerts_Translation',
												id: 'Frm_Alerts_Translation',
												title: 'Details',
												layout: 'border',
												region: 'east',
												split: true,
												flex: 4,
											},

										]
									},
                                    {
                                        xtype: 'panel',
                                        border: 0,
                                        layout: 'border',
                                        title: 'Contact Form',
                                        items:
                                            [

                                                {
                                                    xtype: 'panel',
                                                    region: 'west',
                                                    layout: 'border',
                                                    flex: 1,
                                                    split: true,
                                                    items:
                                                    [
                                                        {
                                                            xtype: 'MList_Subject',
                                                            id: 'MList_Subject',
                                                            title: 'Subject',
                                                            layout: 'border',
                                                            region: 'center',
                                                            split: true,
                                                            callbackClearRelatedBeforeLoad: function()
                                                            {
                                                                var grid = this;
                                                                myApp.getController('Home').Subject_ClearRelatedBeforeReload(grid);


                                                            },
                                                            listeners:
                                                            {
                                                                selectionchange: function ()
                                                                {
                                                                    var grid = this;
                                                                    myApp.getController('Home').Subject_OnSelectionChange(grid);


                                                                }
                                                            }
                                                        },
                                                        {
                                                            xtype: 'Frm_Subject_Translation',
                                                            id: 'Frm_Subject_Translation',
                                                            title: 'Subject Translation',
                                                            layout: 'border',
                                                            region: 'south',
                                                            split: true,
                                                            height: 250
                                                        },
                                                    ]
                                                },
                                                {
                                                    xtype: 'panel',
                                                    region: 'center',
                                                    layout: 'border',
                                                    flex: 1,
                                                    split: true,
                                                    items:
                                                    [
                                                        {
                                                            xtype: 'MList_Sub_Subject',
                                                            id: 'MList_Sub_Subject',
                                                            title: 'Sub Subject',
                                                            layout: 'border',
                                                            region: 'center',
                                                            split: true,
                                                            callbackClearRelatedBeforeLoad: function()
                                                            {
                                                                var grid = this;
                                                                myApp.getController('Home').SubSubject_ClearRelatedBeforeReload(grid);


                                                            },
                                                            listeners:
                                                            {
                                                                selectionchange: function ()
                                                                {
                                                                    var grid = this;
                                                                    myApp.getController('Home').SubSubject_OnSelectionChange(grid);


                                                                }
                                                            }
                                                        },
                                                        {
                                                            xtype: 'Frm_Sub_Subject_Translation',
                                                            id: 'Frm_Sub_Subject_Translation',
                                                            title: 'Sub Subject Translation',
                                                            layout: 'border',
                                                            region: 'south',
                                                            split: true,
                                                            height: 170
                                                        },
                                                    ]
                                                },
                                                {
                                                    xtype: 'panel',
                                                    region: 'east',
                                                    layout: 'border',
                                                    flex: 1,
                                                    split: true,
                                                    items:
                                                        [
                                                            {
                                                                xtype: 'Frm_Subject_Answer_Translation',
                                                                id: 'Frm_Subject_Answer_Translation',
                                                                // xtype: 'panel',
                                                                title: 'Answer Translation',
                                                                layout: 'border',
                                                                region: 'center',
                                                                split: true,
                                                                height: 500
                                                            },
                                                        ]
                                                },

                                            ]
                                    },
									{
										xtype: 'panel',
										border: 0,
										layout: 'border',
										title: 'Disaster Mode',
										items:
										[

											{
												xtype: 'MList_Disaster_Mode',
												id: 'MList_Disaster_Mode',
												title: 'Disaster Mode List',
												layout: 'border',
												region: 'center',
												split: true,
												flex: 3,
												callbackClearRelatedBeforeLoad: function()
												{
													var grid = this;
													myApp.getController('Home').Disaster_ClearRelatedBeforeReload(grid);


												},
												listeners:
												{
													selectionchange: function ()
													{
														var grid = this;
														myApp.getController('Home').Disaster_OnSelectionChange(grid);


													}
												}
											},
											{
												xtype: 'Frm_Disaster_Mode_Translation',
												id: 'Frm_Disaster_Mode_Translation',
												title: 'Details',
												layout: 'border',
												region: 'east',
												split: true,
												flex: 4,
											},

										]
									}

								],
							}

						]
					}

				]
			});

			//Load Sites for the first time
			var grid = Ext.ComponentManager.get('MList_Sites');
			grid.loadData();

		}
	});
	</script>
</head>
<body>


</body>
</html>
