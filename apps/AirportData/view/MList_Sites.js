﻿// Require Popup Form of Sites (Wnd for New - Edit)
Ext.Loader.setPath('Home', "apps/Home");
Ext.require([ 'Home.view.Wnd_NewEditSites']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('AirportData.view.MList_Sites' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Sites',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
	
		// Toolbar  : Add New, Edit, Delete
		{
			xtype: 'toolbar',
			dock: 'top',
			layout: { type: 'hbox' },
			items: 
			[	

		
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind = '';
		this.filterShowAll = 0;		
		this.maxRecords = 100;
				
		this.fieldNameID = 'SiteID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				
			}					
		};		
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'SiteID', type: 'number', defaultValue: 0 }
				,{name: 'SiteDomainName', type: 'string'}
			],			
			proxy: 
			{
				type: 'ajax',
				url: '', 
				method : 'POST',
				timeout : 60000,						
				headers : { 'Content-Type' : 'application/json' },
				reader: {
					type: 'json',
					root: 'data'
				},
				simpleSortMode: true,				
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: true,
			},
			items: 
			[
				{header: ' Domain', dataIndex: 'SiteDomainName',  flex: 100/100}
			]
		};
		
	

		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'AirportData',
			appMethod: 'get_SiteList',
			maxRecords: me.maxRecords,

		};
		
		return params;
	},
	

	
	
	
		
	
	 
	
});





