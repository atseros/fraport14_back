﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('AirportData.view.MList_Airports' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Airports',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
	
		// Toolbar  : Add New, Edit, Delete
		{
			xtype: 'toolbar',
			dock: 'top',
			layout: { type: 'hbox' },
			items: 
			[	
				{
					xtype: 'container', 
					width: 65, 
					margin: '2 2 2 5',
					html:'<b>AIRPORTS</b>',
				},
				{
					xtype: 'button', 
					text: '',
					tooltip: 'search',
					iconCls: 'fa fa-search',
					scale: 'small',
					listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				},
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind = '';
		this.SiteID = 0;		
		this.LangID = 1;		
				
		this.fieldNameID = 'AipID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				
			}					
		};		
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'AipID', type: 'number' }
				,{name: 'Aip_Code', type: 'string' }
				,{name: 'Aip_ICAO', type: 'string' }
				,{name: 'AipLng_Region', type: 'string'}
				,{name: 'AipLng_Regionorg', type: 'string'}
				,{name: 'AipLng_NameShort', type: 'string'}
				,{name: 'AipLng_Name', type: 'string'}
				,{name: 'AipLng_Land', type: 'string'}
			],			
			proxy: 
			{
				type: 'ajax',
				url: '', 
				method : 'POST',
				timeout : 60000,						
				headers : { 'Content-Type' : 'application/json' },
				reader: {
					type: 'json',
					root: 'data'
				},
				simpleSortMode: true,				
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: true,
			},
			items: 
			[
				 {header: ' ID', dataIndex: 'AipID',  width: 60 }
				,{header: ' Last Updated',  width: 120}
				,{header: ' Code', dataIndex: 'Aip_Code',  width: 70}
				,{header: ' ICAO', dataIndex: 'Aip_ICAO',  width: 70}
				,{header: ' Region', dataIndex: 'AipLng_Region', width: 60}
				,{header: ' Regionorg', dataIndex: 'AipLng_Regionorg',  width: 100}
				,{header: ' NameShort', dataIndex: 'AipLng_NameShort',  width: 100}
				,{header: ' Name', dataIndex: 'AipLng_Name',  flex: 1}
				,{header: ' Land', dataIndex: 'AipLng_Land',  flex: 1}
			]
		};
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'AirportData',
			appMethod: 'get_AirportsList',
			filterStrToFind:	me.filterStrToFind,
			SiteID:	me.SiteID,
			LangID:	me.LangID,
		};
		
		return params;
	},

	
});





