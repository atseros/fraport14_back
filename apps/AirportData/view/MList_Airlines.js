﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('AirportData.view.MList_Airlines' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Airlines',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
	
		// Toolbar  : Add New, Edit, Delete
		{
			xtype: 'toolbar',
			dock: 'top',
			layout: { type: 'hbox' },
			items: 
			[	
				{
					xtype: 'container', 
					width: 65, 
					margin: '2 2 2 5',
					html:'<b>AIRLINES</b>',
				},
				{
					xtype: 'button', 
					text: '',
					tooltip: 'search',
					iconCls: 'fa fa-search',
					scale: 'small',
					listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				},
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind = '';
		this.SiteID = 0;		
		this.LangID = 1;		
				
		this.fieldNameID = 'ArlID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				
			}					
		};		
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'ArlID', type: 'number', defaultValue: 0 }				
				,{name: 'Arl_Code', type: 'string' }
				,{name: 'ArlLng_Name', type: 'string' }
			],			
			proxy: 
			{
				type: 'ajax',
				url: '', 
				method : 'POST',
				timeout : 60000,						
				headers : { 'Content-Type' : 'application/json' },
				reader: {
					type: 'json',
					root: 'data'
				},
				simpleSortMode: true,				
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: true,
			},
			items: 
			[
				 {header: ' ID', dataIndex: 'ArlID',  width: 60}
				,{header: ' Last Updated',  width: 120}
				,{header: ' Code', dataIndex: 'Arl_Code',  width: 90}
				,{header: ' Name', dataIndex: 'ArlLng_Name',  flex: 100/100}
				
			]
		};
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'AirportData',
			appMethod: 'get_AirlinesList',
			filterStrToFind:	me.filterStrToFind,
			SiteID:	me.SiteID,
			LangID:	me.LangID,
		};
		
		return params;
	},

	
});





