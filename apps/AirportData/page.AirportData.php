<?php
//If it is not called at root appMain.php do  nothing
if(!isset($appFrw)) exit();


require_once(realpath(__DIR__."/../../db")."/class.DB_sys_usr.php");

$UsrDetails = DB_sys_usr::get_UsrDetails($appFrw, $appFrw->UsrID);
//if($UsrDetails['UsrRole'] == 1)	exit();

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="PRAGMA" content="NO-CACHE">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Airport Data</title>
	<script type="text/javascript" src="libjs/ext-4/ext-all.js"></script>
	<link rel="stylesheet" type="text/css" href="libjs/ext-4/resources/css/ext-all-gray.css">
	<link rel="stylesheet" type="text/css" href="resources/css/header.css?dc=<?php echo microtime(true)?>">
	<link rel="stylesheet" type="text/css" href="resources/css/general.css?dc=<?php echo microtime(true)?>">
	<link rel="stylesheet" type="text/css" href="resources/css/icon.css">
	<link rel="shortcut icon" href="resources/images/favicon/favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome-4.7.0/css/font-awesome.min.css">
	
	<script type="text/javascript" src="libjs/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="libjs/TinyMCETextArea/TinyMCETextArea.js"></script>
	<script type="text/javascript" src="libjs/TinyMCETextArea/My_TinyMCETextArea.js"></script>
	
	<?php 

	$menuCurrentPage = 'Airport Data'; 
	include('apps/Include/MainMenu.php'); 

	$header_username = $UsrDetails['UsrFullName']; 
	include('apps/Include/Header.php'); 

	?>
	
	<script type="text/javascript">
	Ext.Loader.setConfig( {enabled: true, disableCaching: true} );
	
	Ext.Loader.setPath('Ext.ux', "libjs/ext-4.ux");
	
	Ext.require([
					'Ext.form.Panel',
					'Ext.ux.form.MultiSelect',
					'Ext.ux.form.ItemSelector',
					'Ext.tip.QuickTipManager',
					'Ext.ux.ajax.JsonSimlet',
					'Ext.ux.ajax.SimManager',
					'Ext.ux.grid.FiltersFeature',
					'Ext.ux.grid.menu.ListMenu',
					'Ext.ux.grid.menu.RangeMenu',
					'Ext.ux.grid.filter.BooleanFilter',
					'Ext.ux.grid.filter.DateFilter',
					'Ext.ux.grid.filter.ListFilter',
					'Ext.ux.grid.filter.NumericFilter',
					'Ext.ux.grid.filter.StringFilter',
					'Ext.ux.grid.images.*',
					'Ext.ux.CheckColumn',
					'Ext.selection.CellModel',
					'Ext.grid.plugin.CellEditing'
	]);
	
	Ext.Loader.setPath('FileMgr', 'apps/FileMgr');
	Ext.require([ 'FileMgr.view.ExtFileList']);
	Ext.require([ 'FileMgr.view.ShowImageFile']);

	var AirportData = Ext.application({
		name: 'AirportData',
		appFolder: 'apps/AirportData',
	
		controllers: ['AirportData'],
	
		launch: function()
		{				
			var myApp = this;
			
			Ext.tip.QuickTipManager.init();
				
			Ext.util.Format.decimalSeparator = ',';
			Ext.util.Format.thousandSeparator = '.';
				
			Ext.create('Ext.container.Viewport', 
			{
				border: 0,
				layout: 'border',
				items: 
				[

					{
						xtype: 'Header',
						region: 'north',
					},
					{
						xtype: 'panel',
						region: 'center',
						layout: 'border',
						items:
						[
							{
								xtype: 'MList_Sites',
								id: 'MList_Sites',
								region: 'west',
								layout: 'border',
								title: 'Sites - Airports',
								width: 250,
								split: true,
								collapsible: true,
								collapsed: false,
								callbackClearRelatedBeforeLoad: function()
								{
									var grid = this;
									myApp.getController('AirportData').Sites_ClearRelatedBeforeReload(grid);
									
							
								},	
								listeners: 
								{
									selectionchange: function () 
									{
										var grid = this;
										myApp.getController('AirportData').Sites_OnSelectionChange(grid);
										
						
									}									
								}
							},							
							{
								xtype: 'panel',
								region: 'center',
								layout: 'border',
								split: true,
								items:
								[
									{
										xtype: 'panel',
										region: 'north',
										layout: 'border',
										height: 30,
										items:
										[
											{
												xtype: 'container',
												region: 'west',
												layout: 'border',
												margin: '0 5 0 0',
												width: 60,
												html: '<div style="margin-top:5px; font-weight:bold;">Language:</div> '
											},
											{
												xtype: 'container',
												region: 'west',
												layout: 'border',
												width: 150,
												items:
												[
													{
														xtype: 'combo',    
														fieldLabel: '',														
														width:150,
														value:"1",
														margin: '0 0 0 5',
														store:
														{
															fields: 
															[
																{name: 'id', type: 'string'},
																{name: 'Descr', type: 'string'}
															],
															data: 
															[
																{ id: 1, Descr: 'English'}
																,{ id: 2, Descr: 'Greek'}
																,{ id: 3, Descr: 'Deutch'}
																,{ id: 4, Descr: 'Russian'}
															],
															autoLoad: true
														},
														queryMode:'local',
														valueField: 'id',
														displayField: 'Descr',
														listeners:
														{
															'change': function(field, newValue)
															{
																gridAirports = Ext.ComponentManager.get('MList_Airports');	
																gridAirports.LangID = newValue;
																gridAirports.loadData();
																
																gridAirlines = Ext.ComponentManager.get('MList_Airlines');	
																gridAirlines.LangID = newValue;
																gridAirlines.loadData();
															}
														}
													}
												]
											}											
										]
									},
									{
										xtype: 'tabpanel',
										region: 'center',
										title: '',
										defferedRender: false,
										split: true,
										items:
										[
											{
												xtype: 'panel',
												region: 'center',
												layout: 'border',
												title: 'Airports - Airlines',
												items:
												[
													{
														xtype: 'MList_Airports',
														id: 'MList_Airports',
														layout: 'border',
														region: 'west',
														flex: 2,
														split: true,
													},
													{
														xtype: 'MList_Airlines',
														id: 'MList_Airlines',
														layout: 'border',
														region: 'center',
														flex: 1,
														split: true,
													},
												]
											},
											{
												xtype: 'panel',
												region: 'center',
												title: 'Arrivals',
											},
											{
												xtype: 'panel',
												region: 'center',
												title: 'Departures',
											},
										]
									}
								]
							},
						]
					}
					
				]
			});
			
			// Load for the first time
			var grid = Ext.ComponentManager.get('MList_Sites');		
			grid.loadData();
			
		}
	});
	</script>
</head>
<body>
	
	
</body>
</html>
