﻿Ext.define('AirportData.controller.AirportData', {
    extend: 'Ext.app.Controller',
    alias: 'widget.AirportDataControler',

    models: [],
    stores: [],
    views: [
			'MList_Sites',
			'MList_Airports',
			'MList_Airlines',
	],
	
	
	/*******************************************************************
	* Sites_ClearRelatedBeforeReload
	********************************************************************/
	Sites_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear 
		Ext.ComponentManager.get('MList_Airports').clearData();
		
		Ext.ComponentManager.get('MList_Airlines').clearData();
		
	},
	
	/*******************************************************************
	* Sites_OnSelectionChange
	********************************************************************/
	Sites_OnSelectionChange: function(grid)
	{
		SiteID = grid.getSelectedRecordID();
		
		//Load 
		Ext.ComponentManager.get('MList_Airports').SiteID = SiteID;
		Ext.ComponentManager.get('MList_Airports').loadData();
		
		//Load 
		Ext.ComponentManager.get('MList_Airlines').SiteID = SiteID;
		Ext.ComponentManager.get('MList_Airlines').loadData();
		
	},		
	
    
});