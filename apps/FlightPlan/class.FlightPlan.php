<?php

require_once(realpath(__DIR__."/../../db")."/class.DB_sys_seasonal_flight_plan.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_seasonal_flight_plan_version.php");

/*********************************************************************************************
* CLASS FlightPlan
*
* DESCRIPTION: 
*	Class that handles requests from FlightPlan page 
*
*********************************************************************************************/
class FlightPlan
{
	private $appFrw;
	
	/****************************************************************************************
	* FlightPlan::CONSTRUCTOR
	****************************************************************************************/
	function FlightPlan($appFrw)
	{
		$this->appFrw = $appFrw;
	}
	
	
	/*
		SEASONAL FLIGHT PLAN VERSION
	*/
	
	/****************************************************************************************
	* FlightPlan::get_SeasonalFlightPlanVersion_NewRecordDefValues
	****************************************************************************************/
	function get_SeasonalFlightPlanVersion_NewRecordDefValues()
	{
		$params = array();
		
		$params["SiteID"] = $_REQUEST["SiteID"];
		
		$results = DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* FlightPlan::InsertSeasonalFlightPlanVersionRecord
	****************************************************************************************/
	function InsertSeasonalFlightPlanVersionRecord()
	{
		$params = array();
		
		$params["SfpvID"] = $_REQUEST["SfpvID"];
		$params["SfpvSiteID"] = $_REQUEST["SfpvSiteID"];
		$params["SfpvStatus"] = $_REQUEST["SfpvStatus"];
		$params["SfpvTitle"] = $_REQUEST["SfpvTitle"];
		
		$results = DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* FlightPlan::get_SeasonalFlightPlanVersionRecord
	****************************************************************************************/
	function get_SeasonalFlightPlanVersionRecord()
	{
		$params["SfpvID"] = $_REQUEST["SfpvID"];
						
		$results = DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* FlightPlan::UpdateSeasonalFlightPlanVersionRecord
	****************************************************************************************/
	function UpdateSeasonalFlightPlanVersionRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['SfpvID'])) { $params['SfpvID'] = $_REQUEST['SfpvID']; }
		if(isset($_REQUEST['SfpvSiteID'])) { $params['SfpvSiteID'] = $_REQUEST['SfpvSiteID']; }
		if(isset($_REQUEST['SfpvStatus'])) { $params['SfpvStatus'] = $_REQUEST['SfpvStatus']; }
		if(isset($_REQUEST['SfpvTitle'])) { $params['SfpvTitle'] = $_REQUEST['SfpvTitle']; }
		
		$results = DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* FlightPlan::DeleteSeasonalFlightPlanVersionRecord
	****************************************************************************************/
	function DeleteSeasonalFlightPlanVersionRecord()
	{
		$params = array();
		
		$params["SfpvID"] = $_REQUEST["SfpvID"];
		
		$results = DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* FlightPlan::getSeasonalFlightPlanVersionList
	****************************************************************************************/
	function getSeasonalFlightPlanVersionList()
	{
		$params = array();
		
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		
		$results = DB_sys_seasonal_flight_plan_version::sys_seasonal_flight_plan_version_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		FLIGHT PLAN CSV 
	*/
	
	/****************************************************************************************
	* FlightPlan::upload_flight_csv
	****************************************************************************************/
	function upload_flight_csv()
	{
		$params = array();
		
		$params['SfpvID'] 	= isset($_REQUEST['SfpvID']) ? $_REQUEST['SfpvID'] : 0;
		
		
		if(!empty($_FILES["FlightCsv"]["tmp_name"]))
		{
		
			$params["FlightCsv"] = $_FILES;
		
		}	
		
		$results = DB_sys_seasonal_flight_plan::sys_seasonal_flight_plan_insert_csv($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode($results) ;
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/****************************************************************************************
	* FlightPlan::getSeasonalFlightPlanVersionList
	****************************************************************************************/
	function getSeasonalFlightPlanList()
	{
		$params = array();
		
		$params['SfpvID'] 	= isset($_REQUEST['SfpvID']) ? $_REQUEST['SfpvID'] : 0;
		
		$results = DB_sys_seasonal_flight_plan::sys_seasonal_flight_plan_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}	
	
}
?>