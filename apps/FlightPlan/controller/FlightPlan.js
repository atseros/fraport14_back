﻿Ext.define('FlightPlan.controller.FlightPlan', {
    extend: 'Ext.app.Controller',
    alias: 'widget.FlightPlanControler',

    models: [],
    stores: [],
    views: [
				
				'MList_SeasonalFlightPlanVersion'	
				,'MList_SeasonalFLightPlan'	
		
			],
	
	/*******************************************************************
	* MList_Sites_ClearRelatedBeforeReload
	********************************************************************/
	MList_Sites_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear Airlines Translation
		Ext.ComponentManager.get('MList_SeasonalFlightPlanVersion').clearData();
		
	},
	
	/*******************************************************************
	* MList_Sites_OnSelectionChange
	********************************************************************/
	MList_Sites_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_SiteID = selectedRecord.get('SiteID');
		
				// Load Airlines Translation
				Ext.ComponentManager.get('MList_SeasonalFlightPlanVersion').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_SeasonalFlightPlanVersion').loadData();
				
			}	
		}		
	},
	
	/*******************************************************************
	* MList_SeasonalFlightPlanVersion_ClearRelatedBeforeReload
	********************************************************************/
	MList_SeasonalFlightPlanVersion_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear Airlines Translation
		Ext.ComponentManager.get('MList_SeasonalFLightPlan').clearData();
		
	},
	
	/*******************************************************************
	* MList_SeasonalFlightPlanVersion_OnSelectionChange
	********************************************************************/
	MList_SeasonalFlightPlanVersion_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_SfpvID = selectedRecord.get('SfpvID');
		
				// Load Airlines Translation
				Ext.ComponentManager.get('MList_SeasonalFLightPlan').SfpvID = selected_SfpvID;
				Ext.ComponentManager.get('MList_SeasonalFLightPlan').loadData();
				
			}	
		}		
	},	
	
});