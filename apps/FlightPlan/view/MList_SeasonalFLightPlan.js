﻿// // Require Popup Form of FLight Plan (Wnd for New Record)
Ext.require([ 'FlightPlan.view.Wnd_UploadFLightPlan']);


Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('FlightPlan.view.MList_SeasonalFLightPlan' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_SeasonalFLightPlan',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
				{
					xtype: 'tbfill'
				},
				{
					 xtype:		'button'
					,text:		'Upload CSV'
					,iconCls: 	'fa fa-upload'
					,margin:		'0 0 0 0'
					,handler:	function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.UploadCSV();
					}				
				},	
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		//this.fieldNameID 		= 'SfpvID';
		
		this.SfpvID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				//return value.get('SfpvID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				{name: 'SfpArrivalDeparture', type: 'string' }
				,{name: 'SfpFromDate', type: 'string' }
				,{name: 'SfpToDate', type: 'string' }
				,{name: 'SfpDays', type: 'string' }
				,{name: 'SfpFlightNumber', type: 'string' }
				,{name: 'SfpTime', type: 'string' }
				,{name: 'SfpDestination', type: 'string'}
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
											
				{ header: 'Arrival / Departure', dataIndex: 'SfpArrivalDeparture', flex: 50/100,},
				{ header: 'From Date', dataIndex: 'SfpFromDate', flex: 50/100,},
				{ header: 'To Date', dataIndex: 'SfpToDate', flex: 50/100,},
				{ header: 'Days', dataIndex: 'SfpDays', flex: 50/100,},
				{ header: 'Flight Number', dataIndex: 'SfpFlightNumber', flex: 50/100,},
				{ header: 'Time', dataIndex: 'SfpTime', flex: 50/100,},
				{ header: 'Destination', dataIndex: 'SfpDestination', flex: 50/100,},
				
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'FlightPlan',
			appMethod			: 'getSeasonalFlightPlanList',
			SfpvID				: me.SfpvID,
		};
		
		return params;
	},


	/**************************************************************************************************
	* UploadCSV
	*
	* Button handler for UploadCSV
	*
	***************************************************************************************************/
	UploadCSV: function()
	{
		var thisgrid	= this;
		
		var SfpvID 		= thisgrid.SfpvID;
		
		if (SfpvID == 0)
		{
			Ext.Msg.alert('Attention!', 'Please select a version first');
			return;
		}
		
		var x = new FlightPlan.view.Wnd_UploadFLightPlan;
		x.SfpvID = thisgrid.SfpvID;	
		x.callbackRefresh = function( ){ thisgrid.loadData(); };  
		x.show(this);
		
		
	},
});