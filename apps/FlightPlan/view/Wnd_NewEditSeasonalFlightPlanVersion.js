﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('FlightPlan.view.Wnd_NewEditSeasonalFlightPlanVersion', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditSeasonalFlightPlanVersion',
		
	layout: 'border',
	title: 'Version Data',
    height: 140,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[
				// Hidden field:  SfpvID
				{
					xtype: 'hiddenfield',
					name: 'SfpvID',
					fieldLabel: 'SfpvID',
				},
				// Hidden field:  SfpvSiteID
				{
					xtype: 'hiddenfield',
					name: 'SfpvSiteID',
					fieldLabel: 'SfpvSiteID',
				},
				// Combo field: SfpvStatus
				{
					xtype: 'combo',    
					name: 'SfpvStatus',
					forceSelection: true,
					fieldLabel: 'Status',
					cls: 'readonly_combos',
					editable: false,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},				
				// Text field:  SfpvTitle
				{
					xtype: 'textfield',
					name: 'SfpvTitle',
					fieldLabel: 'Title',
				},				
								
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
		
		this.SiteID = 0;			
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'FlightPlan',
			appMethod		: 'get_SeasonalFlightPlanVersion_NewRecordDefValues',
			SiteID			:this.SiteID,
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'FlightPlan',
			appMethod		: 'get_SeasonalFlightPlanVersionRecord',
			SfpvID			: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'FlightPlan',
			appMethod:				'InsertSeasonalFlightPlanVersionRecord',
			SfpvID:						values.SfpvID,
			SfpvSiteID:					values.SfpvSiteID,
			SfpvStatus:				values.SfpvStatus,
			SfpvTitle:					values.SfpvTitle,
			
		};

		return params;
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'FlightPlan',
			appMethod:				'UpdateSeasonalFlightPlanVersionRecord',
			SfpvID:						values.SfpvID,
			SfpvSiteID:					values.SfpvSiteID,
			SfpvStatus:				values.SfpvStatus,
			SfpvTitle:					values.SfpvTitle,

		};
		
		return params;
		
	},
	
});