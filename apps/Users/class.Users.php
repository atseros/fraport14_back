<?php


/*********************************************************************************************
* CLASS Users
*
* DESCRIPTION: 
*	Class that handles requests from Users page 
*
*********************************************************************************************/
class Users
{
	private $appFrw;
	
	/****************************************************************************************
	* Users::CONSTRUCTOR
	****************************************************************************************/
	function Users($appFrw)
	{
		$this->appFrw = $appFrw;
	}
	
		/****************************************************************************************
	* Users::get_Site_NewRecordDefValues
	****************************************************************************************/
	function get_Site_NewRecordDefValues()
	{
		$params = array();
		
		$results = DB_sys_usr::sys_usr_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Users::InsertUserRecord
	****************************************************************************************/
	function InsertUserRecord()
	{
		$params = array();
		
		$params["UsrID"] = $_REQUEST["UsrID"];
		$params["UsrFirstName"] = $_REQUEST["UsrFirstName"];
		$params["UsrLastName"] = $_REQUEST["UsrLastName"];
		$params["UsrLogin"] = $_REQUEST["UsrLogin"];
		$params["UsrPassword"] = $_REQUEST["UsrPassword"];
		$params["UsrRole"] = $_REQUEST["UsrRole"];
		$params["UsrStatus"] = $_REQUEST["UsrStatus"];
		
		$results = DB_sys_usr::sys_usr_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Users::getUserRecord
	****************************************************************************************/
	function getUserRecord()
	{
		$params["UsrID"] = $_REQUEST["UsrID"];
						
		$results = DB_sys_usr::sys_usr_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Users::UpdateUserRecord
	****************************************************************************************/
	function UpdateUserRecord()
	{
		
		
		$params = array();
		
		if(isset($_REQUEST['UsrID'])) { $params['UsrID'] = $_REQUEST['UsrID']; }
		if(isset($_REQUEST['UsrFirstName'])) { $params['UsrFirstName'] = $_REQUEST['UsrFirstName']; }
		if(isset($_REQUEST['UsrLastName'])) { $params['UsrLastName'] = $_REQUEST['UsrLastName']; }
		if(isset($_REQUEST['UsrLogin'])) { $params['UsrLogin'] = $_REQUEST['UsrLogin']; }
		if(isset($_REQUEST['UsrPassword'])) { $params['UsrPassword'] = $_REQUEST['UsrPassword']; }
		if(isset($_REQUEST['UsrRole'])) { $params['UsrRole'] = $_REQUEST['UsrRole']; }
		if(isset($_REQUEST['UsrStatus'])) { $params['UsrStatus'] = $_REQUEST['UsrStatus']; }
		
		$results = DB_sys_usr::sys_usr_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Users::DeleteUserRecord
	****************************************************************************************/
	function DeleteUserRecord()
	{
		$params = array();
		
		$params["UsrID"] = $_REQUEST["UsrID"];
		
		$results = DB_sys_usr::sys_usr_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Users::get_List
	****************************************************************************************/
	function get_List()
	{
		$params = array();
		
		$params['maxRecords'] 		= isset($_REQUEST['maxRecords']) ? $_REQUEST['maxRecords'] : 0;
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['filterShowAll'] 	= isset($_REQUEST['filterShowAll']) ? $_REQUEST['filterShowAll'] : 0;
		
		$results = DB_sys_usr::sys_usr_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	
	

	
}
?>