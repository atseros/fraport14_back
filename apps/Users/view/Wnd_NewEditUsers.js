﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Users.view.Wnd_NewEditUsers', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditUsers',
		
	layout: 'border',
	title: 'User Data',
    height: 250,
    width: 450,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 100,
				region :'north',
           },
			items: 
			[				
				// Hidden field:  UsrID
				{
					xtype: 'hiddenfield',
					name: 'UsrID',
					fieldLabel: 'UsrID',
					readOnly: true,
					cls: 'readonly'
				},
				// Text field:  UsrFirstName
				{
					xtype: 'textfield',
					name: 'UsrFirstName',
					fieldLabel: 'First Name',
				},
				// Text field:  UsrLastName
				{
					xtype: 'textfield',
					name: 'UsrLastName',
					fieldLabel: 'Last Name',
				},
				// Text field:  UsrLogin
				{
					xtype: 'textfield',
					name: 'UsrLogin',
					fieldLabel: 'Username',
				},
				// Text field:  UsrPassword
				{
					xtype: 'textfield',
					name: 'UsrPassword',
					fieldLabel: 'Password',
					inputType: 'password',
				},
				// Combo field:  UsrRole		
				{
					xtype: 'combo',    
					name: 'UsrRole',
					fieldLabel: 'Role',
					labelWidth: 100,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							// { id: 1, Descr: 'User'},
							// { id: 2, Descr: 'Site Admin'},
							{ id: 3, Descr: 'Global Admin'}
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},
				// Combo field:  UsrStatus		
				{
					xtype: 'combo',    
					name: 'UsrStatus',
					fieldLabel: 'Status',
					labelWidth: 100,
					store:
					{
						fields: 
						[
							{name: 'id', type: 'int'},
							{name: 'Descr', type: 'string'}
						],
						data: 
						[
							{ id: 1, Descr: 'Active'},
							{ id: 2, Descr: 'Inactive'},
						],
						autoLoad: true
					},
					queryMode:'local',
					valueField: 'id',
					displayField: 'Descr'
				},
				/*
				{
					region: 'center',
					xtype: 'my_tinymce_textarea',
					name: 'UsrComments',
					
				}
				*/
			]
        }        
    ],
	

    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
		
		
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Users',
			appMethod	: 'get_Site_NewRecordDefValues',
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
/*
		var tinymce_editor1 = this.query('my_tinymce_textarea[name=UsrComments]')[0];
		var thisf = me.query('form')[0];
		var id = thisf.getForm().getValues().UsrID;

		tinymce_editor1.submitUrl='appAjax.php?appPage=Users&appMethod=handleTinyMCE_UploadImage&table_name=page_art&id=' + id;		
*/
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Users',
			appMethod	: 'getUserRecord',
			UsrID		: this.editRecID
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
/*
		tinymce_editor1 = this.query('my_tinymce_textarea[name=UsrComments]')[0];

		tinymce_editor1.submitUrl='appAjax.php?appPage=Users&appMethod=handleTinyMCE_UploadImage&table_name=page_art&id=' + this.editRecID;		
*/
	},
	
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		var form = this.query('form')[0];
				
		var values = form.getValues();
		
		var params = 
		{
			appPage:					'Users',
			appMethod:					'InsertUserRecord',
			UsrID:						values.UsrID,
			UsrFirstName:				values.UsrFirstName,
			UsrLastName:				values.UsrLastName,
			UsrLogin:					values.UsrLogin,
			UsrPassword:				values.UsrPassword,
			UsrRole:					values.UsrRole,
			UsrStatus:					values.UsrStatus,
			
		};

		return params;
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		var form = this.query('form')[0];
		
		var values = form.getValues();

		console.log(values);
		
		var params = 
		{
			appPage:					'Users',
			appMethod:					'UpdateUserRecord',
			UsrID:						values.UsrID,
			UsrFirstName:				values.UsrFirstName,
			UsrLastName:				values.UsrLastName,
			UsrLogin:					values.UsrLogin,
			UsrPassword:				values.UsrPassword,
			UsrRole:					values.UsrRole,
			UsrStatus:					values.UsrStatus,

		};
		
		return params;
	},
	
});