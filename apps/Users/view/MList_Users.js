﻿// Require Popup Form of Users (Wnd for New - Edit)
Ext.require([ 'Users.view.Wnd_NewEditUsers']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Users.view.MList_Users' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Users',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
	
		// Toolbar  : Add New, Edit, Delete
		{
			xtype: 'toolbar',
			dock: 'top',
			layout: { type: 'hbox' },
			items: 
			[	
				{
					xtype: 'button',
					text: 'Show All',
					itemId: '',
					iconCls: 'fa fa-filter',
					enableToggle: true,
					pressed: false,
					handler: function(button) 
					{ 
						var gridpanel = this.up('panel'); 
						
						if(!button.pressed)
							gridpanel.filterShowAll = 0;
						else 
							gridpanel.filterShowAll = 1;
						gridpanel.loadData();	
					}					
				},	
				{
					xtype: 'textfield',
					labelWidth: 40,
					allowBlank: true,
					width: 250, 
					fieldLabel: 'Search',
					margin: '0 0 0 20',
					enableKeyEvents: true,
					listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							},	
  
						}
					}					
				},
				{
					xtype: 'button', 
					text: '',
					tooltip: 'search',
					iconCls: 'fa fa-search',
					scale: 'small',
					listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				},
				{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				},															
				{
					xtype: 'button',
					text: 'Edit',
					iconCls: 'fa fa-pencil-square-o',
					handler: function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				},
				{
					xtype: 'button',
					text: 'Delete',
					iconCls: 'fa fa-trash',
					handler: function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}					
				}
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind = '';
		this.filterShowAll = 0;		
		this.maxRecords = 100;
				
		this.fieldNameID = 'UsrID';
	
		// config
		this.viewConfig = 
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('UsrStatus') > 2 ? 'inactive-row' : 'active-row';	
				
			}					
		};		
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'UsrID', type: 'number', defaultValue: 0 }
				,{name: 'UsrLogin', type: 'string'}
				,{name: 'UsrStatus', type: 'number', defaultValue: 0 }
				,{name: 'UsrRole', type: 'number', defaultValue: 0 }
				,{name: 'UsrFullName', type: 'string'}
			],			
			proxy: 
			{
				type: 'ajax',
				url: '', 
				method : 'POST',
				timeout : 60000,						
				headers : { 'Content-Type' : 'application/json' },
				reader: {
					type: 'json',
					root: 'data'
				},
				simpleSortMode: true,				
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: true,
			},
			items: 
			[
				{header: 'S', dataIndex: 'UsrStatus',  width: 30, resizable: false,
						renderer: function(value, p, record) 
						{
							if (value == 2) 
							{
								return '<i class="fa fa-times" aria-hidden="true" style="font-size:16px;color:red;"></i>';
							}
							else 
							{
								return '<i class="fa fa-check" aria-hidden="true" style="font-size:16px;color:green;"></i>';
							}
								
						}
				}
				,{header: 'Name', dataIndex: 'UsrFullName',  flex: 30/100}
				,{header: 'User Name', dataIndex: 'UsrLogin',  flex: 20/100}
				,{header: 'Role', dataIndex: 'UsrRole',  flex: 30/100, resizable: false,
						renderer: function(value, p, record) 
						{
							if (value == 1) 
							{
								return 'User';
							}
							else if (value == 2) 
							{
								return 'Site Admin';
							}
							else if (value == 3) 
							{
								return 'Global Admin';
							}
								
						}
				}
			]
		};
		
		// Double Click - Open form for Edit
		this.on( 'itemdblclick', this.editRecord, this) ;

		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage: 'Users',
			appMethod: 'get_List',
			maxRecords: me.maxRecords,
			filterStrToFind: me.filterStrToFind,
			filterShowAll: me.filterShowAll,

		};
		
		return params;
	},
	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Users.view.Wnd_NewEditUsers; 		
		x.editRecID = 0;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
			
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Users.view.Wnd_NewEditUsers; 
			x.editRecID = RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();		
		
		if(RecID > 0)
		{
			params = 
			{
				appPage		: 'Users',
				appMethod	: 'DeleteUserRecord',
				UsrID		: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
													
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
		
	},
	
	
	
	
		
	
	 
	
});





