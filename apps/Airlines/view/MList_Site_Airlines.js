﻿// Require Popup Form of Airline (Wnd for New Record)
Ext.require([ 'Airlines.view.Wnd_UploadSiteAirlines']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Airlines.view.MList_Site_Airlines' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Site_Airlines',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				250 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 0'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				}
				,{
					xtype: 'tbfill'
				},
				{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				},
                {
                    xtype: 'tbfill'
                },
                {
                    xtype: 'button',
                    text: 'Export Airlines',
                    iconCls: 'fa fa-file-excel-o',
                    handler: function()
                    {
                        var thisgrid = this.up('panel');

                        var SiteID = thisgrid.SiteID;

                        window.open("/appAjax.php?appPage=Airlines&appMethod=getSiteAirlinesExportList&SiteID="+SiteID);
                    }
                },
                ,{
					xtype: 'tbfill'
				},
                {
                    xtype:		'button'
                    ,text:		'Upload Airlines'
                    ,iconCls: 	'fa fa-upload'
                    ,margin:		'0 0 0 0'
                    ,handler:	function()
                    {
                        var thisgrid = this.up('panel');
                        thisgrid.UploadAirlines();
                    }
                },
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.maxRecords 		= 1000;
		this.fieldNameID 		= 'SiteAirlineID';
		
		this.SiteID = 0;
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('C_AirlinesID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'C_AirlinesID', type: 'number', defaultValue: 0 }
				,{name: 'SiteAirlineID', type: 'number', defaultValue: 0 }
				,{name: 'C_AirlinesName', type: 'string' }
				,{name: 'C_AirlinesWebsite', type: 'string' }
				// ,{name: 'C_AirlinesEmail', type: 'string' }
				,{name: 'C_AirlinesE_Check_In', type: 'string' }
				,{name: 'C_AirlinesE_Booking', type: 'string' }
				// ,{name: 'C_AirlinesFullUrl', type: 'string'}
				,{name: 'C_AirlinesLogoFullUrl', type: 'string'}
				,{name: 'C_AirlinesEscapeDefault', type: 'string'}
				,{name: 'C_AirlinesOnlineContact', type: 'string'}
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				// {header: 'Image', dataIndex: 'C_AirlinesFullUrl',  width: 40, resizable:false,
					// renderer: function(value, p, record) 
					// {
							// return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					// }
				 // },
				{header: 'Image', dataIndex: 'C_AirlinesLogoFullUrl',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					}
				 },				 
				{ header: 'Name', dataIndex: 'C_AirlinesName', flex: 50/100,},
				{ header: 'Website', dataIndex: 'C_AirlinesWebsite', flex: 25/100,},
				// { header: 'Email', dataIndex: 'C_AirlinesEmail', flex: 25/100,},
				{ header: 'Online Contact', dataIndex: 'C_AirlinesOnlineContact', flex: 25/100,},
				{header: 'E-Check In', dataIndex: 'C_AirlinesE_Check_In',  width: 65, resizable:false, align: 'center',
					renderer: function(value, p, record) 
					{
							if(value !="")
							return '<a href="'+value+'" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size:16px;color:green;margin-top:5px"></i></a>';
				
					}
				 },
				 {header: 'E-Booking', dataIndex: 'C_AirlinesE_Booking',  width: 65, resizable:false,align: 'center', 
					renderer: function(value, p, record) 
					{
							if(value !="")
							return '<a href="'+value+'" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size:16px;color:blue; margin-top:5px"></i></a>';
				
					}
				 },
				 { header: 'Escape Default', dataIndex: 'C_AirlinesEscapeDefault', flex: 25/100,},
				
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		var params =
		{
			appPage			: 'Airlines',
			appMethod			: 'getSiteAirlinesList',
			maxRecords		: me.maxRecords,
			filterStrToFind		: me.filterStrToFind,
			SiteID				: me.SiteID,
		};
		
		return params;
	},

	
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Airlines'
				,appMethod	: 'DeleteSiteAirlinesRecord'
				,SiteAirlineID	: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
								//Ext.ComponentManager.get('MList_Nodes_Media').loadData();					
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},


    /**************************************************************************************************
     * UploadAirlines
     *
     * Button handler for UploadAirlines
     *
     ***************************************************************************************************/
    UploadAirlines: function()
    {
        var thisgrid	= this;

        var x = new Airlines.view.Wnd_UploadSiteAirlines;

        x.SiteID = thisgrid.SiteID;

        x.callbackRefresh = function( ){ thisgrid.loadData(); };
        x.show(this);


    },
	

});