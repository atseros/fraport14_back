﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('Airlines.view.Wnd_NewEditAirline', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditAirline',
		
	layout: 'border',
	title: 'Airline Data',
    height: 385,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 80,
				region :'north',
           },
			items: 
			[
				// Hidden field:  C_AirlinesID
				{
					xtype: 'hiddenfield',
					name: 'C_AirlinesID',
				},
				//File
				{
					xtype: 'filefield',
					name: 'Logo',
					fieldLabel: 'Logo',
					border: 0,
					layout: 'border',
					region :'north',
					regex: /^.*\.(jpg|JPG|gif|GIF|png|PNG|bmp|BMP)$/,
					regexText: ''
				},
				//File
				{
					xtype: 'filefield',
					name: 'File',
					fieldLabel: 'Image',
					border: 0,
					layout: 'border',
					region :'north',
					regex: /^.*\.(jpg|JPG|gif|GIF|png|PNG|bmp|BMP)$/,
					regexText: ''
				},
				//File
				// {
					// xtype: 'filefield',
					// name: 'Flights',
					// fieldLabel: 'Flights Icon',
					// border: 0,
					// layout: 'border',
					// region :'north',
					// regex: /^.*\.(jpg|JPG|gif|GIF|png|PNG|bmp|BMP)$/,
					// regexText: ''
				// },
				// Line Separator
				{
					xtype: 'container',
					html: '<hr>',
					height:10
				},				
				// Text field:  C_AirlinesName
				{
					xtype: 'textfield',
					name: 'C_AirlinesName',
					fieldLabel: 'Name',
					allowBlank: false,
				},	
				// Text field:  C_AirlinesIATA
				{
					xtype: 'textfield',
					name: 'C_AirlinesIATA',
					fieldLabel: 'IATA',
				},	
				// Text field:  C_AirlinesICAO
				{
					xtype: 'textfield',
					name: 'C_AirlinesICAO',
					fieldLabel: 'ICAO',
					allowBlank: false,
				},	
				// Text field:  C_AirlinesWebsite
				{
					xtype: 'textfield',
					name: 'C_AirlinesWebsite',
					fieldLabel: 'Website',
				},	
				// Text field:  C_AirlinesEmail
				// {
					// xtype: 'textfield',
					// name: 'C_AirlinesEmail',
					// fieldLabel: 'Email',
				// },	
				// Text field:  C_AirlinesOnlineContact
				{
					xtype: 'textfield',
					name: 'C_AirlinesOnlineContact',
					fieldLabel: 'Online Contact',
				},	
				// Text field:  C_AirlinesE_Check_In
				{
					xtype: 'textfield',
					name: 'C_AirlinesE_Check_In',
					fieldLabel: 'E-Check In',
				},	
				// Text field:  C_AirlinesE_Booking
				{
					xtype: 'textfield',
					name: 'C_AirlinesE_Booking',
					fieldLabel: 'E-Booking',
				},	
				// Text field:  C_AirlinesEscapeDefault
				{
					xtype: 'textfield',
					name: 'C_AirlinesEscapeDefault',
					fieldLabel: 'Escape Default',
				},	
				
											
				
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	*
	* params for Ajax request that loads Default Values for New Record (insert mode)
	*
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage		: 'Airlines',
			appMethod		: 'get_CentralAirline_NewRecordDefValues',
			
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	*
	* runs after Ajax Request that gets Default Values for New Record has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
		
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	*
	* params for Ajax request that loads Record Values (edit mode)
	*
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage		: 'Airlines',
			appMethod		: 'get_CentralAirlineRecord',
			C_AirlinesID	: this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	*
	* runs after Ajax Request that gets Record Values has returned with success
	*
	* by default loads values in form fields
	*
	*	var me = this;
	*	var thisf = me.query('form')[0];
	*	
	*	thisf.getForm().setValues(data);
	*	
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;
		
		me.DataAndImage();
	},
	
	/**************************************************************************************************
	* getInsertParams
	*
	* params for Ajax request that inserts New Record (insert mode)
	*
	***************************************************************************************************/
	getInsertParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* getUpdateParams
	*
	* params for Ajax request that updates Record (edit mode)
	*
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* DataAndImage
	*
	* function that uploades Images and add data
	*
	***************************************************************************************************/
	DataAndImage: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();
		
		
		if (me.editRecID == 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Airlines&appMethod=InsertCentralAirlineRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been added.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not add.');                    
					}
				});
			}
			
		}
		else	if (me.editRecID > 0)
		{
			if (form.isValid()) 
			{
				form.submit(
				{
					url: 'appAjax.php?appPage=Airlines&appMethod=UpdateCentralAirlineRecord',
					waitMsg: ('Uploading your file...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been updated.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not update.');                    
					}
				});
			}
			
		}			

        
	},
	
});