﻿Ext.define('Airlines.view.Wnd_UploadAirlines', {
    extend: 'Ext.window.Window',
    alias: 'widget.Wnd_UploadAirlines',
		
	layout: 'border',
	title: 'CSV Selection',
    height: 100,
    width: 500,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
	dockedItems: 
	[
        {
			xtype: 'toolbar',
			dock: 'bottom',
            layout: { pack: 'end', type: 'hbox' },
            items: 
			[
                 {
                    xtype: 'button',
                    text: 'Save',
                    iconCls: 'fa fa-floppy-o',
                    handler: function () {
                        var wnd = this.up('.window');
						wnd.uploadFile();
                    }
                },
				{
                    xtype: 'button',
                    text: 'Cancel',
                    iconCls: 'fa fa-ban',
                    handler: function () 
					{										
                        var wnd = this.up('.window');
						wnd.cancel();
                    }
                },
                
            ]
        }
    ],
   items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 40,
				region :'north',
           },
			items: 
			[
				// Text field:  filefield
				{
					xtype: 'filefield',
					margin: '5 0 0 5',
					name: 'ExcelAirlines',
					fieldLabel: 'Excel',
					regex: /^.*\.(xls|XLS)$/,
				},
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
		this.tblName;
		this.recID;
		this.curFolder;
		        			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},


	
	/**************************************************************************************************
	* uploadFile
	*
	* function that handles the image upload
	*
	***************************************************************************************************/
	uploadFile: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();

        if (form.isValid()) 
		{
            form.submit(
			{
				url: 'appAjax.php?appPage=Airlines&appMethod=upload_excel_airlines',
                waitMsg: ('Uploading your file...'),
                success: function (fp, o) 
				{	
					Ext.Msg.alert('Success', 'Your file has been uploaded.');
					if (typeof (me.callbackRefresh) == 'function')
					{
						me.callbackRefresh();
					}
					me.close();
                },
                failure: function (form, action) 
				{
                    Ext.Msg.alert('Error', 'Your file did not upload.');                    
                }
            });
        }
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  cancel    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	cancel: function () 
	{	
		 this.close();
    },	
	
	
});