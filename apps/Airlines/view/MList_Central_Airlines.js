﻿// Require Popup Form of Airline (Wnd for New Record)
Ext.require([ 'Airlines.view.Wnd_NewEditAirline']);

Ext.require([ 'Airlines.view.Wnd_UploadAirlines']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Airlines.view.MList_Central_Airlines' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Central_Airlines',
	
	autoScroll: false,                
	layout: 'border',
	
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: 
	[
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: 
			[
								
				{
					xtype: 'button',
					text: 'Show Available Only',
					itemId: '',
					iconCls: 'fa fa-filter',
					enableToggle: true,
					pressed: false,
					handler: function(button) 
					{ 
						var gridpanel = this.up('panel'); 
						
						if(!button.pressed)
						{
							gridpanel.filterShowAll = 0;
							button.setText('Show Available Only');
						}		
						else
						{
							gridpanel.filterShowAll = 1;
							button.setText('Show All');
						}		
							
						gridpanel.loadData();	
					}					
				},
				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				250 
					,fieldLabel:		'Search'
					,margin:			'0 0 0 0'
					,enableKeyEvents:	true
					,listeners: 
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue(); 		
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: 
						{
							fn: function(field,e) 
							{
								var gridpanel = this.up('panel'); 
								var val = field.getValue();                
								gridpanel.filterStrToFind = val;
							}
  
						}
					}					
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners: 
					{
						click: function() 
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}					
				}
				,{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.addRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.editRecord();
					}					
				}
				,{
					 xtype:		'button'
					,text:		'Delete'
					,iconCls: 'fa fa-trash'
					,handler:	function() 
					{ 
						var thisgrid = this.up('panel');
						thisgrid.deleteRecord();
					}			
				},
				{
					 xtype:		'button'
					,text:		'Add Airline To Website'
					,iconCls: 	'fa fa-arrow-right'
					,margin:		'0 0 0 10'
					,handler:	function() 
					{
						var thisgrid = this.up('panel');
						thisgrid.AddAirlineToWebsite();
					}				
				},
                {
					xtype: 'tbfill'
				},
                {
                    xtype: 'button',
                    text: 'Export Airlines',
                    iconCls: 'fa fa-file-excel-o',
                    handler: function()
                    {
                        var thisgrid = this.up('panel');

                        window.open("/appAjax.php?appPage=Airlines&appMethod=getCentralAirlinesExportList");
                    }
                },
                {
                    xtype:		'button'
                    ,text:		'Upload Airlines'
                    ,iconCls: 	'fa fa-upload'
                    ,margin:		'0 0 0 0'
                    ,handler:	function()
                    {
                        var thisgrid = this.up('panel');
                        thisgrid.UploadAirlines();
                    }
                },
			]
		},
	],		
	
	
	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterShowAll = 0;
		this.maxRecords 		= 1000;
		this.fieldNameID 		= 'C_AirlinesID';
		
		this.SiteID = 0;
		this.SiteDomainName = "";
		
		// config
		this.viewConfig = 
		{
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('C_AirlinesID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = 
		{
			autoLoad: false,	
			autoSync: false,			
			fields: 
			[
				 {name: 'C_AirlinesID', type: 'number', defaultValue: 0 }
				,{name: 'C_AirlinesName', type: 'string' }
				,{name: 'C_AirlinesWebsite', type: 'string' }
				// ,{name: 'C_AirlinesEmail', type: 'string' }
				,{name: 'C_AirlinesIATA', type: 'string' }
				,{name: 'C_AirlinesICAO', type: 'string' }
				,{name: 'C_AirlinesE_Check_In', type: 'string' }
				,{name: 'C_AirlinesE_Booking', type: 'string' }
				,{name: 'C_AirlinesFullUrl', type: 'string'}
				,{name: 'C_AirlinesFullUrlLogo', type: 'string'}
				,{name: 'C_AirlinesEscapeDefault', type: 'string'}
				// ,{name: 'C_AirlinesFlightsLogo', type: 'string'}
				,{name: 'C_AirlinesOnlineContact', type: 'string'}
				
					
				 
				
			],			
			proxy: 
			{
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			},
		};					

		// columns
		this.columns = 
		{
			defaults: 
			{
				resizable: true,
				sortable: false,
			},
			items: 
			[
				
				{header: 'Logo', dataIndex: 'C_AirlinesFullUrlLogo',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					}
				 },				
				{header: 'Image', dataIndex: 'C_AirlinesFullUrl',  width: 40, resizable:false,
					renderer: function(value, p, record) 
					{
							return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					}
				 },				
				// {header: 'Flight Icon', dataIndex: 'C_AirlinesFlightsLogo',  width: 40, resizable:false,
					// renderer: function(value, p, record) 
					// {
							// return '<a href="'+value+'" target="_blank"><img src="'+value+'" height="32" width="32" /></a>';
				
					// }
				 // },				
				{ header: 'Name', dataIndex: 'C_AirlinesName', flex: 50/100,},
				{ header: 'ICAO', dataIndex: 'C_AirlinesICAO', flex: 10/100,},
				{ header: 'IATA', dataIndex: 'C_AirlinesIATA', flex: 10/100,},
				{ header: 'Website', dataIndex: 'C_AirlinesWebsite', flex: 25/100,},
				// { header: 'Email', dataIndex: 'C_AirlinesEmail', flex: 25/100,},
				{ header: 'Online Contact', dataIndex: 'C_AirlinesOnlineContact', flex: 25/100,},
				{header: 'E-Check In', dataIndex: 'C_AirlinesE_Check_In',  width: 60, resizable:false, align: 'center',
					renderer: function(value, p, record) 
					{
							if(value !="")
							return '<a href="'+value+'" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size:16px;color:green;margin-top:5px"></i></a>';
				
					}
				 },
				 {header: 'E-Booking', dataIndex: 'C_AirlinesE_Booking',  width: 60, resizable:false,align: 'center', 
					renderer: function(value, p, record) 
					{
							if(value !="")
							return '<a href="'+value+'" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size:16px;color:blue; margin-top:5px"></i></a>';
				
					}
				 },
				 { header: 'Escape Default', dataIndex: 'C_AirlinesEscapeDefault', flex: 25/100,},
				
				
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		if (me.filterShowAll == 1)
		{
			SiteID = me.SiteID;
		}
		else
		{
			SiteID = -1;
		}
		
		var params =
		{
			appPage			: 'Airlines',
			appMethod			: 'getCentralAirlinesList',
			maxRecords		: me.maxRecords,
			filterStrToFind		: me.filterStrToFind,
			SiteID				: SiteID,
		};
		
		return params;
	},

	
	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new Airlines.view.Wnd_NewEditAirline; 		
		x.editRecID = 0;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new Airlines.view.Wnd_NewEditAirline; 
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); Ext.ComponentManager.get('MList_Sites').loadData(); };  
			x.show(this);
			x.loadData();
		}
	},
	
	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function () 
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();	
		
		if(RecID > 0)
		{
			params = 
			{
				 appPage		: 'Airlines'
				,appMethod	: 'DeleteCentralAirlineRecord'
				,C_AirlinesID	: RecID
			};
		}		
		
		return params;
	},
	
	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;
		
		var params = thisgrid.getDeleteRecordParams();
		
		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?', 
				function(btn) 
				{
					if (btn === 'yes') 
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data) 
								{										
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}
								Ext.ComponentManager.get('MList_Sites').loadData();					
								thisgrid.loadData();
							}
						});
						return true;
					} 
					else 
					{
						return false;
					}
				});
		}
	},
	
	/**************************************************************************************************
	* AddAirlineToWebsite
	*
	* Button handler for AddAirlineToWebsite
	*
	***************************************************************************************************/
	AddAirlineToWebsite: function()
	{
		var thisgrid = this;
		
		var SiteID  	= thisgrid.SiteID;
		var C_AirlinesID 	= thisgrid.getSelectedRecordID();

		params = 
		{
			appPage		: 'Airlines',
			appMethod		: 'AddAirlineToWebsite',
			SiteAirlineSiteID	: SiteID,
			SiteAirlineAirlineID	: C_AirlinesID,
			
		};
		
		
		Ext.Msg.confirm('Confirmation', 'Add Airline to '+thisgrid.SiteDomainName+' ?', 
		function(btn) 
			{
				if (btn === 'yes') 
				{
					Ext.Ajax.request({
						url: 'appAjax.php',
						method: 'post',
						params:  params,
						success: function(response)
						{
							var text = response.responseText;
							var obj = Ext.JSON.decode(text);
							if (!obj.data) 
							{										
								Ext.Msg.alert('Not added!', obj.error);
								return;
							}
							Ext.ComponentManager.get('MList_Sites').loadData();
							thisgrid.loadData();
							// Ext.ComponentManager.get('MList_CentralImageGalleryDetailed').loadData();	
						}	
					});
					return true;
				} 
				else 
				{
					return false;
				}
			}
		);	
		
	},


    /**************************************************************************************************
     * UploadAirlines
     *
     * Button handler for UploadAirlines
     *
     ***************************************************************************************************/
    UploadAirlines: function()
    {
        var thisgrid	= this;

        var x = new Airlines.view.Wnd_UploadAirlines;
        x.callbackRefresh = function( ){ thisgrid.loadData(); };
        x.show(this);


    },
});