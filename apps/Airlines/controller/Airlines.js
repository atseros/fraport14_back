﻿Ext.define('Airlines.controller.Airlines', {
    extend: 'Ext.app.Controller',
    alias: 'widget.AirlinesControler',

    models: [],
    stores: [],
    views: [
					'MList_Central_Airlines'
					,'Frm_Central_Airlines_Translation'
					,'MList_Sites'
					,'MList_Site_Airlines'
		
			],
	
	/*******************************************************************
	* CentralAirlines_ClearRelatedBeforeReload
	********************************************************************/
	CentralAirlines_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear Airlines Translation
		Ext.ComponentManager.get('Frm_Central_Airlines_Translation').clearData();
		
	},
	
	/*******************************************************************
	* CentralAirlines_OnSelectionChange
	********************************************************************/
	CentralAirlines_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_C_AirlinesID = selectedRecord.get('C_AirlinesID');
		
				// Load Airlines Translation
				Ext.ComponentManager.get('Frm_Central_Airlines_Translation').C_AirlinesID = selected_C_AirlinesID;
				Ext.ComponentManager.get('Frm_Central_Airlines_Translation').loadData();
				
			}	
		}		
	},	
	
	/*******************************************************************
	* Sites_ClearRelatedBeforeReload
	********************************************************************/
	Sites_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear SiteData
		Ext.ComponentManager.get('MList_Central_Airlines').SiteID = 0;
		Ext.ComponentManager.get('MList_Central_Airlines').SiteDomainName = "";
		
		// Clear Site Airlines
		Ext.ComponentManager.get('MList_Site_Airlines').clearData();		
		
	},
	
	/*******************************************************************
	* Sites_Media_OnSelectionChange
	********************************************************************/
	Sites_Media_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_SiteID = selectedRecord.get('SiteID');
				var selected_SiteDomainName = selectedRecord.get('SiteDomainName');
					
				// Set Site Data
				Ext.ComponentManager.get('MList_Central_Airlines').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Central_Airlines').SiteDomainName = selected_SiteDomainName;
			
				// Load Site Airlines
				Ext.ComponentManager.get('MList_Site_Airlines').SiteID = selected_SiteID;
				Ext.ComponentManager.get('MList_Site_Airlines').loadData();
				
			}	
		}		
	},		
    
});