<?php

require_once(realpath(__DIR__."/../../db")."/class.DB_sys_central_airlines.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_central_airlineslng.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_site_airline.php");

/*********************************************************************************************
* CLASS Airlines
*
* DESCRIPTION: 
*	Class that handles requests from Airlines page 
*
*********************************************************************************************/
class Airlines
{
	private $appFrw;
	
	/****************************************************************************************
	* Airlines::CONSTRUCTOR
	****************************************************************************************/
	function Airlines($appFrw)
	{
		$this->appFrw = $appFrw;
	}
	
	/*
		AIRLINES 
	*/
	
	/****************************************************************************************
	* Airlines::get_CentralAirline_NewRecordDefValues
	****************************************************************************************/
	function get_CentralAirline_NewRecordDefValues()
	{
		$params = array();
		
		$results = DB_sys_central_airlines::sys_central_airlines_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Airlines::InsertCentralAirlineRecord
	****************************************************************************************/
	function InsertCentralAirlineRecord()
	{
		$params = array();
		
		$params["C_AirlinesID"] = $_REQUEST["C_AirlinesID"];
		$params["C_AirlinesName"] = $_REQUEST["C_AirlinesName"];
		$params["C_AirlinesWebsite"] = $_REQUEST["C_AirlinesWebsite"];
		// $params["C_AirlinesEmail"] = $_REQUEST["C_AirlinesEmail"];
		$params["C_AirlinesICAO"] = $_REQUEST["C_AirlinesICAO"];
		$params["C_AirlinesIATA"] = $_REQUEST["C_AirlinesIATA"];
		$params["C_AirlinesE_Check_In"] = $_REQUEST["C_AirlinesE_Check_In"];
		$params["C_AirlinesE_Booking"] = $_REQUEST["C_AirlinesE_Booking"];
		$params["C_AirlinesEscapeDefault"] = $_REQUEST["C_AirlinesEscapeDefault"];
		$params["C_AirlinesOnlineContact"] = $_REQUEST["C_AirlinesOnlineContact"];
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["C_AirlinesFileName"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_airlines', $params["C_AirlinesID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$md");
		
			$mdnewsize = 'mdnew-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$mdnewsize");
			
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}

		if(!empty($_FILES["Logo"]["tmp_name"]))
		{
			$params["C_AirlinesFileNameLogo"] = $_FILES["Logo"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_airlines', $params["C_AirlinesID"] )."/";
			
			$tmp_uploaded_file = $_FILES["Logo"]["tmp_name"];
			$uploaded_filename = $_FILES["Logo"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'logo-thumb-' . $_FILES["Logo"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'logo-md-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'logo-md3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'logo-sd-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'logo-sd3-1-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'logo-sd3-2-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'logo-xs-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'logo-xs3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'logo-rectangle-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}		
		
		if(!empty($_FILES["Flights"]["tmp_name"]))
		{
			$params["C_AirlinesFlightsLogo"] = $_FILES["Flights"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_airlines', $params["C_AirlinesID"] )."/";
			
			$tmp_uploaded_file = $_FILES["Flights"]["tmp_name"];
			$uploaded_filename = $_FILES["Flights"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'flights-logo-' . $_FILES["Flights"]["name"];
			
			FileManager::resize_crop_image(32, 32, "$folder/$uploaded_filename", "$folder/$thumb");
		}		
		
		$result = DB_sys_central_airlines::sys_central_airlines_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Airlines::get_CentralAirlineRecord
	****************************************************************************************/
	function get_CentralAirlineRecord()
	{
		$params["C_AirlinesID"] = $_REQUEST["C_AirlinesID"];
						
		$results = DB_sys_central_airlines::sys_central_airlines_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Airlines::UpdateCentralAirlineRecord
	****************************************************************************************/
	function UpdateCentralAirlineRecord()
	{
		$params = array();

		if(isset($_REQUEST['C_AirlinesID'])) { $params['C_AirlinesID'] = $_REQUEST['C_AirlinesID']; }
		if(isset($_REQUEST['C_AirlinesName'])) { $params['C_AirlinesName'] = $_REQUEST['C_AirlinesName']; }
		if(isset($_REQUEST['C_AirlinesWebsite'])) { $params['C_AirlinesWebsite'] = $_REQUEST['C_AirlinesWebsite']; }
		// if(isset($_REQUEST['C_AirlinesEmail'])) { $params['C_AirlinesEmail'] = $_REQUEST['C_AirlinesEmail']; }
		if(isset($_REQUEST['C_AirlinesIATA'])) { $params['C_AirlinesIATA'] = $_REQUEST['C_AirlinesIATA']; }
		if(isset($_REQUEST['C_AirlinesICAO'])) { $params['C_AirlinesICAO'] = $_REQUEST['C_AirlinesICAO']; }
		if(isset($_REQUEST['C_AirlinesE_Check_In'])) { $params['C_AirlinesE_Check_In'] = $_REQUEST['C_AirlinesE_Check_In']; }
		if(isset($_REQUEST['C_AirlinesE_Booking'])) { $params['C_AirlinesE_Booking'] = $_REQUEST['C_AirlinesE_Booking']; }
		if(isset($_REQUEST['C_AirlinesEscapeDefault'])) { $params['C_AirlinesEscapeDefault'] = $_REQUEST['C_AirlinesEscapeDefault']; }
		if(isset($_REQUEST['C_AirlinesOnlineContact'])) { $params['C_AirlinesOnlineContact'] = $_REQUEST['C_AirlinesOnlineContact']; }
		
		
		if(!empty($_FILES["File"]["tmp_name"]))
		{
			$params["C_AirlinesFileName"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_airlines', $params["C_AirlinesID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'md-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$md");
		
			$mdnewsize = 'mdnew-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(1280, 600, "$folder/$uploaded_filename", "$folder/$mdnewsize");
			
			$md3 = 'md3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'sd-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'sd3-1-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'sd3-2-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'xs-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'xs3-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'rectangle-' . $_FILES["File"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}	
		
		if(!empty($_FILES["Logo"]["tmp_name"]))
		{
			$params["C_AirlinesFileNameLogo"] = $_FILES["Logo"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_airlines', $params["C_AirlinesID"] )."/";
			
			$tmp_uploaded_file = $_FILES["Logo"]["tmp_name"];
			$uploaded_filename = $_FILES["Logo"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'logo-thumb-' . $_FILES["Logo"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
		
			$md = 'logo-md-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(1280, 720, "$folder/$uploaded_filename", "$folder/$md");
		
			$md3 = 'logo-md3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(960, 540, "$folder/$uploaded_filename", "$folder/$md3");
		
			$sd = 'logo-sd-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(767, 767, "$folder/$uploaded_filename", "$folder/$sd");
		
			$sd3_1 = 'logo-sd3-1-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(603, 340, "$folder/$uploaded_filename", "$folder/$sd3_1");
		
			$sd3_2 = 'logo-sd3-2-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(388, 218, "$folder/$uploaded_filename", "$folder/$sd3_2");
		
			$xs = 'logo-xs-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(362, 362, "$folder/$uploaded_filename", "$folder/$xs");
			
			$xs3 = 'logo-xs3-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(320, 180, "$folder/$uploaded_filename", "$folder/$xs3");
			
			$rectangle = 'logo-rectangle-' . $_FILES["Logo"]["name"];
			FileManager::resize_crop_image(339, 191, "$folder/$uploaded_filename", "$folder/$rectangle");
			
		}	
		
		if(!empty($_FILES["Flights"]["tmp_name"]))
		{
			$params["C_AirlinesFlightsLogo"] = $_FILES["Flights"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_airlines', $params["C_AirlinesID"] )."/";
			
			$tmp_uploaded_file = $_FILES["Flights"]["tmp_name"];
			$uploaded_filename = $_FILES["Flights"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'flights-logo-' . $_FILES["Flights"]["name"];
			
			FileManager::resize_crop_image(32, 32, "$folder/$uploaded_filename", "$folder/$thumb");
		}	
		
		$result = DB_sys_central_airlines::sys_central_airlines_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* Airlines::DeleteCentralAirlineRecord
	****************************************************************************************/
	function DeleteCentralAirlineRecord()
	{
		$params = array();
		
		$params["C_AirlinesID"] = $_REQUEST["C_AirlinesID"];
		
		$results = DB_sys_central_airlines::sys_central_airlines_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

	/****************************************************************************************
	* Airlines::getCentralAirlinesList
	****************************************************************************************/
	function getCentralAirlinesList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : -1;
		
		$results = DB_sys_central_airlines::sys_central_airlines_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		AIRLINES LNG
	*/
	
	/****************************************************************************************
	* Home::get_CentralAirlinesLngRecord
	****************************************************************************************/
	function get_CentralAirlinesLngRecord()
	{
		//Check if Record Exists
		$params["C_AirlinesLngC_AirlinesID"] = $_REQUEST["C_AirlinesLngC_AirlinesID"];
		$params["C_AirlinesLngType"] = $_REQUEST["C_AirlinesLngType"];
		
		
		$CheckIfCentralAirlineLngExists = DB_sys_central_airlineslng::sys_central_airlineslng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfCentralAirlineLngExists == 0)
		{
			// // Insert NodeLng
			$CentralAirlinesLngDefValues = DB_sys_central_airlineslng::sys_central_airlineslng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["C_AirlinesLngID"] = $CentralAirlinesLngDefValues['data']['C_AirlinesLngID'];
			
			$result = DB_sys_central_airlineslng::sys_central_airlineslng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_central_airlineslng::sys_central_airlineslng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$C_AirlinesLngID = DB_sys_central_airlineslng::sys_central_airlineslng_GetC_AirlinesLngID($this->appFrw, $params);
			
			$params["C_AirlinesLngID"] = $C_AirlinesLngID; 
			
			$results = DB_sys_central_airlineslng::sys_central_airlineslng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
		
	}
	
	/****************************************************************************************
	* Home::UpdateCentralAirlinesLngRecord
	****************************************************************************************/
	function UpdateCentralAirlinesLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['C_AirlinesLngID'])) { $params['C_AirlinesLngID'] = $_REQUEST['C_AirlinesLngID']; }
		if(isset($_REQUEST['C_AirlinesLngType'])) { $params['C_AirlinesLngType'] = $_REQUEST['C_AirlinesLngType']; }
		if(isset($_REQUEST['C_AirlinesLngC_AirlinesID'])) { $params['C_AirlinesLngC_AirlinesID'] = $_REQUEST['C_AirlinesLngC_AirlinesID']; }
		if(isset($_REQUEST['C_AirlinesLngName'])) { $params['C_AirlinesLngName'] = $_REQUEST['C_AirlinesLngName']; }
		// if(isset($_REQUEST['C_AirlinesLngAddress'])) { $params['C_AirlinesLngAddress'] = $_REQUEST['C_AirlinesLngAddress']; }
		if(isset($_REQUEST['C_AirlinesLngPhones'])) { $params['C_AirlinesLngPhones'] = $_REQUEST['C_AirlinesLngPhones']; }
		if(isset($_REQUEST['C_AirlinesLngDictionaryDescription'])) { $params['C_AirlinesLngDictionaryDescription'] = $_REQUEST['C_AirlinesLngDictionaryDescription']; }
		
		
		$results = DB_sys_central_airlineslng::sys_central_airlineslng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		Site Airlines
	*/
	
		/****************************************************************************************
	* Home::AddAirlineToWebsite
	****************************************************************************************/
	function AddAirlineToWebsite()
	{
		$params = array();
		
		$params["SiteAirlineSiteID"] = $_REQUEST["SiteAirlineSiteID"];
		$params["SiteAirlineAirlineID"] = $_REQUEST["SiteAirlineAirlineID"];
		
		//Check if Airline is already added
		$CheckIfAirlineIsAlreadyAdded = DB_sys_site_airline::sys_site_airline_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfAirlineIsAlreadyAdded == 0)
		{
			$SiteAirlineDefValues = DB_sys_site_airline::sys_site_airline_get_NewRecordDefValues($this->appFrw, $params);
			
			$SiteAirlineID = $SiteAirlineDefValues['data']['SiteAirlineID'];
			
			$params["SiteAirlineID"] = $SiteAirlineID;
			
			$result = DB_sys_site_airline::sys_site_airline_InsertRecord($this->appFrw, $params);
			
			if ($result['success']==true)
				return json_encode($result) ;
			else 
				return json_encode((array('error' => $result["reason"])));	
		}
		else
		{
			return json_encode((array('error' => 'The selected Airline is already added at the selected Website')));	
		}			
		
	}
	
	/****************************************************************************************
	* Airlines::DeleteSiteAirlinesRecord
	****************************************************************************************/
	function DeleteSiteAirlinesRecord()
	{
		$params = array();
		
		$params["SiteAirlineID"] = $_REQUEST["SiteAirlineID"];
		
		$results = DB_sys_site_airline::sys_site_airline_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Airlines::getSiteAirlinesList
	****************************************************************************************/
	function getSiteAirlinesList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;
		
		$results = DB_sys_site_airline::sys_site_airline_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}

    /*
        Export to Excel Airlines
    */

    /****************************************************************************************
     * Airlines::getCentralAirlinesExportList
     ****************************************************************************************/
    function getCentralAirlinesExportList()
    {
        require_once(realpath(__DIR__."/../../libphp/PHPExcel_1.8.0_doc")."/PHPExcel.php");
        require_once(realpath(__DIR__."/../../libphp/PHPExcel_1.8.0_doc")."/PHPExcel/Writer/Excel2007.php");

        $params = array();

        $results = DB_sys_central_airlines::sys_central_airlines_get_ExportList($this->appFrw, $params);
//
//        echo '<pre>';
//        print_r($results);
//        echo '</pre>';
//        exit();

        $sheet = array();

        foreach($results["data"] as $row)
        {
            $line = array();

            //ID
            $line[1] = $row["C_AirlinesID"];
            //NAME
            $line[2] = $row["C_AirlinesName"];
            //ICAO
            $line[3] = $row["C_AirlinesICAO"];
            //IATA
            $line[4] = $row["C_AirlinesIATA"];
            //WEBSITE
            $line[5] = $row["C_AirlinesWebsite"];
            //ONLINE CONTACT
            $line[6] = $row["C_AirlinesOnlineContact"];
            //E-CHECK IN
            $line[7] = $row["C_AirlinesE_Check_In"];
            //E-BOOKING
            $line[8] = $row["C_AirlinesE_Booking"];
            //ESCAPE DEFAULT
            $line[9] = $row["C_AirlinesEscapeDefault"];
            //TRANSLATION NAME EN
            $line[10] = $row["C_AirlinesLngName_EN"];
            //TRANSLATION PHONE EN
            $line[11] = $row["C_AirlinesLngPhones_EN"];
            //TRANSLATION SUBTITLE EN
            $line[12] = $row["C_AirlinesLngDictionaryDescription_EN"];
            //TRANSLATION NAME EL
            $line[13] = $row["C_AirlinesLngName_EL"];
            //TRANSLATION PHONE EL
            $line[14] = $row["C_AirlinesLngPhones_EL"];
            //TRANSLATION SUBTITLE EL
            $line[15] = $row["C_AirlinesLngDictionaryDescription_EL"];

            array_push($sheet, $line);
        }

//        echo '<pre>';
//        print_r($sheet);
//        echo '</pre>';
//        exit();

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        for($col = 'A'; $col !== 'C'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('Central Airlines');
        }

        $objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'ICAO');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'IATA');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Website');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Online Contact');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'E-Check In');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'E-Booking');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Escape Default');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Translation Name En');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Translation Phone En');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Translation Subtitle En');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Translation Name El');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Translation Phone El');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Translation Subtitle El');

        $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        $objPHPExcel->getActiveSheet()->protectCells('A1:A10000', '1234');
        $objPHPExcel->getActiveSheet()->getStyle('B1:B10000')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
        $objPHPExcel->getActiveSheet()->protectCells('C1:D10000', '1234');
        $objPHPExcel->getActiveSheet()->getStyle('E1:O10000')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);

        $objPHPExcel->getActiveSheet()->fromArray($sheet, null, 'A2');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="central_airlines.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    /****************************************************************************************
     * Airlines::getSiteAirlinesExportList
     ****************************************************************************************/
    function getSiteAirlinesExportList()
    {
        require_once(realpath(__DIR__."/../../libphp/PHPExcel_1.8.0_doc")."/PHPExcel.php");
        require_once(realpath(__DIR__."/../../libphp/PHPExcel_1.8.0_doc")."/PHPExcel/Writer/Excel2007.php");

        $params = array();

        $params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;

        $results = DB_sys_site_airline::sys_site_airline_get_ExportList($this->appFrw, $params);

//        echo '<pre>';
//        print_r($results);
//        echo '</pre>';
//        exit();

        $sheet = array();

        foreach($results["data"] as $row)
        {
            $line = array();

            //ID
            $line[1] = $row["SiteAirlineID"];
            //NAME
            $line[2] = $row["C_AirlinesName"];
            //WEBSITE
            $line[3] = $row["C_AirlinesWebsite"];
            //ONLINE CONTACT
            $line[4] = $row["C_AirlinesOnlineContact"];
            //E-CHECK IN
            $line[5] = $row["C_AirlinesE_Check_In"];
            //E-BOOKING
            $line[6] = $row["C_AirlinesE_Booking"];
            //ESCAPE DEFAULT
            $line[7] = $row["C_AirlinesEscapeDefault"];
            //TRANSLATION AIRPORT PHONES EN
            $line[8] = $row["SiteAirlineLngAirportPhones_EN"];
            //TRANSLATION RESERVATIONS EN -> Renamed to Cargo Services in Excel Title
            $line[9] = $row["SiteAirlineLngReservations_EN"];
            //TRANSLATION LOST AND FOUND EN
            $line[10] = $row["SiteAirlineLngLostAndFound_EN"];
            //TRANSLATION HANDLER EN
            $line[11] = $row["SiteAirlineLngHandler_EN"];
            //TRANSLATION HANDLER AIRPORT PHONES EN
            $line[12] = $row["SiteAirlineLngHandlerAirportPhone_EN"];
            //TRANSLATION EMAIL EN
            $line[13] = $row["SiteAirlineLngEmail_EN"];
            //TRANSLATION AIRPORT PHONES EL
            $line[14] = $row["SiteAirlineLngAirportPhones_EL"];
            //TRANSLATION RESERVATIONS EL -> Renamed to Cargo Services in Excel Title
            $line[15] = $row["SiteAirlineLngReservations_EL"];
            //TRANSLATION LOST AND FOUND EL
            $line[16] = $row["SiteAirlineLngLostAndFound_EL"];
            //TRANSLATION HANDLER EL
            $line[17] = $row["SiteAirlineLngHandler_EL"];
            //TRANSLATION HANDLER AIRPORT PHONES EL
            $line[18] = $row["SiteAirlineLngHandlerAirportPhone_EL"];
            //TRANSLATION EMAIL EL
            $line[19] = $row["SiteAirlineLngEmail_EL"];

            array_push($sheet, $line);
        }

//        echo '<pre>';
//        print_r($sheet);
//        echo '</pre>';
//        exit();

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        for($col = 'A'; $col !== 'C'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('Central Airlines');
        }

        $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Website');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Online Contact');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'E-Check In');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'E-Booking');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Escape Default');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Translation Airport Phones En');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Translation Cargo Services En');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Translation Lost & Found En');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Translation Handler En');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Translation Handler Airport Phones En');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Translation Email En');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Translation Airport Phones El');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Translation Cargo Services El');
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Translation Lost & Found El');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Translation Handler El');
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Translation Handler Airport Phones El');
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Translation Email El');

        $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        $objPHPExcel->getActiveSheet()->protectCells('A1:A10000', '1234');
        $objPHPExcel->getActiveSheet()->getStyle('B1:S10000')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);

        $objPHPExcel->getActiveSheet()->fromArray($sheet, null, 'A2');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="site_airlines.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    /****************************************************************************************
     * Airlines::upload_excel_airlines
     ****************************************************************************************/
    function upload_excel_airlines()
    {
        $params = array();

        if(!empty($_FILES["ExcelAirlines"]["tmp_name"]))
        {
            $params["ExcelAirlines"] = $_FILES;
        }

        $results = DB_sys_central_airlines::sys_central_airlines_update_airlines($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode($results) ;
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * Airlines::upload_excel_site_airlines
     ****************************************************************************************/
    function upload_excel_site_airlines()
    {
        $params = array();

        $params['SiteID'] 	= isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : 0;

        if(!empty($_FILES["ExcelAirlines"]["tmp_name"]))
        {
            $params["ExcelAirlines"] = $_FILES;
        }

        $results = DB_sys_site_airline::sys_site_airline_update_site_airlines($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode($results) ;
        else
            return json_encode((array('error' => $results["reason"])));
    }
}
?>