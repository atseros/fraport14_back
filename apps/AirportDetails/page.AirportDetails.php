<?php
//If it is not called at root appMain.php do  nothing
if(!isset($appFrw)) exit();


require_once(realpath(__DIR__."/../../db")."/class.DB_sys_usr.php");

$UsrDetails = DB_sys_usr::get_UsrDetails($appFrw, $appFrw->UsrID);
//if($UsrDetails['UsrRole'] == 1)	exit();

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="PRAGMA" content="NO-CACHE">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Airport Details</title>
	<script type="text/javascript" src="libjs/ext-4/ext-all.js"></script>
	<link rel="stylesheet" type="text/css" href="libjs/ext-4/resources/css/ext-all-gray.css">
	<link rel="stylesheet" type="text/css" href="resources/css/header.css?dc=<?php echo microtime(true)?>">
	<link rel="stylesheet" type="text/css" href="resources/css/general.css?dc=<?php echo microtime(true)?>">
	<link rel="stylesheet" type="text/css" href="resources/css/icon.css?v1">
	<link rel="shortcut icon" href="resources/images/favicon/favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome-4.7.0/css/font-awesome.min.css">
	
	<script type="text/javascript" src="libjs/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="libjs/TinyMCETextArea/TinyMCETextArea.js"></script>
	<script type="text/javascript" src="libjs/TinyMCETextArea/My_TinyMCETextArea.js"></script>
	
	<?php 

	$menuCurrentPage = 'Airport Details';
	include('apps/Include/MainMenu.php'); 

	$header_username = $UsrDetails['UsrFullName']; 
	include('apps/Include/Header.php'); 

	?>
	
	<script type="text/javascript">
	Ext.Loader.setConfig( {enabled: true, disableCaching: true} );
	
	Ext.Loader.setPath('Ext.ux', "libjs/ext-4.ux");
	
	Ext.require([
        'Ext.form.Panel',
        'Ext.ux.form.MultiSelect',
        'Ext.ux.form.ItemSelector',
        'Ext.tip.QuickTipManager',
        'Ext.ux.ajax.JsonSimlet',
        'Ext.ux.ajax.SimManager',
        'Ext.ux.grid.FiltersFeature',
        'Ext.ux.grid.menu.ListMenu',
        'Ext.ux.grid.menu.RangeMenu',
        'Ext.ux.grid.filter.BooleanFilter',
        'Ext.ux.grid.filter.DateFilter',
        'Ext.ux.grid.filter.ListFilter',
        'Ext.ux.grid.filter.NumericFilter',
        'Ext.ux.grid.filter.StringFilter',
        'Ext.ux.grid.images.*',
        'Ext.ux.CheckColumn',
        'Ext.selection.CellModel',
        'Ext.grid.plugin.CellEditing'
	]);
	
	Ext.Loader.setPath('FileMgr', 'apps/FileMgr');
	Ext.require([ 'FileMgr.view.ExtFileList']);
	Ext.require([ 'FileMgr.view.ShowImageFile']);

	var AirportDetails = Ext.application({
		name: 'AirportDetails',
		appFolder: 'apps/AirportDetails',
	
		controllers: ['AirportDetails'],
	
		launch: function()
		{				
			var myApp = this;
			
			Ext.tip.QuickTipManager.init();
				
			Ext.util.Format.decimalSeparator = ',';
			Ext.util.Format.thousandSeparator = '.';
				
			Ext.create('Ext.container.Viewport', {
				border: 0,
				layout: 'border',
				items: [
					{
						xtype: 'Header',
						region: 'north',
					},
					{
						xtype: 'panel',
						region: 'center',
						layout: 'border',
						items: [
							{
								xtype: 'panel',
								region: 'center',
								layout: 'border',
								flex: 1,
								split: true,
								items: [
									{
										xtype: 'MList_Airport_Details',
										id: 'MList_Airport_Details',
										title: 'Airport Details',
										region: 'center',
										split: true,
										flex: 2,
										/*callbackClearRelatedBeforeLoad: function() {
											var grid = this;
											myApp.getController('Airlines').CentralAirlines_ClearRelatedBeforeReload(grid);
										},	
										listeners: {
											selectionchange: function () {
												var grid = this;
												myApp.getController('Airlines').CentralAirlines_OnSelectionChange(grid);
											}									
										}*/
									}
								]
							}
						]
					}
					
				]
			});
			
			// Load for the first time
			var grid = Ext.ComponentManager.get('MList_Airport_Details');
			grid.loadData();
		}
	});
	</script>
</head>
<body>
	
	
</body>
</html>
