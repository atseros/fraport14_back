﻿Ext.define('AirportDetails.controller.AirportDetails', {
    extend: 'Ext.app.Controller',
    alias: 'widget.AirportDetailsControler',

    views: [
		'MList_Airport_Details'
	],
	
	/*******************************************************************
	* AirportDetails_ClearRelatedBeforeReload
	********************************************************************/
	AirportDetails_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear AirportDetails Translation
		Ext.ComponentManager.get('Frm__AirportDetails_Translation').clearData();
		
	},
	
	/*******************************************************************
	* AirportDetails_OnSelectionChange
	********************************************************************/
	AirportDetails_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_C_AirportDetailsID = selectedRecord.get('C_AirportDetailsID');
		
				// Load AirportDetails Translation
				Ext.ComponentManager.get('Frm__AirportDetails_Translation').C_AirportDetailsID = selected_C_AirportDetailsID;
				Ext.ComponentManager.get('Frm__AirportDetails_Translation').loadData();
				
			}	
		}		
	}
    
});