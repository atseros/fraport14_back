﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.Wnd_NewEditTemplate']);

Ext.define('AirportDetails.view.Wnd_NewEditAirportDetails', {
    extend: 'Include.view.Wnd_NewEditTemplate',
    alias: 'widget.Wnd_NewEditAirportDetails',
		
	layout: 'border',
	title: 'AirportDetails Data',

    height: 600,
    width: 500,
	modal: true,
	

    /**************************************************************************************************
	* Form Fields
	***************************************************************************************************/
    items: 
	[
        {
            xtype: 'form',
			region: 'center',
            layout: 'border',
            defaults:
			{
				margin: '3 10 3 10',
				labelWidth: 80,
				region :'north',
           },
			items: 
			[
				// Hidden field:  AirportID
				{
					xtype: 'hiddenfield',
					name: 'AirportID',
				},
				// Text field:  AirportName
				{
					xtype: 'textfield',
					name: 'AirportName',
					fieldLabel: 'Name',
					allowBlank: false,
				},	
				// Text field:  AirportIATA
				{
					xtype: 'textfield',
					name: 'AirportIATA',
					fieldLabel: 'IATA',
					allowBlank: false,
				},	
				// Text field:  AirportICAO
				{
					xtype: 'textfield',
					name: 'AirportICAO',
					fieldLabel: 'ICAO',
					allowBlank: false,
				},	
				// Text field:  AirportCity
				{
					xtype: 'textfield',
					name: 'AirportCity',
					fieldLabel: 'City',
					allowBlank: false,
				},	
				//Text field:  AirportCountry
				{
					xtype: 'textfield',
					name: 'AirportCountry',
					fieldLabel: 'Country',
					allowBlank: false,
				},
				// Text field:  AirportLatitude
				{
					xtype: 'textfield',
					name: 'AirportLatitude',
					fieldLabel: 'Latitude',
					allowBlank: false,
				},	
				// Text field:  AirportLongitude
				{
					xtype: 'textfield',
					name: 'AirportLongitude',
					fieldLabel: 'Longitude',
					allowBlank: false,
				},	
				// Text field:  AirportAltitude
				{
					xtype: 'textfield',
					name: 'AirportAltitude',
					fieldLabel: 'Altitude',
					allowBlank: false,
				},	
				// Text field:  AirportTimezone
				{
					xtype: 'textfield',
					name: 'AirportTimezone',
					fieldLabel: 'Timezone',
					allowBlank: false,
				},
				// Text field:  AirportDST
				{
					xtype: 'textfield',
					name: 'AirportDST',
					fieldLabel: 'DST',
					allowBlank: false,
				},
				// Text field:  AirportTzDbTimeZone
				{
					xtype: 'textfield',
					name: 'AirportTzDbTimeZone',
					fieldLabel: 'TzDbTimeZone',
					allowBlank: false,
				},
				// Text field:  AirportType
				{
					xtype: 'textfield',
					name: 'AirportType',
					fieldLabel: 'Type',
					allowBlank: false,
				},
				// Text field:  AirportSource
				{
					xtype: 'textfield',
					name: 'AirportSource',
					fieldLabel: 'Source',
					allowBlank: false,
				},
				// Text field:  AirportFraSHORTNAME
				{
					xtype: 'textfield',
					name: 'AirportFraSHORTNAME',
					fieldLabel: 'Shortname',
					allowBlank: false,
				},
				// Text field:  AirportFraEN
				{
					xtype: 'textfield',
					name: 'AirportFraEN',
					fieldLabel: 'EN',
					allowBlank: false,
				},
				// Text field:  AirportFraGR
				{
					xtype: 'textfield',
					name: 'AirportFraGR',
					fieldLabel: 'GR',
					allowBlank: false,
				},
				// Text field:  AirportCountryLatitude
				{
					xtype: 'textfield',
					name: 'AirportCountryLatitude',
					fieldLabel: 'Country Latitude',
				},
				// Text field:  AirportCountryLongitude
				{
					xtype: 'textfield',
					name: 'AirportCountryLongitude',
					fieldLabel: 'Country Longitude',
				}
			]
        }        
    ],
    
	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
    initComponent: function () 
	{
        this.editRecID = 0;		
			    
		this.callbackRefresh = '';					
				
		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadDefaultParams
	***************************************************************************************************/
	getLoadDefaultParams: function () 
	{
		var params = 
		{
			appPage : 'AirportDetails',
			appMethod : 'get_AirportDetails_NewRecordDefValues'
		};
		
		return params;
	},
	
	/**************************************************************************************************
	* afterLoadDefault
	***************************************************************************************************/
	afterLoadDefault: function(data)
	{
		this.callParent(arguments);
	
		// do anything else here
		
	},
	
	/**************************************************************************************************
	* getLoadRecordParams
	***************************************************************************************************/
	getLoadRecordParams: function () 
	{
		var params = 
		{
			appPage : 'AirportDetails',
			appMethod : 'get_AirportDetailsRecord',
			AirportID : this.editRecID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* afterLoadRecord
	***************************************************************************************************/
	afterLoadRecord: function(data)
	{
		this.callParent(arguments);
		
		// do anything else here
		
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	save: function () 
	{
		var me = this;
		
		me.DataAndImage();
	},
	
	/**************************************************************************************************
	* getInsertParams
	***************************************************************************************************/
	getInsertParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* getUpdateParams
	***************************************************************************************************/
	getUpdateParams: function () 
	{
		
	},
	
	/**************************************************************************************************
	* DataAndImage
	*
	* function that uploades Images and add data
	*
	***************************************************************************************************/
	DataAndImage: function () 
	{
		var me = this;
		
		var thisf = me.query('form')[0];
		
        var form = thisf.getForm();
		
		
		if (me.editRecID == 0) {
			if (form.isValid()) {
				form.submit( {
					url: 'appAjax.php?appPage=AirportDetails&appMethod=InsertAirportDetailsRecord',
					waitMsg: ('Saving...'),
					success: function (fp, o) 
					{	
						Ext.Msg.alert('Success', 'Record has been added.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) {
						Ext.Msg.alert('Error', 'Record was not added.');
					}
				});
			}
			
		}
		else if (me.editRecID > 0) {
			if (form.isValid())  {
				form.submit( {
					url: 'appAjax.php?appPage=AirportDetails&appMethod=UpdateAirportDetailsRecord',
					waitMsg: ('Saving...'),
					success: function (fp, o) {
						Ext.Msg.alert('Success', 'Record has been updated.');
						if (typeof (me.callbackRefresh) == 'function')
						{
							me.callbackRefresh();
						}
						me.close();
					},
					failure: function (form, action) 
					{
						Ext.Msg.alert('Error', 'Record did not update.');                    
					}
				});
			}
		}
	}
	
});