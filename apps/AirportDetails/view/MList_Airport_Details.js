﻿Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

// Require Popup Form of Airline (Wnd for New Record)
Ext.require([ 'AirportDetails.view.Wnd_NewEditAirportDetails']);
//Ext.require([ 'AirportDetails.view.Wnd_UploadAirportDetails']);

Ext.define('AirportDetails.view.MList_Airport_Details' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Airport_Details',
	
	autoScroll: false,
	
	/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems: [
		// Toolbar No1 : Show All, Search, Add New, Edit, Delete
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items: [
				/*{
					xtype: 'button',
					text: 'Show Available Only',
					itemId: '',
					iconCls: 'fa fa-filter',
					enableToggle: true,
					pressed: false,
					handler: function(button) {
						var gridpanel = this.up('panel'); 
						
						if(!button.pressed) {
							gridpanel.filterShowAll = 0;
							button.setText('Show Available Only');
						}		
						else{
							gridpanel.filterShowAll = 1;
							button.setText('Show All');
						}		
							
						gridpanel.loadData();
					}					
				},*/
				{
					xtype: 'textfield'
					, labelWidth: 40
					, allowBlank: true
					, width: 250
					, fieldLabel: 'Search'
					, margin: '0 0 0 0'
					, enableKeyEvents: true
					, listeners: {
						specialkey: function (field, e) {
							if (e.getKey() == e.ENTER) {
								var gridpanel = this.up('panel');
								var val = field.getValue();
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup: {
							fn: function (field, e) {
								var gridpanel = this.up('panel');
								var val = field.getValue();
								gridpanel.filterStrToFind = val;
							}
						}
					}
				}
				,{
					 xtype: 'button'
					,text: ''
					,tooltip: 'search'
					,iconCls: 'fa fa-search'
					,listeners: {
						click: function(){ this.up('gridpanel').loadData(); }
					}					
				}
				,{
					xtype: 'tbfill'
				},
				{
					xtype: 'button',
					text: 'Add',
					iconCls: 'fa fa-plus',
					handler: function() { this.up('gridpanel').addRecord(); }
				},		
				{
					 xtype: 'button'
					,text: 'Edit'
					,iconCls: 'fa fa-pencil-square-o'
					,handler: function(){ this.up('gridpanel').editRecord(); }
				},
                {
					xtype: 'tbfill'
				},
                {
                    xtype: 'button',
                    text: 'Export Airports',
                    iconCls: 'fa fa-file-excel-o',
                    handler: function() {
                        var thisgrid = this.up('panel');

                        window.open("/appAjax.php?appPage=AirportDetails&appMethod=getAirportDetailsExportList");
                    }
                }
			]
		}
	],		

	/**************************************************************************************************
	* initComponent
	***************************************************************************************************/
	initComponent: function() 
	{
		this.filterStrToFind 	= '';
		this.filterShowAll = 0;
		this.maxRecords 		= 500;
		this.fieldNameID 		= 'AirportID';
		
		this.SiteID = 0;
		this.SiteDomainName = "";
		
		// config
		this.viewConfig = {
			markDirty:false,
			forceFit: true, 		
			preserveScrollOnRefresh: true,
			getRowClass: function(value){
				return value.get('AirportID') > 1 ? 'expanded_row' : 'expanded_row';
			}			
		};	
		
		// store
		this.store = {
			autoLoad: false,	
			autoSync: false,			
			fields: [
				 {name: 'AirportID', type: 'number', defaultValue: 0 }
				,{name: 'AirportName', type: 'string' }
				,{name: 'AirportCity', type: 'string' }
				,{name: 'AirportCountry', type: 'string' }
				,{name: 'AirportIATA', type: 'string' }
				,{name: 'AirportICAO', type: 'string' }
				,{name: 'AirportLatitude', type: 'string' }
				,{name: 'AirportLongitude', type: 'string' }
				,{name: 'AirportAltitude', type: 'string'}
				,{name: 'AirportTimezone', type: 'string'}
				,{name: 'AirportDST', type: 'string'}
				,{name: 'AirportTzDbTimeZone', type: 'string'}
				,{name: 'AirportType', type: 'string'}
				,{name: 'AirportSource', type: 'string'}
				,{name: 'AirportFraSHORTNAME', type: 'string'}
				,{name: 'AirportFraEN', type: 'string'}
				,{name: 'AirportFraGR', type: 'string'}
				,{name: 'AirportCountryLatitude', type: 'string'}
				,{name: 'AirportCountryLongitude', type: 'string'}
			],			
			proxy: {
				 type:		'ajax'
				,url:		''
				,method:	'POST'
				,timeout:	60000						
				,headers:	{ 'Content-Type' : 'application/json' }
				,reader:	{
					 type: 'json'
					,root: 'data'
				}
				,simpleSortMode: true,
			}
		};					

		// columns
		this.columns =  {
			defaults: {
				resizable: true,
				sortable: false,
			},
			items: [
				{ header: 'Name', 				dataIndex: 'AirportName', flex: 25/100,},
				{ header: 'IATA', 				dataIndex: 'AirportIATA', flex: 10/100,},
				{ header: 'Country',			dataIndex: 'AirportCountry', flex: 25/100,},
				{ header: 'City', 				dataIndex: 'AirportCity', flex: 25/100,},
				{ header: 'ICAO', 				dataIndex: 'AirportICAO', flex: 10/100,},
				{ header: 'Latitude', 			dataIndex: 'AirportLatitude', flex: 25/100,},
				{ header: 'Longitude', 			dataIndex: 'AirportLongitude', flex: 25/100,},
				{ header: 'Altitude', 			dataIndex: 'AirportAltitude', flex: 25/100,},
				{ header: 'Timezone', 			dataIndex: 'AirportTimezone', flex: 25/100,},
				{ header: 'DST', 				dataIndex: 'AirportDST', flex: 25/100,},
				{ header: 'TimeZone', 			dataIndex: 'AirportTzDbTimeZone', flex: 25/100,},
				{ header: 'Type', 				dataIndex: 'AirportType', flex: 25/100,},
				{ header: 'Source', 			dataIndex: 'AirportSource', flex: 25/100,},
				{ header: 'Shortname', 			dataIndex: 'AirportFraSHORTNAME', flex: 25/100,},
				{ header: 'EN', 				dataIndex: 'AirportFraEN', flex: 25/100,},
				{ header: 'GR', 				dataIndex: 'AirportFraGR', flex: 25/100,},
				{ header: 'Country Latitude', 	dataIndex: 'AirportCountryLatitude', flex: 25/100,},
				{ header: 'Country Longitude', 	dataIndex: 'AirportCountryLongitude', flex: 25/100,},
			]
		};
		
		// Double Click - Open form for Content
		this.on( 'itemdblclick', this.editRecord, this) ;
		
		this.callParent(arguments);		
	},
	
	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;
		
		/*if (me.filterShowAll == 1) {
			SiteID = me.SiteID;
		}
		else {
			SiteID = -1;
		}*/
		
		var params = {
			appPage: 'AirportDetails',
			appMethod: 'getAirportDetailsList',
			maxRecords: me.maxRecords,
			filterStrToFind: me.filterStrToFind,
			//SiteID: SiteID,
		};
		
		return params;
	},

	/**************************************************************************************************
	* addRecord
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;
		
		var x = new AirportDetails.view.Wnd_NewEditAirportDetails;
		x.editRecID = 0;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };	
		x.show(thisgrid);
		x.loadData();
	},
		
	/**************************************************************************************************
	* editRecord
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();
		
		if(RecID > 0)
		{
			var x = new AirportDetails.view.Wnd_NewEditAirportDetails;
			x.editRecID 	= RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };
			x.show(this);
			x.loadData();
		}
	}
});