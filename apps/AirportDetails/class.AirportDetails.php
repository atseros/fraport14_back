<?php

require_once(realpath(__DIR__."/../../db")."/class.DB_sys_airports.php");

/*********************************************************************************************
* CLASS AirportDetails
*
* DESCRIPTION:
*	Class that handles requests from AirportDetails page
*
*********************************************************************************************/
class AirportDetails
{
	private $appFrw;

	/****************************************************************************************
	* AirportDetails::CONSTRUCTOR
	****************************************************************************************/
	function __construct($appFrw)
	{
		$this->appFrw = $appFrw;
	}

	/*
		AIRLINE DETAILS
	*/

    /****************************************************************************************
     * AirportDetails::get_AirportDetails_NewRecordDefValues
     ****************************************************************************************/
    function get_AirportDetails_NewRecordDefValues()
    {
        $params = array();

        $results = DB_sys_airports::sys_airports_get_NewRecordDefValues($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * AirportDetails::InsertAirportDetailsRecord
     ****************************************************************************************/
    function InsertAirportDetailsRecord()
    {
        /*$params = array();

        $params["C_AirlinesID"] = $_REQUEST["C_AirlinesID"];
        $params["C_AirlinesName"] = $_REQUEST["C_AirlinesName"];
        $params["C_AirlinesWebsite"] = $_REQUEST["C_AirlinesWebsite"];
        // $params["C_AirlinesEmail"] = $_REQUEST["C_AirlinesEmail"];
        $params["C_AirlinesICAO"] = $_REQUEST["C_AirlinesICAO"];
        $params["C_AirlinesIATA"] = $_REQUEST["C_AirlinesIATA"];
        $params["C_AirlinesE_Check_In"] = $_REQUEST["C_AirlinesE_Check_In"];
        $params["C_AirlinesE_Booking"] = $_REQUEST["C_AirlinesE_Booking"];
        $params["C_AirlinesEscapeDefault"] = $_REQUEST["C_AirlinesEscapeDefault"];
        $params["C_AirlinesOnlineContact"] = $_REQUEST["C_AirlinesOnlineContact"];*/

        $result = DB_sys_airports::sys_airports_InsertRecord($this->appFrw, $_REQUEST);

        if ($result['success']==true)
            return json_encode($result) ;
        else
            return json_encode((array('error' => $result["reason"])));
    }

    /****************************************************************************************
     * AirportDetails::get_AirportDetailsRecord
     ****************************************************************************************/
    function get_AirportDetailsRecord()
    {
        $params["AirportID"] = $_REQUEST["AirportID"];

        $results = DB_sys_airports::sys_airports_getRecord($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * AirportDetails::UpdateAirportDetailsRecord
     ****************************************************************************************/
    function UpdateAirportDetailsRecord()
    {
        /*$params = array();

        if(isset($_REQUEST['C_AirlinesID'])) { $params['C_AirlinesID'] = $_REQUEST['C_AirlinesID']; }
        if(isset($_REQUEST['C_AirlinesName'])) { $params['C_AirlinesName'] = $_REQUEST['C_AirlinesName']; }
        if(isset($_REQUEST['C_AirlinesWebsite'])) { $params['C_AirlinesWebsite'] = $_REQUEST['C_AirlinesWebsite']; }
        // if(isset($_REQUEST['C_AirlinesEmail'])) { $params['C_AirlinesEmail'] = $_REQUEST['C_AirlinesEmail']; }
        if(isset($_REQUEST['C_AirlinesIATA'])) { $params['C_AirlinesIATA'] = $_REQUEST['C_AirlinesIATA']; }
        if(isset($_REQUEST['C_AirlinesICAO'])) { $params['C_AirlinesICAO'] = $_REQUEST['C_AirlinesICAO']; }
        if(isset($_REQUEST['C_AirlinesE_Check_In'])) { $params['C_AirlinesE_Check_In'] = $_REQUEST['C_AirlinesE_Check_In']; }
        if(isset($_REQUEST['C_AirlinesE_Booking'])) { $params['C_AirlinesE_Booking'] = $_REQUEST['C_AirlinesE_Booking']; }
        if(isset($_REQUEST['C_AirlinesEscapeDefault'])) { $params['C_AirlinesEscapeDefault'] = $_REQUEST['C_AirlinesEscapeDefault']; }
        if(isset($_REQUEST['C_AirlinesOnlineContact'])) { $params['C_AirlinesOnlineContact'] = $_REQUEST['C_AirlinesOnlineContact']; }
*/
        $result = DB_sys_airports::sys_airports_UpdateRecord($this->appFrw, $_REQUEST);

        if ($result['success']==true)
            return json_encode($result) ;
        else
            return json_encode((array('error' => $result["reason"])));
    }

    /****************************************************************************************
     * AirportDetails::getAirportDetailsList
     ****************************************************************************************/
    function getAirportDetailsList()
    {
        $params = array();

        $params["maxRecords"] = $_REQUEST["maxRecords"];
        $params['filterStrToFind'] = isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
        $params['SiteID'] = isset($_REQUEST['SiteID']) ? $_REQUEST['SiteID'] : -1;

        $results = DB_sys_airports::sys_airports_get_List($this->appFrw, $params);

        if ($results['success']==true)
            return json_encode((array('data' => $results["data"])));
        else
            return json_encode((array('error' => $results["reason"])));
    }

    /****************************************************************************************
     * AirportDetails::getAirportDetailsExportList
     ****************************************************************************************/
    function getAirportDetailsExportList()
    {
        require_once(realpath(__DIR__."/../../libphp/PHPExcel_1.8.0_doc")."/PHPExcel.php");
        require_once(realpath(__DIR__."/../../libphp/PHPExcel_1.8.0_doc")."/PHPExcel/Writer/Excel2007.php");

        $params = array();

        $results = DB_sys_airports::sys_airports_get_ExportList($this->appFrw, $params);
        //echo '<pre>';print_r($results);echo '</pre>';exit();

        $sheet = array();

        foreach($results["data"] as $row)
        {
            $line = array();

            $line[1] = $row["AirportID"];
            $line[2] = $row["AirportName"];
            $line[3] = $row["AirportCity"];
            $line[4] = $row["AirportCountry"];
            $line[5] = $row["AirportIATA"];
            $line[6] = $row["AirportICAO"];
            $line[7] = $row["AirportLatitude"];
            $line[8] = $row["AirportLongitude"];
            $line[9] = $row["AirportAltitude"];
            $line[10] = $row["AirportTimezone"];
            $line[11] = $row["AirportDST"];
            $line[12] = $row["AirportTzDbTimeZone"];
            $line[13] = $row["AirportType"];
            $line[14] = $row["AirportSource"];
            $line[15] = $row["AirportFraSHORTNAME"];
            $line[16] = $row["AirportFraEN"];
            $line[17] = $row["AirportFraGR"];
            $line[18] = $row["AirportCountryLatitude"];
            $line[19] = $row["AirportCountryLongitude"];

            array_push($sheet, $line);
        }
        //echo '<pre>';print_r($sheet);echo '</pre>';exit();

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        for($col = 'A'; $col !== 'C'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setTitle('Airports');
        }

        $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'City');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Country');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'IATA');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ICAO');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Latitude');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Longitude');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Altitude');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Timezone');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'DST');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'TzDbTimeZone');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Type');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Source');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'FraSHORTNAME');
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'FraEN');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'FraGR');
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'CountryLatitude');
        $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'CountryLongitude');

        //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        //$objPHPExcel->getActiveSheet()->protectCells('A1:A10000', '1234');
        //$objPHPExcel->getActiveSheet()->getStyle('B1:B10000')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
        //$objPHPExcel->getActiveSheet()->protectCells('C1:D10000', '1234');
        //$objPHPExcel->getActiveSheet()->getStyle('E1:O10000')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);

        $objPHPExcel->getActiveSheet()->fromArray($sheet, null, 'A2');

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="airports.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}
