<?php

require_once(realpath(__DIR__."/../../db")."/class.DB_sys_central_media.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_central_medialng.php");

/*********************************************************************************************
* CLASS CentralMediaGallery
*
* DESCRIPTION: 
*	Class that handles requests from CentralMediaGallery page 
*
*********************************************************************************************/
class CentralMediaGallery
{
	private $appFrw;
	
	/****************************************************************************************
	* CentralMediaGallery::CONSTRUCTOR
	****************************************************************************************/
	function CentralMediaGallery($appFrw)
	{
		$this->appFrw = $appFrw;
	}
	
	/*
		CENTRAL MEDIA
	*/
	
	/****************************************************************************************
	* CentralMediaGallery::get_CentralMedia_NewRecordDefValues
	****************************************************************************************/
	function get_CentralMedia_NewRecordDefValues()
	{
		$params = array();
	
		$params["CentralMediaType"] = $_REQUEST["CentralMediaType"];
		
		$results = DB_sys_central_media::sys_central_media_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* CentralMediaGallery::InsertCentralMediaRecord
	****************************************************************************************/
	function InsertCentralMediaRecord()
	{
		$params = array();
		
		$params["CentralMediaID"] = $_REQUEST["CentralMediaID"];
		$params["CentralMediaTitle"] = $_REQUEST["CentralMediaTitle"];
		$params["CentralMediaType"] = $_REQUEST["CentralMediaType"];

		
		if(!empty($_FILES["File"]["tmp_name"]) && $params["CentralMediaType"] == 1)
		{
			$params["CentralMediaFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_media', $params["CentralMediaID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
			
			$menu = 'menu-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(560, 315, "$folder/$uploaded_filename", "$folder/$menu");
		}
		else if($_REQUEST["CentralMediaUrl"] !="" && $params["CentralMediaType"] == 2 )
		{
			// Get Youtube Code
			parse_str(parse_url($_REQUEST["CentralMediaUrl"], PHP_URL_QUERY), $ID_youtube);
			$CentralMediaUrl = $ID_youtube['v'];
			
			// Get Still Image
			//$url   = 'http://img.youtube.com/vi/'.$CvdUrl.'/maxresdefault.jpg';
			
			$params["CentralMediaFilename"] = $CentralMediaUrl;
			
		}	
		else if(!empty($_FILES["File"]["tmp_name"]) && $params["CentralMediaType"] == 3)
		{
			$params["CentralMediaFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_media', $params["CentralMediaID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
		}		
			
		
		$result = DB_sys_central_media::sys_central_media_InsertRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));

	}
	
	/****************************************************************************************
	* CentralMediaGallery::get_CentralMediaRecord
	****************************************************************************************/
	function get_CentralMediaRecord()
	{
		$params["CentralMediaID"] = $_REQUEST["CentralMediaID"];
			
		$results = DB_sys_central_media::sys_central_media_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* CentralMediaGallery::UpdateCentralMediaRecord
	****************************************************************************************/
	function UpdateCentralMediaRecord()
	{
		$params = array();

		if(isset($_REQUEST['CentralMediaID'])) { $params['CentralMediaID'] = $_REQUEST['CentralMediaID']; }
		if(isset($_REQUEST['CentralMediaTitle'])) { $params['CentralMediaTitle'] = $_REQUEST['CentralMediaTitle']; }
		if(isset($_REQUEST['CentralMediaType'])) { $params['CentralMediaType'] = $_REQUEST['CentralMediaType']; }
	
			
		if(!empty($_FILES["File"]["tmp_name"]) && $params["CentralMediaType"] == 1)
		{
			$params["CentralMediaFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_media', $params["CentralMediaID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
			$thumb = 'thumb-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(100, 100, "$folder/$uploaded_filename", "$folder/$thumb");
			
			$menu = 'menu-' . $_FILES["File"]["name"];
			
			FileManager::resize_crop_image(560, 315, "$folder/$uploaded_filename", "$folder/$menu");
		}
		else if($_REQUEST["CentralMediaUrl"] !="" && $params["CentralMediaType"] == 2 )
		{
			// Get Youtube Code
			parse_str(parse_url($_REQUEST["CentralMediaUrl"], PHP_URL_QUERY), $ID_youtube);
			$CentralMediaUrl = $ID_youtube['v'];
			
			// Get Still Image
			//$url   = 'http://img.youtube.com/vi/'.$CvdUrl.'/maxresdefault.jpg';
			
			$params["CentralMediaFilename"] = $CentralMediaUrl;
			
		}	
		else if(!empty($_FILES["File"]["tmp_name"]) && $params["CentralMediaType"] == 3)
		{
			$params["CentralMediaFilename"] = $_FILES["File"]["name"];
			
			$folder = FileManager::getTblFolderPath('sys_central_media', $params["CentralMediaID"] )."/";
			
			$tmp_uploaded_file = $_FILES["File"]["tmp_name"];
			$uploaded_filename = $_FILES["File"]["name"];
			
			move_uploaded_file($tmp_uploaded_file, "$folder/$uploaded_filename");
			
		}				
		
		$result = DB_sys_central_media::sys_central_media_UpdateRecord($this->appFrw, $params);
		
		if ($result['success']==true)
			return json_encode($result) ;
		else 
			return json_encode((array('error' => $result["reason"])));
	}
	
	/****************************************************************************************
	* CentralMediaGallery::DeleteCentralMediaRecord
	****************************************************************************************/
	function DeleteCentralMediaRecord()
	{
		$params = array();
		
		$params["CentralMediaID"] = $_REQUEST["CentralMediaID"];
		
		$results = DB_sys_central_media::sys_central_media_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	
	/****************************************************************************************
	* CentralMediaGallery::getCentralMediaList
	****************************************************************************************/
	function getCentralMediaList()
	{
		$params = array();
		
		$params["maxRecords"]	= $_REQUEST["maxRecords"];
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : "";
		$params['CentralMediaType'] 	= isset($_REQUEST['CentralMediaType']) ? $_REQUEST['CentralMediaType'] : 0;
		
		$results = DB_sys_central_media::sys_central_media_getList($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		CENTRAL MEDIA LNG
	*/
	
	/****************************************************************************************
	* CentralMediaGallery::get_CentralMediaLngRecord
	****************************************************************************************/
	function get_CentralMediaLngRecord()
	{
		//Check if Record Exists
		$params["CentralMediaLngCentralMediaID"] = $_REQUEST["CentralMediaLngCentralMediaID"];
		$params["CentralMediaLngType"] = $_REQUEST["CentralMediaLngType"];
		
		
		$CheckIfCentralMediaLngExists = DB_sys_central_medialng::sys_central_medialng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfCentralMediaLngExists == 0)
		{
			$CentralMediaLngDefValues = DB_sys_central_medialng::sys_central_medialng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["CentralMediaLngID"] = $CentralMediaLngDefValues['data']['CentralMediaLngID'];
			
			$result = DB_sys_central_medialng::sys_central_medialng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_central_medialng::sys_central_medialng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$CentralMediaLngID = DB_sys_central_medialng::sys_central_medialng_GetCentralMediaLngID($this->appFrw, $params);
			
			$params["CentralMediaLngID"] = $CentralMediaLngID; 
			
			$results = DB_sys_central_medialng::sys_central_medialng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
		
		
	}
	
	/****************************************************************************************
	* CentralMediaGallery::UpdateCentralMediaLngRecord
	****************************************************************************************/
	function UpdateCentralMediaLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['CentralMediaLngID'])) { $params['CentralMediaLngID'] = $_REQUEST['CentralMediaLngID']; }
		if(isset($_REQUEST['CentralMediaLngType'])) { $params['CentralMediaLngType'] = $_REQUEST['CentralMediaLngType']; }
		if(isset($_REQUEST['CentralMediaLngMediaID'])) { $params['CentralMediaLngMediaID'] = $_REQUEST['CentralMediaLngMediaID']; }
		if(isset($_REQUEST['CentralMediaLngTitle'])) { $params['CentralMediaLngTitle'] = $_REQUEST['CentralMediaLngTitle']; }
		
		$results = DB_sys_central_medialng::sys_central_medialng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
}
?>