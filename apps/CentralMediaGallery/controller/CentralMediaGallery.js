﻿Ext.define('CentralMediaGallery.controller.CentralMediaGallery', {
    extend: 'Ext.app.Controller',
    alias: 'widget.CentralMediaGalleryControler',

    models: [],
    stores: [],
    views: [
				'MList_Central_Images'
				,'Frm_Central_Images_Translation'
				,'MList_Central_Videos'
				,'Frm_Central_Videos_Translation'
				,'MList_Central_Documents'
				,'Frm_Central_Documents_Translation'
			  ],
	
	
	
    /*******************************************************************
	* Images_ClearRelatedBeforeReload
	********************************************************************/
	Images_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear Images Details
		Ext.ComponentManager.get('Frm_Central_Images_Translation').clearData();
		
	},
	
	/*******************************************************************
	* Images_OnSelectionChange
	********************************************************************/
	Images_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_CentralMediaID = selectedRecord.get('CentralMediaID');
		
				// Load Images Details
				Ext.ComponentManager.get('Frm_Central_Images_Translation').CentralMediaID = selected_CentralMediaID;
				Ext.ComponentManager.get('Frm_Central_Images_Translation').loadData();
			
				
			}	
		}		
	},

   /*******************************************************************
	* Videos_ClearRelatedBeforeReload
	********************************************************************/
	Videos_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear Images Details
		Ext.ComponentManager.get('Frm_Central_Videos_Translation').clearData();
		
	},
	
	/*******************************************************************
	* Videos_OnSelectionChange
	********************************************************************/
	Videos_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_CentralMediaID = selectedRecord.get('CentralMediaID');
		
				// Load Images Details
				Ext.ComponentManager.get('Frm_Central_Videos_Translation').CentralMediaID = selected_CentralMediaID;
				Ext.ComponentManager.get('Frm_Central_Videos_Translation').loadData();
			
				
			}	
		}		
	},

  /*******************************************************************
	* Documents_ClearRelatedBeforeReload
	********************************************************************/
	Documents_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear Images Details
		Ext.ComponentManager.get('Frm_Central_Documents_Translation').clearData();
		
	},
	
	/*******************************************************************
	* Documents_OnSelectionChange
	********************************************************************/
	Documents_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_CentralMediaID = selectedRecord.get('CentralMediaID');
		
				// Load Images Details
				Ext.ComponentManager.get('Frm_Central_Documents_Translation').CentralMediaID = selected_CentralMediaID;
				Ext.ComponentManager.get('Frm_Central_Documents_Translation').loadData();
			
				
			}	
		}		
	},

	
	
});