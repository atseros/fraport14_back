﻿Ext.define('Dictionary.controller.Dictionary', {
    extend: 'Ext.app.Controller',
    alias: 'widget.DictionaryControler',

    models: [],
    stores: [],
    views: [
				'MList_Dictionary'
				,'Frm_Dictionary_Translation'
		
			],
			
	/*******************************************************************
	* Dictionary_ClearRelatedBeforeReload
	********************************************************************/
	Dictionary_ClearRelatedBeforeReload: function(grid)
	{	
		// Clear Dictionary Translation
		Ext.ComponentManager.get('Frm_Dictionary_Translation').clearData();
		
	},
	
	/*******************************************************************
	* Dictionary_OnSelectionChange
	********************************************************************/
	Dictionary_OnSelectionChange: function(grid)
	{
		var sr = grid.getSelectionModel().getSelection();
		var selectedRecord;											
		if(sr.length > 0)
		{
			var selectedRecord = sr[0];
			if (!Ext.isEmpty(selectedRecord)) 
			{
				var selected_DicID = selectedRecord.get('DicID');
		
				// Load Image gallery Translation
				Ext.ComponentManager.get('Frm_Dictionary_Translation').DicID = selected_DicID;
				Ext.ComponentManager.get('Frm_Dictionary_Translation').loadData();
				
			}	
		}		
	},			
	

});