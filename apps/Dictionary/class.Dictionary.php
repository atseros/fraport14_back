<?php

require_once(realpath(__DIR__."/../../db")."/class.DB_sys_dictionary.php");
require_once(realpath(__DIR__."/../../db")."/class.DB_sys_dictionarylng.php");


/*********************************************************************************************
* CLASS Dictionary
*
* DESCRIPTION: 
*	Class that handles requests from Dictionary page 
*
*********************************************************************************************/
class Dictionary
{
	private $appFrw;
	
	/****************************************************************************************
	* Dictionary::CONSTRUCTOR
	****************************************************************************************/
	function Dictionary($appFrw)
	{
		$this->appFrw = $appFrw;
	}
	
	/*
		DICTIONARY
	*/
	
	
	/****************************************************************************************
	* Home::get_Dictionary_NewRecordDefValues
	****************************************************************************************/
	function get_Dictionary_NewRecordDefValues()
	{
		$params = array();
		
		$results = DB_sys_dictionary::sys_dictionary_get_NewRecordDefValues($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::InsertDictionaryRecord
	****************************************************************************************/
	function InsertDictionaryRecord()
	{
		$params = array();
		
		$params["DicID"] = $_REQUEST["DicID"];
		$params["DicName"] = $_REQUEST["DicName"];
		
		$results = DB_sys_dictionary::sys_dictionary_InsertRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_DictionaryRecord
	****************************************************************************************/
	function get_DictionaryRecord()
	{
		$params["DicID"] = $_REQUEST["DicID"];
						
		$results = DB_sys_dictionary::sys_dictionary_getRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::UpdateDictionaryRecord
	****************************************************************************************/
	function UpdateDictionaryRecord()
	{
		$params = array();
		
		if(isset($_REQUEST['DicID'])) { $params['DicID'] = $_REQUEST['DicID']; }
		if(isset($_REQUEST['DicName'])) { $params['DicName'] = $_REQUEST['DicName']; }
		
		$results = DB_sys_dictionary::sys_dictionary_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::DeleteDictionaryRecord
	****************************************************************************************/
	function DeleteDictionaryRecord()
	{
		$params = array();
		
		$params["DicID"] = $_REQUEST["DicID"];
		
		$results = DB_sys_dictionary::sys_dictionary_DeleteRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/****************************************************************************************
	* Home::get_DictionaryList
	****************************************************************************************/
	function get_DictionaryList()
	{
		$params = array();
		
		$params['filterStrToFind'] 	= isset($_REQUEST['filterStrToFind']) ? $_REQUEST['filterStrToFind'] : '';
		
		$results = DB_sys_dictionary::sys_dictionary_get_List($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}
	
	/*
		DICTIONARY LNG
	*/
	
	/****************************************************************************************
	* Home::get_DictionaryLngRecord
	****************************************************************************************/
	function get_DictionaryLngRecord()
	{	
		//Check if Record Exists
		$params["DicLngDicID"] = $_REQUEST["DicLngDicID"];
		$params["DicLngType"] = $_REQUEST["DicLngType"];
		
		
		$CheckIfDictionaryLngExists = DB_sys_dictionarylng::sys_dictionarylng_CheckBeforeInsert($this->appFrw, $params);
		
		if ($CheckIfDictionaryLngExists == 0)
		{
			$DictionaryLngDefValues = DB_sys_dictionarylng::sys_dictionarylng_get_NewRecordDefValues($this->appFrw, $params);
			
			$params["DicLngID"] = $DictionaryLngDefValues['data']['DicLngID'];
			
			$result = DB_sys_dictionarylng::sys_dictionarylng_InsertRecord($this->appFrw, $params);	
			
			if($result > 0)
			{
				$results = DB_sys_dictionarylng::sys_dictionarylng_getRecord($this->appFrw, $params);
			
				if ($results['success']==true)
				return json_encode((array('data' => $results["data"])));
				else 
				return json_encode((array('error' => $results["reason"])));
			}
			
		}
		else
		{
			$DicLngID = DB_sys_dictionarylng::sys_dictionarylng_GetDicLngID($this->appFrw, $params);
			
			$params["DicLngID"] = $DicLngID; 
			
			$results = DB_sys_dictionarylng::sys_dictionarylng_getRecord($this->appFrw, $params);
			
			if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
			else 
			return json_encode((array('error' => $results["reason"])));
		}
	}
	
	// /****************************************************************************************
	// * Home::UpdateDictionaryLngRecord
	// ****************************************************************************************/
	function UpdateDictionaryLngRecord()
	{
		$params = array();

		if(isset($_REQUEST['DicLngID'])) { $params['DicLngID'] = $_REQUEST['DicLngID']; }
		if(isset($_REQUEST['DicLngType'])) { $params['DicLngType'] = $_REQUEST['DicLngType']; }
		if(isset($_REQUEST['DicLngDicID'])) { $params['DicLngDicID'] = $_REQUEST['DicLngDicID']; }
		if(isset($_REQUEST['DicLngName'])) { $params['DicLngName'] = $_REQUEST['DicLngName']; }
			
		$results = DB_sys_dictionarylng::sys_dictionarylng_UpdateRecord($this->appFrw, $params);
		
		if ($results['success']==true)
			return json_encode((array('data' => $results["data"])));
		else 
			return json_encode((array('error' => $results["reason"])));
	}	
		
	
}
?>