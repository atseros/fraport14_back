﻿// Require Popup Form of Dictionary (Wnd for New - Edit)
Ext.require([ 'Dictionary.view.Wnd_NewEditDictionary']);

Ext.Loader.setPath('Include', "apps/Include");
Ext.require([ 'Include.view.ListTemplate']);

Ext.define('Dictionary.view.MList_Dictionary' ,{
    extend: 'Include.view.ListTemplate',
    alias : 'widget.MList_Dictionary',

	autoScroll: false,
	layout: 'border',


/**************************************************************************************************
	* Toolbars
	***************************************************************************************************/
	dockedItems:
	[
		// Toolbar No1 : Search,
		{
			 xtype:		'toolbar'
			,dock:		'top'
			,layout:	{ type: 'hbox' }
			,items:
			[

				{
					 xtype:				'textfield'
					,labelWidth:		40
					,allowBlank:		true
					,width:				200
					,fieldLabel:		'Search'
					,margin:			'0 0 0 0'
					,enableKeyEvents:	true
					,listeners:
					{
						specialkey: function(field,e)
						{
							if (e.getKey() == e.ENTER)
							{
								var gridpanel = this.up('panel');
								var val = field.getValue();
								gridpanel.filterStrToFind = val;
								gridpanel.loadData();
							}
						},
						keyup:
						{
							fn: function(field,e)
							{
								var gridpanel = this.up('panel');
								var val = field.getValue();
								gridpanel.filterStrToFind = val;
							}

						}
					}
				}
				,{
					 xtype:		'button'
					,text:		''
					,tooltip:	'search'
					,iconCls:	'fa fa-search'
					,listeners:
					{
						click: function()
						{
							var gridpanel = this.up('panel');
							gridpanel.loadData();
						}
					}
				},

			]
		},
		//Toolbar No2 : Add New, Edit, Delete
		 {
		 xtype:		'toolbar'
		 ,dock:		'top'
		 ,layout:	{ type: 'hbox' }
		 ,items:
		 [

		 {
		 xtype: 'button',
		 text: 'Add',
		 iconCls: 'fa fa-plus',
		 handler: function()
		 {
		 var thisgrid = this.up('panel');
		 thisgrid.addRecord();
		 }
		 },
		 ,{
		 xtype:		'button'
		 ,text:		'Edit'
		 ,iconCls: 'fa fa-pencil-square-o'
		 ,handler:	function()
		 {
		 var thisgrid = this.up('panel');
		 thisgrid.editRecord();
		 }
		 }
		//  ,{
		// 			 xtype:		'button'
		// 			 ,text:		'Delete'
		// 			 ,iconCls: 'fa fa-trash'
		// 			 ,handler:	function()
		// 			 {
		// 				 var thisgrid = this.up('panel');
		// 				 thisgrid.deleteRecord();
		// 			 }
		// 		 }
			 ]
		 },

	],

	/**************************************************************************************************
	* initComponent
	*
	* Config, Store, Columns, Renderers
	*
	***************************************************************************************************/
	initComponent: function()
	{
		this.filterStrToFind = '';

		this.fieldNameID = 'DicID';

		// config
		this.viewConfig =
		{
			markDirty:false,
			preserveScrollOnRefresh: true,
			getRowClass: function(value)
			{
				return value.get('DicID') > 1 ? 'expanded_row' : 'expanded_row';
			}
		};

		// store
		this.store =
		{
			autoLoad: false,
			autoSync: false,
			fields:
			[
				 {name: 'DicID', type: 'number', defaultValue: 0 }
				,{name: 'DicName', type: 'string'}

			],
			proxy:
			{
				type: 'ajax',
				url: '',
				method : 'POST',
				timeout : 60000,
				headers : { 'Content-Type' : 'application/json' },
				reader: {
					type: 'json',
					root: 'data'
				},
				simpleSortMode: true,
			},
		};

		// columns
		this.columns =
		{
			defaults:
			{
				resizable: true,
				sortable: true,
			},
			items:
			[
				{header: ' Name', dataIndex: 'DicName',  flex: 100/100}

			]
		};

		// Double Click - Open form for Edit
		this.on( 'itemdblclick', this.editRecord, this) ;

		this.callParent(arguments);
	},

	/**************************************************************************************************
	* getLoadParams
	***************************************************************************************************/
	getLoadParams: function()
	{
		var me = this;

		var params =
		{
			appPage: 'Dictionary',
			appMethod: 'get_DictionaryList',
			filterStrToFind: me.filterStrToFind,

		};

		return params;
	},

	/**************************************************************************************************
	* addRecord
	*
	* Button handler for Add New
	*
	***************************************************************************************************/
	addRecord: function()
	{
		var thisgrid = this;

		var x = new Dictionary.view.Wnd_NewEditDictionary;
		x.editRecID = 0;
		x.callbackRefresh = function(recID) { thisgrid.loadData(); };
		x.show(thisgrid);
		x.loadData();
	},

	/**************************************************************************************************
	* editRecord
	*
	* Button handler for Edit
	***************************************************************************************************/
	editRecord: function()
	{
		var thisgrid	= this;
		var RecID 		= thisgrid.getSelectedRecordID();

		if(RecID > 0)
		{
			var x = new Dictionary.view.Wnd_NewEditDictionary;
			x.editRecID = RecID;
			x.callbackRefresh = function( recID ){ thisgrid.loadData(); };
			x.show(this);
			x.loadData();
		}
	},

	/**************************************************************************************************
	* getDeleteRecordParams
	*
	* params for Ajax request that deletes a record
	*
	***************************************************************************************************/
	getDeleteRecordParams: function ()
	{
		var thisgrid 	= this;
		var params		= null;
		var RecID 		= thisgrid.getSelectedRecordID();

		if(RecID > 0)
		{
			params =
			{
				appPage		: 'Dictionary',
				appMethod		: 'DeleteDictionaryRecord',
				DicID			: RecID
			};
		}

		return params;
	},

	/**************************************************************************************************
	* deleteRecord
	*
	* Button handler for Delete
	*
	***************************************************************************************************/
	deleteRecord: function()
	{
		var thisgrid = this;

		var params = thisgrid.getDeleteRecordParams();

		if(params)
		{
			Ext.Msg.confirm('Confirm Deletion', 'Are you sure you want to delete this record?',
				function(btn)
				{
					if (btn === 'yes')
					{
						Ext.Ajax.request({
							url: 'appAjax.php',
							method: 'post',
							params:  params,
							success: function(response)
							{
								var text = response.responseText;
								var obj = Ext.JSON.decode(text);
								if (!obj.data)
								{
									Ext.Msg.alert('Not deleted!', obj.error);
									return;
								}

								thisgrid.loadData();
							}
						});
						return true;
					}
					else
					{
						return false;
					}
				});
		}

	},








});
