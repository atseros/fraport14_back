<?php
//If it is not called at root appMain.php do  nothing
if(!isset($appFrw)) exit();

$tblName	= $_REQUEST["tblName"];
$recID		= $_REQUEST["recID"];

// ΠΕΡΙΓΡΑΦΗ - ΤΙΤΛΟΣ Filemanager
$fldShortName			= "";
$fldFullName			= "";

// Company
if( $tblName == "appCmp" AND $recID > 0)
{
	require_once(realpath(__DIR__."db")."/class.DB_appCmp.php");
	$results = DB_appCmp::get_appCmpFullName($appFrw, array('CmpID'=>$recID));
	if($results['success'] == true)
		$fldFullName = 'Company: '.$results['data'];
}


$fldShortName = substr($fldFullName, 0 , 100);


$handle_url_list 		= "appAjax.php?appPage=FileMgr&appMethod=webix_list_folder&tblName=".$tblName."&recID=".$recID;
$handle_url_upload 		= "appAjax.php?appPage=FileMgr&appMethod=webix_upload&tblName=".$tblName."&recID=".$recID;
$handle_url_download	= "appAjax.php?appPage=FileMgr&appMethod=webix_download&tblName=".$tblName."&recID=".$recID;
$handle_url_copy		= "appAjax.php?appPage=FileMgr&appMethod=webix_copy&tblName=".$tblName."&recID=".$recID;
$handle_url_move		= "appAjax.php?appPage=FileMgr&appMethod=webix_move&tblName=".$tblName."&recID=".$recID;
$handle_url_remove		= "appAjax.php?appPage=FileMgr&appMethod=webix_remove&tblName=".$tblName."&recID=".$recID;
$handle_url_rename		= "appAjax.php?appPage=FileMgr&appMethod=webix_rename&tblName=".$tblName."&recID=".$recID;
$handle_url_create		= "appAjax.php?appPage=FileMgr&appMethod=webix_create&tblName=".$tblName."&recID=".$recID;


	
?>

<!doctype html>
<html>
<head>
<title><?php echo($fldShortName);?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Cache-Control" content="no-cache">
	<meta http-equiv="Expires" content="-1">
	<meta  name = "viewport" content = "initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no">
	<link rel="shortcut icon" href="resources/images/folder.png">
	
	<script src="libjs/webix/webix.js" type="text/javascript"></script>
	<script src="libjs/webix/filemanager/filemanager.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="libjs/webix/webix.css">
	<link rel="stylesheet" type="text/css" href="libjs/webix/filemanager/filemanager.css">


</head>
<body>

<style>
.header_style  
{
	font-weight: bold;
	font-size: 18px;
	text-align: bottom;
}
</style>

<script type="text/javascript">

	// folder name
	var fldFullName		= "<?php echo($fldFullName);?>";
	// variables for copy - cut - paste
	var mode = "";
	var source = "";
	// handler url 
	var handle_url_list 	= "<?php echo($handle_url_list);?>";
	var handle_url_upload 	= "<?php echo($handle_url_upload);?>";
	var handle_url_download	= "<?php echo($handle_url_download);?>";
	var handle_url_copy		= "<?php echo($handle_url_copy);?>";
	var handle_url_move		= "<?php echo($handle_url_move);?>";
	var handle_url_remove	= "<?php echo($handle_url_remove);?>";
	var handle_url_rename	= "<?php echo($handle_url_rename);?>";
	var handle_url_create	= "<?php echo($handle_url_create);?>";
							
	// Max Upload file size in MB
	var max_upload_size = 50;
	
	webix.ready(function()
	{		
		/***************************************************************************************************
		* Button Open
		*****************************************************************************************************/
		//var button = webix.ui( { view:"button", container:"btn_div", label: "Open" } );
		
		/***************************************************************************************************
		*  File Manager
		*****************************************************************************************************/
		webix.ui(
		{

			rows:
			[
				{ template: '<span>'+fldFullName+'</span><hr><br/>', height:45, css:"header_style" },
				{ view:"button", id:"btnOpen", label: "Open" },
				
				{
						view:"filemanager",
						id:"files",
						handlers:{
							"upload" 	: "<?php echo($handle_url_upload);?>",
							"download"	: "<?php echo($handle_url_download);?>",
							"copy"		: "<?php echo($handle_url_copy);?>",
							"move"		: "<?php echo($handle_url_remove);?>",
							"remove"	: "<?php echo($handle_url_remove);?>",
							"rename"	: "<?php echo($handle_url_rename);?>",
							"create"	: "<?php echo($handle_url_create);?>"
						},						
						uploadProgress: 
						{
							type:"top",							
							delay:3000,
							hide:true
						}
			}
			
			]
		});

		   
				
		/***************************************************************************************************
		* Init
		*****************************************************************************************************/
		fmanager = $$("files");
		button = $$("btnOpen");
		fmanager.load(handle_url_list);
		
		webix.extend(fmanager, webix.OverlayBox);
		
		var uploader = fmanager.getUploader();	
					
		/***************************************************************************************************
		* Handle Button Open
		*****************************************************************************************************/		
		button.attachEvent("onItemClick", function(id, e){
			
			var id  = fmanager.$$("table").getSelectedId();
			if(id)
			{
				var item = fmanager.getItem(id);
				//alert( Object.keys(item ) );
				//alert('id= ' + item.id  + '\r\n' + 'value= ' + item.value + '\r\n' + 'type = ' + item.type );
				
				if(item)
				{
					if( item.type == 'folder')
					{
						fmanager.setPath(item.id);
						return true;						
					}
					else
					{	
						if(item.id)
						{
													
							
							var windowUrl = handle_url_download + "&forceOpen=1" + "&source=" + encodeURIComponent(item.id);

							var windowId = item.value + new Date().getTime();
							var windowFeatures = 
								'channelmode=no,directories=no,fullscreen=no,' +
								'location=no,dependent=yes,menubar=no,resizable=no,scrollbars=yes,' + 
								'status=no,toolbar=no,titlebar=no,' +
								'left=0,top=0,width=400px,height=200px';
							//var windowReference = window.open(windowUrl, windowId, windowFeatures);
							var windowReference = window.open( windowUrl );
 
							//$file_down_url = down_url + "&FilePath=" + item.id;
							//window.open($file_down_url, "_blank");

						}
					}
				}
			}
			
			
		});
		
		/***************************************************************************************************
		* IHandle After Upload --- Refresh after done
		*****************************************************************************************************/
		uploader.attachEvent("onBeforeFileAdd", function(item){
			var mb = item.size /(1024*1024);
			mb = Math.round( mb * 100) /100;
			if(mb > max_upload_size)
			{
				webix.alert("File is " + mb + " MB.<br/> It is too big.<br/> Max Upload File size = "+ max_upload_size + "MB" );
				return false;
			}
			estim_duration = item.size / 100 ;
			
			fmanager.config.uploadProgress.delay = estim_duration ;		
			
			fmanager.showOverlay("<div><span style=\"color:red; font-weight:bold;\">Please wait while uploading files ...</span></div>");
			
			return true;
		});		
				
		uploader.attachEvent("onUploadComplete", function(response)
		{
			if( response.status != "server")
			{
					webix.alert("Error Uploading " + response.file);
			}
			fmanager.hideOverlay();			
			//handle_url_upload = handle_url_upload+"1";
			//fmanager.clearAll();
			//fmanager.load(handle_url_list);
			location.reload();
			
			return true;
		});		
				

		/***************************************************************************************************
		* Handle After Rename -- refresh after
		*****************************************************************************************************/
		fmanager.attachEvent("onAfterEditStop",function(id,state,editor,view)
		{
			//location.reload();
			//handle_url_rename = handle_url_rename+"1";
			webix.ajax().post(handle_url_rename, { action:"rename", source:id, target:state.value },{
							error:function(text, data, XmlHttpRequest){ alert("Απέτυχε η αλλαγή ονόματος"); fmanager.clearAll(); fmanager.load(handle_url_list); },
							success:function(text, data, XmlHttpRequest){ location.reload() }
						}
					); 
					
			return false;
		});
		
		/***************************************************************************************************
		* Handle Before Create
		*****************************************************************************************************/
		fmanager.attachEvent("onBeforeCreateFolder",function(id)
		{
			//fmanager.createFolder(id);
			
			//handle_url_create = handle_url_create+"1";			
			webix.ajax().post(handle_url_create, { action:"create", source:"NewFolder", target:id },{
							error:function(text, data, XmlHttpRequest){ alert("Απέτυχε η δημιουργία"); fmanager.clearAll(); fmanager.load(handle_url_list); },
							success:function(text, data, XmlHttpRequest){ location.reload();}
						}
					); 

			return false;
		});
		
		
		/***************************************************************************************************
		* Handle Before Copy -- show what is copied
		*****************************************************************************************************/
		fmanager.attachEvent("onBeforeMarkCopy", function(ids)
		{
			source = ids;
			mode = "copy";
			webix.message("Copy: "+source);
			return true;
		});
		
		/***************************************************************************************************
		* Handle Before Cut -- show what is catted
		*****************************************************************************************************/
		fmanager.attachEvent("onBeforemarkCut", function(ids)
		{
			source = ids;
			mode = "move";
			webix.message("Cut: "+source);
			return true;
		});
		
		/***************************************************************************************************
		* Handle Before Paste -- do not use default functions - there are problems
		*****************************************************************************************************/
		fmanager.attachEvent("onBeforePasteFile", function(ids)
		{
			//handle_url_copy = handle_url_copy+"1";				
			

			if (source && mode)
			{
				source_str = "";
				source_arr = String(source).split(',');

				for (i = 0; i < source_arr.length; i ++) 
				{
					if(source_str == "")
						source_str =  source_str + source_arr[i];
					else
						source_str =  source_str + ":" + source_arr[i]
				}
 
				if (mode == "copy")
				{
					webix.ajax().post(handle_url_copy, { action:"copy", source:source_str, target:ids },{
							error:function(text, data, XmlHttpRequest){ alert("Απέτυχε η αντιγραφή"); fmanager.clearAll(); fmanager.load(handle_url_list);},
							success:function(text, data, XmlHttpRequest){ location.reload(); }
						}
					); 
					//fmanager.copyFile(source, ids);
				}
				else
				{
					webix.ajax().post(handle_url_copy, { action:"move", source:source_str, target:ids },{
							error:function(text, data, XmlHttpRequest){ alert("Απέτυχε η μεταφορά"); fmanager.clearAll(); fmanager.load(handle_url_list);},
							success:function(text, data, XmlHttpRequest){ location.reload(); }
						}
					); 
					//fmanager.moveFile(source, ids);
				}
				source = mode = "";
			}		
			
			return false;
		});

		/***************************************************************************************************
		* Handle Before Drop == Move -- do not use default functions - there are problems
		*****************************************************************************************************/
		fmanager.attachEvent("onBeforeDrop",function(context,ev){
			
			//handle_url_move = handle_url_move+"1";	
			
			$source_str = "";
			$target_str = "";
			source_arr = String(context.source).split(',');

			for (i = 0; i < source_arr.length; i ++) 
			{
				if($source_str == "")
					$source_str =  $source_str + source_arr[i];
				else
					$source_str =  $source_str + ":" + source_arr[i];
			}
			
			$target_str = $target_str + context.target;

			webix.ajax().post(handle_url_move, { action:"move", source:$source_str, target:$target_str },{
							error:function(text, data, XmlHttpRequest){ alert("Απέτυχε η μεταφορά"); fmanager.clearAll(); fmanager.load(handle_url_list);},
							success:function(text, data, XmlHttpRequest){ fmanager.clearAll(); fmanager.load(handle_url_list); }
						}
					); 
			return false;
		});
		
		var delete_id;
		
		/***************************************************************************************************
		* Handle Before remove -- do not use default it uses comma to separate files and this does not work
		*****************************************************************************************************/
		fmanager.attachEvent("onBeforeDeleteFile", function(id)
		{
			//handle_url_remove = handle_url_remove+"1";	
			
			var str_id = ""+id;
	
			var source_str = str_id.replace(/,/g, ":");
			
			webix.ajax().post(handle_url_remove, { action:"remove", source:source_str },{
							error:function(text, data, XmlHttpRequest){ alert("Απέτυχε η διαγραφή"); fmanager.clearAll(); fmanager.load(handle_url_list);},
							success:function(text, data, XmlHttpRequest){ location.reload(); }
						}
					); 

			//fmanager.deleteFile(id);
			return true;
		});

		
		
	});
	
	
</script>
</body>
</html>
<?php

?>