Ext.define('FileMgr.view.WndUploadImage', {
    extend: 'Ext.window.Window',
    alias: 'widget.WndUploadImage',
    itemId: 'WndUploadImage',
	title: 'Upload Image',
	
	modal: true,
    height: 120,
    width: 500,
	resizable:false,

	layout: 'border',
	
	items:
	[
		{
			xtype: 'form',
			region : 'center',
			itemid: 'ImgUploadFrm',
			layout: 'vbox',
			frame:true,
			bodyPadding: 5,
			items: 
			[
				{
					xtype:'filefield',
					name:'ImgFile',			
					emptyText: 'Select an image', 
					fieldLabel: 'Image',
					labelWidth: 50,
					width: 440,
					buttonText: '',
					buttonConfig: { iconCls: 'upload-btn' },						
				}
			]
		}
	],
	dockedItems: 
	[
        {
            xtype: 'toolbar',
            flex: 1,
            dock: 'bottom',
            ui: 'footer',
            layout: 
			{
                pack: 'end',
                type: 'hbox'
            },
            items: 
			[
                {
                    xtype: 'button',
                    text: 'Upload',
                    itemId: 'BtnUpload',
                    iconCls: 'upload-btn',
                    handler: function () 
					{										
                        this.up('.window').uploadImage();
                    }
                }
            ]
        }
    ],
	
    initComponent: function() 
	{
		this.tblName = '';
		this.recID = 0;
		this.callbackRefresh = '';	
		
		this.callParent(arguments);
    },
		
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  save    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	uploadImage: function () 
	{
		var win = this;
		
		var form = win.down('.form');
		console.log(form);
		//if(form.isValid())
		{
			form.submit(
			{ 
            	url: 'appAjax.php?appPage=FileMgr&appMethod=upload_image&tblName='+this.tblName+'&recID='+this.recID, 
       			waitMsg: 'Uploading your photo...', 
           			success: function(fp, o)  
							{ 
                				var msg =  'Uploaded file "' + o.result.file + '" on the server';
								//Ext.Msg.alert('Success',msg);
								if (typeof (win.callbackRefresh) == 'function')
								{
									win.callbackRefresh();
								}
								win.close();
                			},
							failure:function(form, action)
							{
								if(action.failureType == 'server')
								{ 
									var obj = Ext.JSON.decode(action.response.responseText); 
									var msg =  obj.errors.reason;
									
									win.close();
									Ext.Msg.alert('Upload Failed!', msg); 
								}
								else
								{ 
									var obj = Ext.JSON.decode(action.response.responseText); 
									var msg =  'Image upload server is unreachable : ' + action.response.responseText;
									
									win.close();
									Ext.Msg.alert('Warning!', msg); 
									
								}							
							}	 
                		});
		}
		
		
		
	}
});