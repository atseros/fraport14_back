// Require Popup Form of Upload Image
Ext.require([ 'FileMgr.view.WndUploadImage']);

Ext.define('FileMgr.view.ShowImageFile', {
    extend: 'Ext.Panel',
    alias: 'widget.ShowImageFile',
    itemId: 'ShowImageFile',
	
	layout:'vbox',
		
    initComponent: function() 
	{
		this.tblName;
		this.recID;
       
		this.toolbar;
		this.ImageField;
		
		this.callParent(arguments);
		
		// Toolbar for Btn 
		this.toolbar = Ext.create('Ext.toolbar.Toolbar', { dock: 'bottom', layout:'hbox' }); 
		this.addDocked(this.toolbar);  


		// OpenUploadBtn
		this.OpenUploadBtn = Ext.create('Ext.Button', 
		{
			text: 'Change Image',
			iconCls: 'upload-btn',
			flex: 1,
			handler: function()
			{
				var panel = this.up('panel');

				var x = new FileMgr.view.WndUploadImage;
				x.tblName = panel.tblName;
				x.recID = panel.recID;
				x.callbackRefresh = function(){ panel.loadImage(); };       
				x.show(this);
			}
		});	
		this.toolbar.insert(this.OpenUploadBtn);
		
		// Image
		this.ImageField = Ext.create('Ext.Img', {
			src: '',
			width:200,
			height: 200,
			mode : 'image',
			listeners: 
			{
				afterrender: function (me)
				{
					me.el.on( { load: function (evt, ele, opts){ me.setLoading(false,false); } } );
				}
			}

		});
		this.add(this.ImageField);

    },
		
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  loadImage    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	loadImage: function () 
	{
		this.ImageField.setSrc('');
		this.ImageField.setLoading(true,true);
		this.ImageField.setSrc('appAjax.php?appPage=FileMgr&appMethod=load_image&tblName='+this.tblName+'&recID='+this.recID+'&TimeMil='+(new Date().getTime()));
	},
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//  clearImage    function
	//////////////////////////////////////////////////////////////////////////////////////////////
	clearImage: function () 
	{
		this.tblName;
		this.recID;
		this.ImageField.setSrc('');
	}
});