Ext.define('FileMgr.view.ExtFileList' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.extfilelist',

	store: {
		fields: ['filetype', 'filename', 'filesize', 'filedate', 'filedir'],
		proxy: {
			type: 'ajax',
			url: 'appAjax.php?appPage=FileMgr&appMethod=dir_folder&tblName=' + this.tblName + '&recID=' + this.recID + '&curFolder=' + this.curFolder,
			reader: {
				type: 'json',
				root: 'files'
			}
		}
	},
		
	stripeRows: true,

	/**********************************************************************************************
	* initComponent
	**********************************************************************************************/
	initComponent: function() 
	{
		this.tblName;
		this.recID;
		this.curFolder;		
		this.allowSamba;
		this.sambaServer;
		
		this.toolbar;		
		this.HomeBtn;
		this.DownloadBtn;
		this.MngBtn;
		this.SambaLink;
		
		function renderIcon(val)
		{
			if(val in {avi:1,bmp:1,bmp2:1,bz2:1,c:1,cc:1,cgi:1,cpp:1,css:1,cxx:1,dhtml:1,dir:1,doc:1,document:1,docx:1,exe:1,file:1,fon:1,gif:1,gz:1,h:1,hpp:1,htm:1,html:1,inc:1,jar:1,java:1,jpeg:1,jpg:1,js:1,m3u:1,midi:1,mov:1,mp3:1,mpeg:1,mpg:1,pdf:1,php:1,php3:1,php4:1,php5:1,phtml:1,pl:1,pls:1,png:1,py:1,ra:1,ram:1,rar:1,rm:1,sh:1,shtml:1,sql:1,swf:1,tar:1,tbz:1,tgz:1,txt:1,txt:1,txt2:1,wav:1,xhtml:1,xls:1,xlsx:1,xml:1,zip:1})
				return '<img src="resources/images/filemgr/' + val + '.png">';
			else 
				return '<img src="resources/images/filemgr/file.png">'
		}
		
		this.columns = 
		[
			{ header: '', 				width: 30, 	dataIndex: 'filetype',	align: 'left',	renderer:renderIcon },
			{ header: 'Name', 			flex: 1,	dataIndex: 'filename' },
			{ header: 'Last Modified', 	width: 120, dataIndex: 'filedate' },
			{ header: 'Folder', 		dataIndex: 'filedir',	hidden: true }
		];
		
		this.callParent(arguments);
		
		// add toolbar and buttons
		this.toolbar = Ext.create('Ext.toolbar.Toolbar', { dock: 'top' }); 
		this.addDocked(this.toolbar);  

		// Home Button
		this.HomeBtn = Ext.create('Ext.Button', 
		{
			text: 'Home Folder',
			iconCls: 'filehome-btn',
			handler: function()
			{
				var panel = this.up('panel');
				panel.curFolder = "";
				panel.loadFolderFiles();				
			}
		});	
		this.toolbar.insert(this.HomeBtn);
		
		// Download Button
		this.DownloadBtn = Ext.create('Ext.Button', 
		{
			text: 'Download',
			iconCls: 'filedownload-btn',
			handler: function()
			{
				var panel = this.up('panel');
				records = panel.getSelectionModel().getSelection();
				
				if (records != null && records[0].get('filetype')!='dir')
				{
					panel.downloadFile( encodeURIComponent(records[0].get('filename')));
				}
			}
		});			
		this.DownloadBtn.setVisible(false);
		this.toolbar.insert(this.DownloadBtn);
		
		// File Manager Button
		this.MngBtn = Ext.create('Ext.Button', 
		{
			text: 'File Manager',
			iconCls: 'filemanager-btn',
			handler: function()
			{
				var panel = this.up('panel');
				if( panel.tblName != "" && panel.recID > 0)
				{
					var path = 'appMain.php?appPage=FileMgr&tblName=' + panel.tblName + '&recID=' + panel.recID;
					window.open(path);
				}
				return;
			}
		});			
		this.MngBtn.setVisible(false);
		this.toolbar.insert(this.MngBtn);
		
		// Samba Link
		this.SambaLink = Ext.create('Ext.container.Container', { html: '' });		
		this.toolbar.insert(this.SambaLink);
		this.SambaLink.setVisible(false);
	},
	
	/**********************************************************************************************
	* updateSambaLink
	**********************************************************************************************/
	updateSambaLink: function()
	{
		Ext.Ajax.request({
			url: 'appAjax.php',
			method: 'POST',
			params: {
				appPage: 'FileMgr',
				appMethod: 'getSambaUrl',
				tblName: this.tblName,
				recID: this.recID,
				domID: this.id,
			},
			success: function(response){
				var text = response.responseText;
				var obj = Ext.JSON.decode(text);				
				if (obj.success) 
				{
					var panel = Ext.getCmp(obj.domID);
					if (obj.allowSamba) 
					{
						panel.allowSamba=true;
						panel.sambaServer=obj.serverUrl;
						panel.SambaLink.update('<a href="'+obj.serverUrl+'" style="text-decoration:none" target="_blank">' +
													'<table border=0><tr>'+
														'<td><img src="resources/images/filemgr/folder_wrench.png"/></td>'+
														'<td style="font-size: 11px;font-weight: normal;font-family: tahoma,arial,verdana,sans-serif; color: #333; ">'+
														'&nbsp;'+obj.serverName+
														'</td>' +
													'</tr></table>' +
												'</a>');
						panel.SambaLink.setVisible(true);
												
						return;
					}
					else
					{
						panel.sambaServer	= "";
						panel.allowSamba	= false;
						panel.SambaLink.setVisible(false);
						
					}
				}				
			}
		});	
	},
	
	/**********************************************************************************************
	* clearFolderFiles
	**********************************************************************************************/
	clearFolderFiles: function()
	{
		this.tblName ="";
		this.recID = 0;
		this.curFolder = "";
		
		this.sambaServer	="";
		this.allowSamba	=false;
		this.SambaLink.setVisible(false);
		this.MngBtn.setVisible(false);
		this.DownloadBtn.setVisible(false);
		this.view.refresh();
		this.getStore().removeAll();
	},
	
	/**********************************************************************************************
	* loadFolderFiles
	**********************************************************************************************/
	loadFolderFiles: function()
	{
		if(this.tblName!="" && this.recID > 0)
		{
			this.updateSambaLink();
						
			this.getStore().load({
						url: 'appAjax.php?appPage=FileMgr&appMethod=dir_folder&tblName=' + this.tblName + '&recID=' + this.recID + '&curFolder=' + this.curFolder
					});
			this.MngBtn.setVisible(true);
			this.DownloadBtn.setVisible(true);
		}
		else
		{
			this.sambaServer	="";
			this.allowSamba	=false;
			this.SambaLink.setVisible(false);
			this.MngBtn.setVisible(false);
			this.DownloadBtn.setVisible(false);
			this.view.refresh();
		}
	},
	
	/**********************************************************************************************
	* downloadFile
	**********************************************************************************************/
	downloadFile: function(filename)
	{
		var path = 'appAjax.php?appPage=FileMgr&appMethod=download_file&tblName=' + this.tblName + '&recID=' + this.recID + '&curFolder=' + this.curFolder+'&filename=' + filename;
		
		window.open(path);
	},
		
	
	/**********************************************************************************************
	* listeners
	**********************************************************************************************/
	listeners: 
	{
		itemdblclick: function(view, record, item, index, eventObj, eOptsObj )
		{
			if(record.get('filetype')!='dir' && record.get('filename') == 'No files' )
			{
				
			}
			else if(record.get('filetype')!='dir')
			{
				this.downloadFile( encodeURIComponent(record.get('filename')) );
			}
			else
			{
				this.curFolder = encodeURIComponent( record.get('filedir') );
				this.loadFolderFiles();
			}
		}
	}
	
});