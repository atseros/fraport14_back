<?php

require_once(realpath(__DIR__."/../../db")."/class.DB_sysUsr.php");


/*********************************************************************************************
* CLASS FileMgr
*
* DESCRIPTION: 
*	Class that handles requests from FileMgr page 
*
*********************************************************************************************/
class FileMgr
{
	private $appFrw;
	
	/****************************************************************************************
	* FileMgr::CONSTRUCTOR
	****************************************************************************************/
	function FileMgr($appFrw)
	{
		$this->appFrw = $appFrw;
	}
	/****************************************************************************************
	* FileMgr->upload_image
	****************************************************************************************/
	function upload_image()
	{
		$tblName = isset($_REQUEST['tblName']) ? $_REQUEST['tblName'] : '';
		$recID = isset($_REQUEST['recID']) ? $_REQUEST['recID'] : 0;
		
		if( $tblName == "" OR $recID == 0 )
		{
			$result["success"] = false;
			$result["errors"]["reason"] = "No photo";			
			exit( json_encode($result) );
		}

		$tmp_uploaded_file = $_FILES["ImgFile"]["tmp_name"];
		$uploaded_filename = $_FILES["ImgFile"]["name"];
		
		if(!is_file($tmp_uploaded_file))
		{
			$result["success"] = false;
			$result["errors"]["reason"] = "No photo selected";			
			exit( json_encode($result) );
		}
		
		$ret = FileManager::uploadImg($tblName, $recID, $tmp_uploaded_file, $uploaded_filename);
				
		$result["success"] = true;
		$result["file"] = $uploaded_filename;
		echo json_encode($result);	 
	}
	
	/****************************************************************************************
	* FileMgr->load_image
	****************************************************************************************/
	function load_image()
	{
		$tblName = isset($_REQUEST['tblName']) ? $_REQUEST['tblName'] : '';
		$recID = isset($_REQUEST['recID']) ? $_REQUEST['recID'] : 0;
		
		if( $tblName == "" OR $recID == 0 )
			exit();
		
		FileManager::sendImage($tblName, $recID);
		
		exit();
	}
	
	/****************************************************************************************
	* FileMgr->getSambaUrl
	****************************************************************************************/
	function getSambaUrl()
	{
		$tblName 	= $_REQUEST['tblName'];
		$recID 		= $_REQUEST['recID'];
		$domID 		= $_REQUEST['domID'];
		$allowSamba	= false;
		
		// Samba Share only for Users, Superusers, Administrators
		if( $this->appFrw->UsrID )
		{
			$UsrDetails = DB_sysUsr::get_UsrDetails($this->appFrw, $this->appFrw->UsrID);
			
			if( $UsrDetails['UsrRole'] == 1 OR $UsrDetails['UsrRole'] == 2 OR $UsrDetails['UsrRole'] == 3 )
				$allowSamba 	= true;
			else
				$allowSamba 	= false;
		}
		else
		{
			$allowSamba 	= true;
		}
		
	
		$sambaShareUrl = FileManager::getTblSambaSharePath($this->appFrw, $tblName, $recID);
		
		$results["success"] 	= true;
		$results["domID"] 		= $domID;
		$results["allowSamba"] 	= $allowSamba;
		$results['serverUrl'] 	= $sambaShareUrl;
		$results['serverName'] 	= 'Athens';
		
		return json_encode( $results );
	}
	/****************************************************************************************
	* FileMgr->webix_list_folder
	****************************************************************************************/
	function webix_list_folder()
	{
		$tblName 			= $_REQUEST['tblName'];
		$recID 				= $_REQUEST['recID'];
		
		$root_folder = FileManager::getTblFolderPath($tblName, $recID );
				
		$ret_data = FileManager::dirFolder($root_folder, "", true);
		
		$all_data = array(
					array( 
						"value" => "\\",
						"type" => "folder",
						"size" => 0,
						"date" => 0,
						"id" => "/",
						"data" => $ret_data,
						"open" => true
					)
				);
			
		return json_encode( $all_data );
	}
		
	/****************************************************************************************
	* FileMgr->webix_upload
	****************************************************************************************/
	function webix_upload()
	{
		$tblName 		= $_REQUEST['tblName'];
		$recID 			= $_REQUEST['recID'];
		$path_utf8 		= $_POST["target"];
		$name_utf8 		= $_FILES['upload']['name'];
		$temp_utf8 		= $_FILES['upload']['tmp_name'];
		
		$rootFolder = FileManager::getTblFolderPath($tblName, $recID );
		
		if( FileManager::uploadFile($rootFolder, $path_utf8, $name_utf8, $temp_utf8) )
		{
			if($path_utf8 == "/")
				$id_utf8   = "/".$name_utf8;
			else 
				$id_utf8   = $path_utf8."/".$name_utf8;
			
			$result = array(
				"status" => "server",
				"id"     => $id_utf8,
				"folder" => $path_utf8,
				"file"   => $name_utf8,
				"type"   => pathinfo($name_utf8, PATHINFO_EXTENSION)
			);
				
			return json_encode($result);
		}
	}
	
	/****************************************************************************************
	* FileMgr->webix_download
	****************************************************************************************/
	function webix_download()
	{		
		$tblName 		= $_REQUEST['tblName'];
		$recID 			= $_REQUEST['recID'];
		$forceOpen 		= isset($_REQUEST['forceOpen']) ? $_REQUEST['forceOpen'] : 0;

		$RelFilePathName_utf8 = isset($_REQUEST["source"]) ? $_REQUEST["source"] : "";
		
		$rootFolder = FileManager::getTblFolderPath($tblName, $recID );

		
		return FileManager::downloadFile($rootFolder, $RelFilePathName_utf8 , $forceOpen);
	}
	
	/****************************************************************************************
	* FileMgr->webix_copy
	****************************************************************************************/
	function webix_copy()
	{
		$tblName 			= $_REQUEST['tblName'];
		$recID 				= $_REQUEST['recID'];
		$FilesToCopy_utf8 	= $_POST["source"];
		$Destination_utf8 	= $_POST["target"];
			
		$Destination_utf8 = str_replace('["', "", $Destination_utf8);
		$Destination_utf8 = str_replace('"]', "", $Destination_utf8);
		$Destination_utf8 = str_replace('\\\\', "\\", $Destination_utf8);
		
		$rootFolder = FileManager::getTblFolderPath($tblName, $recID );
		
		if( FileManager::copyFileOrFolder($rootFolder, $FilesToCopy_utf8, $Destination_utf8) )
			return json_encode("ok");
		else 
			return;
	}

	/****************************************************************************************
	* FileMgr->webix_move
	****************************************************************************************/
	function webix_move()
	{
		$tblName 		= $_REQUEST['tblName'];
		$recID 			= $_REQUEST['recID'];
		$FilesToMove_utf8 = $_POST["source"];
				
		if( substr( $_POST["target"] , 0 , 6) == "{\"rind")
		{
			$pos1 = strpos($_POST["target"], ",\"row\":");
			$pos2 = strpos($_POST["target"], ",\"column\":");

			$Destination_utf8 = substr($_POST["target"], $pos1+ 7, $pos2 - $pos1 - 7);
			$Destination_utf8 = str_replace('\\\\', "\\", $Destination_utf8);
			$Destination_utf8 = str_replace('"', "", $Destination_utf8);
		}
		else
		{
			$Destination_utf8 = $_POST["target"];
	
			$Destination_utf8 = str_replace('["', "", $Destination_utf8);
			$Destination_utf8 = str_replace('"]', "", $Destination_utf8);
			$Destination_utf8 = str_replace('\\\\', "\\", $Destination_utf8);
		}
		
		$rootFolder = FileManager::getTblFolderPath($tblName, $recID );
		
		if( FileManager::moveFileOrFolder($rootFolder, $FilesToMove_utf8, $Destination_utf8) )
			return json_encode("ok");
		else
			return;
	}
	
	/****************************************************************************************
	* FileMgr->webix_remove
	****************************************************************************************/
	function webix_remove()
	{
		$tblName 			= $_REQUEST['tblName'];
		$recID 				= $_REQUEST['recID'];
		$FilesToRemove_utf8 = $_POST["source"];
		
		$FilesToRemove_utf8 = str_replace('["', "", $FilesToRemove_utf8);
		$FilesToRemove_utf8 = str_replace('"]', "", $FilesToRemove_utf8);
		
		$rootFolder = FileManager::getTblFolderPath($tblName, $recID );
		
		if( FileManager::removeFileOrFolder($rootFolder, $FilesToRemove_utf8) )
			return json_encode("ok");
		else 
			return ;
	}
	
	/****************************************************************************************
	* FileMgr->webix_rename
	****************************************************************************************/
	function webix_rename()
	{
		$tblName 				= $_REQUEST['tblName'];
		$recID 					= $_REQUEST['recID'];
		$SourceRelPathName_utf8 = $_POST["source"];
		$NewName_utf8 			= $_POST["target"];
		
		$rootFolder = FileManager::getTblFolderPath($tblName, $recID );
		
		if( FileManager::renameFileOrFolder($rootFolder,$SourceRelPathName_utf8, $NewName_utf8) )
			return json_encode("ok");
		else 
			return json_encode("error");
	}
	
	/****************************************************************************************
	* FileMgr->webix_create
	****************************************************************************************/
	function webix_create()
	{
		$tblName 			= $_REQUEST['tblName'];
		$recID 				= $_REQUEST['recID'];		
		
		$NewFolderName_utf8 = $_POST["source"];
		$TargetFolder_utf8 	= $_POST["target"];
		
		$NewFolderName_utf8 = str_replace('["', "", $NewFolderName_utf8);
		$NewFolderName_utf8 = str_replace('"]', "", $NewFolderName_utf8);
	
		$TargetFolder_utf8 = str_replace('["', "", $TargetFolder_utf8);
		$TargetFolder_utf8 = str_replace('"]', "", $TargetFolder_utf8);
		
		$root_folder = FileManager::getTblFolderPath($tblName, $recID );
		
		if( FileManager::createFolder($root_folder, $TargetFolder_utf8, $NewFolderName_utf8) )
			return json_encode("ok");
		else
			return json_encode("error");
	}
	
	/****************************************************************************************
	* FileMgr->download_file
	****************************************************************************************/
	function download_file()
	{
		$tblName 		= $_REQUEST['tblName'];
		$recID 			= $_REQUEST['recID'];
		$relPathFolder 	= $_REQUEST['curFolder'];
		$filename 		= $_REQUEST['filename'];
		
		if($relPathFolder != "")
			$relPathFileName = $relPathFolder.'/'.$filename;
		else 
			$relPathFileName = $filename;
			
		$rootFolder = FileManager::getTblFolderPath($tblName, $recID );

		return FileManager::downloadFile($rootFolder, $relPathFileName );
	}
	
	/****************************************************************************************
	* FileMgr->dir_folder
	****************************************************************************************/
	function dir_folder()
	{
		$dir = '';
		
		$tblName 		= $_REQUEST['tblName'];
		$recID 			= $_REQUEST['recID'];
		$relPathFolder 	= $_REQUEST['curFolder'];
						
		$result = FileManager::dirFolder_extFileList($tblName, $recID, $relPathFolder);
		
		return json_encode(array('files' => $result));
	}
	
	
}
?>